<?php
  $seccionActiva=2;
  include_once('../cabecera.php');
  
  $estadisticas = 0;
  
  if($_SESSION['tipoUsuario'] == 'CLIENTE'){
    $cliente      = datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');
    $estadisticas = estadisticasDocumentos($cliente['codigoCliente']);
  } 
  else {
    if (isset($_POST['cliente'])) {
      $cliente      = $_POST['cliente'];
      $estadisticas = estadisticasDocumentos($cliente);
    } 
    else if (isset($_GET['cliente'])) {
      $cliente      = $_GET['cliente'];
      $estadisticas = estadisticasDocumentos($cliente);
    }
  }  
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Documentación:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-file-text"></i> <span class="value"><?php echo $estadisticas; ?></span><br />Documentos registrados</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Formularios</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <?php if ($_SESSION['tipoUsuario'] != 'CLIENTE') { ?>
				          <a href="<?php echo $_CONFIG['raiz']; ?>documentacion/seleccionaCliente.php" class="shortcut"><i class="shortcut-icon icon-search"></i><span class="shortcut-label">Nueva busqueda</span> </a>
                <?PHP } ?>
                <!--a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a-->
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
	
        <?php if ($_SESSION['tipoUsuario'] == 'CLIENTE') { ?>
          <div class="span12">        
            <div class="widget widget-table action-table">
              <div class="widget-header"> 
                <i class="icon-list"></i>
                <h3>Documentos registrados</h3>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable">
                  <thead>
                    <tr>
                      <th> Consultoría </th>
                      <th> Documento </th>
                      <th class="centro"></th>
                      <th class="centro">Documentos firmados</th>
                    </tr>
                  </thead>
                <tbody>

                <?php
                  imprimeDocumentos($cliente['codigoCliente']);
                ?>
              
              </tbody>
            </table>
          </div>
        <?php 
          }
          else if(isset($_POST['cliente']) || isset($_GET['cliente'])) {
        ?>
          <div class="span12">		    
            <div class="widget widget-table action-table">
              <div class="widget-header"> 
                <i class="icon-list"></i>
                <h3>Documentos registrados</h3>
              </div>
          <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable">
                  <thead>
                    <tr>
                      <th> Consultoría </th>
                      <th> Documento </th>
                      <th class="centro"></th>
                      <th class="centro">Documentos firmados</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php
                      imprimeDocumentos($cliente,2);
                    ?>
              
                  </tbody>
                </table>
              </div>
        <?php 
          } 
          else { 
        ?>
          <h6 style="width:100%;clear:both;font-size:26px;text-align:center;">Debes elegir un cliente</h6>
        
        <?php } ?>
          <!-- /widget-content-->
      </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->
<?php
  abreVentanaModalDocumentacion('seleccion de registros');    
    echo '<div id="listadoRegistros"></div>';
    campoOculto('', 'codigoTrabajo');
	  campoOculto('', 'codigoDocumento');
    // campoTexto('codigoTrabajo', 'trabajo');
	  // campoTexto('codigoDocumento', 'documento');
	  campoOculto('','registros');
  cierraVentanaModalDocumentacion('enviar','Descargar','icon-download');
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
  $('.btnModal').unbind();
  $('#enviar').unbind();
  
  $('.btnModal').click(function(e){
    e.preventDefault();
   /*if($(this).attr('codigo')=='VIGILANCIA'){
      $('#cajaGestion form').attr('target','_blank');
    } else {
      $('#cajaGestion form').attr('target','_self');
    }*/

    $('#codigoTrabajo').val($(this).attr('codigoTrabajo'));
    $('#codigoDocumento').val($(this).attr('codigo'));

    var parametros = {
      "codigo"        : $(this).attr('codigo'),
      "codigoTrabajo" : $(this).attr('codigoTrabajo')
    }
    
    $.ajax({
      data : parametros,
      url  : 'obtieneListado.php',
      type : 'post',
      beforeSend: function () {
        $("#listadoRegistros").html('<center><i class="icon-refresh icon-spin"></i></center>');
      },
      success: function (response) {
        $("#listadoRegistros").html(response);
      }
    });
    
    $('input[name=todo]').change(function(){
      alert($(this).val());
    });
    
    $('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
  });

  $('#enviar').click(function(e){
    e.preventDefault();
    
    var valoresChecks = [];
    var i = 0;
    
    $('input[name="listado[]"]:checked').each(function() {
      valoresChecks[i] = $(this).val();
      i++;
    });
    
    if (valoresChecks.length == 0 && $("#info").length == 0) {
      alert('Debes seleccionar al menos un registro');
    } 
    else {
      if(valoresChecks.length != 0) {
        $('#registros').val(valoresChecks.toString());
      } 
      else {
        $('#registros').val($('#info').val());
      }

      var campos = [];
      
      campos['codigoTrabajo']   = $('#codigoTrabajo').val();
      campos['codigoDocumento'] = $('#codigoDocumento').val();
      campos['registros']       = $('#registros').val();
      
      
      creaFormulario('generaDocumento.php', campos, 'post', '_blank');

      $('#cajaGestion').modal('hide');
      // $('#cajaGestion form').submit(); 
    }
  });
});
</script>
<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
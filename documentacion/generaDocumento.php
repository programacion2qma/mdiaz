<?php
	include_once('funciones.php');
	
	
	if(isset($_POST['codigoTrabajo'])){
		$documentos = [
			1 => 'ANEXO I REGISTRO DE ACTIVIDADES DE TRATAMIENTO',
			8 => 'ANEXO VIII: FUNCIONES Y OBLIGACIONES',
			'ENCARGADOS'      => 'CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO',					  
			'ENCARGADOSACTUA' => 'CONTRATO PARA ACTUAR COMO ENCARGADO DEL TRATAMIENTO',
			'VIGILANCIA'      => 'CARTEL VIDEOVIGILANCIA',
			'DELEGADOS'       => 'ANEXO IX: AUTORIZACIONES DELEGADAS',
			'SUBENCARGADOS'   => 'CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO'
		];
					  
		generaDocumento($_POST['codigoTrabajo'],$documentos[$_POST['codigoDocumento']]);

	} 
	else {
		generaDocumento($_GET['codigo'],$_GET['documento']);
	}
?>
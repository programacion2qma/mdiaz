<?php
	include_once('funciones.php');
	
	compruebaSesion();

	$datos = arrayFormulario();

	extract($datos);
	
	echo '<br/>';

	conexionBD();

	$sql = "SELECT 
				c.razonSocial, 
				c.domicilio, 
				c.cp, 
				c.ficheroLogo, 
				c.localidad, 
				c.cif, 
				c.provincia, 
				sf.referencia AS familia, 
				t.formulario, 
				c.codigo AS codigoCliente, 
				c.videovigilancia 
			FROM 
				trabajos t
				INNER JOIN clientes c ON t.codigoCliente = c.codigo 
				INNER JOIN servicios s ON t.codigoServicio = s.codigo 
				INNER JOIN servicios_familias sf ON s.codigoFamilia = sf.codigo 
			WHERE 
				t.codigo = ".$codigoTrabajo.";";

	$datos      = consultaBD($sql, false, true);
	$formulario = recogerFormularioServicios($datos);

	if ($codigo == 8) {

		if ($formulario['pregunta8'] != '') {
			$nombreRL = $formulario['pregunta8'];
		
			if (isset($formulario['pregunta627'])) {
				$nombreRL .= ' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
			}
		
			campoCheckIndividual('listado[]', $nombreRL.' (REPRESENTANTE LEGAL)', false, 'RL');
		}

		$where = '';

		if ($formulario['pregunta14'] != '') {
			$where = " AND nifUsuarioLOPD!='".$formulario['pregunta14']."'";
		}

		$sql = "SELECT 
					* 
				FROM 
					usuarios_lopd 
				WHERE 
					codigoTrabajo = ".$codigoTrabajo." 
				AND 
					(fechaBajaUsuarioLOPD = '' OR fechaBajaUsuarioLOPD='0000-00-00') ".$where.";";

		$consulta = consultaBD($sql);
		while ($item = mysql_fetch_assoc($consulta)) {
			campoCheckIndividual('listado[]', $item['nombreUsuarioLOPD'].' ('.$item['puestoUsuarioLOPD'].')', false, $item['codigo']);
		}

	
		$consulta = consultaBD("SELECT * FROM proveedores_lopd WHERE codigoTrabajo=".$codigoTrabajo.";");
		while ($item = mysql_fetch_assoc($consulta)) {
			campoCheckIndividual('listado[]', $item['nombre'].' (PROVEEDOR)',false,$item['codigo'].'_PROV');
		}

	} 
	else if ($codigo == 'ENCARGADOS') {
		$encargados = array(
			158,
			168,
			178,
			189,
			200,
			210,
			220,
			230,
			240,
			250,
			260,
			270,
			280,
			290,
			487
		);

		foreach ($encargados as $key => $value) {
			if($formulario['pregunta'.$value] == 'SI') {
				$i = $value + 1;
				campoCheckIndividual('listado[]',$formulario['pregunta'.$i],false,'ENCARGADOFIJO_'.$value);
			}
		}

		$sql = 'SELECT 
					* 
				FROM 
					otros_encargados_lopd 
				WHERE 
					codigoTrabajo = '.$codigoTrabajo.' 
				AND 
					cifEncargado != "'.$formulario['pregunta9'].'";';
	
		$encargados = consultaBD($sql);
		while ($item = mysql_fetch_assoc($encargados)) {
			campoCheckIndividual('listado[]', $item['nombreEncargado'], false, 'ENCARGADO_'.$item['codigo']);
		}

	} 
	else if($codigo == 1) {
		$encargados = array(
			158 => 574,
			168 => 575,
			178 => 576,
			189 => 577,
			200 => 578,
			210 => 579,
			220 => 609,
			230 => 580,
			240 => 581,
			250 => 582,
			260 => 583,
			270 => 584,
			280 => 585,
			290 => 586,
			487 => 587
		);
	
		$ficheros = consultaBD('SELECT * FROM declaraciones WHERE codigoCliente='.$datos['codigoCliente']);
		while ($item = mysql_fetch_assoc($ficheros)) {
			$entro = false;
		
			foreach ($encargados as $key => $value) {
				if (!$entro) {
					if ($formulario['pregunta'.$key] == 'SI') {
						
						$ficherosTrabaja = explode('&$&', $formulario['pregunta'.$value]);
						if (in_array($item['codigo'], $ficherosTrabaja)){
							$i = $key + 1;						
							campoCheckIndividual('listado[]', $item['nombreFichero'],false,$item['codigo'].'_FIJO_'.$key);
							$entro = true;
						}
					}
				}
			}
		
			if (!$entro) {
				$otrosEncargados = consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$codigoTrabajo);
				while ($e = mysql_fetch_assoc($otrosEncargados)) {
					if (!$entro) {
						$ficherosTrabaja = explode('&$&',$e['ficheroEncargado']);
						
						if (in_array($item['codigo'], $ficherosTrabaja)) {						
							campoCheckIndividual('listado[]',$item['nombreFichero'],false,$item['codigo'].'_NOFIJO_'.$e['codigo']);
							$entro = true;
						}
					}
				}
			}
		
			if(!$entro){
				campoCheckIndividual('listado[]',$item['nombreFichero'],false,$item['codigo'].'_SIN');
			}
		}

	} 
	else if($codigo == 'ENCARGADOSACTUA') {
		$encargados = array(
			158 => 574,
			168 => 575,
			178 => 576,
			189 => 577,
			200 => 578,
			210 => 579,
			220 => 609,
			230 => 580,
			240 => 581,
			250 => 582,
			260 => 583,
			270 => 584,
			280 => 585,
			290 => 586,
			487 => 587
		);
	
		$sql = 'SELECT 
					* 
				FROM 
					declaraciones 
				WHERE 
					codigoCliente = '.$datos['codigoCliente'].' 
				AND 
					cif_nif_responsableFichero != "'.$datos['cif'].'";';
		
		$ficheros = consultaBD($sql);
		while ($item = mysql_fetch_assoc($ficheros)) {
			foreach ($encargados as $key => $value) {

				if ($formulario['pregunta'.$key] == 'SI') {
					$ficherosTrabaja = explode('&$&', $formulario['pregunta'.$value]);

					if (in_array($item['codigo'], $ficherosTrabaja)) {

						$i = $key == 487 ? 289 : $key + 2;

						if (trim($formulario['pregunta'.$i]) == trim($datos['cif'])) {
							campoCheckIndividual('listado[]', $item['nombreFichero'].' - '.$item['n_razon'],false,'RESP_'.$item['codigo'].'_FIJO_'.$key);
							
							$responsables = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion='.$item['codigo']);
							while ($responsable = mysql_fetch_assoc($responsables)) {
								if ($responsable['razonSocial'] != '') {
									campoCheckIndividual('listado[]',$item['nombreFichero'].' - '.$responsable['razonSocial'],false,'OTRO_'.$responsable['codigo'].'_FIJO_'.$key);
								}
							}
						}
					}
				}
			}

			$otrosEncargados = consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$codigoTrabajo);
			while ($e = mysql_fetch_assoc($otrosEncargados)) {

				$ficherosTrabaja = explode('&$&', $e['ficheroEncargado']);

				if (in_array($item['codigo'], $ficherosTrabaja)) {
					
					if(trim($e['cifEncargado']) == trim($datos['cif']) ){
						campoCheckIndividual('listado[]',$item['nombreFichero'].' - '.$item['n_razon'],false,'RESP_'.$item['codigo'].'_NOFIJO_'.$e['codigo']);
						
						$responsables = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion='.$item['codigo']);
						while ($responsable = mysql_fetch_assoc($responsables)) {
							if ($responsable['razonSocial'] != '') {
								campoCheckIndividual('listado[]',$item['nombreFichero'].' - '.$responsable['razonSocial'],false,'OTRO_'.$responsable['codigo'].'_NOFIJO_'.$e['codigo']);
							}
						}
					}
				}
			}
		}

	} 
	else if ($codigo == 'VIGILANCIA') {
		areaTexto('info', 'Más información sobre el tratamiento de sus datos personales', $datos['videovigilancia'],'areaInforme'); 
	} 
	else if ($codigo == 'DELEGADOS') {
		campoCheckIndividual('listado[]','Autorizaciones delegadas',false,0);
	
		$sql = "SELECT 
					d.codigo, 
					u1.nombreUsuarioLOPD AS usuario1, 
					u2.nombreUsuarioLOPD AS usuario2 
				FROM 
					delegaciones_lopd d
				INNER JOIN usuarios_lopd u1 ON 
					d.usuario1Delegaciones = u1.codigo 
				INNER JOIN usuarios_lopd u2 ON 
					d.usuario2Delegaciones = u2.codigo 
				WHERE 
					d.codigoTrabajo = ".$codigoTrabajo;
	
		$consulta = consultaBD($sql);
		while($item=mysql_fetch_assoc($consulta)){
			campoCheckIndividual('listado[]','Autorizaciones delegadas - '.$item['usuario1'].' -> '.$item['usuario2'],false,$item['codigo']);
		}
	
	} 
	else if ($codigo == 'SUBENCARGADOS') {
		$encargados = array(
			158 => 574,
			168 => 575,
			178 => 576,
			189 => 577,
			200 => 578,
			210 => 579,
			220 => 609,
			230 => 580,
			240 => 581,
			250 => 582,
			260 => 583,
			270 => 584,
			280 => 585,
			290 => 586,
			487 => 587
		);
	
		$sql = 'SELECT 
					* 
				FROM 
					declaraciones 
				WHERE 
					codigoCliente = '.$datos['codigoCliente'].' 
				AND 
					cif_nif_responsableFichero != "'.$datos['cif'].'"';
		
		$ficheros = consultaBD($sql);	
		while ($item = mysql_fetch_assoc($ficheros)) {
			$query = 'SELECT * FROM responsables_fichero WHERE codigoDeclaracion='.$item['codigo'];
		
			$responsables = consultaBD($query);
			foreach ($encargados as $key => $value) {
				if ($formulario['pregunta'.$key] == 'SI') {
					$ficherosTrabaja = explode('&$&', $formulario['pregunta'.$value]);
				
					if (in_array($item['codigo'], $ficherosTrabaja)) {
						$i = $key + 1;
						
						if (mysql_num_rows($responsables) > 0) {
							$sql = 'SELECT 
										* 
									FROM 
										declaraciones_subcontrata 
									WHERE 
										codigoTrabajo = '.$codigoTrabajo.' 
									AND 
										tipo = "ENCARGADOFIJO" 
									AND 
										codigoResponsable = '.$key;
							
							$subcontratas = consultaBD($sql);						
							while($s = mysql_fetch_assoc($subcontratas)) {
								$responsables = consultaBD($query);
							
								while ($r = mysql_fetch_assoc($responsables)) {
									campoCheckIndividual('listado[]',$s['empresa'].' - '.$r['razonSocial'].' - '.$item['nombreFichero'],false,$s['codigo'].'_FIJO_'.$r['codigo'].'_'.$item['codigo']);
								}
							}
						}
					}
				}
			}
		
			$responsables    = consultaBD($query);
			$otrosEncargados = consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$codigoTrabajo);
		
			while ($e = mysql_fetch_assoc($otrosEncargados)) {
				$ficherosTrabaja = explode('&$&',$e['ficheroEncargado']);
			
				if (in_array($item['codigo'], $ficherosTrabaja)) {
					if(mysql_num_rows($responsables) > 0) {
						$sql = 'SELECT 
									* 
								FROM 
									declaraciones_subcontrata 
								WHERE 
									codigoTrabajo = '.$codigoTrabajo.' 
								AND 
									tipo = "ENCARGADO" 
								AND 
									codigoResponsable = '.$e['codigo'];
						
						$subcontratas = consultaBD($sql);
						while ($s = mysql_fetch_assoc($subcontratas)){
							$responsables = consultaBD($query);
							while ($r = mysql_fetch_assoc($responsables)) {
								campoCheckIndividual('listado[]',$s['empresa'].' - '.$r['razonSocial'].' - '.$item['nombreFichero'],false,$s['codigo'].'_NOFIJO_'.$r['codigo'].'_'.$item['codigo']);
							}
						}
					}
				}
			}
		}
	}

	cierraBD();
?>

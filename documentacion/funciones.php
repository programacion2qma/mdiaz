<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

function seleccionaCliente(){
	abreVentanaGestion("Selecciona cliente",'index.php');
		if($_SESSION['tipoUsuario'] == 'COMERCIAL'){
			$sql="SELECT clientes.codigo as codigo, clientes.razonSocial AS texto
			FROM (trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo) 
			LEFT JOIN ventas_servicios ON trabajos.codigoVenta=ventas_servicios.codigo
			WHERE ventas_servicios.codigoComercial=".$_SESSION['codigoU']." AND trabajos.codigoServicio <> '408'
			GROUP BY clientes.razonSocial";
		} else {
			$sql="SELECT clientes.codigo as codigo, clientes.razonSocial AS texto
			FROM (trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo) 
			LEFT JOIN ventas_servicios ON trabajos.codigoVenta=ventas_servicios.codigo
			WHERE trabajos.codigoServicio <> '408'
			GROUP BY clientes.razonSocial";
		}

campoSelectConsulta('cliente','Cliente',$sql);
cierraVentanaGestion('../consultorias/index.php',true,true,'Ver documentos','icon-check',false);
}

function estadisticasDocumentos($cliente){
	$listado=listadoDocumentos(false,$cliente);
	$trabajos=consultaBD('SELECT COUNT(trabajos.codigo) AS total FROM trabajos LEFT JOIN servicios ON trabajos.codigoServicio=servicios.codigo  WHERE servicios.servicio LIKE "%LOPD%" AND codigoCliente='.$cliente,true,true);
	return count($listado['nombreDocumentos']) * $trabajos['total'];
}

function estadisticasDocumentosFirmados($codigoTrabajo,$documento){
	$documentos=consultaBD('SELECT COUNT(documentos_firmados.codigo) AS total FROM documentos_firmados WHERE documento="'.$documento.'" AND codigoTrabajo='.$codigoTrabajo,true,true);
	return $documentos['total'];
}

function imprimeDocumentos($cliente, $tipo = 1) {
	global $_CONFIG;
		
	$sql = "SELECT 
				t.codigo, 
				s.servicio, 
				t.formulario, 
				sf.referencia, 
				t.codigoCliente 
			FROM 
				trabajos t 
			LEFT JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			LEFT JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo
			WHERE 
				s.servicio LIKE '%LOPD%' 
			AND 
				codigoCliente = ".$cliente.";";

	$trabajos = consultaBD($sql, true);	

	while ($datos = mysql_fetch_assoc($trabajos)) {
		
		$listado = listadoDocumentos($datos,$cliente);
		extract($listado);

		for ($i=0; $i < count($ficheroDocumentos); $i++) {
			if($nombreDocumentos[$i] == 'CONTRATO PARA ACTUAR COMO ENCARGADO DEL TRATAMIENTO') {
				echo "
					<tr>
						<td>".$datos['servicio']."</td>
						<td>".$nombreDocumentos[$i]."</td>
						<td class='centro'>
							<div class='btn-group'>
								<a codigo='ENCARGADOSACTUA' codigoTrabajo='".$datos['codigo']."' class='noAjax btn btn-propio btnModal' href='".$_CONFIG['raiz']."documentacion/generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."' target='_blank'><i class='icon-cloud-download'></i> Descargar</a></li>
							</div>
					 	</td>";
			}
			else if($nombreDocumentos[$i]=='CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO'){
				echo "
					<tr>
						<td>".$datos['servicio']."</td>
						<td>".$nombreDocumentos[$i]."</td>
						<td class='centro'>
							<div class='btn-group'>
								<a codigo='ENCARGADOS' codigoTrabajo='".$datos['codigo']."' class='noAjax btn btn-propio btnModal' href='".$_CONFIG['raiz']."documentacion/generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."' target='_blank'><i class='icon-cloud-download'></i> Descargar</a></li>
							</div>
					 	</td>";
			}
			else if($nombreDocumentos[$i]=='CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO'){
				echo "
					<tr>
						<td>".$datos['servicio']."</td>
						<td>".$nombreDocumentos[$i]."</td>
						<td class='centro'>
							<div class='btn-group'>
								<a codigo='SUBENCARGADOS' codigoTrabajo='".$datos['codigo']."' class='noAjax btn btn-propio btnModal' href='".$_CONFIG['raiz']."documentacion/generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."' target='_blank'><i class='icon-cloud-download'></i> Descargar</a></li>
							</div>
					 	</td>";
			}
			else if($nombreDocumentos[$i]=='ANEXO VIII: FUNCIONES Y OBLIGACIONES'){
				echo "
					<tr>
						<td>".$datos['servicio']."</td>
						<td>".$nombreDocumentos[$i]."</td>
						<td class='centro'>
							<div class='btn-group'>
								<a codigo='8' codigoTrabajo='".$datos['codigo']."' class='noAjax btn btn-propio btnModal' href='".$_CONFIG['raiz']."documentacion/generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."'><i class='icon-cloud-download'></i> Descargar</a></li>
							</div>
					 	</td>";
			}	
			else if($nombreDocumentos[$i]=='ANEXO I REGISTRO DE ACTIVIDADES DE TRATAMIENTO'){
				echo "
					<tr>
						<td>".$datos['servicio']."</td>
						<td>".$nombreDocumentos[$i]."</td>
						<td class='centro'>
							<div class='btn-group'>
								<a codigo='1' codigoTrabajo='".$datos['codigo']."' class='noAjax btn btn-propio btnModal' href='".$_CONFIG['raiz']."documentacion/generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."'><i class='icon-cloud-download'></i> Descargar</a></li>
							</div>
					 	</td>";
			}
			else if($nombreDocumentos[$i]=='CARTEL VIDEOVIGILANCIA'){
				echo "
					<tr>
						<td>".$datos['servicio']."</td>
						<td>".$nombreDocumentos[$i]."</td>
						<td class='centro'>
							<div class='btn-group'>
								<a codigo='VIGILANCIA' codigoTrabajo='".$datos['codigo']."' class='noAjax btn btn-propio btnModal' href='".$_CONFIG['raiz']."documentacion/generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."'><i class='icon-cloud-download'></i> Descargar</a></li>
							</div>
					 	</td>";
			}
			else if($nombreDocumentos[$i]=='ANEXO IX: AUTORIZACIONES DELEGADAS'){
				echo "
					<tr>
						<td>".$datos['servicio']."</td>
						<td>".$nombreDocumentos[$i]."</td>
						<td class='centro'>
							<div class='btn-group'>
								<a codigo='DELEGADOS' codigoTrabajo='".$datos['codigo']."' class='noAjax btn btn-propio btnModal' href='".$_CONFIG['raiz']."documentacion/generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."' target='_blank'><i class='icon-cloud-download'></i> Descargar</a></li>
							</div>
					 	</td>";
			}			
			else{
				echo "
					<tr>
						<td>".$datos['servicio']."</td>
						<td>".$nombreDocumentos[$i]."</td>
						<td class='centro'>
							<div class='btn-group'>";
							if($nombreDocumentos[$i]=='MANUAL DE PBC'){
				echo "			<a class='noAjax btn btn-propio' target='_blank' href='".$_CONFIG['raiz'].$ficheroDocumentos[$i].$datos['codigo']."'><i class='icon-cloud-download'></i> Descargar</a></li>";
							} else {
				echo "			<a class='noAjax btn btn-propio' href='".$_CONFIG['raiz']."documentacion/	generaDocumento.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."'><i class='icon-cloud-download'></i> Descargar</a></li>";
							}
				echo "		</div>
					 	</td>";
			}

			$btn = '';

			if ($tipo>1 && $nombreDocumentos[$i] != 'MANUAL DE PBC') {
				$firmados = comprobarFirmadosVistos($datos['codigo'], $ficheroDocumentos[$i]);
				if($firmados['documentos'] == 'SI') {
					$texto='Ver documentos';
					$clase='';
					if($firmados['noVistos']=='SI'){
						$texto='Nuevos documentos';
						$clase='nuevoDocumento';
					}
					$btn="<div class='btn-group'>
								<a class='noAjax btn btn-propio $clase' href='".$_CONFIG['raiz']."documentacion/documentosFirmados.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."'><i class='icon-cloud-upload'></i> $texto</a></li>
							</div>";
				}
			} else if($nombreDocumentos[$i]!='MANUAL DE PBC'){
				$btn="<div class='btn-group'>
								<a class='noAjax btn btn-propio' href='".$_CONFIG['raiz']."documentacion/documentosFirmados.php?codigo=".$datos['codigo']."&documento=".$ficheroDocumentos[$i]."'><i class='icon-cloud-upload'></i> Ver/subir documentos</a></li>
							</div>";
			}
			echo "<td class='centro'>
							$btn
					 	</td>
					</tr>";
		}
	}
	
}

function comprobarFirmadosVistos($codigoTrabajo,$documento){
	$res=array('documentos'=>'NO','noVistos'=>'NO');
	$listado=consultaBD('SELECT * FROM documentos_firmados WHERE documento="'.$documento.'" AND codigoTrabajo='.$codigoTrabajo,true);
	while($item=mysql_fetch_assoc($listado)){
		$res['documentos']='SI';
		if($item['visto']=='NO'){
			$res['noVistos']='SI';
		}
	}
	return $res;
}

function imprimeDocumentosFirmados($codigoTrabajo,$documento){
	global $_CONFIG;
	conexionBD();
	$documentos=consultaBD('SELECT * FROM documentos_firmados WHERE documento="'.$documento.'" AND codigoTrabajo='.$codigoTrabajo,true);
	cierraBD();

	while($datos=mysql_fetch_assoc($documentos)){
			$target='';
			$icono='';
			$clase='';
			if(strpos($datos['ficheroFirmado'], '.docx')===false && strpos($datos['ficheroFirmado'], '.doc')===false && strpos($datos['ficheroFirmado'], '.xlsx')===false && strpos($datos['ficheroFirmado'], '.xls')===false){
				$target="target='_blank'";
			}
			if($datos['visto']=='NO' && $_SESSION['tipoUsuario']!='CLIENTE'){
				$icono='<i id="icono'.$datos['codigo'].'" class="icon-cloud-upload iconoFactura nuevoDocumento"></i>';
				$clase='descargaFichero';
			}
			echo "
					<tr>
						<td>".formateaFechaWeb($datos['fechaSubida'])."</td>
						<td>".$datos['nombre']." ".$icono."</td>
						<td class='centro'>
							<div class='btn-group'>
								<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span> Acciones</button>
								<ul class='dropdown-menu' role='menu'>
								<li><a href='".$_CONFIG['raiz']."documentacion/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
			    				<li class='divider'></li>
								<li><a $target class='noAjax $clase' icono='".$datos['codigo']."' href='".$_CONFIG['raiz']."documentos/firmados/".$datos['ficheroFirmado']."'><i class='icon-cloud-download'></i> Descargar</a></li>
								</ul>
							</div>
					 	</td>";

			echo "
					 	<td>
							<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
		        		</td>
					</tr>";
	}
	
}

function listadoDocumentos($datos = false, $cliente = false) {

	if($datos != false){
		$formulario = recogerFormularioServicios($datos);

		conexionBD();
		$consulta = consultaBD('SELECT * FROM usuarios_lopd WHERE codigoTrabajo="'.$datos['codigo'].'";');
		$usuariosAccedenFichero = mysql_num_rows($consulta);
		$consulta = consultaBD('SELECT * FROM proveedores_lopd WHERE codigoTrabajo="'.$datos['codigo'].'";');
		$proveedores = mysql_num_rows($consulta);
		cierraBD();

		$res = array();
		$res['nombreDocumentos']  = array('FACTURA','INFORMACION CURRICULUM','CARTEL INFORMATIVO','CLIENTES INFORMACION CONSENTIMIENTO','CORREO ELECTRONICO','Documento de Seguridad','CARTEL VIDEOVIGILANCIA');
		$res['ficheroDocumentos'] = array('FACTURA','INFORMACION CURRICULUM','CARTEL INFORMATIVO','CLIENTES INFORMACION CONSENTIMIENTO','CORREO ELECTRONICO','documentoseguridad','CARTEL VIDEOVIGILANCIA');

		if($usuariosAccedenFichero > 0){
			array_push($res['nombreDocumentos'], "ANEXO IV: REGISTRO DE ACCESO A SOPORTES Y RECURSOS");
			array_push($res['ficheroDocumentos'], "ANEXO IV: REGISTRO DE ACCESO A SOPORTES Y RECURSOS");
			array_push($res['nombreDocumentos'], "ANEXO VIII: FUNCIONES Y OBLIGACIONES");
			array_push($res['ficheroDocumentos'], "ANEXO VIII: FUNCIONES Y OBLIGACIONES");
			array_push($res['nombreDocumentos'], "ANEXO III: REGISTRO DE USUARIOS");
			array_push($res['ficheroDocumentos'], "ANEXO III: REGISTRO DE USUARIOS");
		}
		if($proveedores > 0){
			array_push($res['nombreDocumentos'], "ANEXO III.2: PROVEEDORES SIN ACCESO A DATOS Y CON ACCESO A LAS DEPENDENCIAS");
			array_push($res['ficheroDocumentos'], "ANEXO III.2: PROVEEDORES SIN ACCESO A DATOS Y CON ACCESO A LAS DEPENDENCIAS");
		}

		if(isset($formulario['pregunta644']) && $formulario['pregunta644'] == 'SI' ) {
			array_push($res['nombreDocumentos'], "PÁGINA WEB: AVISO LEGAL", "PÁGINA WEB: POLÍTICA DE COOKIES", "PÁGINA WEB: POLÍTICA DE PRIVACIDAD");
			array_push($res['ficheroDocumentos'], "WEB_AVISO_LEGAL", "WEB_COOKIES", "WEB_PRIVACIDAD");
		}

		$consulta = consultaBD('SELECT * FROM delegaciones_lopd WHERE codigoTrabajo="'.$datos['codigo'].'";', true);
		$usuariosAccedenFichero=mysql_num_rows($consulta);
		if($formulario['pregunta425']=='SI' && $usuariosAccedenFichero>0){
			array_push($res['nombreDocumentos'], "ANEXO IX: AUTORIZACIONES DELEGADAS");
			array_push($res['ficheroDocumentos'], "ANEXO IX: AUTORIZACIONES DELEGADAS");
		}

		conexionBD();
		$consultaAux = consultaBD("SELECT * FROM incidencias_lopd WHERE codigoCliente='$cliente';");
		$incidencias = mysql_num_rows($consultaAux);
		cierraBD();	

		if($incidencias > 0){
			array_push($res['nombreDocumentos'], "ANEXO X: QUIEBRAS DE SEGURIDAD");
			array_push($res['ficheroDocumentos'], "ANEXO X: QUIEBRAS DE SEGURIDAD");
		}

		$encargados = consultaBD('SELECT COUNT(codigo) AS total FROM otros_encargados_lopd WHERE codigoTrabajo='.$datos['codigo'], true, true);
		if($formulario['pregunta158']=='SI' || $formulario['pregunta168']=='SI' || $formulario['pregunta178']=='SI' || $formulario['pregunta189']=='SI' || $formulario['pregunta200']=='SI' || $formulario['pregunta210']=='SI' || $formulario['pregunta220']=='SI' || $formulario['pregunta230']=='SI' || $formulario['pregunta240']=='SI' || $formulario['pregunta250']=='SI' || $formulario['pregunta260']=='SI' || $formulario['pregunta270']=='SI' || $formulario['pregunta280']=='SI' || $formulario['pregunta290']=='SI' || $formulario['pregunta487']=='SI' || $encargados['total']>0){

			$resultado=compruebaCheckSubcontratacion($formulario,$datos['codigo']);
			//echo $resultado['siSubcontrata'];
			//echo $resultado['noSubcontrata'];		

			if($resultado['siSubcontrata']>0){
				array_push($res['nombreDocumentos'], "CONTRATO PARA ACTUAR COMO ENCARGADO DEL TRATAMIENTO");
				array_push($res['ficheroDocumentos'], "CONTRATO PARA ACTUAR COMO ENCARGADO DEL TRATAMIENTO");
			}
			if($resultado['noSubcontrata']>0){
				array_push($res['nombreDocumentos'], "CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO");
				array_push($res['ficheroDocumentos'], "CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO");
			}
			if($resultado['subencargados']=='SI'){
				array_push($res['nombreDocumentos'], "CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO");
				array_push($res['ficheroDocumentos'], "CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO");
			}

		}

		conexionBD();
		$consulta1=consultaBD("SELECT * FROM entradas_no_periodicas WHERE codigoCliente='$cliente';");
		$entradasNoPeriodicas=mysql_num_rows($consulta1);
		cierraBD();

		if($entradasNoPeriodicas>0){
			array_push($res['nombreDocumentos'], "ANEXO VI: REGISTRO DE ENTRADAS Y SALIDAS NO PERIÓDICAS");
			array_push($res['ficheroDocumentos'], "ANEXO VI: REGISTRO DE ENTRADAS Y SALIDAS NO PERIÓDICAS");
		}

		conexionBD();
		$consulta2=consultaBD("SELECT * FROM entradas_periodicas WHERE codigoCliente='$cliente';");
		$entradasPeriodicas=mysql_num_rows($consulta2);
		cierraBD();

		if($entradasPeriodicas>0){
			array_push($res['nombreDocumentos'], "ANEXO V: REGISTRO DE ENTRADAS Y SALIDAS PERIÓDICAS");
			array_push($res['ficheroDocumentos'], "ANEXO V: REGISTRO DE ENTRADAS Y SALIDAS PERIÓDICAS");
		}

		conexionBD();
		$consulta3=consultaBD("SELECT * FROM soportes_lopd_nueva WHERE codigoCliente='$cliente';");
		$soportes=mysql_num_rows($consulta3);
		cierraBD();

		if($soportes>0){
			array_push($res['nombreDocumentos'], "ANEXO II: REGISTRO DE SOPORTES");
			array_push($res['ficheroDocumentos'], "ANEXO II: REGISTRO DE SOPORTES");
		}

		conexionBD();
		$consulta4=consultaBD("SELECT * FROM soportes_cancelados WHERE codigoCliente='$cliente';");
		$soportesCancelados=mysql_num_rows($consulta4);
		cierraBD();

		if($soportesCancelados>0){
			array_push($res['nombreDocumentos'], "ANEXO VII: REGISTRO DE SOPORTES BLOQUEADOS");
			array_push($res['ficheroDocumentos'], "ANEXO VII: REGISTRO DE SOPORTES BLOQUEADOS");
		}

		conexionBD();
		$consulta5=consultaBD("SELECT * FROM declaraciones WHERE codigoCliente='$cliente';");
		$declaraciones=mysql_num_rows($consulta5);
		cierraBD();

		if($declaraciones>0){
			array_push($res['nombreDocumentos'], "ANEXO I REGISTRO DE ACTIVIDADES DE TRATAMIENTO");
			array_push($res['ficheroDocumentos'], "ANEXO I REGISTRO DE ACTIVIDADES DE TRATAMIENTO");
		}

		/*if($datos['referencia']=='PBC1'){
			array_push($res['nombreDocumentos'], 'MANUAL DE PBC');
			array_push($res['ficheroDocumentos'],'zona-cliente/generaDocumento.php?ref=pbc&codigo=');
		}*/

		if(!isset($formulario['pregunta604']) || $formulario['pregunta604']==''){
			array_push($res['nombreDocumentos'], "ANEXO FINAL: DESIGNACIÓN DE RESPONSABLE DE SEGURIDAD");
			array_push($res['ficheroDocumentos'], "ANEXO FINAL PRIMERO");
		} else {
			array_push($res['nombreDocumentos'], "ANEXO FINAL: DESIGNACIÓN DE DELEGADO DE PROTECCIÓN DE DATOS");
			array_push($res['ficheroDocumentos'], "ANEXO FINAL SEGUNDO");
		}

		$valores=array('Acceso'=>'DERECHOS DE ACCESO','Rectificación'=>'DERECHOS DE RECTIFICACIÓN','Supresión o cancelación'=>'DERECHOS DE SUPRESIÓN O CANCELACIÓN','Oposición y decisiones individuales automatizadas'=>'DERECHOS DE OPOSICIÓN Y DECISIONES INDIVIDUALES AUTOMATIZADAS','Limitación del tratamiento'=>'DERECHOS DE LIMITACIÓN DEL TRATAMIENTO','Portabilidad de los datos'=>'DERECHOS DE PORTABILIDAD DE LOS DATOS');
		foreach ($valores as $key => $value) {
			$derechos=consultaBD('SELECT COUNT(codigo) AS total FROM derechos WHERE tipo="'.$key.'" AND codigoCliente='.$datos['codigoCliente'],true,true);
			if($derechos['total']>0){
				array_push($res['nombreDocumentos'], $value);
				array_push($res['ficheroDocumentos'], $value);
			}
		}
		
	} else {

		$sql = "SELECT 
					t.codigo, 
					s.servicio, 
					t.formulario, 
					sf.referencia, 
					t.codigoCliente 
				FROM 
					trabajos t 
				LEFT JOIN servicios s ON 
					t.codigoServicio = s.codigo 
				LEFT JOIN servicios_familias sf ON 
					s.codigoFamilia = sf.codigo 
				WHERE 
					s.servicio LIKE '%LOPD%'
				AND codigoCliente = ".$cliente;
		
		$trabajos = consultaBD($sql, true);
		
		while($datos = mysql_fetch_assoc($trabajos)) {
			$formulario = recogerFormularioServicios($datos);
			conexionBD();
			$consulta=consultaBD('SELECT * FROM usuarios_lopd WHERE codigoTrabajo="'.$datos['codigo'].'";');
			$usuariosAccedenFichero=mysql_num_rows($consulta);
			$consulta=consultaBD('SELECT * FROM proveedores_lopd WHERE codigoTrabajo="'.$datos['codigo'].'";');
			$proveedores=mysql_num_rows($consulta);
			cierraBD();

			$res=array();
			$res['nombreDocumentos']  = array('FACTURA','INFORMACION CURRICULUM','CARTEL INFORMATIVO','CLIENTES INFORMACION CONSENTIMIENTO','CORREO ELECTRONICO','Documento de Seguridad','CARTEL VIDEOVIGILANCIA');
			$res['ficheroDocumentos'] = array('FACTURA','INFORMACION CURRICULUM','CARTEL INFORMATIVO','CLIENTES INFORMACION CONSENTIMIENTO','CORREO ELECTRONICO','documentoseguridad','CARTEL VIDEOVIGILANCIA');

			if($usuariosAccedenFichero>0){
				array_push($res['nombreDocumentos'], "ANEXO IV: REGISTRO DE ACCESO A SOPORTES Y RECURSOS");
				array_push($res['ficheroDocumentos'], "ANEXO IV: REGISTRO DE ACCESO A SOPORTES Y RECURSOS");
				array_push($res['nombreDocumentos'], "ANEXO VIII: FUNCIONES Y OBLIGACIONES");
				array_push($res['ficheroDocumentos'], "ANEXO VIII: FUNCIONES Y OBLIGACIONES");
				array_push($res['nombreDocumentos'], "ANEXO III: REGISTRO DE USUARIOS");
				array_push($res['ficheroDocumentos'], "ANEXO III: REGISTRO DE USUARIOS");
			}
			if($proveedores>0){
				array_push($res['nombreDocumentos'], "ANEXO III.2: PROVEEDORES SIN ACCESO A DATOS Y CON ACCESO A LAS DEPENDENCIAS");
				array_push($res['ficheroDocumentos'], "ANEXO III.2: PROVEEDORES SIN ACCESO A DATOS Y CON ACCESO A LAS DEPENDENCIAS");
			}

			if(isset($formulario['pregunta644']) && $formulario['pregunta644'] == 'SI' ) {
				array_push($res['nombreDocumentos'], "PÁGINA WEB: AVISO LEGAL", "PÁGINA WEB: POLÍTICA DE COOKIES", "PÁGINA WEB: POLÍTICA DE PRIVACIDAD");
				array_push($res['ficheroDocumentos'], "WEB_AVISO_LEGAL", "WEB_COOKIES", "WEB_PRIVACIDAD");
			}

			$consulta=consultaBD('SELECT * FROM delegaciones_lopd WHERE codigoTrabajo="'.$datos['codigo'].'";',true);
			$usuariosAccedenFichero=mysql_num_rows($consulta);
			if($formulario['pregunta425']=='SI' && $usuariosAccedenFichero>0){
				array_push($res['nombreDocumentos'], "ANEXO IX: AUTORIZACIONES DELEGADAS");
				array_push($res['ficheroDocumentos'], "ANEXO IX: AUTORIZACIONES DELEGADAS");
			}


			conexionBD();
			$consultaAux=consultaBD("SELECT * FROM incidencias_lopd WHERE codigoCliente='$cliente';");
			$incidencias=mysql_num_rows($consultaAux);
			cierraBD();	

			if($incidencias>0){
				array_push($res['nombreDocumentos'], "QUIEBRAS DE SEGURIDAD");
				array_push($res['ficheroDocumentos'], "QUIEBRAS DE SEGURIDAD");
			}

			$encargados=consultaBD('SELECT COUNT(codigo) AS total FROM otros_encargados_lopd WHERE codigoTrabajo='.$datos['codigo'],true,true);
			if($formulario['pregunta158']=='SI' || $formulario['pregunta168']=='SI' || $formulario['pregunta178']=='SI' || $formulario['pregunta189']=='SI' || $formulario['pregunta200']=='SI' || $formulario['pregunta210']=='SI' || $formulario['pregunta220']=='SI' || $formulario['pregunta230']=='SI' || $formulario['pregunta240']=='SI' || $formulario['pregunta250']=='SI' || $formulario['pregunta260']=='SI' || $formulario['pregunta270']=='SI' || $formulario['pregunta280']=='SI' || $formulario['pregunta290']=='SI' || $formulario['pregunta487']=='SI' || $encargados['total']>0){

				$resultado=compruebaCheckSubcontratacion($formulario,$datos['codigo']);
				//echo $resultado['siSubcontrata'];
				//echo $resultado['noSubcontrata'];		

				if($resultado['siSubcontrata']>0){
					array_push($res['nombreDocumentos'], "CONTRATO PARA ACTUAR COMO ENCARGADO DEL TRATAMIENTO");
					array_push($res['ficheroDocumentos'], "CONTRATO PARA ACTUAR COMO ENCARGADO DEL TRATAMIENTO");
				}
				if($resultado['noSubcontrata']>0){
					array_push($res['nombreDocumentos'], "CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO");
					array_push($res['ficheroDocumentos'], "CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO");
				}
				if($resultado['subencargados']=='SI'){
					array_push($res['nombreDocumentos'], "CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO");
					array_push($res['ficheroDocumentos'], "CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO");
				}

			}

			conexionBD();
			$consulta1=consultaBD("SELECT * FROM entradas_no_periodicas WHERE codigoCliente='$cliente';");
			$entradasNoPeriodicas=mysql_num_rows($consulta1);
			cierraBD();

			if($entradasNoPeriodicas>0){
				array_push($res['nombreDocumentos'], "ANEXO VI: REGISTRO DE ENTRADAS Y SALIDAS NO PERIÓDICAS");
				array_push($res['ficheroDocumentos'], "ANEXO VI: REGISTRO DE ENTRADAS Y SALIDAS NO PERIÓDICAS");
			}

			conexionBD();
			$consulta2=consultaBD("SELECT * FROM entradas_periodicas WHERE codigoCliente='$cliente';");
			$entradasPeriodicas=mysql_num_rows($consulta2);
			cierraBD();

			if($entradasPeriodicas>0){
				array_push($res['nombreDocumentos'], "ANEXO V: REGISTRO DE ENTRADAS Y SALIDAS PERIÓDICAS");
				array_push($res['ficheroDocumentos'], "ANEXO V: REGISTRO DE ENTRADAS Y SALIDAS PERIÓDICAS");
			}

			conexionBD();
			$consulta3=consultaBD("SELECT * FROM soportes_lopd_nueva WHERE codigoCliente='$cliente';");
			$soportes=mysql_num_rows($consulta3);
			cierraBD();

			if($soportes>0){
				array_push($res['nombreDocumentos'], "ANEXO II: REGISTRO DE SOPORTES");
				array_push($res['ficheroDocumentos'], "ANEXO II: REGISTRO DE SOPORTES");
			}

			conexionBD();
			$consulta4=consultaBD("SELECT * FROM soportes_cancelados WHERE codigoCliente='$cliente';");
			$soportesCancelados=mysql_num_rows($consulta4);
			cierraBD();

			if($soportesCancelados>0){
				array_push($res['nombreDocumentos'], "ANEXO VII: REGISTRO DE SOPORTES BLOQUEADOS");
				array_push($res['ficheroDocumentos'], "ANEXO VII: REGISTRO DE SOPORTES BLOQUEADOS");
			}

			conexionBD();
			$consulta5=consultaBD("SELECT * FROM declaraciones WHERE codigoCliente='$cliente';");
			$declaraciones=mysql_num_rows($consulta5);
			cierraBD();

			if($declaraciones>0){
				array_push($res['nombreDocumentos'], "ANEXO I REGISTRO DE ACTIVIDADES DE TRATAMIENTO");
				array_push($res['ficheroDocumentos'], "ANEXO I REGISTRO DE ACTIVIDADES DE TRATAMIENTO");
			}

			/*if($datos['referencia']=='PBC1'){
				array_push($res['nombreDocumentos'], 'MANUAL DE PBC');
				array_push($res['ficheroDocumentos'],'zona-cliente/generaDocumento.php?ref=pbc&codigo=');
			}*/

			if(!isset($formulario['pregunta604']) || $formulario['pregunta604']==''){
				array_push($res['nombreDocumentos'], "ANEXO FINAL: DESIGNACIÓN DE RESPONSABLE DE SEGURIDAD");
				array_push($res['ficheroDocumentos'], "ANEXO FINAL PRIMERO");
			} else {
				array_push($res['nombreDocumentos'], "ANEXO FINAL: DESIGNACIÓN DE DELEGADO DE PROTECCIÓN DE DATOS");
				array_push($res['ficheroDocumentos'], "ANEXO FINAL SEGUNDO");
			}

			$valores=array('Acceso'=>'DERECHOS DE ACCESO','Rectificación'=>'DERECHOS DE RECTIFICACIÓN','Supresión o cancelación'=>'DERECHOS DE SUPRESIÓN O CANCELACIÓN','Oposición y decisiones individuales automatizadas'=>'DERECHOS DE OPOSICIÓN Y DECISIONES INDIVIDUALES AUTOMATIZADAS','Limitación del tratamiento'=>'DERECHOS DE LIMITACIÓN DEL TRATAMIENTO','Portabilidad de los datos'=>'DERECHOS DE PORTABILIDAD DE LOS DATOS');
			foreach ($valores as $key => $value) {
				$derechos=consultaBD('SELECT COUNT(codigo) AS total FROM derechos WHERE tipo="'.$key.'" AND codigoCliente='.$datos['codigoCliente'],true,true);
				if($derechos['total']>0){
					array_push($res['nombreDocumentos'], $value);
					array_push($res['ficheroDocumentos'], $value);
				}
			}
		}
	}	


	return $res;
}

function generaDocumento($codigo, $tipoDocumento){
	require_once '../../api/phpword/PHPWord.php';

    $PHPWord=new PHPWord();
	
	conexionBD();
	if($tipoDocumento=="clausulasCorreos"){
		clausulasCorreos($codigo, $PHPWord, 'clausulas_correos.docx');
	}
	if($tipoDocumento=="FACTURA"){
		clausulasFacturas($codigo, $PHPWord, 'facturas.docx');
	}
	if($tipoDocumento=="ANEXO IV: REGISTRO DE ACCESO A SOPORTES Y RECURSOS"){
		anexoIVLOPD($codigo, $PHPWord, 'anexoIV.docx');
	}
	if($tipoDocumento=="ANEXO IX: AUTORIZACIONES DELEGADAS"){
		//anexoIXLOPD($codigo, $PHPWord, 'AnexoIX.docx');
		//anexoIXLOPDIND(150, $PHPWord, 'AnexoIX_ind.docx');
		zipAnexoIXLOPD($codigo, $PHPWord);
	}
	if($tipoDocumento=="ANEXO VIII: FUNCIONES Y OBLIGACIONES"){
		zipAnexoVIIILOPD($codigo, $PHPWord, 'anexoVIII.docx');
	}
	if($tipoDocumento=="ANEXO X: QUIEBRAS DE SEGURIDAD"){
		anexoXLOPD($codigo, $PHPWord, 'anexoX.docx');
	}
	if($tipoDocumento=="CONTRATO PARA ACTUAR COMO ENCARGADO DEL TRATAMIENTO"){
		zipAnexoContratoEncargadoTratamientoLOPD($codigo, $PHPWord, 'anexoContratoEncargadoTratamiento.docx');
	}
	if($tipoDocumento=="ANEXO III: REGISTRO DE USUARIOS"){
		anexoIIILOPD($codigo, $PHPWord, 'anexoIII.docx');
	}
	if($tipoDocumento=="ANEXO III.2: PROVEEDORES SIN ACCESO A DATOS Y CON ACCESO A LAS DEPENDENCIAS"){
		anexoIII2LOPD($codigo, $PHPWord, 'anexoIII2.docx');
	}
	if($tipoDocumento=="ANEXO VI: REGISTRO DE ENTRADAS Y SALIDAS NO PERIÓDICAS"){
		anexoVILOPD($codigo, $PHPWord, 'anexoVI.docx');
	}
	if($tipoDocumento=="ANEXO V: REGISTRO DE ENTRADAS Y SALIDAS PERIÓDICAS"){
		anexoVLOPD($codigo, $PHPWord, 'anexoV.docx', false);
	}
	if($tipoDocumento=="ANEXO II: REGISTRO DE SOPORTES"){
		anexoIILOPD($codigo, $PHPWord, 'anexoII.docx');
	}
	if($tipoDocumento=="ANEXO VII: REGISTRO DE SOPORTES BLOQUEADOS"){
		anexoVIILOPD($codigo, $PHPWord, 'anexoVII.docx');
	}
	if($tipoDocumento=="INFORMACION CURRICULUM"){
		anexoInformacionCurriculum($codigo, $PHPWord, 'InformacionCurriculum.docx');
	}
	if($tipoDocumento=="CARTEL INFORMATIVO"){
		anexoCartelInformativo($codigo, $PHPWord, 'cartelInformativo.docx');
	}
	if($tipoDocumento=="CLIENTES INFORMACION CONSENTIMIENTO"){
		anexoClientesInformacionConsentimiento($codigo, $PHPWord, 'clienteslInformacionConsentimiento.docx');
	}
	if($tipoDocumento=="CORREO ELECTRONICO"){
		anexoCorreoElectronico($codigo, $PHPWord, 'correoElectronico.docx');
	}
	if($tipoDocumento=="ANEXO I REGISTRO DE ACTIVIDADES DE TRATAMIENTO"){
		zipAnexoI($codigo, $PHPWord, 'anexoI.docx');
	}
	if($tipoDocumento=="documentoseguridad"){
		generaDocumentoDeSeguridad($codigo, $PHPWord, 'ds.docx');
	}
	if($tipoDocumento=="CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO"){
		zipContratoSuscribirloEncargadoTratamientoLOPD($codigo, $PHPWord, 'anexoContratoEncargadoTratamiento.docx');
	}
	if($tipoDocumento=="CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO"){
		zipContratoSubencargadosLOPD($codigo, $PHPWord, 'subencargados.docx');
	}
	if($tipoDocumento=="ANEXO FINAL PRIMERO"){
		anexoFinal1LOPD($codigo, $PHPWord, 'anexoFinal1.docx');
	}	
	if($tipoDocumento=="ANEXO FINAL SEGUNDO"){
		anexoFinal2LOPD($codigo, $PHPWord, 'anexoFinal2.docx');
	} 

	if($tipoDocumento=="CARTEL VIDEOVIGILANCIA"){
		cartelVideovigilancia($codigo);
	}		

	if($tipoDocumento=="DERECHOS DE ACCESO"){
		derechosAcceso($codigo, $PHPWord, 'derechosAcceso.docx',1);
	} 

	if($tipoDocumento=="DERECHOS DE RECTIFICACIÓN"){
		derechosAcceso($codigo, $PHPWord, 'derechosAcceso.docx',2);
	} 

	if($tipoDocumento=="DERECHOS DE SUPRESIÓN O CANCELACIÓN"){
		derechosAcceso($codigo, $PHPWord, 'derechosAcceso.docx',3);
	} 

	if($tipoDocumento=="DERECHOS DE OPOSICIÓN Y DECISIONES INDIVIDUALES AUTOMATIZADAS"){
		derechosAcceso($codigo, $PHPWord, 'derechosAcceso.docx',4);
	} 

	if($tipoDocumento=="DERECHOS DE LIMITACIÓN DEL TRATAMIENTO"){
		derechosAcceso($codigo, $PHPWord, 'derechosAcceso.docx',5);
	} 

	if($tipoDocumento=="DERECHOS DE PORTABILIDAD DE LOS DATOS"){
		derechosAcceso($codigo, $PHPWord, 'derechosAcceso.docx',6);
	} 

	if ($tipoDocumento == "WEB_AVISO_LEGAL" ) {
		documentoAvisoLegalWeb($codigo);
	}

	if ($tipoDocumento == "WEB_COOKIES" ) {
		documentoCookiesWeb($codigo);
	}

	if ($tipoDocumento == "WEB_PRIVACIDAD" ) {
		documentoPrivacidadWeb($codigo);
	}

	cierraBD();
}

function documentoAvisoLegalWeb($codigo) {

	$sql = "SELECT 
				c.razonSocial, 
				c.domicilio, 
				c.cp, 
				c.localidad, 
				c.provincia, 
				sf.referencia AS familia, 
				t.formulario, 
				c.tomo,
				c.libro,
				c.folio,
				c.seccion,
				c.hoja,
				c.ficheroLogo
			FROM 
				trabajos t
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo 
			WHERE 
				t.codigo = ".$codigo;
	
	$datos      = consultaBD($sql, true, true);
	$formulario = recogerFormularioServicios($datos);
	$datos      = datosPersonales($datos, $formulario);
	$fichero    = 'WEB_AvisoLegal.docx';
	$PHPWord    = new PHPWord();
	$documento  = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_avisoLegal.docx');	

	reemplazarLogo($documento,$datos);
	
	if (isset($formulario['pregunta604']) && $formulario['pregunta604'] != '') {
		$delegado = '<w:p w:rsidR="003F0D60" w:rsidRPr="0037124A" w:rsidRDefault="003F0D60" w:rsidP="0053700A"><w:pPr><w:pStyle w:val="Sinespaciado"/><w:jc w:val="both"/><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="0037124A"><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">Delegado de Protección de Datos: '.$formulario['pregunta604'].'.</w:t></w:r></w:p><w:p w:rsidR="003F0D60" w:rsidRPr="0037124A" w:rsidRDefault="003F0D60" w:rsidP="003F0D60"><w:pPr><w:pStyle w:val="Sinespaciado"/><w:jc w:val="both"/><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="0037124A"><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">E-mail/dirección: '.$formulario['pregunta607'].' / '.$formulario['pregunta626'].'.</w:t></w:r></w:p>';
	} else {
		$delegado = '';
	}

	$documento->setValue("responsable", utf8_decode(sanearCaracteres($formulario['pregunta3'])));
	$documento->setValue("nifResponsable", utf8_decode($formulario['pregunta9']));	
	$direccion = $formulario['pregunta4'].' CP: '.$formulario['pregunta10'].', '.$formulario['pregunta5'].' ('.$formulario['pregunta11'].')';
	$documento->setValue("direccionResponsable", utf8_decode($direccion));
	$documento->setValue("telefonoResponsable", utf8_decode($formulario['pregunta6']));
	$documento->setValue("emailResponsable", utf8_decode($formulario['pregunta7']));	
	$registroMercantil = "Tomo ".$datos['tomo']." Libro ".$datos['libro']." Folio ".$datos['folio']." Sección ".$datos['seccion']." Hoja ".$datos['hoja'];
	$documento->setValue("registroMercantil", utf8_decode($registroMercantil));
	$documento->setValue("titulacion", utf8_decode($formulario['pregunta13']));
	$documento->setValue("delegado", utf8_decode($delegado));

	$documento->save('../documentos/consultorias/'.$fichero);

	header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header("Content-Disposition: attachment; filename=".$fichero);
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function documentoCookiesWeb($codigo) {

	$sql = "SELECT 
				c.razonSocial, 
				c.domicilio, 
				c.cp, 
				c.localidad, 
				c.provincia, 
				sf.referencia AS familia, 
				t.formulario, 
				c.tomo,
				c.libro,
				c.folio,
				c.seccion,
				c.hoja,
				c.ficheroLogo
			FROM 
				trabajos t
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo 
			WHERE 
				t.codigo = ".$codigo;
	
	$datos      = consultaBD($sql, true, true);
	$formulario = recogerFormularioServicios($datos);
	$datos      = datosPersonales($datos, $formulario);
	$fichero    = 'WEB_Cookies.docx';
	$PHPWord    = new PHPWord();
	$documento  = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_WEB_Cookies.docx');	

	reemplazarLogo($documento,$datos);

	$documento->setValue("responsable", utf8_decode(sanearCaracteres($formulario['pregunta3'])));

	$documento->save('../documentos/consultorias/'.$fichero);

	header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header("Content-Disposition: attachment; filename=".$fichero);
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function documentoPrivacidadWeb($codigo) {
	$sql = "SELECT 
				c.razonSocial, 
				c.domicilio, 
				c.cp, 
				c.localidad, 
				c.provincia, 
				sf.referencia AS familia, 
				t.formulario, 
				c.tomo,
				c.libro,
				c.folio,
				c.seccion,
				c.hoja,
				c.ficheroLogo
			FROM 
				trabajos t
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo 
			WHERE 
				t.codigo = ".$codigo;
	
	$datos      = consultaBD($sql, true, true);
	$formulario = recogerFormularioServicios($datos);
	$datos      = datosPersonales($datos, $formulario);
	$fichero    = 'WEB_PoliticaPrivacidad.docx';
	$PHPWord    = new PHPWord();
	$documento  = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_PoliticaPrivacidad.docx');	

	reemplazarLogo($documento,$datos);
	
	if (isset($formulario['pregunta604']) && $formulario['pregunta604'] != '') {
		$delegado = '<w:p w:rsidR="009E7A3E" w:rsidRPr="00FF2575" w:rsidRDefault="009E7A3E" w:rsidP="009E7A3E"><w:pPr><w:pStyle w:val="Sinespaciado"/><w:jc w:val="both"/><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="00FF2575"><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">Delegado de Protección de Datos: '.$formulario['pregunta604'].'. </w:t></w:r></w:p><w:p w:rsidR="009E7A3E" w:rsidRPr="00FF2575" w:rsidRDefault="009E7A3E" w:rsidP="008039DF"><w:pPr><w:pStyle w:val="Sinespaciado"/><w:jc w:val="both"/><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="00FF2575"><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">E-mail/dirección: '.$formulario['pregunta607'].' / '.$formulario['pregunta626'].'. </w:t></w:r></w:p>';
	} else {
		$delegado = '';
	}

	$documento->setValue("responsable", utf8_decode(sanearCaracteres($formulario['pregunta3'])));
	$documento->setValue("nifResponsable", utf8_decode($formulario['pregunta9']));	
	$direccion = $formulario['pregunta4'].' CP: '.$formulario['pregunta10'].', '.$formulario['pregunta5'].' ('.$formulario['pregunta11'].')';
	$documento->setValue("direccionResponsable", utf8_decode($direccion));
	$documento->setValue("telefonoResponsable", utf8_decode($formulario['pregunta6']));
	$documento->setValue("emailResponsable", utf8_decode($formulario['pregunta7']));	
	$registroMercantil = "Tomo ".$datos['tomo']." Libro ".$datos['libro']." Folio ".$datos['folio']." Sección ".$datos['seccion']." Hoja ".$datos['hoja'];
	$documento->setValue("registroMercantil", utf8_decode($registroMercantil));
	$documento->setValue("titulacion", "ppe");
	$documento->setValue("delegado", utf8_decode($delegado));

	$documento->save('../documentos/consultorias/'.$fichero);

	header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
	header("Content-Disposition: attachment; filename=".$fichero);
	header("Content-Transfer-Encoding: binary");

	readfile('../documentos/consultorias/'.$fichero);
}

function datosPersonales($datos, $formulario){

	if($datos['familia']=='LOPD1'){
		$datos['razonSocial'] = $formulario['pregunta3'];
		$datos['domicilio']   = $formulario['pregunta4'];
		$datos['cp']          = $formulario['pregunta10'];
		$datos['localidad']   = $formulario['pregunta5'];
		$datos['provincia']   = $formulario['pregunta11'];
		$datos['cif']         = $formulario['pregunta9'];
	}

	return $datos;
}

function clausulasCorreos($codigo, $PHPWord, $nombreFichero=''){
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("nombre",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("direccion",utf8_decode($datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')'));
	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$nombreFichero);
}

function clausulasFacturas($codigo, $PHPWord, $nombreFichero=''){
	$fichero='FACTURA.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, servicios_familias.referencia AS familia, formulario, ficheroLogo FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	if($formulario['pregunta605']!=''){
		$documento->setValue("datosDPD",utf8_decode($formulario['pregunta4'].' - CP: '.$formulario['pregunta10'].' '.$formulario['pregunta5'].' ('.$formulario['pregunta11'].')'));
		$documento->setValue("emailDPD",utf8_decode($formulario['pregunta7']));		
	}else{
		$documento->setValue("datosDPD",utf8_decode($formulario['pregunta4'].' - CP: '.$formulario['pregunta10'].' '.$formulario['pregunta5'].' ('.$formulario['pregunta11'].')'));
		$documento->setValue("emailDPD",utf8_decode($formulario['pregunta7']));		
	}
	
	reemplazarLogo($documento,$datos);
	
	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function reemplazarLogo($documento,$datos,$imagen='image1.png',$campo='ficheroLogo'){
	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos[$campo]);
    if($hayLogo!='NO' && $hayLogo!=''){
    	$logo = '../documentos/logos-clientes/'.$datos[$campo];
    	$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
		if (!copy($logo, $nuevo_logo)) {
    		echo "Error al copiar $fichero...\n";
		}	
		$documento->replaceImage('../documentos/logos-clientes/',$imagen);	
	}
}

function anexoIVLOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_IV_REGISTRO_DE_ACCESO_A_SOPORTES_Y_RECURSOS.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.ficheroLogo, clientes.localidad, clientes.provincia, servicios_familias.referencia AS familia, formulario, ficheroLogo FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",fecha());

	reemplazarLogo($documento,$datos);

	$tablaUsuarios=fechaTabla(date("d/m/Y"),'ACCESO A SOPORTES Y RECURSOS');
	$tablaUsuarios.=cabeceraAnexoIV();

	$consulta=consultaBD("SELECT * FROM usuarios_lopd WHERE codigoTrabajo='$codigo' AND fechaBajaUsuarioLOPD='0000-00-00';",true);
	$i=1;
	while($usuariosAccedenFichero=mysql_fetch_assoc($consulta)){
		$tablaUsuarios.=filaAnexoIV($codigo,array(sanearCaracteres($usuariosAccedenFichero['nombreUsuarioLOPD']),$usuariosAccedenFichero['accesoUsuarioLOPD'],$usuariosAccedenFichero['ficherosUsuarioLOPD'],$usuariosAccedenFichero['codigo']),$i);
		$i++;			
	}

	$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
	$campos=array(158=>array(159,589,574),168=>array(169,590,575),178=>array(179,591,576),189=>array(190,592,577),200=>array(201,593,578),210=>array(211,594,579),220=>array(221,595,609),230=>array(231,596,580),240=>array(241,597,581),250=>array(251,598,582),260=>array(261,599,583),270=>array(271,600,584),280=>array(281,601,585),290=>array(291,602,586),487=>array(488,603,587)
	);
	foreach ($encargados as $key => $value) {
		if($formulario['pregunta'.$value]=='SI'){
			$item=array(sanearCaracteres($formulario['pregunta'.$campos[$value][0]]),$formulario['pregunta'.$campos[$value][1]],$formulario['pregunta'.$campos[$value][2]],$value);
			$tablaUsuarios.=filaAnexoIV($codigo,$item,$i,'ENCARGADOFIJO');
			$i++;
		}
	}

	$otrosEncargados=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$codigo.' AND cifEncargado!="'.$formulario['pregunta9'].'";',true);
	while($item=mysql_fetch_assoc($otrosEncargados)){
		$tablaUsuarios.=filaAnexoIV($codigo,array(sanearCaracteres($item['nombreEncargado']),$item['accesosEncargado'],$item['ficheroEncargado'],$item['codigo']),$i,'ENCARGADO');
		$i++;
	}
	$tablaUsuarios.=pieAnexoIV($i);

	$i=$i-1;
	$documento->setValue("tabla",utf8_decode($tablaUsuarios));

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function cabeceraAnexoIV(){
	return '<w:tbl><w:tblPr><w:tblW w:w="5000" w:type="pct"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1139"/><w:gridCol w:w="3647"/><w:gridCol w:w="1701"/><w:gridCol w:w="3402"/><w:gridCol w:w="3506"/></w:tblGrid><w:tr w:rsidR="005E4AD2" w:rsidRPr="008F5EA5" w14:paraId="28E86B07" w14:textId="77777777" w:rsidTr="005E4AD2"><w:tc><w:tcPr><w:tcW w:w="1139" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="6EE0CC37" w14:textId="77777777" w:rsidR="005E4AD2" w:rsidRPr="008F5EA5" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3647" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="41074222" w14:textId="70E06FE9" w:rsidR="005E4AD2" w:rsidRPr="008F5EA5" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>USUARIO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="4E80D869" w14:textId="1B6B83E3" w:rsidR="005E4AD2" w:rsidRPr="008F5EA5" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>ACCESO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3402" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3FFB079D" w14:textId="753E202F" w:rsidR="005E4AD2" w:rsidRPr="008F5EA5" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FICHERO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3506" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="1C5FF937" w14:textId="20877B06" w:rsidR="005E4AD2" w:rsidRPr="008F5EA5" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc></w:tr>';

}
function filaAnexoIV($codigo,$item,$i,$tipoSoporte='USUARIOS'){
	$tipoAcceso=array('X'=>'Acceso total','W'=>'Creación<w:br/>Modificación<w:br/>Borrado','R'=>'Sólo lectura','S'=>'Sólo a las dependencias','B'=>'Sólo a datos básicos','N'=>'Ninguno');
	$tipoUsuario=array('USUARIOS'=>' (Usuario propio)','ENCARGADOFIJO'=>' (Encargado del tratamiento)','ENCARGADO'=>' (Encargado del tratamiento)');
	$ref=$i<10?'0'.$i:$i;
	$ficherosAcce=explode('&$&', $item[2]);
	$consultaAux=consultaBD("SELECT * FROM soportes_automatizados_lopd WHERE codigoTrabajo='$codigo' AND nombreAutomatizados='".$tipoSoporte."_".$item[3]."';",true);
	$datosAux=mysql_fetch_assoc($consultaAux);
	$soporteF=explode('&$&', $datosAux['soportesAutomatizados']);
	$res='<w:tr w:rsidR="005E4AD2" w14:paraId="541C930D" w14:textId="77777777" w:rsidTr="005E4AD2"><w:tc><w:tcPr><w:tcW w:w="1139" w:type="dxa"/></w:tcPr><w:p w14:paraId="775EC049" w14:textId="77777777" w:rsidR="005E4AD2" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3647" w:type="dxa"/></w:tcPr><w:p w14:paraId="09039B12" w14:textId="36DB34DD" w:rsidR="005E4AD2" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$item[0].$tipoUsuario[$tipoSoporte].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/></w:tcPr><w:p w14:paraId="6A60C067" w14:textId="11376569" w:rsidR="005E4AD2" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipoAcceso[$item[1]].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3402" w:type="dxa"/></w:tcPr>';
	for($j=0;$j<count($ficherosAcce);$j++){
		$ficheroNombre=datosRegistro('declaraciones',$ficherosAcce[$j],'codigo');
		$res.='<w:p w14:paraId="1CC21878" w14:textId="36B3512E" w:rsidR="005E4AD2" w:rsidRPr="002B0C55" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>- '.$ficheroNombre['nombreFichero'].'</w:t></w:r></w:p>';
	}
	

	$res.='</w:tc><w:tc><w:tcPr><w:tcW w:w="3506" w:type="dxa"/></w:tcPr>';
	for($k=0;$k<count($soporteF);$k++){
		$soporteNombre=datosRegistro('soportes_lopd_nueva',$soporteF[$k],'codigo');
		$res.='<w:p w14:paraId="5D318065" w14:textId="49D59CE0" w:rsidR="005E4AD2" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>-'.sanearCaracteres($soporteNombre['nombreSoporteLOPD']).'</w:t></w:r></w:p>';
	}

	$res.='<w:p w14:paraId="4F178380" w14:textId="77777777" w:rsidR="005E4AD2" w:rsidRDefault="005E4AD2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

	return $res;
}

function pieAnexoIV($total){
	$total=$total-1;
	return '</w:tbl>'.pieTabla($total);
}

function recogeNombreFicheros($aux){
    $nombresF=split('&$&', $aux['ficherosUsuarioLOPD']);

    return $nombresF;
    
}
function anexoIXLOPD($codigo, $PHPWord, $i, $nombreFichero = ''){
	$fichero = 'ANEXO_IX_AUTORIZACIONES_DELEGADAS_'.$i.'.docx';

	$valores = array(
		'',
		'Actualizar y difundir los registros y documentos preceptivos para el cumplimiento de la normativa en vigor en materia de Protección de Datos de las Personas Físicas.',
		'Ejercer el deber de información',
		'Solicitar peticiones de consentimiento',
		'Difundir entre el personal las normas referentes a Protección de Datos de Carácter Personal que les afectan, así como las consecuencias por su incumplimiento.',
		'Concesión de permisos de acceso a datos  al personal propio',
		'Asignar las claves de acceso a cada usuario',
		'Revisar que se cambien las claves de usuario y contraseñas anualmente',
		'Contratar los servicios de los encargados del tratamiento y firmar el correspondiente contrato de encargado del tratamiento',
		'Autorizar la ejecución de trabajos fuera de los locales del responsable o del encargado del tratamiento',
		'Realizar una auditoría cada dos años o ante modificaciones sustanciales en los sistemas de información con repercusiones en seguridad o encargarla a un tercero, propio o ajeno',
		'Autorizar  las entradas y salidas de soportes y datos, incluidas las que se realizan a través de redes de telecomunicaciones',
		'Registrar las entradas y salidas de soportes y datos',
		'Registrar las entradas y salidas de datos por red',
		'Revisar semestralmente los procedimientos de generación de copias de respaldo y recuperación de datos',
		'Analizar trimestralmente las incidencias y poner medidas correctoras',
		'Registrar las incidencias que se produzcan (tipo, momento de su detección, persona que la notifica, efectos y medidas correctoras adoptadas) y notificarlas a quién corresponda',
		'Autorizar la ejecución del procedimiento de recuperación de datos',
		'Registrar los procedimientos de recuperación, persona que lo ejecuta, datos restaurados y, en su caso, datos grabados manualmente',
		'Designar  Responsables de Seguridad',
		'Designar al Delegado de Protección de Datos',
		'Delegar responsabilidades propias de su cargo así como cualquiera de las funciones y obligaciones que le correspondan por  delegación en otros miembros de la plantilla',
		'Otras'
	);

	$sql = "SELECT 
				c.*, 
				sf.referencia AS familia, 
				t.formulario
			FROM 
				trabajos t 
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo 
			WHERE 
				t.codigo = ".$codigo;

	$datos      = consultaBD($sql, false, true);
	$formulario = recogerFormularioServicios($datos);
	$datos      = datosPersonales($datos,$formulario);
	
	$documento = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",fecha());
	
	reemplazarLogo($documento,$datos,'image3.png');

	$tabla  = fechaTabla(date("d/m/Y"), 'AUTORIZACIONES DELEGADAS');
	$tabla .= '<w:tbl><w:tblPr><w:tblW w:w="8613" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="702"/><w:gridCol w:w="1958"/><w:gridCol w:w="2212"/><w:gridCol w:w="1298"/><w:gridCol w:w="2443"/></w:tblGrid><w:tr w:rsidR="0088254E" w:rsidRPr="008F5EA5" w14:paraId="2C498856" w14:textId="77777777" w:rsidTr="00E16BE0"><w:tc><w:tcPr><w:tcW w:w="702" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="75431AAF" w14:textId="1875C639" w:rsidR="0088254E" w:rsidRPr="008F5EA5" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF</w:t></w:r><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1958" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2A63A81B" w14:textId="01100CE1" w:rsidR="0088254E" w:rsidRPr="008F5EA5" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>USUARIO QUE DELEGA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2212" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="4C34E974" w14:textId="7D1A7013" w:rsidR="0088254E" w:rsidRPr="008F5EA5" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>USUARIO EN QUIEN DELEGA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1298" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="36C57E99" w14:textId="444AE695" w:rsidR="0088254E" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA INICIO DELEGACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2443" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="24ECD239" w14:textId="4F9F5356" w:rsidR="0088254E" w:rsidRPr="008F5EA5" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA FINALIZACIÓN DELEGACIÓN</w:t></w:r></w:p></w:tc></w:tr>';
	
	$i = 1;

	$sql = "SELECT 
				u1.nombreUsuarioLOPD AS usuario1, 
				u2.nombreUsuarioLOPD AS usuario2, 
				d.*
			FROM 
				delegaciones_lopd d 
			LEFT JOIN usuarios_lopd u1 ON 
				d.usuario1Delegaciones = u1.codigo 
			LEFT JOIN usuarios_lopd u2 ON 
				d.usuario2Delegaciones = u2.codigo 
			WHERE 
				d.codigoTrabajo = ".$codigo.";";

	$delegaciones = consultaBD($sql);
	
	while ($delegacion = mysql_fetch_assoc($delegaciones)) {
		
		$ref        = str_pad($i, 2, '0', STR_PAD_LEFT);
		$facultades = '';

		/* Se comenta el 17/06/2024 por que no se usa
		$listado    = explode('&$&', $delegacion['facultades']);
		
		foreach ($listado as $key => $value) {
			$facultades .= $value == 22 ? '- '.sanearCaracteres($delegacion['otras']).'<w:br/>' : '- '.$valores[$value].'<w:br/>';
		}
		*/
		
		$tabla .= '<w:tr w:rsidR="0088254E" w14:paraId="6D301254" w14:textId="77777777" w:rsidTr="00E16BE0"><w:tc><w:tcPr><w:tcW w:w="702" w:type="dxa"/></w:tcPr><w:p w14:paraId="1C00047D" w14:textId="77777777" w:rsidR="0088254E" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1958" w:type="dxa"/></w:tcPr><w:p w14:paraId="47B9008F" w14:textId="0126DC36" w:rsidR="0088254E" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$delegacion['usuario1'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2212" w:type="dxa"/></w:tcPr><w:p w14:paraId="1F573CED" w14:textId="60695767" w:rsidR="0088254E" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$delegacion['usuario2'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1298" w:type="dxa"/></w:tcPr><w:p w14:paraId="1818413C" w14:textId="32ED7830" w:rsidR="0088254E" w:rsidRDefault="00E16BE0" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWord($delegacion['fechaDelegaciones']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2443" w:type="dxa"/></w:tcPr><w:p w14:paraId="70C914CE" w14:textId="51968FD2" w:rsidR="0088254E" w:rsidRPr="002B0C55" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWord($delegacion['fechaFinDelegaciones']).'</w:t></w:r></w:p></w:tc></w:tr>';
		
		$i++;
	}

	$total  = $i - 1;
	$tabla .= '</w:tbl>'.pieTabla($total);
	
	$documento->setValue("tabla",utf8_decode($tabla));

	/*$listado2='';
	$delegaciones=consultaBD("SELECT u1.nombreUsuarioLOPD AS usuario1, u2.nombreUsuarioLOPD AS usuario2, fechaDelegaciones, fechaFinDelegaciones, facultades FROM delegaciones_lopd d LEFT JOIN usuarios_lopd u1 ON d.usuario1Delegaciones=u1.codigo LEFT JOIN usuarios_lopd u2 ON d.usuario2Delegaciones=u2.codigo WHERE d.codigoTrabajo='$codigo';",true);
	$i=1;
	while($delegacion=mysql_fetch_assoc($delegaciones)){
		$ref=str_pad($i,4,'0',STR_PAD_LEFT);
		$listado2.='<w:p w14:paraId="51B14901" w14:textId="77777777" w:rsidR="00BD58B5" w:rsidRPr="006962F9" w:rsidRDefault="00BD58B5" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="194A7E9C" w14:textId="77777777" w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB" w:rsidRDefault="00C22433" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">REF. </w:t></w:r><w:r w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:u w:val="single"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p>';
		$listado2.='<w:p w14:paraId="59E7F8F1" w14:textId="77777777" w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB" w:rsidRDefault="00C10AEF" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="33462053" w14:textId="7F4ACA3D" w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB" w:rsidRDefault="000964E9" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>USUARIO QUE DELEGA: '.$delegacion['usuario1'].'</w:t></w:r><w:r w:rsidR="002E7E5A" w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p>';
		$listado2.='<w:p w14:paraId="2452E3C6" w14:textId="77777777" w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB" w:rsidRDefault="00C10AEF" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="1AF8F1AF" w14:textId="77777777" w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB" w:rsidRDefault="000964E9" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>DELEGADO: '.$delegacion['usuario2'].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p>';
		$listado2.='<w:p w14:paraId="5B5C7661" w14:textId="77777777" w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB" w:rsidRDefault="00C10AEF" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="05363CBC" w14:textId="77777777" w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB" w:rsidRDefault="00C10AEF" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>FECHA</w:t></w:r><w:r w:rsidR="000964E9" w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve"> INICIO DELEGACIÓN </w:t></w:r><w:r w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWord($delegacion['fechaDelegaciones']).'</w:t></w:r></w:p>';
		$listado2.='<w:p w14:paraId="0CC4B571" w14:textId="77777777" w:rsidR="000964E9" w:rsidRPr="00DB3BCB" w:rsidRDefault="000964E9" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="71716BEA" w14:textId="77777777" w:rsidR="000964E9" w:rsidRPr="00DB3BCB" w:rsidRDefault="000964E9" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>FECHA FINALIZACIÓN DELEGACIÓN</w:t></w:r><w:r w:rsidR="00E35C10" w:rsidRPr="00DB3BCB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> '.formateaFechaWord($delegacion['fechaFinDelegaciones']).'</w:t></w:r></w:p>';
		$listado2.='<w:p w14:paraId="2745F53B" w14:textId="77777777" w:rsidR="00C10AEF" w:rsidRPr="00DB3BCB" w:rsidRDefault="00C10AEF" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="41DDB0C6" w14:textId="77777777" w:rsidR="00C10AEF" w:rsidRDefault="0054135F" w:rsidP="00C10AEF"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>FACULTADES DELEGADAS</w:t></w:r><w:r w:rsidR="002D5705"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">: </w:t></w:r></w:p>';
		$facultades=explode('&$&', $delegacion['facultades']);
		foreach ($valores as $key => $value) {
			if($value!=''){
				if(in_array($key, $facultades)){
					$listado2.='<w:p w14:paraId="7C3CEE05" w14:textId="77777777" w:rsidR="001B5B2F" w:rsidRPr="001B5B2F" w:rsidRDefault="002D5705" w:rsidP="001B5B2F"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00E8093B"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/></w:rPr><w:lastRenderedPageBreak/><w:sym w:font="Wingdings 2" w:char="F054"/></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$value.'</w:t></w:r></w:p>';
				} else {
					$listado2.='<w:p w14:paraId="7C3CEE05" w14:textId="77777777" w:rsidR="001B5B2F" w:rsidRPr="001B5B2F" w:rsidRDefault="002D5705" w:rsidP="001B5B2F"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00E8093B"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/></w:rPr><w:lastRenderedPageBreak/><w:sym w:font="Wingdings" w:char="F0A8"/></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$value.'</w:t></w:r></w:p>';
				}
			}
		}
		$i++;
	}
	$documento->setValue("tablaDelegaciones",utf8_decode($listado2));*/
	$documento->save('../documentos/consultorias/'.$fichero);
	
	/*header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);*/

    return $fichero;
}

function anexoIXLOPDIND($codigo, $PHPWord,$i,$nombreFichero = '') {
	
	$fichero = 'ANEXO_IX_AUTORIZACIONES_DELEGADAS_'.$i.'.docx';
	
	$sql = "SELECT 
				c.*, 
				sf.referencia AS familia, 
				t.formulario, 
				u1.nombreUsuarioLOPD AS usuario1, 
				u1.nifUsuarioLOPD AS nif1, 
				u2.nombreUsuarioLOPD AS usuario2, 
				u2.nifUsuarioLOPD AS nif2, 
				d.*
			FROM 
				delegaciones_lopd d
			INNER JOIN trabajos t ON 
				d.codigoTrabajo = t.codigo 
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN usuarios_lopd u1 ON 
				d.usuario1Delegaciones = u1.codigo 
			INNER JOIN usuarios_lopd u2 ON 
				d.usuario2Delegaciones = u2.codigo 
			INNER JOIN servicios s ON  
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo
			WHERE 
				d.codigo = ".$codigo;
	
	$datos      = consultaBD($sql, false, true);
	$formulario = recogerFormularioServicios($datos);
	$datos      = datosPersonales($datos,$formulario);
	
	$documento = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	
	$documento->setValue("cliente", utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("localidad", utf8_decode(sanearCaracteres($datos['localidad'])));
	$documento->setValue("usuario1", utf8_decode(sanearCaracteres($datos['usuario1'])));
	$documento->setValue("usuario2", utf8_decode(sanearCaracteres($datos['usuario2'])));
	$documento->setValue("nif1", utf8_decode(sanearCaracteres($datos['nif1'])));
	$documento->setValue("nif2", utf8_decode(sanearCaracteres($datos['nif2'])));
	$documento->setValue("fechaInicio", utf8_decode(formateaFechaWeb($datos['fechaDelegaciones'])));
	
	if ($datos['fechaFinDelegaciones'] != '' && $datos['fechaFinDelegaciones'] != '0000-00-00') {
		$fechaFin = ' hasta el '.formateaFechaWeb($datos['fechaFinDelegaciones']);
	} 
	else {
		$fechaFin = '';
	}
	
	$documento->setValue("fechaFin",utf8_decode($fechaFin));
	
	reemplazarLogo($documento,$datos,'image3.png');

	$facultades = array(
		'',
		'Actualizar y difundir los registros y documentos preceptivos para el cumplimiento de la normativa en vigor en materia de Protección de Datos de las Personas Físicas.',
		'Ejercer el deber de información',
		'Solicitar peticiones de consentimiento',
		'Difundir entre el personal las normas referentes a Protección de Datos de Carácter Personal que les afectan, así como las consecuencias por su incumplimiento.',
		'Concesión de permisos de acceso a datos al personal propio',
		'Asignar las claves de acceso a cada usuario',
		'Revisar que se cambien las claves de usuario y contraseñas anualmente',
		'Contratar los servicios de los encargados del tratamiento y firmar el correspondiente contrato de encargado del tratamiento',
		'Autorizar la ejecución de trabajos fuera de los locales del responsable o del encargado del tratamiento',
		'Realizar una auditoría cada dos años o ante modificaciones sustanciales en los sistemas de información con repercusiones en seguridad o encargarla a un tercero, propio o ajeno',
		'Autorizar  las entradas y salidas de soportes y datos, incluidas las que se realizan a través de redes de telecomunicaciones',
		'Registrar las entradas y salidas de soportes y datos',
		'Registrar las entradas y salidas de datos por red',
		'Revisar semestralmente los procedimientos de generación de copias de respaldo y recuperación de datos',
		'Analizar trimestralmente las incidencias y poner medidas correctoras',
		'Registrar las incidencias que se produzcan (tipo, momento de su detección, persona que la notifica, efectos y medidas correctoras adoptadas) y notificarlas a quién corresponda',
		'Autorizar la ejecución del procedimiento de recuperación de datos',
		'Registrar los procedimientos de recuperación, persona que lo ejecuta, datos restaurados y, en su caso, datos grabados manualmente',
		'Designar  Responsables de Seguridad',
		'Designar al Delegado de Protección de Datos',
		'Delegar responsabilidades propias de su cargo así como cualquiera de las funciones y obligaciones que le correspondan por  delegación en otros miembros de la plantilla',
		'Otras'
	);
	
	
	$listado = explode('&$&', $datos['facultades']);
	$i = 1;
	$texto = '';

	foreach ($listado as $value) {

		$funcion = $value == '22' ? $i.'ª. '.sanearCaracteres($datos['otras']) : $i.'ª. '.$facultades[$value];

		$texto .= '<w:p w14:paraId="6BF47C60" w14:textId="62ED7392" w:rsidR="00E044CB" w:rsidRDefault="00811369" w:rsidP="00923D8D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">'.$funcion.'</w:t></w:r></w:p>';
		
		$i++;
	}
	
	$documento->setValue("funciones", utf8_decode($texto));

	$documento->save('../documentos/consultorias/'.$fichero);
	
	/*header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);*/	

    return $fichero;
}

function zipAnexoIXLOPD($codigo, $PHPWord){
	
	$listado    = explode(',',$_POST['registros']);
	$documentos = array();
	
	$i = 1;
	
	foreach ($listado as $value) {
		if ($value == 0) {			
			array_push($documentos,anexoIXLOPD($codigo, $PHPWord,$i,'anexoIX.docx'));
		} 
		else {
			array_push($documentos,anexoIXLOPDIND($value, $PHPWord,$i, 'AnexoIX_ind.docx'));			
		}
		
		$i++;
	}

	$zip = new ZipArchive();
    $nameZip = '../documentos/consultorias/anexos_IX.zip';
    if(file_exists($nameZip)){
        unlink($nameZip);
    }
    $ficheroZip = $nameZip;
    if($zip->open($ficheroZip,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $doc){
            $zip->addFile('../documentos/consultorias/'.$doc , $doc);
        }
    }
    if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }
	
	header("Content-Type: application/zip");
    header("Content-Disposition: attachment; filename=anexos_IX.zip");
    header("Content-Transfer-Encoding: binary");

    readfile($nameZip);
}

function zipAnexoVIIILOPD($codigo, $PHPWord, $nombreFichero=''){
	$listado=explode(',',$_POST['registros']);
	$registros=array();
	$registrosP=array();

	foreach ($listado as $value) {
		$value=explode('_',$value);
		if(count($value)>1){
			array_push($registrosP, $value[0]);
		} else {
			array_push($registros, $value[0]);
		}
	}

	$documentos=array();
	$i=1;

	if(in_array('RL', $registros)){
		array_push($documentos, anexoVIIILOPD($codigo, $PHPWord, $nombreFichero));
		$i++;
		$i=array_search('RL',$registros);
		unset($registros[$i]);
		$i = 2;
	}


	$_POST['registros']=implode(',',$registros);
	$_POST['registrosP']=implode(',',$registrosP);

	$datos=consultaBD("SELECT 
						clientes.razonSocial, 
						clientes.domicilio, 
						clientes.cp, 
						clientes.ficheroLogo, 
						clientes.localidad, 
						clientes.cif, 
						clientes.provincia, 
						servicios_familias.referencia AS familia, 
						formulario 
						FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo 
						INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo 
						INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo 
						WHERE trabajos.codigo = ".$codigo,true,true);
	
	$formulario = recogerFormularioServicios($datos);
	
	if($_POST['registros']!=''){
		$consulta=consultaBD("SELECT * FROM usuarios_lopd WHERE codigo IN (".$_POST['registros'].");",true);
		while($item=mysql_fetch_assoc($consulta)){
			array_push($documentos, anexoVIIILOPD_2($datos, $PHPWord,'anexoVIII_2.docx',$i,$item));
			$i++;
		}
	}

	if($_POST['registrosP']!=''){
		$consulta=consultaBD("SELECT * FROM proveedores_lopd WHERE codigo IN (".$_POST['registrosP'].");",true);
		while($item=mysql_fetch_assoc($consulta)){
			array_push($documentos, anexoVIIILOPD_3($datos, $PHPWord,'anexoVIII_2.docx',$i,$item));
			$i++;
		}
	}

	$zip = new ZipArchive();
    $nameZip = '../documentos/consultorias/anexos_VIII.zip';

    if(file_exists($nameZip)){
        unlink($nameZip);
    }

    $ficheroZip = $nameZip;
    if($zip->open($ficheroZip,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $doc){
            $zip->addFile('../documentos/consultorias/'.$doc , $doc);
        }
    }
    if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }
	
	header("Content-Type: application/zip");
    header("Content-Disposition: attachment; filename=anexos_VIII.zip");
    header("Content-Transfer-Encoding: binary");

    readfile($nameZip);
}

function anexoVIIILOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_VIII_FUNCIONES_Y_OBLIGACIONES_1.docx';
	$acceso=array('NO'=>'SIN','SI'=>'CON');
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.ficheroLogo, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$nombreRL=$formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}
	$documento->setValue("representante",utf8_decode($nombreRL));

	$documento->setValue("cp",utf8_decode($datos['cp']));
	$documento->setValue("provincia",utf8_decode(convertirMinuscula($datos['provincia'])));
	$documento->setValue("localidad",utf8_decode($datos['localidad']));
	$documento->setValue("domicilio",utf8_decode($datos['domicilio']));
	$documento->setValue("email",utf8_decode($formulario['pregunta7']));
	$documento->setValue("fecha",utf8_decode($formulario['pregunta2']));
	reemplazarLogo($documento,$datos);

	$usuarios='';
	$consulta=consultaBD("SELECT * FROM usuarios_lopd WHERE codigoTrabajo='$codigo' AND nifUsuarioLOPD!='".$formulario['pregunta14']."' AND (fechaBajaUsuarioLOPD='' OR fechaBajaUsuarioLOPD='0000-00-00');",true);
	$i=1;
	//DIFERENTE LA S
	while($usuariosAccedenFichero=mysql_fetch_assoc($consulta)){
		$acceso='CON';
		$requisitos='<w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Guardar secreto profesional</w:t></w:r><w:r w:rsidR="00AC6ABE" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> sobre los datos de carácter personal y cualquier información o circunstancia relativa a clientes, usuarios</w:t></w:r><w:r w:rsidR="007A68AF" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>, otros empleados/as</w:t></w:r><w:r w:rsidR="00AC6ABE" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> y otras personas cuyos datos conozca y a los que tenga acceso como consecuencia de la relación laboral mantenida con el titular. Este deber de secreto se extiende a cualquier fase del tratamiento de los citados datos y subsistirá aún después de concluida la relación con el responsable de ficheros.</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Utilizar los recursos para su desempeño profesional</w:t></w:r></w:p><w:p w:rsidR="006404AA" w:rsidRPr="008E5717" w:rsidRDefault="006404AA" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="006404AA" w:rsidRPr="008E5717" w:rsidRDefault="006404AA" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Facilitar los derechos ARCO a los interesados</w:t></w:r></w:p><w:p w:rsidR="006404AA" w:rsidRPr="008E5717" w:rsidRDefault="006404AA" w:rsidP="006404AA"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="006404AA" w:rsidRPr="008E5717" w:rsidRDefault="006404AA" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Cambiar su clave de usuario y contraseña anualmente</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Responsabilizarse de la confidencialidad de su puesto de trabajo</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Mantener la configuración de su puesto de trabajo</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">No extraer datos de los ficheros para tratarlos con programas ofimáticos</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>No realizar funciones que requieran una autorización del responsable</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Comunicar al responsable cualquier incidencia que afecte a la seguridad de los datos</w:t></w:r></w:p><w:p w:rsidR="00425D35" w:rsidRPr="008E5717" w:rsidRDefault="00425D35" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00AA702B"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Borrar previamente los ficheros de datos antes de destruir su soporte</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00AA702B"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Conservar los datos registrados automáticamente en el sistema</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00AA702B"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Dejar constancia de todas las entradas y salidas a través del correo electrónico</w:t></w:r></w:p>';
		if($usuariosAccedenFichero['accesoUsuarioLOPD']=='S'){
			$acceso='SIN';
			$requisitos='<w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Guardar secreto profesional</w:t></w:r><w:r w:rsidR="00AC6ABE" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> sobre los datos de carácter personal y cualquier información o circunstancia relativa a clientes, usuarios</w:t></w:r><w:r w:rsidR="007A68AF" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>, otros empleados/as</w:t></w:r><w:r w:rsidR="00AC6ABE" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> y otras personas cuyos datos conozca y a los que tenga acceso como consecuencia de la relación laboral mantenida con el titular. Este deber de secreto se extiende a cualquier fase del tratamiento de los citados datos y subsistirá aún después de concluida la relación con el responsable de ficheros.</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Comunicar al responsable cualquier incidencia que afecte a la seguridad de los datos</w:t></w:r></w:p>';
		}
		if($usuarios!=''){
			$usuarios.='<w:p w14:paraId="66F01424" w14:textId="77777777" w:rsidR="008C1D50" w:rsidRDefault="008C1D50"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:br w:type="page"/></w:r></w:p><w:br/>';
		}
		$usuarios.='<w:p w14:paraId="4AFE403C" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>INFORMACIÓN SOBRE SUS FUNCIONES Y OBLIGACIONES Y SOBRE EL TRATAMIENTO DE SUS DATOS A EMPLEADO/A '.$acceso.' ACCESO A DATOS.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="09627CEC" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">'.sanearCaracteres($datos['razonSocial']).' informa a '.$usuariosAccedenFichero['nombreUsuarioLOPD'].' que sus datos personales pasarán a formar parte de un fichero debidamente protegido cuyo responsable es </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>, que tiene como finalidad gestionar adecuadamente la relación laboral mantenida con el mismo.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="35833734" w14:textId="5DBD687A" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">Los datos personales objeto de tratamiento podrán ser comunicados a terceros sin el consentimiento del interesado, siempre que dicha cesión o comunicación responda a una necesidad de la propia relación laboral, guarde relación con ella y se limite a una finalidad concreta, tal y como se establece </w:t></w:r><w:r w:rsidR="00654FA0" w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>en el Reglamento General de Protección de Datos (Reglamento 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016).</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="6F0915F2" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">Para ejercer su derecho de acceso, rectificación, cancelación u oposición, deberá dirigirse al responsable del fichero </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">,  con domicilio en '.$datos['domicilio'].', C.P. '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).'), o bien, a la dirección de correo electrónico: </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rStyle w:val="Hyperlink"/><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:color w:val="auto"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$formulario['pregunta7'].'</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="5A63B9A0" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>Por medio del presente documento se le comunica que sus funciones y obligaciones con respecto al cumplimiento de los requisitos establecidos en el Reglamento General de Protección de Datos, son las siguientes:</w:t></w:r></w:p><w:br/>';
		$usuarios.=$requisitos.'<w:br/>';
		$usuarios.='<w:p w14:paraId="0EE13D1E" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>El incumplimiento de cualquiera de estas obligaciones será sancionado de acuerdo a lo establecido en el Estatuto de los Trabajadores y Convenios Colectivos de aplicación, facultando al titular  adoptar las medidas legales oportunas contra el empleado así como a reclamarle las indemnizaciones que correspondan por dicho incumplimiento.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="797A459D" w14:textId="77777777" w:rsidR="00A62415" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>En '.$datos['localidad'].', a '.$formulario['pregunta2'].'</w:t></w:r></w:p>';
		$usuarios.='<w:p w14:paraId="2F5F7114" w14:textId="77777777" w:rsidR="009C567A" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblStyle w:val="TableGrid"/><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="4360"/><w:gridCol w:w="4360"/></w:tblGrid><w:tr w:rsidR="001024C7" w14:paraId="63BA0B65" w14:textId="77777777" w:rsidTr="006A6C22"><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="7B81C7D5" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack" w:colFirst="1" w:colLast="1"/><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>Empleado/a</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="3CDC1FBB" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="009C567A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="right"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>El responsable del fichero</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="001024C7" w14:paraId="202ADA48" w14:textId="77777777" w:rsidTr="006A6C22"><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="7353FFF3" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>'.$usuariosAccedenFichero['nombreUsuarioLOPD'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="788EFA72" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="009C567A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="right"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r></w:p></w:tc></w:tr><w:bookmarkEnd w:id="0"/></w:tbl>';

	}
	$usuarios='';
	$documento->setValue("usuarios",utf8_decode($usuarios));

	$documento->save('../documentos/consultorias/'.$fichero);
	
	/*header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);*/
    return $fichero;
}

function anexoVIIILOPD_2($datos, $PHPWord, $nombreFichero='',$i,$usuariosAccedenFichero){
	global $_CONFIG;
	$fichero='ANEXO_VIII_FUNCIONES_Y_OBLIGACIONES_'.$i.'.docx';
	$acceso=array('NO'=>'SIN','SI'=>'CON');
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$puesto = $usuariosAccedenFichero['puestoUsuarioLOPD'] != '' ? sanearCaracteres($usuariosAccedenFichero['puestoUsuarioLOPD']) : "Empleado/a";

	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$nombreRL=$formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}
	$documento->setValue("representante",utf8_decode($nombreRL));

	$documento->setValue("cp",utf8_decode($datos['cp']));
	$documento->setValue("provincia",utf8_decode(convertirMinuscula($datos['provincia'])));
	$documento->setValue("localidad",utf8_decode($datos['localidad']));
	$documento->setValue("domicilio",utf8_decode($datos['domicilio']));
	$documento->setValue("email",utf8_decode($formulario['pregunta7']));
	$documento->setValue("fecha",utf8_decode($formulario['pregunta2']));
	reemplazarLogo($documento,$datos);

	$usuarios='';
		$acceso='CON';
		$requisitos='<w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Guardar secreto profesional</w:t></w:r><w:r w:rsidR="00AC6ABE" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> sobre los datos de carácter personal y cualquier información o circunstancia relativa a clientes, usuarios</w:t></w:r><w:r w:rsidR="007A68AF" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>, otros empleados/as</w:t></w:r><w:r w:rsidR="00AC6ABE" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> y otras personas cuyos datos conozca y a los que tenga acceso como consecuencia de la relación laboral mantenida con el titular. Este deber de secreto se extiende a cualquier fase del tratamiento de los citados datos y subsistirá aún después de concluida la relación con el responsable de ficheros.</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Utilizar los recursos para su desempeño profesional</w:t></w:r></w:p><w:p w:rsidR="006404AA" w:rsidRPr="008E5717" w:rsidRDefault="006404AA" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="006404AA" w:rsidRPr="008E5717" w:rsidRDefault="006404AA" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Facilitar los derechos ARCO a los interesados</w:t></w:r></w:p><w:p w:rsidR="006404AA" w:rsidRPr="008E5717" w:rsidRDefault="006404AA" w:rsidP="006404AA"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="006404AA" w:rsidRPr="008E5717" w:rsidRDefault="006404AA" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Cambiar su clave de usuario y contraseña anualmente</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Responsabilizarse de la confidencialidad de su puesto de trabajo</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Mantener la configuración de su puesto de trabajo</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">No extraer datos de los ficheros para tratarlos con programas ofimáticos</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>No realizar funciones que requieran una autorización del responsable</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Comunicar al responsable cualquier incidencia que afecte a la seguridad de los datos</w:t></w:r></w:p><w:p w:rsidR="00425D35" w:rsidRPr="008E5717" w:rsidRDefault="00425D35" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00AA702B"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Borrar previamente los ficheros de datos antes de destruir su soporte</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00AA702B"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Conservar los datos registrados automáticamente en el sistema</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00AA702B"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Dejar constancia de todas las entradas y salidas a través del correo electrónico</w:t></w:r></w:p>';
		if($usuariosAccedenFichero['accesoUsuarioLOPD']=='S'){
			$acceso='SIN';
			$requisitos='<w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Guardar secreto profesional</w:t></w:r><w:r w:rsidR="00AC6ABE" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> sobre los datos de carácter personal y cualquier información o circunstancia relativa a clientes, usuarios</w:t></w:r><w:r w:rsidR="007A68AF" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>, otros empleados/as</w:t></w:r><w:r w:rsidR="00AC6ABE" w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> y otras personas cuyos datos conozca y a los que tenga acceso como consecuencia de la relación laboral mantenida con el titular. Este deber de secreto se extiende a cualquier fase del tratamiento de los citados datos y subsistirá aún después de concluida la relación con el responsable de ficheros.</w:t></w:r></w:p><w:p w:rsidR="00C02FF5" w:rsidRPr="008E5717" w:rsidRDefault="00C02FF5" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Comunicar al responsable cualquier incidencia que afecte a la seguridad de los datos</w:t></w:r></w:p>';
		}
		if($usuarios!=''){
			$usuarios.='<w:p w14:paraId="66F01424" w14:textId="77777777" w:rsidR="008C1D50" w:rsidRDefault="008C1D50"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:br w:type="page"/></w:r></w:p><w:br/>';
		}
		$usuarios.='<w:p w14:paraId="4AFE403C" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>INFORMACIÓN SOBRE SUS FUNCIONES Y OBLIGACIONES Y SOBRE EL TRATAMIENTO DE SUS DATOS A EMPLEADO/A '.$acceso.' ACCESO A DATOS.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="09627CEC" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">'.sanearCaracteres($datos['razonSocial']).' informa a '.$usuariosAccedenFichero['nombreUsuarioLOPD'].' que sus datos personales pasarán a formar parte de un fichero debidamente protegido cuyo responsable es </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>, que tiene como finalidad gestionar adecuadamente la relación laboral mantenida con el mismo.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="35833734" w14:textId="5DBD687A" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">Los datos personales objeto de tratamiento podrán ser comunicados a terceros sin el consentimiento del interesado, siempre que dicha cesión o comunicación responda a una necesidad de la propia relación laboral, guarde relación con ella y se limite a una finalidad concreta, tal y como se establece </w:t></w:r><w:r w:rsidR="00654FA0" w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>en el Reglamento General de Protección de Datos (Reglamento 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016).</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="6F0915F2" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">Para ejercer su derecho de acceso, rectificación, cancelación u oposición, deberá dirigirse al responsable del fichero </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">,  con domicilio en '.$datos['domicilio'].', C.P. '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).'), o bien, a la dirección de correo electrónico: </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rStyle w:val="Hyperlink"/><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:color w:val="auto"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$formulario['pregunta7'].'</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="5A63B9A0" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>Por medio del presente documento se le comunica que sus funciones y obligaciones con respecto al cumplimiento de los requisitos establecidos en el Reglamento General de Protección de Datos, son las siguientes:</w:t></w:r></w:p><w:br/>';
		$usuarios.=$requisitos.'<w:br/>';
		$usuarios.='<w:p w14:paraId="0EE13D1E" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>El incumplimiento de cualquiera de estas obligaciones será sancionado de acuerdo a lo establecido en el Estatuto de los Trabajadores y Convenios Colectivos de aplicación, facultando al titular  adoptar las medidas legales oportunas contra el empleado así como a reclamarle las indemnizaciones que correspondan por dicho incumplimiento.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="797A459D" w14:textId="77777777" w:rsidR="00A62415" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>En '.$datos['localidad'].', a '.formateaFechaWeb($usuariosAccedenFichero['fechaAltaUsuarioLOPD']).'</w:t></w:r></w:p>';
		$usuarios.='<w:p w14:paraId="2F5F7114" w14:textId="77777777" w:rsidR="009C567A" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblStyle w:val="TableGrid"/><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="4360"/><w:gridCol w:w="4360"/></w:tblGrid><w:tr w:rsidR="001024C7" w14:paraId="63BA0B65" w14:textId="77777777" w:rsidTr="006A6C22"><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="7B81C7D5" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack" w:colFirst="1" w:colLast="1"/><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>'.$puesto.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="3CDC1FBB" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="009C567A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="right"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>El responsable del fichero</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="001024C7" w14:paraId="202ADA48" w14:textId="77777777" w:rsidTr="006A6C22"><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="7353FFF3" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>'.$usuariosAccedenFichero['nombreUsuarioLOPD'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="788EFA72" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="009C567A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="right"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r></w:p></w:tc></w:tr><w:bookmarkEnd w:id="0"/></w:tbl>';

	$documento->setValue("usuarios",utf8_decode($usuarios));

	$documento->save('../documentos/consultorias/'.$fichero);
	
	/*header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);*/
    return $fichero;
}

function anexoVIIILOPD_3($datos, $PHPWord, $nombreFichero='',$i,$usuariosAccedenFichero){
	global $_CONFIG;
	$fichero='ANEXO_VIII_FUNCIONES_Y_OBLIGACIONES_'.$i.'.docx';
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$nombreRL=$formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}
	$documento->setValue("representante",utf8_decode($nombreRL));

	$documento->setValue("cp",utf8_decode($datos['cp']));
	$documento->setValue("provincia",utf8_decode(convertirMinuscula($datos['provincia'])));
	$documento->setValue("localidad",utf8_decode($datos['localidad']));
	$documento->setValue("domicilio",utf8_decode($datos['domicilio']));
	$documento->setValue("email",utf8_decode($formulario['pregunta7']));
	$documento->setValue("fecha",utf8_decode($formulario['pregunta2']));
	reemplazarLogo($documento,$datos);

	$usuarios='';
		$requisitos='<w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="008707F8"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Guardar   secreto profesional sobre los datos de carácter personal y cualquier información o circunstancia relativa a clientes, usuarios, otros empleados/as y otras personas cuyos datos conozca como consecuencia de la relación contractual mantenida con el titular. Este deber de secreto subsistirá aún después de concluida la relación con el responsable del tratamiento.</w:t></w:r></w:p><w:p w:rsidR="008707F8" w:rsidRPr="008E5717" w:rsidRDefault="008707F8" w:rsidP="00F80EA3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="4"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E5717"><w:rPr><w:rFonts w:cs="Calibri"/></w:rPr><w:t>Comunicar al responsable cualquier incidencia que detecte y que afecte a la seguridad de los datos</w:t></w:r></w:p>';
		if($usuarios!=''){
			$usuarios.='<w:p w14:paraId="66F01424" w14:textId="77777777" w:rsidR="008C1D50" w:rsidRDefault="008C1D50"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:br w:type="page"/></w:r></w:p><w:br/>';
		}
		$usuarios.='<w:p w14:paraId="4AFE403C" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>INFORMACIÓN SOBRE SUS FUNCIONES Y  OBLIGACIONES Y  SOBRE  EL  TRATAMIENTO DE  SUS DATOS A PROVEEDOR SIN ACCESO A  DATOS DE CARÁCTER PERSONAL PERO CON  ACCESO A LAS DEPENDENCIAS EN LAS QUE SE TRATAN ESTOS DATOS.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="09627CEC" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">'.sanearCaracteres($datos['razonSocial']).' informa a '.$usuariosAccedenFichero['nombre'].' que sus datos personales pasarán a formar parte de un fichero debidamente protegido cuyo responsable es </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>, que tiene como finalidad gestionar adecuadamente la relación contractual de prestación de servicios de '.$usuariosAccedenFichero['servicio'].' acordada por ambas partes.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="35833734" w14:textId="5DBD687A" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">Los datos personales objeto de tratamiento podrán ser comunicados a terceros sin el consentimiento del interesado, siempre que dicha cesión o comunicación responda a una necesidad de la propia relación contractual, guarde relación con ella y se limite a una finalidad concreta, tal y como se establece </w:t></w:r><w:r w:rsidR="00654FA0" w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>en el Reglamento General de Protección de Datos (Reglamento 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016).</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="6F0915F2" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">Para ejercer su derecho de acceso, rectificación, cancelación u oposición, limitación de tratamiento o portabilidad, deberá dirigirse al responsable del fichero </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t xml:space="preserve">,  con domicilio en '.$datos['domicilio'].', C.P. '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).'), o bien, a la dirección de correo electrónico: </w:t></w:r><w:r w:rsidRPr="003472EB"><w:rPr><w:rStyle w:val="Hyperlink"/><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:color w:val="auto"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$formulario['pregunta7'].'</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="5A63B9A0" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>Por medio del presente documento se le comunica que sus funciones y obligaciones con respecto al cumplimiento de los requisitos establecidos en el Reglamento General de Protección de Datos, son las siguientes:</w:t></w:r></w:p><w:br/>';
		$usuarios.=$requisitos.'<w:br/>';
		$usuarios.='<w:p w14:paraId="0EE13D1E" w14:textId="77777777" w:rsidR="00A62415" w:rsidRPr="003472EB" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>El incumplimiento de cualquiera de estas obligaciones será sancionado de acuerdo a lo establecido por la normativa administrativa, civil o penal en vigor, facultando al titular adoptar las medidas legales oportunas contra el proveedor así como a reclamarle las indemnizaciones que correspondan por dicho incumplimiento.</w:t></w:r></w:p><w:br/>';
		$usuarios.='<w:p w14:paraId="797A459D" w14:textId="77777777" w:rsidR="00A62415" w:rsidRDefault="00A62415" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="003472EB"><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>En '.$datos['localidad'].', a '.formateaFechaWeb($usuariosAccedenFichero['fechaAlta']).'</w:t></w:r></w:p>';
		$usuarios.='<w:p w14:paraId="2F5F7114" w14:textId="77777777" w:rsidR="009C567A" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblStyle w:val="TableGrid"/><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/><w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="4360"/><w:gridCol w:w="4360"/></w:tblGrid><w:tr w:rsidR="001024C7" w14:paraId="63BA0B65" w14:textId="77777777" w:rsidTr="006A6C22"><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="7B81C7D5" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack" w:colFirst="1" w:colLast="1"/><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>Proveedor</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="3CDC1FBB" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="009C567A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="right"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>El responsable del fichero</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="001024C7" w14:paraId="202ADA48" w14:textId="77777777" w:rsidTr="006A6C22"><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="7353FFF3" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="00A62415"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="left"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>'.$usuariosAccedenFichero['representante'].'<w:br/>Representante legal de '.$usuariosAccedenFichero['nombre'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4360" w:type="dxa"/></w:tcPr><w:p w14:paraId="788EFA72" w14:textId="77777777" w:rsidR="001024C7" w:rsidRDefault="009C567A" w:rsidP="009C567A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="right"/><w:rPr><w:rFonts w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/></w:rPr><w:t>'.$nombreRL.'<w:br/>Representante legal de '.sanearCaracteres($datos['razonSocial']).'</w:t></w:r></w:p></w:tc></w:tr><w:bookmarkEnd w:id="0"/></w:tbl>';

	$documento->setValue("usuarios",utf8_decode($usuarios));

	$documento->save('../documentos/consultorias/'.$fichero);
	
	/*header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);*/
    return $fichero;
}

function anexoXLOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	
	$fichero = 'ANEXO_X_INCIDENCIAS.docx';

	$sql = "SELECT 
				c.*, 
				sf.referencia AS familia, 
				t.formulario 
			FROM 
				trabajos t 
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo 
			WHERE 
				t.codigo = ".$codigo.";";
	
	$datos      = consultaBD($sql, false, true);
	$formulario = recogerFormularioServicios($datos);
	$datos      = datosPersonales($datos,$formulario);
	
	$documento = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("true","");
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",utf8_decode(date("d/m/Y")));

	reemplazarLogo($documento,$datos,'image2.png');


	$tablaIncidencias = fechaTabla(date('d/m/Y'),'QUIEBRAS DE SEGURIDAD');

	$tablaIncidencias .= '<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="796"/><w:gridCol w:w="1832"/><w:gridCol w:w="2012"/><w:gridCol w:w="2015"/><w:gridCol w:w="2065"/></w:tblGrid><w:tr w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w14:paraId="51541499" w14:textId="77777777" w:rsidTr="00A07FBD"><w:tc><w:tcPr><w:tcW w:w="1139" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="5D5FC966" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2618" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3EBE3515" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>INCIDENTE PRODUCIDO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3330" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2207371B" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3220" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="09D9BD6F" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>NOTIFICA A LA AUTORIDAD</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3088" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="01450EC4" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>NOTIFICADO A LOS INTERESADOS</w:t></w:r></w:p></w:tc></w:tr>';

	$i = 1;	
	$incidencias = consultaBD("SELECT * FROM incidencias_lopd WHERE codigoCliente='".$datos['codigo']."';");
	
	while ($incidencia = mysql_fetch_assoc($incidencias)) {
		$ref = str_pad($i, 2, '0', STR_PAD_LEFT);
		
		$notificada = $incidencia['fechaNotificadaAutoridad'] != '' ? 'SI' : 'NO';
		
		$tablaIncidencias .= '<w:tr w:rsidR="00E17BBB" w14:paraId="64E5EF4C" w14:textId="77777777" w:rsidTr="00E17BBB"><w:trPr><w:trHeight w:val="624"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1139" w:type="dxa"/></w:tcPr><w:p w14:paraId="21A70F8C" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2618" w:type="dxa"/></w:tcPr><w:p w14:paraId="210C2442" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="00E17BBB" w:rsidRDefault="00E17BBB" w:rsidP="00E17BBB"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>'.$incidencia['incidente'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3330" w:type="dxa"/></w:tcPr><w:p w14:paraId="69D7CCDE" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>'.formateaFechaWeb($incidencia['fechaOcurrio']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>7</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3220" w:type="dxa"/></w:tcPr><w:p w14:paraId="16DB5923" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="002B0C55" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>'.formateaFechaWeb($incidencia['fechaNotificadaAutoridad']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3088" w:type="dxa"/></w:tcPr><w:p w14:paraId="77F74A2F" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>'.formateaFechaWeb($incidencia['fechaNotificadaInteresados']).'</w:t></w:r></w:p></w:tc></w:tr>';

		$i++;		
	}	
	
	$total = $i - 1;
	
	$tablaIncidencias .= '</w:tbl>'.pieTabla($total);

	$tablaTexto = '';
	$j = 1;
	
	$incidenciasAux = consultaBD("SELECT * FROM incidencias_lopd WHERE codigoCliente='".$datos['codigo']."';");
	while($incidenciaAux = mysql_fetch_assoc($incidenciasAux)) {
		$ref = str_pad($j, 4, '0', STR_PAD_LEFT);
		
		$tablaTexto .= '<w:p w14:paraId="152D5665" w14:textId="77777777" w:rsidR="00FF760B" w:rsidRDefault="00FF760B" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w14:paraId="0758240E" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:u w:val="single"/></w:rPr><w:t>Referencia</w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/><w:u w:val="single"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p><w:p w14:paraId="5B6BEBE0" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="5644D275" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>INCIDENTE: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['incidente'].'</w:t></w:r></w:p><w:p w14:paraId="294AA36E" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="2F6C6D08" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Fecha: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.formateaFechaWeb($incidenciaAux['fechaOcurrio']).'</w:t></w:r></w:p><w:p w14:paraId="6D9F9CFA" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1F561131" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Fichero en el que están incluidos los datos objeto de quiebra de seguridad: </w:t></w:r><w:r w:rsidR="00396720" w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">  </w:t></w:r><w:r w:rsidR="00396720" w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['ficherosDatosQuiebra'].'</w:t></w:r></w:p><w:p w14:paraId="43DE4DB5" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4257E87A" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Naturaleza de la quiebra de seguridad: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['naturalezaQuiebra'].'</w:t></w:r></w:p><w:p w14:paraId="417E745A" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="2B4266FB" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Categorías de datos afectados: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['categoriaDatosAfectados'].'</w:t></w:r></w:p><w:p w14:paraId="2D756D32" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="0CEAF1FB" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Categoría de interesados afectados: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['categoriaInteresadosAfectados'].'</w:t></w:r></w:p><w:p w14:paraId="2C51E35B" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="5C8BE43C" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Medidas adoptadas para solventar la quiebra: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['medidasAdoptadas'].'</w:t></w:r></w:p><w:p w14:paraId="7DBFCB41" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="41E56750" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Medidas aplicadas para paliar posibles efectos negativos sobre los interesados: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['medidasAplicadas'].'</w:t></w:r></w:p><w:p w14:paraId="18B934B8" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1428235B" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Notificada a la autoridad de protección de datos competente el día: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.formateaFechaWeb($incidenciaAux['fechaNotificadaAutoridad']).'</w:t></w:r></w:p><w:p w14:paraId="62E1097D" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="6E35D96C" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Notificada a los interesados el día: </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.formateaFechaWeb($incidenciaAux['fechaNotificadaInteresados']).'</w:t></w:r></w:p><w:p w14:paraId="78F79C66" w14:textId="77777777" w:rsidR="00A72B20" w:rsidRPr="00EC6CFD" w:rsidRDefault="00A72B20" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="22E2FC59" w14:textId="77777777" w:rsidR="00A72B20" w:rsidRPr="00EC6CFD" w:rsidRDefault="00A72B20" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Motivos por los que no se ha notificado a la AEPD en el plazo de 72 horas: </w:t></w:r><w:r w:rsidR="00EC4C14" w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00EC4C14" w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.nl2br($incidenciaAux['motivos']).'</w:t></w:r></w:p>';

		$j++;
	}


	$documento->setValue("tabla",utf8_decode($tablaIncidencias));
	$documento->setValue("tablaTexto",utf8_decode($tablaTexto));	

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

/*function anexoContratoEncargadoTratamientoLOPD($codigo, $PHPWord, $nombreFichero=''){
	$fichero='CONTRATO PARA ACTUAR COMO ENCARGADO DE TRATAMIENTO.docx';
	$sitio=array('ENCARGADO'=>'Locales del encargado','RESPONSABLE'=>'Locales del responsable','REMOTO'=>'Mediante acceso remoto');
	$servicio=array('asesoría laboral para la realización de nóminas','asesoría contable/fiscal','video vigilancia','PRL','mantenimiento de equipos informáticos','mantenimiento de aplicaciones','mantenimiento de la página web','copias de seguridad "Backup"','transporte/mensajería para envíos a clientes','limpieza','mantenimiento de instalaciones','vigilancia/seguridad','mantenimiento de la copiadora','colaboración mercantil','selección de personal');
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	// Datos Responsable del fichero
	$documento->setValue("cliente",utf8_decode($datos['razonSocial']));
	$documento->setValue("cifCliente",utf8_decode($datos['cif']));
	$documento->setValue("domicilioCliente",utf8_decode($datos['domicilio']));
	$documento->setValue("cpCliente",utf8_decode($datos['cp']));
	$documento->setValue("localidadCliente",utf8_decode($datos['localidad']));
	$documento->setValue("provinciaCliente",utf8_decode($datos['provincia']));
	$documento->setValue("representanteCliente",utf8_decode($formulario['pregunta8']));

	// Datos encargado del tratamiento
	$documento->setValue("encargado",utf8_decode($formulario['pregunta159']));
	$documento->setValue("cifEncargado",utf8_decode($formulario['pregunta160']));
	$documento->setValue("domicilioEncargado",utf8_decode($formulario['pregunta161']));
	$documento->setValue("cpEncargado",utf8_decode($formulario['pregunta164']));
	$documento->setValue("localidadEncargado",utf8_decode($formulario['pregunta163']));
	$documento->setValue("provinciaEncargado",utf8_decode($formulario['pregunta163']));
	$documento->setValue("representanteEncargado",utf8_decode($formulario['pregunta167']));

	$documento->setValue("fechaContrato",utf8_decode($formulario['pregunta543']));
	$documento->setValue("servicio",utf8_decode($formulario['pregunta158']));
	$documento->setValue("sitio",utf8_decode($sitio[$formulario['pregunta511']]));	


	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}*/

function zipAnexoContratoEncargadoTratamientoLOPD($codigo, $PHPWord, $nombreFichero=''){
	$fichero='CONTRATO_PARA_ACTUAR_COMO_ENCARGADO_DE_TRATAMIENTO.pdf';
	$registros=explode(',', $_POST['registros']);
	$ficheros=array();
	$responsables=array();
	foreach ($registros as $key => $value) {
		$parts=explode('_', $value);
		if($parts[0]=='RESP'){
			$fichero=array('responsable'=>$parts[1],'encargadoF'=>0,'encargadoN'=>0);
			if($parts[2]=='FIJO'){
				$fichero['encargadoF']=$parts[3];
			} else {
				$fichero['encargadoN']=$parts[3];
			}
			array_push($ficheros, $fichero);
		} else {
			$responsable=array('responsable'=>$parts[1],'encargadoF'=>0,'encargadoN'=>0);
			if($parts[2]=='FIJO'){
				$responsable['encargadoF']=$parts[3];
			} else {
				$responsable['encargadoN']=$parts[3];
			}
			array_push($responsables, $responsable);
		}
	}
	$documentos=array();
	$i=1;
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario, trabajos.codigoCliente, clientes.administrador, clientes.actividad, clientes.servicios, trabajos.codigo AS codigoTrabajo, clientes.apellido1, clientes.apellido2 FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	foreach ($ficheros as $key => $value) {
		$item=consultaBD('SELECT * FROM declaraciones WHERE codigo='.$value['responsable'],true,true);
		if($item['n_razon']!=''){
			if($value['encargadoF']!=0){
				$item=preparaEncargado($item,$value['encargadoF'],'FIJO',$datos);
			} elseif($value['encargadoN']!=0){
				$item=preparaEncargado($item,$value['encargadoN'],'NOFIJO');
			}
			$item['fechaMostrar']=$item['fecha'];
			$item['fechaMostrarBaja']=$item['fechaBaja'];
			$documento=anexoContratoEncargadoTratamientoLOPD($datos, $item, $PHPWord, $i);
			if($documento!=''){
				array_push($documentos, $documento);
				$i++;
			}
		}
	}
	if(count($responsables)>0){
		foreach ($responsables as $key => $value) {
			$responsable=consultaBD('SELECT * FROM responsables_fichero WHERE codigo='.$value['responsable'],true,true);
			$item=datosRegistro('declaraciones',$responsable['codigoDeclaracion']);
			$item=preparaResponsable($item,$responsable);
			if($value['encargadoF']!=0){
				$item=preparaEncargado($item,$value['encargadoF'],'FIJO',$datos);
			} elseif($value['encargadoN']!=0){
				$item=preparaEncargado($item,$value['encargadoN'],'NOFIJO');
			}
			$item['fechaMostrar']=$item['fechaAltaResponsable'];
			$item['fechaMostrarBaja']=$item['fechaBajaResponsable'];
			$documento=anexoContratoEncargadoTratamientoLOPD($datos, $item, $PHPWord, $i);
			if($documento!=''){
				array_push($documentos, $documento);
				$i++;
			}
		}
	}
	$zip = new ZipArchive();
    $nameZip = '../documentos/consultorias/contratos_actua_encargados.zip';
    if(file_exists($nameZip)){
        unlink($nameZip);
    }
    $ficheroZip = $nameZip;
    if($zip->open($ficheroZip,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $doc){
            $zip->addFile('../documentos/consultorias/'.$doc , $doc);
        }
    }
    if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }
	
	header("Content-Type: application/zip");
    header("Content-Disposition: attachment; filename=contratos_actua_encargados.zip");
    header("Content-Transfer-Encoding: binary");

    readfile($nameZip);
}

function preparaEncargado($item,$codigo,$tipo,$datos=false, $conexion = true){

	if($tipo=='FIJO'){
		$campos=array(
			158=>array(511,159,160,161,164,163,167,543,574,544,541,162,611,165,'Asesoría laboral para la realización de nóminas'),
			168=>array(512,169,170,171,174,173,177,545,575,546,527,172,612,175,'Asesoría contable/fiscal'),
			178=>array(513,179,180,181,184,183,187,547,576,548,542,182,613,185,'Video vigilancia'),
			189=>array(514,190,191,192,195,194,198,549,577,550,528,193,614,196,'PRL'),
			200=>array(515,201,202,203,206,205,209,551,578,552,529,204,615,207,'Mantenimiento de equipos informáticos'),
			210=>array(516,211,212,213,216,215,219,553,579,554,530,214,616,217,'Mantenimiento de aplicaciones'),
			220=>array(517,221,222,223,226,225,229,555,609,556,531,224,617,227,'Mantenimiento de la pagina web'),
			230=>array(518,231,232,233,236,235,239,557,580,558,532,234,618,237,'Copias de seguridad "Backup"'),
			240=>array(519,241,242,243,246,245,249,559,581,560,533,244,618,246,'Transporte/mensajería para envíos a clientes'),
			250=>array(520,251,252,253,256,255,259,561,582,562,534,254,620,257,'Limpieza'),
			260=>array(521,261,262,263,266,265,269,563,583,564,535,264,621,267,'Mantenimiento de instalaciones'),
			270=>array(522,271,272,273,276,275,279,565,584,566,536,274,622,277,'Vigilancia/seguridad'),
			280=>array(523,281,282,283,286,285,289,567,585,568,537,284,623,287,'Mantenimiento de la copiadora'),
			290=>array(524,291,292,293,296,295,299,569,586,570,538,294,624,297,'Colaboración mercantil'),
			487=>array(525,488,489,490,493,492,496,572,587,544,571,491,625,494,'Selección de personal')
		);
		$item['codigoEncargado']=$codigo;
		$item['tipoEncargado']='ENCARGADOFIJO';
		$formulario=recogerFormularioServicios($datos);
		$item['donde_encargado']=$formulario['pregunta'.$campos[$codigo][0]];
		$item['n_razon_encargado']=$formulario['pregunta'.$campos[$codigo][1]];
		$item['cif_nif_encargado']=$formulario['pregunta'.$campos[$codigo][2]];
		$item['dir_postal_encargado']=$formulario['pregunta'.$campos[$codigo][3]];
		$item['postal_encargado']=$formulario['pregunta'.$campos[$codigo][4]];
		$item['localidad_encargado']=$formulario['pregunta'.$campos[$codigo][5]];
		$item['provincia_encargado']=$formulario['pregunta'.$campos[$codigo][12]];
		$item['nombre']=$formulario['pregunta'.$campos[$codigo][6]];
		$item['fecha']=formateaFechaBD($formulario['pregunta'.$campos[$codigo][7]]);
		$item['fechaBaja']=formateaFechaBD($formulario['pregunta'.$campos[$codigo][9]]);
		$item['email_encargado']=$formulario['pregunta'.$campos[$codigo][11]];
		$item['subcontratacion_encargado']=$formulario['pregunta'.$campos[$codigo][10]];
		$item['servicioEncargado']=$campos[$codigo][14];
		$item['telefono_encargado']=$campos[$codigo][13];
		$tratamientos=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$item['codigoEncargado'].' AND fijo="SI"', $conexion,true);
	} else if($tipo=='NOFIJO'){
		$item['codigoEncargado']=$codigo;
		$item['tipoEncargado']='ENCARGADO';
		$encargado=datosRegistro('otros_encargados_lopd',$codigo);
		$item['donde_encargado']=$encargado['dondeEncargado'];
		$item['n_razon_encargado']=$encargado['nombreEncargado'];
		$item['cif_nif_encargado']=$encargado['cifEncargado'];
		$item['dir_postal_encargado']=$encargado['direccionEncargado'];
		$item['postal_encargado']=$encargado['cpEncargado'];
		$item['localidad_encargado']=$encargado['localidadEncargado'];
		$item['provincia_encargado']=$encargado['provinciaEncargado'];
		$item['nombre']=$encargado['representanteEncargado'];
		$item['fecha']=$encargado['fechaAltaEncargado'];
		$item['fechaBaja']=$encargado['fechaBajaEncargado'];
		$item['email_encargado']=$encargado['emailEncargado'];
		$item['subcontratacion_encargado']=$encargado['subcontratacionEncargado'];
		$item['servicioEncargado']=$encargado['servicioEncargado'];
		$item['telefono_encargado']=$encargado['telefonoEncargado'];
		$tratamientos=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$item['codigoEncargado'].' AND fijo="NO"', true,true);
	} else if($tipo=='SIN'){
		$item['codigoEncargado']=0;
		$item['tipoEncargado']='';
		$item['donde_encargado']='';
		$item['n_razon_encargado']='';
		$item['cif_nif_encargado']='';
		$item['dir_postal_encargado']='';
		$item['postal_encargado']='';
		$item['localidad_encargado']='';
		$item['provincia_encargado']='';
		$item['nombre']='';
		$item['fecha']='';
		$item['fechaBaja']='';
		$item['email_encargado']='';
		$item['subcontratacion_encargado']='';
		$item['servicioEncargado']='';
		$item['telefono_encargado']='';
		$tratamientos=false;
	} 
	
	if($tratamientos){
		foreach ($tratamientos as $key => $value) {
			if(strpos($key,'check')!==false){
				$item[$key]=$value;
			}
		}
	}

	return $item;
}

function anexoContratoEncargadoTratamientoLOPD($datos, $item, $PHPWord, $i){
	$fichero='CONTRATO_PARA_ACTUAR_COMO_ENCARGADO_DE_TRATAMIENTO_'.$i.'.pdf';
	$formulario=recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        #cabecera{
	        	margin-bottom:20px;
	    	}

	        #datosCabecera{
	        	width:50%;
	        	position:absolute;
	        	right:-20px;
	        	top:20px;
	    	}

	    	#datosCabecera td{
	    		padding-right:10px;
	    	}

	    	#datosCabecera .dato{
	    		border-bottom:1px solid #000;
	    		width:200px;
	    		padding:0px;
	    		text-align:right;
	    	}

	    	.titulo{
	    		font-size:17px;
	    		font-weight:bold;
	    		text-align:center;
	    		color:#15284B;
	    		background:#EDF1F9;
	    		padding:10px;
	    	}

	    	h1{
	    		color:#15284B;
	    		font-size:12px;
	    		text-decoration:underline;
	    		text-align:center;
	    		margin-bottom:10px;
	    		margin-top:15px;
	    	}
	    	
	    	p{
	    		line-height: 20px;
	    		margin-bottom:3px;
	    		margin-top:3px;
	    		text-align: justify;
	    	}

	    	#tablaFirmas{
	    		width:100%;
	    		margin-top:100px;
	    	}

	    	#tablaFirmas td{
	    		width:50%;
	    		text-align:center;
	    	}

	    	ul{
	    		margin-left:-50px;
	    	}

	    	ul li{
	    		padding-left:22px;
	    		padding-top:5px;
	    	}

	    	ul .marcado{
	    		background:url(../img/check.png) left top no-repeat;
	    	}

	    	.logo{
	    		width:200px;
	    	}

	    	
	-->
	</style>";
	
	$contenido.=obtieneContratoParaActuar($datos,$item,$i);
	
	require_once('../../api/html2pdf/html2pdf.class.php');
	$html2pdf=new HTML2PDF('P','A4','es');
	$html2pdf->pdf->SetDisplayMode('fullpage');
	$html2pdf->setDefaultFont("calibri");
	$html2pdf->WriteHTML($contenido);
	$html2pdf->Output('../documentos/consultorias/'.$fichero,'f');
	return $fichero;
}

function preparaResponsable($item,$responsable){
	$item['n_razon']=$responsable['razonSocial'];
	$item['cif_nif_responsableFichero']=$responsable['cif'];
	$item['dir_postal_responsableFichero']=$responsable['direccion'];
	$item['postal_responsableFichero']=$responsable['cp'];
	$item['localidad_responsableFichero']=$responsable['localidad'];
	$item['provincia_responsableFichero']=convertirMinuscula($responsable['provincia']);
	$item['email_responsableFichero']=$responsable['email']==''?'del Responsable del tratamiento':$responsable['email'];
	$item['representante_responsableFichero']=$responsable['representante'];
	$item['fechaBajaResponsable']=$responsable['fechaFin'];
	$item['fechaAltaResponsable']=$responsable['fechaInicio'];
	return $item;
}

function obtieneContratoParaActuar($datos,$item,$i){
	if(!isset($item['fechaBajaResponsable'])){
		$item['fechaBajaResponsable']='';
	}
	$provinciasAgencia=array(''=>'','01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	$sitio=array(''=>'','N'=>'','NULL'=>'','ENCARGADO'=>'Locales del encargado','RESPONSABLE'=>'Locales del responsable','REMOTO'=>'Mediante acceso remoto');
	$sitios=explode('&$&', $item['donde_encargado']);
	$sitiosTexto='';
	$localesEncargado='';
	foreach ($sitios as $key => $value) {
		if($sitiosTexto!=''){
			$sitiosTexto.=', ';
		}
		$sitiosTexto.=$sitio[$value];
		if($value=='ENCARGADO'){
			$localesEncargado='Al realizarse el tratamiento de los datos personales en los locales del encargado del tratamiento, y con el fin de acreditar las medidas de seguridad implantadas y actualizadas por el encargado del tratamiento, este deberá remitir al responsable del fichero el documento de seguridad donde figuren dichas medidas, así como la identificación del fichero.';
		}
	}
	$servicio=array('asesoría laboral para la realización de nóminas','asesoría contable/fiscal','video vigilancia','PRL','mantenimiento de equipos informáticos','mantenimiento de aplicaciones','mantenimiento de la página web','copias de seguridad "Backup"','transporte/mensajería para envíos a clientes','limpieza','mantenimiento de instalaciones','vigilancia/seguridad','mantenimiento de la copiadora','colaboración mercantil','selección de personal');
	$actividades=array('ABOGACIA'=>'Abogacía','FISCAL'=>'Asesoramiento fiscal','CONTABLE'=>'Asesoramiento fiscal y contable','CONTABILIDAD'=>'Contabilidad externa','AUDITORIA'=>'Auditoría de cuentas','PROMOCION'=>'Promoción inmobiliaria','OTRA'=>'Otra');
	$niveles=array(''=>'',1=>'Básico',2=>'Medio',3=>'Alto');
	//echo $datos['actividad'];
	$servicio=$datos['actividad']=='OTRA'?$datos['servicios']:$actividades[$datos['actividad']];
	$servicio=$item['servicioEncargado'];
	$tiposDatos=array('checkDatosProtegidos0'=>'Ideología','checkDatosProtegidos1'=>'Afiliación sindical','checkDatosProtegidos2'=>'Religión','checkDatosProtegidos3'=>	'Creencias','checkDatosEspeciales0'=>'Origen racial o Étnico','checkDatosEspeciales1'=>'Salud','checkDatosEspeciales2'=>'Vida sexual','checkDatosIdentificativos0'=>'NIF / DNI','checkDatosIdentificativos1'=>'Nº SS / Mutualidad','checkDatosIdentificativos2'=>'Nombre y apellidos','checkDatosIdentificativos3'=>'Tarjeta sanitaria','checkDatosIdentificativos4'=>'Dirección','checkDatosIdentificativos5'=>'Teléfono','checkDatosIdentificativos6'=>'Firma','checkDatosIdentificativos7'=>'Huella','checkDatosIdentificativos8'=>'Imagen / voz','checkDatosIdentificativos9'=>'Marcas físicas','checkDatosIdentificativos10'=>'Firma electrónica','checkDatosIdentificativos11'=>'Correo electrónico','checkDatosIdentificativos12'=>'Otros datos biométricos','checkDatosIdentificativos13'=>'Otros datos de carácter identificativo');
	$origenes=array('checkOrigen0'=>'El propio interesado o su representante legal','checkOrigen1'=>'Otras personas físicas','checkOrigen2'=>'Fuentes accesibles al público','checkOrigen3'=>'Registros públicos','checkOrigen4'=>'Entidad privada','checkOrigen5'=>'Administraciones Públicas');
	$colectivos=array('checkColectivo0'=>'Empleados','checkColectivo1'=>'Clientes y Usuarios','checkColectivo2'=>'Proveedores','checkColectivo3'=>'Asociados o Miembros','checkColectivo4'=>'Propietarios o Arrendatarios','checkColectivo5'=>'Pacientes','checkColectivo6'=>'Estudiantes','checkColectivo7'=>'Personas de Contacto','checkColectivo8'=>'Padres o Tutores','checkColectivo9'=>'Representante Legal','checkColectivo10'=>'Solicitantes','checkColectivo11'=>'Beneficiarios','checkColectivo12'=>'Cargos Públicos');
	$datosContenido='';
	foreach ($tiposDatos as $key => $value) {
		if($item[$key]!='NO'){
			if($datosContenido!=''){
				$datosContenido.=', ';
			}
			$datosContenido.=$value;
		}
	}
	if($item['otrosTiposDatos']=='SI' && $item['desc_otros_tipos']!=''){
		if($datosContenido!=''){
				$datosContenido.=', ';
			}
		if($item['desc_otros_tipos']==''){
			$datosContenido.='Otros tipos de datos';
		} else {
			$datosContenido.=$item['desc_otros_tipos'];
		}
	}
	$datosOrigenes='';
	foreach ($origenes as $key => $value) {
		if($item[$key]!='NO'){
			if($datosOrigenes!=''){
				$datosOrigenes.=', ';
			}
			$datosOrigenes.=$value;
		}
	}
	$datosColectivos='';
	foreach ($colectivos as $key => $value) {
		if($item[$key]!='NO'){
			if($datosColectivos!=''){
				$datosColectivos.=', ';
			}
			$datosColectivos.=$value;
		}
	}
	if($item['checkColectivo13']!='NO' && $item['desc_otros_tipos']!=''){
		if($datosColectivos!=''){
			$datosColectivos.=', ';
		}
		if($item['otro_col']==''){
			$datosColectivos.='Otro colectivo';
		} else {
			$datosColectivos.=$item['otro_col'];
		}
	}
	$tiposArray=array('checkRecogida'=>'Recogida','checkRegistro'=>'Registro','checkEstructuracion'=>'Estructuración','checkModificacion'=>'Modificación','checkConservacion'=>'Conservación','checkInterconexion'=>'Interconexión','checkConsulta'=>'Consulta','checkCotejo'=>'Cotejo','checkAnalisis'=>'Análisis de conducta','checkComunicacion'=>'Comunicación','checkSupresion'=>'Supresión','checkDestruccion'=>'Destrucción','checkComunicacion1'=>'Comunicación por transmisión al responsable del fichero','checkComunicacion2'=>'Comunicación a la Administración Pública competente','checkComunicacion3'=>'Comunicación permitida por ley');
	$tipoTratamiento='';
	foreach ($tiposArray as $key => $value) {
		if($item[$key]=='SI'){
			if($tipoTratamiento!=''){
				$tipoTratamiento.=', ';
			}
			$tipoTratamiento.=$value;
		}
	}
	$contenido="<page>
		<div class='titulo'>".$i.".	CONTRATO PARA ACTUAR COMO ENCARGADO DEL TRATAMIENTO.</div>
		<p>De una parte:</p>
		<p><b>* Como responsable del fichero: ".$item['n_razon']."</b></p>
		<p>- Con CIF: ".$item['cif_nif_responsableFichero']."</p>
		<p>- Dirección: ".$item['dir_postal_responsableFichero']."</p>
		<p>- Código Postal: ".$item['postal_responsableFichero']."</p>
		<p>- Localidad: ".$item['localidad_responsableFichero']."</p>		
		<p>- Provincia: ".convertirMinuscula($provinciasAgencia[$item['provincia_responsableFichero']])."</p>
		<p>- Representante: ".$item['representante_responsableFichero']."</p>
		<p>Y de otra parte:</p>
		<p><b>* Como encargado del tratamiento: ".$item['n_razon_encargado']."</b></p>
		<p>- Con CIF/NIF: ".$item['cif_nif_encargado']."</p>
		<p>- Dirección: ".$item['dir_postal_encargado']."</p>
		<p>- Código Postal: ".$item['postal_encargado']."</p>
		<p>- Localidad: ".$item['localidad_encargado']."</p>		
		<p>- Provincia: ".convertirMinuscula($provinciasAgencia[$item['provincia_encargado']])."</p>
		<p>- Representante legal: ".$item['nombre']."</p>
		<p>Ambas partes contratantes se reconocen recíprocamente con capacidad legal necesaria para suscribir, con fecha ".formateaFechaWeb($item['fechaMostrar']).", el presente contrato de encargado del tratamiento, regulado por artículo 28 del RGPD – Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos- y MANIFIESTAN:</p>
			<ul style='list-style:none;'>
			<li>I.	Que  ".$item['n_razon']." -en lo sucesivo responsable del tratamiento- necesita contratar los servicios de la empresa externa ".$item['n_razon_encargado'].".</li>
			<li>II.	Que mediante lo establecido en el presente contrato, se habilita a la entidad ".$item['n_razon_encargado']." -en lo sucesivo encargada del tratamiento- a tratar por cuenta del responsable del tratamiento los datos de carácter personal necesarios para prestar los servicios contratados, concretados en la cláusula primera del presente documento.</li>
			</ul>				
		<h1>CLÁUSULAS:</h1>
		<p><b>PRIMERA. Objeto del encargo del tratamiento.</b></p>
		<p>El encargado del tratamiento tratará los datos de carácter personal del responsable del tratamiento que resulten necesarios para prestar el servicio siguiente: ".$servicio.".</p>
		<p>El encargado del tratamiento tratará los datos en los ".$sitiosTexto."</p>
		<p>Concreción de los tratamientos a realizar: ".$tipoTratamiento."</p>
		<p><b>SEGUNDA. Identificación de la información afectada.</b></p>
		<p>Para la ejecución de las prestaciones derivadas del cumplimiento del objeto de este encargo, el responsable del tratamiento pone a disposición del encargado del tratamiento, la siguiente información sobre los datos personales que va a tratar:</p>
		<p>* El fichero que contiene los datos es: ".$item['nombreFichero'].".</p>
		<p>* Su nivel de seguridad es ".$niveles[$item['nivel']].".</p>
		<p>* Los datos que contiene son los siguientes: ".$datosContenido."</p>
		<p>* La categoría de los interesados es '".$datosColectivos."'</p>
		<p>* Y su origen o procedencia es '".$datosOrigenes."'</p>		
		<p><b>TERCERA. Duración.</b></p>	
		<p>El presente acuerdo se inicia a partir de la fecha de firma del presente documento, finalizando el día ".formateaFechaWeb($item['fechaMostrarBaja']).".</p>
		<p>Una vez finalice el presente contrato, el encargado del tratamiento debe devolver al responsable los datos personales y suprimir cualquier copia que esté en su poder.</p>
		<p><b>CUARTA. Obligaciones del encargado del tratamiento.</b></p>
		<p>El encargado del tratamiento y todo su personal se obliga a:</p>
			<ul style='list-style:none;'>
			<li>a)	Utilizar los datos personales objeto de tratamiento, o los que recoja para su inclusión, sólo para la finalidad objeto de este encargo. En ningún caso podrá utilizar los datos para fines propios.</li>
			<li>b)	Tratar los datos de acuerdo con las instrucciones del responsable del tratamiento.Si el encargado del tratamiento considera que aluna de las instrucciones infringe el RGPD o cualquier otra disposición en materia de protección de datos de la Unión o de los Estados miembros, el encargado informará inmediatamente al responsable.</li>
			<li>c)	Con la información facilitada por el responsable del fichero en la CLÁUSULA SEGUNDA del presente documento, el encargado del tratamiento debe proceder a cumplimentar aquellos registros que contempla el documento de seguridad que está obligado a mantener actualizado el encargado del tratamiento.<br/>".$localesEncargado."</li>
			<li>d)	No comunicar los datos a terceras personas, salvo que cuente con la autorización expresa del responsable del tratamiento, en los supuestos legalmente admisibles.</li>";
			$subcontratacion='';
			if($item['subcontratacion_encargado']=='SI'){
				$listado=consultaBD('SELECT * FROM declaraciones_subcontrata WHERE tipo="'.$item['tipoEncargado'].'" AND codigoResponsable='.$item['codigoEncargado'].' AND codigoTrabajo='.$datos['codigoTrabajo'],true);
				$campos=array('nif'=>'CIF/NIF','direccion'=>'Direccion','cp'=>'CP','localidad'=>'Localidad','provincia'=>'Provincia','servicio'=>'Servicio');
				while($d=mysql_fetch_assoc($listado)){
					if($subcontratacion!=''){
						$subcontratacion.='; ';
					}
					$subcontratacion.=$d['empresa'];
					foreach ($campos as $campo => $value) {
						if($d[$campo]!=''){
							if($campo=='provincia'){
								$subcontratacion.=', '.$value.' '.convertirMinuscula($provinciasAgencia[$d[$campo]]);
							} else {
								$subcontratacion.=', '.$value.' '.$d[$campo];
							}
						}
					}
				}
			}	
			if($subcontratacion!=''){
				$contenido.="<li>e)	Sí está permitida la subcontratación, con las siguientes empresas: ".$subcontratacion."</li>";
			} else {
				$contenido.="<li>e)	No está permitida la subcontratación.</li>";
			}
			$contenido.="<li>f)	Mantener el deber de secreto respecto a los datos de carácter personal a los que haya tenido acceso en virtud del presente encargo, incluso después de que finalice su objeto.</li>
			<li>g)	Garantizar que las personas autorizadas para tratar datos personales se comprometan, de forma expresa y por escrito, a respetar la confidencialidad y a cumplir las medidas de seguridad correspondientes, de las que hay que informarles convenientemente.</li>
			<li>h)	Mantener a disposición del responsable la documentación acreditativa del cumplimiento de la obligación establecida en el apartado anterior.</li>
			<li>i)	Garantizar la formación necesaria en materia de protección de datos personales de las personas autorizadas para tratar datos personales.</li>
			<li>j)	Asistir al responsable del tratamiento en la respuesta al ejercicio de los derechos de:<br/>-	Acceso, rectificación, supresión y oposición.<br/>-	Limitación del tratamiento.<br/>-	Portabilidad de datos.<br/>-	A no ser objeto de decisiones individualizadas automatizadas (incluida la elaboración de perfiles).<br/><br/>Cuando las personas afectadas ejerzan los derechos de acceso, rectificación, supresión y oposición, limitación del tratamiento, portabilidad de datos y a no ser objeto de decisiones individualizadas automatizadas, ante el encargado del tratamiento, éste debe comunicarlo por correo electrónico a la dirección ".$item['email_responsableFichero'].". La comunicación debe hacerse de forma inmediata y en ningún caso más allá del día laborable siguiente al de la recepción de la solicitud, juntamente, en su caso, con otras informaciones que puedan ser relevantes para resolver la solicitud.</li>
			<li>k)	Registrando aquellas rectificaciones o exclusiones (por cancelación u oposición) que el responsable le comunique como consecuencia de peticiones de aquellos interesados cuyos datos esté tratando.</li>
			<li>l)	Derecho de información. El encargado del tratamiento, en el momento de la recogida de los datos, debe facilitar por escrito al interesado la información relativa a los tratamientos de datos que se van a realizar.</li>
			<li>m)	El encargado del tratamiento notificará al responsable del tratamiento, sin dilación indebida y en cualquier caso antes de que transcurran 72 horas, las violaciones de la seguridad de los datos personales a su cargo de las que tenga conocimiento, juntamente  con toda la información relevante para la documentación y comunicación de la incidencia.<br/>Se facilitará como mínimo, la información siguiente en el supuesto que se disponga de ella:<br/>-	Descripción de la naturaleza de la violación de la seguridad de los datos personales, inclusive, cuando sea posible, las categorías y el número aproximado de interesados afectados, y las categorías y el número aproximado de interesados afectados, y las categorías y el número aproximado de registros de datos personales afectados.<br/>-	Descripción de las posibles consecuencias o propuestas para poner remedio a la violación de la seguridad de los datos personales, incluyendo, si procede, las medidas adoptadas para mitigar los posibles efectos negativos.<br/>-	Si no es posible facilitar la información simultáneamente, y en la medida en que no lo sea, la información se facilitará de manera gradual sin dilación indebida.</li>
			<li>n)	Poner a disposición del responsable toda la información necesaria para demostrar el cumplimiento de sus obligaciones, así como para la realización de las auditorías o las inspecciones que realicen el responsable y otro auditor autorizado por él.</li>
			<li>o)	Cumplir todos los requisitos exigidos por el RGPD y, en todo caso, implantar mecanismos para:<br/>-	Garantizar la confidencialidad, integridad y disponibilidad de los sistemas y servicios de tratamiento.<br/>-	Restaurar la disponibilidad y el acceso a los datos personales de forma rápida, en caso de incidente físico o técnico.<br/>-	Verificar, evaluar y valorar, de forma regular, la eficacia de las medidas técnicas y organizativas implantadas para garantizar la seguridad del tratamiento.<br/>-	Cifrar los datos personales, en su caso.</li>
			<li>p)	Devolver al responsable del tratamiento los datos de carácter personal y, si procede, los soportes donde consten, una vez cumplida la prestación.<br/>La devolución debe comportar el borrado total de los datos existentes en los equipos informáticos utilizados por el encargado.<br/>No obstante, el encargado puede conservar una copia, con los datos debidamente bloqueados, mientras puedan derivarse responsabilidades de la ejecución de la prestación.</li>
			</ul>	
		<p><b>QUINTA. Obligaciones del responsable del tratamiento.</b></p>
		<p>Corresponde al responsable del tratamiento:</p>
			<ul style='list-style:none;'>
			<li>a)	Entregar al encargado los datos necesarios para la prestación del servicio a los que se refiere la CLÁUSULA PRIMERA del presente documento.</li>
			<li>b)	Supervisar el tratamiento, incluida la realización de inspecciones y auditorías.</li>
			</ul>
		<p>En prueba de conformidad con todas sus partes, firman el presente contrato el ".formateaFechaWeb($item['fechaMostrar']).".</p>
		<table id='tablaFirmas'>
			<tr>
				<td><b>Como responsable del fichero</b></td>
				<td><b>Como encargado del tratamiento</b></td>			
			</tr>
		</table>
		<table id='tablaFirmas'>
			<tr>
				<td><b>D./ª. ".$item['representante_responsableFichero']."</b></td>
				<td><b>D./ª. ".$item['nombre']."</b></td>			
			</tr>
			<tr>
				<td>REPRESENTANTE DEL RESPONSABLE</td>
				<td>REPRESENTANTE DEL ENCARGADO</td>
			</tr>
		</table>
			<page_footer>

			</page_footer>
		</page>";

		return $contenido;
}

function formateaFechaWord($fecha){
	if($fecha!='' && strpos($fecha, '/')==false){
		$fecha=formateaFechaWeb($fecha);
	}
	return $fecha;
}
function anexoIIILOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_III_REGISTRO_DE_USUARIOS.docx';
	$tipoAcceso=array('X'=>'Acceso total','W'=>'Creación/Modificación/Borrado','R'=>'Sólo lectura','S'=>'Sólo a las dependencias','B'=>'Solo a datos básicos','N'=>'Ninguno');
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.ficheroLogo, clientes.cp, clientes.localidad, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",fecha());
	reemplazarLogo($documento,$datos,'image1.png');

	$tablaUsuarios=fechaTabla(date("d/m/Y"),'USUARIOS');
	$tablaUsuarios.='<w:p w14:paraId="1060A2A2" w14:textId="7A44C6D1" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="00397BF1" w:rsidP="0016753F"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"></w:t></w:r><w:r w:rsidR="00D303CF"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t></w:t></w:r></w:p>';
	$tablaUsuarios.='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="976"/><w:gridCol w:w="2404"/><w:gridCol w:w="2536"/><w:gridCol w:w="2684"/><w:gridCol w:w="2201"/><w:gridCol w:w="2594"/></w:tblGrid><w:tr w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w14:paraId="107AC35C" w14:textId="77777777" w:rsidTr="00F50CD3"><w:tc><w:tcPr><w:tcW w:w="976" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3BF96EC5" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2404" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="6B1B3CD4" w14:textId="7A3FE6B6" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>NOMBRE DE USUARIO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2536" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="0EF972E0" w14:textId="3551C889" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>TIPO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2684" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="12B8B78C" w14:textId="5F2CB27E" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>ACCESO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2201" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="0CBC6A37" w14:textId="08E1F012" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA ALTA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2594" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="536623DD" w14:textId="0EEC19D0" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00F50CD3"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA BAJA</w:t></w:r></w:p></w:tc></w:tr>';

	$consulta=consultaBD("SELECT * FROM usuarios_lopd WHERE codigoTrabajo='$codigo';",true);
	$i=1;
	while($usuariosAccedenFichero=mysql_fetch_assoc($consulta)){
		$ref=$i<10?'0'.$i:$i;
		if($usuariosAccedenFichero['accesoUsuarioLOPD']=='S'){
			$tipoPropio='Usuario propio sin acceso';
		}else{
			$tipoPropio='Usuario propio con acceso';
		}

  		$tablaUsuarios.='<w:tr w:rsidR="00F50CD3" w14:paraId="712C6CAC" w14:textId="77777777" w:rsidTr="00F50CD3"><w:tc><w:tcPr><w:tcW w:w="976" w:type="dxa"/></w:tcPr><w:p w14:paraId="50A54D61" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2404" w:type="dxa"/></w:tcPr><w:p w14:paraId="70965B7A" w14:textId="741C0BA7" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.sanearCaracteres($usuariosAccedenFichero['nombreUsuarioLOPD']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2536" w:type="dxa"/></w:tcPr><w:p w14:paraId="53403DF9" w14:textId="2E0C2485" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipoPropio.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2684" w:type="dxa"/></w:tcPr><w:p w14:paraId="62964D9A" w14:textId="4E3AB708" w:rsidR="00F50CD3" w:rsidRPr="002B0C55" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipoAcceso[$usuariosAccedenFichero['accesoUsuarioLOPD']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2201" w:type="dxa"/></w:tcPr><w:p w14:paraId="3FF5C70D" w14:textId="12C80E55" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWord($usuariosAccedenFichero['fechaAltaUsuarioLOPD']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2594" w:type="dxa"/></w:tcPr><w:p w14:paraId="3B2BE61C" w14:textId="2043E17F" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWord($usuariosAccedenFichero['fechaBajaUsuarioLOPD']).'</w:t></w:r></w:p><w:p w14:paraId="1E279FDC" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

		$i++;			
	}

	$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
	$campos=array(158=>array(159,589,574,543,544),168=>array(169,590,575,545,546),178=>array(179,591,576,547,548),189=>array(190,592,577,549,550),200=>array(201,593,578,551,552),210=>array(211,594,579,553,554),220=>array(221,595,609,555,556),230=>array(231,596,580,557,558),240=>array(241,597,581,559,560),250=>array(251,598,582,561,562),260=>array(261,599,583,563,564),270=>array(271,600,584,565,566),280=>array(281,601,585,567,568),290=>array(291,602,586,569,570),487=>array(488,603,587,572,544));
	foreach ($encargados as $key => $value) {
		if($formulario['pregunta'.$value]=='SI'){
			$ref=$i<10?'0'.$i:$i;
			if($formulario['pregunta'.$campos[$value][2]]=='' || $formulario['pregunta'.$campos[$value][2]]=='NULL'){
				$tipoPropio='Usuario ajeno sin acceso';
			}else{
				$tipoPropio='Usuario ajeno con acceso';
			}

			$tablaUsuarios.='<w:tr w:rsidR="00F50CD3" w14:paraId="712C6CAC" w14:textId="77777777" w:rsidTr="00F50CD3"><w:tc><w:tcPr><w:tcW w:w="976" w:type="dxa"/></w:tcPr><w:p w14:paraId="50A54D61" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2404" w:type="dxa"/></w:tcPr><w:p w14:paraId="70965B7A" w14:textId="741C0BA7" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.sanearCaracteres($formulario['pregunta'.$campos[$value][0]]).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2536" w:type="dxa"/></w:tcPr><w:p w14:paraId="53403DF9" w14:textId="2E0C2485" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipoPropio.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2684" w:type="dxa"/></w:tcPr><w:p w14:paraId="62964D9A" w14:textId="4E3AB708" w:rsidR="00F50CD3" w:rsidRPr="002B0C55" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipoAcceso[$formulario['pregunta'.$campos[$value][1]]].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2201" w:type="dxa"/></w:tcPr><w:p w14:paraId="3FF5C70D" w14:textId="12C80E55" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$formulario['pregunta'.$campos[$value][3]].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2594" w:type="dxa"/></w:tcPr><w:p w14:paraId="3B2BE61C" w14:textId="2043E17F" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$formulario['pregunta'.$campos[$value][4]].'</w:t></w:r></w:p><w:p w14:paraId="1E279FDC" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

			$i++;
		}		
	}

	$encargados=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$codigo.' AND cifEncargado!="'.$formulario['pregunta9'].'"',true);
	while($value=mysql_fetch_assoc($encargados)){
		if($value['nombreEncargado']!=''){
			$ref=$i<10?'0'.$i:$i;
			if($value['ficheroEncargado']=='' || $value['ficheroEncargado']=='NULL' || $value['ficheroEncargado']=='N'){
				$tipoPropio='Usuario ajeno sin acceso';
			}else{
				$tipoPropio='Usuario ajeno con acceso';
			}

			$tablaUsuarios.='<w:tr w:rsidR="00F50CD3" w14:paraId="712C6CAC" w14:textId="77777777" w:rsidTr="00F50CD3"><w:tc><w:tcPr><w:tcW w:w="976" w:type="dxa"/></w:tcPr><w:p w14:paraId="50A54D61" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2404" w:type="dxa"/></w:tcPr><w:p w14:paraId="70965B7A" w14:textId="741C0BA7" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.sanearCaracteres($value['nombreEncargado']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2536" w:type="dxa"/></w:tcPr><w:p w14:paraId="53403DF9" w14:textId="2E0C2485" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipoPropio.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2684" w:type="dxa"/></w:tcPr><w:p w14:paraId="62964D9A" w14:textId="4E3AB708" w:rsidR="00F50CD3" w:rsidRPr="002B0C55" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipoAcceso[$value['accesosEncargado']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2201" w:type="dxa"/></w:tcPr><w:p w14:paraId="3FF5C70D" w14:textId="12C80E55" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($value['fechaAltaEncargado']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2594" w:type="dxa"/></w:tcPr><w:p w14:paraId="3B2BE61C" w14:textId="2043E17F" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($value['fechaBajaEncargado']).'</w:t></w:r></w:p><w:p w14:paraId="1E279FDC" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

			$i++;
		}		
	}
	$total=$i-1;
	$tablaUsuarios.='</w:tbl>'.pieTabla($total);

	
	$documento->setValue("tabla",utf8_decode($tablaUsuarios)); 

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoIII2LOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_III2_PROVEEDORES_SIN_ACCESO_A_DATOS_Y_CON_ACCESO_A_LAS_DEPENDENCIAS.docx';
	$tipoAcceso=array('X'=>'Acceso total','W'=>'Creación/Modificación/Borrado','R'=>'Sólo lectura','S'=>'Sólo a las dependencias','B'=>'Solo a datos básicos','N'=>'Ninguno');
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.ficheroLogo, clientes.cp, clientes.localidad, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",fecha());
	reemplazarLogo($documento,$datos,'image1.png');

	$tablaUsuarios=fechaTabla(date("d/m/Y"),'PROVEEDORES SIN ACCESO A DATOS Y CON ACCESO A LAS DEPENDENCIAS');
	$tablaUsuarios.='<w:p w14:paraId="1060A2A2" w14:textId="7A44C6D1" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="00397BF1" w:rsidP="0016753F"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"></w:t></w:r><w:r w:rsidR="00D303CF"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t></w:t></w:r></w:p>';
	$tablaUsuarios.='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="976"/><w:gridCol w:w="2404"/><w:gridCol w:w="2536"/><w:gridCol w:w="2684"/><w:gridCol w:w="2201"/><w:gridCol w:w="2594"/></w:tblGrid><w:tr w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w14:paraId="107AC35C" w14:textId="77777777" w:rsidTr="00F50CD3"><w:tc><w:tcPr><w:tcW w:w="976" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3BF96EC5" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2404" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="6B1B3CD4" w14:textId="7A3FE6B6" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>NOMBRE O DENOMINACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2536" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="0EF972E0" w14:textId="3551C889" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>NIF</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2684" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="12B8B78C" w14:textId="5F2CB27E" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>SERVICIO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2201" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="0CBC6A37" w14:textId="08E1F012" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA ALTA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2594" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="536623DD" w14:textId="0EEC19D0" w:rsidR="00F50CD3" w:rsidRPr="008F5EA5" w:rsidRDefault="00F50CD3" w:rsidP="00F50CD3"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA BAJA</w:t></w:r></w:p></w:tc></w:tr>';

	$consulta=consultaBD("SELECT * FROM proveedores_lopd WHERE codigoTrabajo='$codigo';",true);
	$i=1;
	while($usuariosAccedenFichero=mysql_fetch_assoc($consulta)){
		$ref=$i<10?'0'.$i:$i;
  		$tablaUsuarios.='<w:tr w:rsidR="00F50CD3" w14:paraId="712C6CAC" w14:textId="77777777" w:rsidTr="00F50CD3"><w:tc><w:tcPr><w:tcW w:w="976" w:type="dxa"/></w:tcPr><w:p w14:paraId="50A54D61" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2404" w:type="dxa"/></w:tcPr><w:p w14:paraId="70965B7A" w14:textId="741C0BA7" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.sanearCaracteres($usuariosAccedenFichero['nombre']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2536" w:type="dxa"/></w:tcPr><w:p w14:paraId="53403DF9" w14:textId="2E0C2485" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$usuariosAccedenFichero['nif'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2684" w:type="dxa"/></w:tcPr><w:p w14:paraId="62964D9A" w14:textId="4E3AB708" w:rsidR="00F50CD3" w:rsidRPr="002B0C55" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$usuariosAccedenFichero['servicio'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2201" w:type="dxa"/></w:tcPr><w:p w14:paraId="3FF5C70D" w14:textId="12C80E55" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWord($usuariosAccedenFichero['fechaAlta']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2594" w:type="dxa"/></w:tcPr><w:p w14:paraId="3B2BE61C" w14:textId="2043E17F" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWord($usuariosAccedenFichero['fechaBaja']).'</w:t></w:r></w:p><w:p w14:paraId="1E279FDC" w14:textId="77777777" w:rsidR="00F50CD3" w:rsidRDefault="00F50CD3" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

		$i++;			
	}

	
	$total=$i-1;
	$tablaUsuarios.='</w:tbl>'.pieTabla($total);

	
	$documento->setValue("tabla",utf8_decode($tablaUsuarios)); 

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoVLOPD($codigo, $PHPWord, $nombreFichero='', $conexion = true){
	
	global $_CONFIG;
	
	$fichero = 'ANEXO_V_ENTRADAS_Y_SALIDAS_PERIÓDICAS.docx';
	
	$tipo = array(
		'ENTRADASALIDA' => 'Salida y Entrada',
		'ENTRADA'       => 'Entrada',
		'SALIDA'        =>'Salida'
	);
	
	$frecuencia = array(
		''  => '',
		'1' => 'Semanal',
		'2' => 'Mensual',
		'3' => 'Anual'
	);
	
	$sql = "SELECT 
				c.*, 
				sf.referencia AS familia, 
				t.formulario 
			FROM trabajos t 
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo 
			WHERE t.codigo = ".$codigo.";";
	
	$datos      = consultaBD($sql, $conexion, true);	
	$formulario = recogerFormularioServicios($datos);
	$datos      = datosPersonales($datos,$formulario);
	
	$documento = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
		
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",utf8_decode(date("d/m/Y")));
	
	reemplazarLogo($documento, $datos, 'image2.png');	

	$tablaEntradasP = fechaTabla(date("d/m/Y"),'ENTRADAS Y SALIDAS PERIÓDICAS');

	$tablaEntradasP .= '<w:tbl><w:tblPr><w:tblW w:w="13716" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="798"/><w:gridCol w:w="4697"/><w:gridCol w:w="1701"/><w:gridCol w:w="2268"/><w:gridCol w:w="4252"/></w:tblGrid><w:tr w:rsidR="00F44EB4" w:rsidRPr="008F5EA5" w14:paraId="4FF7F3F8" w14:textId="77777777" w:rsidTr="00F44EB4"><w:tc><w:tcPr><w:tcW w:w="798" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="0B3FED2D" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4697" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2E7A904B" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>USUARIO AUTORIZADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2EFE68E2" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>TIPO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="710A858F" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA 1º OPERACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4252" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3BB2A886" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc></w:tr>';

	$i = 1;	
	
	$entradasP = consultaBD("SELECT * FROM entradas_periodicas WHERE codigoCliente='".$datos['codigo']."';", $conexion);
	while($entrada = mysql_fetch_assoc($entradasP)) {
		
		$ref = $i < 10 ? '0'.$i : $i;
		
		$tablaEntradasP .= '<w:tr w:rsidR="00304DA2" w14:paraId="74D4D6F1" w14:textId="77777777" w:rsidTr="00304DA2"><w:trPr><w:trHeight w:val="382"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="798" w:type="dxa"/></w:tcPr><w:p w14:paraId="59B5D326" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2669" w:type="dxa"/></w:tcPr><w:p w14:paraId="2F7249DF" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['usuarioAutorizado'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/></w:tcPr><w:p w14:paraId="4E88CD86" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipo[$entrada['tipo']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1417" w:type="dxa"/></w:tcPr><w:p w14:paraId="71415B87" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="002B0C55" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($entrada['fechaPrimeraOperacion']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2047" w:type="dxa"/></w:tcPr><w:p w14:paraId="73613FAD" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['soporte'].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';

		$i++;		
	}	
	
	$total = $i - 1;
	$tablaEntradasP .= '</w:tbl>'.pieTabla($total);

	$tablaTexto = '';
	
	$j = 1;
	
	$entradasAux = consultaBD("SELECT * FROM entradas_periodicas WHERE codigoCliente='".$datos['codigo']."';", $conexion);
	while ($entradasP = mysql_fetch_assoc($entradasAux)) {
		
		$ref = str_pad($j, 4, '0', STR_PAD_LEFT);
		
		$tablaTexto .= '<w:p w:rsidR="00F917B3" w:rsidRPr="002539ED" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="002539ED"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">REFERENCIA: </w:t></w:r><w:r w:rsidRPr="002539ED"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">TIPO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$tipo[$entradasP['tipo']].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">FRECUENCIA: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$frecuencia[$entradasP['frecuencia']].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">FECHA 1ª OPERACIÓN:  </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaPrimeraOperacion']).'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">USUARIO AUTORIZADO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['usuarioAutorizado'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">SOPORTE FÍSICO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['soporte'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">Nº UNIDADES: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['numUnidades'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">CRITERIO / TRATAMIENTO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['criterio'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">EMISOR: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['emisor'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">DESTINATARIO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['destinatario'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">MOTIVO DE LA OPERACIÓN: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">'.$entradasP['motivoOperacion'].' </w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">CESIÓN DE DATOS: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['cesionDatos'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">DEVOLUCIÓN OBLIGATORIA: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['devolucion'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">FECHA DE DEVOLUCIÓN: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaDevolucion']).'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F22FB9" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">SEGURIDADES: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['seguridades'].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:sectPr w:rsidR="00F22FB9"><w:pgSz w:w="11906" w:h="16838"/><w:pgMar w:top="1417" w:right="1701" w:bottom="1417" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/><w:cols w:space="708"/><w:docGrid w:linePitch="360"/></w:sectPr>';

		$j++;
	}


	$documento->setValue("tablaEntradas",utf8_decode($tablaEntradasP));
	$documento->setValue("tablaTexto",utf8_decode($tablaTexto));	

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoVILOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_VI_ENTRADAS_Y_SALIDAS_NO_PERIODICAS.docx';
	$tipo=array('ENTRADASALIDA'=>'Salida y Entrada','ENTRADA'=>'Entrada','SALIDA'=>'Salida');
	$datos=consultaBD("SELECT clientes.codigo, clientes.razonSocial, clientes.domicilio, clientes.ficheroLogo, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("true","");
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",utf8_decode(date("d/m/Y")));

	reemplazarLogo($documento,$datos,'image2.png');	

	$tablaEntradasP=fechaTabla(date("d/m/Y"),'ENTRADAS Y SALIDAS NO PERIÓDICAS');

	$tablaEntradasP.='<w:tbl><w:tblPr><w:tblW w:w="13716" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="798"/><w:gridCol w:w="4697"/><w:gridCol w:w="1701"/><w:gridCol w:w="2268"/><w:gridCol w:w="4252"/></w:tblGrid><w:tr w:rsidR="00F44EB4" w:rsidRPr="008F5EA5" w14:paraId="4FF7F3F8" w14:textId="77777777" w:rsidTr="00F44EB4"><w:tc><w:tcPr><w:tcW w:w="798" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="0B3FED2D" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4697" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2E7A904B" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>USUARIO AUTORIZADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2EFE68E2" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>TIPO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="710A858F" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA 1º OPERACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4252" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3BB2A886" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc></w:tr>';

	$i=1;	
	$entradasP=consultaBD("SELECT * FROM entradas_no_periodicas WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entrada=mysql_fetch_assoc($entradasP)){
		$ref=$i<10?'0'.$i:$i;
		$tablaEntradasP.='<w:tr w:rsidR="00304DA2" w14:paraId="74D4D6F1" w14:textId="77777777" w:rsidTr="00304DA2"><w:trPr><w:trHeight w:val="382"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="798" w:type="dxa"/></w:tcPr><w:p w14:paraId="59B5D326" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2669" w:type="dxa"/></w:tcPr><w:p w14:paraId="2F7249DF" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['usuarioAutorizado'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/></w:tcPr><w:p w14:paraId="4E88CD86" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipo[$entrada['tipo']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1417" w:type="dxa"/></w:tcPr><w:p w14:paraId="71415B87" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="002B0C55" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($entrada['fechaPrimeraOperacion']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2047" w:type="dxa"/></w:tcPr><w:p w14:paraId="73613FAD" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['soporte'].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';

		$i++;		
	}	
	$total=$i-1;
	$tablaEntradasP.='</w:tbl>'.pieTabla($total);

	$tablaTexto='';
	$j=1;
	$entradasAux=consultaBD("SELECT * FROM entradas_no_periodicas WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entradasP=mysql_fetch_assoc($entradasAux)){
		$ref=str_pad($j,4,'0',STR_PAD_LEFT);
		$tablaTexto.='<w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">Nº OPERACIÓN </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">TIPO </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$tipo[$entradasP['tipo']].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">CRITERIO/TRATAMIENTO </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['criterio'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">FECHA </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaPrimeraOperacion']).'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">SOPORTE </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['soporte'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">Nº UNIDADES </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['numUnidades'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">EMISOR  </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['emisor'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">DESTINATARIO </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['destinatario'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">FORMA DE ENVIO </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['formaEnvio'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">USUARIO AUTORIZADO </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['usuarioAutorizado'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>MOTIVO DE LA OPERACIÓN</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve"> '.$entradasP['motivoOperacion'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">¿CESIÓN DE DATOS? </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['cesionDatos'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">¿DEVOLUCIÓN OBLIGATORIA? </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['devolucion'].'</w:t></w:r></w:p><w:p w:rsidR="007F37A2" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="008B6A56" w:rsidRDefault="007F37A2" w:rsidP="007F37A2"><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>FECHA DE DEVOLUCIÓN</w:t></w:r><w:r w:rsidR="00F653C3"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00F653C3"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaDevolucion']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:sectPr w:rsidR="008B6A56" w:rsidRPr="00F653C3"><w:pgSz w:w="11906" w:h="16838"/><w:pgMar w:top="1417" w:right="1701" w:bottom="1417" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/><w:cols w:space="708"/><w:docGrid w:linePitch="360"/></w:sectPr>';
		$j++;
	}


	$documento->setValue("tablaEntradas",utf8_decode($tablaEntradasP));
	$documento->setValue("tablaTexto",utf8_decode($tablaTexto));
	//$documento->setValue("tablaEntradas","");
	//$documento->setValue("tablaTexto","");	

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoIILOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_II_REGISTRO_DE_SOPORTES.docx';
	$datos=consultaBD("SELECT clientes.codigo, clientes.razonSocial, clientes.ficheroLogo, clientes.domicilio, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue('true',"");
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	reemplazarLogo($documento,$datos,'image1.png');	

	$tablaEntradasP=fechaTabla(date("d/m/Y"),'SOPORTES');

	$tablaEntradasP.='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1139"/><w:gridCol w:w="2618"/><w:gridCol w:w="3330"/><w:gridCol w:w="3220"/><w:gridCol w:w="3088"/></w:tblGrid><w:tr w:rsidR="00841F33" w:rsidRPr="008F5EA5" w14:paraId="517F6DB4" w14:textId="77777777" w:rsidTr="008F5EA5"><w:tc><w:tcPr><w:tcW w:w="1196" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="05333464" w14:textId="77777777" w:rsidR="00397BF1" w:rsidRPr="008F5EA5" w:rsidRDefault="008F5EA5" w:rsidP="00052A6D"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1732" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="186D8B0A" w14:textId="77777777" w:rsidR="00841F33" w:rsidRPr="008F5EA5" w:rsidRDefault="008F5EA5" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1666" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3D7BC6A4" w14:textId="77777777" w:rsidR="00841F33" w:rsidRPr="008F5EA5" w:rsidRDefault="008F5EA5" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>CONTENIDO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2441" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="50804A68" w14:textId="77777777" w:rsidR="00841F33" w:rsidRPr="008F5EA5" w:rsidRDefault="008F5EA5" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>MECANISMOS DE ACCESO AL PUESTO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1685" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="64B366E6" w14:textId="77777777" w:rsidR="00841F33" w:rsidRPr="008F5EA5" w:rsidRDefault="008F5EA5" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>MECANISMO DE ACCESO A LOS RECURSOS</w:t></w:r></w:p></w:tc></w:tr>';

	$i=1;	
	$entradasP=consultaBD("SELECT * FROM soportes_lopd_nueva WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entrada=mysql_fetch_assoc($entradasP)){
		$accesoSoporte=explode('&$&', $entrada['mecanismosAccesoSoporteLOPD']);
		$ref=$i<10?'0'.$i:$i;
		$tablaEntradasP.='<w:tr w:rsidR="00197951" w14:paraId="38080FE0" w14:textId="77777777" w:rsidTr="00197951"><w:tc><w:tcPr><w:tcW w:w="817" w:type="dxa"/></w:tcPr><w:p w14:paraId="20A07EAA" w14:textId="77777777" w:rsidR="00841F33" w:rsidRDefault="00197951" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2693" w:type="dxa"/></w:tcPr><w:p w14:paraId="4828054B" w14:textId="77777777" w:rsidR="00841F33" w:rsidRDefault="00197951" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.sanearCaracteres($entrada['nombreSoporteLOPD']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/></w:tcPr><w:p w14:paraId="425FD74D" w14:textId="0525B729" w:rsidR="00841F33" w:rsidRDefault="00197951" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['contenidoSoporteLOPD'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3402" w:type="dxa"/></w:tcPr><w:p w14:paraId="71D382A1" w14:textId="71C87A63" w:rsidR="00841F33" w:rsidRPr="002B0C55" w:rsidRDefault="00197951" w:rsidP="00197951"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['mecanismosAccesoSoporteLOPD'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3260" w:type="dxa"/></w:tcPr><w:p w14:paraId="5A475CA3" w14:textId="23641363" w:rsidR="00841F33" w:rsidRDefault="00197951" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['mecanismosAccesoRecursosSoporteLOPD'].'</w:t></w:r></w:p><w:p w14:paraId="7EB9C6AE" w14:textId="77777777" w:rsidR="00841F33" w:rsidRDefault="00197951" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr></w:p></w:tc></w:tr>';
		$i++;		
	}	
	$total=$i-1;
	$tablaEntradasP.='</w:tbl>'.pieTabla($total);

	$documento->setValue("tablaEntradas",utf8_decode($tablaEntradasP));

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoVIILOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_VII_REGISTRO_SOPORTES_BLOQUEADOS.docx';
	$datos=consultaBD("SELECT clientes.codigo, clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.ficheroLogo, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",utf8_decode(date("d/m/Y")));

    reemplazarLogo($documento,$datos,'image1.png');	

	$tablaEntradasP=fechaTabla(date('d/m/Y'),'SOPORTES BLOQUEADOS');

	$tablaEntradasP.='<w:tbl><w:tblPr><w:tblW w:w="8897" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1340"/><w:gridCol w:w="3021"/><w:gridCol w:w="1984"/><w:gridCol w:w="2552"/></w:tblGrid><w:tr w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w14:paraId="284F0595" w14:textId="77777777" w:rsidTr="00164EEF"><w:tc><w:tcPr><w:tcW w:w="1340" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="4B0C3F59" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF</w:t></w:r><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>ERENCIA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3021" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2FE71F50" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1984" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="50A9072B" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA RECEPCIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2552" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="7F84E4EB" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA CONTESTACIÓN</w:t></w:r></w:p></w:tc></w:tr>';

	$i=1;	
	$entradasP=consultaBD("SELECT * FROM soportes_cancelados WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entrada=mysql_fetch_assoc($entradasP)){
		$ref=$i<10?'0'.$i:$i;
		$tablaEntradasP.='<w:tr w:rsidR="00164EEF" w14:paraId="2F2C62BB" w14:textId="77777777" w:rsidTr="00164EEF"><w:tc><w:tcPr><w:tcW w:w="1340" w:type="dxa"/></w:tcPr><w:p w14:paraId="7A95E27E" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3021" w:type="dxa"/></w:tcPr><w:p w14:paraId="631EF2E8" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['soporte'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1984" w:type="dxa"/></w:tcPr><w:p w14:paraId="3559A74B" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($entrada['fechaRecepcion']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2552" w:type="dxa"/></w:tcPr><w:p w14:paraId="4B73A922" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="002B0C55" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($entrada['fechaContestacion']).'</w:t></w:r></w:p></w:tc></w:tr>';

		$i++;		
	}	
	$total=$i-1;
	$tablaEntradasP.='</w:tbl>'.pieTabla($total);

	$tablaTexto='';
	$j=1;
	$entradasAux=consultaBD("SELECT * FROM soportes_cancelados WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entradasP=mysql_fetch_assoc($entradasAux)){
		$ref=str_pad($j,4,'0',STR_PAD_LEFT);
		$datosFichero=datosRegistro('declaraciones',$entradasP['codigoFichero']);
		$tablaTexto .= '<w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86"><w:pPr><w:rPr><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:u w:val="single"/></w:rPr><w:t>REFERENCIA:</w:t></w:r><w:r w:rsidRPr="00C35F86"><w:rPr><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">: '.$ref.'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t xml:space="preserve">INTERESADO QUE SOLICITA LA CANCELACIÓN: '.htmlspecialchars($entradasP['interesadoSolicitaCancelacion']).'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t xml:space="preserve">FECHA RECEPCIÓN SOLICUTUD: '.formateaFechaWeb($entradasP['fechaRecepcion']).'</w:t></w:r></w:p><w:p w:rsidR="001F119C" w:rsidRDefault="001F119C" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t>MEDIO RECEPCIÓN: '.$entradasP['medioRecepcion'].'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t xml:space="preserve">FECHA CONTESTACIÓN: '.formateaFechaWeb($entradasP['fechaContestacion']).'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t xml:space="preserve">MEDIO DE CONTESTACIÓN: '.$entradasP['medioContestacion'].'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t xml:space="preserve">FICHERO: '.$datosFichero['nombreFichero'].'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t xml:space="preserve">SOPORTE: '.$entradasP['soporte'].'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t>FECHA BLOQUEO: '.formateaFechaWeb($entradasP['fechaBloqueo']).'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t xml:space="preserve">FECHA BORRADO/DESTRUCCIÓN: '.formateaFechaWeb($entradasP['fechaBorrado']).'</w:t></w:r></w:p><w:p w:rsidR="00C35F86" w:rsidRPr="00C35F86" w:rsidRDefault="00C35F86" w:rsidP="001F119C"><w:pPr><w:ind w:left="708"/></w:pPr><w:r><w:t>FORMA DE RALIZAR BORRADO/DESTRUCCIÓN: '.$entradasP['formaRealizarBorrado'].'</w:t></w:r></w:p>';
		// $tablaTexto.='<w:p w:rsidR="007E394D" w:rsidRPr="00D21EE3" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00D21EE3"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">REFERENCIA  </w:t></w:r><w:r w:rsidRPr="00D21EE3"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/><w:u w:val="single"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p>
		// <w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p>
		// <w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>INTERESADO</w:t></w:r>
		// <w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> QUE SOLICITA LA CANCELACIÓN</w:t></w:r>
		// <w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>:</w:t></w:r>
		// <w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r>
		// <w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.htmlspecialchars($entradasP['interesadoSolicitaCancelacion']).'</w:t></w:r></w:p>
		// <w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>FECHA RECEPCIÓN SOLICITUD</w:t></w:r><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaRecepcion']).'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="00D2466F" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>MEDIO RECEPCIÓN</w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>: '.$entradasP['medioRecepcion'].'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve">FECHA CONTESTACIÓN: </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaContestacion']).'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve">MEDIO CONTESTACIÓN: </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.$entradasP['medioContestacion'].'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> FICHERO: </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'
		// .$datosFichero['nombreFichero'].'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="00D2466F" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve">SOPORTE: </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.$entradasP['soporte'].'</w:t></w:r></w:p>
		// <w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>FECHA BLOQUEO:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaBloqueo']).'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRPr="00D2466F" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>FECHA BORRADO/DESTRUCCIÓN:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaBorrado']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00084966" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve">FORMA DE </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>REALIZAR EL BORRADO/DESTRUCCIÓN</w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> '.$entradasP['formaRealizarBorrado'].'</w:t></w:r></w:p><w:sectPr w:rsidR="00084966"><w:pgSz w:w="11906" w:h="16838"/><w:pgMar w:top="1417" w:right="1701" w:bottom="1417" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/><w:cols w:space="708"/><w:docGrid w:linePitch="360"/></w:sectPr>';
		
		$j++;
	}


	$documento->setValue("tablaEntradas",utf8_decode($tablaEntradasP));
	$documento->setValue("tablaTexto",utf8_decode($tablaTexto));	

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoInformacionCurriculum($codigo, $PHPWord, $nombreFichero=''){
	$fichero='INFORMACION_CURRICULUM.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, clientes.administrador, clientes.nifAdministrador, clientes.telefono, clientes.email, clientes.actividad, servicios_familias.referencia AS familia, formulario, clientes.servicios, ficheroLogo, clientes.cif, clientes.apellido1, clientes.apellido2 FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo=".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	/*$documento->setValue("cliente",utf8_decode($datos['razonSocial']));
	$documento->setValue("nif",utf8_decode($datos['nifAdministrador']));
	$documento->setValue("administrador",utf8_decode($datos['administrador']));
	$documento->setValue("domicilio",utf8_decode($datos['domicilio'].' '.$datos['cp'].' '.$datos['localidad'].' '.$datos['provincia']));
	$documento->setValue("telefono",utf8_decode($datos['telefono']));
	$documento->setValue("email",utf8_decode($datos['email']));
	$documento->setValue("actividad",utf8_decode($datos['actividad']));*/
	//$documento->setValue("datosDPD",utf8_decode($formulario['pregunta605']));
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("true","");	
	$actividad=$datos['actividad']=='OTRA'?$datos['servicios']:$datos['actividad'];
	$info='<w:p w:rsidR="00F771C8" w:rsidRPr="00EC402A" w:rsidRDefault="00F771C8" w:rsidP="00F771C8"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t xml:space="preserve">Responsable del Tratamiento: </w:t></w:r><w:r w:rsidR="003175C7" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r></w:p><w:p w:rsidR="00F771C8" w:rsidRPr="00EC402A" w:rsidRDefault="00F771C8" w:rsidP="00F771C8"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:suppressAutoHyphens w:val="0"/><w:autoSpaceDN/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/><w:jc w:val="both"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t>NIF/CIF</w:t></w:r><w:r w:rsidR="003175C7" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t>: '.$datos['cif'].'</w:t></w:r></w:p><w:p w:rsidR="00F771C8" w:rsidRPr="00EC402A" w:rsidRDefault="00F771C8" w:rsidP="00F771C8"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:suppressAutoHyphens w:val="0"/><w:autoSpaceDN/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/><w:jc w:val="both"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t>Representante legal:</w:t></w:r><w:r w:rsidR="003175C7" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/><w:color w:val="000000" w:themeColor="accent2"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="003175C7" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t>'.$datos['administrador'].' '.$datos['apellido1'].' '.$datos['apellido2'].'</w:t></w:r></w:p>';
	if($formulario['pregunta605']!=''){
	$info.='<w:p w:rsidR="00F771C8" w:rsidRPr="00EC402A" w:rsidRDefault="00F771C8" w:rsidP="00F771C8"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:suppressAutoHyphens w:val="0"/><w:autoSpaceDN/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/><w:jc w:val="both"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t xml:space="preserve">Datos de contacto del Delegado de Protección de Datos: '.$formulario['pregunta605'].' </w:t></w:r><w:r w:rsidR="006229B4" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t></w:t></w:r><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p>';
	}
	$info.='<w:p w:rsidR="00F771C8" w:rsidRPr="00EC402A" w:rsidRDefault="00F771C8" w:rsidP="00F771C8"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:suppressAutoHyphens w:val="0"/><w:autoSpaceDN/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/><w:jc w:val="both"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t xml:space="preserve">Sector o Actividad: </w:t></w:r><w:r w:rsidR="002711C2" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t>'.$actividad.'</w:t></w:r></w:p><w:p w:rsidR="00F771C8" w:rsidRPr="00EC402A" w:rsidRDefault="00F771C8" w:rsidP="00F771C8"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:suppressAutoHyphens w:val="0"/><w:autoSpaceDN/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/><w:jc w:val="both"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t>Domicilio fiscal:</w:t></w:r><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="003175C7" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr><w:t>'.$datos['domicilio'].' C.P. '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')</w:t></w:r></w:p><w:p w:rsidR="00F771C8" w:rsidRPr="00EC402A" w:rsidRDefault="00F771C8" w:rsidP="00F771C8"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:suppressAutoHyphens w:val="0"/><w:autoSpaceDN/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/><w:jc w:val="both"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t xml:space="preserve">Teléfono de contacto: </w:t></w:r><w:r w:rsidR="003175C7" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/><w:bCs/></w:rPr><w:t>'.$datos['telefono'].'</w:t></w:r></w:p><w:p w:rsidR="00F771C8" w:rsidRPr="00EC402A" w:rsidRDefault="00F771C8" w:rsidP="006E6917"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:suppressAutoHyphens w:val="0"/><w:autoSpaceDN/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1"/><w:jc w:val="both"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsia="Times New Roman" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr><w:t>Correo electrónico:</w:t></w:r><w:r w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="003175C7" w:rsidRPr="00EC402A"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorHAnsi"/></w:rPr><w:t>'.$datos['email'].'</w:t></w:r></w:p>';


	$documento->setValue("info",utf8_decode($info));

	reemplazarLogo($documento,$datos);

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoCartelInformativo($codigo, $PHPWord, $nombreFichero=''){
	$fichero='CARTEL_INFORMATIVO.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, clientes.administrador, clientes.nifAdministrador, clientes.telefono, clientes.email, clientes.actividad, servicios_familias.referencia AS familia, formulario, clientes.servicios, ficheroLogo FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo=".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("domicilio",utf8_decode($datos['domicilio'].' C.P. '.$datos['cp'].' '.$datos['localidad'].' '.convertirMinuscula($datos['provincia'])));
	$documento->setValue("email",utf8_decode($datos['email']));
	$datosDPD='';
	if($formulario['pregunta604']!=''){
		$datosDPD='Los datos de contacto del Delegado de Protección de Datos son: '.sanearCaracteres($formulario['pregunta604']);
		if($formulario['pregunta605']!=''){
			$datosDPD.=', '.sanearCaracteres($formulario['pregunta605']);
		}
		if($formulario['pregunta607']!=''){
			$datosDPD.=', Email: '.$formulario['pregunta607'];
		}
		$datosDPD.='. ';
	}
	$documento->setValue("datosDPD",utf8_decode($datosDPD));
	reemplazarLogo($documento,$datos);	
	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoClientesInformacionConsentimiento($codigo, $PHPWord, $nombreFichero=''){

	global $_CONFIG;

	$fichero='CLIENTES_INFORMACION_CONSENTIMIENTO.docx';

	$sql = "SELECT 
				c.codigo as cliente,
				c.razonSocial, 
				c.domicilio, 
				c.cp, 
				c.localidad, 
				c.provincia, 
				c.administrador, 
				c.nifAdministrador, 
				c.telefono, 
				c.email, 
				c.actividad, 
				c.cif, 
				sf.referencia, 
				c.servicios AS familia, 
				t.formulario, 
				c.ficheroLogo 
			FROM 
				trabajos t 
			INNER JOIN clientes c ON t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON s.codigoFamilia = sf.codigo 
			WHERE t.codigo = ".$codigo.";";

	$datos = consultaBD($sql, false, true);		
	$formulario = recogerFormularioServicios($datos);
	$formulario = completarDatosLOPD($formulario, $datos['cliente']);
	$datos = datosPersonales($datos,$formulario);

	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($formulario['pregunta3'])));
	$documento->setValue("nif",utf8_decode($formulario['pregunta14']));

	$nombreRL = $formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}

	$documento->setValue("administrador",utf8_decode($nombreRL));
	$documento->setValue("domicilio",utf8_decode(sanearCaracteres($formulario['pregunta4'].' C.P. '.$formulario['pregunta10'].' '.$formulario['pregunta5'].' ('.$formulario['pregunta11'].')')));
	$documento->setValue("telefono",utf8_decode($formulario['pregunta6']));
	$documento->setValue("email",utf8_decode($formulario['pregunta7']));
	$documento->setValue("actividad",utf8_decode(sanearCaracteres($formulario['pregunta13'])));
	$documento->setValue("cif",utf8_decode($formulario['pregunta9']));
	$documento->setValue("datosDPD",utf8_decode($formulario['pregunta605']));	

	reemplazarLogo($documento,$datos);	

	$info='<w:p w:rsidR="009319E3" w:rsidRPr="00DF3E4F" w:rsidRDefault="002A6BE3" w:rsidP="00EC44A1"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="3"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="C0504D" w:themeColor="accent2"/></w:rPr></w:pPr><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Responsable</w:t></w:r><w:r w:rsidR="00235A80" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> del Tratamiento</w:t></w:r><w:r w:rsidR="009319E3" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="0026149B" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="005405D2" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($formulario['pregunta3']).'</w:t></w:r></w:p><w:p w:rsidR="009319E3" w:rsidRPr="00DF3E4F" w:rsidRDefault="002A6BE3" w:rsidP="009319E3"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="3"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>NIF/</w:t></w:r><w:r w:rsidR="009319E3" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>CIF</w:t></w:r><w:r w:rsidR="009319E3" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/><w:color w:val="C0504D" w:themeColor="accent2"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="0026149B" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/><w:color w:val="C0504D" w:themeColor="accent2"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="005405D2" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$formulario['pregunta9'].'</w:t></w:r></w:p><w:p w:rsidR="0026149B" w:rsidRPr="00DF3E4F" w:rsidRDefault="009319E3" w:rsidP="009319E3"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="3"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Representante legal</w:t></w:r><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="0026149B" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:bCs/><w:color w:val="C0504D" w:themeColor="accent2"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="005405D2" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$nombreRL.'</w:t></w:r></w:p>';
	if($formulario['pregunta605']!=''){
		$info.='<w:p w:rsidR="009319E3" w:rsidRPr="00DF3E4F" w:rsidRDefault="0026149B" w:rsidP="00895477"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="3"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Datos de contacto del Delegado de Protección de Datos (sólo en el supuesto que sea nombrado) </w:t></w:r><w:r w:rsidR="00C9659E" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$formulario['pregunta605'].'</w:t></w:r><w:r w:rsidR="002A6BE3" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p>';
	}
	$info.='<w:p w:rsidR="009319E3" w:rsidRPr="00DF3E4F" w:rsidRDefault="009319E3" w:rsidP="009319E3"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="3"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="C0504D" w:themeColor="accent2"/></w:rPr></w:pPr><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Sector</w:t></w:r><w:r w:rsidR="00C03BF3" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> o Actividad</w:t></w:r><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">: </w:t></w:r><w:r w:rsidR="005405D2" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$formulario['pregunta13'].'</w:t></w:r></w:p><w:p w:rsidR="009319E3" w:rsidRPr="00DF3E4F" w:rsidRDefault="009319E3" w:rsidP="009319E3"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="3"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="C0504D" w:themeColor="accent2"/></w:rPr></w:pPr><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Domicilio fiscal:</w:t></w:r><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="005405D2" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>'.$formulario['pregunta4'].' C.P. '.$formulario['pregunta10'].' '.$formulario['pregunta5'].' ('.$formulario['pregunta11'].')</w:t></w:r></w:p><w:p w:rsidR="009319E3" w:rsidRPr="00DF3E4F" w:rsidRDefault="009319E3" w:rsidP="009319E3"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="3"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="C0504D" w:themeColor="accent2"/></w:rPr></w:pPr><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Teléfono de contacto: </w:t></w:r><w:r w:rsidR="005405D2" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$formulario['pregunta6'].'</w:t></w:r></w:p><w:p w:rsidR="009319E3" w:rsidRPr="00DF3E4F" w:rsidRDefault="009319E3" w:rsidP="009319E3"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="3"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>Correo electrónico</w:t></w:r><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="005405D2" w:rsidRPr="00DF3E4F"><w:rPr><w:rFonts w:cs="Times New Roman"/></w:rPr><w:t>'.$formulario['pregunta7'].'</w:t></w:r></w:p>';
	$documento->setValue("info",utf8_decode($info));
	$info2='.';
	if($datos['referencia']=='PBC1'){
		$info2=' y durante 10 años en cumplimiento de la Ley de Prevención del Blanqueo de Capitales y de la Financiación del Terrorismo. ';
	}
	$documento->setValue("info2",utf8_decode($info2));	
	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoCorreoElectronico($codigo, $PHPWord, $nombreFichero=''){
	$fichero='CORREO_ELECTRONICO.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, clientes.administrador, clientes.nifAdministrador, clientes.telefono, clientes.email, clientes.actividad, clientes.cif, servicios_familias.referencia AS familia, formulario, clientes.servicios, ficheroLogo FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo=".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);

	$datosDPD=datosRegistro('dpd_lopd',$codigo,'codigoTrabajo');

	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$datosDPD='';
	if($formulario['pregunta604']!=''){
		$datosDPD='Los datos de contacto del Delegado de Protección de Datos son: '.$formulario['pregunta604'];
		if($formulario['pregunta605']!=''){
			$datosDPD.=', '.$formulario['pregunta605'];
		}
		if($formulario['pregunta607']!=''){
			$datosDPD.=', Email: '.$formulario['pregunta607'];
		}
		$datosDPD.='. ';
	}	
	$documento->setValue("datosDPD",utf8_decode($datosDPD));
	$documento->setValue("emailDPD",utf8_decode($formulario['pregunta7']));
	$documento->setValue("direccionDPD",utf8_decode($datos['domicilio'].' '.$datos['cp'].' '.$datos['localidad'].' '.convertirMinuscula($datos['provincia'])));	
	reemplazarLogo($documento,$datos);	
	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function zipAnexoI($codigo, $PHPWord, $nombreFichero = '', $conexion = true){
	
	$sql = "SELECT 
				c.*, 
				c.administrador AS representante, 
				sf.referencia AS familia, 
				t.formulario, 
				t.codigoCliente, 
				t.codigo AS codigoTrabajo
			FROM 
				trabajos t
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo 
			WHERE 
				t.codigo = ".$codigo.";";
	
	$datos = consultaBD($sql, $conexion, true);

	$documentos = [];
	$registros  = explode(',', $_POST['registros']);
	$i = 1;

	foreach ($registros as $value) {
		$value = explode('_', $value);

		$fichero = consultaBD('SELECT * FROM declaraciones WHERE codigo = '.$value[0], true, true);
		$fichero['fechaBajaTratamiento']  = $fichero['fechaBaja'];
		$fichero['fechaInicioTratamiento']= $fichero['fechaInicio'];
		$fichero['fechaAltaTratamiento']  = $fichero['fecha'];

		if ($value[1] == 'FIJO'){
			$fichero = preparaEncargado($fichero, $value[2], 'FIJO', $datos, true);
		} 
		else if ($value[1] == 'NOFIJO'){
			$fichero = preparaEncargado($fichero,$value[2], 'NOFIJO', $datos, true);
		} 
		else if ($value[1] == 'SIN'){
			$fichero = preparaEncargado($fichero,false,'SIN',$datos, true);
		}

		$documentos[] = anexoI_X($fichero, $PHPWord, $nombreFichero, $i, $datos, $conexion);
		
		$i++;
	}

	$zip = new ZipArchive();
    $nameZip = '../documentos/consultorias/anexos_I.zip';
    
	if(file_exists($nameZip)){
        unlink($nameZip);
    }
    
	$ficheroZip = $nameZip;
    
	if($zip->open($ficheroZip,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $doc){
            $zip->addFile('../documentos/consultorias/'.$doc , $doc);
        }
    }
    
	if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }
	
	header("Content-Type: application/zip");
    header("Content-Disposition: attachment; filename=anexos_I.zip");
    header("Content-Transfer-Encoding: binary");

    readfile($nameZip);
}

function anexoI($datos, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_I_REGISTRO_DE_ACTIVIDADES_DE_TRATAMIENTO.docx';
	$seguridad=array(1=>'Básico',2=>'Medio',3=>'Alto');
	$sistemaTrata=array(1=>'Automatizado',2=>'Manual',3=>'Mixto');	
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("nifAdministrador",utf8_decode(sanearCaracteres($datos['nifAdministrador'])));
	$documento->setValue("administrador",utf8_decode(sanearCaracteres($datos['administrador'].' '.$datos['apellido1'].' '.$datos['apellido2'])));
	$documento->setValue("domicilio",utf8_decode(sanearCaracteres($datos['domicilio'].' CP:'.$datos['cp'].' - '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')')));
	$documento->setValue("telefono",utf8_decode(sanearCaracteres($datos['telefono'])));
	$documento->setValue("email",utf8_decode(sanearCaracteres($datos['email'])));
	$actividad=$datos['actividad']=='OTRA'?$datos['servicios']:$datos['actividad'];
	$documento->setValue("actividad",utf8_decode(sanearCaracteres($actividad)));
	$documento->setValue("cif",utf8_decode(sanearCaracteres($datos['cif'])));

	$documento->setValue("nombreFichero",utf8_decode(sanearCaracteres($datos['nombreFichero'])));

	$documento->setValue("nombreDPD",utf8_decode(sanearCaracteres($formulario['pregunta604'])));
	$documento->setValue("nifDPD",utf8_decode(sanearCaracteres($formulario['pregunta606'])));
	$documento->setValue("datosDPD",utf8_decode(sanearCaracteres($formulario['pregunta605'])));
	$fecha=date('Y-m-d');
	$documento->setValue("fecha",formateaFechaWeb($fecha));
	$imagen='image2.png';	

     if($_CONFIG['usuarioBD']=='root'){
    	$logo = '../documentos/logos-clientes/imagenDefecto.png';
    } else {
    	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos['ficheroLogo']);
    	if($hayLogo!='NO'){
    		$logo = '../documentos/logos-clientes/'.$datos['ficheroLogo'];
		} else {
			$logo = '../documentos/logos-clientes/imagenDefecto.png';
		}
	}
	$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
	if (!copy($logo, $nuevo_logo)) {
    	echo "Error al copiar $fichero...\n";
	}	
	$documento->replaceImage('../documentos/logos-clientes/',$imagen);	
	$info='<w:p w14:paraId="34D7045B" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Responsable del Tratamiento: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['razonSocial']).'</w:t></w:r></w:p><w:p w14:paraId="37CEFA0A" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">NIF/CIF: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['cif'].'</w:t></w:r></w:p><w:p w14:paraId="1A5B2974" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Representante legal</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="00171AD1" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['administrador'].' '.$datos['apellido1'].' '.$datos['apellido2'].'</w:t></w:r></w:p><w:p w14:paraId="3651F61F" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>NIF/CIF del representante legal:</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$datos['nifAdministrador'].'</w:t></w:r></w:p>';
	if($formulario['pregunta604']!=''){
		$info.='<w:p w14:paraId="015BD8D5" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr><w:t xml:space="preserve">Delegado de Protección de Datos </w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t></w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr><w:t xml:space="preserve">: </w:t></w:r><w:r w:rsidR="006B5DB0" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$formulario['pregunta604'].'</w:t></w:r></w:p><w:p w14:paraId="3B9A277D" w14:textId="77777777" w:rsidR="00587131" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr><w:t xml:space="preserve">NIF/CIF del Delegado de Protección de Datos : </w:t></w:r><w:r w:rsidR="006B5DB0" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$formulario['pregunta606'].'</w:t></w:r></w:p><w:p w14:paraId="23E9C486" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Datos de contacto del Delegado de Protección de Datos: </w:t></w:r><w:r w:rsidR="006B5DB0" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$formulario['pregunta605'].'</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p>';
	}
	$info.='<w:p w14:paraId="5A250D03" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Sector o Actividad: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$actividad.'</w:t></w:r></w:p><w:p w14:paraId="5A711F1A" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Domicilio fiscal:</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$datos['domicilio'].' CP:'.$datos['cp'].' - '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')</w:t></w:r></w:p><w:p w14:paraId="40C321AE" w14:textId="77777777" w:rsidR="00375B74" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Teléfono de contacto: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['telefono'].'</w:t></w:r></w:p><w:p w14:paraId="18EFE602" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>Correo electrónico</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$datos['email'].'</w:t></w:r></w:p>';
	$documento->setValue("info",utf8_decode($info));

	$tablaEntradasP='';

	$tablaEntradasP.='		<w:tbl>
			<w:tblPr>
				<w:tblW w:w="5000" w:type="pct"/>
				<w:tblCellSpacing w:w="6" w:type="dxa"/>
				<w:shd w:val="clear" w:color="auto" w:fill="AAAAAA"/>
				<w:tblCellMar>
					<w:top w:w="24" w:type="dxa"/>
					<w:left w:w="24" w:type="dxa"/>
					<w:bottom w:w="24" w:type="dxa"/>
					<w:right w:w="24" w:type="dxa"/>
				</w:tblCellMar>
				<w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
			</w:tblPr>
			<w:tblGrid>
				<w:gridCol w:w="1484"/>
				<w:gridCol w:w="2244"/>
				<w:gridCol w:w="2179"/>
				<w:gridCol w:w="2669"/>
			</w:tblGrid>
			<w:tr w:rsidR="00CA4720" w:rsidRPr="00472AC1" w:rsidTr="00507EBD">
				<w:trPr>
					<w:trHeight w:val="240"/>
					<w:tblCellSpacing w:w="6" w:type="dxa"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="0" w:type="auto"/>
						<w:shd w:val="clear" w:color="auto" w:fill="CECECE"/>
						<w:vAlign w:val="center"/>
						<w:hideMark/>
					</w:tcPr>
					<w:p w:rsidR="00CA4720" w:rsidRPr="00472AC1" w:rsidRDefault="00CA4720" w:rsidP="00507EBD">
						<w:pPr>
							<w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00472AC1">
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t>REF</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="0" w:type="auto"/>
						<w:shd w:val="clear" w:color="auto" w:fill="CECECE"/>
						<w:vAlign w:val="center"/>
						<w:hideMark/>
					</w:tcPr>
					<w:p w:rsidR="00CA4720" w:rsidRPr="00472AC1" w:rsidRDefault="00CA4720" w:rsidP="00507EBD">
						<w:pPr>
							<w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t>ACTIVIDADES DE TRATAMIENTO –FICHEROS</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="0" w:type="auto"/>
						<w:shd w:val="clear" w:color="auto" w:fill="CECECE"/>
						<w:vAlign w:val="center"/>
						<w:hideMark/>
					</w:tcPr>
					<w:p w:rsidR="00CA4720" w:rsidRPr="00F2567E" w:rsidRDefault="00CA4720" w:rsidP="00507EBD">
						<w:pPr>
							<w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00F2567E">
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t>NIVEL DE SEGURIDAD</w:t>
						</w:r>
						<w:r>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t xml:space="preserve"> </w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="0" w:type="auto"/>
						<w:shd w:val="clear" w:color="auto" w:fill="CECECE"/>
					</w:tcPr>
					<w:p w:rsidR="00CA4720" w:rsidRPr="00F2567E" w:rsidRDefault="00CA4720" w:rsidP="00507EBD">
						<w:pPr>
							<w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
						</w:pPr>
						<w:r>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t>SISTEMA DE TRATAMIENTO</w:t>
						</w:r>
					</w:p>
				</w:tc>
			</w:tr>';

	$i=1;	
	$entradasP=consultaBD("SELECT * FROM declaraciones WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entrada=mysql_fetch_assoc($entradasP)){
		$tablaEntradasP.='			<w:tr w:rsidR="00CA4720" w:rsidRPr="00472AC1" w:rsidTr="00507EBD">
				<w:trPr>
					<w:trHeight w:val="240"/>
					<w:tblCellSpacing w:w="6" w:type="dxa"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="0" w:type="auto"/>
						<w:shd w:val="clear" w:color="auto" w:fill="CECECE"/>
						<w:vAlign w:val="center"/>
						<w:hideMark/>
					</w:tcPr>
					<w:p w:rsidR="00CA4720" w:rsidRPr="00EC2BA8" w:rsidRDefault="00CA4720" w:rsidP="00507EBD">
						<w:pPr>
							<w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000" w:themeColor="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00EC2BA8">
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000" w:themeColor="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t>0'.$i.'</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="0" w:type="auto"/>
						<w:shd w:val="clear" w:color="auto" w:fill="CECECE"/>
						<w:vAlign w:val="center"/>
						<w:hideMark/>
					</w:tcPr>
					<w:p w:rsidR="00CA4720" w:rsidRPr="00884098" w:rsidRDefault="00CA4720" w:rsidP="00507EBD">
						<w:pPr>
							<w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00884098">
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t>'.$entrada['nombreFichero'].'</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="0" w:type="auto"/>
						<w:shd w:val="clear" w:color="auto" w:fill="CECECE"/>
						<w:vAlign w:val="center"/>
						<w:hideMark/>
					</w:tcPr>
					<w:p w:rsidR="00CA4720" w:rsidRPr="00884098" w:rsidRDefault="00CA4720" w:rsidP="00507EBD">
						<w:pPr>
							<w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00884098">
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t>'.$seguridad[$entrada['nivel']].'</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="0" w:type="auto"/>
						<w:shd w:val="clear" w:color="auto" w:fill="CECECE"/>
					</w:tcPr>
					<w:p w:rsidR="00CA4720" w:rsidRPr="00884098" w:rsidRDefault="00CA4720" w:rsidP="00507EBD">
						<w:pPr>
							<w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00884098">
							<w:rPr>
								<w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/>
								<w:caps/>
								<w:color w:val="000000"/>
								<w:sz w:val="22"/>
								<w:szCs w:val="18"/>
							</w:rPr>
							<w:t>'.$sistemaTrata[$entrada['tratamiento']].'</w:t>
						</w:r>
					</w:p>
				</w:tc>
			</w:tr>';

		$i++;		
	}	

	$tablaEntradasP.='</w:tbl>';

	$tablaTexto='';
	$j=1;
	$datosIdentificativos='';
	$destinatarios='';
	$entradasAux=consultaBD("SELECT * FROM declaraciones WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entradasP=mysql_fetch_assoc($entradasAux)){
		$datosIdentificativos=recogeChecks('CI',$entradasP);
		$destinatarios=recogeChecks('CD',$entradasP);
		$origen=recogeChecks('O',$entradasP);
		$colectivos=recogeChecks('C',$entradasP);
		$datosTI=datosRegistro('transferencias_internacionales',$entradasP['codigo'],'codigoDeclaracion');
		$otros=$entradasP['otrosTiposDatos']=='NO'?'NO':$entradasP['desc_otros_tipos'];

		$tablaTexto.='<w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:ind w:left="1440"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="22"/><w:szCs w:val="24"/><w:u w:val="single"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="22"/><w:szCs w:val="24"/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">REFERENCIA 0'.$j.'- '.$entradasP['nombreFichero'].'  -NIVEL DE SEGURIDAD '.$seguridad[$entradasP['nivel']].'-. </w:t></w:r></w:p><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="4F81BD" w:themeColor="accent1"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Base jurídica del tratamiento:</w:t></w:r><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="4F81BD" w:themeColor="accent1"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$entradasP['baseJuridica'].'</w:t></w:r></w:p><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="4F81BD" w:themeColor="accent1"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Tipo de Tratamiento:</w:t></w:r><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="4F81BD" w:themeColor="accent1"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$entradasP['tipoTratamiento'].'</w:t></w:r></w:p><w:p w:rsidR="00BE55AC" w:rsidRPr="0018680A" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Nº de interesados afectados:</w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$entradasP['numInteresados'].'</w:t></w:r></w:p><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve">Finalidad: </w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$entradasP['desc_fin_usos'].'</w:t></w:r></w:p><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Calibri"/><w:sz w:val="22"/></w:rPr><w:t xml:space="preserve">Cuando sea posible, los plazos previstos para la supresión de las diferentes categorías de datos: '.$entradasP['plazo'].'</w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/></w:rPr><w:t></w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w="9750" w:type="dxa"/><w:tblCellSpacing w:w="15" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="3426"/><w:gridCol w:w="6324"/></w:tblGrid><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1750" w:type="pct"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:trHeight w:val="315"/><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr></w:p></w:tc></w:tr></w:tbl><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:vanish/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblCellSpacing w:w="15" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="8149"/><w:gridCol w:w="445"/></w:tblGrid><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:gridSpan w:val="2"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Tipos de datos, estructura y organización del fichero:</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:gridSpan w:val="2"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:noProof/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:drawing><wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="5A345ABA" wp14:editId="25898722"><wp:extent cx="6093460" cy="95250"/><wp:effectExtent l="19050" t="0" r="2540" b="0"/><wp:docPr id="16" name="Imagen 1" descr="http://www.agpd.es/portalwebAGPD/ficheros_inscritos/titularidad_privada/images/lin3.gif"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/></wp:cNvGraphicFramePr><a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:nvPicPr><pic:cNvPr id="0" name="Imagen 1" descr="http://www.agpd.es/portalwebAGPD/ficheros_inscritos/titularidad_privada/images/lin3.gif"/><pic:cNvPicPr><a:picLocks noChangeAspect="1" noChangeArrowheads="1"/></pic:cNvPicPr></pic:nvPicPr><pic:blipFill><a:blip r:embed="rId8" cstate="print"/><a:srcRect/><a:stretch><a:fillRect/></a:stretch></pic:blipFill><pic:spPr bwMode="auto"><a:xfrm><a:off x="0" y="0"/><a:ext cx="6093460" cy="95250"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/><a:ln w="9525"><a:noFill/><a:miter lim="800000"/><a:headEnd/><a:tailEnd/></a:ln></pic:spPr></pic:pic></a:graphicData></a:graphic></wp:inline></w:drawing></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Otros datos de carácter identificativo:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="0018680A" w:rsidP="0018680A"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$datosIdentificativos.'</w:t></w:r><w:r w:rsidR="00BE55AC"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:br/></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Otros tipos de datos:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="0018680A" w:rsidP="0018680A"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$otros.'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Sistema de tratamiento:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="0018680A" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$sistemaTrata[$entradasP['tratamiento']].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:trHeight w:val="315"/><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr></w:p></w:tc></w:tr></w:tbl><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:vanish/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblCellSpacing w:w="15" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="7786"/><w:gridCol w:w="808"/></w:tblGrid><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:gridSpan w:val="2"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Origen y procedencia de los datos:</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:gridSpan w:val="2"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:noProof/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:drawing><wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="11A52918" wp14:editId="08264893"><wp:extent cx="6093460" cy="95250"/><wp:effectExtent l="19050" t="0" r="2540" b="0"/><wp:docPr id="15" name="Imagen 2" descr="http://www.agpd.es/portalwebAGPD/ficheros_inscritos/titularidad_privada/images/lin3.gif"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/></wp:cNvGraphicFramePr><a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:nvPicPr><pic:cNvPr id="0" name="Imagen 2" descr="http://www.agpd.es/portalwebAGPD/ficheros_inscritos/titularidad_privada/images/lin3.gif"/><pic:cNvPicPr><a:picLocks noChangeAspect="1" noChangeArrowheads="1"/></pic:cNvPicPr></pic:nvPicPr><pic:blipFill><a:blip r:embed="rId8" cstate="print"/><a:srcRect/><a:stretch><a:fillRect/></a:stretch></pic:blipFill><pic:spPr bwMode="auto"><a:xfrm><a:off x="0" y="0"/><a:ext cx="6093460" cy="95250"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/><a:ln w="9525"><a:noFill/><a:miter lim="800000"/><a:headEnd/><a:tailEnd/></a:ln></pic:spPr></pic:pic></a:graphicData></a:graphic></wp:inline></w:drawing></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Origen:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="0018680A" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$origen.'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Colectivos:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="0018680A" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>'.$colectivos.'</w:t></w:r></w:p><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="009F64AC"><w:trPr><w:trHeight w:val="315"/><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr></w:p></w:tc></w:tr></w:tbl><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:vanish/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w="9750" w:type="dxa"/><w:tblCellSpacing w:w="15" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="3395"/><w:gridCol w:w="6355"/></w:tblGrid><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:gridSpan w:val="2"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRPr="0018680A" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Cesión o comunicación de datos:</w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t></w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:gridSpan w:val="2"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:noProof/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:drawing><wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="5EA09559" wp14:editId="518E68EC"><wp:extent cx="6093460" cy="95250"/><wp:effectExtent l="19050" t="0" r="2540" b="0"/><wp:docPr id="14" name="Imagen 3" descr="http://www.agpd.es/portalwebAGPD/ficheros_inscritos/titularidad_privada/images/lin3.gif"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/></wp:cNvGraphicFramePr><a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:nvPicPr><pic:cNvPr id="0" name="Imagen 3" descr="http://www.agpd.es/portalwebAGPD/ficheros_inscritos/titularidad_privada/images/lin3.gif"/><pic:cNvPicPr><a:picLocks noChangeAspect="1" noChangeArrowheads="1"/></pic:cNvPicPr></pic:nvPicPr><pic:blipFill><a:blip r:embed="rId8" cstate="print"/><a:srcRect/><a:stretch><a:fillRect/></a:stretch></pic:blipFill><pic:spPr bwMode="auto"><a:xfrm><a:off x="0" y="0"/><a:ext cx="6093460" cy="95250"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/><a:ln w="9525"><a:noFill/><a:miter lim="800000"/><a:headEnd/><a:tailEnd/></a:ln></pic:spPr></pic:pic></a:graphicData></a:graphic></wp:inline></w:drawing></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1734" w:type="pct"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRPr="0018680A" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Destinatarios:</w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="009F64AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$destinatarios.'</w:t></w:r></w:p></w:tc></w:tr></w:tbl><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:vanish/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p><w:tbl><w:tblPr><w:tblW w:w="9750" w:type="dxa"/><w:tblCellSpacing w:w="15" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="3659"/><w:gridCol w:w="6305"/></w:tblGrid><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:gridSpan w:val="2"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRPr="0018680A" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Transferencia internacional:</w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:gridSpan w:val="2"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:noProof/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:drawing><wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="3CC5F7CF" wp14:editId="11C08A62"><wp:extent cx="6093460" cy="95250"/><wp:effectExtent l="19050" t="0" r="2540" b="0"/><wp:docPr id="13" name="Imagen 7" descr="http://www.agpd.es/portalwebAGPD/ficheros_inscritos/titularidad_privada/images/lin3.gif"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/></wp:cNvGraphicFramePr><a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:nvPicPr><pic:cNvPr id="0" name="Imagen 7" descr="http://www.agpd.es/portalwebAGPD/ficheros_inscritos/titularidad_privada/images/lin3.gif"/><pic:cNvPicPr><a:picLocks noChangeAspect="1" noChangeArrowheads="1"/></pic:cNvPicPr></pic:nvPicPr><pic:blipFill><a:blip r:embed="rId8" cstate="print"/><a:srcRect/><a:stretch><a:fillRect/></a:stretch></pic:blipFill><pic:spPr bwMode="auto"><a:xfrm><a:off x="0" y="0"/><a:ext cx="6093460" cy="95250"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom><a:noFill/><a:ln w="9525"><a:noFill/><a:miter lim="800000"/><a:headEnd/><a:tailEnd/></a:ln></pic:spPr></pic:pic></a:graphicData></a:graphic></wp:inline></w:drawing></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="750" w:type="pct"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRPr="0018680A" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve">Destinatarios en países con </w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:br/><w:t>nivel de protección adecuado:</w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="009F64AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$datosTI['destNSadecuado'].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1750" w:type="pct"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRPr="0018680A" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Destinatarios en países sin</w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:br/><w:t>nivel de protección adecuado:</w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="009F64AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$datosTI['destNSinadecuado'].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1750" w:type="pct"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRPr="0018680A" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Otros destinatarios de</w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:br/><w:t>Transferencias Internacionales:</w:t></w:r><w:r w:rsidR="0018680A"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="009F64AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$datosTI['otrosDestTI'].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00BE55AC" w:rsidTr="00CE18C2"><w:trPr><w:tblCellSpacing w:w="15" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1750" w:type="pct"/><w:noWrap/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:t>Transferencias internacionales con</w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="22"/><w:szCs w:val="24"/></w:rPr><w:br/><w:t>autorización del Director de la AEPD:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:tcMar><w:top w:w="15" w:type="dxa"/><w:left w:w="15" w:type="dxa"/><w:bottom w:w="15" w:type="dxa"/><w:right w:w="15" w:type="dxa"/></w:tcMar><w:hideMark/></w:tcPr><w:p w:rsidR="00BE55AC" w:rsidRPr="0018680A" w:rsidRDefault="0018680A" w:rsidP="00CE18C2"><w:pPr><w:spacing w:after="0"/><w:rPr><w:color w:val="000000"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:lang w:eastAsia="en-US"/></w:rPr><w:t xml:space="preserve">    </w:t></w:r><w:r><w:rPr><w:color w:val="000000"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$datosTI['tiautorizacionAEPD'].'</w:t></w:r></w:p></w:tc></w:tr></w:tbl><w:p w:rsidR="00BE55AC" w:rsidRPr="00BE55AC" w:rsidRDefault="00BE55AC" w:rsidP="00BE55AC"/><w:sectPr w:rsidR="00BE55AC" w:rsidRPr="00BE55AC" w:rsidSect="008345FB"><w:headerReference w:type="even" r:id="rId9"/><w:headerReference w:type="default" r:id="rId10"/><w:footerReference w:type="even" r:id="rId11"/><w:footerReference w:type="default" r:id="rId12"/><w:headerReference w:type="first" r:id="rId13"/><w:footerReference w:type="first" r:id="rId14"/><w:pgSz w:w="11906" w:h="16838"/><w:pgMar w:top="1417" w:right="1701" w:bottom="1417" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/><w:cols w:space="708"/><w:docGrid w:linePitch="360"/></w:sectPr>';

		$j++;
	}


	$documento->setValue("tablaEntradas",utf8_decode($tablaEntradasP));
	$documento->setValue("tablaTexto",utf8_decode($tablaTexto));

	$documento->save('../documentos/consultorias/'.$fichero);

	return $fichero;
}

function anexoI_X($datos, $PHPWord, $nombreFichero = '', $indice, $cliente, $conexion = true){
	
	global $_CONFIG;
	
	$fichero      = 'ANEXO_I.'.$indice.'_REGISTRO_DE_ACTIVIDADES_DE_TRATAMIENTO.docx';
	$seguridad    = [
		1 => 'Básico', 
		2 => 'Medio', 
		3 => 'Alto'
	];	
	$sistemaTrata = [
		1 => 'Automatizado', 
		2 => 'Manual', 
		3 => 'Mixto'
	];	
	
	$documento = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente", utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	
	reemplazarLogo($documento, $cliente);

	$formulario = recogerFormularioServicios($cliente);

	$esEncargado = false;
	
	if(trim($datos['cif_nif']) != trim($datos['cif_nif_responsableFichero'])) {		
		
		$encargado = consultaBD('SELECT * FROM otros_encargados_lopd WHERE cifEncargado="'.$datos['cif_nif'].'";', $conexion, true);
		if ($encargado) {			
			$ficheros = explode('&$&', $encargado['ficheroEncargado']);
			
			if(in_array($datos['codigo'], $ficheros)) {			
				$esEncargado = true;
			}
			
		} 
		else {			
			$nifEncargados = array(
				160 => 574,
				170 => 575,
				180 => 576,
				191 => 577,
				202 => 578,
				212 => 579,
				222 => 609,
				232 => 580,
				242 => 581,
				252 => 582,
				262 => 583,
				272 => 584,
				282 => 585,
				292 => 586,
				289 => 587
			);

			foreach ($nifEncargados as $key=>$value) {
				if(trim($formulario['pregunta'.$key]) == trim($datos['cif_nif'])) {
					//echo '5';
					$ficheros = explode('&$&',$formulario['pregunta'.$value]);
					if(in_array($datos['codigo'],$ficheros)){
						//echo '6';
						$esEncargado=true;
					}
				}
			}
		}
	}

	$fecha = $datos['fechaAltaTratamiento'];
	$documento->setValue("fecha",formateaFechaWeb($fecha));
	
	$actividadesAgencia = array(
		''    => '',
		'N01' => 'Comercio',
		'N02' => 'Sanidad',
		'N03' => 'Contabilidad, Auditoría y Asesoría Fiscal',
		'N04' => 'Asociaciones y Clubes',
		'N05' => 'Actividades Inmobiliarias',
		'N06' => 'Industria Química y Farmacéutica',
		'N07' => 'Construcción',
		'N08' => 'Turismo y Hostelería',
		'N09' => 'Maquinaria y Medios de Transporte',
		'N10' => 'Educación',
		'N11' => 'Transporte',
		'N12' => 'Seguros Privados',
		'N13' => 'Servicios Informáticos',
		'N14' => 'Actividades relacionadas con los Productos Alimenticios, Bebidas y Tabacos',
		'N15' => 'Agricultura, Ganadería, Explotación Forestal, Caza, Pesca',
		'N16' => 'Entidades Bancarias y Financieras',
		'N17' => 'Producción de Bienes de Consumo',
		'N18' => 'Sector Energético',
		'N19' => 'Actividades Jurídicas, Notarios y Registradores',
		'N20' => 'Actividades Diversas de Servicios Personales',
		'N21' => 'Actividades de Organizaciones Empresariales, Profesionales Y Patronales',
		'N22' => 'Actividades de Servicios Sociales',
		'N23' => 'Publicidad Directa',
		'N24' => 'Servicios de Telecomunicaciones',
		'N25' => 'Actividades relacionadas con los Juegos de Azar y Apuestas',
		'N26' => 'Seguridad',
		'N27' => 'Selección de Personal',
		'N28' => 'Actividades Postales y de Correo (Operadores Postales, Empresas Prestadoras de Servicios Postales, Transportistas, ...)',
		'N29' => 'Investigación y Desarrollo (I+D)',
		'N30' => 'Actividades Políticas, Sindicales y Religiosas',
		'N31' => 'Mutualidades Colaboradoras de los Organismos de la Seguridad Social',
		'N32' => 'Organizacion de Ferias, Exhibiciones, Congresos y otras actividades relacionadas',
		'N33' => 'Solvencia Patrimonial Y Crédito',
		'N34' => 'Inspección Técnica de Vehículos y otros Análisis Técnicos',
		'N35' => 'Comercio y Servicios Electrónicos',
		'N36' => 'Comunidades de Propietarios',
		'N99' => 'Otras Actividades'
	);
	
	$provinciasAgencia = array(
		''   => '',
		'01' => 'ÁLAVA',
		'02' => 'ALBACETE',
		'03' => 'ALICANTE',
		'04' => 'ALMERÍA',
		'05' => 'ÁVILA',
		'06' => 'BADAJOZ',
		'07' => 'ILLES BALEARS',
		'08' => 'BARCELONA',
		'09' => 'BURGOS',
		'10' => 'CÁCERES',
		'11' => 'CÁDIZ',
		'12' => 'CASTELLÓN DE LA PLANA',
		'13' => 'CIUDAD REAL',
		'14' => 'CÓRDOBA',
		'15' => 'A CORUÑA',
		'16' => 'CUENCA',
		'17' => 'GIRONA',
		'18' => 'GRANADA',
		'19' => 'GUADALAJARA',
		'20' => 'GUIPÚZCOA',
		'21' => 'HUELVA',
		'22' => 'HUESCA',
		'23' => 'JAÉN',
		'24' => 'LEÓN',
		'25' => 'LLEIDA',
		'26' => 'LA RIOJA',
		'27' => 'LUGO',
		'28' => 'MADRID',
		'29' => 'MÁLAGA',
		'30' => 'MURCIA',
		'31' => 'NAVARRA',
		'32' => 'OURENSE',
		'33' => 'ASTURIAS',
		'34' => 'PALENCIA',
		'35' => 'LAS PALMAS',
		'36' => 'PONTEVEDRA',
		'37' => 'SALAMANCA',
		'38' => 'SANTA CRUZ DE TENERIFE',
		'39' => 'CANTABRIA',
		'40' => 'SEGOVIA',
		'41' => 'SEVILLA',
		'42' => 'SORIA',
		'43' => 'TARRAGONA',
		'44' => 'TERUEL',
		'45' => 'TOLEDO',
		'46' => 'VALENCIA',
		'47' => 'VALLADOLID',
		'48' => 'VIZCAYA',
		'49' => 'ZAMORA',
		'50' => 'ZARAGOZA',
		'51' => 'CEUTA',
		'52' => 'MELILLA'
	);
	
	$actividades = array(
		'ABOGACIA'     => 'Abogacía',
		'FISCAL'       => 'Asesoramiento fiscal',
		'CONTABLE'     => 'Asesoramiento fiscal y contable',
		'CONTABILIDAD' => 'Contabilidad externa',
		'AUDITORIA'    => 'Auditoría de cuentas',
		'PROMOCION'    => 'Promoción inmobiliaria',
		'OTRA'         => 'Otra'
	);
	
	$actividad = $cliente['actividad'] == 'OTRA' ? $cliente['servicios'] : $cliente['actividad'];
	
	if ($esEncargado) {		
		$info = '<w:p w14:paraId="1A0B0BF9" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Identidad y datos de contacto del Encargado del Tratamiento:</w:t></w:r></w:p>';
	} 
	else {
		$info = '<w:p w14:paraId="1A0B0BF9" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Identidad y datos de contacto del Responsable del Tratamiento:</w:t></w:r></w:p>';
	}

	$info .= '<w:p w14:paraId="34D7045B" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Responsable del Tratamiento: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['n_razon']).'</w:t></w:r></w:p><w:p w14:paraId="37CEFA0A" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">NIF/CIF: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['cif_nif_responsableFichero'].'</w:t></w:r></w:p><w:p w14:paraId="1A5B2974" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Representante legal</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="00171AD1" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['representante_responsableFichero'].'</w:t></w:r></w:p>';

	$info .= '<w:p w14:paraId="5A250D03" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Sector o Actividad: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$actividadesAgencia[$datos['cap']].'</w:t></w:r></w:p><w:p w14:paraId="5A711F1A" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Domicilio fiscal:</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$datos['dir_postal_responsableFichero'].' CP:'.$datos['postal_responsableFichero'].' - '.$datos['localidad_responsableFichero'].' ('.convertirMinuscula($provinciasAgencia[$datos['provincia_responsableFichero']]).')</w:t></w:r></w:p><w:p w14:paraId="40C321AE" w14:textId="77777777" w:rsidR="00375B74" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Teléfono de contacto: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['telefono_responsableFichero'].'</w:t></w:r></w:p><w:p w14:paraId="18EFE602" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>Correo electrónico</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$datos['email_responsableFichero'].'</w:t></w:r></w:p>';
	
	if (trim($datos['cif_nif']) != trim($datos['cif_nif_responsableFichero']) && 
		trim($datos['cif_nif']) == trim($datos['cif_nif_encargado'])
		) 
	{
		$info = '<w:p w14:paraId="1A0B0BF9" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Identidad y datos de contacto del  Encargado del Tratamiento:</w:t></w:r></w:p>';

		$info .= '<w:p w14:paraId="34D7045B" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Encargado del Tratamiento: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>'.sanearCaracteres($datos['n_razon_encargado']).'</w:t></w:r></w:p><w:p w14:paraId="37CEFA0A" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">NIF/CIF: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['cif_nif_encargado'].'</w:t></w:r></w:p><w:p w14:paraId="1A5B2974" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Representante legal</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="00171AD1" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$cliente['representante'].'</w:t></w:r></w:p>';

		$info .= '<w:p w14:paraId="5A250D03" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Sector o Actividad: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['servicioEncargado'].'</w:t></w:r></w:p><w:p w14:paraId="5A711F1A" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t>Domicilio fiscal:</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$datos['dir_postal_encargado'].' CP:'.$datos['postal_encargado'].' - '.$datos['localidad_encargado'].' ('.convertirMinuscula($provinciasAgencia[$datos['provincia_encargado']]).')</w:t></w:r></w:p><w:p w14:paraId="40C321AE" w14:textId="77777777" w:rsidR="00375B74" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Teléfono de contacto: </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:bCs/></w:rPr><w:t>'.$datos['telefono_encargado'].'</w:t></w:r></w:p><w:p w14:paraId="18EFE602" w14:textId="77777777" w:rsidR="00F30330" w:rsidRPr="009B2881" w:rsidRDefault="00F30330" w:rsidP="00F30330"><w:pPr><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>Correo electrónico</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="0076186B" w:rsidRPr="009B2881"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Times New Roman"/></w:rPr><w:t>'.$datos['email_encargado'].'</w:t></w:r></w:p>';
		
		$tabla = '<w:p w14:paraId="4161F87E" w14:textId="611A690E" w:rsidR="00941A9D" w:rsidRPr="002E3AE4" w:rsidRDefault="002E3AE4" w:rsidP="00F30330"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="002E3AE4"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>RESPONSABLES DEL TRATAMIENTO</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w="9836" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1471"/><w:gridCol w:w="1279"/><w:gridCol w:w="3403"/><w:gridCol w:w="3683"/></w:tblGrid><w:tr w:rsidR="00E752CD" w14:paraId="467A2FF7" w14:textId="77777777" w:rsidTr="007941CA"><w:tc><w:tcPr><w:tcW w:w="748" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/><w:hideMark/></w:tcPr><w:p w14:paraId="2A78566A" w14:textId="5CFC962F" w:rsidR="00E752CD" w:rsidRDefault="007E5594" w:rsidP="00744E49"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>Razón social</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="650" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/><w:hideMark/></w:tcPr><w:p w14:paraId="6597EF7B" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>CIF</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1730" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/><w:hideMark/></w:tcPr><w:p w14:paraId="10CB2CC2" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>Representante Legal</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1873" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/><w:hideMark/></w:tcPr><w:p w14:paraId="32DC39F0" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>Dirección</w:t></w:r></w:p></w:tc></w:tr>';
		
		if ($datos['n_razon'] != '' && $datos['fechaFinResponsable'] == '0000-00-00'){
			
			$domicilio = $datos['dir_postal_responsableFichero'].', '.$datos['postal_responsableFichero'].' '.$datos['localidad_responsableFichero'];
			
			$tabla .= '<w:tr w:rsidR="00E752CD" w14:paraId="4AFD8F18" w14:textId="77777777" w:rsidTr="007941CA"><w:tc><w:tcPr><w:tcW w:w="748" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="588EF48A" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.sanearCaracteres($datos['n_razon']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="650" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="7F483BDA" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$datos['cif_nif_responsableFichero'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1730" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="112D25B7" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$datos['representante_responsableFichero'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1873" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="5A63CA31" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$domicilio.'</w:t></w:r></w:p></w:tc></w:tr>';	
		}
		
		$responsables = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion='.$datos['codigo'].' AND fechaFin="0000-00-00"', $conexion);
		
		while ($item = mysql_fetch_assoc($responsables)){
			
			$domicilio = $item['direccion'].', '.$item['cp'].' '.$item['localidad'];
			
			$tabla .= '<w:tr w:rsidR="00E752CD" w14:paraId="4AFD8F18" w14:textId="77777777" w:rsidTr="007941CA"><w:tc><w:tcPr><w:tcW w:w="748" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="588EF48A" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.sanearCaracteres($item['razonSocial']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="650" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="7F483BDA" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$item['cif'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1730" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="112D25B7" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1873" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="5A63CA31" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="007E5594"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$domicilio.'</w:t></w:r></w:p></w:tc></w:tr>';
		}
		
		$tabla .= '</w:tbl>';
	} 
	else {
		
		$tabla = '<w:p w14:paraId="4161F87E" w14:textId="611A690E" w:rsidR="00941A9D" w:rsidRPr="002E3AE4" w:rsidRDefault="002E3AE4" w:rsidP="00F30330"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="002E3AE4"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>ENCARGADOS DEL TRATAMIENTO</w:t></w:r></w:p><w:tbl><w:tblPr><w:tblW w:w="8131" w:type="dxa"/><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1837"/><w:gridCol w:w="1273"/><w:gridCol w:w="1760"/><w:gridCol w:w="3261"/></w:tblGrid><w:tr w:rsidR="00E752CD" w14:paraId="467A2FF7" w14:textId="77777777" w:rsidTr="002E3AE4"><w:tc><w:tcPr><w:tcW w:w="1130" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/><w:hideMark/></w:tcPr><w:p w14:paraId="2A78566A" w14:textId="5CFC962F" w:rsidR="00E752CD" w:rsidRDefault="002E3AE4" w:rsidP="00744E49"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>Razón social</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="783" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/><w:hideMark/></w:tcPr><w:p w14:paraId="6597EF7B" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>CIF</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1082" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/><w:hideMark/></w:tcPr><w:p w14:paraId="10CB2CC2" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>Representante Legal</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2005" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9"/><w:hideMark/></w:tcPr><w:p w14:paraId="32DC39F0" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>Dirección</w:t></w:r></w:p></w:tc></w:tr>';
		
		$encargados = array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
		$campos = array(
			158 => [159, 589, 574, 160, 167, 161, 164, 163, 162, 611],
			168 => [169, 590, 575, 170, 177, 171, 174, 173, 172, 612],
			178 => [179, 591, 576, 180, 187, 181, 184, 183, 182, 613],
			189 => [190, 592, 577, 191, 198, 192, 195, 194, 193, 614],
			200 => [201, 593, 578, 202, 209, 203, 206, 205, 204, 615],
			210 => [211, 594, 579, 212, 219, 213, 216, 215, 214, 616],
			220 => [221, 595, 609, 222, 229, 223, 226, 225, 224, 617],
			230 => [231, 596, 580, 232, 239, 233, 236, 235, 234, 618],
			240 => [241, 597, 581, 242, 249, 243, 246, 245, 244, 619],
			250 => [251, 598, 582, 252, 259, 253, 256, 255, 254, 620],
			260 => [261, 599, 583, 262, 269, 263, 266, 265, 264, 621],
			270 => [271, 600, 584, 272, 279, 273, 276, 275, 274, 622],
			280 => [281, 601, 585, 282, 289, 283, 286, 285, 284, 623],
			290 => [291, 602, 586, 292, 299, 293, 296, 295, 294, 624],
			487 => [488, 603, 587, 289, 496, 490, 492, 492, 491, 625]
		);
		
		$entra = false;
		
		foreach ($encargados as $key => $value) {
			
			if ($formulario['pregunta'.$value] == 'SI'){
				
				if ($formulario['pregunta'.$campos[$value][1]] != 'S' && $formulario['pregunta'.$campos[$value][1]] != 'N'){
					
					$ficheros = explode('&$&', $formulario['pregunta'.$campos[$value][2]]);
					
					if(in_array($datos['codigo'], $ficheros)){
						$entra = true;
						$domicilio = $formulario['pregunta'.$campos[$value][5]].', (CP: '.$formulario['pregunta'.$campos[$value][6]].') '.$formulario['pregunta'.$campos[$value][7]].' ('.obtieneProvincia($formulario['pregunta'.$campos[$value][9]]).')';
						
						$tabla .= '<w:tr w:rsidR="00E752CD" w14:paraId="4AFD8F18" w14:textId="77777777" w:rsidTr="00E752CD"><w:tc><w:tcPr><w:tcW w:w="1130" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="588EF48A" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.sanearCaracteres($formulario['pregunta'.$campos[$value][0]]).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="783" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="7F483BDA" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$formulario['pregunta'.$campos[$value][3]].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1082" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="112D25B7" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t xml:space="preserve">'.$formulario['pregunta'.$campos[$value][4]].'</w:t></w:r><w:proofErr w:type="spellStart"/><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t></w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2005" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="5A63CA31" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$domicilio.'</w:t></w:r></w:p></w:tc></w:tr>';
					}
				}
			}
		}

		$encargados = consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$cliente['codigoTrabajo'], true);
		
		while ($item = mysql_fetch_assoc($encargados)){
			
			$ficheros = explode('&$&', $item['ficheroEncargado']);
			
			if(in_array($datos['codigo'], $ficheros)){
				if($item['accesosEncargado']!='S' && $item['accesosEncargado']!='N'){					
					$entra = true;
					$domicilio = $item['direccionEncargado'].', (CP: '.$item['cpEncargado'].') '.$item['localidadEncargado'].' ('.obtieneProvincia($item['provinciaEncargado']).')';
					
					$tabla .= '<w:tr w:rsidR="00E752CD" w14:paraId="4AFD8F18" w14:textId="77777777" w:rsidTr="00E752CD"><w:tc><w:tcPr><w:tcW w:w="1130" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="588EF48A" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.sanearCaracteres($item['nombreEncargado']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="783" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="7F483BDA" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$item['cifEncargado'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1082" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="112D25B7" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t xml:space="preserve">'.$item['representanteEncargado'].'</w:t></w:r><w:proofErr w:type="spellStart"/><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t></w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2005" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="5A63CA31" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t>'.$domicilio.'</w:t></w:r></w:p></w:tc></w:tr>';					
				}
			}
		}
		
		if (!$entra) {

			$tabla .= '<w:tr w:rsidR="00E752CD" w14:paraId="4AFD8F18" w14:textId="77777777" w:rsidTr="00E752CD"><w:tc><w:tcPr><w:tcW w:w="1130" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="588EF48A" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="783" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="7F483BDA" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1082" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="112D25B7" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t xml:space="preserve"></w:t></w:r><w:proofErr w:type="spellStart"/><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t></w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2005" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="5A63CA31" w14:textId="77777777" w:rsidR="00E752CD" w:rsidRDefault="00E752CD"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr></w:pPr><w:r w:rsidRPr="00C33473"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:lang w:eastAsia="en-US"/></w:rPr><w:t></w:t></w:r></w:p></w:tc></w:tr>';

		}
		
		$tabla .= '</w:tbl>';
	}
	
	$tiposArray = array(
		'Recogida'       => 'checkTipoTratamiento1',
		'Registro'       => 'checkTipoTratamiento2',
		'Estructuración' => 'checkTipoTratamiento3',
		'Modificación'   => 'checkTipoTratamiento4',
		'Conservación'   => 'checkTipoTratamiento5',
		'Interconexión'  => 'checkTipoTratamiento6',
		'Consulta'       => 'checkTipoTratamiento7',
		'Cotejo'         => 'checkTipoTratamiento8',
		'Análisis de conducta' => 'checkTipoTratamiento9',
		'Comunicación'   => 'checkTipoTratamiento10',
		'Supresión'      => 'checkTipoTratamiento11',
		'Destrucción'    => 'checkTipoTratamiento12',
		'Comunicación por transmisión al responsable del fichero' => 'checkTipoTratamiento13',
		'Comunicación a la Administración Pública competente'     => 'checkTipoTratamiento14',
		'Comunicación permitida por ley' => 'checkTipoTratamiento15'
	);
	
	$tipoTratamiento = '';
	foreach ($tiposArray as $key => $value) {		
		if ($datos[$value] == 'SI'){
			
			if($tipoTratamiento != ''){
				$tipoTratamiento .= ', ';
			}

			$tipoTratamiento .= $key;
		}
	}
		

	// $encargados = consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$cliente['codigoTrabajo'], $conexion);		
	// while ($item = mysql_fetch_assoc($encargados)) {
		
	// 	$ficheroEncargado = explode('&%&', $item['ficheroEncargado']);
		
	// 	// SE COMENTA ESTO POR LA INCIDENCIA https://qmaconsultores.com/crm-qma-2/mantenimiento-proyectos/gestion.php?codigo=11537
		
	// 	// if($item['subcontratacionEncargado'] == 'SI' && in_array($datos['codigo'], $ficheroEncargado)) {
	// 	// 	$consulta = consultaBD("SELECT CONCAT(CHAR(13), ' - ', empresa, ' - ', nif) AS subencargado FROM declaraciones_subcontrata WHERE codigoResponsable = ".$item['codigo']." AND tipo = 'ENCARGADO';", $conexion);
	// 	// 	while($e = mysql_fetch_assoc($consulta)){
	// 	// 		$datos['baseJuridica'] .= $e['subencargado'];
	// 	// 	}
	// 	// }	

	// }	

	$tipoTratamiento.=', '.$datos['tipoTratamiento'];
	$documento->setValue("tabla",utf8_decode($tabla));
	$documento->setValue("info",utf8_decode($info));
	$documento->setValue("nombreFichero",utf8_decode($datos['nombreFichero']));
	$documento->setValue("nivel",utf8_decode($seguridad[$datos['nivel']]));
	$documento->setValue("baseJuridica",utf8_decode(sanearCaracteres(nl2br($datos['baseJuridica']))));
	$documento->setValue("tipoTratamiento",utf8_decode(sanearCaracteres(nl2br($tipoTratamiento))));
	$documento->setValue("numInteresados",utf8_decode(sanearCaracteres(nl2br($datos['numInteresados']))));
	$documento->setValue("desc_fin_usos",utf8_decode(sanearCaracteres(nl2br($datos['desc_fin_usos']))));
	$documento->setValue("plazo",utf8_decode(sanearCaracteres(nl2br($datos['plazo']))));
	$checks=recogeChecks('CI',$datos);
	$documento->setValue("ci",utf8_decode($checks));
	if($datos['otrosTiposDatos']=='SI'){
		$otrosTipos=array('0'=>'Características Personales','1'=>'Académicos y profesionales','2'=>'Detalles del empleo','3'=>'Económicos, financieros y de seguros');
		$texto='';
		for($i=0;$i<4;$i++){
			if($datos['checkOtrosTiposDatos'.$i]!='NO'){
				if($texto!=''){
					$texto.=', ';
				}
				$texto.=$otrosTipos[$i];
			}
		}
		if($datos['desc_otros_tipos']!=''){
			if($texto!=''){
				$texto.=', ';
			}
			$texto.=nl2br($datos['desc_otros_tipos']);
		}
		$documento->setValue("desc_otros_tipos",utf8_decode(sanearCaracteres($texto)));
	} else {
		$documento->setValue("desc_otros_tipos",utf8_decode(''));
	}
	$documento->setValue("tratamiento",utf8_decode($sistemaTrata[$datos['tratamiento']]));
	$checks=recogeChecks('O',$datos);
	$documento->setValue("o",utf8_decode($checks));
	$checks=recogeChecks('C',$datos);
	$documento->setValue("c",utf8_decode($checks));
	$checks=recogeChecks('CD',$datos);
	$documento->setValue("cd",utf8_decode($checks));

	$checks=definirDatosFicheros($datos,false);
	$documento->setValue("datos_protegidos",utf8_decode($checks));
	
	$transferencias=consultaBD('SELECT * FROM transferencias_internacionales WHERE codigoDeclaracion='.$datos['codigo'], true);
	$campos=array('destNSadecuado'=>'','destNSinadecuado'=>'','otrosDestTI'=>'','tiautorizacionAEPD'=>'');
	while($t=mysql_fetch_assoc($transferencias)){
		foreach ($campos as $key => $value) {
			if($t[$key]!=''){
				if($campos[$key]!=''){
					$campos[$key].=', ';
				}
				$campos[$key].=$t[$key];
			}
		}
	}
	$documento->setValue("t1",utf8_decode($campos['destNSadecuado']));
	$documento->setValue("t2",utf8_decode($campos['destNSinadecuado']));
	$documento->setValue("t3",utf8_decode($campos['otrosDestTI']));
	$documento->setValue("t4",utf8_decode($campos['tiautorizacionAEPD']));
	$documento->setValue("localidad",utf8_decode($cliente['localidad']));
	$documento->setValue("representante",utf8_decode($cliente['representante'].' '.$cliente['apellido1'].' '.$cliente['apellido2']));
	$fechaTratamiento='';
	if($datos['fechaInicioTratamiento']!='' && $datos['fechaInicioTratamiento']!='0000-00-00'){
		$fechaTratamiento='Fecha inicio del tratamiento: '.formateaFechaWeb($datos['fechaInicioTratamiento']);
	}
	if($datos['fechaBajaTratamiento']!='' && $datos['fechaBajaTratamiento']!='0000-00-00'){
		if($fechaTratamiento!=''){
			$fechaTratamiento.='<w:br/><w:br/>';
		}
		$fechaTratamiento.='Fecha de baja del tratamiento: '.formateaFechaWeb($datos['fechaBajaTratamiento']);
	}
	$documento->setValue("fechaDeBaja",$fechaTratamiento);
	$documento->setValue("fechaBaja",'');
	/*$fecha=date('Y-m-d');
	$documento->setValue("fecha",formateaFechaWeb($fecha));*/


	$documento->save('../documentos/consultorias/'.$fichero);

	return $fichero;
}

function recogeChecks($tipo,$entrada){

	$res='';

	switch ($tipo) {
		case 'CI':
			# Datos Caracter Identificativos
			$titulosChecks=array(0=>'NIF / DNI',1=>'Nº SS / Mutualidad',2=>'Nombre y apellidos',3=>'Tarjeta sanitaria',4=>'Dirección',5=>'Teléfono',6=>'Firma',7=>'Huella',8=>'Imagen / voz',9=>'Marcas físicas',10=>'Firma electrónica',11=>'Correo electrónico',12=>'Otros datos biométricos',13=>'Otros datos de carácter identificativo');

			for($i=0;$i<14;$i++){
				if($entrada['checkDatosIdentificativos'.$i]!='NO'){
					if($res!=''){
						$res.=', ';
					}
					if($i!=13){
						$res.=$titulosChecks[$i];
					} else {
						$res.=$entrada['ODCI'];
					}
				}
			}		
			break;

		case 'CD':
			# Datos Cesiones Destinatarios
			$titulosChecks=array(0=>'Organizaciones o Personas Directamente Relacionadas con el Responsable','01'=>'Organismos de la Seguridad Social','02'=>'Registros Públicos','03'=>'Colegios Profesionales','04'=>'Administración Tributaria','05'=>'Otros Órganos de la Administración Pública','06'=>'Comisión Nacional del Mercado de Valores','07'=>'Comisión Nacional del Juego','08'=>'Notarios y Procuradores','09'=>'Fuerzas y Cuerpos de Seguridad',10=>'Organismos de la Unión Europea',11=>'Entidades Dedicadas al Cumplimiento o Incumplimiento de Obligaciones Dinerarias',12=>'Bancos, Cajas de Ahorros y Cajas Rurales',13=>'Entidades Aseguradoras',14=>'Otras Entidades Financieras',15=>'Entidades Sanitarias',16=>'Prestaciones de Servicios de Telecomunicaciones',17=>'Empresas Dedicadas a Publicidad o Marketing Directo',18=>'Asociaciones y Organizaciones sin Ánimo de Lucro',19=>'Sindicatos y Juntas de Personal',20=>$entrada['desc_otros']);

			for($i=0;$i<21;$i++){
				if($entrada['checkDestinatariosCesiones'.$i]!='NO'){
					if($res!=''){
						$res.=', ';
					}
					$n=$entrada['checkDestinatariosCesiones'.$i]-1;
					$n=$n<10 && $n!=0?'0'.$n:$n;
					$res.=$titulosChecks[$n];
				}
			}		
			break;

		case 'O':
			# Datos Origenes
			$titulosChecks=array(0=>'El propio interesado o su representante legal',1=>'Otras personas físicas',2=>'Fuentes accesibles al público',3=>'Registros públicos',4=>'Entidad privada',5=>'Administraciones Públicas');

			for($i=0;$i<6;$i++){
				if($entrada['checkOrigen'.$i]!='NO'){
					if($res!=''){
						$res.=', ';
					}
					$res.=$titulosChecks[$entrada['checkOrigen'.$i]-1];
				}
			}		
			break;

		case 'C':
			# Datos Colectivos
			$titulosChecks=array('0'=>'Empleados','01'=>'Clientes y Usuarios','02'=>'Proveedores','03'=>'Asociados o Miembros','04'=>'Propietarios o Arrendatarios','05'=>'Pacientes','06'=>'Estudiantes','07'=>'Personas de Contacto','08'=>'Padres o Tutores','09'=>'Representante Legal',10=>'Solicitantes',11=>'Beneficiarios',12=>'Cargos Públicos',13=>'Otro colectivo');

			for($i=0;$i<14;$i++){
				if($entrada['checkColectivo'.$i]!='NO'){
					if($res!=''){
						$res.=', ';
					}
					if($entrada['checkColectivo'.$i]=='OTRO'){
						$n=13;
					} else {
						$n=$entrada['checkColectivo'.$i]-1;
						$n=$n<10 && $n!=0?'0'.$n:$n;
					}
					$res.=$titulosChecks[$n];
				}
			}		
			break;									
		
		default:
			# Por defecto
			break;
	}

	return $res;

}

function generaDocumentoDeSeguridad($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='DOCUMENTO_DE_SEGURIDAD.docx';	
	/*$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, clientes.administrador, clientes.nifAdministrador, clientes.telefono, clientes.email, clientes.actividad, clientes.cif, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo=".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);	*/

	$datos=datosRegistro('trabajos',$codigo);
	$cliente=datosRegistro('clientes',$datos['codigoCliente']);
	$declaraciones=consultaBD('SELECT nivel FROM declaraciones WHERE codigoCliente='.$cliente['codigo'],true);
	$nivel=0;
	while($d=mysql_fetch_assoc($declaraciones)){
		if(intval($d['nivel'])>$nivel){
			$nivel=intval($d['nivel']);
		}
	}
	if($nivel<2){
		$nombreFichero='ds_sin_alto.docx';
	}
	$formulario=recogerFormularioServicios($datos);
	
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($formulario['pregunta3'])));
	$documento->setValue("fechaHoy",utf8_decode(fecha()));
	//$documento->setValue("fecha",utf8_decode($formulario['pregunta2']));
	$documento->setValue("fecha",utf8_decode(fecha()));
	$documento->setValue("cif",utf8_decode($formulario['pregunta9']));
	$documento->setValue("direccion",utf8_decode(sanearCaracteres($formulario['pregunta4'])));
	$documento->setValue("direccion2",utf8_decode(sanearCaracteres('CP: '.$formulario['pregunta10'].' '.$formulario['pregunta5'].' ('.$formulario['pregunta11'].')')));
	$documento->setValue("pais",utf8_decode('ESPAÑA'));
	$documento->setValue("poblacion",utf8_decode($formulario['pregunta5']));
	$documento->setValue("provincia",utf8_decode(convertirMinuscula($formulario['pregunta11'])));

	// $documento->setValue("responsable",utf8_decode($formulario['pregunta15']));
	// $documento->setValue("nifResponsable",utf8_decode($formulario['pregunta17']));
	$representate = $formulario['pregunta8'].' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	$documento->setValue("responsable",utf8_decode($representate));
	$documento->setValue("nifResponsable",utf8_decode($formulario['pregunta14']));

	$documento->setValue("true",'');
	$nombreRL=$formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}
	$documento->setValue("representante",utf8_decode($nombreRL));
	$documento->setValue("nifRepresentante",utf8_decode($formulario['pregunta14']));
	$documento->setValue("true","");
	reemplazarLogo($documento,$cliente);	
	/*$imagen='image2.png';	
     if($_CONFIG['usuarioBD']=='root'){
    	$logo = '../documentos/logos-clientes/imagenDefecto.png';
    } else {
    	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos['ficheroLogo']);
    	if($hayLogo!='NO'){
    		$logo = '../documentos/logos-clientes/'.$datos['ficheroLogo'];
		} else {
			$logo = '../documentos/logos-clientes/imagenDefecto.png';
		}
	}
	$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
	if (!copy($logo, $nuevo_logo)) {
    	echo "Error al copiar $nuevo_logo...\n";
	}*/	

	$documento->setValue("tablaFicheros",utf8_decode(obtenerFicherosDS($cliente['codigo'])));

	$soportes=consultaBD('SELECT * FROM soportes_lopd_nueva WHERE codigoCliente='.$cliente['codigo'],true);
	$listado=array();
	$listado2=array();
	while($soporte=mysql_fetch_assoc($soportes)){
		$mecanismos=explode(',', $soporte['mecanismosAccesoSoporteLOPD']);
		foreach ($mecanismos as $key => $value) {
			if(!in_array(trim($value), $listado)){
				array_push($listado, trim($value));
			}
		}
		$recursos=explode(',', $soporte['mecanismosAccesoRecursosSoporteLOPD']);
		foreach ($recursos as $key => $value) {
			if(!in_array(trim($value), $listado2)){
				array_push($listado2, trim($value));
			}
		}
	}
	$listadoTexto='';
	foreach ($listado as $key => $value) {
		$listadoTexto.='- '.$value.'<w:br />';
	}
	$listadoTexto2='';
	$listadoTexto3='';
	foreach ($listado2 as $key => $value) {
		$listadoTexto2.='- '.$value.'<w:br />';
		if($listadoTexto3!=''){
			$listadoTexto3.='; ';
		}
		$listadoTexto3.=$value;
	}
	$documento->setValue("medidas",utf8_decode($listadoTexto));
	$documento->setValue("recursos",utf8_decode($listadoTexto2));
	$documento->setValue("recursos2",utf8_decode($listadoTexto3));

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function obtenerFicherosDS($cliente){
	$res='<w:tbl><w:tblPr><w:tblW w:w="4900" w:type="pct"/><w:jc w:val="center"/><w:tblCellSpacing w:w="0" w:type="dxa"/><w:tblCellMar><w:left w:w="0" w:type="dxa"/><w:right w:w="0" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="8334"/></w:tblGrid><w:tr w:rsidR="00133574" w14:paraId="05032F1E" w14:textId="77777777" w:rsidTr="00966081"><w:trPr><w:tblCellSpacing w:w="0" w:type="dxa"/><w:jc w:val="center"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:tbl><w:tblPr><w:tblW w:w="5000" w:type="pct"/><w:tblCellSpacing w:w="7" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="AAAAAA"/><w:tblCellMar><w:top w:w="30" w:type="dxa"/><w:left w:w="30" w:type="dxa"/><w:bottom w:w="30" w:type="dxa"/><w:right w:w="30" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="620"/><w:gridCol w:w="2645"/><w:gridCol w:w="3026"/><w:gridCol w:w="2043"/></w:tblGrid><w:tr w:rsidR="00133574" w:rsidRPr="00350210" w14:paraId="6D84FE79" w14:textId="77777777" w:rsidTr="00966081"><w:trPr><w:trHeight w:val="300"/><w:tblCellSpacing w:w="7" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="CECECE"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="4FEF8573" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00133574" w:rsidP="00966081"><w:pPr><w:pStyle w:val="headlisttext"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000"/></w:rPr><w:lastRenderedPageBreak/><w:t>Ref</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="CECECE"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="32DF5B74" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00133574" w:rsidP="00966081"><w:pPr><w:pStyle w:val="headlisttext"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Fichero inscrito</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="CECECE"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="3DC44057" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00133574" w:rsidP="00966081"><w:pPr><w:pStyle w:val="headlisttext"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>Nivel de seguridad</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="CECECE"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="2CE52339" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00133574" w:rsidP="00966081"><w:pPr><w:pStyle w:val="headlisttext"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000"/></w:rPr><w:t>SISTEMA</w:t></w:r></w:p></w:tc></w:tr>';
	$ficheros=consultaBD('SELECT * FROM declaraciones WHERE codigoCliente='.$cliente.' AND (fechaBaja="0000-00-00" OR fechaBaja>"'.date('Y-m-d').'")',true);
	$i=1;
	$niveles=array(1=>'Básico',2=>'Medio',3=>'Alto');
	$tratamientos=array(1=>'Automatizado',2=>'Manual',3=>'Mixto');
	while($fichero=mysql_fetch_assoc($ficheros)){
	$n=$i<10?'0'.$i:$i;
	if($i%2==0){
		$res.='<w:tr w:rsidR="00133574" w:rsidRPr="00350210" w14:paraId="57686078" w14:textId="77777777" w:rsidTr="00966081"><w:trPr><w:trHeight w:val="300"/><w:tblCellSpacing w:w="7" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="F5F0F0"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="65BEEBB9" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00A201EA" w:rsidP="00966081"><w:pPr><w:jc w:val="center"/><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidR="00133574" w:rsidRPr="00350210"><w:rPr><w:rStyle w:val="Hyperlink"/><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.$n.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="F5F0F0"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="68661389" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00701EE1" w:rsidP="00701EE1"><w:pPr><w:rPr><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>'.$fichero['nombreFichero'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="F5F0F0"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="2F086E39" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00133574" w:rsidP="00966081"><w:pPr><w:pStyle w:val="NormalWeb"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>'.$niveles[$fichero['nivel']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="F5F0F0"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="7179596B" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00BC6FD3" w:rsidP="00966081"><w:pPr><w:jc w:val="center"/><w:rPr><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>'.$tratamientos[$fichero['tratamiento']].'</w:t></w:r></w:p></w:tc></w:tr>';
	} else {
		$res.='<w:tr w:rsidR="00133574" w:rsidRPr="00350210" w14:paraId="12947E53" w14:textId="77777777" w:rsidTr="00966081"><w:trPr><w:trHeight w:val="300"/><w:tblCellSpacing w:w="7" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="22234DB6" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00A201EA" w:rsidP="00966081"><w:pPr><w:jc w:val="center"/><w:rPr><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidR="00133574" w:rsidRPr="00350210"><w:rPr><w:rStyle w:val="Hyperlink"/><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.$n.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="3F690C5E" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00701EE1" w:rsidP="00966081"><w:pPr><w:rPr><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>'.$fichero['nombreFichero'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="3C0C5667" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="007E3978" w:rsidP="00966081"><w:pPr><w:pStyle w:val="NormalWeb"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>'.$niveles[$fichero['nivel']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="405B889B" w14:textId="77777777" w:rsidR="00133574" w:rsidRPr="00350210" w:rsidRDefault="00133574" w:rsidP="00966081"><w:pPr><w:jc w:val="center"/><w:rPr><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00350210"><w:rPr><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>'.$tratamientos[$fichero['tratamiento']].'</w:t></w:r></w:p></w:tc></w:tr>';
	}
		$i++;
	}

	$res.='</w:tbl><w:p w14:paraId="18C62842" w14:textId="77777777" w:rsidR="00133574" w:rsidRDefault="00133574" w:rsidP="00966081"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:tr w:rsidR="00133574" w14:paraId="56895619" w14:textId="77777777" w:rsidTr="00966081"><w:trPr><w:tblCellSpacing w:w="0" w:type="dxa"/><w:jc w:val="center"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="66333BC8" w14:textId="77777777" w:rsidR="00133574" w:rsidRDefault="00133574" w:rsidP="00966081"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri"/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p></w:tc></w:tr></w:tbl>';

	
	/*$res.='<w:tr w:rsidR="00DC0C60" w14:paraId="17941DF7" w14:textId="77777777" w:rsidTr="00725765"><w:trPr><w:trHeight w:val="300"/><w:tblCellSpacing w:w="7" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="6C7266C5" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRPr="00A61D33" w:rsidRDefault="00A61D33" w:rsidP="00725765"><w:pPr><w:jc w:val="center"/><w:rPr><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidR="00DC0C60" w:rsidRPr="00A61D33"><w:rPr><w:rStyle w:val="Hyperlink"/><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:color w:val="auto"/></w:rPr><w:t>01</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="7BDD7558" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRPr="00A61D33" w:rsidRDefault="00DC0C60" w:rsidP="00725765"><w:pPr><w:rPr><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A61D33"><w:rPr><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>CLIENTES</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="79EBB2D1" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRPr="00A61D33" w:rsidRDefault="00DC0C60" w:rsidP="00725765"><w:pPr><w:pStyle w:val="NormalWeb"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A61D33"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>Básico</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="6237F3E2" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRPr="00A61D33" w:rsidRDefault="00DC0C60" w:rsidP="00725765"><w:pPr><w:jc w:val="center"/><w:rPr><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A61D33"><w:rPr><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>Mixto</w:t></w:r></w:p></w:tc></w:tr>';
	$res.='<w:tr w:rsidR="00DC0C60" w14:paraId="6EC62962" w14:textId="77777777" w:rsidTr="00725765"><w:trPr><w:trHeight w:val="300"/><w:tblCellSpacing w:w="7" w:type="dxa"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="F5F0F0"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="284E3FAF" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRPr="00A61D33" w:rsidRDefault="00A61D33" w:rsidP="00725765"><w:pPr><w:jc w:val="center"/><w:rPr><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidR="00DC0C60" w:rsidRPr="00A61D33"><w:rPr><w:rStyle w:val="Hyperlink"/><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:color w:val="auto"/></w:rPr><w:t>02</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="F5F0F0"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="5CEDE3D4" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRPr="00A61D33" w:rsidRDefault="00DC0C60" w:rsidP="00725765"><w:pPr><w:rPr><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A61D33"><w:rPr><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>PROVEEDORES</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="F5F0F0"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="2D4A3241" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRPr="00A61D33" w:rsidRDefault="00DC0C60" w:rsidP="00725765"><w:pPr><w:pStyle w:val="NormalWeb"/><w:jc w:val="center"/><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A61D33"><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi"/><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>Básico</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:shd w:val="clear" w:color="auto" w:fill="F5F0F0"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="1276B338" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRPr="00A61D33" w:rsidRDefault="00DC0C60" w:rsidP="00725765"><w:pPr><w:jc w:val="center"/><w:rPr><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A61D33"><w:rPr><w:b/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>Automatizado</w:t></w:r></w:p></w:tc></w:tr>';
	$res.='</w:tbl><w:p w14:paraId="432E7DBA" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRDefault="00DC0C60" w:rsidP="00725765"><w:pPr><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:tr w:rsidR="00DC0C60" w14:paraId="4E96FF10" w14:textId="77777777" w:rsidTr="00725765"><w:trPr><w:tblCellSpacing w:w="0" w:type="dxa"/><w:jc w:val="center"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/><w:vAlign w:val="center"/><w:hideMark/></w:tcPr><w:p w14:paraId="5840F229" w14:textId="77777777" w:rsidR="00DC0C60" w:rsidRDefault="00DC0C60" w:rsidP="00725765"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:bookmarkEnd w:id="0"/></w:tbl>';*/

	return $res;
}
function compruebaCheckSubcontratacion($formulario,$codigo){
	$res=array();
	$res['siSubcontrata']=0;
	$res['noSubcontrata']=0;
	$res['subencargados']='NO';

	/*if($formulario['pregunta541']=='SI' || $formulario['pregunta527']=='SI' || $formulario['pregunta542']=='SI' || $formulario['pregunta528']=='SI' || $formulario['pregunta529']=='SI' || $formulario['pregunta530']=='SI' || $formulario['pregunta531']=='SI' || $formulario['pregunta532']=='SI' || $formulario['pregunta533']=='SI' || $formulario['pregunta534']=='SI' || $formulario['pregunta535']=='SI' || $formulario['pregunta536']=='SI' || $formulario['pregunta537']=='SI' || $formulario['pregunta538']=='SI' || $formulario['pregunta571']=='SI'){
		$res['siSubcontrata']=$i++;
	}else{
		$res['noSubcontrata']=$j++;
	}*/

	if($formulario['pregunta158']=='SI' && $formulario['pregunta541']=='SI'){
		$res['siSubcontrata']=1;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta574']);
	}elseif($formulario['pregunta158']=='SI' && $formulario['pregunta541']=='NO'){
		$res['noSubcontrata']=1;	
	}

	if($formulario['pregunta168']=='SI' && $formulario['pregunta527']=='SI'){
		$res['siSubcontrata']=2;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta575']);
	}elseif($formulario['pregunta168']=='SI' && $formulario['pregunta527']=='NO'){
		$res['noSubcontrata']=2;	
	}

	if($formulario['pregunta178']=='SI' && $formulario['pregunta542']=='SI'){
		$res['siSubcontrata']=3;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta576']);
	}elseif($formulario['pregunta178']=='SI' && $formulario['pregunta542']=='NO'){
		$res['noSubcontrata']=3;	
	}

	if($formulario['pregunta189']=='SI' && $formulario['pregunta528']=='SI'){
		$res['siSubcontrata']=4;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta577']);
	}elseif($formulario['pregunta189']=='SI' && $formulario['pregunta528']=='NO'){
		$res['noSubcontrata']=4;	
	}

	if($formulario['pregunta200']=='SI' && $formulario['pregunta529']=='SI'){
		$res['siSubcontrata']=5;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta578']);
	}elseif($formulario['pregunta200']=='SI' && $formulario['pregunta529']=='NO'){
		$res['noSubcontrata']=5;	
	}

	if($formulario['pregunta210']=='SI' && $formulario['pregunta530']=='SI'){
		$res['siSubcontrata']=6;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta579']);
	}elseif($formulario['pregunta210']=='SI' && $formulario['pregunta530']=='NO'){
		$res['noSubcontrata']=6;	
	}

	if($formulario['pregunta220']=='SI' && $formulario['pregunta531']=='SI'){
		$res['siSubcontrata']=7;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta609']);
	}elseif($formulario['pregunta220']=='SI' && $formulario['pregunta531']=='NO'){
		$res['noSubcontrata']=7;	
	}

	if($formulario['pregunta230']=='SI' && $formulario['pregunta532']=='SI'){
		$res['siSubcontrata']=8;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta580']);
	}elseif($formulario['pregunta230']=='SI' && $formulario['pregunta532']=='NO'){
		$res['noSubcontrata']=8;	
	}

	if($formulario['pregunta240']=='SI' && $formulario['pregunta533']=='SI'){
		$res['siSubcontrata']=9;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta581']);
	}elseif($formulario['pregunta240']=='SI' && $formulario['pregunta533']=='NO'){
		$res['noSubcontrata']=9;	
	}		

	if($formulario['pregunta250']=='SI' && $formulario['pregunta534']=='SI'){
		$res['siSubcontrata']=10;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta582']);
	}elseif($formulario['pregunta250']=='SI' && $formulario['pregunta534']=='NO'){
		$res['noSubcontrata']=10;	
	}	

	if($formulario['pregunta260']=='SI' && $formulario['pregunta535']=='SI'){
		$res['siSubcontrata']=11;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta583']);
	}elseif($formulario['pregunta260']=='SI' && $formulario['pregunta535']=='NO'){
		$res['noSubcontrata']=11;	
	}

	if($formulario['pregunta270']=='SI' && $formulario['pregunta536']=='SI'){
		$res['siSubcontrata']=12;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta584']);
	}elseif($formulario['pregunta270']=='SI' && $formulario['pregunta536']=='NO'){
		$res['noSubcontrata']=12;	
	}	

	if($formulario['pregunta280']=='SI' && $formulario['pregunta537']=='SI'){
		$res['siSubcontrata']=13;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta585']);
	}elseif($formulario['pregunta280']=='SI' && $formulario['pregunta537']=='NO'){
		$res['noSubcontrata']=13;	
	}

	if($formulario['pregunta290']=='SI' && $formulario['pregunta538']=='SI'){
		$res['siSubcontrata']=14;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta586']);
	}elseif($formulario['pregunta290']=='SI' && $formulario['pregunta538']=='NO'){
		$res['noSubcontrata']=14;	
	}

	if($formulario['pregunta487']=='SI' && $formulario['pregunta571']=='SI'){
		$res['siSubcontrata']=15;
		$res['subencargados']=tieneSubencargado($res['subencargados'],$formulario['pregunta587']);
	}elseif($formulario['pregunta487']=='SI' && $formulario['pregunta571']=='NO'){
		$res['noSubcontrata']=15;	
	}

	$encargados=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$codigo,true);
	while($item=mysql_fetch_assoc($encargados)){
		$res['noSubcontrata']=$item['codigo'];
		if($item['subcontratacionEncargado']=='SI'){
			$res['subencargados']=tieneSubencargado($res['subencargados'],$item['ficheroEncargado']);
			$res['siSubcontrata']=$item['codigo'];
		}
	}
	//print_r($res);

	return $res;
}

function tieneSubencargado($subencargados,$ficheros){
	$ficheros=explode('&$&',$ficheros);
	if($subencargados=='NO'){
		foreach ($ficheros as $f) {
			$responsables=consultaBD('SELECT COUNT(codigo) AS total FROM responsables_fichero WHERE codigoDeclaracion="'.$f.'";',true,true);
			if($responsables['total']>1){
				$subencargados='SI';
			}
		}
	}
	return $subencargados;
}

function recogerFicheroContrato($ficheros){
	$ficheros=explode('&$&', $ficheros);
	$res='';
	$i=0;
	$tipos=array(''=>'',1=>'Automatizado',2=>'Manual',3=>'Mixto');
	foreach ($ficheros as $key => $value) {
		$fichero=datosRegistro('declaraciones',$value);
		$res.='<p>* El fichero que contiene los datos es '.$fichero['nombreFichero'].'.</p>';
		$res.='<p>* Los datos que contienen son los siguientes: '.definirDatosFicheros($fichero).'.</p>';
		$res.='<p>* Sistema de tratamiento: '.$tipos[$fichero['tratamiento']].'</p>';
		$i++;
	}
	if($res==''){
		$res='<p> * Ningún fichero contiene los datos.</p>';
	}
	return $res;
}


function definirDatosFicheros($fichero,$identificativos=true){
	$res='';

	if($identificativos){

		$nombres=array(
			'checkDatosProtegidos0'			=>	'Ideología',
			'checkDatosProtegidos1'			=>	'Afiliación sindical',
			'checkDatosProtegidos2'			=>	'Religión',
			'checkDatosProtegidos3'			=>	'Creencias',
			'checkDatosEspeciales0'			=>	'Origen racial o Étnico',
			'checkDatosEspeciales1'			=>	'Salud',
			'checkDatosEspeciales2'			=>	'Vida sexual',
			'checkDatosIdentificativos0'	=>	'NIF / DNI',
			'checkDatosIdentificativos1'	=>	'Nº SS / Mutualidad',
			'checkDatosIdentificativos2'	=>	'Nombre y apellidos',
			'checkDatosIdentificativos3'	=>	'Tarjeta sanitaria',
			'checkDatosIdentificativos4'	=>	'Dirección',
			'checkDatosIdentificativos5'	=>	'Teléfono',
			'checkDatosIdentificativos6'	=>	'Firma',
			'checkDatosIdentificativos7'	=>	'Huella',
			'checkDatosIdentificativos8'	=>	'Imagen / voz',
			'checkDatosIdentificativos9'	=>	'Marcas físicas',
			'checkDatosIdentificativos10'	=>	'Firma electrónica',
			'checkDatosIdentificativos11'	=>	'Correo electrónico',
			'checkDatosIdentificativos12'	=>	'Otros datos biométricos',
			'checkDatosIdentificativos13'	=>	'Otros datos de carácter identificativo'
		);
	}
	else{
		$nombres=array(
			'checkDatosProtegidos0'			=>	'Ideología',
			'checkDatosProtegidos1'			=>	'Afiliación sindical',
			'checkDatosProtegidos2'			=>	'Religión',
			'checkDatosProtegidos3'			=>	'Creencias',
			'checkDatosEspeciales0'			=>	'Origen racial o Étnico',
			'checkDatosEspeciales1'			=>	'Salud',
			'checkDatosEspeciales2'			=>	'Vida sexual'
		);
	}	
	
	$campos=array_keys($nombres);

	foreach ($campos as $value) {
		if($fichero[$value]!='NO'){
			$res.=$nombres[$value].', ';
		}
	}


	if($identificativos){
		if($fichero['otrosTiposDatos']=='SI'){
			$otrosTipos=array('0'=>'Características Personales','1'=>'Académicos y profesionales','2'=>'Detalles del empleo','3'=>'Económicos, financieros y de seguros');
			for($i=0;$i<4;$i++){
				if($fichero['checkOtrosTiposDatos'.$i]!='NO'){
					$res.=$otrosTipos[$i].', ';
				}
			}
			
			if($fichero['desc_otros_tipos']!=''){
				$res.=$fichero['desc_otros_tipos'].', ';
			}
		}
	}

	if($res!=''){
		$res=quitaUltimaComa($res);
	}
	else{
		$res='Ninguno';
	}


	return $res;
}

function zipContratoSuscribirloEncargadoTratamientoLOPD($codigo, $PHPWord){
	$registros=explode(',', $_POST['registros']);
	$encargadosfijos=array();
	$encargados=array();
	$encargadosdecl=array();
	foreach ($registros as $key => $value) {
		$parts=explode('_', $value);
		if($parts[0]=='ENCARGADO'){
			array_push($encargados, $parts[1]);
		} else if($parts[0]=='ENCARGADODECL'){
			array_push($encargadosdecl, $parts[1]);
		}else {
			array_push($encargadosfijos, $parts[1]);
		}
	}
	$documentos=array();
	$i=1;
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario, ficheroLogo, trabajos.codigo AS codigoTrabajo, clientes.email FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	foreach ($encargadosfijos as $key => $value) {
		$documento=anexoContratoSuscribirloEncargadoTratamientoLOPDWord($datos,$i,$value,$PHPWord);
		if($documento!=''){
			array_push($documentos, $documento);
			$i++;
		}
	}
	if(count($encargados)>0){
		$encargados=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigo IN('.implode(',', $encargados).');',true);
		while($encargado=mysql_fetch_assoc($encargados)){
			array_push($documentos, anexoContratoSuscribirloEncargadoTratamientoLOPDWord2($datos,$i,$encargado,$PHPWord));	
			$i++;
		}
	}
	if(count($encargadosdecl)>0){
		foreach ($encargadosdecl as $key => $value) {
			array_push($documentos, anexoContratoSuscribirloEncargadoTratamientoLOPDWord3($datos,$i,$value,$PHPWord));	
			$i++;
		}
	}
	$zip = new ZipArchive();
    $nameZip = '../documentos/consultorias/contratos_encargados.zip';
    if(file_exists($nameZip)){
        unlink($nameZip);
    }
    $ficheroZip = $nameZip;
    if($zip->open($ficheroZip,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $doc){
            $zip->addFile('../documentos/consultorias/'.$doc , $doc);
        }
    }
    if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }
	
	header("Content-Type: application/zip");
    header("Content-Disposition: attachment; filename=contratos_encargados.zip");
    header("Content-Transfer-Encoding: binary");

    readfile($nameZip);
}

function anexoContratoSuscribirloEncargadoTratamientoLOPD($datos,$i,$n){
	global $_CONFIG;
	$fichero='CONTRATO_EMITIDO_PARA_SUSCRIBIRLO_CON_EL_ENCARGADO_DEL_TRATAMIENTO_'.$i.'.pdf';
	$sitio=array('N'=>'','NULL'=>'','ENCARGADO'=>'Locales del encargado','RESPONSABLE'=>'Locales del responsable','REMOTO'=>'Mediante acceso remoto');
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);

	if($_CONFIG['usuarioBD']=='root'){
    	$logo = "<img class='logo' src='../documentos/logos-clientes/imagenDefecto.png'/>";
    } else {
    	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos['ficheroLogo']);
    	if($hayLogo!='NO'){
    		$logo = "<img class='logo' alt='".$datos['razonSocial']."' src='../documentos/logos-clientes/".$datos['ficheroLogo']."'/>";
		} else {
			$logo = "<img class='logo' src='../documentos/logos-clientes/imagenDefecto.png'/>";
		}
	}
$i=1;
$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: Calibri;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        #cabecera{
	        	margin-bottom:20px;
	    	}

	        #datosCabecera{
	        	width:50%;
	        	position:absolute;
	        	right:-20px;
	        	top:20px;
	    	}

	    	#datosCabecera td{
	    		padding-right:10px;
	    	}

	    	#datosCabecera .dato{
	    		border-bottom:1px solid #000;
	    		width:200px;
	    		padding:0px;
	    		text-align:right;
	    	}

	    	.titulo{
	    		font-size:17px;
	    		font-weight:bold;
	    		text-align:center;
	    		color:#15284B;
	    		background:#EDF1F9;
	    		padding:10px;
	    	}

	    	h1{
	    		color:#15284B;
	    		font-size:12px;
	    		text-decoration:underline;
	    		text-align:center;
	    		margin-bottom:10px;
	    		margin-top:15px;
	    	}
	    	
	    	p{
	    		line-height: 20px;
	    		margin-bottom:3px;
	    		margin-top:3px;
	    		text-align: justify;
	    	}

	    	#tablaFirmas{
	    		width:100%;
	    		margin-top:100px;
	    	}

	    	#tablaFirmas td{
	    		width:50%;
	    		text-align:center;
	    	}

	    	ul{
	    		margin-left:-50px;
	    	}

	    	ul li{
	    		padding-left:22px;
	    		padding-top:5px;
	    	}

	    	ul .marcado{
	    		background:url(../img/check.png) left top no-repeat;
	    	}

	    	.logo{
	    		width:100px;
	    	}

	    	#tablaCabecera{
	    		border:1px solid #000;
	    		border-collapse:collapse;
	    		width:82%;
	    		margin-left:70px;
	    	}

	    	#tablaCabecera td{
	    		border:1px solid #000;
	    		text-align:center;
	    	}

	    	#tablaCabecera .lado{
	    		width:20%;
	    	}
	    	#tablaCabecera .centro{
	    		width:60%;
	    	}
	    	#tablaCabecera .completa{
	    		width:100%;
	    	}
	-->
	</style>";
	$campos=array(
		158=>array(511,159,160,161,164,163,167,543,574,544,541,162),
		168=>array(512,169,170,171,174,173,177,545,575,546,527,172),
		178=>array(513,179,180,181,184,183,187,547,576,548,542,182),
		189=>array(514,190,191,192,195,194,198,549,577,550,528,193),
		200=>array(515,201,202,203,206,205,209,551,578,552,529,204),
		210=>array(516,211,212,213,216,215,219,553,579,554,530,214),
		220=>array(517,221,222,223,226,225,229,555,609,556,531,224),
		230=>array(518,231,232,233,236,235,239,557,580,558,532,234),
		240=>array(519,241,242,243,246,245,249,559,581,560,533,244),
		250=>array(520,251,252,253,256,255,259,561,582,562,534,254),
		260=>array(521,261,262,263,266,265,269,563,583,564,535,264),
		270=>array(522,271,272,273,276,275,279,565,584,566,536,274),
		280=>array(523,281,282,283,286,285,289,567,585,568,537,284),
		290=>array(524,291,292,293,296,295,299,569,586,570,538,294),
		487=>array(525,488,489,490,493,492,496,572,587,544,571,491)
	);
	$servicio=array(158=>'Asesoría laboral para la realización de nóminas',168=>'Asesoría contable/fiscal',178=>'Video vigilancia',189=>'PRL',200=>'Mantenimiento de equipos informáticos',210=>'Mantenimiento de aplicaciones',220=>'Mantenimiento de la página web',230=>'Copias de seguridad "Backup"',240=>'Transporte/mensajería para envíos a clientes',250=>'Limpieza',260=>'Mantenimiento de instalaciones',270=>'Vigilancia/seguridad',280=>'Mantenimiento de la copiadora',290=>'Colaboración mercantil',487=>'Selección de personal');
	$nombreRL=$formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}
	if($formulario['pregunta'.$n]=='SI'){
		$sitios=explode('&$&', $formulario['pregunta'.$campos[$n][0]]);
		$sitiosTexto='';
		foreach ($sitios as $key => $value) {
			if($sitiosTexto!=''){
				$sitiosTexto.=', ';
			}
			$sitiosTexto.=$sitio[$value];
		}
		$servicio=$servicio[$n];
		$contenido.="<page footer='page' backtop='22mm' backbottom='18mm' backleft='18mm' backright='18mm'><br/><br/>
		<page_header>
			<table id='tablaCabecera'>
				<tr>
					<td rowspan='2' class='lado'><img class='logo' src='../img/logoMDiaz.png' alt='QMA Consultores'></td>
					<td class='centro'>Titular: <b>".$datos['razonSocial']."</b></td>
					<td rowspan='2' class='lado'>".$logo."</td>
				</tr>
				<tr>
					<td class='centro' style='border-left:0px;'><b>Documento del sistema de seguridad (R.G.P.D.)</b></td>
				</tr>
				<tr>
					<td colspan='3' class='completa'>Registro: CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO</td>
				</tr>
			</table>
		</page_header>
		<div class='titulo'>CONTRATO EMITIDO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO.</div>
		<p>De una parte:</p>
		<p><b>* Como responsable del fichero: ".$datos['razonSocial']."</b></p>
		<p>- Con CIF: ".$datos['cif']."</p>
		<p>- Dirección: ".$datos['domicilio']."</p>
		<p>- Código Postal: ".$datos['cp']."</p>
		<p>- Localidad: ".$datos['localidad']."</p>		
		<p>- Provincia: ".convertirMinuscula($datos['provincia'])."</p>
		<p>- Representante: ".$nombreRL."</p>
		<p>Y de otra parte:</p>
		<p><b>* Como encargado del tratamiento: ".$formulario['pregunta'.$campos[$n][1]]."</b></p>
		<p>- Con CIF/NIF: ".$formulario['pregunta'.$campos[$n][2]]."</p>
		<p>- Dirección: ".$formulario['pregunta'.$campos[$n][3]]."</p>
		<p>- Código Postal: ".$formulario['pregunta'.$campos[$n][4]]."</p>
		<p>- Localidad: ".$formulario['pregunta'.$campos[$n][5]]."</p>		
		<p>- Provincia: ".convertirMinuscula($formulario['pregunta'.$campos[$n][5]])."</p>
		<p>- Representante legal: ".$formulario['pregunta'.$campos[$n][6]]."</p>
		<p>Ambas partes contratantes se reconocen recíprocamente con capacidad legal necesaria para suscribir, con fecha ".$formulario['pregunta'.$campos[$n][7]].", el presente contrato de encargado del tratamiento, regulado por artículo 28 del RGPD – Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos- y MANIFIESTAN:</p>
			<ul style='list-style:none;'>
			<li>I.	Que  ".$datos['razonSocial']." -en lo sucesivo responsable del tratamiento- necesita contratar los servicios de la empresa externa ".$formulario['pregunta'.$campos[$n][1]].".</li>
			<li>II.	Que mediante lo establecido en el presente contrato, se habilita a la entidad ".$formulario['pregunta'.$campos[$n][1]]." -en lo sucesivo encargada del tratamiento- a tratar por cuenta del responsable del tratamiento los datos de carácter personal necesarios para prestar los servicios contratados, concretados en la cláusula primera del presente documento.</li>
			</ul>				
		<h1>CLÁUSULAS:</h1>
		<p><b>PRIMERA. Objeto del encargo del tratamiento.</b></p>
		<p>El encargado del tratamiento tratará los datos de carácter personal del responsable del tratamiento que resulten necesarios para prestar el servicio siguiente: ".$servicio.".</p>
		<p>El encargado del tratamiento tratará los datos en los ".$sitiosTexto."</p>
		<p>Concreción de los tratamientos a realizar: recogida, registro, estructuración, modificación, conservación, interconexión, consulta, cotejo, análisis de conducta, comunicación, supresión, destrucción, comunicación por transmisión al responsable del fichero.</p>

	<page_footer>
		
	</page_footer>
	</page>
	<page footer='page' backtop='25mm' backbottom='18mm' backleft='18mm' backright='18mm'><br/><br/>
		<page_header>
			<table id='tablaCabecera'>
				<tr>
					<td rowspan='2' class='lado'><img class='logo' src='../img/logoMDiaz.png' alt='QMA Consultores'></td>
					<td class='centro'>Titular: <b>".$datos['razonSocial']."</b></td>
					<td rowspan='2' class='lado'>".$logo."</td>
				</tr>
				<tr>
					<td class='centro' style='border-left:0px;'><b>Documento del sistema de seguridad (R.G.P.D.)</b></td>
				</tr>
				<tr>
					<td colspan='3' class='completa'>Registro: CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO</td>
				</tr>
			</table>
		</page_header>
		<p><b>SEGUNDA. Identificación de la información afectada.</b></p>
		<p>Para la ejecución de las prestaciones derivadas del cumplimiento del objeto de este encargo, el responsable del tratamiento pone a disposición del encargado del tratamiento, la siguiente información sobre los datos personales que va a tratar:</p>
		".recogerFicheroContrato($formulario['pregunta'.$campos[$n][8]])."
		<p><b>TERCERA. Duración.</b></p>	
		<p>El presente acuerdo se inicia a partir de la fecha de firma del presente documento, finalizando el día ".$formulario['pregunta'.$campos[$n][9]].".</p>
		<p>Una vez finalice el presente contrato, el encargado del tratamiento debe devolver al responsable los datos personales y suprimir cualquier copia que esté en su poder.</p>
		<p><b>CUARTA. Obligaciones del encargado del tratamiento.</b></p>
		<p>El encargado del tratamiento y todo su personal se obliga a:</p>
			<ul style='list-style:none;'>
			<li>a)	Utilizar los datos personales objeto de tratamiento, o los que recoja para su inclusión, sólo para la finalidad objeto de este encargo. En ningún caso podrá utilizar los datos para fines propios.</li>
			<li>b)	Tratar los datos de acuerdo con las instrucciones del responsable del tratamiento. Si el encargado del tratamiento considera que aluna de las instrucciones infringe el RGPD o cualquier otra disposición en materia de protección de datos de la Unión o de los Estados miembros, el encargado informará inmediatamente al responsable.</li>
			<li>c)	Con la información facilitada por el responsable del fichero en la CLÁUSULA SEGUNDA del presente documento, el encargado del tratamiento debe proceder a cumplimentar aquellos registros que contempla el documento de seguridad que está obligado a mantener actualizado el encargado del tratamiento.<br/>Al realizarse el tratamiento de los datos personales en los locales del encargado del tratamiento, y con el fin de acreditar las medidas de seguridad implantadas y actualizadas por el encargado del tratamiento, este deberá remitir al responsable del fichero el documento de seguridad donde figuren dichas medidas, así como la identificación del fichero. </li>
			<li>d)	No comunicar los datos a terceras personas, salvo que cuente con la autorización expresa del responsable del tratamiento, en los supuestos legalmente admisibles.</li>";
			if($formulario['pregunta'.$campos[$n][10]]=='SI'){
				$contenido.="<li>e)	Sí está permitida la subcontratación. Para subcontratar con otras empresas, el encargado debe comunicarlo por escrito al responsable, identificando de forma clara e inequívoca la empresa subcontratista y sus datos de contacto. La subcontratación podrá llevarse a cabo si el responsable no manifiesta su oposición en el plazo de 7 días hábiles.</li>";
			} else {
				$contenido.="<li>e)	No subcontratar ninguna de las prestaciones que formen parte del objeto de este contrato que comporten el tratamiento de datos personales, salvo los servicios auxiliares necesarios para el normal funcionamiento de los servicios del encargado.</li>";
			}
$contenido.="<li>f)	Mantener el deber de secreto respecto a los datos de carácter personal a los que haya tenido acceso en virtud del presente encargo, incluso después de que finalice su objeto.</li>
			<li>g)	Garantizar que las personas autorizadas para tratar datos personales se comprometan, de forma expresa y por escrito, a respetar la confidencialidad y a cumplir las medidas de seguridad correspondientes, de las que hay que informarles convenientemente.</li>
			<li>h)	Mantener a disposición del responsable la documentación acreditativa del cumplimiento de la obligación establecida en el apartado anterior.</li>
			<li>i)	Garantizar la formación necesaria en materia de protección de datos personales de las personas autorizadas para tratar datos personales.</li>
			<li>j)	Asistir al responsable del tratamiento en la respuesta al ejercicio de los derechos de:<br/>-	Acceso, rectificación, supresión y oposición.<br/>-	Limitación del tratamiento.<br/>-	Portabilidad de datos.<br/>-	A no ser objeto de decisiones individualizadas automatizadas (incluida la elaboración de perfiles).<br/><br/>Cuando las personas afectadas ejerzan los derechos de acceso, rectificación, supresión y oposición, limitación del tratamiento, portabilidad de datos y a no ser objeto de decisiones individualizadas automatizadas, ante el encargado del tratamiento, éste debe comunicarlo por correo electrónico a la dirección ".$formulario['pregunta'.$campos[$n][11]].". La comunicación debe hacerse de forma inmediata y en ningún caso más allá del día laborable siguiente al de la recepción de la solicitud, juntamente, en su caso, con otras informaciones que puedan ser relevantes para resolver la solicitud.</li>		
			<li>k)	Registrando aquellas rectificaciones o exclusiones (por cancelación u oposición) que el responsable le comunique como consecuencia de peticiones de aquellos interesados cuyos datos esté tratando.</li>
			<li>l)	Derecho de información. El encargado del tratamiento, en el momento de la recogida de los datos, debe facilitar por escrito al interesado la información relativa a los tratamientos de datos que se van a realizar.</li>
			<li>m)	El encargado del tratamiento notificará al responsable del tratamiento, sin dilación indebida y en cualquier caso antes de que transcurran 72 horas, las violaciones de la seguridad de los datos personales a su cargo de las que tenga conocimiento, juntamente  con toda la información relevante para la documentación y comunicación de la incidencia.<br/>Se facilitará como mínimo, la información siguiente en el supuesto que se disponga de ella:<br/>-	Descripción de la naturaleza de la violación de la seguridad de los datos personales, inclusive, cuando sea posible, las categorías y el número aproximado de interesados afectados, y las categorías y el número aproximado de interesados afectados, y las categorías y el número aproximado de registros de datos personales afectados.<br/>-	Descripción de las posibles consecuencias o propuestas para poner remedio a la violación de la seguridad de los datos personales, incluyendo, si procede, las medidas adoptadas para mitigar los posibles efectos negativos.<br/>-	Si no es posible facilitar la información simultáneamente, y en la medida en que no lo sea, la información se facilitará de manera gradual sin dilación indebida.</li>
			<li>n)	Poner a disposición del responsable toda la información necesaria para demostrar el cumplimiento de sus obligaciones, así como para la realización de las auditorías o las inspecciones que realicen el responsable y otro auditor autorizado por él.</li>
			<li>o)	Cumplir todos los requisitos exigidos por la Ley 15/1999 de 13 de diciembre, de Protección de Datos de carácter personal y, en todo caso, implantar mecanismos para:<br/>-	Garantizar la confidencialidad, integridad y disponibilidad de los sistemas y servicios de tratamiento.<br/>-	Restaurar la disponibilidad y el acceso a los datos personales de forma rápida, en caso de incidente físico o técnico.<br/>-	Verificar, evaluar y valorar, de forma regular, la eficacia de las medidas técnicas y organizativas implantadas para garantizar la seguridad del tratamiento.<br/>-	Cifrar los datos personales, en su caso.</li>
			<li>p)	Devolver al responsable del tratamiento los datos de carácter personal y, si procede, los soportes donde consten, una vez cumplida la prestación.<br/>La devolución debe comportar el borrado total de los datos existentes en los equipos informáticos utilizados por el encargado.<br/>No obstante, el encargado puede conservar una copia, con los datos debidamente bloqueados, mientras puedan derivarse responsabilidades de la ejecución de la prestación.</li>
			</ul>	
		<p><b>QUINTA. Obligaciones del responsable del tratamiento.</b></p>
		<p>Corresponde al responsable del tratamiento:</p>
			<ul style='list-style:none;'>
			<li>a)	Entregar al encargado los datos necesarios para la prestación del servicio a los que se refiere la CLÁUSULA PRIMERA del presente documento.</li>
			<li>b)	Supervisar el tratamiento, incluida la realización de inspecciones y auditorías.</li>
			</ul>
		<p>En prueba de conformidad con todas sus partes, firman el presente contrato el ".$formulario['pregunta'.$campos[$n][9]].".</p>
		<table id='tablaFirmas'>
			<tr>
				<td><b>Como responsable del fichero</b></td>
				<td><b>Como encargado del tratamiento</b></td>			
			</tr>
		</table>
		<table id='tablaFirmas'>
			<tr>
				<td><b>D./ª. ".$nombreRL."</b></td>
				<td><b>D./ª. ".$formulario['pregunta'.$campos[$n][6]]."</b></td>			
			</tr>
			<tr>
				<td>REPRESENTANTE DEL RESPONSABLE</td>
				<td>REPRESENTANTE DEL ENCARGADO</td>
			</tr>
		</table>
			<page_footer>

			</page_footer>
		</page>";
		$i++;

require_once('../../api/html2pdf/html2pdf.class.php');
$html2pdf=new HTML2PDF('P','A4','es');
$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->setDefaultFont("calibri");
$html2pdf->WriteHTML($contenido);
$html2pdf->Output('../documentos/consultorias/'.$fichero,'f');

} else {
	$fichero='';
}

return $fichero;
}

function anexoContratoSuscribirloEncargadoTratamientoLOPD2($datos,$i,$encargado){
	global $_CONFIG;
	$fichero='CONTRATO_EMITIDO_PARA_SUSCRIBIRLO_CON_EL_ENCARGADO_DEL_TRATAMIENTO_'.$i.'.pdf';
	$sitio=array('N'=>'','NULL'=>'','ENCARGADO'=>'Locales del encargado','RESPONSABLE'=>'Locales del responsable','REMOTO'=>'Mediante acceso remoto');
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);

	if($_CONFIG['usuarioBD']=='root'){
    	$logo = "<img class='logo' src='../documentos/logos-clientes/imagenDefecto.png'/>";
    } else {
    	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos['ficheroLogo']);
    	if($hayLogo!='NO'){
    		$logo = "<img class='logo' alt='".$datos['razonSocial']."' src='../documentos/logos-clientes/".$datos['ficheroLogo']."'/>";
		} else {
			$logo = "<img class='logo' src='../documentos/logos-clientes/imagenDefecto.png'/>";
		}
	}
$i=1;
$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: Calibri;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        #cabecera{
	        	margin-bottom:20px;
	    	}

	        #datosCabecera{
	        	width:50%;
	        	position:absolute;
	        	right:-20px;
	        	top:20px;
	    	}

	    	#datosCabecera td{
	    		padding-right:10px;
	    	}

	    	#datosCabecera .dato{
	    		border-bottom:1px solid #000;
	    		width:200px;
	    		padding:0px;
	    		text-align:right;
	    	}

	    	.titulo{
	    		font-size:17px;
	    		font-weight:bold;
	    		text-align:center;
	    		color:#15284B;
	    		background:#EDF1F9;
	    		padding:10px;
	    	}

	    	h1{
	    		color:#15284B;
	    		font-size:12px;
	    		text-decoration:underline;
	    		text-align:center;
	    		margin-bottom:10px;
	    		margin-top:15px;
	    	}
	    	
	    	p{
	    		line-height: 20px;
	    		margin-bottom:3px;
	    		margin-top:3px;
	    		text-align: justify;
	    	}

	    	#tablaFirmas{
	    		width:100%;
	    		margin-top:100px;
	    	}

	    	#tablaFirmas td{
	    		width:50%;
	    		text-align:center;
	    	}

	    	ul{
	    		margin-left:-50px;
	    	}

	    	ul li{
	    		padding-left:22px;
	    		padding-top:5px;
	    	}

	    	ul .marcado{
	    		background:url(../img/check.png) left top no-repeat;
	    	}

	    	.logo{
	    		width:100px;
	    	}

	    	#tablaCabecera{
	    		border:1px solid #000;
	    		border-collapse:collapse;
	    		width:82%;
	    		margin-left:70px;
	    	}

	    	#tablaCabecera td{
	    		border:1px solid #000;
	    		text-align:center;
	    	}

	    	#tablaCabecera .lado{
	    		width:20%;
	    	}
	    	#tablaCabecera .centro{
	    		width:60%;
	    	}
	    	#tablaCabecera .completa{
	    		width:100%;
	    	}
	-->
	</style>";
	
			$sitios=explode('&$&', $encargado['dondeEncargado']);
			$sitiosTexto='';
			foreach ($sitios as $key => $value) {
				if($sitiosTexto!=''){
					$sitiosTexto.=', ';
				}
				$sitiosTexto.=$sitio[$value];
			}
			$nombreRL=$formulario['pregunta8'];
			if(isset($formulario['pregunta627'])){
				$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
			}
			$servicio=$encargado['servicioEncargado'];
			$contenido.="<page page='footer' backbottom='18mm' backleft='18mm' backright='18mm'><br/><br/>
			<img class='logo' src='../img/logoMDiaz.png' alt='QMA Consultores'> PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL<br/><br/>
			<div class='titulo'>CONTRATO EMITIDO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO.</div>
			<p>De una parte:</p>
			<p><b>* Como responsable del fichero: ".$datos['razonSocial']."</b></p>
			<p>- Con CIF: ".$datos['cif']."</p>
			<p>- Dirección: ".$datos['domicilio']."</p>
			<p>- Código Postal: ".$datos['cp']."</p>
			<p>- Localidad: ".$datos['localidad']."</p>		
			<p>- Provincia: ".convertirMinuscula($datos['provincia'])."</p>
			<p>- Representante: ".$nombreRL."</p>
			<p>Y de otra parte:</p>
			<p><b>* Como encargado del tratamiento: ".$encargado['nombreEncargado']."</b></p>
			<p>- Con CIF/NIF: ".$encargado['cifEncargado']."</p>
			<p>- Dirección: ".$encargado['direccionEncargado']."</p>
			<p>- Código Postal: ".$encargado['cpEncargado']."</p>
			<p>- Localidad: ".$encargado['localidadEncargado']."</p>		
			<p>- Provincia: ".$encargado['localidadEncargado']."</p>
			<p>- Representante legal: ".$encargado['representanteEncargado']."</p>
			<p>Ambas partes contratantes se reconocen recíprocamente con capacidad legal necesaria para suscribir, con fecha ".$encargado['fechaAltaEncargado'].", el presente contrato de encargado del tratamiento, regulado por artículo 28 del RGPD – Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos- y MANIFIESTAN:</p>
				<ul style='list-style:none;'>
					<li>I.	Que  ".$datos['razonSocial']." -en lo sucesivo responsable del tratamiento- necesita contratar los servicios de la empresa externa ".$encargado['nombreEncargado'].".</li>
					<li>II.	Que mediante lo establecido en el presente contrato, se habilita a la entidad ".$encargado['nombreEncargado']." -en lo sucesivo encargada del tratamiento- a tratar por cuenta del responsable del tratamiento los datos de carácter personal necesarios para prestar los servicios contratados, concretados en la cláusula primera del presente documento.</li>
				</ul>				
			<h1>CLÁUSULAS:</h1>
			<p><b>PRIMERA. Objeto del encargo del tratamiento.</b></p>
			<p>El encargado del tratamiento tratará los datos de carácter personal del responsable del tratamiento que resulten necesarios para prestar el servicio siguiente: ".$servicio.".</p>
			<p>El encargado del tratamiento tratará los datos en los ".$sitiosTexto."</p>
			<p>Concreción de los tratamientos a realizar: recogida, registro, estructuración, modificación, conservación, interconexión, consulta, cotejo, análisis de conducta, comunicación, supresión, destrucción, comunicación por transmisión al responsable del fichero.</p>

		<page_footer>
		
		</page_footer>
		</page>";
		$contenido.="<page page='footer' backtop='25mm' backbottom='18mm' backleft='18mm' backright='18mm'><br/><br/>
		<page_header>
			<table id='tablaCabecera'>
				<tr>
					<td rowspan='2' class='lado'><img class='logo' src='../img/logoMDiaz.png' alt='QMA Consultores'></td>
					<td class='centro'>Titular: <b>".$datos['razonSocial']."</b></td>
					<td rowspan='2' class='lado'>".$logo."</td>
				</tr>
				<tr>
					<td class='centro' style='border-left:0px;'><b>Documento del sistema de seguridad (R.G.P.D.)</b></td>
				</tr>
				<tr>
					<td colspan='3' class='completa'>Registro: CONTRATO PARA SUSCRIBIRLO CON EL ENCARGADO DEL TRATAMIENTO</td>
				</tr>
			</table>
		</page_header>		
			<p><b>SEGUNDA. Identificación de la información afectada.</b></p>
			<p>Para la ejecución de las prestaciones derivadas del cumplimiento del objeto de este encargo, el responsable del tratamiento pone a disposición del encargado del tratamiento, la siguiente información sobre los datos personales que va a tratar:</p>
			".recogerFicheroContrato($encargado['ficheroEncargado'])."
			<p><b>TERCERA. Duración.</b></p>";
			$fechaBaja=$encargado['fechaBajaEncargado']=='0000-00-00'?'':formateaFechaWeb($encargado['fechaBajaEncargado']);	
		$contenido.="<p>El presente acuerdo se inicia a partir de la fecha de firma del presente documento, finalizando el día ".$fechaBaja.".</p>
			<p>Una vez finalice el presente contrato, el encargado del tratamiento debe devolver al responsable los datos personales y suprimir cualquier copia que esté en su poder.</p>	
			<p><b>CUARTA. Obligaciones del encargado del tratamiento.</b></p>
			<p>El encargado del tratamiento y todo su personal se obliga a:</p>
				<ul style='list-style:none;'>
					<li>a)	Utilizar los datos personales objeto de tratamiento, o los que recoja para su inclusión, sólo para la finalidad objeto de este encargo. En ningún caso podrá utilizar los datos para fines propios.</li>
					<li>b)	Tratar los datos de acuerdo con las instrucciones del responsable del tratamiento.Si el encargado del tratamiento considera que aluna de las instrucciones infringe el RGPD o cualquier otra disposición en materia de protección de datos de la Unión o de los Estados miembros, el encargado informará inmediatamente al responsable.</li>
					<li>c)	Con la información facilitada por el responsable del fichero en la CLÁUSULA SEGUNDA del presente documento, el encargado del tratamiento debe proceder a cumplimentar aquellos registros que contempla el documento de seguridad que está obligado a mantener actualizado el encargado del tratamiento.<br/>Al realizarse el tratamiento de los datos personales en los locales del encargado del tratamiento, y con el fin de acreditar las medidas de seguridad implantadas y actualizadas por el encargado del tratamiento, este deberá remitir al responsable del fichero el documento de seguridad donde figuren dichas medidas, así como la identificación del fichero. </li>
					<li>d)	No comunicar los datos a terceras personas, salvo que cuente con la autorización expresa del responsable del tratamiento, en los supuestos legalmente admisibles.</li>";
				if($encargado['subcontratacionEncargado']=='SI'){
					$contenido.="<li>e)	Sí está permitida la subcontratación. Para subcontratar con otras empresas, el encargado debe comunicarlo por escrito al responsable, identificando de forma clara e inequívoca la empresa subcontratista y sus datos de contacto. La subcontratación podrá llevarse a cabo si el responsable no manifiesta su oposición en el plazo de 7 días hábiles.</li>";
				} else {
					$contenido.="<li>e)	No subcontratar ninguna de las prestaciones que formen parte del objeto de este contrato que comporten el tratamiento de datos personales, salvo los servicios auxiliares necesarios para el normal funcionamiento de los servicios del encargado.</li>";
				}
				$contenido.="	<li>f)	Mantener el deber de secreto respecto a los datos de carácter personal a los que haya tenido acceso en virtud del presente encargo, incluso después de que finalice su objeto.</li>
				<li>g)	Garantizar que las personas autorizadas para tratar datos personales se comprometan, de forma expresa y por escrito, a respetar la confidencialidad y a cumplir las medidas de seguridad correspondientes, de las que hay que informarles convenientemente.</li>
				<li>h)	Mantener a disposición del responsable la documentación acreditativa del cumplimiento de la obligación establecida en el apartado anterior.</li>
				<li>i)	Garantizar la formación necesaria en materia de protección de datos personales de las personas autorizadas para tratar datos personales.</li>
				<li>j)	Asistir al responsable del tratamiento en la respuesta al ejercicio de los derechos de:<br/>-	Acceso, rectificación, supresión y oposición.<br/>-	Limitación del tratamiento.<br/>-	Portabilidad de datos.<br/>-	A no ser objeto de decisiones individualizadas automatizadas (incluida la elaboración de perfiles).<br/><br/>Cuando las personas afectadas ejerzan los derechos de acceso, rectificación, supresión y oposición, limitación del tratamiento, portabilidad de datos y a no ser objeto de decisiones individualizadas automatizadas, ante el encargado del tratamiento, éste debe comunicarlo por correo electrónico a la dirección ".$encargado['emailEncargado'].". La comunicación debe hacerse de forma inmediata y en ningún caso más allá del día laborable siguiente al de la recepción de la solicitud, juntamente, en su caso, con otras informaciones que puedan ser relevantes para resolver la solicitud.</li>
					
					<li>k)	Registrando aquellas rectificaciones o exclusiones (por cancelación u oposición) que el responsable le comunique como consecuencia de peticiones de aquellos interesados cuyos datos esté tratando.</li>
					<li>l)	Derecho de información. El encargado del tratamiento, en el momento de la recogida de los datos, debe facilitar por escrito al interesado la información relativa a los tratamientos de datos que se van a realizar.</li>
					<li>m)	El encargado del tratamiento notificará al responsable del tratamiento, sin dilación indebida y en cualquier caso antes de que transcurran 72 horas, las violaciones de la seguridad de los datos personales a su cargo de las que tenga conocimiento, juntamente  con toda la información relevante para la documentación y comunicación de la incidencia.<br/>Se facilitará como mínimo, la información siguiente en el supuesto que se disponga de ella:<br/>-	Descripción de la naturaleza de la violación de la seguridad de los datos personales, inclusive, cuando sea posible, las categorías y el número aproximado de interesados afectados, y las categorías y el número aproximado de interesados afectados, y las categorías y el número aproximado de registros de datos personales afectados.<br/>-	Descripción de las posibles consecuencias o propuestas para poner remedio a la violación de la seguridad de los datos personales, incluyendo, si procede, las medidas adoptadas para mitigar los posibles efectos negativos.<br/>-	Si no es posible facilitar la información simultáneamente, y en la medida en que no lo sea, la información se facilitará de manera gradual sin dilación indebida.</li>
					<li>n)	Poner a disposición del responsable toda la información necesaria para demostrar el cumplimiento de sus obligaciones, así como para la realización de las auditorías o las inspecciones que realicen el responsable y otro auditor autorizado por él.</li>
					<li>o)	Cumplir todos los requisitos exigidos por la Ley 15/1999 de 13 de diciembre, de Protección de Datos de carácter personal y, en todo caso, implantar mecanismos para:<br/>-	Garantizar la confidencialidad, integridad y disponibilidad de los sistemas y servicios de tratamiento.<br/>-	Restaurar la disponibilidad y el acceso a los datos personales de forma rápida, en caso de incidente físico o técnico.<br/>-	Verificar, evaluar y valorar, de forma regular, la eficacia de las medidas técnicas y organizativas implantadas para garantizar la seguridad del tratamiento.<br/>-	Cifrar los datos personales, en su caso.</li>
					<li>p)	Devolver al responsable del tratamiento los datos de carácter personal y, si procede, los soportes donde consten, una vez cumplida la prestación.<br/>La devolución debe comportar el borrado total de los datos existentes en los equipos informáticos utilizados por el encargado.<br/>No obstante, el encargado puede conservar una copia, con los datos debidamente bloqueados, mientras puedan derivarse responsabilidades de la ejecución de la prestación.</li>
				</ul>	
			<p><b>QUINTA. Obligaciones del responsable del tratamiento.</b></p>
			<p>Corresponde al responsable del tratamiento:</p>
				<ul style='list-style:none;'>
					<li>a)	Entregar al encargado los datos necesarios para la prestación del servicio a los que se refiere la CLÁUSULA PRIMERA del presente documento.</li>
					<li>b)	Supervisar el tratamiento, incluida la realización de inspecciones y auditorías.</li>
				</ul>
			<p>En prueba de conformidad con todas sus partes, firman el presente contrato el ".$encargado['fechaAltaEncargado'].".</p>
			<table id='tablaFirmas'>
				<tr>
					<td><b>Como responsable del fichero</b></td>
					<td><b>Como encargado del tratamiento</b></td>			
				</tr>
			</table>
			<table id='tablaFirmas'>
				<tr>
					<td><b>D./ª. ".$nombreRL."</b></td>
					<td><b>D./ª. ".$encargado['representanteEncargado']."</b></td>			
				</tr>
				<tr>
					<td>REPRESENTANTE DEL RESPONSABLE</td>
					<td>REPRESENTANTE DEL ENCARGADO</td>
				</tr>
			</table>
			<page_footer>

			</page_footer>
		</page>";
		$i++;
require_once('../../api/html2pdf/html2pdf.class.php');
$html2pdf=new HTML2PDF('P','A4','es');
$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->setDefaultFont("calibri");
$html2pdf->WriteHTML($contenido);
$html2pdf->Output('../documentos/consultorias/'.$fichero,'f');


return $fichero;
}

function anexoContratoSuscribirloEncargadoTratamientoLOPDWord($datos,$i,$n, $PHPWord){
	global $_CONFIG;
	$fichero='CONTRATO_EMITIDO_PARA_SUSCRIBIRLO_CON_EL_ENCARGADO_DEL_TRATAMIENTO_'.$i.'.docx';
	$sitio=array('N'=>'','NULL'=>'','ENCARGADO'=>'locales del encargado','RESPONSABLE'=>'locales del responsable','REMOTO'=>'mediante acceso remoto');
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$campos=array(
		158=>array(511,159,160,161,164,163,167,543,574,544,541,162,611,629),
		168=>array(512,169,170,171,174,173,177,545,575,546,527,172,612,630),
		178=>array(513,179,180,181,184,183,187,547,576,548,542,182,613,631),
		189=>array(514,190,191,192,195,194,198,549,577,550,528,193,614,632),
		200=>array(515,201,202,203,206,205,209,551,578,552,529,204,615,633),
		210=>array(516,211,212,213,216,215,219,553,579,554,530,214,616,634),
		220=>array(517,221,222,223,226,225,229,555,609,556,531,224,617,635),
		230=>array(518,231,232,233,236,235,239,557,580,558,532,234,618,636),
		240=>array(519,241,242,243,246,245,249,559,581,560,533,244,619,637),
		250=>array(520,251,252,253,256,255,259,561,582,562,534,254,620,638),
		260=>array(521,261,262,263,266,265,269,563,583,564,535,264,621,639),
		270=>array(522,271,272,273,276,275,279,565,584,566,536,274,622,640),
		280=>array(523,281,282,283,286,285,289,567,585,568,537,284,623,641),
		290=>array(524,291,292,293,296,295,299,569,586,570,538,294,624,642),
		487=>array(525,488,489,490,493,492,496,572,587,544,571,491,625,643),
	);
	$servicio=array(158=>'Asesoría laboral para la realización de nóminas',168=>'Asesoría contable/fiscal',178=>'Video vigilancia',189=>'PRL',200=>'Mantenimiento de equipos informáticos',210=>'Mantenimiento de aplicaciones',220=>'Mantenimiento de la página web',230=>'Copias de seguridad "Backup"',240=>'Transporte/mensajería para envíos a clientes',250=>'Limpieza',260=>'Mantenimiento de instalaciones',270=>'Vigilancia/seguridad',280=>'Mantenimiento de la copiadora',290=>'Colaboración mercantil',487=>'Selección de personal');
	if($formulario['pregunta'.$n]=='SI'){
		$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_ContratoSuscribir.docx');
		$sitios=explode('&$&', $formulario['pregunta'.$campos[$n][0]]);
		$sitiosTexto='';
		$localesEncargado='';
		foreach ($sitios as $key => $value) {
			if($sitiosTexto!=''){
				$sitiosTexto.=', ';
			}
			if($value=='REMOTO'){
				$sitiosTexto.=$sitio[$value];
			} else {
				$sitiosTexto.='en los '.$sitio[$value];
			}
			if($value=='ENCARGADO'){
				$localesEncargado='<w:br/>Al realizarse el tratamiento de los datos personales en los locales del encargado del tratamiento, y con el fin de acreditar las medidas de seguridad implantadas y actualizadas por el encargado del tratamiento, este deberá remitir al responsable del fichero el documento de seguridad donde figuren dichas medidas, así como la identificación del fichero.';
			}
		}
		$servicio=$servicio[$n];

		$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
		$documento->setValue("cif",utf8_decode($datos['cif']));
		$documento->setValue("direccion",utf8_decode($datos['domicilio']));
		$documento->setValue("cp",utf8_decode($datos['cp']));
		$documento->setValue("localidad",utf8_decode($datos['localidad']));
		$documento->setValue("provincia",utf8_decode($datos['provincia']));
		$nombreRL=$formulario['pregunta8'];
		if(isset($formulario['pregunta627'])){
			$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
		}
		$documento->setValue("representante",utf8_decode($nombreRL));

		$documento->setValue("encargado",utf8_decode(sanearCaracteres($formulario['pregunta'.$campos[$n][1]])));
		$documento->setValue("cif_e",utf8_decode($formulario['pregunta'.$campos[$n][2]]));
		$documento->setValue("direccion_e",utf8_decode($formulario['pregunta'.$campos[$n][3]]));
		$documento->setValue("cp_e",utf8_decode($formulario['pregunta'.$campos[$n][4]]));
		$documento->setValue("localidad_e",utf8_decode($formulario['pregunta'.$campos[$n][5]]));
		$documento->setValue("provincia_e",utf8_decode(obtieneProvincia($formulario['pregunta'.$campos[$n][12]])));
		$documento->setValue("representante_e",utf8_decode($formulario['pregunta'.$campos[$n][6]]));

		$documento->setValue("fechaInicio",utf8_decode($formulario['pregunta'.$campos[$n][7]]));
		$documento->setValue("servicio",utf8_decode($servicio));
		$documento->setValue("lugarTrabajo",utf8_decode($sitiosTexto));
		$documento->setValue("exclusivamente",utf8_decode(''));

		recogerFicheroContratoWord($formulario['pregunta'.$campos[$n][8]],$documento);

		$documento->setValue("fechaFin",utf8_decode($formulario['pregunta'.$campos[$n][9]]));

		if($formulario['pregunta'.$campos[$n][10]]=='SI'){
			$subcontratacion='';
			$listado=consultaBD('SELECT * FROM declaraciones_subcontrata WHERE tipo="ENCARGADOFIJO" AND codigoResponsable='.$n.' AND codigoTrabajo='.$datos['codigoTrabajo'],true);
			while($item=mysql_fetch_assoc($listado)){
				if($subcontratacion!=''){
					$subcontratacion.=', ';
				}
				$subcontratacion.=$item['empresa'];
			}
			$subcontratacion="Sí está permitida la subcontratación. Con las siguientes empresas: ".$subcontratacion;
		} else {
			$subcontratacion="No está permitida la subcontratación";
		}

		$documento->setValue("subcontratacion",utf8_decode($subcontratacion));
		$documento->setValue("localesEncargado",utf8_decode($localesEncargado));
		$documento->setValue("email",utf8_decode($datos['email']));

		$tratamientos=consultaTratamientosRealiza($n,$datos['codigoTrabajo'],'SI');
		if($tratamientos!=''){
			$tratamientos='Concreción de los tratamientos a realizar: '.$tratamientos.'.';
		}
		if(isset($formulario['pregunta'.$campos[$n][13]]) && $formulario['pregunta'.$campos[$n][13]]=='SI'){
			$tratamientos='Los datos personales tratados por el encargado del tratamiento serán incorporados y tratados exclusivamente en sus sistemas.<w:br/><w:br/>'.$tratamientos;
		}
		$documento->setValue("tratamientosRealiza",utf8_decode($tratamientos));
		$documento->setValue("true",'');
			
		reemplazarLogo($documento,$datos,'image2.png');

		$documento->save('../documentos/consultorias/'.$fichero);
	} else {
		$fichero ='';
	}

	return $fichero;
}

function zipContratoSubencargadosLOPD($codigo, $PHPWord){
	$registros=explode(',', $_POST['registros']);
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario, ficheroLogo, trabajos.codigo AS codigoTrabajo, clientes.email FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$subencargados=array();
	foreach ($registros as $key => $value) {
		$parts=explode('_', $value);
		$item=array('subencargado'=>$parts[0],'tipo'=>$parts[1],'responsable'=>$parts[2],'declaracion'=>$parts[3]);
		array_push($subencargados,$item);
	}
	$documentos=array();
	$i=1;
	foreach ($subencargados as $s) {
		$documento=contratoSubencargadoLOPPDF($datos,$s,$i);
		if($documento!=''){
			array_push($documentos, $documento);
			$i++;
		}
	}
	$zip = new ZipArchive();
    $nameZip = '../documentos/consultorias/contratos_encargados_subencargados.zip';
    if(file_exists($nameZip)){
        unlink($nameZip);
    }
    $ficheroZip = $nameZip;
    if($zip->open($ficheroZip,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $doc){
            $zip->addFile('../documentos/consultorias/'.$doc , $doc);
        }
    }
    if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }
	
	header("Content-Type: application/zip");
    header("Content-Disposition: attachment; filename=contratos_encargados.zip");
    header("Content-Transfer-Encoding: binary");

    readfile($nameZip);
}

function contratoSubencargadoLOPPDF($datos,$s,$i){
	global $_CONFIG;
	$fichero='CONTRATO_ENCARGADO_SUBENCARGADO_DEL_TRATAMIENTO_'.$i.'.pdf';
	$subencargado=datosRegistro('declaraciones_subcontrata',$s['subencargado']);
	if($subencargado['nif']=='B92598101'){
		$firmaMdiaz='<img style="height:100%;" src="../img/firmaMdiaz.jpg">';
	} else {
		$firmaMdiaz='';
	}
	$responsable=datosRegistro('responsables_fichero',$s['responsable']);
	$declaracion=datosRegistro('declaraciones',$s['declaracion']);
	if($s['tipo']=='FIJO'){
		$encargado=creaEncargadoParaSubEncargado($datos,$subencargado['codigoResponsable']);
	} else if($s['tipo']=='NOFIJO'){
		$encargado=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigo='.$subencargado['codigoResponsable'],true,true);
		$item=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$encargado['codigo'].' AND codigoTrabajo='.$datos['codigoTrabajo'].' AND fijo="NO";',true,true);
		foreach ($item as $key => $value) {
			if(strpos($key,'check')!==false){
				$encargado[$key]=$value;
			}
		}
	}
	if($encargado['servicioEncargado']!=''){
		$servicio=$encargado['servicioEncargado'];
		$servicio=$subencargado['servicio'];
	} else {
		$servicio=$subencargado['servicio'];
	}
	$fecha=formateaFechaWeb($encargado['fechaAltaEncargado']);
	$fecha2='que cualquiera de la partes lo renunciare';
	if($encargado['fechaAltaEncargado']!='0000-00-00'){
		$fecha.=' hasta la fecha '.formateaFechaWeb($encargado['fechaBajaEncargado']);
		$fecha2=formateaFechaWeb($encargado['fechaBajaEncargado']);
	}
	$sitio=array('N'=>'','NULL'=>'','ENCARGADO'=>'locales del encargado','RESPONSABLE'=>'locales del responsable','REMOTO'=>'mediante acceso remoto');
	$sitios=explode('&$&', $encargado['dondeEncargado']);
	$sitiosTexto='';
	foreach ($sitios as $key => $value) {
		if($sitiosTexto!=''){
			$sitiosTexto.=', ';
		}
		if($value=='REMOTO'){
			$sitiosTexto.=$sitio[$value];
		} else {
			$sitiosTexto.='en los '.$sitio[$value];
		}
	}
	$tiposArray=array('checkRecogida'=>'Recogida','checkRegistro'=>'Registro','checkEstructuracion'=>'Estructuración','checkModificacion'=>'Modificación','checkConservacion'=>'Conservación','checkInterconexion'=>'Interconexión','checkConsulta'=>'Consulta','checkCotejo'=>'Cotejo','checkAnalisis'=>'Análisis de conducta','checkComunicacion'=>'Comunicación','checkSupresion'=>'Supresión','checkDestruccion'=>'Destrucción','checkComunicacion1'=>'Comunicación por transmisión al responsable del fichero','checkComunicacion2'=>'Comunicación a la Administración Pública competente','checkComunicacion3'=>'Comunicación permitida por ley');
	$tipoTratamiento='';
	foreach ($tiposArray as $key => $value) {
		if($encargado[$key]=='SI'){
			if($tipoTratamiento!=''){
				$tipoTratamiento.=', ';
			}
			$tipoTratamiento.=$value;
		}
	}

	$datosTratados=array('checkDatosProtegidos0'=>'Ideología','checkDatosProtegidos1'=>'Afiliación sindical','checkDatosProtegidos2'=>'Religión','checkDatosProtegidos3'=>'Creencias','checkDatosEspeciales0'=>'Origen racial o Étnico','checkDatosEspeciales1'=>'Salud','checkDatosEspeciales2'=>'Vida sexual','checkDatosIdentificativos0'=>'NIF / DNI','checkDatosIdentificativos1'=>'Nº SS / Mutualidad','checkDatosIdentificativos2'=>'Nombre y apellidos','checkDatosIdentificativos3'=>'Tarjeta sanitaria','checkDatosIdentificativos4'=>'Dirección','checkDatosIdentificativos5'=>'Teléfono','checkDatosIdentificativos6'=>'Firma','checkDatosIdentificativos7'=>'Huella','checkDatosIdentificativos8'=>'Imagen / voz','checkDatosIdentificativos9'=>'Marcas físicas','checkDatosIdentificativos10'=>'Firma electrónica','checkDatosIdentificativos11'=>'Correo electrónico','checkDatosIdentificativos12'=>'Otros datos biométricos','checkDatosIdentificativos13'=>$declaracion['ODCI']);
	$datosTexto='';
	foreach ($datosTratados as $key => $value) {
		if($declaracion[$key]!='NO'){
			if($datosTexto!=''){
				$datosTexto.=', ';
			}
			$datosTexto.=$value;
		}
	}
	if($declaracion['otrosTiposDatos']=='SI'){
		$datosTratados=array('checkOtrosTiposDatos0'=>'Características Personales','checkOtrosTiposDatos1'=>'Académicos y profesionales','checkOtrosTiposDatos2'=>'Detalles del empleo','checkOtrosTiposDatos3'=>'Económicos, financieros y de seguros');
		foreach ($datosTratados as $key => $value) {
			if($declaracion[$key]!='NO'){
				if($datosTexto!=''){
					$datosTexto.=', ';
				}
				$datosTexto.=$value;
			}
		}
		if($declaracion['desc_otros_tipos']!=''){
			if($datosTexto!=''){
				$datosTexto.=', ';
			}
			$datosTexto.=$declaracion['desc_otros_tipos'];
		}
	}
	$sistemas=array(1=>'Automatizado',2=>'Manual',3=>'Mixto');
	$provinciasAgencia=array(''=>'','01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');

	$campos=array('nif'=>'CIF/NIF','direccion'=>'Direccion','cp'=>'CP','localidad'=>'Localidad','provincia'=>'Provincia');
	$subcontratacion=$subencargado['empresa'].' -en lo sucesivo subencargado del tratamiento-';
	foreach ($campos as $campo => $value) {
		if($subencargado[$campo]!=''){
			if($campo=='provincia'){
				$subcontratacion.=', '.$value.' '.convertirMinuscula($provinciasAgencia[$subencargado[$campo]]);
			} else {
				$subcontratacion.=', '.$value.' '.$subencargado[$campo];
			}
		}
	}
	$responsableTexto=$responsable['razonSocial'].' -en lo sucesivo responsable del tratamiento-';
	if($responsable['cif']!=''){
		$responsableTexto.=', con NIF/CIF '.$responsable['cif'];
	}
	$cliente=sanearCaracteres($datos['razonSocial']);
	$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        #cabecera{
	        	margin-bottom:20px;
	    	}

	        #datosCabecera{
	        	width:50%;
	        	position:absolute;
	        	right:-20px;
	        	top:20px;
	    	}

	    	#datosCabecera td{
	    		padding-right:10px;
	    	}

	    	#datosCabecera .dato{
	    		border-bottom:1px solid #000;
	    		width:200px;
	    		padding:0px;
	    		text-align:right;
	    	}

	    	.titulo{
	    		font-size:17px;
	    		font-weight:bold;
	    		text-align:center;
	    		color:#15284B;
	    		background:#EDF1F9;
	    		padding:10px;
	    	}

	    	h1{
	    		color:#15284B;
	    		font-size:12px;
	    		text-decoration:underline;
	    		text-align:center;
	    		margin-bottom:10px;
	    		margin-top:15px;
	    	}
	    	
	    	p{
	    		line-height: 20px;
	    		margin-bottom:3px;
	    		margin-top:3px;
	    		text-align: justify;
	    	}

	    	#tablaFirmas{
	    		width:100%;
	    		margin-top:100px;
	    	}

	    	#tablaFirmas td{
	    		width:50%;
	    		text-align:center;
	    	}

	    	ul{
	    		margin-left:-50px;
	    	}

	    	ul li{
	    		padding-left:22px;
	    		padding-top:5px;
	    	}

	    	ul .marcado{
	    		background:url(../img/check.png) left top no-repeat;
	    	}

	    	.logo{
	    		width:200px;
	    	}

	    	.cabecera{
	    		width:100%;
	    		border:1px solid #000;
	    		padding:3px 5px;
	    	}

	    	.cabecera p{
	    		text-align:center;
	    		text-decoration:underline;
	    	}

	    	.pie{
	    		position:relative;
	    		width:100%;
	    	}

	    	.pie p{
	    		font-size:7px;
	    		position:absolute;
	    	}

	    	.alignLeft{
	    		text-align:left;
	    	}

	    	.alignRight{
	    		text-align:right;
	    	}

	    	.alignCentro{
	    		text-align:center;
	    	}
	    	
	-->
	</style>";
	
	$contenido.='<page backtop="30mm">
	<page_header>
	<div class="cabecera">
		<p>Documento del sistema de seguridad (R.G.P.D)</p>
		<br/>
		<b>Razón Social: '.$cliente.'<br/>
		Documento: CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO</b>
	</div>
	</page_header>';
	$contenido.='
	<b>1. CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO</b><br/><br/>
	De una parte:<br/><br/>
	<b>* Como encargado del tratamiento:</b> '.$encargado['nombreEncargado'].'<br/><br/>
	- Con NIF: '.$encargado['cifEncargado'].' <br/><br/>
	- Dirección: '.$encargado['direccionEncargado'].' <br/><br/>
	- Código Postal: '.$encargado['cpEncargado'].' <br/><br/>
	- Localidad: '.$encargado['localidadEncargado'].' <br/><br/>
	- Provincia: '.obtieneProvincia($encargado['provinciaEncargado']).' <br/><br/>
	- Representante legal: '.$encargado['representanteEncargado'].' <br/><br/>
	Y de otra parte: <br/><br/>
	<b>* Como subencargado del tratamiento:</b> '.$subencargado['empresa'].' <br/><br/>
	- Con NIF: '.$subencargado['nif'].' <br/><br/>
	- Dirección: '.$subencargado['direccion'].' <br/><br/>
	- Código Postal: '.$subencargado['cp'].' <br/><br/>
	- Localidad: '.$subencargado['localidad'].' <br/><br/>
	- Provincia:  '.obtieneProvincia($subencargado['provincia']).'<br/><br/>
	- Representante legal:  '.$subencargado['representante'].'<br/><br/>';
	$contenido.='
	Ambas partes contratantes se reconocen recíprocamente con capacidad legal necesaria para suscribir el presente contrato de subencargado del tratamiento, regulado por artículo 28 del RGPD – Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016 relativo a la protección de las personas físicas en lo que respecta al tratamiento de datos personales y a la libre circulación de estos datos- y <b>MANIFIESTAN</b>:<br/><br/>
	<ul style="list-style:none;">
		<li>I. Que '.$responsableTexto.' ha contratado los servicios de la empresa externa '.$encargado['nombreEncargado'].' para que le preste el servicio siguiente: '.$servicio.', desde la fecha '.$fecha.'.</li>
		<li>II.	Que  para la prestación de los servicios referenciados en el Manifiesto I, '.$encargado['nombreEncargado'].' -en lo sucesivo encargado del tratamiento- necesita contratar los servicios de una empresa externa que deberá acceder, para ello, a los datos personales tratados por el encargado del tratamiento en tal concepto, para lo cual el responsable del tratamiento autoriza al encargado del tratamiento a subcontratar los servicios siguientes: '.$servicio.',  con la empresa '.$subcontratacion.'.</li>
		<li>III. Que mediante lo establecido en el presente contrato, se habilita a '.$subencargado['empresa'].' -en lo sucesivo subencargado del tratamiento- a tratar por cuenta del responsable del tratamiento los datos de carácter personal necesarios para prestar los servicios contratados concretados en el Manifiesto II del presente documento, siguiendo  las directrices que al efecto establezca el encargado del tratamiento. </li>
	</ul>';
	$contenido.='
	<page_footer>
		<div class="pie">
				<p class="alignLeft">Documento generado por UVED Asesores Consultores</p>
				<p class="alignRight">Página [[page_cu]] de [[page_nb]]</p>
		</div>
	</page_footer>
	</page>';
	$contenido.='<page backtop="30mm">
	<page_header>
	<div class="cabecera">
		<p>Documento del sistema de seguridad (R.G.P.D)</p>
		<br/>
		<b>Razón Social: '.$cliente.'<br/>
		Documento: CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO</b>
	</div>
	</page_header>';
	$contenido.='
	<p class="alignCentro"><b>CLÁUSULAS:</b></p>
	<b>PRIMERA. Objeto del subencargo del tratamiento.</b><br/><br/>
	El subencargado del tratamiento tratará los datos de carácter personal del responsable del tratamiento que resulten necesarios para prestar al encargado del tratamiento el servicio siguiente: '.$servicio.'. <br/><br/>
	El subencargado del tratamiento tratará los datos en: '.$sitiosTexto.'. <br/><br/>
	Concreción de los tratamientos a realizar: '.$tipoTratamiento.'. <br/><br/>';
	$contenido.='
	<b>SEGUNDA. Identificación de la información afectada.</b><br/><br/>
	Para la ejecución de las prestaciones derivadas del cumplimiento del objeto de este subencargo, el encargado del tratamiento pone a disposición del subencargado del tratamiento, la siguiente información sobre los datos personales que va a tratar:  <br/><br/>
	* El fichero –Tratamiento- que contiene los datos es: '.$declaracion['nombreFichero'].'. <br/>
	* Los datos que contiene son los siguientes: '.$datosTexto.'. <br/>
	* Sistema de tratamiento: '.$sistemas[$declaracion['tratamiento']].'. <br/><br/>';
	$contenido.='
	<b>TERCERA. Duración.</b> <br/><br/>
	El presente acuerdo se inicia a partir de la fecha de firma del presente documento, finalizando el día '.$fecha2.'.<br/><br/>
	Una vez finalice el presente contrato, el subencargado del tratamiento debe devolver al encargado del tratamiento los datos personales tratados como consecuencia del presente subencargo y suprimir cualquier copia que esté en su poder.<br/><br/>
	<b>CUARTA. Obligaciones del subencargado del tratamiento.<br/><br/></b>
	El encargado del tratamiento y todo su personal se obliga a:';
	$contenido.='
	<ul style="list-style:none;">
		<li>a)	Utilizar los datos personales objeto de tratamiento, o los que recoja para su inclusión, sólo para la finalidad objeto de este subencargo. En ningún caso podrá utilizar los datos para fines propios.</li>
		<li>b)	Tratar los datos de acuerdo con las instrucciones del responsable del tratamiento y/o del encargado del tratamiento.<br/>
		Si el subencargado del tratamiento considera que alguna de las instrucciones infringe el RGPD o cualquier otra disposición en materia de protección de datos de la Unión o de los Estados miembros, el subencargado del tratamiento informará inmediatamente al encargado del tratamiento.</li>
		<li>c)	Con la información facilitada por el encargado del tratamiento en la CLÁUSULA SEGUNDA del presente documento, el subencargado del tratamiento debe proceder a cumplimentar aquellos registros que contempla el documento de seguridad que está obligado a mantener actualizado el subencargado del tratamiento.</li>
		<li>d)	No comunicar los datos a terceras personas, salvo que cuente con la autorización expresa del responsable del tratamiento y/o del encargado del tratamiento, en los supuestos legalmente admisibles.</li>
		<li>e)	No está permitida la subcontratación.</li>
		<li>f)	Mantener el deber de secreto respecto a los datos de carácter personal a los que haya tenido acceso en virtud del presente subencargo, incluso después de que finalice su objeto.</li>
	</ul>';
	$contenido.='
	<page_footer>
		<div class="pie">
				<p class="alignLeft">Documento generado por UVED Asesores Consultores</p>
				<p class="alignRight">Página [[page_cu]] de [[page_nb]]</p>
		</div>
	</page_footer>
	</page>';
	$contenido.='<page backtop="30mm">
	<page_header>
	<div class="cabecera">
		<p>Documento del sistema de seguridad (R.G.P.D)</p>
		<br/>
		<b>Razón Social: '.$cliente.'<br/>
		Documento: CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO</b>
	</div>
	</page_header>';
	$contenido.='
	<ul style="list-style:none;">
		<li>g)	Garantizar que el personal a su cargo, autorizado para tratar los datos personales objeto del presente contrato, se comprometan de forma expresa y por escrito a respetar la confidencialidad y a cumplir las medidas de seguridad correspondientes, de las que hay que informarles convenientemente.</li>
		<li>h)	Mantener a disposición del responsable del tratamiento y/o del encargado del tratamiento, la documentación acreditativa del cumplimiento de la obligación establecida en el apartado anterior.</li>
		<li>i)	Garantizar la formación necesaria en materia de protección de datos personales de las personas autorizadas para tratar datos personales.</li>
		<li>j)	Asistir al responsable del tratamiento en la respuesta al ejercicio de los derechos de:<br/><br/>
		-	Acceso, rectificación, supresión y oposición.<br/>
		-	Limitación del tratamiento.<br/>
		-	Portabilidad de datos.<br/>
		-	A no ser objeto de decisiones individualizadas automatizadas (incluida la elaboración de perfiles).<br/><br/>
		Cuando las personas afectadas ejerzan los derechos de acceso, rectificación, supresión y oposición, limitación del tratamiento, portabilidad de datos y a no ser objeto de decisiones individualizadas automatizadas, ante el subencargado del tratamiento, éste debe comunicarlo por correo electrónico a las siguientes direcciones: '.$responsable['email'].' (dirección del responsable del tratamiento), '.$encargado['emailEncargado'].' (dirección del encargado del tratamiento). La comunicación debe hacerse de forma inmediata y en ningún caso más allá del día laborable siguiente al de la recepción de la solicitud, juntamente, en su caso, con otras informaciones que puedan ser relevantes para resolver la solicitud.</li>
		<li>k)	Registrando aquellas rectificaciones o exclusiones (por cancelación u oposición) que el responsable del tratamiento o el encargado del tratamiento le comuniquen como consecuencia de peticiones de aquellos interesados cuyos datos esté tratando.</li>
		<li>l)	Derecho de información. El subencargado del tratamiento, en el momento de la recogida de los datos, debe facilitar por escrito al interesado la información relativa a los tratamientos de datos que se van a realizar.</li>
		<li>m)	El subencargado del tratamiento notificará al responsable del tratamiento y al encargado del tratamiento, a las direcciones de correo electrónico referidas en el apartado j) de la presente cláusula, sin dilación indebida y en cualquier caso antes de que transcurran 72 horas, las violaciones de la seguridad de los datos personales a su cargo de las que tenga conocimiento, juntamente  con toda la información relevante para la documentación y comunicación de la incidencia.<br/>
		Se facilitará como mínimo, la información siguiente en el supuesto que se disponga de ella:<br/>
		-	Descripción de la naturaleza de la violación de la seguridad de los datos personales, inclusive, cuando sea posible, las categorías y el número aproximado de interesados afectados, y las categorías y el número aproximado de interesados afectados, y las categorías y el número aproximado de registros de datos personales afectados.<br/>
		-	Descripción de las posibles consecuencias o propuestas para poner remedio a la violación de la seguridad de los datos personales, incluyendo, si procede, las medidas adoptadas para mitigar los posibles efectos negativos.<br/>
		-	Si no es posible facilitar la información simultáneamente, y en la medida en que no lo sea, la información se facilitará de manera gradual sin dilación indebida.<br/></li>
		<li>n)	Poner a disposición del responsable y/o del encargado del tratamiento, toda la información necesaria para demostrar el cumplimiento de sus obligaciones, incluida copia del Informe de las Auditorías correspondientes a los tratamientos objeto del presente contrato.</li>
		<li>o)	Cumplir todos los requisitos exigidos por el RGPD y, en todo caso, implantar mecanismos para:<br/>
		-	Garantizar la confidencialidad, integridad y disponibilidad de los sistemas y servicios de tratamiento.<br/>
		-	Restaurar la disponibilidad y el acceso a los datos personales de forma rápida, en caso de incidente físico o técnico.<br/>
		-	Verificar, evaluar y valorar, de forma regular, la eficacia de las medidas técnicas y organizativas implantadas para garantizar la seguridad del tratamiento.</li>
	</ul>';
	$contenido.='
	<page_footer>
		<div class="pie">
				<p class="alignLeft">Documento generado por UVED Asesores Consultores</p>
				<p class="alignRight">Página [[page_cu]] de [[page_nb]]</p>
		</div>
	</page_footer>
	</page>';
	$contenido.='<page backtop="30mm">
	<page_header>
	<div class="cabecera">
		<p>Documento del sistema de seguridad (R.G.P.D)</p>
		<br/>
		<b>Razón Social: '.$cliente.'<br/>
		Documento: CONTRATO ENCARGADO-SUBENCARGADO DEL TRATAMIENTO</b>
	</div>
	</page_header>';
	$contenido.='
	<ul style="list-style:none;">
		<li>p)	Devolver al encargado del tratamiento los datos de carácter personal y, si procede, los soportes donde consten, una vez cumplida la prestación.<br/>
		La devolución debe comportar el borrado total de los datos existentes en los equipos informáticos utilizados por el subencargado del tratamiento.<br/>
		No obstante, el subencargado del tratamiento puede conservar una copia, con los datos debidamente bloqueados, mientras puedan derivarse responsabilidades de la ejecución de la prestación.</li>
	</ul>
	<b>QUINTA. Obligaciones del encargado del tratamiento.</b><br/>
	Corresponde al encargado del tratamiento:
	<ul style="list-style:none;">
		<li>a)	Entregar al subencargado del tratamiento los datos necesarios para la prestación del servicio referenciado en la CLÁUSULA PRIMERA del presente documento.</li>
		<li>b)	Supervisar el tratamiento, pudiendo requerir copia de los Informes de Auditorías realizados por el subencargado del tratamiento.</li>
	</ul>
	<b>SEXTA. Obligaciones del responsable del tratamiento.</b><br/>
	Corresponde al responsable del tratamiento:
	<ul style="list-style:none;">
		<li>a) Entregar al encargado los datos necesarios para la prestación del servicio referenciado en el MANIFIESTO I del presente documento.</li>
		<li>b) Supervisar el tratamiento, incluida la realización de inspecciones y auditorías. </li>
	</ul>
	En prueba de conformidad con todas sus partes, firman el presente contrato el '.formateaFechaWeb(date('Y-m-d')).'.<br/><br/>
	<table style="width:100%">
		<tr>
			<td style="width:50%;"><b>Como encargado del tratamiento</b></td>
			<td style="width:50%;"><b>Como subencargado del tratamiento</b></td>
		</tr>
		<tr>
			<td style="width:50%;height:150px;"></td>
			<td style="width:50%;height:150px;">'.$firmaMdiaz.'</td>
		</tr>
		<tr>
			<td style="width:50%;">'.$encargado['nombreEncargado'].'</td>
			<td style="width:50%;">'.$subencargado['empresa'].'</td>
		</tr>
		<tr>
			<td style="width:50%;">D. '.$encargado['representanteEncargado'].'</td>
			<td style="width:50%;">D. '.$subencargado['representante'].'</td>
		</tr>
		<tr>
			<td style="width:50%;">Representante del encargado </td>
			<td style="width:50%;">Representante del subencargado</td>
		</tr>
	</table>
	';
	$contenido.='
	<page_footer>
		<div class="pie">
				<p class="alignLeft">Documento generado por UVED Asesores Consultores</p>
				<p class="alignRight">Página [[page_cu]] de [[page_nb]]</p>
		</div>
	</page_footer>
	</page>
	';
	
	require_once('../../api/html2pdf/html2pdf.class.php');
	$html2pdf=new HTML2PDF('P','A4','es','true','UTF-8',array(25,10,25,10));
	$html2pdf->pdf->SetDisplayMode('fullpage');
	$html2pdf->setDefaultFont("calibri");
	$html2pdf->WriteHTML($contenido);
	$html2pdf->Output('../documentos/consultorias/'.$fichero,'f');
	return $fichero;
}



function contratoSubencargadoLOPDWord($datos,$s,$i,$PHPWord){
	global $_CONFIG;
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_subencargados.docx');
	$fichero='CONTRATO_ENCARGADO_SUBENCARGADO_DEL_TRATAMIENTO_'.$i.'.docx';
	$subencargado=datosRegistro('declaraciones_subcontrata',$s['subencargado']);
	$responsable=datosRegistro('responsables_fichero',$s['responsable']);
	$declaracion=datosRegistro('declaraciones',$s['declaracion']);
	if($s['tipo']=='FIJO'){
		$encargado=creaEncargadoParaSubEncargado($datos,$subencargado['codigoResponsable']);
	} else if($s['tipo']=='NOFIJO'){
		$encargado=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigo='.$subencargado['codigoResponsable'],true,true);
		$item=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$encargado['codigo'].' AND codigoTrabajo='.$datos['codigoTrabajo'].' AND fijo="NO";',true,true);
		foreach ($item as $key => $value) {
			if(strpos($key,'check')!==false){
				$encargado[$key]=$value;
			}
		}
	}
	if($encargado['servicioEncargado']!=''){
		$servicio=$encargado['servicioEncargado'];
		$servicio=$subencargado['servicio'];
	} else {
		$servicio=$subencargado['servicio'];
	}
	$fecha=formateaFechaWeb($encargado['fechaAltaEncargado']);
	$fecha2='que cualquiera de la partes lo renunciare';
	if($encargado['fechaAltaEncargado']!='0000-00-00'){
		$fecha.=' hasta la fecha '.formateaFechaWeb($encargado['fechaBajaEncargado']);
		$fecha2=formateaFechaWeb($encargado['fechaBajaEncargado']);
	}
	$sitio=array('N'=>'','NULL'=>'','ENCARGADO'=>'locales del encargado','RESPONSABLE'=>'locales del responsable','REMOTO'=>'mediante acceso remoto');
	$sitios=explode('&$&', $encargado['dondeEncargado']);
	$sitiosTexto='';
	foreach ($sitios as $key => $value) {
		if($sitiosTexto!=''){
			$sitiosTexto.=', ';
		}
		if($value=='REMOTO'){
			$sitiosTexto.=$sitio[$value];
		} else {
			$sitiosTexto.='en los '.$sitio[$value];
		}
	}
	$tiposArray=array('checkRecogida'=>'Recogida','checkRegistro'=>'Registro','checkEstructuracion'=>'Estructuración','checkModificacion'=>'Modificación','checkConservacion'=>'Conservación','checkInterconexion'=>'Interconexión','checkConsulta'=>'Consulta','checkCotejo'=>'Cotejo','checkAnalisis'=>'Análisis de conducta','checkComunicacion'=>'Comunicación','checkSupresion'=>'Supresión','checkDestruccion'=>'Destrucción','checkComunicacion1'=>'Comunicación por transmisión al responsable del fichero','checkComunicacion2'=>'Comunicación a la Administración Pública competente','checkComunicacion3'=>'Comunicación permitida por ley');
	$tipoTratamiento='';
	foreach ($tiposArray as $key => $value) {
		if($encargado[$key]=='SI'){
			if($tipoTratamiento!=''){
				$tipoTratamiento.=', ';
			}
			$tipoTratamiento.=$value;
		}
	}

	$datosTratados=array('checkDatosProtegidos0'=>'Ideología','checkDatosProtegidos1'=>'Afiliación sindical','checkDatosProtegidos2'=>'Religión','checkDatosProtegidos3'=>'Creencias','checkDatosEspeciales0'=>'Origen racial o Étnico','checkDatosEspeciales1'=>'Salud','checkDatosEspeciales2'=>'Vida sexual','checkDatosIdentificativos0'=>'NIF / DNI','checkDatosIdentificativos1'=>'Nº SS / Mutualidad','checkDatosIdentificativos2'=>'Nombre y apellidos','checkDatosIdentificativos3'=>'Tarjeta sanitaria','checkDatosIdentificativos4'=>'Dirección','checkDatosIdentificativos5'=>'Teléfono','checkDatosIdentificativos6'=>'Firma','checkDatosIdentificativos7'=>'Huella','checkDatosIdentificativos8'=>'Imagen / voz','checkDatosIdentificativos9'=>'Marcas físicas','checkDatosIdentificativos10'=>'Firma electrónica','checkDatosIdentificativos11'=>'Correo electrónico','checkDatosIdentificativos12'=>'Otros datos biométricos','checkDatosIdentificativos13'=>$declaracion['ODCI']);
	$datosTexto='';
	foreach ($datosTratados as $key => $value) {
		if($declaracion[$key]!='NO'){
			if($datosTexto!=''){
				$datosTexto.=', ';
			}
			$datosTexto.=$value;
		}
	}
	if($declaracion['otrosTiposDatos']=='SI'){
		$datosTratados=array('checkOtrosTiposDatos0'=>'Características Personales','checkOtrosTiposDatos1'=>'Académicos y profesionales','checkOtrosTiposDatos2'=>'Detalles del empleo','checkOtrosTiposDatos3'=>'Económicos, financieros y de seguros');
		foreach ($datosTratados as $key => $value) {
			if($declaracion[$key]!='NO'){
				if($datosTexto!=''){
					$datosTexto.=', ';
				}
				$datosTexto.=$value;
			}
		}
		if($declaracion['desc_otros_tipos']!=''){
			if($datosTexto!=''){
				$datosTexto.=', ';
			}
			$datosTexto.=$declaracion['desc_otros_tipos'];
		}
	}
	$sistemas=array(1=>'Automatizado',2=>'Manual',3=>'Mixto');
	$provinciasAgencia=array(''=>'','01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');

	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	
	$documento->setValue("nombre1",utf8_decode($encargado['nombreEncargado']));
	$documento->setValue("cif1",utf8_decode($encargado['cifEncargado']));
	$documento->setValue("direccion1",utf8_decode($encargado['direccionEncargado']));
	$documento->setValue("cp1",utf8_decode($encargado['cpEncargado']));
	$documento->setValue("localidad1",utf8_decode($encargado['localidadEncargado']));
	$documento->setValue("provincia1",utf8_decode(obtieneProvincia($encargado['provinciaEncargado'])));
	$documento->setValue("repre1",utf8_decode($encargado['representanteEncargado']));
	$documento->setValue("email1",utf8_decode($encargado['emailEncargado']));

	$documento->setValue("nombre2",utf8_decode($subencargado['empresa']));
	$documento->setValue("cif2",utf8_decode($subencargado['nif']));
	$documento->setValue("direccion2",utf8_decode($subencargado['direccion']));
	$documento->setValue("cp2",utf8_decode($subencargado['cp']));
	$documento->setValue("localidad2",utf8_decode($subencargado['localidad']));
	$documento->setValue("provincia2",utf8_decode(obtieneProvincia($subencargado['provincia'])));
	$documento->setValue("repre2",utf8_decode($subencargado['representante']));

	$campos=array('nif'=>'CIF/NIF','direccion'=>'Direccion','cp'=>'CP','localidad'=>'Localidad','provincia'=>'Provincia');
	$subcontratacion=$subencargado['empresa'].' -en lo sucesivo subencargado del tratamiento-';
	foreach ($campos as $campo => $value) {
		if($subencargado[$campo]!=''){
			if($campo=='provincia'){
				$subcontratacion.=', '.$value.' '.convertirMinuscula($provinciasAgencia[$subencargado[$campo]]);
			} else {
				$subcontratacion.=', '.$value.' '.$subencargado[$campo];
			}
		}
	}
	$responsableTexto=$responsable['razonSocial'].' -en lo sucesivo responsable del tratamiento-';
	if($responsable['cif']!=''){
		$responsableTexto.=', con NIF/CIF '.$responsable['cif'];
	}
	$documento->setValue("responsable",utf8_decode($responsableTexto));

	$documento->setValue("emailResponsable",utf8_decode($responsable['email']));
	$documento->setValue("fichero",utf8_decode($declaracion['nombreFichero']));
	$documento->setValue("fecha",utf8_decode($fecha));
	$documento->setValue("fecha2",utf8_decode($fecha2));
	$documento->setValue("fechaHoy",formateaFechaWeb(date('Y-m-d')));
	$documento->setValue("servicio",utf8_decode($servicio));
	$documento->setValue("sitios",utf8_decode($sitiosTexto));
	$documento->setValue("tipos",utf8_decode($tipoTratamiento));
	$documento->setValue("datosTexto",utf8_decode($datosTexto));
	$documento->setValue("sistema",utf8_decode($sistemas[$declaracion['tratamiento']]));
	//echo 'Subencargado: '.$subcontratacion;
	$documento->setValue("subencargado",utf8_decode($subcontratacion));

	$documento->save('../documentos/consultorias/'.$fichero);

	return $fichero;
}

function creaEncargadoParaSubEncargado($datos,$codigo){
	$formulario=recogerFormularioServicios($datos);
	$campos=array(
		158=>array(511,159,160,161,164,163,167,543,574,544,541,162,611),
		168=>array(512,169,170,171,174,173,177,545,575,546,527,172,612),
		178=>array(513,179,180,181,184,183,187,547,576,548,542,182,613),
		189=>array(514,190,191,192,195,194,198,549,577,550,528,193,614),
		200=>array(515,201,202,203,206,205,209,551,578,552,529,204,615),
		210=>array(516,211,212,213,216,215,219,553,579,554,530,214,616),
		220=>array(517,221,222,223,226,225,229,555,609,556,531,224,617),
		230=>array(518,231,232,233,236,235,239,557,580,558,532,234,618),
		240=>array(519,241,242,243,246,245,249,559,581,560,533,244,619),
		250=>array(520,251,252,253,256,255,259,561,582,562,534,254,620),
		260=>array(521,261,262,263,266,265,269,563,583,564,535,264,621),
		270=>array(522,271,272,273,276,275,279,565,584,566,536,274,622),
		280=>array(523,281,282,283,286,285,289,567,585,568,537,284,623),
		290=>array(524,291,292,293,296,295,299,569,586,570,538,294,624),
		487=>array(525,488,489,490,493,492,496,572,587,544,571,491,625)
	);
	$servicio=array(158=>'Asesoría laboral para la realización de nóminas',168=>'Asesoría contable/fiscal',178=>'Video vigilancia',189=>'PRL',200=>'Mantenimiento de equipos informáticos',210=>'Mantenimiento de aplicaciones',220=>'Mantenimiento de la página web',230=>'Copias de seguridad "Backup"',240=>'Transporte/mensajería para envíos a clientes',250=>'Limpieza',260=>'Mantenimiento de instalaciones',270=>'Vigilancia/seguridad',280=>'Mantenimiento de la copiadora',290=>'Colaboración mercantil',487=>'Selección de personal');
	$campos=$campos[$codigo];
	$res=array('servicioEncargado'=>$servicio[$codigo],'nombreEncargado'=>$formulario['pregunta'.$campos[1]],'cifEncargado'=>$formulario['pregunta'.$campos[2]],'direccionEncargado'=>$formulario['pregunta'.$campos[3]],'cpEncargado'=>$formulario['pregunta'.$campos[4]],'localidadEncargado'=>$formulario['pregunta'.$campos[5]],'provinciaEncargado'=>$formulario['pregunta'.$campos[12]],'representanteEncargado'=>$formulario['pregunta'.$campos[6]],'emailEncargado'=>$formulario['pregunta'.$campos[11]],'fechaAltaEncargado'=>formateaFechaBD($formulario['pregunta'.$campos[7]]),'fechaBajaEncargado'=>formateaFechaBD($formulario['pregunta'.$campos[9]]),'dondeEncargado'=>$formulario['pregunta'.$campos[0]]);
	$tiposArray=array('Recogida'=>'checkTipoTratamiento1','Registro'=>'checkTipoTratamiento2','Estructuración'=>'checkTipoTratamiento3','Modificación'=>'checkTipoTratamiento4','Conservación'=>'checkTipoTratamiento5','Interconexión'=>'checkTipoTratamiento6','Consulta'=>'checkTipoTratamiento7','Cotejo'=>'checkTipoTratamiento8','Análisis de conducta'=>'checkTipoTratamiento9','Comunicación'=>'checkTipoTratamiento10','Supresión'=>'checkTipoTratamiento11','Destrucción'=>'checkTipoTratamiento12','Comunicación por transmisión al responsable del fichero'=>'checkTipoTratamiento13','Comunicación a la Administración Pública competente'=>'checkTipoTratamiento14','Comunicación permitida por ley'=>'checkTipoTratamiento15');
	$item=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$codigo.' AND codigoTrabajo='.$datos['codigoTrabajo'].' AND fijo="SI";',true,true);
	foreach ($item as $key => $value) {
		if(strpos($key,'check')!==false){
			$res[$key]=$value;
		}
	};
	return $res;
}

function consultaTratamientosRealiza($codigoEncargado,$codigoTrabajo,$fijo){
	$res='';
	$item=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$codigoEncargado.' AND codigoTrabajo='.$codigoTrabajo.' AND fijo="'.$fijo.'";',true,true);
	if($item){
		$tratamientos=array('checkRecogida'=>'Recogida','checkRegistro'=>'Registro','checkEstructuracion'=>'Estructuración','checkModificacion'=>'Modificación','checkConservacion'=>'Conservación','checkInterconexion'=>'Interconexión','checkConsulta'=>'Consulta','checkCotejo'=>'Cotejo','checkAnalisis'=>'Análisis de conducta','checkComunicacion'=>'Comunicación','checkSupresion'=>'Supresión','checkDestruccion'=>'Destrucción','checkComunicacion1'=>'Comunicación por transmisión al responsable del fichero','checkComunicacion2'=>'Comunicación a la Administración Pública competente','checkComunicacion3'=>'Comunicación permitida por ley');
		foreach ($tratamientos as $key => $value) {
			if($item[$key]=='SI'){
				if($res!=''){
					$res.=', ';
				}
				$res.=$value;
			}
		}
	}
	return $res;
}

function anexoContratoSuscribirloEncargadoTratamientoLOPDWord2($datos,$i,$encargado, $PHPWord){
	global $_CONFIG;
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_ContratoSuscribir.docx');
	$fichero='CONTRATO_EMITIDO_PARA_SUSCRIBIRLO_CON_EL_ENCARGADO_DEL_TRATAMIENTO_'.$i.'.docx';
	$sitio=array('N'=>'','NULL'=>'','ENCARGADO'=>'locales del encargado','RESPONSABLE'=>'locales del responsable','REMOTO'=>'mediante acceso remoto');
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$sitios=explode('&$&', $encargado['dondeEncargado']);
	$sitiosTexto='';
	$localesEncargado='';
	foreach ($sitios as $key => $value) {
		if($sitiosTexto!=''){
			$sitiosTexto.=', ';
		}
		if($value=='REMOTO'){
			$sitiosTexto.=$sitio[$value];
		} else {
			$sitiosTexto.='en los '.$sitio[$value];
		}
		if($value=='ENCARGADO'){
			$localesEncargado='<w:br/>Al realizarse el tratamiento de los datos personales en los locales del encargado del tratamiento, y con el fin de acreditar las medidas de seguridad implantadas y actualizadas por el encargado del tratamiento, este deberá remitir al responsable del fichero el documento de seguridad donde figuren dichas medidas, así como la identificación del fichero.';
		}
	}
	$servicio=$encargado['servicioEncargado'];

		$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
		$documento->setValue("cif",utf8_decode($datos['cif']));
		$documento->setValue("direccion",utf8_decode($datos['domicilio']));
		$documento->setValue("cp",utf8_decode($datos['cp']));
		$documento->setValue("localidad",utf8_decode($datos['localidad']));
		$documento->setValue("provincia",utf8_decode($datos['provincia']));
		$nombreRL=$formulario['pregunta8'];
		if(isset($formulario['pregunta627'])){
			$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
		}
		$documento->setValue("representante",utf8_decode($nombreRL));

		$documento->setValue("encargado",utf8_decode(sanearCaracteres($encargado['nombreEncargado'])));
		$documento->setValue("cif_e",utf8_decode($encargado['cifEncargado']));
		$documento->setValue("direccion_e",utf8_decode($encargado['direccionEncargado']));
		$documento->setValue("cp_e",utf8_decode($encargado['cpEncargado']));
		$documento->setValue("localidad_e",utf8_decode($encargado['localidadEncargado']));
		$documento->setValue("provincia_e",utf8_decode(obtieneProvincia($encargado['provinciaEncargado'])));
		$documento->setValue("representante_e",utf8_decode($encargado['representanteEncargado']));

		$documento->setValue("fechaInicio",utf8_decode(formateaFechaWeb($encargado['fechaAltaEncargado'])));
		$documento->setValue("servicio",utf8_decode($servicio));
		$documento->setValue("lugarTrabajo",utf8_decode($sitiosTexto));

		recogerFicheroContratoWord($encargado['ficheroEncargado'],$documento);
		$fechaBaja=$encargado['fechaBajaEncargado']=='0000-00-00'?'':formateaFechaWeb($encargado['fechaBajaEncargado']);
		$documento->setValue("fechaFin",utf8_decode($fechaBaja));

		if($encargado['subcontratacionEncargado']=='SI'){
			$subcontratacion='';
			$listado=consultaBD('SELECT * FROM declaraciones_subcontrata WHERE tipo="ENCARGADO" AND codigoResponsable='.$encargado['codigo'].' AND codigoTrabajo='.$encargado['codigoTrabajo'],true);
			while($item=mysql_fetch_assoc($listado)){
				if($subcontratacion!=''){
					$subcontratacion.=', ';
				}
				$subcontratacion.=$item['empresa'];
			}
			$subcontratacion="Sí está permitida la subcontratación. Con las siguientes empresas: ".$subcontratacion;
		} else {
			$subcontratacion="No está permitida la subcontratación";
		}

		$documento->setValue("subcontratacion",utf8_decode($subcontratacion));
		$documento->setValue("localesEncargado",utf8_decode($localesEncargado));
		$documento->setValue("email",utf8_decode($datos['email']));

		$tratamientos=consultaTratamientosRealiza($encargado['codigo'],$datos['codigoTrabajo'],'NO');
		if($tratamientos!=''){
			$tratamientos='Concreción de los tratamientos a realizar: '.$tratamientos;
		}

		if($encargado['exclusivoEncargado']=='SI'){
			$tratamientos='Los datos personales tratados por el encargado del tratamiento serán incorporados y tratados exclusivamente en sus sistemas<w:br/><w:br/>'.$tratamientos;
		}
		$documento->setValue("tratamientosRealiza",utf8_decode($tratamientos));
			
		$imagen='image2.png';	
    	if($_CONFIG['usuarioBD']=='root'){
    		$logo = '../documentos/logos-clientes/imagenDefecto.png';
    	} else {
    		$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos['ficheroLogo']);
    		if($hayLogo!='NO' && $hayLogo!=''){
    			$logo = '../documentos/logos-clientes/'.$datos['ficheroLogo'];
			} else {
				$logo = '../documentos/logos-clientes/imagenDefecto.png';
			}
		}
		$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
		if (!copy($logo, $nuevo_logo)) {
    		echo "Error al copiar $fichero...\n";
		}	
		$documento->replaceImage('../documentos/logos-clientes/',$imagen);

		$documento->save('../documentos/consultorias/'.$fichero);

	return $fichero;
}

function anexoContratoSuscribirloEncargadoTratamientoLOPDWord3($datos,$i,$encargado, $PHPWord){
	global $_CONFIG;
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_ContratoSuscribir.docx');
	$fichero='CONTRATO_EMITIDO_PARA_SUSCRIBIRLO_CON_EL_ENCARGADO_DEL_TRATAMIENTO_'.$i.'.docx';
	$sitio=array('N'=>'','NULL'=>'','ENCARGADO'=>'locales del encargado','RESPONSABLE'=>'locales del responsable','REMOTO'=>'mediante acceso remoto');
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$sitiosTexto='';
	$encargado=datosRegistro('declaraciones',$encargado);
	$sitios=explode('&$&', $encargado['donde_encargado']);
	$localesEncargado='';
	foreach ($sitios as $key => $value) {
		if($sitiosTexto!=''){
			$sitiosTexto.=', ';
		}
		if($value=='REMOTO'){
			$sitiosTexto.=$sitio[$value];
		} else {
			$sitiosTexto.='en los '.$sitio[$value];
		}
		if($value=='ENCARGADO'){
			$localesEncargado='<w:br/>Al realizarse el tratamiento de los datos personales en los locales del encargado del tratamiento, y con el fin de acreditar las medidas de seguridad implantadas y actualizadas por el encargado del tratamiento, este deberá remitir al responsable del fichero el documento de seguridad donde figuren dichas medidas, así como la identificación del fichero.';
		}
	}
	$servicio='';

		$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
		$documento->setValue("cif",utf8_decode($datos['cif']));
		$documento->setValue("direccion",utf8_decode($datos['domicilio']));
		$documento->setValue("cp",utf8_decode($datos['cp']));
		$documento->setValue("localidad",utf8_decode($datos['localidad']));
		$documento->setValue("provincia",utf8_decode($datos['provincia']));
		$nombreRL=$formulario['pregunta8'];
		if(isset($formulario['pregunta627'])){
			$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
		}
		$documento->setValue("representante",utf8_decode($nombreRL));

		$documento->setValue("encargado",utf8_decode(sanearCaracteres($encargado['n_razon_encargado'])));
		$documento->setValue("cif_e",utf8_decode($encargado['cif_nif_encargado']));
		$documento->setValue("direccion_e",utf8_decode($encargado['dir_postal_encargado']));
		$documento->setValue("cp_e",utf8_decode($encargado['postal_encargado']));
		$documento->setValue("localidad_e",utf8_decode($encargado['localidad_encargado']));
		$documento->setValue("provincia_e",utf8_decode(obtieneProvincia($encargado['provincia_encargado'])));
		$documento->setValue("representante_e",utf8_decode(''));

		$documento->setValue("fechaInicio",utf8_decode(''));
		$documento->setValue("servicio",utf8_decode($servicio));
		$documento->setValue("lugarTrabajo",utf8_decode($sitiosTexto));

		recogerFicheroContratoWord($encargado['codigo'],$documento);
		$fechaBaja='';
		$documento->setValue("fechaFin",utf8_decode($fechaBaja));

		if($encargado['subcontratacion_encargado']=='SI'){
			$subcontratacion='';
			$listado=consultaBD('SELECT * FROM declaraciones_subcontrata WHERE tipo="TRATAMIENTO" AND codigoResponsable='.$encargado['codigo'],true);
			while($item=mysql_fetch_assoc($listado)){
				if($subcontratacion!=''){
					$subcontratacion.=', ';
				}
				$subcontratacion.=$item['empresa'];
			}
			$subcontratacion="Sí está permitida la subcontratación. Con las siguientes empresas: ".$subcontratacion;
		} else {
			$subcontratacion="No está permitida la subcontratación";
		}
		$documento->setValue("subcontratacion",utf8_decode($subcontratacion));
		$documento->setValue("localesEncargado",utf8_decode($localesEncargado));
		$documento->setValue("email",utf8_decode($datos['email']));

		$tratamientos=consultaTratamientosRealiza($encargado['codigo'],$datos['codigoTrabajo'],'NO');
		$documento->setValue("tratamientosRealiza",utf8_decode($tratamientos));
			
		$imagen='image2.png';	
    	if($_CONFIG['usuarioBD']=='root'){
    		$logo = '../documentos/logos-clientes/imagenDefecto.png';
    	} else {
    		$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos['ficheroLogo']);
    		if($hayLogo!='NO' && $hayLogo!=''){
    			$logo = '../documentos/logos-clientes/'.$datos['ficheroLogo'];
			} else {
				$logo = '../documentos/logos-clientes/imagenDefecto.png';
			}
		}
		$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
		if (!copy($logo, $nuevo_logo)) {
    		echo "Error al copiar $fichero...\n";
		}	
		$documento->replaceImage('../documentos/logos-clientes/',$imagen);

		$documento->save('../documentos/consultorias/'.$fichero);

	return $fichero;
}

function recogerFicheroContratoWord($ficheros, $documento){
	$ficheros=explode('&$&', $ficheros);
	$res='';
	$i=0;
	$tipos=array(''=>'',1=>'Automatizado',2=>'Manual',3=>'Mixto');
	$res='';
	foreach ($ficheros as $key => $value) {
		$fichero=datosRegistro('declaraciones',$value);
		if($fichero){
			$res.='* El fichero que contiene los datos es: '.$fichero['nombreFichero'].'<w:br/>';
			$res.='* Los datos que contiene son los siguientes: '.definirDatosFicheros($fichero).'<w:br/>';
			$res.='* Sistema de tratamiento: '.$tipos[$fichero['tratamiento']].'<w:br/>';
			$i++;
		}
	}
	if($res==''){
		$res='* Ningún fichero contiene los datos.';
	}
	$documento->setValue("ficheros",utf8_decode($res));
}

function obtieneProvincia($codigo){
	$provinciasAgencia=array(''=>'','01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	return convertirMinuscula($provinciasAgencia[$codigo]);
}

function sanearCaracteres($texto){
	$texto = str_replace( '&', '&#38;', $texto);
	$texto = str_replace( '–', '&#45;', $texto);
	$texto = str_replace( '<br />', '<w:br/>', $texto);
	$texto = str_replace( '"', '', $texto);
	return $texto;
}

function operacionesDocumentosFirmados(){
	$res=true;
	global $_CONFIG;
	if(isset($_POST['codigo'])){
		$res=actualizaDatos('documentos_firmados',time(),'../documentos/firmados/');	
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('documentos_firmados',time(),'../documentos/firmados/');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatosConFicheroAPI('documentos_firmados','ficheroFirmado','../documentos/firmados');
	}

	mensajeResultado('nombre',$res,'Documento');
    mensajeResultado('elimina',$res,'Documento', true);
}

function gestionDocumentosFirmados(){
	global $_CONFIG;
	operacionesDocumentosFirmados();
	
	abreVentanaGestion('Gestión de Documentos firmados','?','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('documentos_firmados');

	abreColumnaCampos();
		campoOculto($datos,'codigoTrabajo',$_SESSION['codigoTrabajo']);
		campoOculto($datos,'documento',$_SESSION['documentoFirmado']);
		campoOculto(formateaFechaWeb($datos['fechaSubida']),'fechaSubida',fecha());
		campoOculto($datos,'visto','NO');
		campoTexto('nombre','Nombre',$datos,'span9');
		campoFicheroInf('ficheroFirmado','Documento firmado',0,$datos,$_CONFIG['raiz'].'documentos/firmados/','Descargar');
	cierraColumnaCampos();

	cierraVentanaGestion('documentosFirmados.php',true);
}

function eliminaFichero(){
	global $_CONFIG;
	$fichero=$_POST['fichero'];
	$ruta=$_CONFIG['raiz'].'documentos/firmados/'.$fichero;
	unlink($ruta);
}

function cambiaVisto(){
	$res=cambiaValorCampo('visto','SI',$_POST['codigo'],'documentos_firmados',$conexion=true);
}

function fechaTabla($fecha,$texto){
	return '<w:p w14:paraId="1060A2A2" w14:textId="7A44C6D1" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="00397BF1" w:rsidP="0016753F"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">REGISTRO DE '.$texto.' A </w:t></w:r><w:r w:rsidR="00D303CF"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$fecha.'</w:t></w:r></w:p>';
}

function pieTabla($total){
	return '<w:p w14:paraId="2CF98860" w14:textId="77777777" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="001F0F57" w:rsidP="001F0F57"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="right"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>Nº total de re</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">gistros </w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$total.'</w:t></w:r></w:p>';
}

function anexoFinal1LOPD($codigo, $PHPWord, $nombreFichero=''){
	$fichero='ANEXO_FINAL_DESIGNACIÓN_DE_RESPONSABLE_DE_SEGURIDAD.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, clientes.administrador, clientes.nifAdministrador, clientes.telefono, clientes.email, clientes.actividad, servicios_familias.referencia AS familia, formulario, clientes.servicios, ficheroLogo, cif FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo=".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	reemplazarLogo($documento,$datos);	
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("cif",utf8_decode(sanearCaracteres($datos['cif'])));
	$documento->setValue("domicilio",utf8_decode($datos['domicilio']));
	$documento->setValue("localidad",utf8_decode($datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')'));
	$nombreRL=$formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}
	$documento->setValue("representante",utf8_decode($nombreRL));
	$documento->setValue("representanteNif",utf8_decode($formulario['pregunta14']));
	$documento->setValue("responsable",utf8_decode($formulario['pregunta15']));
	$documento->setValue("fecha",utf8_decode($formulario['pregunta2']));
	
	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function anexoFinal2LOPD($codigo, $PHPWord, $nombreFichero=''){
	$fichero='ANEXO_FINAL_DESIGNACIÓN_DE_DELEGADO_DE_PROTECCION_DE_DATOS.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, clientes.administrador, clientes.nifAdministrador, clientes.telefono, clientes.email, clientes.actividad, servicios_familias.referencia AS familia, formulario, clientes.servicios, ficheroLogo, cif FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo=".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	reemplazarLogo($documento,$datos);	
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("cif",utf8_decode(sanearCaracteres($datos['cif'])));
	$documento->setValue("domicilio",utf8_decode($datos['domicilio'].' C.P. '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')'));
	$nombreRL=$formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}
	$documento->setValue("representante",utf8_decode($nombreRL));
	$documento->setValue("representanteNif",utf8_decode($formulario['pregunta14']));
	$documento->setValue("responsable",utf8_decode($formulario['pregunta15']));
	if($formulario['pregunta2']!=''){
		$fecha=explode('/', $formulario['pregunta2']);
		$meses=array(''=>'','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
		$fecha=$fecha[0].' de '.$meses[$fecha[1]].' de '.$fecha[2];
	} else {
		$fecha='';
	}
	$documento->setValue("fecha",utf8_decode($fecha));
	$documento->setValue("dpo",utf8_decode($formulario['pregunta604']));
	$documento->setValue("dpoDomicilio",utf8_decode($formulario['pregunta626']));
	$documento->setValue("dpoEmail",utf8_decode($formulario['pregunta607']));
	$documento->setValue("dpoNif",utf8_decode($formulario['pregunta606']));
	
	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function cartelVideovigilancia($codigo) {

	ob_start();

	$fichero = 'CARTEL_VIDEOVIGILANCIA.pdf';

	$sql = "SELECT 
				c.*,
				t.formulario
			FROM 
				trabajos t
			INNER JOIN clientes c ON
				c.codigo = t.codigoCliente
			WHERE 
				t.codigo = ".$codigo.";";

	$datos      = consultaBD($sql, true, true);		
	$formulario = recogerFormularioServicios($datos);
	$datos      = datosPersonales($datos,$formulario);	
	
	$actualizcion = consultaBD('UPDATE clientes SET videovigilancia = "'.$_POST['registros'].'" WHERE codigo='.$datos['codigo'], true);
	
	if (isset($formulario['pregunta626']) && $formulario['pregunta626'] != '') {
		$direccion = $formulario['pregunta626'];
	} 
	else {
		$direccion = $datos['domicilio'].' C.P. '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	}

	$i = 1;

	$registros = explode('<br />', nl2br($_POST['registros']));

	// ESTILOS
	$contenido = "
		<style type='text/css'>
			<!--
				body{
					font-size:13px;
					font-family: helvetica;
					font-weight: lighter;
					line-height: 24px;
				}

				img{
					position:absolute;
					width:98%;
				}

				.responsable{
					position:absolute;
					width:99%;
					top:605px;
					left:30px;
					font-weight:bold;
					font-size:20px;
				}

				.direccion{
					position:absolute;
					width:99%;
					top:775px;
					left:30px;
					font-weight:bold;
					font-size:20px;
					width:700px;
				}

				.info{
					position:absolute;
					width:99%;
					top:965px;
					left:30px;
					font-weight:bold;
					font-size:20px;
					width:700px;
				}

				.info2{
					position:absolute;
					width:99%;
					top:100px;
					left:30px;
					font-weight:bold;
					font-size:20px;
					width:570px;
				}

				.pie{
					position:absolute;
					width:99%;
					bottom:0px;
					left:30px;
					font-weight:bold;
					font-size:7px;
					width:570px;
				}

				.cabecera{
					position:absolute;
					width:99%;
					top:10px;
					left:30px;
					font-weight:bold;
					font-size:12px;
					width:570px;
					border:1px solid #000;
					padding:5px;
					text-align:center;
					text-decoration:underline;
				}

				.cabecera p{
					text-align:left;
					text-decoration:none;
				}

				.logo{
					width:70px;
					position:absolute;
					top:15px;
					right:80px;
				}	    	
			-->
		</style>
	";

	// Contenido

	$contenido .= "
		<page backimg='../img/cartelVigilancia.png'>
			<div class='responsable'>".$datos['razonSocial']."</div>
			<div class='direccion'>".$direccion."</div>
			<div class='info'>".$registros[0]."</div>
		</page>
	";

	if (count($registros) > 1) {
		
		unset($registros[0]);
		
		$info = '';
		
		foreach ($registros as $r) {
			
			if($info != '') {
				$info.='<br />';
			}
			
			$info .= $r;
		}
		
		$hayLogo = str_replace('../documentos/logos-clientes/', '', $datos['ficheroLogo']);
		
		$logo = '';
    	
		if ($hayLogo != 'NO' && $hayLogo != '') {
    		$logo = '<img class="logo" src="../documentos/logos-clientes/'.$datos['ficheroLogo'].'" />';
		}
		
		$contenido .= "
			<page backtop='5mm' backbottom='1mm' backright='10mm' backleft='10mm'>
				".$logo."
				<div class='cabecera'>
					Documento del sistema de seguridad (R.G.P.D.)
					<p>Razón Social: ".$datos['razonSocial']."<br/>
					Documento: CARTEL VIDEOVIGILANCIA</p>
				</div>
			
				<div class='info2'>".$info."</div>
			
				<div class='pie'>Documento generado por UVED Asesores Consultores</div>
		
			</page>
		";
	}
	
	require_once('../../api/html2pdf/html2pdf.class.php');
	$html2pdf=new HTML2PDF('P','A4','es');
	$html2pdf->pdf->SetDisplayMode('fullpage');
	$html2pdf->setDefaultFont("calibri");
	$html2pdf->WriteHTML($contenido);
	ob_clean();
	$html2pdf->Output($fichero, 'D');
	return $fichero;
}

function derechosAcceso($codigo, $PHPWord, $nombreFichero='', $tipoDerecho=1){
	$valores=array(''=>'',''=>'',''=>'',''=>'',''=>'');
	switch ($tipoDerecho) {
		case 1:
			$tipoDerecho='Acceso';
			$nombreCabecera='DERECHOS DE ACCESO';
			$fichero='DERECHOS_DE_ACCESO.docx';
			$tipoRespuesta='ACCESO';
			break;
		case 2:
			$tipoDerecho='Rectificación';
			$nombreCabecera='DERECHOS DE RECTIFICACIÓN';
			$fichero='DERECHOS_DE_RECTIFICACIÓN.docx';
			$tipoRespuesta='RECTIFICACIÓN';
			break;
		case 3:
			$tipoDerecho='Supresión o cancelación';
			$nombreCabecera='DERECHOS DE SUPRESIÓN O CANCELACIÓN';
			$fichero='DERECHOS_DE_SUPRESIÓN_O_CANCELACIÓN.docx';
			$tipoRespuesta='CANCELACIÓN';
			break;
		case 4:
			$tipoDerecho='Oposición y decisiones individuales automatizadas';
			$nombreCabecera='DERECHOS DE OPOSICIÓN Y DECISIONES INDIVIDUALES AUTOMATIZADAS';
			$fichero='DERECHOS_DE_OPOSICIÓN_Y_DECISIONES_INDIVIDUALES_AUTOMATIZADAS.docx';
			$tipoRespuesta='OPOSICIÓN';
			break;
		case 5:
			$tipoDerecho='Limitación del tratamiento';
			$nombreCabecera='DERECHOS DE LIMITACIÓN DEL TRATAMIENTO';
			$fichero='DERECHOS_DE_LIMITACIÓN_DEL_TRATAMIENTO.docx';
			$tipoRespuesta='LIMITACIÓN';
			break;
		case 6:
			$tipoDerecho='Portabilidad de los datos';
			$nombreCabecera='DERECHOS DE PORTABILIDAD DE LOS DATOS';
			$fichero='DERECHOS_DE_PORTABILIDAD_DE_LOS_DATOS.docx';
			$tipoRespuesta='PORTABILIDAD DE LOS DATOS';
			break;
		
	}
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.provincia, clientes.administrador, clientes.nifAdministrador, clientes.telefono, clientes.email, clientes.actividad, servicios_familias.referencia AS familia, formulario, clientes.servicios, ficheroLogo, cif, clientes.codigo AS codigoCliente FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo=".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$nombreRL=$formulario['pregunta8'];
	if(isset($formulario['pregunta627'])){
		$nombreRL.=' '.$formulario['pregunta627'].' '.$formulario['pregunta628'];
	}
	$datos['representante']=$nombreRL;
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	reemplazarLogo($documento,$datos);
	$documento->setValue("nombreCabecera",utf8_decode(sanearCaracteres($nombreCabecera)));	
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("cif",utf8_decode(sanearCaracteres($datos['cif'])));

	$tabla='</w:t></w:r></w:p>'.fechaTabla(date('d/m/Y'),$nombreCabecera.' RECIBIDOS');
	$tabla.='<w:tbl><w:tblPr><w:tblW w:w="8956" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="675"/><w:gridCol w:w="2530"/><w:gridCol w:w="1439"/><w:gridCol w:w="1298"/><w:gridCol w:w="3014"/></w:tblGrid><w:tr w:rsidR="00A9190E" w:rsidRPr="008F5EA5" w14:paraId="4DB05B03" w14:textId="77777777" w:rsidTr="00A9190E"><w:tc><w:tcPr><w:tcW w:w="675" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="00CD208E" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRPr="008F5EA5" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2530" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="586694FE" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRPr="008F5EA5" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>INTERESADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1439" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="726EB0F5" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRPr="008F5EA5" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA DE RECEPCIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1298" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="1B87A6C2" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRPr="008F5EA5" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA DE RESPUESTA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3014" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3E152EC7" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRPr="008F5EA5" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t xml:space="preserve">RESPUESTA </w:t></w:r></w:p></w:tc></w:tr>';

	$derechos=consultaBD('SELECT * FROM derechos WHERE tipo="'.$tipoDerecho.'" AND codigoCliente='.$datos['codigoCliente'],true);
	$i=1;
	while($item=mysql_fetch_assoc($derechos)){
		$ref=str_pad($i, 4,'0',STR_PAD_LEFT);
		$tabla.='<w:tr w:rsidR="00A9190E" w14:paraId="02D3DC24" w14:textId="77777777" w:rsidTr="00A9190E"><w:tc><w:tcPr><w:tcW w:w="675" w:type="dxa"/></w:tcPr><w:p w14:paraId="01A07050" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2530" w:type="dxa"/></w:tcPr><w:p w14:paraId="030167F6" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$item['interesado'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1439" w:type="dxa"/></w:tcPr><w:p w14:paraId="67FDF300" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1298" w:type="dxa"/></w:tcPr><w:p w14:paraId="146039E7" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRPr="002B0C55" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRespuesta']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3014" w:type="dxa"/></w:tcPr><w:p w14:paraId="4A9A178D" w14:textId="77777777" w:rsidR="00A9190E" w:rsidRDefault="00A9190E" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$item['respuesta'].'</w:t></w:r></w:p></w:tc></w:tr>';
		$i++;
	}
	$total=$i-1;
	$tabla.='</w:tbl>'.pieTabla($total).'<w:p w14:paraId="21F9F32C" w14:textId="486BC435" w:rsidR="00605031" w:rsidRPr="00CC0769" w:rsidRDefault="0033519A" w:rsidP="00CC0769"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>';

	$derechos=consultaBD('SELECT * FROM derechos WHERE tipo="'.$tipoDerecho.'" AND codigoCliente='.$datos['codigoCliente'],true);
	$respuestas='';
	$valores=array('No figuran datos personales del interesado (2)','Comunicación al Responsable de Ficheros (3)','Datos personales bloqueados (4)','Otorgamiento de rectificación (5)','Comunicación a cesionario de dcho rectificación ejercitado y otorgado (6.1)');
	$i=1;
	while($item=mysql_fetch_assoc($derechos)){
		$ref=str_pad($i, 4,'0',STR_PAD_LEFT);
		$respuestas.='<w:p w14:paraId="57D22798" w14:textId="77777777" w:rsidR="00A841A9" w:rsidRDefault="00A841A9"><w:pPr><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:br w:type="page"/></w:r></w:p>';
		$respuestas.=respuestasDerechos($item['respuesta'],$ref,$item,$datos,$tipoRespuesta);
		$i++;
	}
	$documento->setValue("tabla",utf8_decode($tabla));
	$documento->setValue("respuestas",utf8_decode($respuestas));

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function respuestasDerechos($respuesta,$ref,$item,$datos,$tipoRespuesta){
	$res='';
	if($respuesta=='Subsanar defecto falta de identificación - art. 12.6 RGPD (1)'){
		$res.=respuesta1($ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='No figuran datos personales del interesado (2)'){
		$res.=respuesta2($ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Procede la visualización interesada (3)'){
		$res.=respuesta3($ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Comunicación al Responsable de Ficheros (3)'){
		$res.=respuesta4(3,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Comunicación al Responsable de Ficheros (4)'){
		$res.=respuesta4(4,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Datos personales bloqueados (4)'){
		$res.=respuesta5(4,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Datos personales bloqueados (5)'){
		$res.=respuesta5(5,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Otorgamiento de limitación del tratamiento (4)'){
		$res.=respuesta6(4,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Otorgamiento de rectificación (5)' || $respuesta=='Otorgamiento de cancelación (5)'){
		$res.=respuesta6(5,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Otorgamiento de acceso (6)' || $respuesta=='Otorgamiento de portabilidad (6)' || $respuesta=='Otorgamiento de oposición (6)'){
		$res.=respuesta6(6,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Comunicación a cesionario de dcho. de limitación ejercitado y otorgado (4.1)'){
		$res.=respuesta7(4,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Comunicación a cesionario de dcho. de rectificación ejercitado y otorgado (6.1)' || $respuesta=='Comunicación a cesionario de dcho. de cancelación ejercitado y otorgado (6.1)'){
		$res.=respuesta7(6,$ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Denegación de supresión inmediata por art. 17.3 RGPD (4)'){
		$res.=respuesta8($ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Denegación de portabilidad (5)'){
		$res.=respuesta9($ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Denegación de oposición (5)'){
		$res.=respuesta10($ref,$item,$datos,$tipoRespuesta);
	} else if($respuesta=='Denegación petición a no ser objeto de decisión individual automatizada (5.2)'){
		$res.=respuesta11($ref,$item,$datos,$tipoRespuesta);
	}
	return $res;
}

function respuesta1($ref,$item,$datos,$peticion){
	if($peticion=='ACCESO'){
		$tipo='DERECHO DE ACCESO EJERCITADO';
		$peticion2='acceso';
	} else if($peticion=='RECTIFICACIÓN'){
		$tipo='DERECHO DE RECTIFICACIÓN EJERCITADO';
		$peticion2='rectificación';
	} else if($peticion=='CANCELACIÓN'){
		$tipo='DERECHO DE CANCELACIÓN EJERCITADO';
		$peticion2='cancelación';
	} else if($peticion=='OPOSICIÓN'){
		$tipo='DERECHO DE OPOSICIÓN EJERCITADO';
		$peticion2='oposición';
	} else if($peticion=='LIMITACIÓN'){
		$tipo='DERECHO DE LIMITACIÓN DE TRATAMIENTO EJERCITADO';
		$peticion2='limitación del tratamiento';
	} else if($peticion=='PORTABILIDAD DE LOS DATOS'){
		$tipo='DERECHO A LA PORTABILIDAD DE LOS DATOS EJERCITADO';
		$peticion2='portabilidad de los datos';
	} 
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="455AC154" w14:textId="77777777" w:rsidR="007B587E" w:rsidRPr="00EE71C8" w:rsidRDefault="007B587E" w:rsidP="007B587E"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00EE71C8"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>RESPUESTA A '.$tipo.'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="45B78257" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00C719D6" w:rsidP="00572A5F"><w:pPr><w:pStyle w:val="ListParagraph"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº 1: SUBSANAR DEFECTO FALTA DE IDENTIFICACIÓN –ART. 12.6 RGPD-</w:t></w:r><w:r w:rsidR="00DB32AE" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="2C102C35" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESPUE</w:t></w:r><w:r w:rsidR="00BB4121" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>STA A LA PETICIÓN DE '.$peticion.' REF.</w:t></w:r><w:r w:rsidR="009A18B1" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> '.$ref.'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p><w:p w14:paraId="037CB69D" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p>';
	$res.='<w:p w14:paraId="627651D7" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la petición de '.$peticion2.' de fecha </w:t></w:r><w:r w:rsidR="006E0BD0" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> realizada por </w:t></w:r><w:r w:rsidR="0086652B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="006E0BD0" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="0086652B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>D./ª '.$item['interesado'].'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidR="00B10AD5" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="0086652B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="702E96C5" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="465AF276" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00740FFC" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> en calidad de responsable del fichero, le informa que:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="2A146C1D" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="6C8466CE" w14:textId="77777777" w:rsidR="00141896" w:rsidRPr="008E67D7" w:rsidRDefault="00141896" w:rsidP="001A3C87"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>El artículo 12.6 RGPD establece que “Sin perjuicio de lo dispuesto en el artículo 11, cuando el responsable del tratamiento tenga dudas razonables en relación con la identidad de la persona física que cursa la solicitud a que se refieren los artículos 15 a 21</w:t></w:r><w:r w:rsidR="004B0ED2" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>, podrá solicitar que se facilite la información adicional necesaria para confirmar la identidad del interesado”.</w:t></w:r></w:p><w:p w14:paraId="2E68D79C" w14:textId="77777777" w:rsidR="002421F4" w:rsidRPr="008E67D7" w:rsidRDefault="002421F4" w:rsidP="002421F4"><w:pPr><w:pStyle w:val="ListParagraph"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="07244F5F" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00A650A3" w:rsidP="001A3C87"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Por lo establecido en el artículo referenciado en el párrafo anterior</w:t></w:r><w:r w:rsidR="00C9765C" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, al no quedarnos </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">acreditada </w:t></w:r><w:r w:rsidR="00A87B48" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">la identidad del interesado y/o del solicitante, le requerimos nos facilite la información </w:t></w:r><w:r w:rsidR="00C9765C" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">adicional necesaria para ello, </w:t></w:r><w:r w:rsidR="00A87B48" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">que consistirá en: </w:t></w:r></w:p><w:p w14:paraId="17059EDF" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="00945E35"><w:pPr><w:pStyle w:val="ListParagraph"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:ind w:left="1080"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>Nombre y apellidos del interesado; fotocopia de su documento nacional de identidad, o de su pasaporte u otro documento válido que lo identifique y, en su caso, de la persona que lo represente, o instrumentos electrónicos equivalentes; así como el documento o instrumento electrónico acreditativo de tal representación. La utilización de firma electrónica identificativa del afectado eximirá de la presentación de las fotocopias del DNI o documento equivalente.</w:t></w:r></w:p><w:p w14:paraId="4DAC7CB2" w14:textId="77777777" w:rsidR="002421F4" w:rsidRPr="008E67D7" w:rsidRDefault="002421F4" w:rsidP="00945E35"><w:pPr><w:pStyle w:val="ListParagraph"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:ind w:left="1080"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="6D388E88" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="1"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>Deberá subsanar este defecto ante</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="004711E0" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00D61E0F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>, para que podamos atender su petición.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="2DC0D764" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="5A13E507" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a </w:t></w:r><w:r w:rsidR="00AB05AA" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="002C5335" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.',</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="4846253E" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="460FA459" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="00774826"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">A </w:t></w:r><w:r w:rsidR="00515353" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRespuesta']).'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="6120C897" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p><w:p w14:paraId="41BC3607" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="6C7176F8" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="57EDEC6C" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="5D93CD77" w14:textId="77777777" w:rsidR="00F6061A" w:rsidRPr="008E67D7" w:rsidRDefault="00F6061A" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="0A3E2D18" w14:textId="77777777" w:rsidR="00F6061A" w:rsidRPr="008E67D7" w:rsidRDefault="00F6061A" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="0E54A38D" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Fdo. </w:t></w:r><w:r w:rsidR="00670F94" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="601303AE" w14:textId="77777777" w:rsidR="00223413" w:rsidRPr="008E67D7" w:rsidRDefault="00670F94" w:rsidP="00843FCD"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Representante legal de '.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta2($ref,$item,$datos,$peticion){
	if($peticion=='ACCESO'){
		$tipo='DERECHO DE ACCESO EJERCITADO';
		$peticion2='acceso';
	} else if($peticion=='RECTIFICACIÓN'){
		$tipo='DERECHO DE RECTIFICACIÓN EJERCITADO';
		$peticion2='rectificación';
	} else if($peticion=='CANCELACIÓN'){
		$tipo='DERECHO DE CANCELACIÓN EJERCITADO';
		$peticion2='cancelación';
	} else if($peticion=='OPOSICIÓN'){
		$tipo='DERECHO DE OPOSICIÓN EJERCITADO';
		$peticion2='oposición';
	} else if($peticion=='LIMITACIÓN'){
		$tipo='DERECHO DE LIMITACIÓN DE TRATAMIENTO EJERCITADO';
		$peticion2='limitación del tratamiento';
	} else if($peticion=='PORTABILIDAD DE LOS DATOS'){
		$tipo='DERECHO A LA PORTABILIDAD DE LOS DATOS EJERCITADO';
		$peticion2='portabilidad de los datos';
	} 
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="54811189" w14:textId="77777777" w:rsidR="00667C2F" w:rsidRPr="008E67D7" w:rsidRDefault="00667C2F" w:rsidP="00667C2F"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>RESPUESTA A '.$tipo.'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="768AA11D" w14:textId="77777777" w:rsidR="00667C2F" w:rsidRPr="008E67D7" w:rsidRDefault="00F27B65" w:rsidP="005E608A"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> 2:</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> NO FIGURAN DATOS PERSONALES DEL INTERESADO EN LOS FICHEROS DEL RESPONSABLE</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="3F53C867" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESP</w:t></w:r><w:r w:rsidR="00083656" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>UESTA A LA PETICIÓN DE '.$peticion.' REF.</w:t></w:r><w:r w:rsidR="00351520" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> '.$ref.'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="0C3BDC6E" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="6F842699" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la petición de '.$peticion2.' de fecha </w:t></w:r><w:r w:rsidR="00503B12" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> realizada por </w:t></w:r><w:r w:rsidR="002F1F57" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>D./ª '.$item['representante'].'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="004C450C" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="002F1F57" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].',</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">   con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00D02341" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="3BC9B617" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="445B6998" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00351520" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> en calidad de responsable de  ficheros de datos de carácter personal, le informa que:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="01EED2E0" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="421C8902" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>No figuran datos personales del interesado en nuestros ficheros.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="5BC0DCF5" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="10E97B93" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a </w:t></w:r><w:r w:rsidR="008E2D7A" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="007A2585" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00A733ED" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="68365E56" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="08CE0254" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">A </w:t></w:r><w:r w:rsidR="00503B12" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRespuesta']).'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="623C185F" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p><w:p w14:paraId="367FD5C5" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="6138B790" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="57E6F6B6" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="5293976E" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="032545B6" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="47EFF610" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="5BBF9E54" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Fdo.</w:t></w:r><w:r w:rsidR="001847BD" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> '.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="6129D72D" w14:textId="77777777" w:rsidR="00821401" w:rsidRPr="008E67D7" w:rsidRDefault="00821401" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta3($ref,$item,$datos,$peticion){
	if($peticion=='ACCESO'){
		$tipo='DERECHO DE ACCESO EJERCITADO';
		$peticion2='acceso';
	}
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="54811189" w14:textId="77777777" w:rsidR="00667C2F" w:rsidRPr="008E67D7" w:rsidRDefault="00667C2F" w:rsidP="00667C2F"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>RESPUESTA A '.$tipo.'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="59ED98BD" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00F1031B" w:rsidP="00FC5330"><w:pPr><w:pStyle w:val="ListParagraph"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">SUPUESTO Nº </w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>3</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>: VISUALIZACIÓN EN PANTALLA</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="0B096D0B" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESPUE</w:t></w:r><w:r w:rsidR="00DC115E" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">STA A LA PETICIÓN DE '.$peticion.' </w:t></w:r><w:r w:rsidR="00083656" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>REF.</w:t></w:r><w:r w:rsidR="00DC115E" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> '.$ref.'</w:t></w:r><w:r w:rsidR="00F363A5" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="03A9056F" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="06421CEA" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la petición de '.$peticion2.' de fecha </w:t></w:r><w:r w:rsidR="0021040B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> realizada por </w:t></w:r><w:r w:rsidR="0094367B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00AA0010" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="0094367B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="002656D9" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].'</w:t></w:r><w:r w:rsidR="006B446D" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">   con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="006B446D" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="315BE937" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="2E0EC344" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00E66FCA" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">'.$datos['razonSocial'].' </w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>en calidad de responsable del fichero, le informa que:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="0765CAD8" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="67236A5A" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>Para proceder a la visualización de datos en pantalla solicitada, deberá personarse en nuestras dependencias</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, sitas en </w:t></w:r><w:r w:rsidR="00924983" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">, '.$item['otrosDatos'].'.</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">  </w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="54B3ED24" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="159D4DE1" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4EC1755F" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a </w:t></w:r><w:r w:rsidR="001D68D7" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="001D68D7" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">con domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00FC4E4F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.',</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="6EC0A609" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="6FAE9004" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">A </w:t></w:r><w:r w:rsidR="006639D3" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRespuesta']).'</w:t></w:r><w:r w:rsidR="00E97FC3" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="16D7E743" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="73CFA7EA" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="0069AD27" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="71246F6E" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="1DD3CCFF" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="616C0211" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="07981F32" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="71F16C8B" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="55DE1737" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="442D8999" w14:textId="77777777" w:rsidR="00FB1418" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="00FB1418"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Fdo. </w:t></w:r><w:r w:rsidR="00FB1418" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="22048CA6" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00FB1418" w:rsidP="00FB1418"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidR="003F678F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta4($n,$ref,$item,$datos,$peticion){
	if($peticion=='ACCESO'){
		$tipo='DERECHO DE ACCESO EJERCITADO';
		$peticion2='acceso';
	} else if($peticion=='RECTIFICACIÓN'){
		$tipo='DERECHO DE RECTIFICACIÓN EJERCITADO';
		$peticion2='rectificación';
	} else if($peticion=='CANCELACIÓN'){
		$tipo='DERECHO DE CANCELACIÓN EJERCITADO';
		$peticion2='cancelación';
	} else if($peticion=='OPOSICIÓN'){
		$tipo='DERECHO DE OPOSICIÓN EJERCITADO';
		$peticion2='oposición';
	} else if($peticion=='LIMITACIÓN'){
		$tipo='DERECHO DE LIMITACIÓN DE TRATAMIENTO EJERCITADO';
		$peticion2='limitación del tratamiento';
	} else if($peticion=='PORTABILIDAD DE LOS DATOS'){
		$tipo='DERECHO A LA PORTABILIDAD DE LOS DATOS EJERCITADO';
		$peticion2='portabilidad de los datos';
	} 
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="54811189" w14:textId="77777777" w:rsidR="00667C2F" w:rsidRPr="008E67D7" w:rsidRDefault="00667C2F" w:rsidP="00667C2F"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>RESPUESTA A '.$tipo.'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="1605EAB2" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00AF0AFB" w:rsidP="00AC5996"><w:pPr><w:pStyle w:val="ListParagraph"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> '.$n.'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">: </w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>COMUNICACIÓN AL RESPONSABLE DE FICHEROS</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="7C097398" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">Titular: </w:t></w:r><w:r w:rsidR="00081547" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">'.$datos['razonSocial'].' </w:t></w:r><w:r w:rsidR="00AF0AFB" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>EN CONCEPTO DE ENCARGADO DEL TRATAMIENTO.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="11A4929F" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="71998FBC" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">COMUNICACIÓN DE </w:t></w:r><w:r w:rsidR="0077004D" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>PETICIÓN DE ACCESO RECIBIDO REF.</w:t></w:r><w:r w:rsidR="00D5684B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> '.$ref.'</w:t></w:r><w:r w:rsidR="003A10CF" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="2B2C256C" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="57A4591B" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00081547" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en concepto de encargado del tratamiento de datos de carácter personal, de los cuales es responsable  </w:t></w:r><w:r w:rsidR="00CC1287" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['otrosDatos'].',</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">mediante el presente escrito </w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>informamos al responsable del fichero de la petición de '.$peticion2.' recibida</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> el día  </w:t></w:r><w:r w:rsidR="00244FB0" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidR="00ED3FF7" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> realizada por </w:t></w:r><w:r w:rsidR="008F2CB8" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00AA4A93" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="008F2CB8" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00AA4A93" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].',</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">   con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00D56BEB" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="56FB357A" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="7E3F6CE6" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Se adjunta copia de la solicitud de acceso recibida, </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>a fin de que el responsable del fichero resuelva sobre la misma.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="62419CC1" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="0A2772DB" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">A </w:t></w:r><w:r w:rsidR="00C92F4B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRespuesta']).'</w:t></w:r><w:r w:rsidR="004A2805" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="266DEDC3" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p><w:p w14:paraId="62472023" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="5DA8D7CC" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="316B9B55" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="0832C6C7" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="1F006AEE" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="6E12ED7F" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="672E1473" w14:textId="77777777" w:rsidR="004B4BD4" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="004B4BD4"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Fdo. </w:t></w:r><w:r w:rsidR="004B4BD4" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="6DD36E98" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="004B4BD4" w:rsidP="004B4BD4"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidR="00EB6218" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta5($n,$ref,$item,$datos,$peticion){
	if($peticion=='ACCESO'){
		$tipo='DERECHO DE ACCESO EJERCITADO';
		$peticion2='acceso';
	} else if($peticion=='RECTIFICACIÓN'){
		$tipo='DERECHO DE RECTIFICACIÓN EJERCITADO';
		$peticion2='rectificación';
	} else if($peticion=='OPOSICIÓN'){
		$tipo='DERECHO DE OPOSICIÓN EJERCITADO';
		$peticion2='oposición';
	} else if($peticion=='LIMITACIÓN'){
		$tipo='DERECHO DE LIMITACIÓN DE TRATAMIENTO EJERCITADO';
		$peticion2='limitación del tratamiento';
	} else if($peticion=='PORTABILIDAD DE LOS DATOS'){
		$tipo='DERECHO A LA PORTABILIDAD DE LOS DATOS EJERCITADO';
		$peticion2='portabilidad de los datos';
	} 
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="54811189" w14:textId="77777777" w:rsidR="00667C2F" w:rsidRPr="008E67D7" w:rsidRDefault="00667C2F" w:rsidP="00667C2F"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>RESPUESTA A '.$tipo.'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="6F5E9885" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00110391" w:rsidP="00850CCD"><w:pPr><w:pStyle w:val="ListParagraph"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> '.$n.'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>DATOS PERSONALES BLOQUEADOS</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="1B0E19DC" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESPUE</w:t></w:r><w:r w:rsidR="0077004D" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>STA A LA PETICIÓN DE '.$peticion.' REF.</w:t></w:r><w:r w:rsidR="0016643E" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> '.$ref.'</w:t></w:r><w:r w:rsidR="00CB2A65" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="0702C3FF" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1025D486" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la petición de '.$peticion2.' de fecha </w:t></w:r><w:r w:rsidR="006751B0" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> realizada por </w:t></w:r><w:r w:rsidR="002114B1" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="000E590A" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="002114B1" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="000E590A" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].'</w:t></w:r><w:r w:rsidR="00F423A3" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="005A279F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="1656C5B7" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="19EBB754" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="00E43E21" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> en calidad de responsable de  ficheros de datos de carácter personal, le informa que:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="02E4CDB0" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1EB386F2" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">Los datos personales del interesado se encuentran bloqueados, por lo que no pueden ser tratados excepto para su puesta a disposición de las Administraciones públicas, Jueces y Tribunales. </w:t></w:r><w:r w:rsidR="00484E00" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>En virtud de lo establecido en el artículo 17.3 RGPD</w:t></w:r><w:r w:rsidR="00373E58" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">, </w:t></w:r><w:r w:rsidR="00484E00" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> no pueden ser suprimidos de inmediato, si bien u</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>na vez transcurrido el plazo de prescripción que corresponda, los</w:t></w:r><w:r w:rsidR="00AC59DF" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> datos personales bloqueados serán</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> eliminados de nuestros ficheros</w:t></w:r><w:r w:rsidR="00645AF4" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>.</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="684CB99F" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="6279F315" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a </w:t></w:r><w:r w:rsidR="00242A8E" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00D31078" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.',</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="7B6F632A" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="7B3C29BB" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">A </w:t></w:r><w:r w:rsidR="008A3DA1" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRespuesta']).'</w:t></w:r><w:r w:rsidR="00F01E0D" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>.</w:t></w:r></w:p><w:p w14:paraId="4E19E54A" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="527E398C" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="4F729EC1" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="7B41255B" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="0D430CB2" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="604ACD39" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="30AFE544" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="66FB2835" w14:textId="77777777" w:rsidR="006132B0" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="006132B0"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Fdo. </w:t></w:r><w:r w:rsidR="006132B0" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="4C427689" w14:textId="77777777" w:rsidR="00577845" w:rsidRPr="008E67D7" w:rsidRDefault="006132B0" w:rsidP="00451296"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidR="00E56F47" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta6($n,$ref,$item,$datos,$peticion){
	if($peticion=='ACCESO'){
		$tipo='DERECHO DE ACCESO EJERCITADO';
		$peticion2='acceso';
	} else if($peticion=='RECTIFICACIÓN'){
		$tipo='DERECHO DE RECTIFICACIÓN EJERCITADO';
		$peticion2='rectificación';
	} else if($peticion=='CANCELACIÓN'){
		$tipo='DERECHO DE CANCELACIÓN EJERCITADO';
		$peticion2='cancelación';
	} else if($peticion=='OPOSICIÓN'){
		$tipo='DERECHO DE OPOSICIÓN EJERCITADO';
		$peticion2='oposición';
	} else if($peticion=='LIMITACIÓN'){
		$tipo='DERECHO DE LIMITACIÓN DE TRATAMIENTO EJERCITADO';
		$peticion2='limitación del tratamiento';
	} else if($peticion=='PORTABILIDAD DE LOS DATOS'){
		$tipo='DERECHO A LA PORTABILIDAD DE LOS DATOS EJERCITADO';
		$peticion2='portabilidad de los datos';
	}
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$fichero=datosRegistro('declaraciones',$item['codigoTratamiento'],$campo='codigo');
	$tratamientos=array(1=>'Automatizado',2=>'Manual',3=>'Mixto');
	$niveles=array(1=>'Básico',2=>'Medio',3=>'Alto');
	if(!$fichero){
		$fichero['codigoCliente']=0;
		$fichero['nombreFichero']='';
		$fichero['tratamiento']='';
		$fichero['nivel']='';
		$fichero['datosTratados']='';
		$fichero['origenDatos']='';
		$fichero['colectivos']='';
		$fichero['plazo']='';
		$fichero['cesiones']='';
		$fichero['encargados']='';
	} else {
		$datosTratados=array('checkDatosProtegidos0'=>'Ideología','checkDatosProtegidos1'=>'Afiliación sindical','checkDatosProtegidos2'=>'Religión','checkDatosProtegidos3'=>'Creencias','checkDatosEspeciales0'=>'Origen racial o Étnico','checkDatosEspeciales1'=>'Salud','checkDatosEspeciales2'=>'Vida sexual','checkDatosIdentificativos0'=>'NIF / DNI','checkDatosIdentificativos1'=>'Nº SS / Mutualidad','checkDatosIdentificativos2'=>'Nombre y apellidos','checkDatosIdentificativos3'=>'Tarjeta sanitaria','checkDatosIdentificativos4'=>'Dirección','checkDatosIdentificativos5'=>'Teléfono','checkDatosIdentificativos6'=>'Firma','checkDatosIdentificativos7'=>'Huella','checkDatosIdentificativos8'=>'Imagen / voz','checkDatosIdentificativos9'=>'Marcas físicas','checkDatosIdentificativos10'=>'Firma electrónica','checkDatosIdentificativos11'=>'Correo electrónico','checkDatosIdentificativos12'=>'Otros datos biométricos','checkDatosIdentificativos13'=>$fichero['ODCI']);
		$fichero['datosTratados']='';
		foreach ($datosTratados as $key => $value) {
			if($fichero[$key]!='NO'){
				$fichero['datosTratados'].='<w:p w14:paraId="09056222" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>- '.$value.'</w:t></w:r></w:p>';
			}
		}
		if($fichero['otrosTiposDatos']=='SI'){
			$datosTratados=array('checkOtrosTiposDatos0'=>'Características Personales','checkOtrosTiposDatos1'=>'Académicos y profesionales','checkOtrosTiposDatos2'=>'Detalles del empleo','checkOtrosTiposDatos3'=>'Económicos, financieros y de seguros');
			foreach ($datosTratados as $key => $value) {
				if($fichero[$key]!='NO'){
					$fichero['datosTratados'].='<w:p w14:paraId="09056222" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>- '.$value.'</w:t></w:r></w:p>';
				}
			}
			if($fichero['desc_otros_tipos']!=''){
				$fichero['datosTratados'].='<w:p w14:paraId="09056222" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>- '.$fichero['desc_otros_tipos'].'</w:t></w:r></w:p>';
			}
		}
		$datosTratados=array('checkOrigen0'=>'El propio interesado o su representante legal','checkOrigen1'=>'Otras personas físicas','checkOrigen2'=>'Fuentes accesibles al público','checkOrigen3'=>'Registros públicos','checkOrigen4'=>'Entidad privada','checkOrigen5'=>'Administraciones Públicas');
		$fichero['origenDatos']='';
		foreach ($datosTratados as $key => $value) {
			if($fichero[$key]!='NO'){
				if($fichero['origenDatos']!=''){
					$fichero['origenDatos'].=', ';
				}
				$fichero['origenDatos'].=$value;
			}
		}
		$datosTratados=array('checkColectivo0'=>'Empleados','checkColectivo1'=>'Clientes y Usuarios','checkColectivo2'=>'Proveedores','checkColectivo3'=>'Asociados o Miembros','checkColectivo4'=>'Propietarios o Arrendatarios','checkColectivo5'=>'Pacientes','checkColectivo6'=>'Estudiantes','checkColectivo7'=>'Personas de Contacto','checkColectivo8'=>'Padres o Tutores','checkColectivo9'=>'Representante Legal','checkColectivo10'=>'Solicitantes','checkColectivo11'=>'Beneficiarios','checkColectivo12'=>'Cargos Públicos','checkColectivo13'=>$fichero['otro_col']);
		$fichero['colectivos']='';
		foreach ($datosTratados as $key => $value) {
			if($fichero[$key]!='NO'){
				if($fichero['colectivos']!=''){
					$fichero['colectivos'].=', ';
				}
				$fichero['colectivos'].=$value;
			}
		}
		$datosTratados=array('checkDestinatariosCesiones0'=>'Organizaciones o Personas Directamente Relacionadas con el Responsable','checkDestinatariosCesiones1'=>'Organismos de la Seguridad Social','checkDestinatariosCesiones2'=>'Registros Públicos','checkDestinatariosCesiones3'=>'Colegios Profesionales','checkDestinatariosCesiones4'=>'Administración Tributaria','checkDestinatariosCesiones5'=>'Otros Órganos de la Administración Pública','checkDestinatariosCesiones6'=>'Comisión Nacional del Mercado de Valores','checkDestinatariosCesiones7'=>'Comisión Nacional del Juego','checkDestinatariosCesiones8'=>'Notarios y Procuradores','checkDestinatariosCesiones9'=>'Fuerzas y Cuerpos de Seguridad','checkDestinatariosCesiones10'=>'Organismos de la Unión Europea','checkDestinatariosCesiones11'=>'Entidades Dedicadas al Cumplimiento o Incumplimiento de Obligaciones Dinerarias
','checkDestinatariosCesiones12'=>'Bancos, Cajas de Ahorros y Cajas Rurales','checkDestinatariosCesiones13'=>'Entidades Aseguradoras','checkDestinatariosCesiones14'=>'Otras Entidades Financieras','checkDestinatariosCesiones15'=>'Entidades Sanitarias','checkDestinatariosCesiones16'=>'Prestaciones de Servicios de Telecomunicaciones','checkDestinatariosCesiones17'=>'Empresas Dedicadas a Publicidad o Marketing Directo','checkDestinatariosCesiones18'=>'Asociaciones y Organizaciones sin Ánimo de Lucro','checkDestinatariosCesiones19'=>'Sindicatos y Juntas de Personal','checkDestinatariosCesiones20'=>$fichero['desc_otros']);
		$fichero['cesiones']='';
		foreach ($datosTratados as $key => $value) {
			if($fichero[$key]!='NO'){
				if($fichero['cesiones']!=''){
					$fichero['cesiones'].=', ';
				}
				$fichero['cesiones'].=$value;
			}
		}
		$fichero['encargados']=obtenerEncargados($datos,$fichero);
	}
	$res='<w:p w14:paraId="54811189" w14:textId="77777777" w:rsidR="00667C2F" w:rsidRPr="008E67D7" w:rsidRDefault="00667C2F" w:rsidP="00667C2F"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>RESPUESTA A '.$tipo.'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="36E18E05" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="0032256A" w:rsidP="00EA54BD"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº</w:t></w:r><w:r w:rsidR="00331045" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> '.$n.'</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="00127C15" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> OTORGAMIENTO DE </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t>'.$peticion.'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="06122DC9" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESP</w:t></w:r><w:r w:rsidR="00EE30E2" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>UESTA A LA PETICIÓN DE '.$peticion.' REF.</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="004E2AC9" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>'.$ref.'</w:t></w:r><w:r w:rsidR="004E2AC9" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="027334F1" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="18C2A884" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la petición de '.$peticion2.' de fecha </w:t></w:r><w:r w:rsidR="00AD0C8C" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> realizada por </w:t></w:r><w:r w:rsidR="00333922" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00A446ED" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].',</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> en nombre y representación de </w:t></w:r><w:r w:rsidR="00333922" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00A446ED" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].',</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">   con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00213F86" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="7EC794D0" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="49A4DDA2" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="0098103B" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="00105DC6" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>, en calidad de Responsable del Tratamient</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>o, le informa que</w:t></w:r><w:r w:rsidR="00105DC6" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> l</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>os datos personales que obran en nuestro poder y que corresponden a</w:t></w:r><w:r w:rsidR="00C14D18" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>l</w:t></w:r><w:r w:rsidR="00105DC6" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> interesad</w:t></w:r><w:r w:rsidR="00C14D18" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>o/a la interesada,</w:t></w:r><w:r w:rsidR="00105DC6" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00105DC6" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>están siendo tratados por el/la  Responsable del Tratamiento</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>, aplicándoles todas las medidas d</w:t></w:r><w:r w:rsidR="00105DC6" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>e seguridad</w:t></w:r><w:r w:rsidR="0087362F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> que hemos considerado </w:t></w:r><w:r w:rsidR="00C14D18" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> necesarias </w:t></w:r><w:r w:rsidR="00105DC6" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> para el cumplimiento de lo establecido en el Reglamento General de Protección de Datos, encontrándose</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> en la situación siguiente:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="54D7633F" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="66074818" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="009D7965"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>D</w:t></w:r><w:r w:rsidR="00CD1F27" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>ENOMINACIÓN DEL TRATAMIENTO/FICHERO</w:t></w:r><w:r w:rsidR="007C4FE1" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> EN EL QUE SE ENCUENTRAN INCLUIDOS LOS DATOS</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">: </w:t></w:r><w:r w:rsidR="007576AD" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$fichero['nombreFichero'].'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="4065A340" w14:textId="77777777" w:rsidR="00F71DFF" w:rsidRPr="008E67D7" w:rsidRDefault="00F71DFF" w:rsidP="009D7965"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="1634A29F" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="009D7965"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>SISTEMA DE TRATAMIENTO: '.$tratamientos[$fichero['tratamiento']].'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="7C4F7577" w14:textId="77777777" w:rsidR="00F71DFF" w:rsidRPr="008E67D7" w:rsidRDefault="00F71DFF" w:rsidP="009D7965"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="0F5A4FEB" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="009D7965"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>NIVEL DE SEGURIDAD: '.$niveles[$fichero['nivel']].'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="16B89D40" w14:textId="77777777" w:rsidR="00F71DFF" w:rsidRPr="008E67D7" w:rsidRDefault="00F71DFF" w:rsidP="009D7965"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="4375608A" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="009D7965"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>DATOS TRATADOS:</w:t></w:r></w:p>';
	$res.=$fichero['datosTratados'];
	$res.='<w:p w14:paraId="61DEF383" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4130DBD4" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>FINALIDAD: '.$fichero['desc_fin_usos'].'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="0A6E3BCB" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="41F8A42D" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>ORIGEN DE LOS DATOS: '.$fichero['origenDatos'].'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="7ABF2C7C" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="131A7E98" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>COLECTIVOS DE INTERESADOS: '.$fichero['colectivos'].'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="7A775645" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4F359917" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="008C50D6" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>PLAZO O CRITERIO DE CONSERVACIÓN DE LA INFORMACIÓN: '.$fichero['plazo'].'</w:t></w:r><w:r w:rsidR="007652A7" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="563EE34B" w14:textId="77777777" w:rsidR="009857BA" w:rsidRPr="008E67D7" w:rsidRDefault="009857BA" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="45A28937" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>DESTINATARIOS DE CESIONES: '.$fichero['cesiones'].'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="0776DC13" w14:textId="77777777" w:rsidR="007652A7" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="007652A7"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="75E57740" w14:textId="77777777" w:rsidR="009857BA" w:rsidRPr="008E67D7" w:rsidRDefault="007652A7" w:rsidP="009857BA"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>ENCARGADOS DEL TRATAMIENTO: '.$fichero['encargados'].'</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="7F1CD4D9" w14:textId="77777777" w:rsidR="008315F5" w:rsidRPr="008E67D7" w:rsidRDefault="008315F5" w:rsidP="009857BA"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="101E79C1" w14:textId="77777777" w:rsidR="0087362F" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="00A71F99"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Así mismo le informamos que</w:t></w:r><w:r w:rsidR="0087362F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> tiene derecho a solicitar del Responsable del Tratamiento la rectificación o supresión de sus datos personales o la limitación </w:t></w:r><w:r w:rsidR="003125CB" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">del tratamiento de los mismos, así como </w:t></w:r><w:r w:rsidR="0087362F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">oponerse a dicho tratamiento. </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Para ello</w:t></w:r><w:r w:rsidR="0087362F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> puede dirigirse a </w:t></w:r><w:r w:rsidR="00AA2B38" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="00AA2B38" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">con </w:t></w:r><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:lastRenderedPageBreak/><w:t xml:space="preserve">domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="0076546B" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.'</w:t></w:r><w:r w:rsidR="0087362F" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="7C318A65" w14:textId="77777777" w:rsidR="0087362F" w:rsidRPr="008E67D7" w:rsidRDefault="0087362F" w:rsidP="00A71F99"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1397481E" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="0087362F" w:rsidP="00A71F99"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>También  ti</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>ene derecho a  recabar la tutela de la Agencia Española de Protección de Datos</w:t></w:r><w:r w:rsidR="00B20654" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>, para lo cual puede obtener la información necesaria a través de la página web oficial de la Agencia</w:t></w:r><w:r w:rsidR="001A3C87" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="2DB67174" w14:textId="77777777" w:rsidR="00AC6396" w:rsidRPr="008E67D7" w:rsidRDefault="00AC6396" w:rsidP="00AC6396"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4B2B3EB7" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">A </w:t></w:r><w:r w:rsidR="00AD0C8C" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRespuesta']).'.</w:t></w:r></w:p><w:p w14:paraId="5511C892" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="26487DEE" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="4613D4B9" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="01D49F11" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="54E70B31" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="3EA54399" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="651F2B88" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="001A3C87"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="77D5F045" w14:textId="77777777" w:rsidR="007A4836" w:rsidRPr="008E67D7" w:rsidRDefault="001A3C87" w:rsidP="007A4836"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Fdo. </w:t></w:r><w:r w:rsidR="007A4836" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="656F8E89" w14:textId="77777777" w:rsidR="001A3C87" w:rsidRPr="008E67D7" w:rsidRDefault="007A4836" w:rsidP="007A4836"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidR="002541EE" w:rsidRPr="008E67D7"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function obtenerEncargados($cliente,$fichero){
	$res='';
	$trabajo=datosRegistro('trabajos',$cliente['codigoCliente'],'codigoCliente');
	$formulario=recogerFormularioServicios($trabajo);
	$encargados=array(158=>574,168=>575,178=>576,189=>577,200=>578,210=>579,220=>609,230=>580,240=>581,250=>582,260=>583,270=>584,280=>585,290=>586,487=>587);
	foreach ($encargados as $key => $value) {
		if($formulario['pregunta'.$key]=='SI'){
			$ficheros=explode('&$&', $formulario['pregunta'.$value]);
			if(in_array($fichero['codigo'], $ficheros)){
				if($res!=''){
					$res.=', ';
				}
				$n=$key+1;
				$res.=sanearCaracteres($formulario['pregunta'.$n]);
			}
		}
	}
	$encargados=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
	while($e=mysql_fetch_assoc($encargados)){
		$ficheros=explode('&$&', $e['ficheroEncargado']);
		if(in_array($fichero['codigo'], $ficheros)){
			if($res!=''){
				$res.=', ';
			}
			$res.=sanearCaracteres($e['nombreEncargado']);
		}
	}
	return $res;
}

function respuesta7($n,$ref,$item,$datos,$peticion){
	if($peticion=='RECTIFICACIÓN'){
		$tipo='DERECHO DE RECTIFICACIÓN EJERCITADO';
		$peticion2='rectificación';
	} else if($peticion=='CANCELACIÓN'){
		$tipo='DERECHO DE CANCELACIÓN EJERCITADO';
		$peticion2='cancelación';
	} else if($peticion=='LIMITACIÓN'){
		$tipo='DERECHO DE LIMITACIÓN DE TRATAMIENTO EJERCITADO';
		$peticion2='limitación del tratamiento';
	}
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="7B6F752E" w14:textId="77777777" w:rsidR="00BC056B" w:rsidRPr="00E222BB" w:rsidRDefault="00BC056B" w:rsidP="004F6A0C"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w14:paraId="7AB33DC4" w14:textId="77777777" w:rsidR="00134B5B" w:rsidRPr="00E222BB" w:rsidRDefault="00134B5B" w:rsidP="00134B5B"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:lastRenderedPageBreak/><w:t>SUPUESTO Nº '.$n.'.1</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="49558A47" w14:textId="77777777" w:rsidR="00C23B38" w:rsidRPr="00E222BB" w:rsidRDefault="00C23B38" w:rsidP="00320CCC"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">COMUNICACIÓN </w:t></w:r><w:r w:rsidR="00134B5B" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">A CESIONARIO </w:t></w:r><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:u w:val="single"/></w:rPr><w:t>DE '.$tipo.' Y OTORGADO</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="71A6AC38" w14:textId="77777777" w:rsidR="00C23B38" w:rsidRPr="00E222BB" w:rsidRDefault="00C23B38" w:rsidP="00C23B38"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="2229C5CC" w14:textId="77777777" w:rsidR="00C23B38" w:rsidRPr="00E222BB" w:rsidRDefault="00C23B38" w:rsidP="00C23B38"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>COMUNICACIÓN DE</w:t></w:r><w:r w:rsidR="00134B5B" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> RECTIFICACIÓN Nº </w:t></w:r><w:r w:rsidR="00C913BF" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00853E26" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>'.$tipo.'</w:t></w:r><w:r w:rsidR="00C913BF" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="3795FADA" w14:textId="77777777" w:rsidR="00C913BF" w:rsidRPr="00E222BB" w:rsidRDefault="00C913BF" w:rsidP="00C23B38"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="0FC54B93" w14:textId="77777777" w:rsidR="00C23B38" w:rsidRPr="00E222BB" w:rsidRDefault="00BA064E" w:rsidP="00C23B38"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>, en c</w:t></w:r><w:r w:rsidR="00853E26" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>oncepto de responsable del tratamiento</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> de datos de carácter personal cedidos a  </w:t></w:r><w:r w:rsidR="00491822" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['otrosDatos'].',</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">mediante el presente escrito informamos al cesionario referenciado </w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>que hemos procedido a CONCEDER LA</w:t></w:r><w:r w:rsidR="00DB23EE" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> '.$peticion.'</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="008A1898" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">de datos </w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">solicitada por </w:t></w:r><w:r w:rsidR="00FA30B6" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00DB23EE" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidR="00D7031B" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con DNI '.$item['representanteDni'].'</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="00FA30B6" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00DB23EE" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].'</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">  con DNI </w:t></w:r><w:r w:rsidR="00D7031B" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesadoDni'].',</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00DB23EE" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">'.$item['direccion'].', </w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">recibida el día  </w:t></w:r><w:r w:rsidR="00DB23EE" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="1EF0F9BB" w14:textId="77777777" w:rsidR="00DB23EE" w:rsidRPr="00E222BB" w:rsidRDefault="00DB23EE" w:rsidP="00C23B38"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="1DA93EAC" w14:textId="77777777" w:rsidR="00C23B38" w:rsidRPr="00E222BB" w:rsidRDefault="00DC2FB6" w:rsidP="00C23B38"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>En cumplimiento del artículo 19 del Reglamento General de Protección de Datos, s</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>e adjunta copia de la solicitud recib</w:t></w:r><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>ida, a fin de que el cesionario de dichos datos tenga conocimiento de la '.$peticion2.' efectuada.</w:t></w:r><w:r w:rsidR="00C23B38" w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="00D607C2" w14:textId="77777777" w:rsidR="00C23B38" w:rsidRPr="00E222BB" w:rsidRDefault="00C23B38" w:rsidP="00C23B38"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="2DF44497" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00EE48CB"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>A '.formateaFechaWeb($item['fechaRespuesta']).'.</w:t></w:r></w:p><w:p w14:paraId="0EF827A8" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00607501"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="61160BFE" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00607501"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="25D6FB0F" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00607501"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="436A4045" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00607501"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="5E307E64" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00607501"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="056DAEAE" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00607501"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="5BD2EF59" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00607501"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="463514A1" w14:textId="77777777" w:rsidR="00607501" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00607501"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Fdo. '.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="7D654285" w14:textId="77777777" w:rsidR="00AD761F" w:rsidRPr="00E222BB" w:rsidRDefault="00607501" w:rsidP="00983AEC"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidRPr="00E222BB"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta8($ref,$item,$datos,$peticion){
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="51F80B1B" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="002B4A73" w:rsidRDefault="002F4B2A" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="002B4A73"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t xml:space="preserve">RESPUESTA A </w:t></w:r><w:r w:rsidR="00AF1F83" w:rsidRPr="002B4A73"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve">DERECHO DE </w:t></w:r><w:r w:rsidR="007703A4" w:rsidRPr="002B4A73"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>C</w:t></w:r><w:r w:rsidR="00AF1F83" w:rsidRPr="002B4A73"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>ANCEL</w:t></w:r><w:r w:rsidR="007703A4" w:rsidRPr="002B4A73"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>ACIÓN</w:t></w:r><w:r w:rsidRPr="002B4A73"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> EJERCITADO</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="33CEFB21" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="00C917F1" w:rsidP="00B229A5"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº 4</w:t></w:r><w:r w:rsidR="00E55CE4" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>: DENEGACIÓN DE SUPRESIÓN INMEDIATA</w:t></w:r><w:r w:rsidR="002F4B2A" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> POR </w:t></w:r><w:r w:rsidR="00E55CE4" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>ART. 17.3 RGPD</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="2684DF2E" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="002F4B2A" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESPUEST</w:t></w:r><w:r w:rsidR="009A6EB7" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>A A LA PETICIÓN DE CANCELACIÓN</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> REF. '.$ref.'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="5272B868" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="002F4B2A" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="16195FAD" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="002F4B2A" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la petición de </w:t></w:r><w:r w:rsidR="000C0EC8" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>c</w:t></w:r><w:r w:rsidR="009A6EB7" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>ancel</w:t></w:r><w:r w:rsidR="000C0EC8" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>ación</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> de fecha</w:t></w:r><w:r w:rsidR="000C0EC8" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> '.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> realizada por </w:t></w:r><w:r w:rsidR="009B2EC7" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="009A4E4A" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="009B2EC7" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00FB005D" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].'</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">   con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00841BD5" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="667826EB" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="002F4B2A" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="2A79F29E" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="00FE63E3" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="002F4B2A" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>en calidad de responsable de  ficheros de datos de carácter personal, le informa que:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="235D6ACC" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="002F4B2A" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="51D6CDF4" w14:textId="77777777" w:rsidR="00E55CE4" w:rsidRPr="00C3074F" w:rsidRDefault="00E55CE4" w:rsidP="00E55CE4"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> En virtud de lo establecido en el artículo 17.3 RGPD,  no pueden ser suprimidos de inmediato, si bien una vez transcurrido el plazo de p</w:t></w:r><w:r w:rsidR="00DB390E" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">rescripción que corresponda, sus datos personales </w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> serán eliminados de nuestros ficheros.</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="38A7EC2C" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="002F4B2A" w:rsidP="007E7F4D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1D1C285B" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="002F4B2A" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a </w:t></w:r><w:r w:rsidR="00B2275C" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00460777" w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$direccion.'</w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="5C44F2C3" w14:textId="77777777" w:rsidR="002F4B2A" w:rsidRPr="00C3074F" w:rsidRDefault="002F4B2A" w:rsidP="002F4B2A"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="5C221C86" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>A '.formateaFechaWeb($item['fechaRespuesta']).'.</w:t></w:r></w:p><w:p w14:paraId="23AD19EC" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="42C83306" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="155A412D" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="42832693" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="3C6BBD54" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="0C4B16E3" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="37B9AE00" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Fdo. '.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="6954FDEE" w14:textId="77777777" w:rsidR="006A6C1B" w:rsidRPr="00C3074F" w:rsidRDefault="006A6C1B" w:rsidP="006A6C1B"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidRPr="00C3074F"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta9($ref,$item,$datos,$peticion){
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="11851D71" w14:textId="77777777" w:rsidR="00B47470" w:rsidRDefault="00B47470" w:rsidP="00525968"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="47C5FFD1" w14:textId="77777777" w:rsidR="00B916DD" w:rsidRPr="002B2D41" w:rsidRDefault="00B916DD" w:rsidP="002B2D41"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="11F3D776" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t xml:space="preserve">RESPUESTA A DERECHO </w:t></w:r><w:r w:rsidR="00607566" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>A LA PORTABILIDAD DE LOS DATOS</w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> EJERCITADO</w:t></w:r></w:p><w:p w14:paraId="529D77DF" w14:textId="1E2FF59A" w:rsidR="00694E37" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="002D3B61"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº 5: DENEGA</w:t></w:r><w:r w:rsidR="00484026" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>CIÓN DE PORTABILIDAD</w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> POR LEY</w:t></w:r></w:p><w:p w14:paraId="7E0C9414" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESPUEST</w:t></w:r><w:r w:rsidR="00CF6274" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>A A LA PETICIÓN DE PORTABILIDAD DE LOS DATOS REF</w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>. '.$ref.'</w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="4399BCF3" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w14:paraId="7BF28F83" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>El presente escrito es para dar resp</w:t></w:r><w:r w:rsidR="00FA0031" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>uesta a la petición de portabilidad de los datos</w:t></w:r><w:r w:rsidR="006C3CD9" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> de fecha </w:t></w:r><w:r w:rsidR="00A3048F" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidR="006C3CD9" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>,</w:t></w:r><w:r w:rsidR="00B51950" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">realizada por </w:t></w:r><w:r w:rsidR="00B91963" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00B51950" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="00B91963" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="007B5D67" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].',</w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="007D5D45" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="2378A19B" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="65DC8B73" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00CC551F" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="00DA0198" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> en calidad de responsable de  ficheros de datos de carácter personal, le informa que:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="58DEBA4E" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="739E2204" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">Se le deniega la solicitud de portabilidad </w:t></w:r><w:r w:rsidR="00075BE0" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">referenciada, en virtud de </w:t></w:r><w:r w:rsidR="00254221" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>lo establecido en el artículo 20</w:t></w:r><w:r w:rsidR="00834BB4" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00E16C14" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> del Reglamento General de Protección de Datos</w:t></w:r><w:r w:rsidR="00B62939" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>.</w:t></w:r></w:p><w:p w14:paraId="148BF1EF" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="57CD28AD" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00562575" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">  El artículo 20</w:t></w:r><w:r w:rsidR="0016178C" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00DA0198" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> del Reglamento </w:t></w:r><w:r w:rsidR="00D47503" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>General de Protección de Datos</w:t></w:r><w:r w:rsidR="00DA0198" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> establece lo siguiente:</w:t></w:r></w:p><w:p w14:paraId="749E604B" w14:textId="77777777" w:rsidR="00B82244" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="0016178C"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> “</w:t></w:r><w:r w:rsidR="00B82244" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>1. El interesado tendrá derecho a recibir los datos personales que le incumban, que haya facilitado a un responsable del tratamiento, en un formato estructurado, de uso común y lectura mecánica, y a transmitirlos a otro responsable del tratamiento sin que lo impida el responsable al que se los hubiera facilitado, cuando:</w:t></w:r></w:p><w:p w14:paraId="7AE10937" w14:textId="77777777" w:rsidR="00B82244" w:rsidRPr="00B76B15" w:rsidRDefault="00B82244" w:rsidP="00B82244"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="5"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>El tratamiento esté basado en el consentimiento con arreglo al artículo 6, apartado 1, letra a), o el artículo 9, apartado 2, letra a), o en un contrato con arreglo al artículo 6, apartado 1, letra b), y</w:t></w:r></w:p><w:p w14:paraId="262B7972" w14:textId="77777777" w:rsidR="00B82244" w:rsidRPr="00B76B15" w:rsidRDefault="00B82244" w:rsidP="00B82244"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="5"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>El tratamiento se efectúe por medios automatizados.</w:t></w:r></w:p><w:p w14:paraId="6CDEC4A2" w14:textId="77777777" w:rsidR="00B82244" w:rsidRPr="00B76B15" w:rsidRDefault="00B82244" w:rsidP="00B82244"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="7"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>Al ejercer su derecho  a la portabilidad de los datos de acuerdo con el apartado 1, el interesado tendrá derecho a que los datos personales se transmitan directamente de responsable a responsable cuando sea técnicamente posible.</w:t></w:r></w:p><w:p w14:paraId="392A59DD" w14:textId="77777777" w:rsidR="00F12444" w:rsidRPr="00B76B15" w:rsidRDefault="00F12444" w:rsidP="00B82244"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="7"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>El ejercicio del derecho mencionado en el apartado 1 del presente artículo se entenderá sin perjuicio del artículo 17. Tal derecho no se aplicará al tratamiento que sea necesario para el cumplimiento de una misión realizada en interés público o en el ejercicio de poderes públicos conferidos al responsable del tratamiento.</w:t></w:r></w:p><w:p w14:paraId="28C6330D" w14:textId="77777777" w:rsidR="00137E27" w:rsidRPr="00B76B15" w:rsidRDefault="00F12444" w:rsidP="00B82244"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="7"/></w:numPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>El derecho mencionado en el apartado 1 no afectará negativamente a los derechos y libertades de otros.</w:t></w:r><w:r w:rsidR="004A5B59" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>”</w:t></w:r><w:r w:rsidR="00137E27" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p><w:p w14:paraId="56A1434D" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p>';
	$res.='<w:p w14:paraId="039FC90D" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a </w:t></w:r><w:r w:rsidR="00CC551F" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].',</w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00AC17BF" w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">'.$direccion.', </w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="23F0B6AA" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00B76B15" w:rsidRDefault="00DA0198" w:rsidP="00321C8F"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="649C6169" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>A '.formateaFechaWeb($item['fechaRespuesta']).'.</w:t></w:r></w:p><w:p w14:paraId="5E51E830" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p><w:p w14:paraId="07455AA0" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="0AB24FCB" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="75ABE3E7" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Fdo. '.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="5E5307DB" w14:textId="77777777" w:rsidR="00B21525" w:rsidRPr="00B76B15" w:rsidRDefault="00694E37" w:rsidP="00E35D26"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidRPr="00B76B15"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta10($ref,$item,$datos,$peticion){
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="4BE65F27" w14:textId="77777777" w:rsidR="00707C54" w:rsidRPr="00707C54" w:rsidRDefault="00707C54" w:rsidP="002B2D41"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="3BE48EB7" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t xml:space="preserve">RESPUESTA A DERECHO DE </w:t></w:r><w:r w:rsidR="007179A2" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>OPOSI</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>CIÓN EJERCITADO</w:t></w:r></w:p><w:p w14:paraId="4A43C77D" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="002D3B61"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº 5: DENEGA</w:t></w:r><w:r w:rsidR="00B6200E" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>CIÓN DE OPOSI</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>CIÓN POR LEY</w:t></w:r></w:p><w:p w14:paraId="2548EB6A" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESPUEST</w:t></w:r><w:r w:rsidR="007179A2" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>A A LA PETICIÓN DE OPOSICIÓN</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> REF. '.$ref.'</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="797C15A6" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="285780D6" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la petición de oposición de fecha </w:t></w:r><w:r w:rsidR="00A3048F" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r><w:r w:rsidR="00B51950" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">realizada por </w:t></w:r><w:r w:rsidR="00B91963" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="00B51950" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['representante'].'</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">, en nombre y representación de </w:t></w:r><w:r w:rsidR="00B91963" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">D./ª </w:t></w:r><w:r w:rsidR="007B5D67" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['interesado'].',</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="007D5D45" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="3FF64B36" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="66B03CAB" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00CC551F" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="00DA0198" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> en calidad de responsable de  ficheros de datos de carácter personal, le informa que:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="12145519" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="311165EB" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">Se le deniega la solicitud de oposición </w:t></w:r><w:r w:rsidR="00075BE0" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>referenciada, en virtud de lo establecido en el artículo 21</w:t></w:r><w:r w:rsidR="007179BC" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>.1</w:t></w:r><w:r w:rsidR="00834BB4" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00E16C14" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> del Reglamento General de Protección de Datos</w:t></w:r><w:r w:rsidR="00B62939" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>.</w:t></w:r></w:p><w:p w14:paraId="037FA6F1" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w14:paraId="5334125D" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00D47503" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">  El artículo 21</w:t></w:r><w:r w:rsidR="0016178C" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">.1 </w:t></w:r><w:r w:rsidR="00DA0198" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve"> del Reglamento </w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>General de Protección de Datos</w:t></w:r><w:r w:rsidR="00DA0198" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> establece lo siguiente:</w:t></w:r></w:p><w:p w14:paraId="0FB15713" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="21662CDA" w14:textId="77777777" w:rsidR="00137E27" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="0016178C"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> “</w:t></w:r><w:r w:rsidR="0016178C" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>El interesado tendrá derecho a oponerse en cualquier momento, por motivos relacionados con su situación particular, a que datos personales que le conciernan sean objeto de un tratamiento basado en lo dispuesto en el artículo 6, apartado 1, letras e) ó f)</w:t></w:r><w:r w:rsidR="00AC07F3" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>, incluida la elaboración de perfiles sobre la base de dichas disposiciones. El responsable del tratamiento dejará de tratar los datos personales, salvo que acredite motivos legítimos imperiosos para el tratamiento que prevalezcan sobre los intereses, los derechos y las libertades del interesado, o para la formulación, el ejercicio o la defensa de reclamaciones.</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>”</w:t></w:r><w:r w:rsidR="00137E27" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p><w:p w14:paraId="67D35C78" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p>';
	$res.='<w:p w14:paraId="328F470E" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00DA0198"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a </w:t></w:r><w:r w:rsidR="00CC551F" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].',</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> con domicilio  a efectos de notificaciones en </w:t></w:r><w:r w:rsidR="00AC17BF" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">'.$direccion.', </w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="3DA5A74C" w14:textId="77777777" w:rsidR="00DA0198" w:rsidRPr="00707C54" w:rsidRDefault="00DA0198" w:rsidP="00321C8F"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="170FE5D4" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>A '.formateaFechaWeb($item['fechaRespuesta']).'.</w:t></w:r></w:p><w:p w14:paraId="57F0B9F9" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p><w:p w14:paraId="491B7BFE" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="3E70A6C0" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="02B9E7AC" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="30F975BF" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="6D26E612" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Fdo. '.$datos['representante'].'</w:t></w:r></w:p><w:p w14:paraId="08552E8A" w14:textId="77777777" w:rsidR="00694E37" w:rsidRPr="00707C54" w:rsidRDefault="00694E37" w:rsidP="00694E37"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r></w:p>';
	return $res;
}

function respuesta11($ref,$item,$datos,$peticion){
	$direccion=$datos['domicilio'].', CP '.$datos['cp'].' '.$datos['localidad'].' ('.convertirMinuscula($datos['provincia']).')';
	$res='<w:p w14:paraId="76C45544" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:lastRenderedPageBreak/><w:t>RESPUESTA A DERECHO DE OPOSICIÓN</w:t></w:r><w:r w:rsidR="007D5616" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t xml:space="preserve"> A DECISIÓN INDIVIDUAL AUTOMATIZADA</w:t></w:r></w:p><w:p w14:paraId="6507FAAD" w14:textId="77777777" w:rsidR="00ED682A" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00ED682A"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>SUPUESTO Nº 5</w:t></w:r><w:r w:rsidR="00B21525" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>.2</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">: DENEGACIÓN DE OPOSICIÓN </w:t></w:r><w:r w:rsidR="00B14B23" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">A DECISIÓN INDIVIDUAL AUTOMATIZADA </w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr><w:t>POR LEY</w:t></w:r></w:p><w:p w14:paraId="7E28AB1E" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00ED682A"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr><w:t>RESPUESTA A LA PETICIÓN DE OPOSICIÓN REF. '.$ref.'</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="793989CB" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la petición de oposición </w:t></w:r><w:r w:rsidR="009535F0" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">a decisión individual automatizada </w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>de fecha '.formateaFechaWeb($item['fechaRecepcion']).' realizada por D./ª '.$item['representante'].', en nombre y representación de D./ª '.$item['interesado'].', con domicilio a efectos de notificaciones en '.$item['direccion'].'.</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="2F68202A" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="48A6C8FE" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].' en calidad de responsable de  ficheros de datos de carácter personal, le informa que:</w:t></w:r></w:p>';
	$res.='<w:p w14:paraId="41799B07" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p><w:p w14:paraId="713D44D1" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="009535F0"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">Se le deniega la solicitud de oposición referenciada, en virtud de </w:t></w:r><w:r w:rsidR="009535F0" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>lo establecido en el artículo 22</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">  del Reglamento General de Protección de Datos</w:t></w:r><w:r w:rsidR="009535F0" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>.</w:t></w:r></w:p><w:p w14:paraId="5685E7A2" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr></w:p><w:p w14:paraId="6A14064B" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>El artículo 22 del Reglamento General de Protección de Datos</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> establece lo siguiente:</w:t></w:r></w:p><w:p w14:paraId="1D426049" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>“1. Todo interesado tendrá derecho a no ser objeto de una decisión basada únicamente en el tratamiento automatizado, incluida la elaboración de perfiles, que produzca efectos jurídicos en él o le afecte significativamente de modo similar.</w:t></w:r></w:p><w:p w14:paraId="16749FB9" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>2. El apartado 1 no se aplicará si la decisión:</w:t></w:r></w:p><w:p w14:paraId="13202F76" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>a) es necesaria para la celebración o la ejecución de un contrato entre el interesado y un responsable del tratamiento;</w:t></w:r></w:p><w:p w14:paraId="54C97EA2" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>b) está autorizada por el Derecho de la Unión o de los Estados miembros que se aplique al responsable del tratamiento y que establezca asimismo medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado, o</w:t></w:r></w:p><w:p w14:paraId="510BF29A" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>c) se basa en el consentimiento explícito del interesado.</w:t></w:r></w:p><w:p w14:paraId="5266F169" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t>3. En los casos a que se refiere el apartado 2, letras a) y c), el responsable del tratamiento adoptará las medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado, como mínimo el derecho a obtener intervención humana por parte del responsable, a expresar su punto de vista y a impugnar la decisión.</w:t></w:r></w:p><w:p w14:paraId="52481F76" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:i/></w:rPr><w:t xml:space="preserve">4. Las decisiones a que se refiere el apartado 2 no se basarán en las categorías especiales de datos personales contempladas en el artículo 9, apartado 1, salvo que se aplique el artículo 9, apartado 2, letra a) o g), y se hayan tomado medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado”.  </w:t></w:r></w:p><w:p w14:paraId="33EBD54C" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p>';
	$res.='<w:p w14:paraId="79629FDE" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a '.$datos['razonSocial'].', con domicilio  a efectos de notificaciones en '.$direccion.', así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.</w:t></w:r></w:p><w:p w14:paraId="579C10FE" w14:textId="77777777" w:rsidR="00AA6E21" w:rsidRPr="00707C54" w:rsidRDefault="00AA6E21" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="1944B82B" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>A '.formateaFechaWeb($item['fechaRespuesta']).'.</w:t></w:r></w:p><w:p w14:paraId="7C59FF91" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>Atentamente,</w:t></w:r></w:p><w:p w14:paraId="23AE557D" w14:textId="77777777" w:rsidR="00C013B3" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="00C013B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr></w:p><w:p w14:paraId="74A793F3" w14:textId="44921C09" w:rsidR="001F4C7B" w:rsidRPr="00707C54" w:rsidRDefault="00C013B3" w:rsidP="003C1ED3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Fdo. '.$datos['representante'].'</w:t></w:r><w:r w:rsidR="00AA6E21" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> (</w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t xml:space="preserve">Representante legal de </w:t></w:r><w:r w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>'.$datos['razonSocial'].'</w:t></w:r><w:r w:rsidR="00AA6E21" w:rsidRPr="00707C54"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>)</w:t></w:r></w:p>';
	return $res;
}
?>
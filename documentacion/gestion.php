<?php
  $seccionActiva=2;
  include_once("../cabecera.php");
  gestionDocumentosFirmados();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectpicker').selectpicker();
    $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});

    $( ".eliminaDocumentacion" ).click(function() {
            var id=$(this).attr('id');
            var fichero = $('input#'+id).val();
            $(".control-group."+id+" div").remove();
            $("#"+id).remove();
            $("."+id).append("<div class='controls'><input type='file' name='"+id+"' id='"+id+"' /><div class='tip'>Solo se admiten ficheros de 5 MB como máximo</div></div>");
            $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
            eliminaFichero(fichero);
        });
  });

function eliminaFichero(fichero){
  $.post('../listadoAjax.php?include=documentacion&funcion=eliminaFichero();',{'fichero':fichero});
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
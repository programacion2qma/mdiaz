<?php
  include_once("funciones.php");
  
  //echo session_id().'<br />';

  compruebaSesion();
  if(!isset($_GET['ajax'])){
?>
  <!DOCTYPE html>
  <html lang="es">
  <head>
  <meta charset="utf-8">
  <title><?php echo $_CONFIG['titulosSecciones'][$seccionActiva] ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=0.65, maximum-scale=1.0, user-scalable=1">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <!-- Anti-caché -->
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <!-- Fin anti-caché-->

  <link href="<?php echo $_CONFIG['raiz']; ?>css/bootstrap.min.php" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>css/style.php" rel="stylesheet /">
  <link href="<?php echo $_CONFIG['raiz']; ?>css/paneles.php" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/datepicker.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-select.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-wysihtml5.css" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/animate.css" rel="stylesheet" type="text/css" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/js/guidely/guidely.css" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../api/css/bootstrap-colorpicker.css" rel="stylesheet" />

  <link href="<?php echo $_CONFIG['raiz']; ?>css/jquery.orgchart.php" rel="stylesheet" />

   <link href="<?php echo $_CONFIG['raiz']; ?>css/materialize.php" type="text/css" rel="stylesheet" media="screen,projection"/>

  <!-- Script HTML5, para el soporte del mismo en IE6-8 -->
  <!--[if lt IE 9]>
        <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->


  <!-- Común -->
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery-1.7.2.min.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/controladorAJAX.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>js/materialize.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.fix.clone.js" type="text/javascript"></script>
  <!-- Fin común -->


  </head>
  <body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">

        <span class="logo2">
          <img src="<?php echo $_CONFIG['raiz']; ?>img/logo2.png" alt="<?php echo $_CONFIG['altLogo']; ?>"> &nbsp; <span><?php echo $_CONFIG['tituloSoftware']; ?></span>
        </span>

        <div class="nav-collapse">
          <ul class="nav pull-right cajaUsuario">
            <li class="dropdown"  id="target-5"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user"></i> <?php echo ucfirst($_SESSION['usuario']).perfilUsuario(); ?> <b class="caret"></b></a>
              <ul class="dropdown-menu menu">
                <li><a href="<?php echo $_CONFIG['raiz']; ?>inicio.php"><i class="icon-home"></i> Inicio</a></li>
                <li class="divider"></li>
                <li><a href="<?php echo $_CONFIG['raiz']; ?>cerrarSesion.php" class='noAjax'><i class="icon-power-off"></i> Cerrar sesión</a></li>
              </ul>
            </li>
          </ul>

        </div>

        <div class="nav-collapse">
          <?php cajaFiltroEjercicio(); ?>
        </div>

        <?php cajaHistoricoSQL(); ?>
        <!--/.nav-collapse -->
      </div>
      <!-- /container -->
    </div>
    <!-- /navbar-inner -->
  </div>
  <!-- /navbar -->
  <div class="subnavbar">
    <div class="subnavbar-inner">
      <div class="container">
        <ul class="mainnav menu" id="target-4">
          <?php
            //creaOpcionMenu(false,'Ventas de cursos','inicios-grupos/','icon-pencil',$seccionActiva,array('COMERCIAL','TELEMARKETING','DELEGADO','DIRECTOR'));
            creaOpcionMenuMDiaz(3,'Trabajadores clientes','trabajadores/','icon-industry',$seccionActiva,array('COMERCIAL','DELEGADO','DIRECTOR'));
            creaOpcionDesplegableMenuMDiaz(array(1,2,3,4,5,20,51),'Administración','icon-file-text-o',array('Agrupaciones','Trabajadores','Inicios de grupos','Complementos','Convenios','Colaboradores'),array('agrupaciones/','trabajadores/','inicios-grupos/','complementos/','convenios/','colaboradores/'),array('icon-university','icon-industry','icon-calendar','icon-tablet','icon-handshake-o','icon-share-alt'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','CONTABILIDAD'));

            creaOpcionDesplegableMenuMDiaz(array(15,16,17,18,19,21,22,23,36,37),'Comercial','icon-shopping-cart',array('Clientes potenciales','Clientes','Acción comercial','Preventas','Ventas'),array('posibles-clientes/','clientes/','accion-comercial/','preventas/','ventas/'),array('icon-exclamation-circle','icon-check-circle','icon-tag','icon-handshake-o','icon-tag'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','CONTABILIDAD','COMERCIAL'));

            creaOpcionDesplegableMenuMDiaz(array(15,16),'Comercial','icon-shopping-cart',array('Clientes potenciales','Clientes','Agentes'),array('posibles-clientes/','clientes/','comerciales/',),array('icon-exclamation-circle','icon-check-circle','icon-group'),$seccionActiva,array('CONSULTOR'));


            creaOpcionDesplegableMenuMDiaz(array(6,7,8,9),'Configuración','icon-cogs',array('Familias de servicios','Servicios','Acciones Formativas','Centros de Formación','Tutores'),array('familias-servicios/','servicios/','acciones-formativas/','centros-formacion/','tutores/'),array('icon-cog','icon-cogs','icon-graduation-cap','icon-building-o','icon-briefcase'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));

            creaOpcionDesplegableMenuMDiaz(array(10,11,55,12),'Consultorías','icon-comments-o',array('Gestión','Seguimiento','Documentación','Configuración'),array('consultoria/','seguimiento-consultoria/','documentacion/seleccionaCliente.php','configuracion-consultoria/'),array('icon-list','icon-search','icon-files-o','icon-cogs'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','COMERCIAL','CONSULTOR'));

            creaOpcionDesplegableMenuMDiaz(array(62,63,64,65,66,67,68,69,55),'Consultorías','icon-comments-o',array('Usuarios','Soportes','Usuario <> Soportes','Aplicaciones','Cesiones','Delegaciones','Concurrencias','Encargados de tratamiento','Documentación'),array('zona-cliente-usuarios/','zona-cliente-soportes/','zona-cliente-usuarios-soportes/','zona-cliente-aplicaciones/','zona-cliente-cesiones/','zona-cliente-delegaciones/','zona-cliente-concurrencias/','zona-cliente-encargados/','documentacion/'),array('icon-users','icon-laptop','icon-address-book','icon-code','icon-sitemap','icon-share','icon-handshake-o','icon-archive','icon-files-o'),$seccionActiva,array('CLIENTE'));

            /*creaOpcionDesplegableMenuMDiaz(array(63,65,68,55),'Consultorías','icon-comments-o',array('Soportes','Aplicaciones','Concurrencias','Documentación'),array('zona-cliente-soportes/','zona-cliente-aplicaciones/','zona-cliente-concurrencias/','documentacion/'),array('icon-laptop','icon-code','icon-handshake-o','icon-files-o'),$seccionActiva,array('CLIENTE'));*/
            
            $aparecen=true;
            $aparecen2=true;
            if($_SESSION['tipoUsuario']=='CLIENTE'){
              $aparecen=false;
              $aparecen2=false;
              $cliente=obtenerCodigoCliente(true);
              $servicios=consultaBD('SELECT servicios_familias.*, servicios.servicio FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE codigoCliente='.$cliente,true);
              while($item=mysql_fetch_assoc($servicios)){
                if($item['referencia']=='PBC1'){
                  $aparecen=true;
                  if(strpos($item['servicio'], 'LOPD')!==false){
                    $aparecen2=true;
                  }
                } else if($item['referencia']=='LOPD1'){
                  $aparecen2=true;
                }
              }
            }
            
            if($aparecen){
              creaOpcionMenuMDiaz(53,'Análisis previos de riesgo','analisis-previo/','icon-exclamation-triangle',$seccionActiva,array('CLIENTE'));
              creaOpcionMenuMDiaz(50,'Informes de clientes','informes-clientes/','icon-file-text-o',$seccionActiva,array('ADMIN','CLIENTE'));
            }
    
            
            creaOpcionMenuMDiaz(24,'Agenda','agenda/','icon-calendar',$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','COMERCIAL','DELEGADO','DIRECTOR','TELEMARKETING'));
            creaOpcionMenuMDiaz(34,'Importar clientes potenciales','importar-clientes-potenciales/','icon-cloud-upload',$seccionActiva,array('TELEMARKETING'));

            creaOpcionDesplegableMenuMDiaz(25,'Incidencias','icon-exclamation-triangle',array('Incidencias','Incidencias LOPD','Incidencias de software'),array('incidencias/','incidencias-lopd/','incidencias-software/'),array('icon-exclamation-triangle','icon-lock','icon-laptop'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','COMERCIAL','CONTABILIDAD'));

            if($aparecen2){
              creaOpcionMenuMDiaz(25,'Incidencias LOPD','incidencias-lopd/','icon-lock',$seccionActiva,array('CLIENTE'));
            }
            
            creaOpcionMenuMDiaz(35,'Operadores','usuarios/','icon-lock',$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','CONSULTOR'));

            creaOpcionDesplegableMenuMDiaz(44,'Informes','icon-paste',array('Seguimiento colaboradores','Agenda','Actividad telemarketing/colaborador','Incidencias clientes potenciales'),array('informe-seguimiento-colaborador/','informe-agenda/','informe-actividad-telemarketing-colaborador/','informe-incidencias-posibles-clientes/'),array('icon-share-alt','icon-eur','icon-exclamation-circle','icon-calendar','icon-headphones','icon-exclamation-triangle'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR'));

            creaOpcionDesplegableMenuMDiaz(54,'Otros Documentos','icon-list',array('Entradas y Salidas Periódicas','Entradas y Salidas No Periódicas','Soportes','Soportes Cancelados'),array('entradas-periodicas/','entradas-no-periodicas/','soportes/','soportes-cancelados/'),array('icon-clock-o','icon-bell-slash','icon-laptop','icon-laptop'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','COMERCIAL','CONTABILIDAD'));

            creaOpcionDesplegableMenuMDiaz(60,'Evaluación de Impacto','icon-exclamation-triangle',array('Evaluación Reducida','Evaluación','Gestiones','Informes'),array('evaluacion-reducida/','evaluacion-de-riesgos/','gestion-de-riesgos/','generacion-de-informes/'),array('icon-dashboard','icon-dashboard','icon-gears','icon-paste'),$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','TUTOR','COMERCIAL','CONTABILIDAD','CLIENTE'));

            creaOpcionMenuMDiaz(61,'Auditorías Internas','auditorias-internas/','icon-edit',$seccionActiva,array('ADMIN','ADMINISTRACION1','ADMINISTRACION2','CONSULTOR'));

            if($_SESSION['tipoUsuario']=='CLIENTE'){
              $codigoCliente=obtenerCodigoCliente(true);
              $venta=consultaBD('SELECT ventas_servicios.codigo FROM ventas_servicios INNER JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio INNER JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo WHERE servicios.codigoFamilia=2 AND codigoCliente='.$codigoCliente,true,true);
              if($venta){
                creaOpcionMenuMDiaz(54,'Formación','formacion/','icon-book',$seccionActiva,array('CLIENTE'));
              }
            }
          ?>
        </ul>
      </div>
      <!-- /container -->
    </div>
    <!-- /subnavbar-inner -->
  </div>
  <!-- /subnavbar -->
<?php
}

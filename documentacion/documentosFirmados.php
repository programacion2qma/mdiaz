<?php
  $seccionActiva=2;
  include_once('../cabecera.php');
  operacionesDocumentosFirmados();
  if(isset($_GET['documento'])){
    $_SESSION['documentoFirmado']=$_GET['documento'];
    $_SESSION['codigoTrabajo']=$_GET['codigo'];
  }
  $documentoFirmado=$_SESSION['documentoFirmado'];
  $codigoTrabajo=$_SESSION['codigoTrabajo'];
  $estadisticas=estadisticasDocumentosFirmados($codigoTrabajo,$documentoFirmado);
  $trabajo=datosRegistro('trabajos',$codigoTrabajo,'codigo');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de Documentación:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-file-text"></i> <span class="value"><?php echo $estadisticas; ?></span><br />Documentos firmados</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Formularios</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="<?php echo $_CONFIG['raiz']; ?>documentacion/index.php?cliente=<?php echo $trabajo['codigoCliente'];?>" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
                <?php 
                if($_SESSION['tipoUsuario']=='CLIENTE'){?>
				        <a href="<?php echo $_CONFIG['raiz']; ?>documentacion/gestion.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nuevo documento firmado</span> </a>
                <?php } ?>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3><?php echo $documentoFirmado;?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Fecha de subida </th>
                  <th> Nombre </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeDocumentosFirmados($codigoTrabajo,$documentoFirmado);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.descargaFichero').unbind();
    $('.descargaFichero').click(function(e){
      e.preventDefault();
      var codigo=$(this).attr('icono');
      var icono='#icono'+codigo;
      $(icono).remove();
      var url = $(this).attr('href');
      var target = $(this).attr('target');
      if(target=='_blank'){
        window.open(url, target)
      } else {
        window.location.href = url;
      }
      $.post('../listadoAjax.php?include=documentacion&funcion=cambiaVisto();',{'codigo':codigo});
    });
});
</script>
<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
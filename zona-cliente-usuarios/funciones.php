<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesUsuarios(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaUsuario();
	}
	elseif(isset($_POST['nombreUsuarioLOPD'])){
		$res=insertaUsuario();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		//$res=eliminaDatos('agrupaciones');
	}

	mensajeResultado('nombreUsuarioLOPD',$res,'Usuario');
    mensajeResultado('elimina',$res,'Usuario', true);
}

function insertaUsuario(){
	$res=true;
	prepararCampos();
	$res=insertaDatos('usuarios_lopd');
	return $res;
}

function actualizaUsuario(){
	$res=true;
	prepararCampos();
	$res=actualizaDatos('usuarios_lopd');
	return $res;
}

function prepararCampos(){
	$datos=arrayFormulario();
	$select = $datos['ficherosUsuarioLOPD'];
	$text ='';
	if(!empty($select)){
		for($j=0;$j<count($select);$j++){
			if($j > 0){
				$text .= "&$&";
			}
			$text .= $select[$j];
		}
	}
	$_POST['ficherosUsuarioLOPD'] = $text;

	$select = $datos['soportesUsuarioLOPD'];
	$text ='';
	if(!empty($select)){
		for($j=0;$j<count($select);$j++){
			if($j > 0){
				$text .= "&$&";
			}
			$text .= $select[$j];
		}
	}
	$_POST['soportesUsuarioLOPD'] = $text;

}


function listadoUsuarios(){
	global $_CONFIG;

	$columnas=array('nombreUsuarioLOPD','nifUsuarioLOPD','fechasUsuarioLOPD','puestoUsuarioLOPD','ficherosUsuarioLOPD','servicio','usuarios_lopd.codigo','usuarios_lopd.codigo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	conexionBD();
	$consulta=consultaBD("SELECT usuarios_lopd.codigo, usuarios_lopd.nombreUsuarioLOPD, nifUsuarioLOPD, CONCAT(DATE_FORMAT(fechaAltaUsuarioLOPD,'%d/%m/%Y'),IF(fechaBajaUsuarioLOPD!='0000-00-00',CONCAT('/',DATE_FORMAT(fechaBajaUsuarioLOPD,'%d/%m/%Y')),'')) AS fechasUsuarioLOPD, puestoUsuarioLOPD, ficherosUsuarioLOPD, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM usuarios_lopd INNER JOIN trabajos ON usuarios_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT usuarios_lopd.codigo, usuarios_lopd.nombreUsuarioLOPD, nifUsuarioLOPD, CONCAT(DATE_FORMAT(fechaAltaUsuarioLOPD,'%d/%m/%Y'),IF(fechaBajaUsuarioLOPD!='0000-00-00',CONCAT('/',DATE_FORMAT(fechaBajaUsuarioLOPD,'%d/%m/%Y')),'')) AS fechasUsuarioLOPD, puestoUsuarioLOPD, ficherosUsuarioLOPD, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM usuarios_lopd INNER JOIN trabajos ON usuarios_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fechas=formateaFechaWeb($datos['fechaAltaUsuarioLOPD']);
		if($datos['fechaBajaUsuarioLOPD']!='0000-00-00'){
			$fechas.=' / '.formateaFechaWeb($datos['fechaBajaUsuarioLOPD']);;
		}
		$ficheros=obtieneFicheros($datos['ficherosUsuarioLOPD']);
		$fila=array(
			$datos['nombreUsuarioLOPD'],
			$datos['nifUsuarioLOPD'],
			$datos['fechasUsuarioLOPD'],
			$datos['puestoUsuarioLOPD'],
			$ficheros,
			formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'],
			creaBotonDetalles("zona-cliente-usuarios/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function obtieneFicheros($ficheros){
	$res='';
	$ficheros=explode('&$&', $ficheros);
	foreach ($ficheros as $key => $value) {
		$item=datosRegistro('declaraciones',$value);
		if($res!=''){
			$res.=',<br/>';
		}
		$res.=$item['nombreFichero'];
	}

	return $res;
}


function gestionUsuarios(){
	operacionesUsuarios();

	abreVentanaGestion('Gestión de Usuarios que acceden al fichero','?','','icon-edit','margenAb');
	$datos=compruebaDatos('usuarios_lopd');

	$consulta=consultaBD("SELECT codigo, nombreFichero FROM declaraciones WHERE codigoCliente='".obtenerCodigoCliente(true)."';",true);
	$opciones=array();
	$valores=array();
    while($datosFicheros=mysql_fetch_assoc($consulta)){
        array_push($opciones, $datosFicheros['nombreFichero']);
        array_push($valores, $datosFicheros['codigo']);        
    }

    if(!$datos){
    	abreColumnaCampos();
    		campoSelectConsulta('codigoTrabajo','Consultoría','SELECT trabajos.codigo, servicios.servicio AS texto FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE codigoCliente='.obtenerCodigoCliente(true),false,'selectpicker span5');
    	cierraColumnaCampos(true);
    }

	abreColumnaCampos();
		campoTexto('nombreUsuarioLOPD','Nombre y apellidos',$datos,'span5');
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoTexto('nifUsuarioLOPD','NIF',$datos,'input-small');
		campoFecha('fechaAltaUsuarioLOPD','Fecha alta',$datos);
		campoSelect('datosUsuarioLOPD','¿Pueden sacar datos?',array('Si','No'),array('SI','NO'),$datos,'selectpicker span1');
		campoSelectMultiple('soportesUsuarioLOPD','¿En que tipo de soportes?',array('Automatizado','Manual'),array('AUTO','MANUAL'),$datos,'selectpicker span2 show-tick','');
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto('puestoUsuarioLOPD','Puesto',$datos);
		campoFecha('fechaBajaUsuarioLOPD','Fecha baja',$datos);
		camposelectMultiple('ficherosUsuarioLOPD','Ficheros a los que acceden',$opciones,$valores,$datos,'selectpicker span3 show-tick multipleFicheros');
		campoSelect('accesoUsuarioLOPD','Accesos',array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),$datos,'selectpicker span2 show-tick','');
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
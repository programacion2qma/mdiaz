<?php
  $seccionActiva=53;
  include_once("../cabecera.php");
  gestionInformes();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

  marcarPestania();
	$('.btnSiguiente').click(function(){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
      asignaPagina();
  });

	$('.btnAnterior').click(function(){
      $('.nav-tabs > .active').after('li').find('a').trigger('click');
      asignaPagina();
  });

  $(".nav-tabs li a").click(function() {
        var pagina=$(this).attr('href');
        pagina = pagina.replace('#','');
        $("#pagina").val(pagina);
  });

  mostrarAnalisis();
  $("#empleados").change(function(){
    mostrarAnalisis();
  });
  $("#volumen").change(function(){
    mostrarAnalisis();
  });



});

function asignaPagina(){
  var numero=$('.nav-tabs .active a').attr('href');
  var pagina=$('.nav-tabs .active a').attr('href');
  pagina = pagina.replace('#','');
  $("#pagina").val(pagina);
}

function marcarPestania(){
  var pagina=$("#pagina").val();
  $('.nav-tabs li').removeClass('active');
  $('.nav-tabs li:nth-child('+pagina+')').addClass('active');
}

function mostrarAnalisis(){
  var empleados=$('#empleados').val();
  var volumen=$('#volumen').val();
  if(empleados==2 || volumen==2){
    $('#divNO').removeClass('hide');
    $('#divSI').addClass('hide');
  } else {
    $('#divSI').removeClass('hide');
    $('#divNO').addClass('hide');
  }
}
</script>
</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
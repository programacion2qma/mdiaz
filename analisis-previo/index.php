<?php
  $seccionActiva=53;
  include_once('../cabecera.php');
  $_SESSION['espacio']='NO';
  operacionesInformes();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
          $codigo=obtenerCodigoCliente(true);
          creaBotonesGestion($codigo,'codigoCliente');
        ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Análisis previos de riesgos registrados</h3>
              <div class="pull-right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                //filtroServicios();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaServicios">
                <thead>
                  <tr>
                    <th> Empleados </th>
                    <th> Volumen </th>
                    <th> Fecha </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaServicios','../listadoAjax.php?include=analisis-previo&funcion=listadoTrabajos();');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
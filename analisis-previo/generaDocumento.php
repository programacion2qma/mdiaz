<?php
@include_once('funciones.php');
compruebaSesion();

$contenido=generaInforme();
$nombre='analisis_previo_de_riesgo.pdf';

require_once('../../api/html2pdf/html2pdf.class.php');
$html2pdf=new HTML2PDF('P','A4','es');
$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->WriteHTML($contenido);
$html2pdf->Output($nombre);


function generaInforme(){


$datos = datosRegistro("analisis_previo",$_GET['codigo']);
$cliente= datosRegistro("clientes",$datos['codigoCliente']);

$personas=array(1=>'Menos de 10 personas',2=>'10 personas o más');
$volumen=array(1=>'No supera los 2 millones de euros',2=>'Supera los 2 millones de euros');
$logo='<br/>EL CLIENTE NO TIENE LOGO SUBIDO EN LA HERRAMIENTA';
if($cliente['ficheroLogo']!='NO' && $cliente['ficheroLogo']!=''){ 
	$logo="<img src='../documentos/logos-clientes/".$cliente['ficheroLogo']."' />"; 
}
$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        .principal{
	        	font-weight:bold;
	        	text-align:center;
	        	font-size:18px;
	        	margin-top:30px;
	        	margin-bottom:10px;
	        } 

	        .titulo{
	        	font-weight:bold;
	        	display:block;
	        	margin-bottom:0px;
	        	margin-top:5px;
	        }

	        .subtitulo{
	        	font-weight:bold;
	        	display:block;
	        	padding-left:10px;
	        	margin-bottom:0px
	        }

	        ul{
	        	list-style:none;
	        	margin-left:-55px;
	        	margin-bottom:5px;
	        }

	       	.check li{
	       		background:url(../img/cuadradoBlanco.png) left 2px no-repeat;
	       		padding-left:18px;
	       		padding-top:3px;
	       		padding-bottom:3px;
	        }

	        .check .marcado{
	       		background:url(../img/cuadradoBlancoCheck.png) -5px -2px no-repeat;
	        }

	        .check .marcado1{
	       		background:url(../img/cuadradoBlanco1.png) left 2px no-repeat;
	        }

	        .check .marcado2{
	       		background:url(../img/cuadradoBlanco2.png) left 2px no-repeat;
	        }

	        .check .marcado3{
	       		background:url(../img/cuadradoBlanco3.png) left 2px no-repeat;
	        }
	        
	        .cabecera{

	        }


	        .cabecera .interna{
	        	width:80%;
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        	margin-left:76px;
	        }

	        .cabecera .interna td{
	        	text-align:center;
	        	border:1px solid #000;
	        }

	        .cabecera .interna td img{
	        	height:80px;
	        }

	        .cabecera .interna .tdUno,
	        .cabecera .interna .tdTres{
	        	width:30%;
	        }

	        .cabecera .interna .tdDos{
	        	width:40%;
	        	padding-top:10px;
	        }


	-->
	</style>
	<page footer='page'>
	<div class='cabecera'>
		<table class='interna'>
			<tr>
				<td class='tdUno'><img src='../img/logo2.png' /></td>
				<td class='tdDos'>ANÁLISIS PREVIO DE RIESGOS EN<br/>MATERIA DE PREVENCIÓN DE<br/>BLANQUEO DE CAPITALES Y<br/> FINANCIACIÓN DEL TERRORISMO.</td>
				<td class='tdTres'>".$logo."</td>
			</tr>
		</table>
	</div>
	<h1 class='principal'>SUJETO OBLIGADO: ".$cliente['razonSocial']."</h1>
	Es un documento en el que los sujetos obligados deben describir y evaluar la exposición de su actividad profesional al riesgo de blanqueo de capitales. Para ello, se examinarán determinados elementos. El análisis se cumplimentará, imprimirá y firmará por el responsable de elaborarlo.<br/><br/>

	Los procedimientos de control interno se fundamentarán en un previo análisis de riesgo que será documentado por el sujeto obligado. El análisis identificará y evaluará los riesgos del sujeto obligado por tipos de clientes, países o áreas geográficas, productos, servicios, operaciones y canales de distribución, tomando en consideración variables tales como el propósito de la relación de negocios, el nivel de activos del cliente, el volumen de las operaciones y la regularidad o duración de la relación de negocios. El análisis de riesgo será revisado periódicamente.<br/>
 	Una vez cumplimentado, debe imprimirse y firmarse como acreditación de su aprobación por el sujeto obligado.<br/><br/>
 
	Los sujetos obligados comprendidos en el artículo 2.1 i) a u), ambos inclusive, que, con inclusión de los agentes, ocupen a Menos de 10 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 2 millones de euros, quedan exceptuados de la obligación de documentar un previo análisis de riesgo del propio sujeto obligado en el que se fundamentarán los procedimientos de control interno.<br/><br/>

	- Número de empleados del sujetado obligado -con inclusión de los agentes-: <b>".$personas[$datos['empleados']]."</b><br/>
	- Volumen de negocios anual o balance general anual: <b>".$volumen[$datos['volumen']]."</b><hr/>";
	
	if($datos['empleados']==1 && $datos['volumen']==1){
		$contenido.='El sujeto obligado '.$cliente['razonSocial'].' queda exceptuado de elaborar y documentar un previo análisis de riesgo en los términos del artículo 32 del RLPBC';
	} else {
		$contenido.='Es un documento en el que los sujetos obligados deben describir y evaluar la exposición de su actividad profesional al riesgo de blanqueo de capitales. Para ello,  se examinarán determinados elementos. El análisis se cumplimentará, imprimirá y firmará por el responsable de elaborarlo.<br/><br/>
		<p class="titulo">a)	A fin de valorar el riesgo de los siguientes elementos, se establece una Puntuación de 1 a 3, correspondiendo la mayor Puntuación al mayor riesgo. Marcar las casillas que correspondan a la realidad de la actividad que lleva a cabo el sujeto obligado:</p>
		<p class="titulo">1.	CARACTERÍSTICAS DEL SUJETO OBLIGADO:</p>
			<p class="subtitulo">1.1 UBICACIÓN: Zona geográfica donde desarrolla su actividad profesional:</p>
			<ul class="check">
			<li'.marcarOpcion($datos,'ubicacion',1).'>Zona del interior -Puntuación 1- </li>
			<li'.marcarOpcion($datos,'ubicacion',2).'>Zona del litoral e islas -Puntuación 2-</li>
			<li'.marcarOpcion($datos,'ubicacion',3).'>Municipios de más de 750.000 habitantes -Puntuación 3-</li>
			</ul>

			<p class="subtitulo">1.2 ACTIVIDAD: Frecuencia con que presta servicios de compraventa de inmuebles, compraventa o constitución de sociedades, apertura o gestión de cuentas corrientes o de ahorro de clientes, organización de aportaciones sociales, gestión de sociedades, ejercicio de cargos de administración, asesoramiento fiscal.</p>
			<ul class="check">
			<li'.marcarOpcion($datos,'actividad',1).'>Estas actividades son ejercidas de forma esporádica -Puntuación  1-</li>
			<li'.marcarOpcion($datos,'actividad',2).'>Estas actividades son ejercidas de forma habitual, pero no exclusivamente -Puntuación 2-</li>
			<li'.marcarOpcion($datos,'actividad',3).'>Son la base del despacho -Puntuación 3-</li>
			</ul>

			<p class="subtitulo">1.3 TAMAÑO DEL DESPACHO: Volumen de negocio de las actividades mencionadas en el apartado anterior, entendiendo por volumen de negocio el importe de la facturación:</p>
			<ul class="check">
			<li'.marcarOpcion($datos,'tamanio',1).'>Menos de 50.000 euros al año -Puntuación  1-</li>
			<li'.marcarOpcion($datos,'tamanio',2).'>Más de 50.000 euros al año -Puntuación 2-</li>
			<li'.marcarOpcion($datos,'tamanio',3).'>Más de 100.000 euros al año -Puntuación 3-</li>
			</ul>

			<p class="subtitulo">1.4 SISTEMA DE INGRESO, MOVIMIENTO Y TRANSMISIÓN DE FONDOS A TRAVÉS DE CUENTAS DE CLIENTES:</p>
			<ul class="check">
			<li'.marcarOpcion($datos,'sistema1',1).'>Únicamente provisiones de fonds para pago de impuestos y suplidos -Puntuación  1-</li>
			<li'.marcarOpcion($datos,'sistema1',2).'>Recepción de depósito por cuenta del cliente -Puntuación 2-</li>
			<li'.marcarOpcion($datos,'sistema1',3).'>Recepción de depósito RECEPCIÓN DE FONDOS DEL CLIENTE PARA REALIZAR LA INVERSIÓN Y/O DESINVERSIÓN -Puntuación 3-</li>
			</ul>
			<page_footer>
			<br/>Realizado por: '.$datos['analista'].'<br/>
			en '.$datos['lugar'].' el '.formateaFechaWeb($datos['fecha']).'
		</page_footer>
		</page>
		<page footer="page">
		<p class="titulo">2.	TIPOLOGÍAS DE CLIENTES: SON HABITUALES:</p>
			<ul class="check">
			<li'.marcarOpcion($datos,'clientes',1).'>Clientes españoles -Puntuación  1-</li>
			<li'.marcarOpcion($datos,'clientes',2).'>Clientes residentes -Puntuación 2-</li>
			<li'.marcarOpcion($datos,'clientes',3).'>Clientes residentes de país comunitario de paíz extracomunitario pero equivalente -Puntuación 3-</li>
			<li'.marcarOpcion($datos,'clientes',4).'>Clientes no residente de país no equivalente, paraíso fiscal, con alto nivel de corrupción o sujetos a saciones internacionales -Puntuación 3 con un plus de riesgos-</li>
			<li'.marcarOpcion($datos,'clientes',31).'>Personas con responsabilidad pública -Puntuación 3-</li>
			<li'.marcarOpcion($datos,'clientes',32).'>Clientes con estructura de propiedas compleja -Puntuación 3-</li>
			</ul>
		<p class="titulo">b)	A fin de valorar los siguientes elementos, se establece una Puntuación de 1 a 3, correspondiendo: Puntuación 1 a un cumplimiento satisfactorio sin necesidad de medidas adicionales significativas, la Puntuación 2 a un grado de avance sustantivo en el proceso de implantación de medidas tomadas y la Puntuación 3 a una necesidad de implantación de mejoras relevantes. Puntuar en función del criterio establecido:</p>
		<p class="titulo">1. INVOLUCRACIÓN ALTA DIRECCIÓN: información proporcionada y frecuencia.</p>
			<ul class="check">
			<li'.marcarOpcion2($datos,'informacion').'>Información y/o documentación a la alta dirección de la entidad sobre temas relacionados con la PBC/FT. Frecuencia con la que la alta dirección es informada de estos temas</li>
			<li'.marcarOpcion2($datos,'asistencia').'>Asistencia de la Alta Dirección de la entidad a cursos formativos sobre PBC/FT </li>
			<li'.marcarOpcion2($datos,'decisiones').'>Decisiones tomadas por la Alta Dirección </li>
			</ul>
		<p class="titulo">2. COMPOSICIÓN DEL  OCI: REPRESENTACIÓN Y FUNCIONALIDAD.</p>
			<ul class="check">
			<li'.marcarOpcion2($datos,'areas').'>Áreas representadas en el OCI</li>
			<li'.marcarOpcion2($datos,'frecuencia').'>Frecuencia de las reuniones </li>
			<li'.marcarOpcion2($datos,'dinamica').'>Dinámica de las reuniones, asistencia de personal técnico, información que se lleva, agilidad en la toma de decisiones</li>  
			<li'.marcarOpcion2($datos,'delegacion').'>Delegación de funciones en la unidad o en algún otro subcomité </li>
			</ul>
		<p class="titulo">3. DILIGENCIA DEBIDA.</p>
			<ul class="check">
			<li'.marcarOpcion2($datos,'politica').'>Política de aceptación de clientes: controles adecuados para detectar personas o entidades cuya admisión no está permitida</li>
			<li'.marcarOpcion2($datos,'proceso1').'>Proceso de consulta de listas internacionales e internas </li>
			<li'.marcarOpcion2($datos,'funcionamiento1').'>Funcionamiento de la herramienta informática que asigna el nivel de riesgo del cliente en función de los datos obtenidos en el momento de establecer relaciones de negocio con el mismo </li>  
			<li'.marcarOpcion2($datos,'medidas').'>Medidas adicionales (autorizaciones, documentación adicional requerida, etc.) progresivas en función del riesgo asignado al cliente </li>
			<li'.marcarOpcion2($datos,'controles').'>Controles automáticos de identificación generales </li>
			<li'.marcarOpcion2($datos,'procedimientos1').'>Procedimientos para asegurar que en todas las operaciones se identifica correctamente al titular real de misma</li>  
			<li'.marcarOpcion2($datos,'conocimiento').'>Conocimiento: origen de los fondos </li>
			<li'.marcarOpcion2($datos,'contenido').'>Contenido de los expedientes de conozca a su cliente (informes de clientes) </li>
			<li'.marcarOpcion2($datos,'procedimientos2').'>Procedimientos de verificación del origen de los fondos</li>
			<li'.marcarOpcion2($datos,'conservacion').'>Conservación de documentos</li>  
			<li'.marcarOpcion2($datos,'sistema2').'>Sistema de digitalización de documentos </li>
			</ul>
		<p class="titulo">4. DETECCIÓN, ANÁLISIS Y COMUNICACIÓN.</p>
			<ul class="check">
			<li'.marcarOpcion2($datos,'funcionalidad').'>Funcionalidad de la herramienta para la detección: operativas de riesgo definidas en la herramienta. Inclusión de todas las áreas de negocio de la entidad. </li>
			<li'.marcarOpcion2($datos,'comunicaciones').'>Comunicaciones internas de empleados </li>
			<li'.marcarOpcion2($datos,'funcionamiento2').'>Funcionamiento del sistema de comunicación interna de operaciones sospechosas </li>  
			<li'.marcarOpcion2($datos,'relevancia').'>Relevancia de las comunicaciones realizadas por los empleados. Porcentaje de operaciones finalmente comunicadas al Servicio Ejecutivo</li>
			<li'.marcarOpcion2($datos,'proceso2').'>Proceso de análisis especial: base de datos de operaciones especiales: informes periódicos</li>
			<li'.marcarOpcion2($datos,'documentacion').'>Documentación del proceso de análisis especial. Motivación de la decisión en las operaciones no comunicadas</li>  
			</ul>
		<p class="titulo">5. REVISIONES: EXPERTO EXTERNO</p>
			<ul class="check">
			<li'.marcarOpcion2($datos,'valoracion').'>Valoración del informe del experto externo. Debilidades puestas de manifiesto en el mismo</li>
			<li'.marcarOpcion2($datos,'control').'>Control de subsanación de deficiencias</li>
			<li'.marcarOpcion2($datos,'mejoras').'>Mejoras del sistema de PBC/FT como consecuencia del informe</li>
			</ul>
		<page_footer>
			<br/><br/>Realizado por: '.$datos['analista'].'<br/>
			en '.$datos['lugar'].' el '.formateaFechaWeb($datos['fecha']).'
		</page_footer>
';
	}
$contenido.="</page>";

return $contenido;
}

function marcarOpcion($datos,$campo,$puntuacion){
	$res='';
	if($datos[$campo]==$puntuacion){
		$res=' class="marcado"';
	}
	return $res;
}

function marcarOpcion2($datos,$campo){
	$res=' class="marcado'.$datos[$campo].'"';
	return $res;
}
?>
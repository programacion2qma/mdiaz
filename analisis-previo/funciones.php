<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesInformes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizar();
	}
	elseif(isset($_POST['analista'])){
		$res=insertar();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('analisis_previo');
	}

	mensajeResultado('analista',$res,'Análisis previo');
    mensajeResultado('elimina',$res,'Análisis previo', true);
}

function insertar(){
	$res=true;
	$res=insertaDatos('analisis_previo');
	$_POST['codigo']=mysql_insert_id();
	return $res;
}

function actualizar(){
	$res=true;
	$res=actualizaDatos('analisis_previo');
	return $res;
}



function listadoTrabajos(){
	$columnas=array('empleados','volumen','fecha','empleados','empleados');
	/*if($_SESSION['tipoUsuario']=='ADMIN'){
		array_unshift($columnas, 'codigoUsuario');
		$having=obtieneWhereListado("HAVING 1=1",$columnas);
	} else {
		$having=obtieneWhereListado("HAVING codigoUsuario=".$_SESSION['codigoU'],$columnas);
	}*/
	if($_SESSION['espacio']=='NO'){
		$cliente=consultaBD('SELECT codigoCliente FROM usuarios_clientes WHERE codigoUsuario='.$_SESSION['codigoU'],true,true);
		$having=obtieneWhereListado("HAVING codigoCliente=".$cliente['codigoCliente'],$columnas);
	} else {
		$usuario=datosRegistro('usuarios_clientes',$_SESSION['espacio'],'codigoCliente');
		$having=obtieneWhereListado("HAVING codigoCliente=".$_SESSION['espacio'],$columnas);
	}
	$orden=obtieneOrdenListado($columnas);//Uso de función personalizada
	$limite=obtieneLimitesListado();
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, empleados, volumen, fecha, codigoCliente FROM analisis_previo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, empleados, volumen, fecha, codigoCliente FROM analisis_previo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	$empleados=array(1=>'Menos de 10 personas',2=>'10 personas o más');
	$volumen=array(1=>'No supera los 2 millones de euros',2=>'Supera los 2 millones de euros');
	while($datos=mysql_fetch_assoc($consulta)){
		if($datos['empleados']==1 && $datos['volumen']==1){
			$fecha='Queda exceptuado de elaborar y documentar un previo análisis de riesgo en los términos del artículo 32 del RLPBC';
		} else {
			$fecha=formateaFechaWeb($datos['fecha']);
		}
		$fila=array(
			$empleados[$datos['empleados']],
			$volumen[$datos['volumen']],
			$fecha,
			crearBoton($datos),
			obtieneCheckTabla($datos,'codigo'),
        	"DT_RowId"=>$datos['codigo']
		);
		/*if($_SESSION['tipoUsuario']=='ADMIN'){
			$cliente = consultaBD('SELECT clientes.* FROM clientes INNER JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente WHERE codigoUsuario='.$datos['codigoUsuario'],true,true);
			array_unshift($fila, $cliente['razonSocial']);
		}*/
		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function crearBoton($datos){
	$nombres=array();
	$direcciones=array();
	$iconos=array();
	$target=array();

	if($_SESSION['espacio']=='NO'){
		array_push($nombres, 'Detalles');
		array_push($direcciones, 'analisis-previo/gestion.php?codigo='.$datos['codigo']);
		array_push($iconos, 'icon-search-plus');
		array_push($target, 0);
	}

	
	array_push($nombres, 'Descargar Análisis');
	array_push($direcciones, 'analisis-previo/generaDocumento.php?codigo='.$datos['codigo']);
	array_push($iconos, 'icon-download');
	array_push($target, 1);	

	return botonAcciones($nombres,$direcciones,$iconos,$target);
}

//Esta función utiliza un "orden natural" (primero por longitud, luego por el propio valor) para la primera columna del listado de servicios (que es mayoritariamente numérica)
function obtieneOrdenListadoServicios($columnas){
	$orden = '';
    if(isset($_GET['iSortCol_0'])) {
        $orden = 'ORDER BY  ';
        for ($i=0;$i<(int)$_GET['iSortingCols'];$i++) {
            if($_GET['bSortable_'.(int)$_GET['iSortCol_'.$i]]=='true'){
                $indice=(int)$_GET['iSortCol_'.$i];

                if($indice==0 && $_GET['sSortDir_'.$i]==='asc'){//Si se trata de la primera columna y el orden es ascendente...
                	$orden.="LENGTH(referencia), referencia, ";//.. ordeno por longitud y luego valor
                }
                elseif($indice==0 && $_GET['sSortDir_'.$i]!=='asc'){//Lo mismo si el orden es descendente
                	$orden.="LENGTH(referencia) DESC, referencia DESC, ";
                }
                else{//Orden normal
                	$orden.=$columnas[$indice].' '.($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
                }
            }
        }

        $orden = substr_replace($orden, '', -2);
        if ($orden == 'ORDER BY') {
            $orden = '';
        }
    }
    return $orden;
}

function gestionInformes(){
	global $_CONFIG;
	operacionesInformes();

	abreVentanaGestion('Gestión de Informes de riesgo','?','','icon-edit','',true,'noAjax');
	$desplegables=array();
	if(isset($_REQUEST['codigo'])){
		$datos=compruebaDatos('analisis_previo');
		campoOculto($datos['codigoCliente'],'codigoCliente');
		$cliente=datosRegistro('clientes',$datos['codigoCliente']);
		$desplegables['empleados']=$datos['empleados'];
		$desplegables['volumen']=$datos['volumen'];
	} else {
		$datos=false;
		campoOculto($_GET['codigoCliente'],'codigoCliente');
		$cliente=datosRegistro('clientes',$_GET['codigoCliente']);
		if($cliente['empleados']==10){
			$desplegables['empleados']=1;
		} else {
			$desplegables['empleados']=2;
		}
		if($cliente['volumen']==1){
			$desplegables['volumen']=1;
		} else {
			$desplegables['volumen']=2;
		}
	}	

	$pagina = isset($_POST['pagina']) ? $_POST['pagina']:1;
	campoOculto($pagina,'pagina');
	creaPestaniasAPI(array('Página 1','Página 2','Página 3'));
	abrePestaniaAPI(1,$pagina==1);
		echo 'Los procedimientos de control interno se fundamentarán en un previo análisis de riesgo que será documentado por el sujeto obligado. <br/>
El análisis identificará y evaluará los riesgos del sujeto obligado por tipos de clientes, países o áreas geográficas, productos, servicios, operaciones y canales de distribución, tomando en consideración variables tales como el propósito de la relación de negocios, el nivel de activos del cliente, el volumen de las operaciones y la regularidad o duración de la relación de negocios. El análisis de riesgo será revisado periódicamente.<br/>
 Una vez cumplimentado, debe imprimirse y firmarse como acreditación de su aprobación por el sujeto obligado.<br/><br/>

Los sujetos obligados comprendidos en el artículo 2.1 i) a u), ambos inclusive, que, con inclusión de los agentes, ocupen a Menos de 10 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 2 millones de euros, quedan exceptuados de la obligación de documentar un previo análisis de riesgo del propio sujeto obligado en el que se fundamentarán los procedimientos de control interno
';
		botonesNav(2);
	cierraPestaniaAPI();

	abrePestaniaAPI(2,$pagina==2);
		abreColumnaCampos();	
			campoSelect('empleados','Número de empleados del sujeto obligado - con inclusión de los agentes-',array('Menos de 10 personas','10 personas o más'),array(1,2),$desplegables);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoSelect('volumen','Volumen de negocios anual o balance general anual',array('No supera los 2 millones de euros','Supera los 2 millones de euros'),array(1,2),$desplegables);
		cierraColumnaCampos();
		botonesNav(3);
	cierraPestaniaAPI();

	abrePestaniaAPI(3,$pagina==3);
		echo "<div id='divSI'>";
			echo "El sujeto obligado ".$cliente['razonSocial']." queda exceptuado de elaborar y documentar un previo análisis de riesgo en los términos del artículo 32 del RLPBC";
		echo "</div>";

		echo "<div id='divNO' class='hide'>";
			echo '<br/>';
			abreColumnaCampos();
				campoFecha('fecha','Fecha',$datos);
				campoTexto('lugar','Lugar',$datos);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTexto('analista','Analista',$datos);
			cierraColumnaCampos();
			echo '<br clear="all"><br/><br/>';
			echo "<br clear='all'>Es un documento en el que los sujetos obligados deben describir y evaluar la exposición de su actividad profesional al riesgo de blanqueo de capitales. Para ello,  se examinarán determinados elementos. El análisis se cumplimentará, imprimirá y firmará por el responsable de elaborarlo.<br/><br/>
			<b>a) A fin de valorar el riesgo de los siguientes elementos, se establece una puntuación de 1 a 3, correspondiendo la mayor puntuación al mayor riesgo. Marcar las casillas que correspondan a la realidad de la actividad que lleva a cabo el sujeto obligado:<br/><br/></b>";
			echo "<div id='formulario'>";
			echo "<br clear='all'><h2 class='apartadoFormulario'>1. CARACTERÍSTICAS DEL SUJETO OBLIGADO</h1>";
			
			campoRadio('ubicacion','1.1 UBICACIÓN: Zona geográfica donde desarrolla su actividad profesional',$datos,1,array('Zona del interior - Puntuación 1','Zona del litoral e islas - Puntuación 2','Municipios de más de 750.000 habitantes - Puntuación 3'),array(1,2,3),true);

			campoRadio('actividad','1.2	ACTIVIDAD: Frecuencia con que presta servicios de compraventa de inmuebles, compraventa o constitución de sociedades, apertura o gestión de cuentas corrientes o de ahorro de clientes, organización de aportaciones sociales, gestión de sociedades, ejercicio de cargos de administración, asesoramiento fiscal',$datos,1,array('Estas actividades son ejercidas de forma esporádica - Puntuación 1','Estas actividades son ejercidas de forma habitual, pero no exclusivamente - Puntuación 2','Son la base del despacho - Puntuación 3'),array(1,2,3),true);

			campoRadio('tamanio','1.3 TAMAÑO DEL DESPACHO: Volumen de negocio de las actividades mencionadas en el apartado anterior, entendiendo por volumen de negocio el importe de la facturación',$datos,1,array('Menos de 50.000 euros al año - Puntuación 1','Más de 50.000 euros al año - Puntuación 2','Más de 100.000 euros al año - Puntuación 3'),array(1,2,3),true);

			campoRadio('sistema1','1.4	SISTEMA DE INGRESO, MOVIMIENTO Y TRANSMISIÓN DE FONDOS A TRAVÉS DE CUENTAS DE CLIENTES',$datos,1,array('Únicamente provisiones de fondos para pago de impuestos y suplidos - Puntuación 1','Recepción de depositos por cuenta del cliente - Puntuación 2','Recepción de fondos del cliente para realizar la inversión y/o desinversión - Puntuación 3'),array(1,2,3),true);

			echo "<br clear='all'><h2 class='apartadoFormulario'>2. TIPOLOGÍAS DE CLIENTES: SON HABITUALES</h1>";
			campoRadio('clientes','Habituales',$datos,1,array('Clientes españoles - Puntuación 1','Clientes residentes - Puntuación 2','Clientes no residentes de país comunitario y de país extracomunitario pero equivalente - Puntuación 3','Clientes no residentes de país no equivalente, paraíso fiscal, con alto nivel de corrupción o sujetos a sanciones internacionales - Puntuación 3 con un plus de riesgo','Personas con responsabilidad pública - Puntuación 3','Clientes con estructura de propiedad compleja - Puntuación 3'),array(1,2,3,4,31,32),true);

			echo "<br/><br/>
			<b>b) A fin de valorar los siguientes elementos, se establece una puntuación de 1 a 3, correspondiendo: Puntuación 1 a un cumplimiento satisfactorio sin necesidad de medidas adicionales significativas, la puntuación 2 a un grado de avance sustantivo en el proceso de implantación de medidas tomadas y la puntuación 3 a una necesidad de implantación de mejoras relevantes. Puntuar en función del criterio establecido:<br/><br/></b>";

			echo "<br clear='all'><h2 class='apartadoFormulario'>1. INVOLUCRACIÓN ALTA DIRECCIÓN: información proporcionada y frecuencia</h1>";
			campoSelect('informacion','- Información y/o documentación a la alta dirección de la entidad sobre temas relacionados con la PBC/FT. Frecuencia con la que la alta dirección es informada de estos temas',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('asistencia','- Asistencia de la Alta Dirección de la entidad a cursos formativos sobre PBC/FT ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('decisiones','-	Decisiones tomadas por la Alta Dirección ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);

			echo "<br clear='all'><h2 class='apartadoFormulario'>2 COMPOSICIÓN DEL  OCI: REPRESENTACIÓN Y FUNCIONALIDAD</h1>";
			campoSelect('areas','- Áreas representadas en el OCI ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('frecuencia','-	Frecuencia de las reuniones ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('dinamica','- Dinámica de las reuniones, asistencia de personal técnico, información que se lleva, agilidad en la toma de decisiones ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('delegacion','- Delegación de funciones en la unidad o en algún otro subcomité ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);

			echo "<br clear='all'><h2 class='apartadoFormulario'>3.	DILIGENCIA DEBIDA</h1>";
			campoSelect('politica','- Política de aceptación de clientes: controles adecuados para detectar personas o entidades cuya admisión no está permitida',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('proceso1','- Proceso de consulta de listas internacionales e internas ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('funcionamiento1','- Funcionamiento de la herramienta informática que asigna el nivel de riesgo del cliente en función de los datos obtenidos en el momento de establecer relaciones de negocio con el mismo',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('medidas','- Medidas adicionales (autorizaciones, documentación adicional requerida, etc.) progresivas en función del riesgo asignado al cliente',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('controles','-	Controles automáticos de identificación generales ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			
			campoSelect('procedimientos1','- Procedimientos para asegurar que en todas las operaciones se identifica correctamente al titular real de la misma',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('conocimiento','- Conocimiento: origen de los fondos',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('contenido','- Contenido de los expedientes de conozca a su cliente (informes de clientes) ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('procedimientos2','- Procedimientos de verificación del origen de los fondos ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('conservacion','- Conservación de documentos ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('sistema2','Sistema de digitalización de documentos',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3));
			
			echo "<br clear='all'><h2 class='apartadoFormulario'>4.	DETECCIÓN, ANÁLISIS Y COMUNICACIÓN</h1>";
			campoSelect('funcionalidad','- Funcionalidad de la herramienta para la detección: operativas de riesgo definidas en la herramienta. Inclusión de todas las áreas de negocio de la entidad',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('comunicaciones','- Comunicaciones internas de empleados ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('funcionamiento2','-	Funcionamiento del sistema de comunicación interna de operaciones sospechosas ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('relevancia','-	Relevancia de las comunicaciones realizadas por los empleados. Porcentaje de operaciones finalmente comunicadas al Servicio Ejecutivo',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('proceso2','- Proceso de análisis especial: base de datos de operaciones especiales: informes periódicos ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('documentacion','- Documentación del proceso de análisis especial. Motivación de la decisión en las operaciones no comunicadas',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);

			echo "<br clear='all'><h2 class='apartadoFormulario'>5. REVISIONES: EXPERTO EXTERNO:</h1>";
			campoSelect('valoracion','- Valoración del informe del experto externo. Debilidades puestas de manifiesto en el mismo ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('control','- Control de subsanación de deficiencias ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);
			campoSelect('mejoras','- Mejoras del sistema de PBC/FT como consecuencia del informe ',array('Puntuación 1','Puntuación 2','Puntuación 3'),array(1,2,3),$datos);

			campoTexto('representante','Nombre y apellidos del representante ante el SEPBLAC',$datos,'span10');
		echo "</div>";
	cierraPestaniaAPI();

	cierraPestaniasAPI();

	cierraVentanaGestion('index.php',true);
}




function botonesNav($siguiente=false,$anterior=false){

	if(!!$siguiente){
	echo "<br clear='all'><br/><a href='#".$siguiente."' style='float:right;' class='btn btn-propio btnSiguiente noAjax' data-toggle='tab'>Siguiente <i class='icon-arrow-right'></i></a>";
	}	
	if(!!$anterior){
		echo "<a href='#".$anterior."' style='float:left;' class='btn btn-propio btnAnterior noAjax' data-toggle='tab'><i class='icon-arrow-left'></i> Anterior</a>";
	}
}



function filtroServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Referencia');
	campoTexto(1,'Servicio','','span3');
	campoTextoSimbolo(2,'Precio','€');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(3,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de servicios
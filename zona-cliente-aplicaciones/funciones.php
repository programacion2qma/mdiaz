<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesAplicaciones(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaAplicacion();
	}
	elseif(isset($_POST['nombreAplicacionLOPD'])){
		$res=insertaAplicacion();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('aplicaciones_lopd');
	}

	mensajeResultado('nombreAplicacionesLOPD',$res,'Aplicación');
    mensajeResultado('elimina',$res,'Aplicación', true);
}

function insertaAplicacion(){
	$res=true;
	$res=insertaDatos('aplicaciones_lopd');
	return $res;
}

function actualizaAplicacion(){
	$res=true;
	$res=actualizaDatos('aplicaciones_lopd');
	return $res;
}


function listadoAplicaciones(){
	global $_CONFIG;

	$columnas=array('nombreAplicacionLOPD','activo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT aplicaciones_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM aplicaciones_lopd INNER JOIN trabajos ON aplicaciones_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT aplicaciones_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM aplicaciones_lopd INNER JOIN trabajos ON aplicaciones_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['nombreAplicacionLOPD'],
			$datos['finalidadAplicacionLOPD'],
			$datos['empleadosAplicacionLOPD'],
			formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'],
			creaBotonDetalles("zona-cliente-aplicaciones/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionAplicaciones(){
	operacionesAplicaciones();

	abreVentanaGestion('Gestión de Aplicaciones que traten datos de carácter personal','?','','icon-edit','margenAb');
	$datos=compruebaDatos('aplicaciones_lopd');

    if(!$datos){
    	abreColumnaCampos();
    		campoSelectConsulta('codigoTrabajo','Consultoría','SELECT trabajos.codigo, servicios.servicio AS texto FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE codigoCliente='.obtenerCodigoCliente(true),false,'selectpicker span5');
    	cierraColumnaCampos(true);
    }

	abreColumnaCampos();
		campoTexto('nombreAplicacionLOPD','Aplicación',$datos,'span5');
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoTexto('finalidadAplicacionLOPD','Finalidad',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto('empleadosAplicacionLOPD','Empleados',$datos);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
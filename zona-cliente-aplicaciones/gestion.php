<?php
  $seccionActiva=65;
  include_once("../cabecera.php");
  gestionAplicaciones();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
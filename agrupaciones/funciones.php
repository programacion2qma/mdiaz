<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesAgrupaciones(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('agrupaciones');
	}
	elseif(isset($_POST['agrupacion'])){
		$res=insertaDatos('agrupaciones');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('agrupaciones');
	}

	mensajeResultado('agrupacion',$res,'Agrupación');
    mensajeResultado('elimina',$res,'Agrupación', true);
}


function listadoAgrupaciones(){
	global $_CONFIG;

	$columnas=array('agrupacion','activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT * FROM agrupaciones $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT * FROM agrupaciones $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['agrupacion'],
			creaBotonDetalles("agrupaciones/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionAgrupacion(){
	operacionesAgrupaciones();

	abreVentanaGestion('Gestión de Agrupaciones','?','span3','icon-edit','margenAb');
	$datos=compruebaDatos('agrupaciones');

	campoTexto('agrupacion','Nombre agrupación',$datos,'span3');

	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoRadio('activo','Activa',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}

function filtroAgrupaciones(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
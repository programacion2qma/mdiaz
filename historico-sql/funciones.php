<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de histórico SQL

function listadoSentenciasSQL(){
	$columnas=array('codigoUsuario','fecha','hora','sentencia','fecha');
	$where=obtieneWhereListado("WHERE 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT * FROM sentencias_sql $where";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
echo $query;
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$usuario=obtieneUsuarioSentenciaSQL($datos['codigoUsuario']);

		$fila=array(
			$usuario,
			formateaFechaWeb($datos['fecha']),
			$datos['hora'],
			base64_decode($datos['sentencia']),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}
	cierraBD();

	echo json_encode($res);
}

function obtieneUsuarioSentenciaSQL($codigoUsuario){
	$res='-';
	
	if($codigoUsuario!=0){
		$consulta=consultaBD("SELECT CONCAT(nombre,' ',apellidos,' (',usuario,')') AS usuario FROM usuarios WHERE codigo='$codigoUsuario';",false,true);
		$res=$consulta['usuario'];
	}

	return $res;
}

function filtroSentenciasSQL(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectConsulta(0,'Usuario',"SELECT codigo, CONCAT(nombre,' ',apellidos,' (',usuario,')') AS texto FROM usuarios ORDER BY nombre");
	campoFecha(1,'Fecha desde');
	campoFecha(4,'Hasta');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(2,'Hora','','input-mini pagination-right');
	campoTexto(3,'Sentencia','','span4');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de histórico SQL
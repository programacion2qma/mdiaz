<?php
  $seccionActiva=0;
  include_once('cabecera.php');
  defineFiltroEjercicio();//Para filtro por años
  //revisaFechaVencimientos();
  //revisaFechasTutorias();
?>

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <?php
        creaBotonesInicio();
      ?>

    <div class='span12'>
      <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-calendar"></i>
          <h3>Tareas registradas para hoy</h3>
        </div>
        <div class="widget-content">
          <table class="table table-striped table-bordered datatable"  id="target-3">
            <thead>
              <tr>
                <th> Tarea </th>
                <th> Inicio </th>
                <th> Fin </th>
                <th> Cliente </th>
              </tr>
            </thead>
            <tbody>

              <?php
                imprimeTareasHoy();
              ?>

            </tbody>
          </table>
        </div>
      </div>

     </div>
    </div>
    <!-- /container -->
  </div>
  <!-- /main-inner -->
</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/guidely/guidely.min.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function(){
  <?php animacionInicio(); ?>

  var pendientes=<?php compruebaMensajesPorLeer(); ?>;
  
  if(pendientes>0){
    $('#avisoMensajes').addClass('badge').text(pendientes);
  }

  guidely.add ({
    attachTo: '#target-1'
    , anchor: 'top-left'
    , title: 'Botones de gestión'
    , text: 'En la parte inferior derecha de la pantalla encontrará siempre los <strong>botones de gestión</strong> de la sección en la que se encuentre.<br />Si deja el cursor encima de un botón, aparecerá una pequeña leyenda.'
  });

  guidely.add ({
    attachTo: '#target-3'
    , anchor: 'middle-middle'
    , title: 'Listado de Elementos'
    , text: 'Esta tabla muestra un listado en el que podrá consultar los elementos creados en cada sección. Utilice la caja de <b>Búsqueda</b> para filtrar los registros a visualizar.<br />Adicionalmente, la mayoría de secciones cuentan con el botón <strong>Búsqueda por filtros</strong> ubicado en la parte superior derecha del listado.'
  });

  guidely.add ({
    attachTo: '#target-4'
    , anchor: 'middle-left'
    , title: 'Menú principal'
    , text: 'Una vez que accede al sistema, el <strong>Menú Principal</strong> está siempre disponible para permitirle navegar entre los distintos módulos de la herramienta.<br />Solo tiene que pulsar en la sección o sub-sección correspondiente para acceder a ella.'
  });

    guidely.add ({
    attachTo: '#target-5'
    , anchor: 'bottom-left'
    , title: 'Menú de Usuario y<br /> Selector de Ejercicios'
    , text: 'El <strong>Menú de Usuario</strong> le recuerda en cada momento con que cuenta está trabajando, y le proporciona un acceso directo para cerrar su sesión.<br />Por último, el <strong>Selector de Ejercicios</strong> le permite filtrar los datos que se muestran en el software en función del año seleccionado (o una modalidad sin filtrar, pulsando en la opción <strong>Todos</strong>).'
  });

  $('#target-1').click(function(){
    guidely.init({welcome: true, startTrigger: false, welcomeTitle: 'Guía de uso básico', welcomeText: 'Bienvenido a la Guía de uso del sistema. Pulsando en el botón Iniciar, la plataforma le mostrará unas sencillas explicaciones sobre los distintos elementos que encontrará en la mayoría de las secciones.' });
  });

});
</script>

<!-- /contenido -->
</div>

<?php 
include_once('pie.php');
?>
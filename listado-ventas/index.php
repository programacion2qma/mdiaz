<?php
  $seccionActiva=22;
  include_once('../cabecera.php');

  limpiaServiciosEnFacturas();//Explicación en declaración
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="contenedorListadoVentas">
      
      <form action='generaExcel.php' method="post" class='noAjax'>
        <div class='scrollTablaVentas'><div class='cajaScroll'></div></div>
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Listado detallado de ventas de formación y servicios</h3>
              <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-small"><i class="icon-cloud-download"></i> Descargar en Excel</button>
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroVentas();
                campoOculto('','global');//Campo para filtrar la consulta del excel con el término del campo de búsqueda (si lo hubiere)
              ?>
              <table class="table table-striped tablaTextoPeque table-bordered datatable" id="tablaVentas">
                <thead>
                  <tr>
                    <th>F. de Venta</th>
                    <th>F. de Factura</th>
                    <th> F. Vencimiento</th>
                    <th>F. de Pago</th>
                    <th>Razón Social</th>
                    <th>Cod. AF/Servicio</th>
                    <th>Grupo</th>
                    <th>Acción Formativa/Servicio</th>
                    <th>Alumno</th>
                    <th>F. inicio</th>
                    <th>F. fin</th>
                    <th>Nº Factura</th>
                    <th>Emisor de la Factura</th>
                    <th class='sumatorio'>Base Imponible</th>
                    <th class='sumatorio'>Importe Total</th>
                    <th class='sumatorio'>Importe complemento/s</th>
                    <th class='sumatorio'>Facturación Neta</th>
                    <th>Abono</th>
                    <th>Complemento</th>
                    <th>Complemento 2</th>
                    <th>Medio de Pago</th>
                    <th>Estado de la factura</th>
                    <th>Agente</th>
                    <th>Colaborador</th>
                    <th>Telemarketing</th>
                    <th>Administrativa</th>
                    <th>Finalizado</th>
                    <th> Grupo anulado </th>
                    <th> </th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>
        </form>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaVentas','../listadoAjax.php?include=listado-ventas&funcion=listadoVentas();');

    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaVentas');

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});


    //Parte de scroll doble
    $('.cajaScroll').css('width','4000px');

    $(".widget-content").scroll(function(){
      $(".scrollTablaVentas").scrollLeft($(".widget-content").scrollLeft());
    });
    $(".scrollTablaVentas").scroll(function(){
      $(".widget-content").scrollLeft($(".scrollTablaVentas").scrollLeft());
    });

    $('input[type=search]').change(function(){
      $('#global').val($(this).val());
    })
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
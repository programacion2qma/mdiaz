<?php

include_once('funciones.php');
compruebaSesion();

//Carga de PHP Excel (usa el de la API para ahorrar espacio en el servidor)
require_once('../../api/phpexcel/PHPExcel.php');
require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/IOFactory.php');

//Carga de la plantilla
$objReader=new PHPExcel_Reader_Excel2007();
$documento=$objReader->load("../documentos/listado-ventas/plantilla.xlsx");

generaExcelListadoVentas($documento);

// Definir headers
header("Content-Type: application/ms-xlsx");
header("Content-Disposition: attachment; filename=Listado-ventas-detallado.xlsx");
header("Content-Transfer-Encoding: binary");

// Descargar archivo
readfile('../documentos/listado-ventas/Listado-ventas-detallado.xlsx');
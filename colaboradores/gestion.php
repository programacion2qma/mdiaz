<?php
  $seccionActiva=20;
  include_once("../cabecera.php");
  gestionColaborador();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script src="../js/cuentaBancaria.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('select').selectpicker();

		$('#numeroCuenta').blur(function(){
			validaNumeroCuenta($(this).val());
		});
	});

	

	function validaNumeroCuenta(ccc){
		if(!isAccountComplete(ccc)){
			alert('Por favor, introduzca un número de cuenta válido.');	
		}
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
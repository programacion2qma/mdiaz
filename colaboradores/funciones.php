<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de colaboradores

function operacionesColaboradores(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('colaboradores');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('colaboradores');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('colaboradores');
	}

	mensajeResultado('nombre',$res,'Colaborador');
    mensajeResultado('elimina',$res,'Colaborador', true);
}

/*function creaEstadisticasColaboradores(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='ADMINISTRACION2'){
        $res=consultaBD("SELECT COUNT(colaboradores.codigo) AS total FROM colaboradores WHERE codigoUsuario=$codigoUsuario",true,true);
    }
    else{
        $res=estadisticasGenericas('colaboradores');;
    }

    return $res;
}*/


function listadoColaboradores(){
	global $_CONFIG;

	$columnas=array('colaboradores.codigo','colaboradores.nombre','nombreComercial','cif','provincia','telefono','movil','contacto','email','colaboradores.activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas,false);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT colaboradores.codigo AS codigo, colaboradores.nombre AS agente, nombreComercial, cif, provincia, colaboradores.telefono, movil, contacto, colaboradores.email, colaboradores.activo FROM colaboradores $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT colaboradores.codigo, colaboradores.nombre AS agente, nombreComercial, cif, provincia, colaboradores.telefono, movil, contacto, colaboradores.email, colaboradores.activo FROM colaboradores $having");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['codigo'],
			$datos['agente'],
			$datos['nombreComercial'],
			$datos['cif'],
			convertirMinuscula($datos['provincia']),
			"<div class='nowrap'>".formateaTelefono($datos['telefono'])."</div>",
			"<div class='nowrap'>".formateaTelefono($datos['movil'])."</div>",
			$datos['contacto'],
			"<a href='mailto:".$datos['email']."'>".$datos['email']."</a>",
			botonAcciones(array('Detalles'),array('colaboradores/gestion.php?codigo='.$datos['codigo']),array('icon-search-plus')),

        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}
/*botonAcciones(array('Detalles','Documentación'),array('colaboradores/gestion.php?codigo='.$datos['codigo'],'colaboradores/generaDocumento.php?codigo='.$datos['codigo']),array('icon-search-plus','icon-download')),*/


function gestionColaborador(){
	operacionesColaboradores();

	abreVentanaGestion('Gestión de Colaboradores','?','span3','icon-edit');
	$datos=compruebaDatosColaborador();

	campoID($datos);
	campoTexto('nombre','Razón social',$datos,'span3');
	campoTexto('nombreComercial','Nombre comercial',$datos,'span3');
	campoTextoValidador('cif','NIF/CIF',$datos,'input-small validaCIF','colaboradores');
	campoTexto('direccion','Dirección',$datos,'span4');
	campoTexto('cp','Código Postal',$datos,'input-mini');
	campoTexto('localidad','Localidad',$datos);
	campoSelectProvinciaAndalucia($datos);
	campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small','colaboradores');
	campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',$datos,'input-small','colaboradores');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoSimboloValidador('fax','Fax','<i class="icon-fax"></i>',$datos,'input-small','colaboradores');
	campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large','colaboradores');
	campoTextoSimboloValidador('email2','eMail secundario','<i class="icon-envelope"></i>',$datos,'input-large','colaboradores');
	campoTexto('contacto','Persona de contacto',$datos);
	campoTexto('numeroCuenta','Número de cuenta',$datos);
	campoRadio('llamadoColaborador','Llamado',$datos);
	campoRadio('visitaColaborador','Visitado',$datos);
	campoRadio('resultadoVisitaColaborador','Resultado visita',$datos,'NO',array('Vendido','Seguimiento','No'),array('VENDIDO','SEGUIMIENTO','NO'));
	campoRadio('activo','Activo',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}


function compruebaDatosColaborador(){
	if(isset($_GET['codigoAsesoria'])){
		$datos=array();

		$datosAsesoria=datosRegistro('asesorias',$_GET['codigoAsesoria']);

		$datos['nombre']=$datosAsesoria['asesoria'];
		$datos['cif']=false;
		$datos['provincia']=false;
		$datos['telefono']=$datosAsesoria['telefonoPrincipal'];
		$datos['movil']=false;
		$datos['fax']=false;
		$datos['email']=$datosAsesoria['emailPrincipal'];
		$datos['email2']=false;
		$datos['contacto']=false;
		$datos['direccion']=false;
		$datos['cp']=false;
		$datos['localidad']=false;
		$datos['activo']='SI';
	}
	else{
		$datos=compruebaDatos('colaboradores');	
	}

	return $datos;
}

function filtroColaboradores(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(1,'Razón social');
	campoTexto(2,'Nombre comercial');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectProvinciaAndalucia(false,4);
	campoSelectSiNoFiltro(9,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de colaboradores
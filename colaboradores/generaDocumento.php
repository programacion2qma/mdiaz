<?php

@include_once('funciones.php');
	
	$documentos=array();
	$documentos[0]=generaConvenio($_GET['codigo']);
	generaZip($documentos);

function generaConvenio($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    $datos = datosRegistro("colaboradores",$codigo);
	$nombreFichero="convenio_colaboracion.docx";
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("contacto",utf8_decode($datos['contacto']));
	$documento->setValue("nombre",utf8_decode($datos['nombre']));
	$documento->setValue("cif",utf8_decode($datos['cif']));
	$documento->setValue("direccion",utf8_decode($datos['direccion']));
	$documento->setValue("cp",utf8_decode($datos['cp']));
	$documento->setValue("localidad",utf8_decode($datos['localidad']));
	$documento->setValue("email",utf8_decode($datos['email']));
	$documento->setValue("telefono",utf8_decode($datos['telefono']));


	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	/*header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$nombreFichero);*/

    return '../documentos/consultorias/'.$nombreFichero;
}

function generaZip($documentos){
	$zip = new ZipArchive();
	
	$nameZip = '../documentos/consultorias/documentos_colaborador.zip';
	if(file_exists($nameZip)){
		unlink($nameZip);
	}
	$fichero = $nameZip;
	
	if($zip->open($fichero,ZIPARCHIVE::CREATE)===true) {
		foreach ($documentos as $documento){
			if(is_array($documento)){
				foreach ($documento as $doc){
					$name = str_replace('../documentos/consultorias/', '', $doc);
					$zip->addFile($doc , $name);
				}
			} else {
				$name = str_replace('../documentos/consultorias/', '', $documento);
				$zip->addFile($documento , $name);
			}
		}
	}
	if(!$zip->close()){
		echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
		echo $zip->getStatusString();
	}

	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=documentos_colaborador.zip");
	header("Content-Transfer-Encoding: binary");

	readfile($nameZip);
}
?>
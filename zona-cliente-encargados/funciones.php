<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesEncargados(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaEncargado();
	}
	elseif(isset($_POST['codigoTrabajo'])){
		$res=insertaEncargado();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaEncargados();
	}

	mensajeResultado('codigoTrabajo',$res,'Encargado de tratamiento');
    mensajeResultado('elimina',$res,'Encargado de tratamiento', true);
}

function insertaEncargado(){
	$res=true;
	if($_REQUEST['fijo']=='SI'){
		preparaPOST();
		actualizaFormulario();
		$_REQUEST['codigo']=$_POST['codigoEncargado'];
		insertaTratamientos($_POST['codigoEncargado']);
	} else {
		preparaCampos();
		$res=insertaDatos('otros_encargados_lopd');
		insertaTratamientos($res);
	}
	return $res;
}

function actualizaEncargado(){
	$res=true;
	if($_REQUEST['fijo']=='SI'){
		actualizaFormulario();
		insertaSubcontrataciones($_POST['codigo'],'ENCARGADOFIJO');
		insertaTratamientos($_POST['codigo']);
	} else {
		preparaCampos();
		$res=actualizaDatos('otros_encargados_lopd');
		insertaSubcontrataciones($_POST['codigo'],'ENCARGADO');
		insertaTratamientos($_POST['codigo']);
	}
	return $res;
}

function insertaTratamientos($codigo){
	$_POST['codigoEncargado']=$codigo;
	if(isset($_POST['codigoTrat'])){
		$_POST['codigo']=$_POST['codigoTrat'];
		$res=actualizaDatos('encargados_tratamientos');
	} else {
		$res=insertaDatos('encargados_tratamientos');
	}
	$_REQUEST['codigo']=$codigo;
}

function insertaSubcontrataciones($codigo,$tipo){
	$res=true;
	$res=consultaBD('DELETE FROM declaraciones_subcontrata WHERE codigoResponsable='.$codigo.' AND codigoTrabajo='.$_POST['codigoTrabajo'].' AND tipo="'.$tipo.'";',true);
	$i=0;
	while(isset($_POST['empresa'.$i])){
		if($_POST['empresa'.$i]!=''){
			$res=$res && consultaBD("INSERT INTO declaraciones_subcontrata(`codigo`, `codigoResponsable`, `empresa`, `tipo`, `codigoTrabajo`) VALUES (NULL,".$codigo.",'".$_POST['empresa'.$i]."','".$tipo."',".$_POST['codigoTrabajo'].")",true);
		}
		$i++;
	}
	return $res;
}

function eliminaEncargados(){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$partes=explode('_', $datos['codigo'.$i]);
		if(count($partes)==1){
			$consulta=consultaBD("DELETE FROM otros_encargados_lopd WHERE codigo='".$partes[0]."';");
		} else {
			$trabajo=consultaBD('SELECT formulario FROM trabajos WHERE codigo='.$partes[0],false,true);
			$formulario=str_replace('pregunta'.$partes[1].'=>SI', 'pregunta'.$partes[1].'=>NO', $trabajo['formulario']);
			$consulta=consultaBD('UPDATE trabajos SET formulario="'.$formulario.'" WHERE codigo='.$partes[0]);
		}
	}
	if(!$consulta){
		$res=false;
		echo mysql_error();
	}
	cierraBD();

	return $res;
}

function actualizaFormulario(){
	$trabajo=datosRegistro('trabajos',$_POST['codigoTrabajo']);
	$formulario=recogerFormularioServicios($trabajo);
	$query='';
	$j=1;
	foreach ($formulario as $key => $value) {
		if($j>1){
			$query.="&{}&";
		}
		if(isset($_POST[$key])){
			if(is_array($_POST[$key])){
				$select = $_POST[$key];
				$text ='';
				if(!empty($select)){
					for($k=0;$k<count($select);$k++){
						if($k > 0){
							$text .= "&$&";
						}
						$text .= $select[$k];
					}	
				}
				$formulario[$key]=$text;
			} else {
				$formulario[$key]=$_POST[$key];
			}
		}
		$query.="pregunta$j=>".$formulario[$key];
		$j++;
	}
	$res=consultaBD('UPDATE trabajos SET formulario="'.$query.'" WHERE codigo='.$_POST['codigoTrabajo'],true);

}

function preparaCampos(){
	$select = $_POST['ficheroEncargado'];
	$text ='';
	if(!empty($select)){
		for($j=0;$j<count($select);$j++){
			if($j > 0){
				$text .= "&$&";
			}
			$text .= $select[$j];
		}
	}
	$_POST['ficheroEncargado'] = $text;

	$select = $_POST['dondeEncargado'];
	$text ='';
	if(!empty($select)){
		for($j=0;$j<count($select);$j++){
			if($j > 0){
				$text .= "&$&";
			}
			$text .= $select[$j];
		}
	}
	$_POST['dondeEncargado'] = $text;
}

function preparaPOST(){
	$_POST['pregunta'.$_POST['codigoEncargado']]='SI';
	$campos=array(
		158=>array(159,160,161,162,163,164,165,166,167,511,541,543,544,574,589,611,629),
		168=>array(169,170,171,172,173,174,175,176,177,512,527,545,546,575,590,612,630),
		178=>array(179,180,181,182,183,184,185,186,187,513,542,547,548,576,591,613,188,631),
		189=>array(190,191,192,193,194,195,196,197,198,514,528,549,550,577,592,614,199,497,498,499,500,632),
		200=>array(201,202,203,204,205,206,207,208,209,515,529,551,552,578,593,615,633),
		210=>array(211,212,213,214,215,216,217,218,219,516,530,553,554,579,594,616,634),
		220=>array(221,222,223,224,225,226,227,228,229,517,531,555,556,609,595,617,635),
		230=>array(231,232,233,234,235,236,237,238,239,518,532,557,558,580,596,618,636),
		240=>array(241,242,243,244,245,246,247,248,249,519,533,559,560,581,597,619,637),
		250=>array(251,252,253,254,255,256,257,258,259,520,534,561,562,582,598,620,638),
		260=>array(261,262,263,264,265,266,267,268,269,521,535,563,564,583,599,621,639),
		270=>array(271,272,273,274,275,276,277,278,279,522,536,565,566,584,600,622,640),
		280=>array(281,282,283,284,285,286,287,288,289,523,537,567,568,585,601,623,641),
		290=>array(291,292,293,294,295,296,297,298,299,524,538,569,570,586,602,624,18,642),
		487=>array(488,489,490,491,492,493,494,495,496,525,571,572,544,587,603,625,643));
	$campos=$campos[$_POST['codigoEncargado']];
	$i=0;
	foreach ($campos as $key => $value) {
		$_POST['pregunta'.$value]=$_POST['preguntaNuevo'.$i];
		$i++;
	}
}

function listadoEncargados(){
	global $_CONFIG;

	$columnas=array('fecha','activo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT trabajos.codigo,trabajos.codigoCliente, trabajos.fecha, servicios.servicio, trabajos.formulario FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT trabajos.codigo, trabajos.codigoCliente, trabajos.fecha, servicios.servicio, trabajos.formulario FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
	$servicios=array(158=>'Asesoría laboral para la realización de nóminas',168=>'Asesoría contable/fiscal',178=>'Video vigilancia',189=>'Prevención de riesgos laborales',200=>'Mantenimiento de equipos informáticos',210=>'Mantenimiento de aplicaciones',220=>'Mantenimiento de la página web',230=>'Copias de seguridad',240=>'Transporte/mensajería para envíos a clientes',250=>'Limpieza',260=>'Mantenimiento de instalaciones',270=>'Vigilancia/seguridad',280=>'Mantenimiento de la copiadora',290=>'Agente o colaboración mercantil',487=>'Selección de personal');
	$ficheros=array(158=>574,168=>575,178=>576,189=>577,200=>578,210=>579,220=>609,230=>580,240=>581,250=>582,260=>583,270=>584,280=>585,290=>586,487=>587);
	while($datos=mysql_fetch_assoc($consulta)){
		$formulario=recogerFormularioServicios($datos);
		foreach ($encargados as $key => $value) {
			if($formulario['pregunta'.$value]=='SI'){
				$nombre=$value+1;
				$nif=$value+2;
				$fichero=obtieneFicheros($formulario['pregunta'.$ficheros[$value]]);
				$fila=array(
					$formulario['pregunta'.$nombre],
					$formulario['pregunta'.$nif],
					$servicios[$value],
					$fichero,
					formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'],
					creaBotonDetalles("zona-cliente-encargados/gestion.php?fijo=SI&codigo=".$value."&codigoTrabajo=".$datos['codigo']),
        			obtieneCheckTablaEncargados($datos,$value),
        			"DT_RowId"=>$datos['codigo']
				);

				$res['aaData'][]=$fila;
			}
		}
	}
	$consulta=consultaBD("SELECT otros_encargados_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM otros_encargados_lopd INNER JOIN trabajos ON otros_encargados_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having $orden $limite;");
	while($datos=mysql_fetch_assoc($consulta)){
		$fichero=obtieneFicheros($datos['ficheroEncargado']);
		$fila=array(
			$datos['nombreEncargado'],
			$datos['cifEncargado'],
			$datos['servicioEncargado'],
			$fichero,
			formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'],
			creaBotonDetalles("zona-cliente-encargados/gestion.php?fijo=NO&codigo=".$datos['codigo']."&codigoTrabajo=".$datos['codigoTrabajo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function imprimeEncargados(){
	global $_CONFIG;

	if($_SESSION['tipoUsuario']=='CLIENTE'){
		$codigoCliente=obtenerCodigoCliente(true);
	} elseif (isset($_GET['codigoCliente'])) {
		$codigoCliente=$_GET['codigoCliente'];
		$_SESSION['codigoCliente']=$codigoCliente;
	} elseif (isset($_SESSION['codigoCliente'])) {
		$codigoCliente=$_SESSION['codigoCliente'];
	}
	$consulta=consultaBD("SELECT trabajos.codigo,trabajos.codigoCliente, trabajos.fecha, servicios.servicio, trabajos.formulario FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo HAVING codigoCliente=".$codigoCliente, true);
	$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
	$servicios=array(158=>'Asesoría laboral para la realización de nóminas',168=>'Asesoría contable/fiscal',178=>'Video vigilancia',189=>'Prevención de riesgos laborales',200=>'Mantenimiento de equipos informáticos',210=>'Mantenimiento de aplicaciones',220=>'Mantenimiento de la página web',230=>'Copias de seguridad',240=>'Transporte/mensajería para envíos a clientes',250=>'Limpieza',260=>'Mantenimiento de instalaciones',270=>'Vigilancia/seguridad',280=>'Mantenimiento de la copiadora',290=>'Agente o colaboración mercantil',487=>'Selección de personal');
	$ficheros=array(158=>574,168=>575,178=>576,189=>577,200=>578,210=>579,220=>609,230=>580,240=>581,250=>582,260=>583,270=>584,280=>585,290=>586,487=>587);
	while($datos=mysql_fetch_assoc($consulta)){
		$formulario=recogerFormularioServicios($datos);
		foreach ($encargados as $key => $value) {
			if($formulario['pregunta'.$value]=='SI'){
				$nombre=$value+1;
				$nif=$value+2;
				$fichero=obtieneFicheros($formulario['pregunta'.$ficheros[$value]]);
				echo "
				<tr>
					<td> ".$formulario['pregunta'.$nombre]." </td>
					<td> ".$formulario['pregunta'.$nif]." </td>
					<td> ".$servicios[$value]." </td>
					<td> ".$fichero." </td>
					<td> ".formateaFechaWeb($datos['fecha']).' - '.$datos['servicio']." </td>
        			<td class='td-actions'>
        			<a href='gestion.php?fijo=SI&codigo=".$value."&codigoTrabajo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
        			</td>
        			<td>
                		<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."_".$value."'>
            		</td>
    			</tr>";
			}
		}
	}
	$consulta=consultaBD("SELECT otros_encargados_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM otros_encargados_lopd INNER JOIN trabajos ON otros_encargados_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo HAVING codigoCliente=".$codigoCliente,TRUE);
	while($datos=mysql_fetch_assoc($consulta)){
		$fichero=obtieneFicheros($datos['ficheroEncargado']);
		echo "
			<tr>
				<td> ".$datos['nombreEncargado']." </td>
				<td> ".$datos['cifEncargado']." </td>
				<td> ".$datos['servicioEncargado']." </td>
				<td> ".$fichero." </td>
				<td> ".formateaFechaWeb($datos['fecha']).' - '.$datos['servicio']." </td>
        		<td class='td-actions'>
        			<a href='gestion.php?fijo=NO&codigo=".$datos['codigo']."&codigoTrabajo=".$datos['codigoTrabajo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
        		</td>
        		<td>
                	<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            	</td>
    		</tr>";
	}
}

function obtieneFicheros($ficheros){
	$res='';
	$ficheros=explode('&$&', $ficheros);
	foreach ($ficheros as $key => $value) {
		$item=datosRegistro('declaraciones',$value);
		if($res!=''){
			$res.=',<br/>';
		}
		$res.=$item['nombreFichero'];
	}

	return $res;
}


function gestionEncargados(){
	operacionesEncargados();

	$servicios=array(158=>'Asesoría laboral para la realización de nóminas',168=>'Asesoría contable/fiscal',178=>'Video vigilancia',189=>'Prevención de riesgos laborales',200=>'Mantenimiento de equipos informáticos',210=>'Mantenimiento de aplicaciones',220=>'Mantenimiento de la página web',230=>'Copias de seguridad',240=>'Transporte/mensajería para envíos a clientes',250=>'Limpieza',260=>'Mantenimiento de instalaciones',270=>'Vigilancia/seguridad',280=>'Mantenimiento de la copiadora',290=>'Agente o colaboración mercantil',487=>'Selección de personal');

	$consulta=consultaBD("SELECT codigo, nombreFichero FROM declaraciones WHERE codigoCliente='".obtenerCodigoCliente(true)."';",true);
	$opciones=array();
	$valores=array();
    while($datosFicheros=mysql_fetch_assoc($consulta)){
        array_push($opciones, $datosFicheros['nombreFichero']);
        array_push($valores, $datosFicheros['codigo']);        
    }

	abreVentanaGestion('Gestión de Encargados de tratamiento','?','','icon-edit','margenAb');
	$datos=preparaEncargado();
    if($datos[1][0]==''){
    	abreColumnaCampos();
    		campoSelectConsulta('codigoTrabajo','Consultoría','SELECT trabajos.codigo, servicios.servicio AS texto FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE codigoCliente='.obtenerCodigoCliente(true),false,'selectpicker span5');
    	cierraColumnaCampos(true);
    } else {
    	campoOculto($_REQUEST['codigoTrabajo'],'codigoTrabajo');
    }

	abreColumnaCampos();
		if($_REQUEST['fijo']=='SI'){
			if(isset($_REQUEST['codigo'])){
				campoDato('Servicio',$servicios[$_REQUEST['codigo']]);
			} else {
				campoSelect('codigoEncargado','Servicio',array(),array());
			}
			$e=count($datos[0])-1;//Indice del campo exclusivoEncargado
		} else {
			campoTexto($datos[0][17],'Servicio',$datos[1][17]);
			$e=count($datos[0])-2;//Indice del campo exclusivoEncargado
		}
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoTexto($datos[0][0],'Nombre o Razón Social',$datos[1][0]);
		campoTexto($datos[0][2],'Dirección Social',$datos[1][2]);
		campoTexto($datos[0][4],'Localidad',$datos[1][4]);
		campoSelectProvinciaAEPD($datos[0][15],$datos[1][15]);
		campoTexto($datos[0][6],'Teléfono',$datos[1][6]);
		campoTexto($datos[0][8],'Rpt. Legal',$datos[1][8]);
		echo '<div id="radioSubcontrata">';
			campoRadio($datos[0][10],'¿Está permitida la subcontratación?',$datos[1][10]);
		echo '</div>';
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto($datos[0][1],'NIF/CIF',$datos[1][1],'input-small');
		campoTexto($datos[0][3],'E-mail',$datos[1][3]);
		campoTexto($datos[0][5],'Código Postal',$datos[1][5]);
		campoTexto($datos[0][7],'Persona de contacto/Tlf',$datos[1][7]);
		campoSelectMultiple($datos[0][9],'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$datos[1][9]);
		campoRadio($datos[0][$e],'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$datos[1][$e]);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoFechaBlanco($datos[0][11],'Fecha del contrato de prestación de servicio',formateaFechaBD($datos[1][11]));
		camposelectMultiple($datos[0][13],'Ficheros a los que acceden',$opciones,$valores,$datos[1][13],'selectpicker span3 show-tick multipleFicheros');
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaBlanco($datos[0][12],'Fecha de vencimiento de prestación de servicio',formateaFechaBD($datos[1][12]));
		campoSelect($datos[0][14],'Accesos',array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),$datos[1][14],'selectpicker span2 show-tick','');
	cierraColumnaCampos(true);

	if($_REQUEST['fijo']=='SI' && isset($_REQUEST['codigo'])){
		if($_REQUEST['codigo']==178){
			campoRadio($datos[0][16],'¿Llegan a tener acceso a las imágenes?',$datos[1][16]);
		}
		if($_REQUEST['codigo']==189){
			campoRadio($datos[0][16],'Especialidad: Vigilancia de la Salud',$datos[1][16]);
			campoRadio($datos[0][17],'Especialidad: Ergonomía',$datos[1][17]);
			campoRadio($datos[0][18],'Especialidad: Seguridad',$datos[1][18]);
			campoRadio($datos[0][19],'Especialidad: Higiene',$datos[1][19]);
			campoRadio($datos[0][20],'Especialidad: Otros',$datos[1][20]);
		}
		if($_REQUEST['codigo']==290){
			campoTexto($datos[0][16],'Especificar los servicios que presta',$datos[1][21]);
		}
	}

	echo '<div id="divTratamientos">';
	echo '<h3 class="apartadoFormulario">Tratamientos a realizar</h3>';
	if(isset($_REQUEST['fijo']) && isset($_REQUEST['codigo']) && isset($_REQUEST['codigoTrabajo'])){
		$tratamientos=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$_REQUEST['codigo'].' AND codigoTrabajo='.$_REQUEST['codigoTrabajo'].' AND fijo="'.$_REQUEST['fijo'].'";',true,true);
		if($tratamientos){
			campoOculto($tratamientos['codigo'],'codigoTrat');
		}
	} else {
		$tratamientos=false;
	}
	seccionTratamientos($tratamientos);
	echo '</div>';

	creaTablaSubContratacion();

	cierraVentanaGestion('index.php',true);
}


function seccionTratamientos($tratamientos){
	$listado=array('checkRecogida'=>'Recogida','checkModificacion'=>'Modificación','checkConsulta'=>'Consulta','checkComunicacion'=>'Comunicación','checkComunicacion1'=>'Comunicación por transmisión al responsable del fichero','checkRegistro'=>'Registro','checkConservacion'=>'Conservación','checkCotejo'=>'Cotejo','checkSupresion'=>'Supresión','checkComunicacion2'=>'Comunicación a la Administración Pública competente','checkEstructuracion'=>'Estructuración','checkInterconexion'=>'Interconexión','checkAnalisis'=>'Análisis de conducta','checkDestruccion'=>'Destrucción','checkComunicacion3'=>'Comunicación permitida por ley');
	$i=0;
	
	foreach ($listado as $key => $value) {
		if($i%5==0){
			abreColumnaCampos();
		}
		if($tratamientos){
			$valor=$tratamientos[$key];
		} else {
			if(in_array($key,array('checkRecogida','checkModificacion','checkConsulta','checkComunicacion1','checkRegistro','checkConservacion','checkCotejo','checkSupresion','checkComunicacion2','checkComunicacion3','checkDestruccion'))){
				$valor='SI';
			} else {
				$valor='NO';
			}
		}
		campoCheckIndividual($key,$value,$valor);
		if(($i+1)%5==0){
			cierraColumnaCampos();
		}
		$i++;
	}
	
}

function preparaEncargado(){
	$res=array();
	$res[0]=array();
	$res[1]=array();
	$fijo=$_REQUEST['fijo'];
	campoOculto($fijo,'fijo');
	if(isset($_REQUEST['codigo'])){
		$codigo=$_REQUEST['codigo'];
		$codigoTrabajo=$_REQUEST['codigoTrabajo'];
		campoOculto($codigo,'codigo');
		if($fijo=='SI'){
			$campos=array(
				158=>array(159,160,161,162,163,164,165,166,167,511,541,543,544,574,589,611,629),
				168=>array(169,170,171,172,173,174,175,176,177,512,527,545,546,575,590,612,630),
				178=>array(179,180,181,182,183,184,185,186,187,513,542,547,548,576,591,613,188,631),
				189=>array(190,191,192,193,194,195,196,197,198,514,528,549,550,577,592,614,199,497,498,499,500,632),
				200=>array(201,202,203,204,205,206,207,208,209,515,529,551,552,578,593,615,633),
				210=>array(211,212,213,214,215,216,217,218,219,516,530,553,554,579,594,616,634),
				220=>array(221,222,223,224,225,226,227,228,229,517,531,555,556,609,595,617,635),
				230=>array(231,232,233,234,235,236,237,238,239,518,532,557,558,580,596,618,636),
				240=>array(241,242,243,244,245,246,247,248,249,519,533,559,560,581,597,619,637),
				250=>array(251,252,253,254,255,256,257,258,259,520,534,561,562,582,598,620,638),
				260=>array(261,262,263,264,265,266,267,268,269,521,535,563,564,583,599,621,639),
				270=>array(271,272,273,274,275,276,277,278,279,522,536,565,566,584,600,622,640),
				280=>array(281,282,283,284,285,286,287,288,289,523,537,567,568,585,601,623,641),
				290=>array(291,292,293,294,295,296,297,298,299,524,538,569,570,586,602,624,18,642),
				487=>array(488,489,490,491,492,493,494,495,496,525,571,572,544,587,603,625,643));
			$trabajo=datosRegistro('trabajos',$codigoTrabajo);
			$formulario=recogerFormularioServicios($trabajo);
			foreach ($campos[$codigo] as $key => $value) {
				array_push($res[0], 'pregunta'.$value);
				array_push($res[1], $formulario['pregunta'.$value]);
			}
		} else {
			$item=datosRegistro('otros_encargados_lopd',$codigo);
			foreach ($item as $key => $value) {
				if($key!='codigo' && $key!='codigoTrabajo' && $key!='servicioEncargado'){
					array_push($res[0], $key);
					array_push($res[1], $value);
				}
			}
			$res[1][11]=formateaFechaWeb($res[1][11]);
			$res[1][12]=formateaFechaWeb($res[1][12]);
			array_push($res[0], 'servicioEncargado');
			array_push($res[1], $item['servicioEncargado']);
		}
	} else {
		if($fijo=='SI'){
			for($i=0;$i<17;$i++){
				$res[0][$i]='preguntaNuevo'.$i;
				$res[1][$i]='';
			}
		} else {
			conexionBD();
			$campos=camposTabla('otros_encargados_lopd');
			foreach ($campos as $key => $value) {
				if($value!='codigo' && $value!='codigoTrabajo' && $value!='servicioEncargado'){
					array_push($res[0], $value);
					array_push($res[1], '');
				}
			}
			array_push($res[0], 'servicioEncargado');
			array_push($res[1], '');
			cierraBD();
		}
	}
	return $res;
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function creaBotonesGestionEncargados($codigo=false,$nombre='',$eliminar=true){
    $parametro='';
    if($codigo!=false){
        $parametro='?'.$nombre.'='.$codigo;
    }
    echo '
    <a class="btn-floating btn-large btn-success btn-creacion" href="gestion.php?fijo=SI" title="Nuevo encargado fijo"><i class="icon-plus"></i></a>';
    echo '
    <a class="btn-floating btn-large btn-info btn-creacion2" href="gestion.php?fijo=NO" title="Nuevo encargado"><i class="icon-plus"></i></a>';
    if($eliminar){
    echo '
        <a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
    }
}

function recogeEncargados(){
	$res='<option value="NULL">&nbsp;</option>';
	$datos=arrayFormulario();
	$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
	$servicios=array(158=>'Asesoría laboral para la realización de nóminas',168=>'Asesoría contable/fiscal',178=>'Video vigilancia',189=>'Prevención de riesgos laborales',200=>'Mantenimiento de equipos informáticos',210=>'Mantenimiento de aplicaciones',220=>'Mantenimiento de la página web',230=>'Copias de seguridad',240=>'Transporte/mensajería para envíos a clientes',250=>'Limpieza',260=>'Mantenimiento de instalaciones',270=>'Vigilancia/seguridad',280=>'Mantenimiento de la copiadora',290=>'Agente o colaboración mercantil',487=>'Selección de personal');
	$trabajo=datosRegistro('trabajos',$datos['codigoTrabajo']);
	$formulario=recogerFormularioServicios($trabajo);
	foreach ($encargados as $key => $value) {
		if($formulario['pregunta'.$value]=='NO'){
			$res.='<option value="'.$value.'"';
			$res.='>'.$servicios[$value].'</option>';
		}
	}

	echo $res;
}

function obtieneCheckTablaEncargados($datos,$value){
	$value='fijo_'.$datos['codigo'].'_'.$value;
    $res="<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$value."'></div>";
    return $res;
}

function campoSelectProvinciaAEPD($nombreCampo,$datos){
	$valores=array();
	$nombres=array();
	$provinciasAgencia=array(''=>'','01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	
	foreach ($provinciasAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,convertirMinuscula($nombre));
	}

	campoSelect($nombreCampo,'Provincia',$nombres,$valores,$datos,'selectpicker span3 show-tick');
}

function creaTablaSubContratacion(){
	$tabla='tablaSubcontratacion';
	echo "<br clear='all'>
	<div class='centro divSubcontrata'>
		<br/><h1 style='text-align:left;'>Subcontratación</h1>
		<div class='table-responsive'>
			<table class='table table-striped tabla-simple mitadAncho' id='tablaSubcontratacion'>
			  	<thead>
			    	<tr>
			            <th> Empresa </th>
			            <th></th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;
			  		conexionBD();
			  		if(isset($_REQUEST['fijo']) && isset($_REQUEST['codigo']) && isset($_REQUEST['codigoTrabajo'])){
			  			$tipo=$_REQUEST['fijo']=='SI'?'ENCARGADOFIJO':'ENCARGADO';
		  				$consulta=consultaBD("SELECT * FROM declaraciones_subcontrata WHERE codigoResponsable=".$_REQUEST['codigo']." AND codigoTrabajo=".$_REQUEST['codigoTrabajo']." AND tipo='".$tipo."';");
		  				while($item=mysql_fetch_assoc($consulta)){
		  					$j=$i+1;
		  					campoTextoTabla('empresa'.$i,$item['empresa'],'span6');
		  					echo "</td><td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td></tr>";
		  					$i++;
		  				}
		  			}
			  		

			  		if($i==0){
			  			$j=$i+1;
			  			campoTextoTabla('empresa'.$i,false,'span6');
			  			echo "</td><td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td></tr>";
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"".$tabla."\");'><i class='icon-plus'></i> Añadir empresa</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"".$tabla."\");'><i class='icon-trash'></i> Eliminar empresa</button>
			</div>
		</div>
	</div>";
}

//Fin parte de agrupaciones
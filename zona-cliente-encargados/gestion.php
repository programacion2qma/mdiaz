<?php
  $seccionActiva=69;
  include_once("../cabecera.php");
  gestionEncargados();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>>
<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	$('#codigoTrabajo').change(function(){
		recogerEncargados($(this).val());
	});

	mostrarDivs($('.multipleFicheros option:selected').val());
	$('.multipleFicheros').change(function(){
		mostrarDivs($(this).val());
	});
	mostrarSubcontrata();
	$('#radioSubcontrata input[type=radio]').change(function(){
		mostrarSubcontrata();
	});
});

function recogerEncargados(valor){
	var consulta=$.post('../listadoAjax.php?include=zona-cliente-encargados&funcion=recogeEncargados();',{'codigoTrabajo':valor});
	consulta.done(function(respuesta){
		$('#codigoEncargado').html(respuesta);
		$('#codigoEncargado').selectpicker('refresh');
	});
}

function mostrarDivs(valor){
	if(valor==undefined){
		$('#divTratamientos').addClass('hide');
	} else {
		$('#divTratamientos').removeClass('hide');
	}
}

function mostrarSubcontrata(){
	var valor=$('#radioSubcontrata input[type=radio]:checked').val();
	if(valor=='SI'){
		$('.divSubcontrata').removeClass('hide');
	} else {
		$('.divSubcontrata').addClass('hide');
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
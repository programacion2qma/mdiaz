<?php
include_once('../config.php');
header("Content-type: text/css; charset: UTF-8");
?>

.btn-floating {
  box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
}

.btn-floating:hover {
  box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15);
}


nav ul a.btn-floating {
  margin-top: -2px;
  margin-left: 15px;
  margin-right: 15px;
}



.btn-floating.disabled,
.btn-floating:disabled,
.btn-floating[disabled] {
  background-color: #DFDFDF !important;
  box-shadow: none;
  color: #9F9F9F !important;
  cursor: default;
}

.btn-floating.disabled *,
.btn-floating:disabled *,
.btn-floating[disabled] * {
  pointer-events: none;
}


.btn-floating.disabled:hover,
.btn-floating:disabled:hover,
.btn-floating[disabled]:hover {
  background-color: #DFDFDF !important;
  color: #9F9F9F !important;
}

.btn-floating i,{
  font-size: 1.3rem;
  line-height: inherit;
}

.btn-floating {
  display: inline-block;
  color: #fff;
  position: relative;
  overflow: hidden;
  z-index: 1;
  width: 37px;
  height: 37px;
  line-height: 37px;
  padding: 0;
  background-color: <?php echo $_CONFIG['colorPrincipal'] ?>;
  border-radius: 50%;
  transition: .3s;
  cursor: pointer;
  vertical-align: middle;
}

.btn-floating i {
  width: inherit;
  display: inline-block;
  text-align: center;
  color: #fff;
  font-size: 1rem;
  line-height: 37px;
}

.btn-floating:hover {
  background-color: <?php echo $_CONFIG['colorSecundario'] ?>;
  text-decoration: none;
}

.btn-floating:before {
  border-radius: 0;
}

.btn-floating.btn-large {
  width: 55.5px;
  height: 55.5px;
}

.btn-floating.btn-large i {
  line-height: 55.5px;
}

button.btn-floating {
  border: none;
}

.fixed-action-btn {
  position: fixed;
  right: 23px;
  bottom: 80px;
  padding-top: 15px;
  margin-bottom: 0;
  z-index: 998;
}

.fixed-action-btn.active ul {
  visibility: visible;
}

.fixed-action-btn.horizontal {
  padding: 0 0 0 15px;
}

.fixed-action-btn.horizontal ul {
  text-align: right;
  right: 64px;
  top: 50%;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  height: 100%;
  left: auto;
  width: 500px;
  /*width 100% only goes to width of button container */
}

.fixed-action-btn ul li {
  display: inline-block;
}

.fixed-action-btn ul {
  left: 0;
  right: 0;
  text-align: center;
  position: absolute;
  bottom: 64px;
  margin: 0;
  visibility: hidden;
}

.fixed-action-btn ul li {
  margin-bottom: 15px;
}

.fixed-action-btn ul a.btn-floating {
  opacity: 0;
}

.btn-flat {
  box-shadow: none;
  background-color: transparent;
  color: #343434;
  cursor: pointer;
  transition: background-color .2s;
}

.btn-flat:focus, .btn-flat:active {
  background-color: transparent;
}

.btn-flat:hover {
  background-color: rgba(0, 0, 0, 0.1);
  box-shadow: none;
}

.btn-flat.disabled {
  color: #b3b3b3;
  cursor: default;
}

.btn-floating.btn-large {
  height: 54px;
  line-height: 54px;
}

.btn-floating.btn-large i {
  font-size: 1.6rem;
}

.btn-block {
  display: block;
}

.btn-floating.btn-success{
  background-color:#7eb216;
}

.btn-floating.btn-success:hover{
  background-color:#6e9b13;
}


.btn-floating.btn-danger{
  background-color:#B02B2C;
}

.btn-floating.btn-danger:hover{
  background-color:#bd2b1f;
}

.btn-floating.btn-primary{
  background-color:#21a9ec;
}

.btn-floating.btn-primary:hover{
  background-color:#1399dc;
}


.btn-floating.btn-info{
  background-color:#4eb2d5;
}

.btn-floating.btn-info:hover{
  background-color:#35a7cf;
}


.btn-floating.btn-warning{
  background-color:#f5a732;
}

.btn-floating.btn-warning:hover{
  background-color:#f49a15;
}

.btn-floating.btn-inverse{
  background-color:#555555;
}

.btn-floating.btn-inverse:hover{
  background-color:#464646;
}

.btn-floating.btn-default{
  background-color: #fefefe;
}

.btn-floating.btn-default i{
  color: #333;
}

.btn-floating.btn-default:hover{
  background-color:#f0f0f0;
}
<?php
  include_once("funciones.php");
  if($_GET['tipo']=='aviso'){
    $titulo='Aviso legal';
  } else if($_GET['tipo']=='politica'){
    $titulo='Política de privacidad';
  } else if($_GET['tipo']=='cookies'){
    $titulo='Política de cookies';
  }
?>
  <!DOCTYPE html>
  <html lang="es">
  <head>
  <meta charset="utf-8">
  <title>Muñoz y Díaz Asesores de Calidad, S.L.U.</title>
  <meta name="viewport" content="width=device-width, initial-scale=0.3, maximum-scale=1.0, user-scalable=1">
  <meta name="apple-mobile-web-app-capable" content="yes">

  <link href="<?php echo $_CONFIG['raiz']; ?>css/bootstrap.min.php" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="//fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/font-awesome.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>css/style.php" rel="stylesheet /">

  <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/datepicker.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/bootstrap-select.css" rel="stylesheet" />
  <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/bootstrap-wysihtml5.css" rel="stylesheet" />
  
  <link href="<?php echo $_CONFIG['raiz']; ?>../../api/css/animate.css" rel="stylesheet" type="text/css" />

  <link href="<?php echo $_CONFIG['raiz']; ?>../../api/js/guidely/guidely.css" rel="stylesheet" />

  <!-- Script HTML5, para el soporte del mismo en IE6-8 -->
  <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->


  <!-- Común -->
  <script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery-1.7.2.min.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/controladorAJAX.js" type="text/javascript"></script>
  <!-- Fin común -->


  </head>
  <body>
  <div class="navbar navbar-fixed-top cabeceraCuestionario">
    <div class="navbar-inner">
      <div class="container"> 
        <span class="logo2">
          <img src="<?php echo $_CONFIG['raiz']; ?>img/logo2.png" alt="<?php echo $_CONFIG['altLogo']; ?>"> &nbsp; Muñoz y Díaz Asesores de Calidad, S.L.U.</span>
        </span>
      </div>
    </div>
  </div>
  <!-- /navbar -->
  <div class="subnavbar">
  
  </div>
  
<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales




function texto($tipo){
	abreVentanaCuestionario();
	if($tipo=='aviso'){
		echo '<br/><h1 style="text-align:center;">AVISO LEGAL</h1><br/>';
		echo '<p><b>CONDICIONES GENERALES DE ACCESO Y USO DEL SITIO WEB:</b></p>';
		echo '<p>Este apartado incluye información relativa a las condiciones generales de acceso y uso del sitio web, que deben ser conocidas por el usuario, a los efectos previstos en la Ley 34/2002, de 11 de julio, de Servicios de la Sociedad de la Información y de Comercio Electrónico.<br/><br/>
			Datos del titular: M&D<br/><br/>
			NOMBRE COMERCIAL: UVED-M&D Asesores<br/>
			DENOMINACIÓN SOCIAL: Muñoz y Díaz asesores de calidad, S.L.U. (M&D)<br/>
			C.I.F.: B92598101<br/>
			DOMICILIO SOCIAL: C/ Cuesta de los Rojas, 8, 3ºB, (29200) Antequera (Málaga)<br/>
			TELÉFONO: 952 70 50 50<br/>
			DIRECCION DE CORREO ELECTRÓNICO: <a href="mailto:asesoresmunozydiaz@gmail.com">asesoresmunozydiaz@gmail.com</a><br/>
			Datos registrales: Reg. M. Málaga, Tomo 3714, Libro 2625, Folio 210, Inscripción3ª, Hoja MA 74725.</p>';
		echo '<p><b>Condiciones de acceso:</b></p>';
		echo '<p>El  acceso  a  este  sitio  web  es  responsabilidad  exclusiva  de  los  usuarios  y  supone  que conocen  y  aceptan  las  advertencias  legales,  las  condiciones  y  los  términos  de  uso  que contiene.<br/><br/>
			El  Usuario es  el  único  responsable  frente  a  cualquier  reclamación  o  acción  legal, judicial  o  extrajudicial,  iniciada  por  terceras  personas,  basada  en  la  utilización  por  el Usuario del Servicio.<br/><br/>
			Si  el  usuario  no  estuviera  de  acuerdo  con  el  contenido  de  las  presentes  condiciones generales  de  navegación,  deberá  abandonar  el  sitio  web  y  no  podrá  acceder  a  él  ni disponer de los servicios que ofrece.<br/><br/>
			El  usuario  garantiza  la  veracidad  y  la  autenticidad  de  la  información  comunicada  a consecuencia  de  los  servicios  que  ofrece  M&D para  la  comunicación  de  quejas reclamaciones  y/o  sugerencias  y  se  obliga,  asimismo,  a  mantener  esta  información actualizada.<br/><br/>
			M&D puede modificar, en cualquier momento que lo estime oportuno, la configuración de  este  sitio  web,  las  condiciones  del  servicio  y  su  contenido,  así  como  eliminarlos, limitarlos o suspenderlos de manera temporal o definitiva, o impedir el acceso a este, y procurar informar al usuario de este cambio, siempre que las circunstancias lo permitan, mediante una publicación en el sitio web.</p>';
		echo '<p><b>Condiciones de uso:</b></p>';
		echo '<p>El Usuario se compromete a utilizar el sitio weby los Servicios de conformidad con la ley, el presente Aviso Legal, las Condiciones Particulares de algunos Servicios y con la moral,    las    buenas    costumbres    generalmente    aceptadas y    el    orden    público. El  Usuario  se  abstendrá  de  utilizar  cualquiera  de  los  Servicios  con  fines  o  efectos ilícitos,  prohibidos  en  el  presente  Aviso  Legal,  lesivos  de  los  derechos  e  intereses  de terceros  o  que  de  cualquier  forma  puedan  dañar,  inutilizar,  sobrecargar,  deteriorar  o impedir   la   normal   utilización   de   los   Servicios,   los   equipos   informáticos   o   los documentos,  archivos  y  toda  clase  de  contenidos  almacenados  en  cualquier  equipo informático  (hacking)  de M&D,  de  otros  Usuarios  o  de  cualquier  Usuario  de  Internet (hardware  y  software).  A  título  meramente  indicativo  y  no  exhaustivo,  el  Usuario  se compromete  a  no  transmitir,  difundir  o  poner  a  disposición  de  terceros  informaciones, datos,   contenidos,   mensajes,   gráficos,   dibujos,   archivos   de   sonido   y/o   imagen, fotografías, grabaciones, software y, en general, cualquier clase de material que;</p>';
		echo '<ol>';
		echo '<li>Sea  contrario  o  atente  contra  los  derechos  fundamentales  y  las  libertades  públicas reconocidas constitucionalmente o en el resto de legislación.</li>
			<li>Sea  contrario  al  derecho  al  honor,  a  la  intimidad  personal  y  familiar  o  a  la  propia imagen de las personas.</li>
			<li>Constituya  publicidad  ilícita,  engañosa  o  desleal  o  de  cualquier  forma  constituya competencia desleal.</li>
			<li>Incorpore virus u otros elementos físicos o electrónicos que puedan dañar o impedir el normal funcionamiento de la red, del sistema o de equipos informáticos (hardware  y software)  de M&D o  de  terceros  o  que  puedan  dañar  los  documentos  electrónicos  y archivos almacenados en dichos equipos informáticos.</li>
			<li>Provoque  dificultades  en  el  normal  funcionamiento  del  servicio.Incite,  induzca  o promueva  actuaciones  delictivas,  denigratorias,  infamantes,  violentas  o  contrarias  a  la ley, a la moral, a las buenas costumbres o al orden público.</li>
			<li>Incite,  induzca  o  promueva  actuaciones  o  actitudes  discriminatorias  por  razón  de sexo, raza, religión, creencias, edad o condición.</li>
			<li>Ponga  a  disposición,  incorpore  o  permita  acceder  a  productos,  elementos,  mensajes y/o  servicios  delictivos,  violentos,  ofensivos  o  contrarias  a  la ley,  a  la  moral,  a  las buenas costumbres o al orden público.</li>
			<li>Se   encuentre   protegido   por   derechos   de   propiedad   intelectual   o   industrial pertenecientes a terceros, sin que el Usuario haya obtenido previamente de sus titulares la autorización necesaria para llevar a cabo el uso que efectúa o pretende efectuar.</li>
			<li>Viole los derechos empresariales de terceros.</li>';
		echo '</ol>';
		echo '<p><b>PROPIEDAD INTELECTUAL E INDUSTRIAL:</b></p>';
		echo '<p>Todos  los  elementos  que  integran  el  diseño  gráfico  de  la  web,  los  menús,  botones  de navegación,  el  código  HTML,  y,  en  general,  todos  los  contenidos  e  información  a  los que  el  Usuario  pueda  acceder  a través  de  las  páginas  web  de M&D están  sujetos  a derechos de propiedad industrial e intelectual, patentes, marcas o copyright de M&D o de  terceros  titulares  de  los  mismos.  Queda  expresamente  prohibida  para  el  Usuario  la alteración,   modificación,   explotación,   reproducción,   distribución   o   comunicación pública o cualquier otro derecho que corresponda al titular del derecho afectado</p>';
		echo '<p><b>EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDADES;</b></p>';
		echo '<p>M&D no  garantiza  la  disponibilidad  y  continuidad  del  funcionamiento  del  Servicio,  ni su  utilidad  para  la  realización  de  ninguna  actividad  en  particular,  ni  su  infalibilidad  y calidad.<br/><br/>
			M&D excluye  cualquier  responsabilidad  por  los  daños  y  perjuicios  de  toda  naturaleza que  puedan  deberse  a  la  falta  de  disponibilidad,  de  continuidad  o  de  calidad del funcionamiento del servicio.<br/><br/>
			M&D excluye  cualquier  responsabilidad  por  los  daños  y  perjuicios  de  toda  naturaleza que  puedan  deberse  a  la  presencia  de  virus  o  a  la  presencia  de  otros  elementos  en  los contenidos  que  puedan  producir  alteraciones  en  el  sistema  informático,  documentos electrónicos o ficheros de los Usuarios.<br/><br/>
			M&D excluye  cualquier  responsabilidad por  los  daños  y  perjuicios  de  toda  naturaleza que  pudieran  causarse  por  los  Usuarios  en  la  transmisión,  difusión,  almacenamiento, puesta  a  disposición,  recepción,  obtención  o  acceso  a  los  contenidos  y,  en  particular, aunque no exclusivamente, por los daños y perjuicios que puedan deberse a:</p>';
		echo '<ul>';
		echo '<li>El incumplimiento de la ley, moral o buenas costumbres generalmente aceptadas o el orden público como consecuencia de la transmisión, difusión, almacenamiento, puesta a disposición, recepción, obtención o acceso a los contenidos.</li>
			<li>La  infracción  de  los  derechos  de  propiedad  industrial  o  intelectual,  de  los  secretos empresariales,  de  compromisos  contractuales  de  cualquier  clase,  de  los  derechos  al honor, a la intimidad personal y familiar y a la imagen de las personas, delos derechos de  propiedad  y  de  toda  otra  naturaleza  pertenecientes  a  un  tercero  como  consecuencia de la transmisión, difusión, almacenamiento, puesta a disposición, recepción, obtención o acceso a los contenidos.</li>
			<li>La realización de actos de competencia desleal y publicidad ilícita como consecuencia de la transmisión, difusión, almacenamiento, puesta a disposición, recepción, obtención o acceso a los contenidos.</li>
			<li>Falta  de  veracidad,  exactitud,  exhaustividad,  pertenencia  y/o  actualidad  de  los contenidos.</li>
			<li>La  suplantación  de  la  personalidad  de  un  tercero  efectuada  por  un  Usuario  en cualquier clase de comunicación realizada a través del servicio.</li>
			<li>El   incumplimiento,   retraso   en   el   cumplimiento,   cumplimiento   defectuoso   o terminación por cualquier causa de las obligaciones contraídaspor terceros  y contratos realizados con terceros a través de o con motivo del acceso a los contenidos.</li>
			<li>Los  vicios  y  defectos  de  toda  clase  de  los  contenidos  transmitidos,  difundidos, almacenados,   puestos   a   disposición   o   de   otra   forma   transmitidos   o   puestos   a disposición, recibidos, obtenidos o a los que se haya accedido a través de los servicios.</li>';
		echo '</ul>';
		echo '<p><b>LEGISLACIÓN APLICABLE:</b></p>';
		echo 'El presente Aviso Legal se rige en todos y cada uno de sus extremos por la ley española.';
		echo '<p><b>JURISDICCIÓN:</b></p>';
		echo '<p>Las  partes se  someten  expresamente  a  los  Juzgados  y  Tribunales  de  Málaga, con renuncia expresa  a  cualquier  otro  fuero  que  pudiera  corresponderles,  para  dirimir cualquier  controversia  derivada  del  acceso  y  uso  de  este  sitio  web,  sus  contenidos  y/o servicios,  así  como  dela  interpretación  y cumplimiento  de  las  presentes  condiciones generales y de cualquier otro texto de carácter contractual contenido en esta web.</p>';

	} else if($tipo=='politica'){
		echo '<br/><h1 style="text-align:center;">POLITICA DE PRIVACIDAD</h1><br/>';
		echo '<p><b>Identidad y datos de contacto del Responsable del Tratamiento:</b></p>';
		echo '<ul>';
		echo '<li><b>Responsabledel Tratamiento:</b> MUÑOZ Y DÍAZ ASESORES DE CALIDAD, S.L.U. (M&D)</li>
			<li><b>NIF/CIF:</b> B92598101</li>
			<li><b>Sector o Actividad:</b> Prevención del blanqueo de capitales, Ley Orgánica de Protección de Datos, Calidad y Medio Ambiente</li>
			<li><b>Domicilio  fiscal:</b> C/  La  Granja  1,Los  tejares-Huertas  del  Rio,  (29314)Archidona (Málaga)</li>
			<li><b>Teléfono de contacto:</b> 952705050</li>
			<li><b>Correo electrónico:</b> asesoresmunozydiaz@gmail.com</li>';
		echo '</ul>';
		echo '<p style="text-align:center;"><b>INFORMACIÓN SOBRE PROTECCIÓN DE DATOS</b></p>';
		echo '<p>En  cumplimiento  de  lo  establecido  por  el  Reglamento  General  de  Protección  de  Datos (Reglamento  2016/679  del  Parlamento  Europeo  y  del  Consejo  de  27  de  abril  de  2016),  se  le informa de los siguientes extremos</p>';
		echo '<ul>';
		echo '<li><b>FINES  DEL   TRATAMIENTO:</b> Los  datos  personales  que   facilite  al  Responsable  del Tratamiento  se  incorporarán  a  los    ficheros de  éste y  serán  tratados  exclusivamente para la gestión de cualquier petición, reclamación, queja o sugerencia que realice ante el mismo.</li>
			<li><b>CRITERIOS DE CONSERVACIÓN DE LOS DATOS:</b> Los datos personales que proporcione se  conservarán  mientras  sean  necesarios  o  pertinentes  para  la  finalidad  para  la  que han sido recabados o para el cumplimiento de las obligaciones legales del Responsable del  Tratamiento. Al  respecto, consideramos  que  si  no  se  opone  al  tratamiento de  sus datos  y/o  no  los  cancela  expresamente,  continúa  interesado  en  seguir  incorporado  a nuestros tratamientos hasta que M&D lo considere oportuno y mientras sea adecuado a la finalidad para la que se obtuvieron.</li>
			<li><b>LEGITIMACIÓN:</b> La  base  legal  para  el  tratamiento  de  sus  datos  personales  es  el consentimiento  que  presta  al  aceptar  esta  Política  de  Privacidad  antes  de  enviarnos/facilitarnos  sus  datos. Los  datos  que  le  solicitamos  son  adecuados,  pertinentes  y estrictamente  necesarios  y  en  ningún  caso  está  obligado  a  facilitárnoslos,  pero  su  no comunicación   podrá   afectar gestión   de   cualquier   petición,  reclamación,   queja   o sugerencia que realice ante el mismo.</li>
			<li><b>DESTINATARIOS:</b> No se cederán datos a terceros, salvo obligación legal o autorización expresa y por escrito del interesado o su representante legal.</li>
			<li><b>DERECHOS:</b> Puede ejercer los derechos de acceso, oposición, rectificación, supresión o cancelación  de  sus  datos  así  como  solicitar  la  limitación  de  su tratamiento y  la portabilidad  de  los  mismos,  mediante  escrito  firmado  y  dirigido  por  correo  postal al Responsable del Tratamiento al domicilio indicado en el encabezamiento del presente documento   o   mediante   correo   electrónico   a   la   dirección   de   correo   electrónico indicada en el encabezamiento del presente documento.</li>
			<li>El  escrito  por  el  que  se  ejercite  alguno  de  los  derechos  referenciados  en  el  párrafo anterior   deberá   contener:   nombre   y   apellidos   del   interesado;   fotocopia   de   su documento  nacional  de  identidad, o  de  su  pasaporte  u otro  documento válido que  lo identifique y, en su caso, de la persona que lo represente, o instrumentos electrónicos equivalentes; documento o instrumento electrónico acreditativo de tal representación.  La  utilización  de  firma  electrónica  identificativa  del  solicitante  exime de la presentación de las fotocopias del DNI o documento equivalente.</li>
			<li>El  usuario  también  tiene  derecho  a  presentar  una  reclamación  ante  la  Agencia Española de Protección de Datos, a través de la página web oficial de la Agencia.</li>';
		echo '</ul>';
	} else  {
		echo '<br/><h1 style="text-align:center;">POLÍTICA DE COOKIES</h1><br/>';
		echo '<p><b>QUÉ SON LAS COOKIES Y PARA QUÉ SE UTILIZAN:</b></p>';
		echo '<p>Las cookies son archivos o dispositivos que se descargan en el equipo de un usuario al navegar en un sitio web.<br/><br/>
			Las cookies son herramientas que tienen un papel esencial para la prestación de numerosos servicios de la sociedad de la información, ya que facilitan la navegación por internet, concentran la mayor inversión publicitaria y ofrecen una publicidad basada en ocasiones en los hábitos de navegación.</p>';
		echo '<p><b>TIPOS DE COOKIES QUE UTILIZAMOS EN ESTA WEB:</b></p>';
		echo '<ul>';
		echo '<li>Según la entidad que las gestiona:
			<ul style="list-style-type:square;">
			<li>Cookies propias: Son aquellas que enviamos al equipo del usuario desde un equipo o dominio gestionado por <b>MUÑOZ Y DIAZ ASESORES DE CALIDAD, S.L.U.</b> y desde el que se presta el servicio solicitado por el usuario.</li>
			</ul></li>';
		echo '<li>Según el plazo de tiempo que permanecen activadas:
			<ul style="list-style-type:square;">
			<li>Cookies de sesión: Son aquellas diseñadas para recabar y almacenar datos mientras el usuario accede a la página web.</li>
			<li>Cookies persistentes: Tipo de cookies en el que los datos siguen almacenados en el terminal y pueden ser accedidos y tratados durante un periodo definido por elresponsable de la cookie, que puede ir de unos minutos a varios años.</li>
			</ul></li>';
		echo '<li>Según su finalidad:
			<ul style="list-style-type:square;">
			<li>Cookies técnicas: Son aquellas que permiten al usuario la navegación a través de una página web, plataforma o aplicación y la utilización de las diferentes opciones o servicios que en ella existan.</li>
			<li>Cookies de personalización: Son aquéllas que permiten al usuario acceder al servicio con algunas características de carácter general predefinidas en función de una serie de criterios en el terminal del usuario como por ejemplo serían el idioma, el tipo de navegador a través del cual accede al servicio, etc.</li>
			</ul></li>';
		echo '</ul>';
		echo '<p><b>ACEPTACIÓN DE COOKIES</b></p>';
		echo '<ul>';
		echo '<li>Si pulsa el botón "Aceptar política de cookies" situado en la parte inferior que aparece en el presente aviso, relativo a información y aceptación de cookies, estará aceptando la instalación de cookies en su equipo.</li>';
		echo '<li>Si no pulsa el botón "Aceptar política de cookies" o si pulsa el botón “Rechazar política de cookies” situados en la parte inferior que aparece en el presente aviso, es posible que no pueda acceder al espacio web o que no le sea posible la utilización total o parcial del servicio por lo que, si se da esta circunstancia, le ofrecemos como alternativa el acceso al servicio que requiera mediante correo electrónico dirigido a la siguiente dirección: <b><a href="mailto:asesoresmunozydiaz@gmail.com">asesoresmunozydiaz@gmail.com</a></b>. Los datos que incluya en el correspondiente correo electrónico se tratarán por <b>MUÑOZ Y DIAZ ASESORES DE CALIDAD, S.L.U.</b> en concepto de Responsable del Tratamiento, para gestionar y atender su solicitud, en base al consentimiento que debe otorgarnos de forma expresa en dicho correo, mediante el texto "AUTORIZO EXPRESAMENTE TRATEN MIS DATOS PERSONALES INCLUIDOS EN EL PRESENTE CORREO PARA ATENDER LA SOLICITUD QUE REALIZO MEDIANTE EL MISMO"; en el supuesto que no incluya dicho texto, no podremos gestionar su solicitud y o bien eliminaremos su correo o le solicitaremos su autorización para el tratamiento de sus datos por la misma vía (correo electrónico). Si desea obtener más información sobre el tratamiento de sus datos personales, puede solicitárnosla por correo electrónico a la dirección <b><a href="mailto:asesoresmunozydiaz@gmail.com">asesoresmunozydiaz@gmail.com</a></b>.</li>';
		echo '</ul>';
		echo '<p><b>RESTRICCIÓN, BLOQUEO O ELIMINACIÓN DE COOKIES YA INSTALADAS</b></p>';
		echo '<ul>';
		echo '<li>Puede restringir, bloquear o eliminar las cookies instaladas en su equipo mediante la configuración de las opciones del navegador que tenga instalado en su equipo.</li>';
		echo '<li>En caso que no permita la instalación de cookies en su navegador, es posible que no pueda acceder a nuestras utilidades.</li>';
		echo '<li>Si utiliza el navegador Internet Explorer, puede obtener información sobre cómo bloquear o eliminar el uso de cookies en <a href="http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10" target="_blank">http://windows.microsoft.com/es-xl/internet-explorer/delete-manage-cookies#ie="ie-10</a></li>';
		echo '<li>Si utiliza el navegador Chrome, puede obtener información sobre cómo bloquear o eliminar el uso de cookies en <a href="http://support.google.com/chrome/answer/95647?hl=es" target="_blank">http://support.google.com/chrome/answer/95647?hl=es</a></li>';
		echo '<li>Si utiliza el navegador Firefox, puede obtener información sobre cómo bloquear o eliminar el uso de cookies en <a href="http://support.mozilla.org/es/kb/Borrar%20cookies" target="_blank">http://support.mozilla.org/es/kb/Borrar%20cookies</a></li>';
		echo '</ul>';
		echo '<br/><div id="divCookie" align="center"><a class="btn btn-success" href="javascript:void(0);" onclick="aceptaCookies();"><i class="fa fa-check-circle"></i> Aceptar política de cookies</a> <a class="btn btn-danger" href="javascript:void(0);" onclick="rechazaCookies();"><i class="fa fa-check-circle"></i> Rechazar política de cookies</a></div>';
	}
	cierraVentanaCuestionario();
}



function cierraVentanaCuestionario(){
	echo "
	      </fieldset>
	    </form>
	    </div>
	</div>";
}


function abreVentanaCuestionario(){
	echo '<div class="widget">
            <div class="widget-content" style="padding:10px;">
              <div class="tab-pane" id="formcontrols">
                <form id="edit-profile" class="form-horizontal" action="?" method="post">
                  <fieldset style="text-align:justify;">';
}

//Fin parte de satisfacción de clientes
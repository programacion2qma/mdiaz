<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
         <div class="span12"> 
          Muñoz y Díaz Asesores de Calidad, S.L.U. &copy; <?php echo date('Y').' '.$_CONFIG['textoPie']; ?>     
        </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 

<!-- Javascript
================================================== -->
<script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>

<script src="../js/bootstrap.js" type="text/javascript"></script>
<script src="../js/base.js" type="text/javascript"></script>
<script src="../js/bootstrap-datepicker.js" type="text/javascript"></script>

</body>
</html>

<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesInformes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizar();
	}
	elseif(isset($_POST['codigoInformeOriginal'])){
		$res=renovar();
	}
	elseif(isset($_POST['fecha'])){
		$res=insertar();
	}
	elseif(isset($_POST['codigoClienteImportar'])){
		$res=importarDatos();
	}
	elseif(isset($_POST['servicio0'])){
		$res=asignarServicios();
	}
	elseif(isset($_GET['codigoFinalizar'])){
		$res=finalizarInforme();
	}
	elseif(isset($_GET['codigoRenovacion'])){
		$res=actualizarRenovacion();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('informes_clientes');
	}

	mensajeResultado('referencia',$res,'Informe de cliente');
    mensajeResultado('elimina',$res,'Informe de cliente', true);
}

function actualizarRenovacion(){
	$res=true;
	$res=consultaBD("UPDATE informes_clientes SET renovada='".$_GET['respuesta']."' WHERE codigo=".$_GET['codigoRenovacion'],true);
	return $res;
}

function renovar(){
	$res=true;
	$res=duplicarInforme();
	$res=insertaDatos('informes_renovaciones');
	if($_POST['tipoRenovacion']=='PARTIR'){
		echo '<script type="text/javascript">
  				window.location="gestion.php?codigo='.$_POST['codigoInformeNuevo'].'";
			</script>';
	}
	return $res;
}

function duplicarInforme(){
	$res=true;
	
	$informe=datosRegistro('informes_clientes',$_POST['codigoInformeOriginal']);
	duplicarRegistro('informes_clientes',$informe);
	$_POST['referencia']=generaNumeroReferencia('informes_clientes','referencia','codigoUsuario='.$informe['codigoUsuario']);
	$_POST['fecha']=$_POST['fechaRenovacion'];
	$_POST['renovada']='PENDIENTE';
	$_POST['codigoExcel']='NULL';
	$res=insertaDatos('informes_clientes');
	$codigoInformeNuevo = consultaBD("SELECT codigo FROM informes_clientes ORDER BY codigo DESC LIMIT 1;", true, true);
	$_POST['codigoInformeNuevo'] = $codigoInformeNuevo['codigo'];

	$personas=consultaBD('SELECT * FROM informes_clientes_fisicas WHERE codigoInforme='.$_POST['codigoInformeOriginal'],true);
	while($persona=mysql_fetch_assoc($personas)){
		duplicarRegistro('informes_clientes_fisicas',$persona);
		$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
		insertaDatos('informes_clientes_fisicas');
	}

	$personas=consultaBD('SELECT * FROM informes_clientes_juridicas WHERE codigoInforme='.$_POST['codigoInformeOriginal'],true);
	while($persona=mysql_fetch_assoc($personas)){
		duplicarRegistro('informes_clientes_juridicas',$persona);
		$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
		insertaDatos('informes_clientes_juridicas');
	}

	$personas=consultaBD('SELECT * FROM personas_fisicas_informes_clientes WHERE codigoInforme='.$_POST['codigoInformeOriginal'],true);
	while($persona=mysql_fetch_assoc($personas)){
		duplicarRegistro('personas_fisicas_informes_clientes',$persona);
		$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
		insertaDatos('personas_fisicas_informes_clientes');
	}

	$personas=consultaBD('SELECT * FROM personas_juridicas_informes_clientes WHERE codigoInforme='.$_POST['codigoInformeOriginal'],true);
	while($persona=mysql_fetch_assoc($personas)){
		duplicarRegistro('personas_juridicas_informes_clientes',$persona);
		$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
		insertaDatos('personas_juridicas_informes_clientes');
	}

	$personas=consultaBD('SELECT * FROM personas_sinjuridicas_informes_clientes WHERE codigoInforme='.$_POST['codigoInformeOriginal'],true);
	while($persona=mysql_fetch_assoc($personas)){
		duplicarRegistro('personas_sinjuridicas_informes_clientes',$persona);
		$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
		insertaDatos('personas_sinjuridicas_informes_clientes');
	}

	$personas=consultaBD('SELECT * FROM personas_fideocomisos_informes_clientes WHERE codigoInforme='.$_POST['codigoInformeOriginal'],true);
	while($persona=mysql_fetch_assoc($personas)){
		duplicarRegistro('personas_fideocomisos_informes_clientes',$persona);
		$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
		insertaDatos('personas_fideocomisos_informes_clientes');
	}

	$servicios=datosRegistro('servicios_informes_clientes',$_POST['codigoInformeOriginal'],'codigoInforme');
	duplicarRegistro('servicios_informes_clientes',$servicios);
	$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
	insertaDatos('servicios_informes_clientes');

	$comprobaciones=datosRegistro('comprobaciones_informes_clientes',$_POST['codigoInformeOriginal'],'codigoInforme');
	duplicarRegistro('comprobaciones_informes_clientes',$comprobaciones);
	$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
	insertaDatos('comprobaciones_informes_clientes');

	$personas=consultaBD('SELECT * FROM firmado_informes_clientes WHERE codigoInforme='.$_POST['codigoInformeOriginal'],true);
	while($persona=mysql_fetch_assoc($personas)){
		duplicarRegistro('firmado_informes_clientes',$persona);
		$_POST['codigoInforme']=$_POST['codigoInformeNuevo'];
		insertaDatos('firmado_informes_clientes');
	}

	return $res;
}

function duplicarRegistro($tabla,$item){
	conexionBD();
	$campos=camposTabla($tabla);
	cierraBD();
	foreach($campos as $campo){
		if(substr_count($campo,'fecha')==1){//Para formatear la fecha a almacenar
			$_POST[$campo]=formateaFechaWeb($item[$campo]);
		} else {
			$_POST[$campo]=$item[$campo];
		}
	}
}

function importarDatos(){
	$res = true;
	$mensaje = '';
	$_POST['excel'] = subeDocumento('importado',time(),'documentos');
	
	if($_POST['excel'] != 'NO'){
		copy('documentos/'.$_POST['excel'], 'documentos/importacion.xlsm');
	} else {
		$res = false;
		$mensaje = 'Error al subir el fichero';
	}

	$_POST['codigoCliente'] = $_POST['codigoClienteImportar'];
	$_POST['fecha'] = fecha();

	$res = $res && insertaDatos('informes_clientes_excel');
	if($res){
		$codigoExcel = ultimoRegistro('informes_clientes_excel', true);
	}

	$cliente=datosRegistro('usuarios_clientes',$_POST['codigoClienteImportar'],'codigoCliente');

	require_once('../../api/phpexcel/PHPExcel.php');
	require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
	require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');

	$objReader = new PHPExcel_Reader_Excel2007();
	$objPHPExcel = $objReader->load("documentos/importacion.xlsm");
	$objPHPExcel->setActiveSheetIndex(0);
	$i=3;
	$documentos = array(
		''                                   => 'DNI',
		'dni'                                => 'DNI',
		'pasaporte'                          => 'PASAPORTE',
		'tarjeta de residencia'              => 'RESIDENCIA',
		'tarjeta de identidad de extranjero' => 'EXTRANJERO',
		'tarjeta oficial de identidad personal expedido por las autoridades de origen' => 'ORIGEN',
		'documento de identidad expedido por el ministerio de asuntos exteriores de cooperacion' => 'EXTERIORES',
		'otro' => 'OTROS'
	);
	$actividades=array(''=>'SOCIO','socio'=>'SOCIO','empresario individual'=>'EMPRESARIO','profesional ejerciente'=>'PROFESIONAL','trabajador por cuenta ajena'=>'TRABAJADOR','jubilado'=>'JUBILADO','pensionista'=>'PENSIONISTA','desempleado'=>'DESEMPLEADO','cargo publico'=>'CARGO','sin actividad laboral'=>'SIN');
	$sino = array(
		''   => 'NO',
		'Si' => 'NO',
		'si' => 'NO',
		'No' => 'SI',
		'no' => 'SI'
	);

	$checkServicios=array(''=>'','Auditoria de cuentas'=>1,
		'Contabilidad externa'=>2,
		'Asesoria fiscal' => 3,
		'asesoria fiscal' => 3,
		'Concepcion, realizacion o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de bienes inmuebles'=>4,
		'Concepcion, realizacion o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de entidades comerciales'=>5,
		'Gestion de fondos, valores u otros activos'=>6,
		'Apertura y/o gestion de cuentas corrientes, cuentas de ahorros o cuentas de valores'=>7, 
		'Organizacion de aportaciones para la creacion, funcionamiento o gestion de empresas'=>8,
		'Creacion, funcionamiento o gestion de fideicomisos (trusts), sociedades o estructuras analogas'=>9,
		'Constitucion de sociedad u otra persona juridica'=>10,
		'Direccion o secretaria de sociedad, socio de una asociacion, funciones similares con otras personas juridicas'=>11,
		'Facilitar un domicilio social o una direccion comercial, postal, administrativa y otros servicios afines a una sociedad, asociacion o cualquier otro instrumento o persona juridicos'=>12, 
		'Ejercer funciones de fideicomisario en un fideicomiso expreso o instrumento juridico similar'=>13,
		'Disponer que otra persona ejerza funciones de fideicomisario en un fideicomiso expreso o instrumento juridico similar'=>14, 
		'Ejercer funciones de accionista por cuenta de otra persona'=>15,
		'Disponer que persona ha de ejercer las funciones de accionista'=>16,
		'Actuacion por cuenta de cliente en operación financiera'=>17,
		'Actuacion por cuenta de cliente en operación inmobiliaria'=>18);

	$codigoUsuario=$cliente['codigoUsuario'];
	$sale=false;
	$pais=74;
	while(!$sale && $res){
		if($objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue()!=''){
			$codigoFisica=0;
			$codigoJuridica=0;
			$anio=date('Y');
			$usuario=datosRegistro('usuarios_clientes',$_POST['codigoCliente'],'codigoCliente');
			$referencia=generaNumeroReferencia('informes_clientes','referencia','codigoUsuario='.$usuario['codigoUsuario']);
			$fecha=fechaBD();
			$nombre=$objPHPExcel->getActiveSheet()->getCell('A'.$i)->getValue();
			$nombre.=' '.$objPHPExcel->getActiveSheet()->getCell('B'.$i)->getValue();
			$nombre.=' '.$objPHPExcel->getActiveSheet()->getCell('C'.$i)->getValue();
			$tipoDocumento = strtolower($objPHPExcel->getActiveSheet()->getCell('D'.$i)->getValue());
			$tipoDocumento=$documentos[$tipoDocumento];
			$numeroDocumento=$objPHPExcel->getActiveSheet()->getCell('E'.$i)->getValue();
			$actividad=$objPHPExcel->getActiveSheet()->getCell('F'.$i)->getValue();
			$actividad=$actividades[$actividad];
			$presencial = strtolower($objPHPExcel->getActiveSheet()->getCell('G'.$i)->getValue());
			$presencial=$sino[$presencial];
			$servicio=$objPHPExcel->getActiveSheet()->getCell('H'.$i)->getValue();
			$servicio=utf8_decode($servicio);
			if($servicio == '' || $servicio == 'En nombre propio'){
				$servicio=1;
			}
			elseif($servicio == 'En nombre propio y de 3/s' || substr($servicio,0,-4) == 'En nombre propio y de 3'){
				$servicio=2;
			}
			elseif($servicio == 'En representacion de tercero/s'){
				$servicio=3;
			}
			$nombreJuridica=$objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getValue();
			$nombreJuridica.=' '.$objPHPExcel->getActiveSheet()->getCell('R'.$i)->getValue();
			$nombreJuridica.=' '.$objPHPExcel->getActiveSheet()->getCell('S'.$i)->getValue();
			$tipoDocumentoJuridica=$objPHPExcel->getActiveSheet()->getCell('T'.$i)->getValue();
			$tipoDocumentoJuridica=$documentos[$tipoDocumentoJuridica];
			$numeroDocumentoJuridica=$objPHPExcel->getActiveSheet()->getCell('U'.$i)->getValue();
			$actividadJuridica=$objPHPExcel->getActiveSheet()->getCell('V'.$i)->getValue();
			$actividadJuridica=$actividades[$actividadJuridica];
			$sql="INSERT INTO informes_clientes VALUES(NULL, '".$anio."', '".$referencia."', '".$fecha."', '".$nombre."', '".$tipoDocumento."', '".$numeroDocumento."', '', '', '', '', '".$pais."', '".$actividad."', 'NO', '".$presencial."', '".$servicio."', '', 'NO', 'NO', '".$nombreJuridica."', '".$tipoDocumentoJuridica."', '".$numeroDocumentoJuridica."', '', '', '', '', '".$pais."', '".$actividadJuridica."', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', '','', '0000-00-00', 'SIN', '', 'NO', '".$codigoUsuario."', '1', 'NO', '0000-00-00', 'PENDIENTE', ".$codigoExcel.", '', '', '', '', '0000-00-00');";
			$res = $res && consultaBD($sql,true);
			if($res) {	
				$codigoInforme=consultaBD("SELECT codigo FROM informes_clientes ORDER BY codigo DESC LIMIT 1",true,true);
				$codigoInforme=$codigoInforme['codigo'];
				$res = $res && consultaBD('INSERT INTO servicios_informes_clientes(codigo,codigoInforme) VALUES (NULL,'.$codigoInforme.')',true);
				$res = $res && consultaBD("INSERT INTO informes_clientes_fisicas VALUES (NULL, '".$codigoInforme."', '".$nombre."','".$tipoDocumento."', '".$numeroDocumento."', '', '', '', '', '".$pais."', '".$actividad."', 'NO', '".$presencial."', '".$servicio."', '', '');", true);
				if(!$res){
					$mensaje = 'Error 2 en la insercción de registros';
				}
			} else {
				$mensaje = 'Error 1 en la insercción de registros';
			}
		} else {
			$sale=true;
		}
		if($objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue()!='' && $res){
			$sale=false;
			$res = $res && consultaBD('UPDATE informes_clientes SET checkPersonasFisicas="SI" WHERE codigo='.$codigoInforme,true);
			$nombre=$objPHPExcel->getActiveSheet()->getCell('I'.$i)->getValue();
			$nombre.=' '.$objPHPExcel->getActiveSheet()->getCell('J'.$i)->getValue();
			$nombre.=' '.$objPHPExcel->getActiveSheet()->getCell('K'.$i)->getValue();
			$tipoDocumento=$objPHPExcel->getActiveSheet()->getCell('L'.$i)->getValue();
			$tipoDocumento=$documentos[$tipoDocumento];
			$numeroDocumento=$objPHPExcel->getActiveSheet()->getCell('M'.$i)->getValue();
			$actividad=$objPHPExcel->getActiveSheet()->getCell('N'.$i)->getValue();
			$actividad=$actividades[$actividad];
			$sql="INSERT INTO personas_fisicas_informes_clientes VALUES (NULL, '".$codigoInforme."', '".$nombre."', '".$tipoDocumento."', '".$numeroDocumento."', '', '', '', '', '".$pais."', '".$actividad."', 'NO', 'NO', 'NO', '', '');";			
			$res = $res && consultaBD($sql, true);
			if($res) {
				$codigoFisica = ultimoRegistro('personas_fisicas_informes_clientes', true);
			} else {
				$mensaje = 'Error 3 en la insercción de registros';
			}
		}
		if($objPHPExcel->getActiveSheet()->getCell('O'.$i)->getValue()!='' && $res){
			$sale=false;
			$res = $res && consultaBD('UPDATE informes_clientes SET checkPersonasJuridicas="SI" WHERE codigo='.$codigoInforme,true);
			$nombre=$objPHPExcel->getActiveSheet()->getCell('O'.$i)->getValue();
			$numeroDocumento=$objPHPExcel->getActiveSheet()->getCell('P'.$i)->getValue();
			$sql="INSERT INTO personas_juridicas_informes_clientes VALUES (NULL, '".$codigoInforme."', '', '".$nombre."', '', '".$numeroDocumento."', '', '', '', '', '', '".$pais."', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', 'NO', '', '');";
			$res = $res && consultaBD($sql, true);
			if($res) {
				$codigoJuridica = ultimoRegistro('personas_juridicas_informes_clientes', true);
			} else {
				$mensaje = 'Error 4 en la insercción de registros';
			}
		}
		if($objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getValue()!='' && $res){
			$sale=false;
			$res = $res && consultaBD('UPDATE informes_clientes SET checkPersonasJuridicas="SI" WHERE codigo='.$codigoInforme,true);
			$nombreJuridica=$objPHPExcel->getActiveSheet()->getCell('Q'.$i)->getValue();
			$nombreJuridica.=' '.$objPHPExcel->getActiveSheet()->getCell('R'.$i)->getValue();
			$nombreJuridica.=' '.$objPHPExcel->getActiveSheet()->getCell('S'.$i)->getValue();
			$tipoDocumentoJuridica=$objPHPExcel->getActiveSheet()->getCell('T'.$i)->getValue();
			$tipoDocumentoJuridica=$documentos[$tipoDocumentoJuridica];
			$numeroDocumentoJuridica=$objPHPExcel->getActiveSheet()->getCell('U'.$i)->getValue();
			$actividadJuridica=$objPHPExcel->getActiveSheet()->getCell('V'.$i)->getValue();
			$actividadJuridica=$actividades[$actividadJuridica];
			$consulta = "INSERT INTO informes_clientes_juridicas VALUES (NULL,'".$codigoInforme."', '".$nombreJuridica."', '".$tipoDocumentoJuridica."', '".$numeroDocumentoJuridica."','', '', '', '', '".$pais."', '".$actividadJuridica."', 'NO', 'NO', 'NO', '');";
			$res = $res && consultaBD($consulta, true);
			if(!$res) {
				$mensaje = 'Error 5 en la insercción de registros';
				print($i);
				print($consulta);
			}
		}
		if($objPHPExcel->getActiveSheet()->getCell('W'.$i)->getValue()!='' && $res){
			$sale=false;
			$datosServicios=datosRegistro('servicios_informes_clientes',$codigoInforme,'codigoInforme');
			$campo= trim(utf8_decode(ucfirst(strtolower($objPHPExcel->getActiveSheet()->getCell('W'.$i)->getValue()))));
			
			if(substr($campo, 0, 40) == 'Actuacion por cuenta de cliente en opera'){
				if(substr($campo, -12) == 'inmobiliaria'){
					$campo='checkServicio18';		
				} else {
					$campo='checkServicio17';
				}
			} else {
				$servicio = $checkServicios[$campo];
				$campo='checkServicio'.$checkServicios[$campo];
			}
			
			$campoFecha1='fechaContratacion'.$campo;
			$campoFecha2='fechaFinalizacion'.$campo;
			//echo $i.'<br/>';
			$fecha1=preparaFechaServicios($objPHPExcel->getActiveSheet()->getCell('X'.$i)->getValue()); 
			$fecha2=preparaFechaServicios($objPHPExcel->getActiveSheet()->getCell('Y'.$i)->getValue());  
			$res=consultaBD('UPDATE servicios_informes_clientes SET '.$campo.'="SI",'.$campoFecha1.'="'.$fecha1.'",'.$campoFecha2.'="'.$fecha2.'" WHERE codigo='.$datosServicios['codigo'],true);
			if($codigoFisica!=0){
				$consulta = "INSERT INTO servicios_personas_informes_clientes VALUES (NULL,".$codigoInforme.",'".$codigoFisica."_FISICAS','FISICAS','".$servicio."','FIJO');";
				$res = $res && consultaBD($consulta ,true);
			}
			if($codigoJuridica!=0){				
				$consulta = "INSERT INTO servicios_personas_informes_clientes VALUES (NULL,".$codigoInforme.",'".$codigoFisica."_JURIDICAS','JURIDICAS','".$servicio."','FIJO');";
				$res = $res && consultaBD($consulta ,true);
			}
			if(!$res) {
				$mensaje = 'Error 6 en la insercción de registros';
			}
		}
		$i++;
	}

	if($res){
		mensajeOK("Importación realizada correctamente");
		if($_SESSION['tipoUsuario']=='ADMIN'){
			echo '<script type="text/javascript">
					  window.location="../clientes/espacio_cliente.php?codigoCliente='.$_POST['codigoClienteImportar'].'";
				</script>';
		}		
	} else {
		$mensaje .= '. Última línea introducida: '.($i - 1);
		mensajeError($mensaje);
	}

	// if($_SESSION['tipoUsuario']=='ADMIN'){
	// 	echo '<script type="text/javascript">
  	// 			window.location="../clientes/espacio_cliente.php?codigoCliente='.$_POST['codigoClienteImportar'].'";
	// 		</script>';
	// }
}

function preparaFechaServicios($fecha){
	$fecha=trim($fecha);
	if($fecha==''){
		$fecha='0000-00-00';
	} else {
		$fecha=explode('/', $fecha);
		if(count($fecha)>1){
			$fecha[0]=str_pad($fecha[0], 2, "0", STR_PAD_LEFT);
			$fecha[1]=str_pad($fecha[1], 2, "0", STR_PAD_LEFT);
			$fecha[2]=strlen($fecha[2])==2?'20'.$fecha[2]:$fecha[2];
			$fecha=$fecha[2].'-'.$fecha[1].'-'.$fecha[0];
		} else {
			$fecha = date($format = "Y-m-d", PHPExcel_Shared_Date::ExcelToPHP($fecha[0])); 
		}
	}
	//echo $fecha.'<br/>';
	return $fecha;
}
/*function finalizarInforme(){
	$res=true;
	$res=consultaBD('UPDATE informes_clientes SET finalizado="SI" WHERE codigo='.$_GET['codigoFinalizar'],true);
	$_POST['referencia']='referencia';
	return $res;
}*/

function creaPostUsuario(){
	if(isset($_POST['codigoCliente'])){
		$usuario=datosRegistro('usuarios_clientes',$_POST['codigoCliente'],'codigoCliente');
		$_POST['codigoUsuario']=$usuario['codigoUsuario'];
	}
	if(!isset($_POST['referencia'])){
		$_POST['anio']=date('Y');;
		$_POST['referencia']=generaNumeroReferencia('informes_clientes','referencia','codigoUsuario='.$_POST['codigoUsuario']);
	}
}

function preparaMedidas(){
	$res='';
	if(isset($_POST['checkMedidasSimplificadas'])){
		$res='SIMPLIFICADAS';
	}
	if(isset($_POST['checkMedidasNormales'])){
		$res='NORMALES';
	}
	if(isset($_POST['checkMedidasReforzadas'])){
		if($res!=''){
			$res.='&$&';
		}
		$res.='REFORZADAS';
	}
	if(isset($_POST['checkMedidasExamen'])){
		if($res!=''){
			$res.='&$&';
		}
		$res.='EXAMEN';
	}
	$_POST['medidas']=$res;
}

function preparaDecision(){
	$decision=$_POST['decision'];
	$text ='';
	if(!empty($decision)){
		for($k=0;$k<count($decision);$k++){
			if($k > 0){
				$text .= "&$&";
			}
			$text .= $decision[$k];
		}
	}
	$_POST['decision']=$text;
}

function insertar(){
	$res=true;
	finalizaInforme();
	creaPostUsuario();
	preparaMedidas();
	//preparaDecision();
	$res=insertaDatos('informes_clientes');
	$_POST['codigo']=$res;
	$res=completar($_POST['codigo']);
	$_REQUEST['codigo']=$_POST['codigo'];
	return $res;
}

function actualizar(){
	$res=true;
	finalizaInforme();
	creaPostUsuario();
	preparaMedidas();
	//preparaDecision();
	$res=actualizaDatos('informes_clientes');
	$_SESSION['codigoProvisional']=$_POST['codigo'];
	$res=completar($_POST['codigo']);
	$_REQUEST['codigo']=$_SESSION['codigoProvisional'];
	return $res;
}

function completar($codigo){
	$res=true;
	$res=insertaPersonas($codigo);
	$res=insertaPersonasFisicas($codigo);
	$res=insertaPersonasJuridicas($codigo);
	$res=insertaPersonasIdentificacionJuridicas($codigo);
	$res=insertaPersonasSinJuridica($codigo);
	$res=insertaPersonasFideocomisos($codigo);
	$res=insertaProfesionales($codigo);
	$res=insertaServiciosContratados($codigo);
	$res=insertaComprobaciones($codigo);
	$res=insertaFirmados($codigo);
	return $res;
}

function insertaPersonas($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$notIn='(0';
	for($i=0;isset($datos['nombre'.$i]);$i++){
		if($datos['nombre'.$i]!=''){
			$datos = comprobarCheck($datos,array('checkCargoPublico'.$i,'checkNoPresencial'.$i));
			if(isset($datos['codigoExiste'.$i]) &&$datos['codigoExiste'.$i]!=''){
				$res=$res && consultaBD("UPDATE informes_clientes_fisicas SET nombre='".$datos['nombre'.$i]."',tipoDocumento='".$datos['tipoDocumento'.$i]."',numeroDocumento='".$datos['numeroDocumento'.$i]."',calle='".$datos['calle'.$i]."',numero='".$datos['numero'.$i]."',cp='".$datos['cp'.$i]."',ciudad='".$datos['ciudad'.$i]."',pais='".$datos['pais'.$i]."',actividadLaboral='".$datos['actividadLaboral'.$i]."',checkCargoPublico='".$datos['checkCargoPublico'.$i]."',checkNoPresencial='".$datos['checkNoPresencial'.$i]."',solicitaServicio='".$datos['solicitaServicio'.$i]."',observaciones='".$datos['observaciones'.$i]."',localidad='".$datos['localidad'.$i]."' WHERE codigo=".$datos['codigoExiste'.$i]);
				$notIn.=','.$datos['codigoExiste'.$i];
			} else {
				$res=$res && consultaBD("INSERT INTO informes_clientes_fisicas VALUES(NULL,".$codigo.",'".$datos['nombre'.$i]."','".$datos['tipoDocumento'.$i]."','".$datos['numeroDocumento'.$i]."','".$datos['calle'.$i]."','".$datos['numero'.$i]."','".$datos['cp'.$i]."','".$datos['ciudad'.$i]."','".$datos['pais'.$i]."','".$datos['actividadLaboral'.$i]."','".$datos['checkCargoPublico'.$i]."','".$datos['checkNoPresencial'.$i]."','".$datos['solicitaServicio'.$i]."','".$datos['observaciones'.$i]."','".$datos['localidad'.$i]."');");
				$notIn.=','.mysql_insert_id();
			}
		}
	}
	$notIn.=')';
	$res=consultaBD("DELETE FROM informes_clientes_fisicas WHERE codigo NOT IN ".$notIn." AND codigoInforme=$codigo");
	cierraBD();

	return $res;
}

function insertaPersonasIdentificacionJuridicas($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$notIn='(0';
	for($i=0;isset($datos['nombreIdentificacionJuridica'.$i]);$i++){
		if($datos['nombreIdentificacionJuridica'.$i]!=''){
			$datos = comprobarCheck($datos,array('checkCargoPublicoIdentificacionJuridica'.$i,'checkMenorIdentificacionJuridica'.$i,'checkIncapacitadoIdentificacionJuridica'.$i));
			if(isset($datos['codigoExisteIdentificacionJuridica'.$i]) &&$datos['codigoExisteIdentificacionJuridica'.$i]!=''){
				$res=$res && consultaBD("UPDATE informes_clientes_juridicas SET nombre='".$datos['nombreIdentificacionJuridica'.$i]."',tipoDocumento='".$datos['tipoDocumentoIdentificacionJuridica'.$i]."',numeroDocumento='".$datos['numeroDocumentoIdentificacionJuridica'.$i]."',calle='".$datos['calleIdentificacionJuridica'.$i]."',numero='".$datos['numeroIdentificacionJuridica'.$i]."',cp='".$datos['cpIdentificacionJuridica'.$i]."',ciudad='".$datos['ciudadIdentificacionJuridica'.$i]."',pais='".$datos['paisIdentificacionJuridica'.$i]."',actividadLaboral='".$datos['actividadLaboralIdentificacionJuridica'.$i]."',checkCargoPublico='".$datos['checkCargoPublicoIdentificacionJuridica'.$i]."',checkMenor='".$datos['checkMenorIdentificacionJuridica'.$i]."',checkIncapacitado='".$datos['checkIncapacitadoIdentificacionJuridica'.$i]."',localidad='".$datos['localidadIdentificacionJuridica'.$i]."' WHERE codigo=".$datos['codigoExisteIdentificacionJuridica'.$i]);
				$notIn.=','.$datos['codigoExisteIdentificacionJuridica'.$i];
			} else {
				$res=$res && consultaBD("INSERT INTO informes_clientes_juridicas VALUES(NULL,".$codigo.",'".$datos['nombreIdentificacionJuridica'.$i]."','".$datos['tipoDocumentoIdentificacionJuridica'.$i]."','".$datos['numeroDocumentoIdentificacionJuridica'.$i]."','".$datos['calleIdentificacionJuridica'.$i]."','".$datos['numeroIdentificacionJuridica'.$i]."','".$datos['cpIdentificacionJuridica'.$i]."','".$datos['ciudadIdentificacionJuridica'.$i]."','".$datos['paisIdentificacionJuridica'.$i]."','".$datos['actividadLaboralIdentificacionJuridica'.$i]."','".$datos['checkCargoPublicoIdentificacionJuridica'.$i]."','".$datos['checkMenorIdentificacionJuridica'.$i]."','".$datos['checkIncapacitadoIdentificacionJuridica'.$i]."','".$datos['localidadIdentificacionJuridica'.$i]."');");
				$notIn.=','.mysql_insert_id();
			}
		}
	}
	$notIn.=')';
	$res=consultaBD("DELETE FROM informes_clientes_juridicas WHERE codigo NOT IN ".$notIn." AND codigoInforme=$codigo");
	cierraBD();

	return $res;
}


function insertaPersonasFisicas($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$notIn='(0';
	for($i=0;isset($datos['nombreFisica'.$i]);$i++){
		if($datos['nombreFisica'.$i]!=''){
			$datos = comprobarCheck($datos,array('checkCargoPublicoFisica'.$i,'checkMenorFisica'.$i,'checkIncapacitadoFisica'.$i));
			if(isset($datos['codigoExisteFisica'.$i]) &&$datos['codigoExisteFisica'.$i]!=''){
				$res=$res && consultaBD("UPDATE personas_fisicas_informes_clientes SET nombreFisica='".$datos['nombreFisica'.$i]."',tipoDocumentoFisica='".$datos['tipoDocumentoFisica'.$i]."',numeroDocumentoFisica='".$datos['numeroDocumentoFisica'.$i]."',calleFisica='".$datos['calleFisica'.$i]."',numeroFisica='".$datos['numeroFisica'.$i]."',cpFisica='".$datos['cpFisica'.$i]."',ciudadFisica='".$datos['ciudadFisica'.$i]."',paisFisica='".$datos['paisFisica'.$i]."',actividadLaboralFisica='".$datos['actividadLaboralFisica'.$i]."',checkCargoPublicoFisica='".$datos['checkCargoPublicoFisica'.$i]."',checkMenorFisica='".$datos['checkMenorFisica'.$i]."',checkIncapacitadoFisica='".$datos['checkIncapacitadoFisica'.$i]."',observacionesFisica='".$datos['observacionesFisica'.$i]."',localidadFisica='".$datos['localidadFisica'.$i]."' WHERE codigo=".$datos['codigoExisteFisica'.$i]);
				$notIn.=','.$datos['codigoExisteFisica'.$i];
			} else {
				$res=$res && consultaBD("INSERT INTO personas_fisicas_informes_clientes VALUES(NULL,".$codigo.",'".$datos['nombreFisica'.$i]."','".$datos['tipoDocumentoFisica'.$i]."','".$datos['numeroDocumentoFisica'.$i]."','".$datos['calleFisica'.$i]."','".$datos['numeroFisica'.$i]."','".$datos['cpFisica'.$i]."','".$datos['ciudadFisica'.$i]."','".$datos['paisFisica'.$i]."','".$datos['actividadLaboralFisica'.$i]."','".$datos['checkCargoPublicoFisica'.$i]."','".$datos['checkMenorFisica'.$i]."','".$datos['checkIncapacitadoFisica'.$i]."','".$datos['observacionesFisica'.$i]."','".$datos['localidadFisica'.$i]."');");
				$notIn.=','.mysql_insert_id();
			}
		}
	}
	$notIn.=')';
	$res=consultaBD("DELETE FROM personas_fisicas_informes_clientes WHERE codigo NOT IN ".$notIn." AND codigoInforme=$codigo");
	cierraBD();

	return $res;
}

function insertaPersonasJuridicas($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$notIn='(0';
	for($i=0;isset($datos['nombreJuridica'.$i]);$i++){
		if($datos['razonSocial'.$i]!=''){
			$datos = comprobarCheck($datos,array('checkEntidadDerecho'.$i,'checkSociedadPersona'.$i,'checkEntidadFinanciera'.$i,'checkSucursalFilial'.$i,'checkSociedadCotizada'.$i,'checkSociedadMera'.$i,'checkSociedadCuya'.$i));
			if(isset($datos['codigoExisteJuridica'.$i]) &&$datos['codigoExisteJuridica'.$i]!=''){
				$res=$res && consultaBD("UPDATE personas_juridicas_informes_clientes SET nombreJuridica='".$datos['nombreJuridica'.$i]."',razonSocial='".$datos['razonSocial'.$i]."',formaJuridica='".$datos['formaJuridica'.$i]."',numCIF='".$datos['numCIF'.$i]."',objetoSocial='".$datos['objetoSocial'.$i]."',calleJuridica='".$datos['calleJuridica'.$i]."',numeroJuridica='".$datos['numeroJuridica'.$i]."',cpJuridica='".$datos['cpJuridica'.$i]."',ciudadJuridica='".$datos['ciudadJuridica'.$i]."',paisJuridica='".$datos['paisJuridica'.$i]."',checkEntidadDerecho='".$datos['checkEntidadDerecho'.$i]."',checkSociedadPersona='".$datos['checkSociedadPersona'.$i]."',checkEntidadFinanciera='".$datos['checkEntidadFinanciera'.$i]."',checkSucursalFilial='".$datos['checkSucursalFilial'.$i]."',checkSociedadCotizada='".$datos['checkSociedadCotizada'.$i]."',checkSociedadMera='".$datos['checkSociedadMera'.$i]."',checkSociedadCuya='".$datos['checkSociedadCuya'.$i]."',observacionesJuridica='".$datos['observacionesJuridica'.$i]."',localidadJuridica='".$datos['localidadJuridica'.$i]."' WHERE codigo=".$datos['codigoExisteJuridica'.$i]);
				$notIn.=','.$datos['codigoExisteJuridica'.$i];
			} else {
				$res=$res && consultaBD("INSERT INTO personas_juridicas_informes_clientes VALUES(NULL,".$codigo.",'".$datos['nombreJuridica'.$i]."','".$datos['razonSocial'.$i]."','".$datos['formaJuridica'.$i]."','".$datos['numCIF'.$i]."','".$datos['objetoSocial'.$i]."','".$datos['calleJuridica'.$i]."','".$datos['numeroJuridica'.$i]."','".$datos['cpJuridica'.$i]."','".$datos['ciudadJuridica'.$i]."','".$datos['paisJuridica'.$i]."','".$datos['checkEntidadDerecho'.$i]."','".$datos['checkSociedadPersona'.$i]."','".$datos['checkEntidadFinanciera'.$i]."','".$datos['checkSucursalFilial'.$i]."','".$datos['checkSociedadCotizada'.$i]."','".$datos['checkSociedadMera'.$i]."','".$datos['checkSociedadCuya'.$i]."','".$datos['observacionesJuridica'.$i]."','".$datos['localidadJuridica'.$i]."');");
				$notIn.=','.mysql_insert_id();
			}
		}
	}
	$notIn.=')';
	$res=consultaBD("DELETE FROM personas_juridicas_informes_clientes WHERE codigo NOT IN ".$notIn." AND codigoInforme=$codigo");
	cierraBD();

	return $res;
}

function insertaPersonasSinJuridica($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$notIn='(0';
	for($i=0;isset($datos['nombreSinJuridica'.$i]);$i++){
		if($datos['nombreSinJuridica'.$i]!=''){
			$datos = comprobarCheck($datos,array('checkCargoPublicoSinJuridica'.$i,'checkMenorSinJuridica'.$i,'checkIncapacitadoSinJuridica'.$i));
			if(isset($datos['codigoExisteSinJuridica'.$i]) &&$datos['codigoExisteSinJuridica'.$i]!=''){
				$res=$res && consultaBD("UPDATE personas_sinjuridicas_informes_clientes SET nombreSinJuridica='".$datos['nombreSinJuridica'.$i]."',tipoDocumentoSinJuridica='".$datos['tipoDocumentoSinJuridica'.$i]."',numeroDocumentoSinJuridica='".$datos['numeroDocumentoSinJuridica'.$i]."',calleSinJuridica='".$datos['calleSinJuridica'.$i]."',numeroSinJuridica='".$datos['numeroSinJuridica'.$i]."',cpSinJuridica='".$datos['cpSinJuridica'.$i]."',ciudadSinJuridica='".$datos['ciudadSinJuridica'.$i]."',paisSinJuridica='".$datos['paisSinJuridica'.$i]."',actividadLaboralSinJuridica='".$datos['actividadLaboralSinJuridica'.$i]."',checkCargoPublicoSinJuridica='".$datos['checkCargoPublicoSinJuridica'.$i]."',checkMenorSinJuridica='".$datos['checkMenorSinJuridica'.$i]."',checkIncapacitadoSinJuridica='".$datos['checkIncapacitadoSinJuridica'.$i]."',observacionesSinJuridica='".$datos['observacionesSinJuridica'.$i]."',localidadSinJuridica='".$datos['localidadSinJuridica'.$i]."' WHERE codigo=".$datos['codigoExisteSinJuridica'.$i]);
				$notIn.=','.$datos['codigoExisteSinJuridica'.$i];
			} else {
				$res=$res && consultaBD("INSERT INTO personas_sinjuridicas_informes_clientes VALUES(NULL,".$codigo.",'".$datos['nombreSinJuridica'.$i]."','".$datos['tipoDocumentoSinJuridica'.$i]."','".$datos['numeroDocumentoSinJuridica'.$i]."','".$datos['calleSinJuridica'.$i]."','".$datos['numeroSinJuridica'.$i]."','".$datos['cpSinJuridica'.$i]."','".$datos['ciudadSinJuridica'.$i]."','".$datos['paisSinJuridica'.$i]."','".$datos['actividadLaboralSinJuridica'.$i]."','".$datos['checkCargoPublicoSinJuridica'.$i]."','".$datos['checkMenorSinJuridica'.$i]."','".$datos['checkIncapacitadoSinJuridica'.$i]."','".$datos['observacionesSinJuridica'.$i]."','".$datos['localidadSinJuridica'.$i]."');");
				$notIn.=','.mysql_insert_id();
			}
		}
	}
	$notIn.=')';
	$res=consultaBD("DELETE FROM personas_sinjuridicas_informes_clientes WHERE codigo NOT IN ".$notIn." AND codigoInforme=$codigo");
	cierraBD();

	return $res;
}

function insertaPersonasFideocomisos($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$notIn='(0';
	for($i=0;isset($datos['nombreFideocomiso'.$i]);$i++){
		if($datos['nombreFideocomiso'.$i]!=''){
			$datos = comprobarCheck($datos,array('checkCargoPublicoFideocomiso'.$i,'checkMenorFideocomiso'.$i,'checkIncapacitadoFideocomiso'.$i,'checkFideocomisarioFideocomiso'.$i));
			if(isset($datos['codigoExisteFideocomiso'.$i]) &&$datos['codigoExisteFideocomiso'.$i]!=''){
				$res=$res && consultaBD("UPDATE personas_fideocomisos_informes_clientes SET nombreFideocomiso='".$datos['nombreFideocomiso'.$i]."',tipoDocumentoFideocomiso='".$datos['tipoDocumentoFideocomiso'.$i]."',numeroDocumentoFideocomiso='".$datos['numeroDocumentoFideocomiso'.$i]."',calleFideocomiso='".$datos['calleFideocomiso'.$i]."',numeroFideocomiso='".$datos['numeroFideocomiso'.$i]."',cpFideocomiso='".$datos['cpFideocomiso'.$i]."',ciudadFideocomiso='".$datos['ciudadFideocomiso'.$i]."',paisFideocomiso='".$datos['paisFideocomiso'.$i]."',condicionFideocomiso='".$datos['condicionFideocomiso'.$i]."',actividadLaboralFideocomiso='".$datos['actividadLaboralFideocomiso'.$i]."',checkCargoPublicoFideocomiso='".$datos['checkCargoPublicoFideocomiso'.$i]."',checkMenorFideocomiso='".$datos['checkMenorFideocomiso'.$i]."',checkIncapacitadoFideocomiso='".$datos['checkIncapacitadoFideocomiso'.$i]."',checkFideocomisarioFideocomiso='".$datos['checkFideocomisarioFideocomiso'.$i]."',observacionesFideocomiso='".$datos['observacionesFideocomiso'.$i]."',localidadFideocomiso='".$datos['localidadFideocomiso'.$i]."' WHERE codigo=".$datos['codigoExisteFideocomiso'.$i]);
				$notIn.=','.$datos['codigoExisteFideocomiso'.$i];
			} else {
				$res=$res && consultaBD("INSERT INTO personas_fideocomisos_informes_clientes VALUES(NULL,".$codigo.",'".$datos['nombreFideocomiso'.$i]."','".$datos['tipoDocumentoFideocomiso'.$i]."','".$datos['numeroDocumentoFideocomiso'.$i]."','".$datos['calleFideocomiso'.$i]."','".$datos['numeroFideocomiso'.$i]."','".$datos['cpFideocomiso'.$i]."','".$datos['ciudadFideocomiso'.$i]."','".$datos['paisFideocomiso'.$i]."','".$datos['condicionFideocomiso'.$i]."','".$datos['actividadLaboralFideocomiso'.$i]."','".$datos['checkCargoPublicoFideocomiso'.$i]."','".$datos['checkMenorFideocomiso'.$i]."','".$datos['checkIncapacitadoFideocomiso'.$i]."','".$datos['checkFideocomisarioFideocomiso'.$i]."','".$datos['observacionesFideocomiso'.$i]."','".$datos['localidadFideocomiso'.$i]."');");
				$notIn.=','.mysql_insert_id();
			}
		}
	}
	$notIn.=')';
	$res=consultaBD("DELETE FROM personas_fideocomisos_informes_clientes WHERE codigo NOT IN ".$notIn." AND codigoInforme=$codigo");
	cierraBD();

	return $res;
}

function insertaProfesionales($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();

	$res=consultaBD("DELETE FROM profesionales_informes_clientes WHERE codigoInforme=$codigo");

	for($i=0;isset($datos['profesional'.$i]);$i++){
		if($datos['profesional'.$i]!=''){
			$res=$res && consultaBD("INSERT INTO profesionales_informes_clientes VALUES(NULL,".$codigo.",'".$datos['profesional'.$i]."');");
		}
	}

	cierraBD();

	return $res;
}

function insertaServiciosContratados($codigo){
	$res=true;
	if(isset($_POST['codigoServicios'])){
		$_POST['codigo']=$_POST['codigoServicios'];
		$res=actualizaDatos('servicios_informes_clientes');
	} else {
		$_POST['codigoInforme']=$codigo;
		$res=insertaDatos('servicios_informes_clientes');
	}
	$informe=datosRegistro('informes_clientes',$codigo);
	$codigoUsuario=$informe['codigoUsuario'];
	conexionBD();
	$res=consultaBD('DELETE FROM servicios_informes_clientes_personalizados_seleccionados WHERE codigoInforme='.$codigo);
	$i=0;
	while(isset($_POST['falsonombreServicioPersonalizado'.$i])){
		if(isset($_POST['checkServicioPersonalizado'.$i])){
			if($_POST['checkServicioPersonalizado'.$i]=='SI'){
				$res=consultaBD('INSERT INTO servicios_informes_clientes_personalizados VALUES(NULL,'.$codigoUsuario.',"'.$_POST['falsonombreServicioPersonalizado'.$i].'");');
				$codigoServicio=mysql_insert_id();
			} else {
				$codigoServicio=$_POST['checkServicioPersonalizado'.$i];
			}
			$res=consultaBD('INSERT INTO servicios_informes_clientes_personalizados_seleccionados VALUES(NULL,'.$codigo.','.$codigoServicio.',"'.formateaFechaBD($_POST['fechaContratacionServicioPersonalizado'.$i]).'","'.formateaFechaBD($_POST['fechaFinalizacionServicioPersonalizado'.$i]).'","SI");');
		}
		$i++;
	}
	cierraBD();
	return $res;
}

function insertaComprobaciones($codigo){
	$res=true;
	if(isset($_POST['codigoComprobaciones'])){
		$_POST['codigo']=$_POST['codigoComprobaciones'];
		$res=actualizaDatos('comprobaciones_informes_clientes');
	} else {
		$_POST['codigoInforme']=$codigo;
		$res=insertaDatos('comprobaciones_informes_clientes');
	}
	return $res;
}

function insertaFirmados($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();

	$res=consultaBD("DELETE FROM firmado_informes_clientes WHERE codigoInforme=$codigo");

	for($i=0;isset($datos['firmado'.$i]);$i++){
		if($datos['firmado'.$i]!=''){
			$res=$res && consultaBD("INSERT INTO firmado_informes_clientes VALUES(NULL,".$codigo.",'".$datos['firmado'.$i]."');");
		}
	}

	cierraBD();

	return $res;
}

function finalizaInforme(){
	if($_POST['resultado']=='SIN'){
		$_POST['finalizado']='NO';
	} else {
		$_POST['finalizado']='SI';
	}
}

function asignarServicios(){
	$res=true;
	conexionBD();
	$notIn='(0';
	$i=0;
	while(isset($_POST['servicio'.$i])){
		if($_POST['servicio'.$i]!='' && $_POST['persona'.$i]!=''){
			$tipo=explode('_', $_POST['persona'.$i]);
			$tipoServicio=explode('_',$_POST['servicio'.$i]);
			if(isset($_POST['codigoExiste'.$i]) && $_POST['codigoExiste'.$i]!=''){
				$res=$res && consultaBD("UPDATE servicios_personas_informes_clientes SET codigoPersona='".$_POST['persona'.$i]."',tipo='".$tipo[1]."',servicio='".$tipoServicio[0]."',tipoServicio='".$tipoServicio[1]."' WHERE codigo=".$_POST['codigoExiste'.$i]);
				$notIn.=','.$_POST['codigoExiste'.$i];
			} else {
				$res=$res && consultaBD("INSERT INTO servicios_personas_informes_clientes VALUES (NULL,".$_POST['codigoInforme'].",'".$_POST['persona'.$i]."','".$tipo[1]."','".$tipoServicio[0]."','".$tipoServicio[1]."')");
				$notIn.=','.mysql_insert_id();
			}
		}
		$i++;
	}
	$notIn.=')';
	$res=$res && consultaBD('DELETE FROM servicios_personas_informes_clientes WHERE codigo NOT IN'.$notIn.' AND codigoInforme='.$_POST['codigoInforme']);
	cierraBD();
	return $res;
}

/*
IF(informes_clientes.checkPersonasFisicas='SI',
				,CONCAT('Personas físicas: ',GROUP_CONCAT(personas_fisicas_informes_clientes.nombreFisica SEPARATOR ', '),'<br/>')
				,''),
				IF(informes_clientes.checkPersonasJuridicas='SI',
				,CONCAT('Personas jurídicas: ',GROUP_CONCAT(personas_juridicas_informes_clientes.nombreJuridica SEPARATOR ', '))
				,''),
*/

function listadoTrabajos($examen='NO'){

	if($_SESSION['tipoUsuario']=='ADMIN'){
        $columnas=array('razonSocial','referencia1','referencia2','fecha','nombre','representacion','finalizado','referencia2','referencia2');
    } else {
		$columnas=array('referencia1','referencia2','fecha','nombre','representacion','finalizado','referencia2','referencia2');
	}

	$ejercicio=false;

	if($examen=='RENOVACION'){
		$ejercicio=false;
	}

	if($_SESSION['tipoUsuario']=='ADMIN'){
		if($_SESSION['espacio']=='NO'){
			array_unshift($columnas, 'codigoUsuario');
			$having=obtieneWhereListado("HAVING 1=1",$columnas,false, false, $ejercicio);
		} else {
			$usuario=datosRegistro('usuarios_clientes',$_SESSION['espacio'],'codigoCliente');
			$having=obtieneWhereListado("HAVING codigoUsuario=".$usuario['codigoUsuario'],$columnas,false,false,$ejercicio);
		}
	} else {
		$having=obtieneWhereListado("HAVING codigoUsuario=".$_SESSION['codigoU'],$columnas,false,false,$ejercicio);
	}

	if($examen=='EXAMEN'){
		$having.=' AND medidas LIKE "%EXAMEN%"'; 
	}

	if($examen=='RENOVACION'){
		$having.=' AND fecha < DATE_SUB(CURDATE(), INTERVAL 11 MONTH) AND fecha != "0000-00-00" AND fechaFinalizacion="0000-00-00" AND renovada="PENDIENTE"'; 
	}

	$orden=obtieneOrdenListado($columnas);//Uso de función personalizada

	if($orden==''){
		$orden='ORDER BY referencia2';
	}

	$limite=obtieneLimitesListado();

	$query="SELECT informes_clientes.codigo,IF(finalizado='SI','M','N') AS referencia1, referencia AS referencia2, fecha, 
			GROUP_CONCAT(DISTINCT informes_clientes_fisicas.nombre SEPARATOR '<br/>') AS nombre, anio, finalizado, informes_clientes.codigoUsuario,resultado,renovada,clientes.razonSocial, medidas,
			fechaFinalizacion,
			CONCAT( 
				IF(informes_clientes.checkPersonasFisicas='SI'
				,CONCAT('Personas físicas: ',GROUP_CONCAT(personas_fisicas_informes_clientes.nombreFisica SEPARATOR ', '),'<br/>')
				,''),
				IF(informes_clientes.checkPersonasJuridicas='SI'
				,CONCAT('Personas jurídicas: ',GROUP_CONCAT(personas_juridicas_informes_clientes.razonSocial SEPARATOR ', '))
				,'')
			) AS representacion 
			FROM informes_clientes INNER JOIN usuarios ON informes_clientes.codigoUsuario=usuarios.codigo 
			INNER JOIN usuarios_clientes ON usuarios.codigo=usuarios_clientes.codigoUsuario 
			INNER JOIN clientes ON usuarios_clientes.codigoCliente=clientes.codigo 
			LEFT JOIN informes_clientes_fisicas ON informes_clientes.codigo=informes_clientes_fisicas.codigoInforme 
			LEFT JOIN personas_fisicas_informes_clientes ON informes_clientes.codigo=personas_fisicas_informes_clientes.codigoInforme
			LEFT JOIN personas_juridicas_informes_clientes ON informes_clientes.codigo=personas_juridicas_informes_clientes.codigoInforme
			GROUP BY informes_clientes.codigo 
			$having";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);

	$iconoC=array('SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');

	$referencias=array('SI'=>'M','NO'=>'N');

	while($datos=mysql_fetch_assoc($consulta)){
		$mostrar=true;
		$notificacion='';
		$fecha=new DateTime($datos['fecha']);
		$fecha->add(new DateInterval('P11M'));

		if(comparaFechas($fecha->format('Y-m-d'),date('Y-m-d'))==1){
			if($datos['fecha']!='0000-00-00'){
		
				if($datos['renovada']=='PENDIENTE' && $datos['fechaFinalizacion']=="0000-00-00"){
					$notificacion="<a href='#'' class='enlacePopOver noAjax' position='right' title='Aviso de renovación'><i class='icon-question-circle iconoFactura icon-naranja'></i></a><div class='hide'>En espera de que se acepte o no la renovación automática</div>";
				} else if($datos['renovada']=='SI'){

					$renovacion=datosRegistro('informes_renovaciones',$datos['codigo'],'codigoInformeOriginal');
					if($renovacion){
						if($renovacion['tipoRenovacion']=='PARTIR'){
							$mostrar=false;
						} else {
						$notificacion="<a href='#'' class='enlacePopOver noAjax' position='right' title='Aviso de renovación'><i class='icon-refresh iconoFactura icon-success'></i></a><div class='hide'>Informe renovado</div>";
						}	
					} else {
						$notificacion="<a href='#'' class='enlacePopOver noAjax' position='right' title='Aviso de renovación'><i class='icon-check-circle iconoFactura icon-success'></i></a><div class='hide'>Aceptada la renovación automática, acceda al formulario de renovación desde el botón Detalles Acciones</div>";
					}

				} else if($datos['renovada']=='NO'){
					$notificacion="<a href='#'' class='enlacePopOver noAjax' position='right' title='Aviso de renovación'><i class='icon-times-circle iconoFactura icon-danger'></i></a><div class='hide'>Rechazada la renovación automática, se precisa un nuevo informe</div>";
				}
			}
		}

		if($examen=='RENOVACION'){
			$fila=array(
				$datos['referencia1'],
				$datos['referencia2'],
				formateaFechaWeb($datos['fecha']),
				$datos['nombre'],
				$datos['representacion'],
				creaBotonDetalles("informes-clientes/index.php?filtro=RENOVACION&respuesta=SI&codigoRenovacion=".$datos['codigo'],'No se aprecian indicios de blanqueo de capitales','icon-check'),
				creaBotonDetalles("informes-clientes/index.php?filtro=RENOVACION&respuesta=NO&codigoRenovacion=".$datos['codigo'],'Se aprecian indicios de blanqueo de capitales','icon-times'),
				obtieneCheckTabla($datos),
        		"DT_RowId"=>$datos['codigo']
			);
		} else {
			if($mostrar){
				$fila=array(
					$datos['referencia1'].' '.$notificacion,
					$datos['referencia2'],
					formateaFechaWeb($datos['fecha']),
					$datos['nombre'],
					$datos['representacion'],
					"<div class='centro'>".$iconoC[$datos['finalizado']]."</div>",
					crearBoton($datos['codigo'],$datos['finalizado'],$datos['renovada']),
					obtieneCheckTabla($datos),
        			"DT_RowId"=>$datos['codigo']
				);
			}
		}

		if($_SESSION['tipoUsuario']=='ADMIN'){
			if($datos['codigoUsuario'] == NULL){
				array_unshift($fila, '');
			} else {
				$cliente = consultaBD('SELECT clientes.* FROM clientes INNER JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente WHERE codigoUsuario='.$datos['codigoUsuario'],true,true);
				if(isset($fila) && is_array($fila)){
					array_unshift($fila, $datos['razonSocial']);
				}
			}
		}

		if($mostrar){
			$res['aaData'][]=$fila;
		}

	}

	echo json_encode($res);
}

function crearBoton($codigo,$finalizado,$renovacion='NO'){
	$nombres=array();
	$direcciones=array();
	$iconos=array();
	$target=array();

	$clave=rand(5, 62);

	array_push($nombres, 'Detalles');
	array_push($nombres, 'Servicios');
	array_push($nombres, 'Descargar informe');
	array_push($direcciones, 'informes-clientes/gestion.php?kd='.$clave.'&'.encriptar('codigo',$clave).'='.encriptar($codigo,$clave));
	array_push($direcciones, 'informes-clientes/gestion.php?servicios&kd='.$clave.'&'.encriptar('codigo',$clave).'='.encriptar($codigo,$clave));
	array_push($direcciones, 'informes-clientes/generaDocumento.php?kd='.$clave.'&'.encriptar('codigo',$clave).'='.encriptar($codigo,$clave));
	array_push($iconos, 'icon-search-plus');
	array_push($iconos, 'icon-random');
	array_push($iconos, 'icon-download');
	array_push($target, 0);
	array_push($target, 0);
	array_push($target, 1);	
	
	if($renovacion=='SI'){
		$renovacion=datosRegistro('informes_renovaciones',$codigo,'codigoInformeOriginal');
		if(!$renovacion){
			array_push($nombres, 'Renovación');
			array_push($direcciones, 'informes-clientes/gestion.php?renovacion&kd='.$clave.'&'.encriptar('codigo',$clave).'='.encriptar($codigo,$clave));
			array_push($iconos, 'icon-refresh');
			array_push($target, 0);
		}
	}

	return botonAcciones($nombres,$direcciones,$iconos,$target);
}

//Esta función utiliza un "orden natural" (primero por longitud, luego por el propio valor) para la primera columna del listado de servicios (que es mayoritariamente numérica)
function obtieneOrdenListadoServicios($columnas){
	$orden = '';
    if(isset($_GET['iSortCol_0'])) {
        $orden = 'ORDER BY  ';
        for ($i=0;$i<(int)$_GET['iSortingCols'];$i++) {
            if($_GET['bSortable_'.(int)$_GET['iSortCol_'.$i]]=='true'){
                $indice=(int)$_GET['iSortCol_'.$i];

                if($indice==0 && $_GET['sSortDir_'.$i]==='asc'){//Si se trata de la primera columna y el orden es ascendente...
                	$orden.="LENGTH(referencia), referencia, ";//.. ordeno por longitud y luego valor
                }
                elseif($indice==0 && $_GET['sSortDir_'.$i]!=='asc'){//Lo mismo si el orden es descendente
                	$orden.="LENGTH(referencia) DESC, referencia DESC, ";
                }
                else{//Orden normal
                	$orden.=$columnas[$indice].' '.($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
                }
            }
        }

        $orden = substr_replace($orden, '', -2);
        if ($orden == 'ORDER BY') {
            $orden = '';
        }
    }
    return $orden;
}

function gestionRenovacion(){
	desencriptaGET();

	if($_SESSION['tipoUsuario'] != 'ADMIN' && isset($_GET['codigo'])){
		$registro = datosRegistro("informes_clientes", $_GET['codigo']);    	
		compruebaAcceso($registro['codigoUsuario']);
	}	

	abreVentanaGestion('Gestión de Renovación','index.php','','icon-edit','',true,'noAjax');
		campoOculto($_GET['codigo'],'codigoInformeOriginal');
		campoFecha('fechaRenovacion','Fecha de renovación');
		campoTexto('responsableRenovacion','Responsable de renovación');
		campoRadio('tipoRenovacion','Tipo de renovacion','','DUPLICADO',array('Duplicado del informe anterior (Marque esta opción si desea duplicar su informe y que se muestre en el listado de informes de clientes.<br/>Se generará una nueva referencia)','Informe a partir del anterior (Marque esta opción si desea que el informe a crear, sustituya al anterior en el listado de informes de clientes.<br/>Deberás adecuar las fechas de los servicios al año en cuestión para el que necesite generar el informe)'),array('DUPLICADO','PARTIR'),true);
	cierraVentanaGestion('index.php');
}

function gestionInformes(){
	global $_CONFIG;
	desencriptaGET();

	if($_SESSION['tipoUsuario'] != 'ADMIN' && isset($_GET['codigo'])){
		$registro = datosRegistro("informes_clientes", $_GET['codigo']);    	
		compruebaAcceso($registro['codigoUsuario']);
	}	

	operacionesInformes();

	abreVentanaGestion('Gestión de Informes de riesgo','?','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('informes_clientes');
	$recargar='';
	if($datos){
		$host= $_SERVER["HTTP_HOST"];
    	$url=  $_SERVER["REQUEST_URI"];
    	$url=explode('?', $url);
    	$recargar="https://".$host.$url[0].'?codigo='.$datos['codigo'];
    	$comprobaciones=datosRegistro('comprobaciones_informes_clientes',$datos['codigo'],'codigoInforme');
		if($comprobaciones){
			campoOculto($comprobaciones['codigo'],'codigoComprobaciones');
		}
	} else {
		$comprobaciones['codigoInforme']=0;
	}
	$servicios=datosRegistro('servicios_informes_clientes',$datos['codigo'],'codigoInforme');
	if($servicios){
		campoOculto($servicios['codigo'],'codigoServicios');
	}
	$pagina = isset($_POST['pagina']) ? $_POST['pagina']:1;
	campoOculto($pagina,'pagina');
	campoOculto($datos,'renovada','PENDIENTE');
	campoOculto($datos,'codigoExcel','NULL');
	creaPestaniasAPI(array('Datos de informe','Identificación del cliente','Clientes','Actuación en representación de otra persona física','Actuación en representación de otra persona jurídica','Identificación de titular real de la persona jurídica','Actuación en representación de entidad sin personalidad jurídica','Fideocomisos o instrumentos jurídicos análogos','Examen especial','Servicios contratados por el cliente','Medidas de diligencia debida aplicadas','Finalización prestación servicio','Examen especial','Análisis técnico','Interrupción de la relación de negocios','Resultado del examen especial'));
	abrePestaniaAPI(1,$pagina==1);
		echo "Con carácter previo al establecimiento de la relación de negocios o a la intervención en cualquier operación ocasional se debe identificar al titular  y adoptar las medidas adecuadas en función del riesgo -artículo 4 y 9 del RLPBC-.<br/><br/>";
		abreColumnaCampos();
			if($datos){
				campoOculto($datos,'anio');
				campoOculto($datos,'referencia');
				campoDato('Referencia',$datos['referencia'].' / '.substr($datos['anio'], 2));
			}
		cierraColumnaCampos();
		abreColumnaCampos();
			if($_SESSION['tipoUsuario']=='ADMIN'){
				if($datos){
					$cliente=datosRegistro('usuarios_clientes',$datos['codigoUsuario'],'codigoUsuario');
				} else {
					$cliente=false;
				}
				campoSelectConsultaAjax('codigoCliente','Cliente','SELECT codigo, razonSocial AS texto FROM clientes',$cliente,'','selectpicker selectAjax span4 show-tick');
			} else {
				campoOculto($datos,'codigoUsuario',$_SESSION['codigoU']);
			}
			campoFecha('fecha','Fecha del informe',$datos);
		cierraColumnaCampos();
		if($datos){
				$renovacion=datosRegistro('informes_renovaciones',$datos['codigo'],'codigoInformeNuevo');
				if($renovacion){
					$informeOriginal=datosRegistro('informes_clientes',$renovacion['codigoInformeOriginal']);
					echo "<br clear='all'><h2 class='apartadoFormulario'>Datos de renovación</h1>";
					campoDato('Informe original','<a href="gestion.php?codigo='.$informeOriginal['codigo'].'" target="_blank" class="noAjax">'.$informeOriginal['referencia'].' / '.substr($datos['anio'], 2).'</a>');
					campoDato('Responsable renovación',$renovacion['responsableRenovacion']);
				}
			}
		botonesNav(2);
	cierraPestaniaAPI();

	abrePestaniaAPI(2,$pagina==2);	
		echo "Los sujetos obligados identificarán y comprobarán, con carácter PREVIO al establecimiento de la relación de negocios o de la ejecución de operaciones ocasionales,  mediante documentos fehacientes –establecidos en el <a href='#'' class='enlacePopOver noAjax' position='right' title='Art. 6 RLPBC: Documentos fehacientes a efectos de identificación formal <button class=\"cierraPopover btn btn-propio\"><i class=\"icon-times\"></i></button>'>art. 6 RLPBC</a><div class='hide'>".articulo6()."</div>- la identidad de cuantas personas físicas o jurídicas pretendan establecer relaciones de negocio o intervenir en cualesquiera operaciones ocasionales cuyo importe sea igual o superior a 1.000 euros. Los documentos de identificación deberán encontrarse en vigor en el momento de establecer relaciones de negocio o ejecutar operaciones ocasionales. En el supuesto de personas jurídicas, la vigencia de los datos consignados en la documentación aportada deberá acreditarse mediante una declaración responsable del cliente";

		echo "<div class='hide'><br clear='all'><br/><h2 class='apartadoFormulario'>Persona física que solicita el servicio</h1>";
		campoTexto('nombre','Nombre y apellidos',$datos,'span5');
		campoSelect('tipoDocumento','Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),'','selectpicker span8 tipoDocumento show-tick');
			campoTexto('numeroDocumento','Número',$datos);
		echo "<br clear='all'><h2 class='apartadoFormulario'>Residencia</h1>";
		abreColumnaCampos();
			campoTexto('calle','Dirección',$datos);
			echo '<div class="hide">';
			campoTexto('numero','Nº',$datos,'input-mini');
			echo '</div>';
		cierraColumnaCampos();
		abreColumnaCampos();
			campoTexto('cp','CP',$datos,'input-small');
			campoTexto('ciudad','Ciudad',$datos);
			selectPaises('pais',$datos,true);
		cierraColumnaCampos();

		echo "<br clear='all'><h2 class='apartadoFormulario'>Actividad</h1>";
		campoSelect('actividadLaboral','Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),$datos,'selectpicker span3 show-tick actividadlaboral');
		campoCheckIndividualConClase('checkCargoPublico','Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',$datos,'checkReforzadas');
		campoCheckIndividualConClase('checkNoPresencial','Solicita servicio de forma <a href="#" title="Artículo 21 RLPBC: Requisitos en las relaciones de negocio y operaciones no presenciales <button class=\'cierraPopover btn btn-propio\'><i class=\'icon-times\'></i></button>" class="enlacePopOver noAjax" position="right">no presencial</a><div class="hide">'.articulo21().'</div>',$datos,'checkReforzadas');
		campoRadio('solicitaServicio','Solicita el servicio',$datos,1,array('En nombre propio','En nombre propio y de 3º/s','En representación de tercero/s'),array(1,2,3),true);
		areaTexto('observaciones','Observaciones',$datos);
		echo '</div>';
		creaTablaIdentificacion($datos);

		echo '<div id="pestania3" class="hide">';
		botonesNav(3);
		echo '</div><div id="pestania4">';
		botonesNav(4);
		echo '</div>';
	cierraPestaniaAPI();

	abrePestaniaAPI(3,$pagina==3);
		echo "En los casos de representación legal o voluntaria, la identidad del representante y de la persona o entidad representada será comprobada documentalmente. A estos efectos deberá obtenerse copia del documento fehaciente a efectos de identificación, tanto del representante como del representado, así como del documento público acreditativo de los poderes de representación conferidos<br/><br/>";
		botonesNav(4);
	cierraPestaniaAPI();

	abrePestaniaAPI(4,$pagina==4);
		campoCheckIndividual('checkPersonasFisicas','Actuación en representación de otra persona física',$datos);
		echo '<br/><div id="divPersonasFisicas" class="hide">';
		creaTablaPersonasFisicas($datos);
		echo '</div><br/><br/>(Esta ventana se cumplimentará si quien contrata el servicio actúa en nombre de un tercero persona física)';
		botonesNav(5);
	cierraPestaniaAPI();

	abrePestaniaAPI(5,$pagina==5);
		campoCheckIndividual('checkPersonasJuridicas','Actuación en representación de otra persona jurídica',$datos);
		echo '<br/><div id="divPersonasJuridicas" class="hide">';
		creaTablaPersonasJuridicas($datos);
		echo '</div><br/><br/>(Esta ventana se cumplimentará si quien contrata el servicio actúa en nombre de un tercero persona jurídica)';
		echo '<div id="pestania6" class="hide">';
		botonesNav(6);
		echo '</div><div id="pestania7">';
		botonesNav(7);
		echo '</div>';
	cierraPestaniaAPI();

	abrePestaniaAPI(6,$pagina==6);
	echo '<div class="hide">';
	echo 'En el supuesto que el sujeto obligado preste el servicio a una persona jurídica, con carácter previo identificará y comprobará la identidad de la persona o personas físicas que en último término posean o controlen, directa o indirectamente, un porcentaje superior al 25 por ciento del capital o de los derechos de voto de la persona jurídica, o que a través de acuerdos o disposiciones estatutarias o por otros medios ejerzan el control, directo o indirecto, de la gestión de la persona jurídica. Cuando no exista una persona física que cumpla estos requisitos, se considerará que ejerce dicho control el administrador o administradores.<br/><br/>
		No será preceptivo comprobar la identidad del titular real respecto de los siguientes clientes - en aplicación de medidas simplificadas de diligencia debida-:<br/><br/>
		a) Las entidades de derecho público de los Estados miembros de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>países tercero equivalente</a>.<br/>
		b) Las sociedades u otras personas jurídicas controladas o participadas mayoritariamente por entidades de derecho público de los Estados miembros de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>países terceros equivalentes</a>.<br/>
		c) Las entidades financieras, exceptuadas las entidades de pago, domiciliadas en la Unión Europea o en <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>países terceros equivalentes</a> que sean objeto de supervisión para garantizar el cumplimiento de las obligaciones de prevención del blanqueo de capitales y de la financiación del terrorismo-<br/>
		d) Las sucursales o filiales de entidades financieras, exceptuadas las entidades de pago, domiciliadas en la Unión Europea o en <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>países terceros equivalentes</a>, cuando estén sometidas por la matriz a procedimientos de prevención del blanqueo de capitales y de la financiación del terrorismo.<br/>
		e) Las sociedades cotizadas cuyos valores se admitan a negociación en un mercado regulado de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>países terceros equivalentes</a> así como sus sucursales y filiales participadas mayoritariamente.<br/><br/><br/>';
		campoTexto('nombreJuridica','Nombre y apellidos',$datos);
		campoSelect('tipoDocumentoJuridica','Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),$datos,'selectpicker span8 tipoDocumento show-tick');
			campoTexto('numeroDocumentoJuridica','Número',$datos);
		echo "<br clear='all'><h2 class='apartadoFormulario'>Residencia</h1>";
		abreColumnaCampos();
			campoTexto('calleJuridica','Dirección',$datos);
			echo '<div class="hide">';
			campoTexto('numeroJuridica','Nº',$datos,'input-mini');
			echo '</div>';
		cierraColumnaCampos();
		abreColumnaCampos();
			campoTexto('cpJuridica','CP',$datos,'input-small');
			campoTexto('ciudadJuridica','Ciudad',$datos);
			selectPaises('paisJuridica',$datos,true);
		cierraColumnaCampos();

		echo "<br clear='all'><h2 class='apartadoFormulario'>Actividad</h1>";
		campoSelect('actividadLaboralJuridica','Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','SIN'),$datos,'selectpicker span3 show-tick actividadlaboral');
		campoCheckIndividualConClase('checkCargoPublicoJuridica','Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',$datos,'checkReforzadas');
		campoCheckIndividualConClase('checkMenorJuridica','Menor de edad',$datos,'checkReforzadas');
		campoCheckIndividualConClase('checkIncapacitadoJuridica','Incapacitado',$datos,'checkReforzadas');
		echo '</div>';
		creaTablaIdentificacionJuridica($datos);
		botonesNav(7);
	cierraPestaniaAPI();

	abrePestaniaAPI(7,$pagina==7);
		campoCheckIndividual('checkPersonasSinJuridicas','Actuación en representación de entidad sin personalidad jurídica',$datos);
		echo '<br/>Los sujetos obligados identificarán y comprobarán mediante documentos fehacientes la identidad de todos los partícipes de las entidades sin personalidad jurídica. No obstante, en el supuesto de entidades sin personalidad jurídica que no ejerzan actividades económicas bastará, con carácter general, con la identificación y comprobación mediante documentos fehacientes de la identidad de la persona que actúe por cuenta de la entidad';
		echo '<br/><br/><div id="divPersonasSinJuridicas" class="hide">';
		creaTablaPersonasSinJuridica($datos);
		echo '</div>';
		botonesNav(8);
	cierraPestaniaAPI();

	abrePestaniaAPI(8,$pagina==8);
		campoCheckIndividual('checkFideocomisos','Fideocomisos o instrumentos jurídicos análogos',$datos);
		echo '<br/>Los sujetos obligados requerirán el documento constitutivo, sin perjuicio de proceder a la identificación y comprobación de la identidad del fideicomitente, de los fideicomisarios, del protector, de los beneficiarios y de cualquier otra persona física que ejerza el control efectivo final sobre el fideicomiso.';
		echo '<br/><br/><div id="divFideocomisos" class="hide">';
		creaTablaFideocomisos($datos);
		echo '</div>';
		echo '<div id="pestania9" class="hide">';
		botonesNav(9);
		echo '</div><div id="pestania10">';
		botonesNav(10);
		echo '</div>';
	cierraPestaniaAPI();

	abrePestaniaAPI(9,$pagina==9);
		echo '<br/>En aquellos supuestos en que un fideicomisario no declare su condición de tal y se determine esta circunstancia por el sujeto obligado, se pondrá fin a la relación de negocios, procediendo a realizar el examen especial a que se refiere el artículo 17 de la Ley 10/2010, de 28 de abril';
		botonesNav(10);
	cierraPestaniaAPI();

	abrePestaniaAPI(10,$pagina==10);
		abreColumnaCampos('span5');
		echo '<u>Servicios sujetos a la LPBC</u>
		<div class="serviciosLPBC sujetos"><div class="fijos">';

		campoCheckIndividual('falsocheckServicio1','Auditoría de cuentas',false,'SI',false);
		campoCheckIndividual('falsocheckServicio2','Contabilidad externa',false,'SI',false);
		campoCheckIndividual('falsocheckServicio3','Asesoría fiscal',false,'SI',false);
		campoCheckIndividual('falsocheckServicio4','Concepción, realización o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de bienes inmuebles',false,'SI',false);
		campoCheckIndividual('falsocheckServicio5','Concepción, realización o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de entidades comerciales',false,'SI',false);
		campoCheckIndividual('falsocheckServicio6','Gestión de fondos, valores u otros activos',false,'SI',false);
		campoCheckIndividual('falsocheckServicio7','Apertura y/o gestión de cuentas corrientes, cuentas de ahorros o cuentas de valores',false,'SI',false);
		campoCheckIndividual('falsocheckServicio8','Organización de aportaciones para la creación, funcionamiento o gestión de empresas',false,'SI',false);
		campoCheckIndividual('falsocheckServicio9','Creación, funcionamiento o gestión de fideicomisos (trusts), sociedades o estructuras análogas',false,'SI',false);
		campoCheckIndividual('falsocheckServicio17','Actuación por cuenta de cliente en operación financiera',false,'SI',false);
		campoCheckIndividual('falsocheckServicio18','Actuación por cuenta de cliente en operación inmobiliaria',false,'SI',false);
		campoCheckIndividual('falsocheckServicio10','Constitución de sociedad u otra persona jurídica',false,'SI',false);
		campoCheckIndividual('falsocheckServicio11','Dirección o secretaría de sociedad, socio de una asociación, funciones similares con otras personas jurídicas',false,'SI',false);
		campoCheckIndividual('falsocheckServicio12','Facilitar un domicilio social o una dirección comercial, postal, administrativa y  otros servicios afines a una sociedad, asociación o cualquier otro instrumento o persona jurídicos',false,'SI',false);
		campoCheckIndividual('falsocheckServicio13','Ejercer funciones de fideicomisario en un fideicomiso expreso o instrumento jurídico similar',false,'SI',false);
		campoCheckIndividual('falsocheckServicio14','Disponer que otra persona ejerza funciones de fideicomisario en un fideicomiso expreso o instrumento jurídico similar',false,'SI',false);
		campoCheckIndividual('falsocheckServicio15','Ejercer funciones de accionista por cuenta de otra persona',false,'SI',false);
		campoCheckIndividual('falsocheckServicio16','Disponer qué persona ha de ejercer las funciones de accionista',false,'SI',false);
		campoCheckIndividual('falsocheckServicio19','Intermediación en la concesión de préstamos o créditos',false,'SI',false);
		campoCheckIndividual('falsocheckServicio20','Concesión/gestión de préstamos',false,'SI',false);
		campoCheckIndividual('falsocheckServicio21','Préstamos personales',false,'SI',false);
		campoCheckIndividual('falsocheckServicio22','Concesión/gestión de créditos',false,'SI',false);
		campoCheckIndividual('falsocheckServicio23','Crédito al consumo',false,'SI',false);
		campoCheckIndividual('falsocheckServicio24','Crédito hipotecario',false,'SI',false);
		campoCheckIndividual('falsocheckServicio25','Financiación de transacciones comerciales',false,'SI',false);
		campoCheckIndividual('falsocheckServicio26','Factoring, con o sin recurso',false,'SI',false);
		campoCheckIndividual('falsocheckServicio27','Arrendamiento financiero/actividades complementarias previstas en el párrafo 8 de la D.A. 7ª de la Ley 26/1988, de 29 de julio, de Disciplina e Intervención de las Entidades de Crédito',false,'SI',false);
		campoCheckIndividual('falsocheckServicio28','Emisión y gestión de tarjetas de crédito',false,'SI',false);
		campoCheckIndividual('falsocheckServicio29','Concesión de avales y garantías/suscripción de compromisos similares',false,'SI',false);
		echo '</div>';
		serviciosPersonalizadosFalso($datos);
		echo '</div>';
		cierraColumnaCampos();
		
		
		abreColumnaCampos('span5');
		echo '<u>Servicios contratados por el cliente</u>
		<div class="serviciosLPBC contratados"><div class="fijos">';
		campoCheckIndividualConFechas('checkServicio1','Auditoría de cuentas',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio2','Contabilidad externa',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio3','Asesoría fiscal',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio4','Concepción, realización o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de bienes inmuebles',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio5','Concepción, realización o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de entidades comerciales',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio6','Gestión de fondos, valores u otros activos',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio7','Apertura y/o gestión de cuentas corrientes, cuentas de ahorros o cuentas de valores',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio8','Organización de aportaciones para la creación, funcionamiento o gestión de empresas',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio9','Creación, funcionamiento o gestión de fideicomisos (trusts), sociedades o estructuras análogas',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio17','Actuación por cuenta de cliente en operación financiera',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio18','Actuación por cuenta de cliente en operación inmobiliaria',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio10','Constitución de sociedad u otra persona jurídica',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio11','Dirección o secretaría de sociedad, socio de una asociación, funciones similares con otras personas jurídicas',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio12','Facilitar un domicilio social o una dirección comercial, postal, administrativa y  otros servicios afines a una sociedad, asociación o cualquier otro instrumento o persona jurídicos',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio13','Ejercer funciones de fideicomisario en un fideicomiso expreso o instrumento jurídico similar',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio14','Disponer que otra persona ejerza funciones de fideicomisario en un fideicomiso expreso o instrumento jurídico similar',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio15','Ejercer funciones de accionista por cuenta de otra persona',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio16','Disponer qué persona ha de ejercer las funciones de accionista',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio19','Intermediación en la concesión de préstamos o créditos',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio20','Concesión/gestión de préstamos',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio21','Préstamos personales',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio22','Concesión/gestión de créditos',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio23','Crédito al consumo',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio24','Crédito hipotecario',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio25','Financiación de transacciones comerciales',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio26','Factoring, con o sin recurso',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio27','Arrendamiento financiero/actividades complementarias previstas en el párrafo 8 de la D.A. 7ª de la Ley 26/1988, de 29 de julio, de Disciplina e Intervención de las Entidades de Crédito',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio28','Emisión y gestión de tarjetas de crédito',$servicios,'SI',false);
		campoCheckIndividualConFechas('checkServicio29','Concesión de avales y garantías/suscripción de compromisos similares',$servicios,'SI',false);
		echo '</div>';
		serviciosPersonalizados($datos);
		echo '</div>';
		cierraColumnaCampos();

	echo "<br clear='all'><br/><br/><h2 class='apartadoFormulario'>Circunstancias</h1>";
		abreColumnaCampos('span5');
			campoCheckIndividualConClase('checkPortador','Relaciones de negocio y operaciones con clientes que emplean habitualmente medios de pago al portador',$datos,'checkReforzadas');
			campoCheckIndividualConClase('checkFondos','Se transfieren fondos de o hacía <b>países, territorios o jurisdicciones de riesgos</b>',$datos,'checkExamen');
			campoCheckIndividualConClase('checkVolumen','El volumen de la operación u operaciones, activas o pasivas, del cliente no se corresponden con su actividad o antecedentes operativos ',$datos,'checkExamen');
			campoCheckIndividualConClase('checkPrestacion1','En la prestación del servicio se detecta que una misma cuenta, sin causa que lo justifique, está siendo abonada mediante ingresos en efectivo por un número elevado de personas',$datos,'checkExamen');
			campoCheckIndividualConClase('checkPrestacion3','En la prestación del servicio se detecta pluralidad de transferencias realizadas por varios ordenantes a un mismo beneficiario en el exterior o por un único ordenante en el exterior a varios beneficiarios en España, sin que se aprecie relación de negocio entre los intervinientes',$datos,'checkExamen');
		cierraColumnaCampos();
		abreColumnaCampos('span5');
			campoCheckIndividualConClase('checkIntermediarios','Relaciones de negocio y operaciones ejecutadas a través de intermediarios',$datos,'checkReforzadas');
			campoCheckIndividualConClase('checkNaturaleza','La naturaleza de la operación u operaciones, activas o pasivas, del cliente no se corresponden con su actividad o antecedentes operativos',$datos,'checkExamen');
			campoCheckIndividualConClase('checkOperativa','Operativa con agentes que, por su naturaleza, volumen, cuantía, zona geográfica u otras características de las operaciones, difieran significativamente de las usuales u ordinarias del sector o de las propias del sujeto obligado',$datos,'checkExamen');
			campoCheckIndividualConClase('checkPrestacion2','En la prestación del servicio se detecta que una misma cuenta, sin causa que lo justifique, recibe múltiples ingresos en efectivo de la misma persona',$datos,'checkExamen');
			campoCheckIndividualConClase('checkPrestacion4','En la prestación del servicio se detectan transferencias en las que no se contiene la identidad del ordenante o el número de cuenta origen de la transferencia',$datos,'checkExamen');
		cierraColumnaCampos();
		echo '<br clear="all">';
		areaTexto('observaciones2','Observaciones',$datos);
		botonesNav(11);
	cierraPestaniaAPI();
	abrePestaniaAPI(11,$pagina==11);
		echo '<br/>';
		/*campoRadio('medidas','Medidas de diligencia debida aplicadas',$datos,'NORMALES',array('Simplificadas','Normales','Reforzadas','Examen especial'),array('SIMPLIFICADAS','NORMALES','REFORZADAS','EXAMEN'),true);*/
		campoDato('Medidas de diligencia debida aplicadas',false);
		campoCheckMedidas($datos);
		echo '<div id="textoNormales" class="textoMedidas">CLIENTE SUSCEPTIBLE DE APLICACIÓN DE MEDIDAS NORMALES DE DILIGENCIA DEBIDA, POR LO QUE HABRÁ QUE IDENTIFICAR Y COMPROBAR LA IDENTIDAD DE LA PERSONA QUE SOLICITE EL SERVICIO ASÍ COMO LA DEL TITULAR REAL, REGISTRAR LA ACTIVIDAD PROFESIONAL O EMPRESARIAL DECLARADA POR EL CLIENTE Y REALIZAR UN SEGUIMIENTO CONTINUO DE LA RELACIÓN DE NEGOCIOS MANTENIDA CON EL CLIENTE –SECCIÓN 1ª DEL CAPÍTULO II DEL RLPBC- </div>';
		echo '<div id="textoSimplificadas" class="textoMedidas">CLIENTE SUSCEPTIBLE DE APLICACIÓN DE MEDIDAS SIMPLIFICADAS DE DILIGENCIA DEBIDA -ART. 15 DEL RLPBC- PROCEDIENDO TAN SOLO A LA COMPROBACIÓN DE LA IDENTIDAD DE LAS PERSONAS FÍSICAS QUE NOS SOLICITAN EL SERVICIO Y LA ACREDITACIÓN DE LA REPRESENTACIÓN</div>';
		echo '<div id="textoReforzadas" class="textoMedidas">CLIENTE SUSCEPTIBLE DE APLICACIÓN DE MEDIDAS REFORZADAS DE DILIGENCIA DEBIDA, DEBIENDO IDENTIFICARSE AL CLIENTE MEDIANTE ALGUNO DE LOS MEDIOS  ESTABLECIDOS EN EL <a href="#" title="Artículo 21 RLPBC: Requisitos en las relaciones de negocio y operaciones no presenciales <button class=\'cierraPopover btn btn-propio\'><i class=\'icon-times\'></i></button>" position="right" class="enlacePopOver noAjax">ARTÍCULO 21 RLPBC</a><div class="hide">'.articulo21().'</div></div>';
		echo '<div id="textoReforzadas2" class="textoMedidas">CLIENTE SUSCEPTIBLE DE APLICACIÓN DE MEDIDAS REFORZADAS DE DILIGENCIA DEBIDA, PROCEDIENDO EN TODO CASO LA COMPROBACIÓN DE LAS ACTIVIDADES DECLARADAS POR EL CLIENTE ASÍ COMO LA IDENTIDAD DEL TITULAR REAL - <a href="#" title="Artículo 20 RLPBC: Medidas reforzadas de diligencia debida <button class=\'cierraPopover btn btn-propio\'><i class=\'icon-times\'></i></button>" position="right" class="enlacePopOver noAjax">ARTÍCULO 20 RLPBC</a><div class="hide">'.articulo20().'</div></div>';
		echo '<div id="textoExamen" class="textoMedidas">CLIENTE SUSCEPTIBLE DE SOMETIMIENTO A PROCESO INTEGRAL DE EXAMEN ESPECIAL, DEBIENDO ANALIZARSE TODA LA OPERATIVA RELACIONADA, TODOS LOS INTERVINIENTES EN LA OPERACIÓN Y TODA LA INFORMACIÓN RELEVANTE OBRANTE EN EL SUJETO OBLIGADO Y, EN SU CASO, EN EL GRUPO EMPRESARIAL – <a href="#" title="Artículo 25 RLPBC: Examen especial <button class=\'cierraPopover btn btn-propio\'><i class=\'icon-times\'></i></button>" position="bottom" class="enlacePopOver noAjax">ART. 25 RLPBC</a><div class="hide">'.articulo25().'</div></div>';
		echo "<br clear='all'><h2 class='apartadoFormulario'>Comprobaciones</h1><button class='btn btn-propio' id='marcarTodos'>Marcar todas</button><br/><br/><div class='comprobaciones'>";
		echo '<div id="bloque1" class="bloques">';
		campoComprobacion('checkDNI','<b>DNI/ PASAPORTE/ TARJETA DE RESIDENCIA/ TARJETA DE IDENTIDAD DE EXTRANJERO/ TARJETA OFICIAL DE IDENTIDAD PERSONAL EXPEDIDO POR LAS AUTORIDADES DE ORIGEN (ciudadanos de la Unión Europea)/ DOCUMENTO DE IDENTIDAD EXPEDIDO POR EL MINISTERIO DE ASUNTOS EXTERIORES Y DE COOPERACIÓN (personal de representaciones diplomáticas y consulares)/ Otro documento de identidad personal (excepcionalmente):</b> Para comprobar la identidad real del cliente.',$comprobaciones);
		campoComprobacion('checkEscritura1','<b>Escritura de la persona jurídica:</b> Para conocer la estructura de propiedad o control, actividad principal, distribución del capital social, domicilio social, cuantía del capital social y forma de desembolso.',$comprobaciones);
		campoComprobacion('checkPoderes','<b>Poderes:</b> Para que quede acreditada la representación.',$comprobaciones);
		echo '</div><div id="bloque2" class="bloques">';
		campoComprobacion('checkDeclaracion1','<b>Declaración responsable del cliente ó de la persona que tenga atribuida la representación de la persona jurídica:</b> Para identificar y comprobar la identidad del titular real de la persona jurídica.',$comprobaciones);
		echo '</div><div id="bloque3" class="bloques">';
		campoComprobacion('checkIdentificacion1','<b>Identificación del cliente no presencial: Firma electrónica; ó</b>',$comprobaciones);
		campoComprobacion('checkIdentificacion2','<b>Identificación del cliente no presencial: Primer ingreso procedente de una cuenta a nombre del mismo cliente</b> abierta en una entidad domiciliada en España, en la Unión Europea o en países terceros equivalentes; ó',$comprobaciones);
		campoComprobacion('checkIdentificacion3','<b>Identificación del cliente no presencial: Copia del documento de identidad,</b> de los establecidos en el <a href="#" class="enlacePopOver noAjax" position="bottom" title="Artículo 6 RLPBC: Documentos fehacientes a efectos de identificación formal">artículo 6 RLPBC</a><div class="hide">'.articulo6().'</div>, que corresponda, siempre que dicha copia esté <b>expedida por un fedatario público.</b>',$comprobaciones);
		campoComprobacion('checkRenta','<b>Declaración de Renta:</b> Para conocer los ingresos del cliente.',$comprobaciones);
		campoComprobacion('checkReintegro','<b>Reintegro o ingreso en cuenta bancaria:</b> Para conocer la entidad bancaria, el titular de la cuenta, origen (propio o préstamo) y destino de los fondos, así como su importe.',$comprobaciones);
		campoComprobacion('checkAutorizacion1','<b>Autorización para el movimiento de dinero en España:</b> Para acreditar el traslado de fondos dentro del territorio nacional.',$comprobaciones);
		campoComprobacion('checkAutorizacion2','<b>Autorización para sacar dinero de España:</b> Para acreditar los fondos que salen del territorio nacional.',$comprobaciones);
		campoComprobacion('checkBienes','<b>Declaración de bienes:</b> Para conocer los bienes del cliente.',$comprobaciones);
		campoComprobacion('checkLibro','<b>Libro de familia:</b> Para identificar a los familiares más próximos (cónyuge, padres e hijos).',$comprobaciones);
		campoComprobacion('checkImpuesto','<b>Impuesto de sociedades:</b> Para conocer los bienes, obligaciones y valor del patrimonio neto.',$comprobaciones);
		echo '</div><div id="bloque4" class="bloques">';
		campoComprobacion('checkCertificado1','<b>Certificado de vida laboral</b> Para conocer las entidades, el régimen y periodos de tiempo en los que ha trabajado cotizando a la Seguridad Social.',$comprobaciones);
		campoComprobacion('checkContrato1','<b>Contrato laboral:</b> Para conocer el último puesto de trabajo, periodo en el que permaneció desarrollando la actividad y condiciones de contratación del cliente.',$comprobaciones);
		campoComprobacion('checkCertificado2','<b>Certificado de empadronamiento:</b> Para conocer la residencia actual del interviniente, tercero y/o titular real.',$comprobaciones);
		campoComprobacion('checkCertificacion1','<b>Certificación diplomática de residencia en el extranjero:</b> Para acreditar la residencia de españoles en el extranjero.',$comprobaciones);
		campoComprobacion('checkCertificacion2','<b>Certificación literal de nacimiento:</b> Para acreditar fecha y lugar de nacimiento e identidad de los padres.',$comprobaciones);
		campoComprobacion('checkDeclaracion2','<b>Declaración censal:</b> Para conocer el domicilio social y domicilio fiscal de la entidad.',$comprobaciones);
		campoComprobacion('checkFacturas','<b>Facturas de consumos (teléfono, luz, agua, etc.) de personas jurídicas:</b> Para conocer el domicilio social y volumen de la actividad en base a los consumos realizados.',$comprobaciones);
		campoComprobacion('checkContrato2','<b>Contrato de alquiler:</b> Para verificar el domicilio social donde se realiza la actividad y verificar su coincidencia con el domicilio fiscal de la entidad.',$comprobaciones);
		campoComprobacion('checkDocumentoTC2','<b>Documento TC2:</b> Para conocer el domicilio social y volumen de la entidad en base al número de empleados.',$comprobaciones);
		campoComprobacion('checkDocumentoUnico','<b>Documento Único Administrativo (DUA/DAE):</b> Para conocer los intercambios (importaciones y/o exportaciones) de mercancías entre miembros comunitarios y terceros países, pudiendo también comprobar la identidad de la persona que firma el documento.',$comprobaciones);
		campoComprobacion('checkInforme','<b>Informe externo:</b> Para conocer bienes patrimoniales, valor de adquisición y antigüedad, cargos societarios y otros datos de interés de la persona jurídica.',$comprobaciones);
		campoComprobacion('checkNota','<b>Nota simple del Registro de la Propiedad:</b> Para conocer el propietario de un determinado bien, transmisiones realizadas y posibles cargas que tenga.',$comprobaciones);
		campoComprobacion('checkEscritura2','<b>Escritura del inmueble: Para acreditar la titularidad del inmueble y los derechos reales sobre el mismo.',$comprobaciones);
		echo '</div>';
		campoTexto('otraDocumentacion','Otra documentación',$comprobaciones,'span9');
		echo '</div>';
		botonesNav(12);
	cierraPestaniaAPI();
	abrePestaniaAPI(12,$pagina==12);
		$fecha = $datos ? $datos['fechaFinalizacion']:'0000-00-00';
		campoFecha('fechaFinalizacion','Fecha finalización prestación servicio',$fecha);
		creaTablaProfesionales($datos);
		echo '<br/>';
		$resultado = $datos ? $datos['resultado'] : 'PRESTAR';
		campoSelect('resultado','Resultado del análisis objeto del presente informe',array('Sin finalizar, pendiente de comprobaciones','Prestar el servicio al no detectar indicios de blanqueo de capitales','Proceder al examen especial'),array('SIN','PRESTAR','EXAMEN'), $resultado ,'selectpicker span6 show-tick');
		campoTexto('elaborado','El informe ha sido elaborado por',$datos,'span6');
		echo '<div id="divTextoFinal" style="margin-left:170px;margin-top:20px;">El presente informe se debe descargar, imprimir y firmar por las personas que adoptan la decisión, conservándolo durante 10 años junto a la documentación que  lo fundamente</div>';
		echo '<div id="pestania13" class="hide">';
		botonesNav(13);
		echo '</div>';
	cierraPestaniaAPI();
	abrePestaniaAPI(13,$pagina==13);
		echo '<br/>Los sujetos obligados examinarán con especial atención cualquier hecho y operación, con independencia de su cuantía, que, por su naturaleza, pueda estar relacionado con el blanqueo de capitales o la financiación del terrorismo, reseñando por escrito los resultados del examen. En particular, los sujetos obligados examinarán con especial atención toda operación o pauta de comportamiento compleja, inusual o sin un propósito económico o lícito aparente, o que presente indicios de simulación o fraude –art. 17 LPBC-';
			echo '<br/><br/><div id="informeRiesgo"></div>';
		botonesNav(14);
	cierraPestaniaAPI();
	abrePestaniaAPI(14,$pagina==14);
		campoSelectHTML('conclusion','Motivación',array('Se aprecia justificación económica, profesional o de negocio para la realización<br/>de las operaciones, por lo que no concurren en la operativa indicios o certeza<br/>de relación con el blanqueo de capitales o la financiación del terrorismo','No se aprecia justificación económica, profesional o de negocio para la realización<br/>de las operaciones, concurriendo en la operativa indicios o certeza de relación con<br/>el blanqueo de capitales o la financiación del terrorismo'),array(1,2),$datos,'selectpicker span6',"");
		echo '<div style="margin-left:170px;magin-bottom:20px;">El representante ó el órgano de control interno deben hacer constar expresamente la motivación de su decisión</div>';
		areaTexto('otraMotivacion','Otros motivos',$datos,'areaInforme'); 
		echo '<div id="pestania15" class="hide">';
		botonesNav(15);
		echo '</div><div id="pestania16">';
		botonesNav(16);
		echo '</div>';
	cierraPestaniaAPI();
	abrePestaniaAPI(15,$pagina==15);
		campoSelect('interrupcion','Interrupción de la relación de negocios',array('No','Si'),array('NO','SI'),$datos,'selectpicker span2','');
		echo '<div style="margin-left:170px;magin-bottom:20px;">La no interrupción viene determinada porque no sea posible interrumpir la relación de negocios, porque pueda dificultar la investigación o por la necesidad de no interferir en una entrega vigilada acordada conforme a lo dispuesto en el artículo 263 bis de la LECr</div>';
		areaTexto('motivosJustifican','Motivos que justifican la ejecución de la operación',$datos,'areaInforme');
		botonesNav(16);
	cierraPestaniaAPI();
	abrePestaniaAPI(16,$pagina==16);
		echo '<div id="divPestania16_1" style="margin-left:170px;margin-bottom:20px;">Procede la prestación del servicio solicitado al considerar que no concurren en la operativa indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo</div>';
		echo '<div id="divPestania16_2" class="hide" style="margin-left:170px;margin-bottom:20px;">Procede la comunicación, sin dilación, al SEPBLAC  –en el soporte y formato establecido: <a target="_blank" href="https://www.sepblac.es/es/sujetos-obligados/tramites/comunicacion-por-indicio/" target="_blank">https://www.sepblac.es/es/sujetos-obligados/tramites/comunicacion-por-indicio/</a> - por considerar que concurren en la operativa indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo</div>';
		$nombres=array('');
		$valores=array('NULL');
		if($datos){
			$manual=consultaBD('SELECT manuales_pbc.* FROM manuales_pbc INNER JOIN trabajos ON manuales_pbc.codigoTrabajo=trabajos.codigo WHERE codigoCliente='.$cliente['codigoCliente'],true,true);
			if($manual){
				if($manual['representante']!=''){
					array_push($nombres, $manual['representante']);
					array_push($valores, $manual['representante']);
				}
				$personas=consultaBD('SELECT * FROM personas_pbc WHERE codigoManual='.$manual['codigo'],true);
				while($p=mysql_fetch_assoc($personas)){
					array_push($nombres, $p['nombre']);
					array_push($valores, $p['nombre']);
				}
			}
		}
		//campoSelectMultiple('decision','Decisión adoptada por',$nombres,$valores,$datos,'selectpicker span6 show-tick');
		echo '<center>';
		campoDato('Decisión adoptada por',false);
		campoOculto($datos,'decision');
		creaTablaFirmados($datos);
		echo '</center>';
		echo '<div style="margin-left:170px;margin-bottom:20px;margin-top:20px;">En el supuesto en que la detección de la operación derive de la comunicación interna de un empleado, agente o directivo de la entidad, la decisión final adoptada sobre si procede o no la comunicación por indicio de la operación, será puesta en conocimiento del comunicante</div>';
		campoTexto('comunicante','Si se ha detectado la operación por la comunicación interna de un empleado, agente o directivo de la entidad, identificar al comunicante',$datos);
		campoFecha('fechaDecision','Fecha finalización examen especial',$datos);
		echo '<div id="divPestania16_2_otra" class="hide">';
			campoFecha('fechaSEPBLAC','Fecha comunicación al SEPBLAC',$datos);
		echo '</div>';
		echo '<div style="margin-left:170px;margin-top:20px;">El presente informe se debe descargar, imprimir y firmar por las personas que adoptan la decisión, conservándolo durante 10 años junto a la documentación que  lo fundamente</div>';
	cierraPestaniaAPI();
	/*abrePestaniaAPI(17,$pagina==17);
		echo '<div id="textoConclusion"></div>';
		echo "<br clear='all'><a href='#'' class='enlacePopOver noAjax apartadoFormulario' position='right' title='Motivación <button class=\"cierraPopover btn btn-propio\"><i class=\"icon-times\"></i></button>'>Motivación</a><div class='hide'>El representante ó el órgano de control interno deben hacer constar expresamente la motivación de su decisión</div>";
		echo '<br/><br/><div id="divInterrupcion">';
		
		echo '<div id="textoInterrupcion">LA NO INTERRUPCIÓN VIENE DETERMINADA POR LA NECESIDAD DE NO INTERFERIR EN UNA ENTREGA VIGILADA ACORDADA CONFORME A LO DISPUESTO EN EL ARTÍCULO 263 bis DE LA LECr</div></div>';
		botonesNav(18);
	cierraPestaniaAPI();
	abrePestaniaAPI(18,$pagina==18);
		
		campoDato('Decisión adoptada por','','decision');
		creaTablaFirmados($datos);
		echo '<br/><br/>En el supuesto en que la detección de la operación derive de la comunicación interna de un empleado, agente o directivo de la entidad, la decisión final adoptada sobre si procede o no la comunicación por indicio de la operación, será puesta en conocimiento del comunicante<br/><br/>';

		if($datos){
			echo 'ESTE DOCUMENTO SE DEBE IMPRIMIR Y FIRMAR (recuerde guardar antes de imprimir para visualizar los últimos cambios)<br/><br/><a href="generaDocumento.php?codigo='.$datos['codigo'].'" class="btn btn-propio noAjax" target="_blank"><i class="icon-download"></i> Imprimir</a>';
		} else {
			echo 'GUARDA EL INFORME PARA PODER IMPRIMIRLO';
		}
	cierraPestaniaAPI();*/

	cierraPestaniasAPI();

	cierraVentanaGestionInforme($recargar,'index.php',true);
}

function serviciosPersonalizadosFalso($datos){
	$codigoUsuario=0;
	if($_SESSION['tipoUsuario']=='ADMIN'){
		if($datos){
			$codigoUsuario=$datos['codigoUsuario'];
		}
	} else {
		$codigoUsuario=$_SESSION['codigoU'];
	}
	$i=0;
	echo "<div class='table-responsive'>";
	echo "<table class='table table-striped tabla-simple tablaServiciosPersonalizados' id='tablaServiciosPersonalizadosFalso'>
				  	<tbody>";
				  	$servicios=consultaBD('SELECT codigo, nombre FROM servicios_informes_clientes_personalizados WHERE codigoUsuario='.$codigoUsuario,true);
				  	while($s=mysql_fetch_assoc($servicios)){
				  		$ss=false;
				  		if($datos){
				  			$ss=consultaBD('SELECT * FROM servicios_informes_clientes_personalizados_seleccionados WHERE codigoServicio='.$s['codigo'].' AND codigoInforme='.$datos['codigo'],true,true);
				  		}
				  		if($datos && $ss['checkSeleccionado']=='SI'){
				  			echo '<tr style="display:none">';
				  		} else {
				  			echo '<tr>';
				  		}
				  			campoCheckTabla('falsocheckServicioPersonalizado'.$i,'',$s['codigo']);
				  			campoTextoTabla('falsonombreServicioPersonalizado'.$i,$s['nombre'],'input-large nombreServicioPersonalizado');
				  			echo "<td><input type='checkbox' name='filasTabla[]' value='$i'></td>";
				  		echo '</tr>';
				  		$i++;
				  	}
				  	echo '<tr>';
				  		campoCheckTabla('falsocheckServicioPersonalizado'.$i);
				  		campoTextoTabla('falsonombreServicioPersonalizado'.$i,'','input-large nombreServicioPersonalizado');
				  		echo "<td><input type='checkbox' name='filasTabla[]' value='$i'></td>";
				  	echo '</tr>';
	echo "			</tbody>
				</table>
				<center>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFilaServiciosPersonalizados(\"tablaServiciosPersonalizadosFalso\");'><i class='icon-plus'></i></button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaServiciosPersonalizados(\"tablaServiciosPersonalizadosFalso\");'><i class='icon-trash'></i></button>
	            </center>";
	echo "</div>";
}

function serviciosPersonalizados($datos){
	$codigoUsuario=0;
	if($_SESSION['tipoUsuario']=='ADMIN'){
		if($datos){
			$codigoUsuario=$datos['codigoUsuario'];
		}
	} else {
		$codigoUsuario=$_SESSION['codigoU'];
	}
	$i=0;
	echo "<div class='table-responsive'>
				<table class='table table-striped tabla-simple tablaServiciosPersonalizados' id='tablaServiciosPersonalizados'>
				  	<tbody>";
				  	$servicios=consultaBD('SELECT codigo, nombre FROM servicios_informes_clientes_personalizados WHERE codigoUsuario='.$codigoUsuario,true);
				  	while($s=mysql_fetch_assoc($servicios)){
				  		$ss=false;
				  		if($datos){
				  		$ss=consultaBD('SELECT * FROM servicios_informes_clientes_personalizados_seleccionados WHERE codigoServicio='.$s['codigo'].' AND codigoInforme='.$datos['codigo'],true,true);
				  		}
				  		if($datos && $ss['checkSeleccionado']=='SI'){
				  			echo '<tr>';
				  			campoCheckTabla('checkServicioPersonalizado'.$i,$s['codigo'],$s['codigo']);
				  		} else {
				  			echo '<tr style="display:none">';
				  			campoCheckTabla('checkServicioPersonalizado'.$i,'',$s['codigo']);
				  		}
				  			echo "<td><input type='text' class='input-large' id='nombreServicioPersonalizado".$i."' name='nombreServicioPersonalizado".$i."' readonly='readonly' value='".$s['nombre']."' /><br/>
				  				<div class='fechasServiciosPersonalizados'>
				  					<div>Contratación</div>
				  					<div>Finalización</div>
				  					<div>";
				  					campoTextoSolo('fechaContratacionServicioPersonalizado'.$i,formateaFechaWeb($ss['fechaContratacion']),'input-small datepicker hasDatepicker');
				  			echo "	</div>
				  					<div>";
				  					campoTextoSolo('fechaFinalizacionServicioPersonalizado'.$i,formateaFechaWeb($ss['fechaFinalizacion']),'input-small datepicker hasDatepicker');

				  		echo "		</div>
				  				</div>";

				  		echo '</tr>';
				  		$i++;
				  	}
				  	echo '<tr style="display:none;">';
				  		campoCheckTabla('checkServicioPersonalizado'.$i);
				  		echo "<td><input type='text' class='input-large' id='nombreServicioPersonalizado".$i."' name='nombreServicioPersonalizado".$i."' readonly='readonly' /><br/>
				  				<div class='fechasServiciosPersonalizados'>
				  					<div>Contratación</div>
				  					<div>Finalización</div>
				  					<div>";
				  					campoTextoSolo('fechaContratacionServicioPersonalizado'.$i,'','input-small datepicker hasDatepicker');
				  		echo "	</div>
				  				<div>";
				  					campoTextoSolo('fechaFinalizacionServicioPersonalizado'.$i,'','input-small datepicker hasDatepicker');

				  		echo "		</div>
				  				</div>";

				  	echo '</tr>';
	echo "			</tbody>
				</table>
		</div>";
}

function campoComprobacion($campo,$texto,$comprobaciones){
	if($comprobaciones['codigoInforme']==0){
		$eliminado=false;
		$comprobaciones=false;
	} else {
		$eliminado=consultaBD('SELECT * FROM comprobaciones_informes_clientes_eliminadas WHERE codigoInforme='.$comprobaciones['codigoInforme'].' AND campo="'.$campo.'";',true, true);
	}
	$clases=array('SI'=>'','NO'=>'class="hide"','eliminado'=>'NO');
	if($eliminado){
		$clases['SI']='class="hide"';
		$clases['NO']='';
		$clases['eliminado']='SI';
	}
	echo '<div id="'.$campo.'SI" '.$clases['SI'].'>';
	$boton='<button class=" btn btn-propio eliminaComprobacion" campo="'.$campo.'"><i class="icon-trash-o"></i></button>';
	campoCheckIndividual($campo,$texto.' '.$boton,$comprobaciones);
	echo '</div>';
	echo '<div id="'.$campo.'NO" '.$clases['NO'].'>';
	echo '<button title="'.strip_tags($texto).'" class=" btn btn-propio activaComprobacion" campo="'.$campo.'"><i class="icon-undo"></i></button>';
	echo '</div>';
	campoOculto($clases['eliminado'],$campo.'Eliminado');
}

function gestionServicios(){
	desencriptaGET();

	if($_SESSION['tipoUsuario'] != 'ADMIN' && isset($_GET['codigo'])){
		$registro = datosRegistro("informes_clientes", $_GET['codigo']);    	
		compruebaAcceso($registro['codigoUsuario']);
	}	

	operacionesInformes();
	$informe=consultaBD("SELECT informes_clientes.codigo, informes_clientes.nombre, clientes.razonSocial FROM informes_clientes INNER JOIN usuarios ON informes_clientes.codigoUsuario=usuarios.codigo INNER JOIN usuarios_clientes ON usuarios.codigo=usuarios_clientes.codigoUsuario INNER JOIN clientes ON usuarios_clientes.codigoCliente=clientes.codigo WHERE informes_clientes.codigo=".$_GET['codigo'],true,true);

	$arrayServicios=obtieneArrayServicios();
	$nombresS=array('');
	$valoresS=array('');
	$servicios=consultaBD('SELECT * FROM servicios_informes_clientes WHERE codigoInforme='.$informe['codigo'],true,true);
	for($i=1;$i<=29;$i++){
		if($servicios['checkServicio'.$i]=='SI'){
			array_push($valoresS, $i.'_FIJO');
			array_push($nombresS, $arrayServicios[$i]);
		}
	}

	$servicios=consultaBD('SELECT s2.* FROM servicios_informes_clientes_personalizados_seleccionados s1 INNER JOIN servicios_informes_clientes_personalizados s2 ON s1.codigoServicio=s2.codigo WHERE codigoInforme='.$informe['codigo'],true);
	while($s=mysql_fetch_assoc($servicios)){
		array_push($valoresS, $s['codigo'].'_PERS');
		array_push($nombresS, $s['nombre']);
	}

	$nombres=array('');
	$valores=array('');
	$listado=consultaBD('SELECT * FROM personas_fisicas_informes_clientes WHERE codigoInforme='.$informe['codigo'],true);
	while($item=mysql_fetch_assoc($listado)){
		array_push($nombres, $item['nombreFisica']);
		array_push($valores, $item['codigo'].'_FISICAS');
	}
	$listado=consultaBD('SELECT * FROM personas_juridicas_informes_clientes WHERE codigoInforme='.$informe['codigo'],true);
	while($item=mysql_fetch_assoc($listado)){
		array_push($nombres, $item['razonSocial']);
		array_push($valores, $item['codigo'].'_JURIDICAS');
	}
	$listado=consultaBD('SELECT * FROM personas_sinjuridicas_informes_clientes WHERE codigoInforme='.$informe['codigo'],true);
	while($item=mysql_fetch_assoc($listado)){
		array_push($nombres, $item['nombreSinJuridica']);
		array_push($valores, $item['codigo'].'_SINJURIDICAS');
	}
	$listado=consultaBD('SELECT * FROM personas_fideocomisos_informes_clientes WHERE codigoInforme='.$informe['codigo'],true);
	while($item=mysql_fetch_assoc($listado)){
		array_push($nombres, $item['nombreFideocomiso']);
		array_push($valores, $item['codigo'].'_FIDEOCOMISOS');
	}
	abreVentanaGestion('Gestión de servicios','gestion.php?servicios&codigo='.$informe['codigo']);
	campoDato('Cliente',$informe['razonSocial']);
	campoDato('Solicitante',$informe['nombre']);
	campoOculto($informe['codigo'],'codigoInforme');
	echo "<br/><center>
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaServicios'>
				  	<thead>
				    	<tr>
				            <th> Servicio </th>
				            <th> Persona </th>
				            <th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";
				  	$i=0;
				  	$listado=consultaBD("SELECT * FROM servicios_personas_informes_clientes WHERE codigoInforme=".$informe['codigo'],true);
				  	while($item=mysql_fetch_assoc($listado)){
				  		$j=$i+1;
				  		echo '<tr>';
				  			campoOculto($item['codigo'],'codigoExiste'.$i);
				  			campoSelect('servicio'.$i,'',$nombresS,$valoresS,$item['servicio'].'_'.$item['tipoServicio'],'selectpicker span5 show-tick',"data-live-search='true'",1);
				  			campoSelect('persona'.$i,'',$nombres,$valores,$item['codigoPersona'],'selectpicker span5 show-tick',"data-live-search='true'",1);
				  			echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>";
				  		echo '</tr>';
				  		$i++;
				  	}
				  	if($i==0){
				  		$j=1;
				  		echo '<tr>';
				  			campoSelect('servicio'.$i,'',$nombresS,$valoresS,false,'selectpicker span5 show-tick',"data-live-search='true'",1);
				  			campoSelect('persona'.$i,'',$nombres,$valores,false,'selectpicker span5 show-tick',"data-live-search='true'",1);
				  			echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>";
				  		echo '</tr>';
				  	}
	echo "      	</tbody>
	            </table>
	            <center>
	            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaServicios\");'><i class='icon-plus'></i> Añadir</button> 
	            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaServicios\");'><i class='icon-trash'></i> Eliminar</button>
	            </center>";
	cierraVentanaGestion('index.php');
}


function creaTablaClientes($datos=false){
	conexionBD();

	echo "	              
			<center>
			Deja el campo vacío para borrar el cliente   
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaClientes'>
				  	<thead>
				    	<tr>
				            <th> Clientes </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM clientes_informes_clientes WHERE codigoInforme=".$datos['codigo']);
				  			while($persona=mysql_fetch_assoc($consulta)){
				  				echo '<tr>';
				  					campoTextoTabla('persona'.$i,$persona['persona'],'span7');
				  				echo '</tr>';
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr>';
				  			campoTextoTabla('persona'.$i,'','span7');
				  			echo '</tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaClientes\");'><i class='icon-plus'></i> Añadir cliente</button> 
				</div>
			</div>
			</center>";

	cierraBD();
}

function creaTablaIdentificacion($datos){
	echo '<br/><br/><br/>Deja el campo \'Nombre y apellidos\' vacío para borrar la persona  
		<table id="tablaIdentificacion">';
		$i=0;
		if($datos){
			$consulta=consultaBD("SELECT * FROM informes_clientes_fisicas WHERE codigoInforme=".$datos['codigo'],true);
			while($persona=mysql_fetch_assoc($consulta)){
				echo '<tr><td>';
				campoOculto($persona['codigo'],'codigoExiste'.$i);
				echo "<h2 class='apartadoFormulario'>Persona física que solicita el servicio</h1>";
				campoTexto('nombre'.$i,'Nombre y apellidos',$persona['nombre'],'span5');
				campoSelect('tipoDocumento'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),$persona['tipoDocumento'],'selectpicker span8 tipoDocumento show-tick');
				campoTexto('numeroDocumento'.$i,'Número',$persona['numeroDocumento']);
				echo "<br clear='all'><h2 class='apartadoFormulario'>Residencia</h1>";
				abreColumnaCampos();
					campoTexto('calle'.$i,'Dirección',$persona['calle']);
					echo '<div class="hide">';
						campoTexto('numero'.$i,'Nº',$persona['numero'],'input-mini');
					echo '</div>';
				cierraColumnaCampos();
				abreColumnaCampos();
					campoTexto('cp'.$i,'CP',$persona['cp'],'input-small');
					campoTexto('localidad'.$i,'Localidad',$persona['localidad']);
					campoTexto('ciudad'.$i,'Ciudad',$persona['ciudad']);
					selectPaises('pais'.$i,$persona['pais'],true);
				cierraColumnaCampos();

				echo "<br clear='all'><h2 class='apartadoFormulario'>Actividad</h1>";
				campoSelect('actividadLaboral'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),$persona['actividadLaboral'],'selectpicker span3 show-tick actividadlaboral');
				campoCheckIndividualConClase('checkCargoPublico'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',$persona['checkCargoPublico'],'checkReforzadas');
				campoCheckIndividualConClase('checkNoPresencial'.$i,'Solicita servicio de forma <a href="#" title="Artículo 21 RLPBC: Requisitos en las relaciones de negocio y operaciones no presenciales <button class=\'cierraPopover btn btn-propio\'><i class=\'icon-times\'></i></button>" class="enlacePopOver noAjax" position="right">no presencial</a><div class="hide">'.articulo21().'</div>',$persona['checkNoPresencial'],'checkReforzadas');
				echo '<div class="divSolicitaServicio">';
					campoRadio('solicitaServicio'.$i,'Solicita el servicio',$persona['solicitaServicio'],1,array('En nombre propio','En nombre propio y de 3º/s','En representación de tercero/s'),array(1,2,3),true);
				echo '</div>';
				areaTexto('observaciones'.$i,'Observaciones',$persona['observaciones']);
				echo '</td></tr>';
				$i++;
			}
		}
		if($i==0){
				echo '<tr><td>';
				echo "<h2 class='apartadoFormulario'>Persona física que solicita el servicio</h1>";
				campoTexto('nombre'.$i,'Nombre y apellidos',false,'span5');
				campoSelect('tipoDocumento'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),false,'selectpicker span8 tipoDocumento show-tick');
				campoTexto('numeroDocumento'.$i,'Número',false);
				echo "<br clear='all'><h2 class='apartadoFormulario'>Residencia</h1>";
				abreColumnaCampos();
					campoTexto('calle'.$i,'Dirección',false);
					echo '<div class="hide">';
						campoTexto('numero'.$i,'Nº',false,'input-mini');
					echo '</div>';
				cierraColumnaCampos();
				abreColumnaCampos();
					campoTexto('cp'.$i,'CP',false,'input-small');
					campoTexto('localidad'.$i,'Localidad',false);
					campoTexto('ciudad'.$i,'Ciudad',false);
					selectPaises('pais'.$i,false,true);
				cierraColumnaCampos();

				echo "<br clear='all'><h2 class='apartadoFormulario'>Actividad</h1>";
				campoSelect('actividadLaboral'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),false,'selectpicker span3 show-tick actividadlaboral');
				campoCheckIndividualConClase('checkCargoPublico'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',false,'checkReforzadas');
				campoCheckIndividualConClase('checkNoPresencial'.$i,'Solicita servicio de forma <a href="#" title="Artículo 21 RLPBC: Requisitos en las relaciones de negocio y operaciones no presenciales <button class=\'cierraPopover btn btn-propio\'><i class=\'icon-times\'></i></button>" class="enlacePopOver noAjax" position="right">no presencial</a><div class="hide">'.articulo21().'</div>',false,'checkReforzadas');
				echo '<div class="divSolicitaServicio">';
					campoRadio('solicitaServicio'.$i,'Solicita el servicio',false,1,array('En nombre propio','En nombre propio y de 3º/s','En representación de tercero/s'),array(1,2,3),true);
				echo '</div>';
				areaTexto('observaciones'.$i,'Observaciones');
				echo '</td></tr>';
		}
		
	echo '</table>';
	echo "<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaIdentificacion\");'><i class='icon-plus'></i> Añadir persona</button> 
				</div>";
}

function creaTablaIdentificacionJuridica($datos){
	echo '<br/><br/><br/>Deja el campo \'Nombre y apellidos\' vacío para borrar la persona  
		<table id="tablaIdentificacionJuridica">';
		$i=0;
		if($datos){
			$consulta=consultaBD("SELECT * FROM informes_clientes_juridicas WHERE codigoInforme=".$datos['codigo'],true);
			while($persona=mysql_fetch_assoc($consulta)){
				echo '<tr><td>';
				campoOculto($persona['codigo'],'codigoExisteIdentificacionJuridica'.$i);
				echo "<h2 class='apartadoFormulario'>Identificación de titular real de la persona jurídica</h1>";
				campoTexto('nombreIdentificacionJuridica'.$i,'Nombre y apellidos',$persona['nombre'],'span5');
				campoSelect('tipoDocumentoIdentificacionJuridica'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),$persona['tipoDocumento'],'selectpicker span8 tipoDocumento show-tick');
				campoTexto('numeroDocumentoIdentificacionJuridica'.$i,'Número',$persona['numeroDocumento']);
				echo "<br clear='all'><h2 class='apartadoFormulario'>Residencia</h1>";
				abreColumnaCampos();
					campoTexto('calleIdentificacionJuridica'.$i,'Dirección',$persona['calle']);
					echo '<div class="hide">';
						campoTexto('numeroIdentificacionJuridica'.$i,'Nº',$persona['numero'],'input-mini');
					echo '</div>';
				cierraColumnaCampos();
				abreColumnaCampos();
					campoTexto('cpIdentificacionJuridica'.$i,'CP',$persona['cp'],'input-small');
					campoTexto('localidadIdentificacionJuridica'.$i,'Localidad',$persona['localidad']);
					campoTexto('ciudadIdentificacionJuridica'.$i,'Ciudad',$persona['ciudad']);
					selectPaises('paisIdentificacionJuridica'.$i,$persona['pais'],true);
				cierraColumnaCampos();

				echo "<br clear='all'><h2 class='apartadoFormulario'>Actividad</h1>";
				campoSelect('actividadLaboralIdentificacionJuridica'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),$persona['actividadLaboral'],'selectpicker span3 show-tick actividadlaboral');
				campoCheckIndividualConClase('checkCargoPublicoIdentificacionJuridica'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',$persona['checkCargoPublico'],'checkReforzadas');
				campoCheckIndividualConClase('checkMenorIdentificacionJuridica'.$i,'Menor de edad',$persona['checkMenor'],'checkReforzadas');
				campoCheckIndividualConClase('checkIncapacitadoIdentificacionJuridica'.$i,'Incapacitado',$persona['checkIncapacitado'],'checkReforzadas');
				echo '</td></tr>';
				$i++;
			}
		}
		if($i==0){
				echo '<tr><td>';
				echo "<h2 class='apartadoFormulario'>Identificación de titular real de la persona jurídica</h1>";
				campoTexto('nombreIdentificacionJuridica'.$i,'Nombre y apellidos',false,'span5');
				campoSelect('tipoDocumentoIdentificacionJuridica'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),false,'selectpicker span8 tipoDocumento show-tick');
				campoTexto('numeroDocumentoIdentificacionJuridica'.$i,'Número');
				echo "<br clear='all'><h2 class='apartadoFormulario'>Residencia</h1>";
				abreColumnaCampos();
					campoTexto('calleIdentificacionJuridica'.$i,'Dirección');
					echo '<div class="hide">';
						campoTexto('numeroIdentificacionJuridica'.$i,'Nº',false,'input-mini');
					echo '</div>';
				cierraColumnaCampos();
				abreColumnaCampos();
					campoTexto('cpIdentificacionJuridica'.$i,'CP',false,'input-small');
					campoTexto('localidadIdentificacionJuridica'.$i,'Localidad');
					campoTexto('ciudadIdentificacionJuridica'.$i,'Ciudad');
					selectPaises('paisIdentificacionJuridica'.$i,false,true);
				cierraColumnaCampos();

				echo "<br clear='all'><h2 class='apartadoFormulario'>Actividad</h1>";
				campoSelect('actividadLaboralIdentificacionJuridica'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),false,'selectpicker span3 show-tick actividadlaboral');
				campoCheckIndividualConClase('checkCargoPublicoIdentificacionJuridica'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',false,'checkReforzadas');
				campoCheckIndividualConClase('checkMenorIdentificacionJuridica'.$i,'Menor de edad',false,'checkReforzadas');
				campoCheckIndividualConClase('checkIncapacitadoIdentificacionJuridica'.$i,'Incapacitado',false,'checkReforzadas');
				echo '</td></tr>';
		}
		
	echo '</table>';
	echo "<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaIdentificacionJuridica\");'><i class='icon-plus'></i> Añadir titular</button> 
				</div>";
}

function creaTablaPersonasFisicas($datos=false){
	conexionBD();

	echo "	              
			<center>   
			Deja el campo 'Nombre y apellidos' vacío para borrar la persona  
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaPersonasFisicas'>
				  	<thead>
				    	<tr>
				            <th> DATOS DE LA PERSONA FÍSICA REPRESENTADA </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM personas_fisicas_informes_clientes WHERE codigoInforme=".$datos['codigo']);
				  			while($persona=mysql_fetch_assoc($consulta)){
				  				echo '<tr><td><br/>';
				  				campoOculto($persona['codigo'],'codigoExisteFisica'.$i);
				  				campoTexto('nombreFisica'.$i,'Nombre y apellidos',$persona['nombreFisica']);
								campoSelect('tipoDocumentoFisica'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),$persona['tipoDocumentoFisica'],'selectpicker span8 tipoDocumento show-tick',$persona['tipoDocumentoFisica']);
								campoTexto('numeroDocumentoFisica'.$i,'Número',$persona['numeroDocumentoFisica']);
								abreColumnaCampos();
									campoTexto('calleFisica'.$i,'Dirección',$persona['calleFisica']);
									echo '<div class="hide">';
									campoTexto('numeroFisica'.$i,'Nº',$persona['numeroFisica'],'input-mini');
									echo '</div>';
								cierraColumnaCampos();
								abreColumnaCampos();
									campoTexto('cpFisica'.$i,'CP',$persona['cpFisica'],'input-small');
									campoTexto('localidadFisica'.$i,'Localidad',$persona['localidadFisica']);
									campoTexto('ciudadFisica'.$i,'Ciudad',$persona['ciudadFisica']);
									selectPaises('paisFisica'.$i,$persona['paisFisica'],true);
								cierraColumnaCampos();
								campoSelect('actividadLaboralFisica'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),$persona['actividadLaboralFisica'],'selectpicker span3 show-tick actividadlaboral');
								campoCheckIndividualConClase('checkCargoPublicoFisica'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',$persona['checkCargoPublicoFisica'],'checkReforzadas');
								campoCheckIndividualConClase('checkMenorFisica'.$i,'Menor de edad',$persona['checkMenorFisica'],'checkReforzadas');
								campoCheckIndividualConClase('checkIncapacitadoFisica'.$i,'Incapacitado',$persona['checkIncapacitadoFisica'],'checkReforzadas');
								echo '<br/>';
								areaTexto('observacionesFisica'.$i,'Observaciones',$persona['observacionesFisica']);
				  			echo '</td></tr>';
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr><td><br/>';
				  				campoTexto('nombreFisica'.$i,'Nombre y apellidos');
								campoSelect('tipoDocumentoFisica'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),'','selectpicker span8 tipoDocumento show-tick');
								campoTexto('numeroDocumentoFisica'.$i,'Número');
								abreColumnaCampos();
									campoTexto('calleFisica'.$i,'Dirección');
									echo '<div class="hide">';
									campoTexto('numeroFisica'.$i,'Nº','','input-mini');
									echo '</div>';
								cierraColumnaCampos();
								abreColumnaCampos();
									campoTexto('cpFisica'.$i,'CP','','input-small');
									campoTexto('localidadFisica'.$i,'Localidad',$persona['localidadFisica']);
									campoTexto('ciudadFisica'.$i,'Ciudad');
									selectPaises('paisFisica'.$i,'',true);
								cierraColumnaCampos();
								campoSelect('actividadLaboralFisica'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),'','selectpicker span3 show-tick actividadlaboral');
								campoCheckIndividualConClase('checkCargoPublicoFisica'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público','','checkReforzadas');
								campoCheckIndividualConClase('checkMenorFisica'.$i,'Menor de edad','','checkReforzadas');
								campoCheckIndividualConClase('checkIncapacitadoFisica'.$i,'Incapacitado','','checkReforzadas');
								echo '<br/>';
								areaTexto('observacionesFisica'.$i,'Observaciones');
				  			echo '</td></tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaPersonasFisicas\");'><i class='icon-plus'></i> Añadir persona</button> 
				</div>
			</div>
			</center>";

	cierraBD();
}

function creaTablaPersonasJuridicas($datos=false){
	conexionBD();

	echo "	              
			<center>
			Deja el campo 'Nombre comercial' vacío para borrar la persona  
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaPersonasJuridicas'>
				  	<thead>
				    	<tr>
				            <th> DATOS DE LA PERSONA JURÍDICA REPRESENTADA </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM personas_juridicas_informes_clientes WHERE codigoInforme=".$datos['codigo']);
				  			while($persona=mysql_fetch_assoc($consulta)){
				  				echo '<tr><td><br/>';
				  				campoOculto($persona['codigo'],'codigoExisteJuridica'.$i);
				  				campoTexto('nombreJuridica'.$i,'Nombre comercial',$persona['nombreJuridica']);
				  				campoTexto('razonSocial'.$i,'Razón social',$persona['razonSocial']);
								campoTexto('formaJuridica'.$i,'Forma jurídica',$persona['formaJuridica']);
								campoTexto('numCIF'.$i,'Núm. CIF',$persona['numCIF']);
								campoTexto('objetoSocial'.$i,'Objeto social',$persona['objetoSocial']);
								abreColumnaCampos();
									campoTexto('calleJuridica'.$i,'Dirección',$persona['calleJuridica']);
									echo '<div class="hide">';
									campoTexto('numeroJuridica'.$i,'Nº',$persona['numeroJuridica'],'input-mini');
									echo '</div>';
								cierraColumnaCampos();
								abreColumnaCampos();
									campoTexto('cpJuridica'.$i,'CP',$persona['cpJuridica'],'input-small');
									campoTexto('localidadJuridica'.$i,'Localidad',$persona['localidadJuridica']);
									campoTexto('ciudadJuridica'.$i,'Ciudad',$persona['ciudadJuridica']);
									selectPaises('paisJuridica'.$i,$persona['paisJuridica'],true);
								cierraColumnaCampos();
								echo '<div class="checksInformes">';
								campoCheckIndividualConClase('checkEntidadDerecho'.$i,'Entidad de derecho público de Estado miembro de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>país tercero equivalente</a>',$persona['checkEntidadDerecho'],'susceptibleSimplificadas');
								campoCheckIndividualConClase('checkSociedadPersona'.$i,'Sociedad o persona jurídica controlada o participada mayoritariamente por entidades de derecho público de los Estados miembros de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>países tercero equivalente</a>',$persona['checkSociedadPersona'],'susceptibleSimplificadas');
								campoCheckIndividualConClase('checkEntidadFinanciera'.$i,'Entidad financiera -exceptuadas las entidades de pago- domiciliada en la Unión Europea o en <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>país tercero equivalente</a>, que sea objeto de supervisión para garantizar el cumplimiento de las obligaciones de prevención del blanqueo de capitales y de la financiación del terrorismo',$persona['checkEntidadFinanciera'],'susceptibleSimplificadas');
								campoCheckIndividualConClase('checkSucursalFilial'.$i,'Sucursal o filial de entidades financieras -exceptuadas las entidades de pago-, domiciliada en la Unión Europea o en <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>país tercero equivalente</a>, cuando esté sometida por la matriz a procedimientos de prevención del blanqueo de capitales y de la financiación del terrorismo.',$persona['checkSucursalFilial'],'susceptibleSimplificadas');
								campoCheckIndividualConClase('checkSociedadCotizada'.$i,'Sociedad cotizada cuyos valores se admiten a negociación en un mercado regulado de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>país tercero equivalente</a>, sus sucursales y filiales participadas mayoritariamente',$persona['checkSociedadCotizada'],'susceptibleSimplificadas');
								campoCheckIndividualConClase('checkSociedadMera'.$i,'Sociedad de mera tenencia de activos ',$persona['checkSociedadMera'],'checkReforzadas');
								campoCheckIndividualConClase('checkSociedadCuya'.$i,'Sociedad cuya estructura accionarial y de control no sea transparente o resulte inusual o excesivamente compleja',$persona['checkSociedadCuya'],'checkReforzadas');
								echo '</div>';
								echo '<br/>';
								areaTexto('observacionesJuridica'.$i,'Observaciones',$persona['observacionesJuridica']);
				  			echo '</td></tr>';
				  			$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr><td><br/>';
				  				campoTexto('nombreJuridica'.$i,'Nombre comercial');
				  				campoTexto('razonSocial'.$i,'Razón social');
								campoTexto('formaJuridica'.$i,'Forma jurídica');
								campoTexto('numCIF'.$i,'Núm. CIF');
								campoTexto('objetoSocial'.$i,'Objeto social');
								abreColumnaCampos();
									campoTexto('calleJuridica'.$i,'Dirección');
									echo '<div class="hide">';
									campoTexto('numeroJuridica'.$i,'Nº','','input-mini');
									echo '</div>';
								cierraColumnaCampos();
								abreColumnaCampos();
									campoTexto('cpJuridica'.$i,'CP','','input-small');
									campoTexto('localidadJuridica'.$i,'Localidad');
									campoTexto('ciudadJuridica'.$i,'Ciudad');
									selectPaises('paisJuridica'.$i,'',true);
								cierraColumnaCampos();
								echo '<div class="checksInformes">';
								campoCheckIndividualConClase('checkEntidadDerecho'.$i,'Entidad de derecho público de Estado miembro de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>país tercero equivalente</a>','','susceptibleSimplificadas');
								campoCheckIndividualConClase('checkSociedadPersona'.$i,'Sociedad o persona jurídica controlada o participada mayoritariamente por entidades de derecho público de los Estados miembros de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>países tercero equivalente</a>','','susceptibleSimplificadas');
								campoCheckIndividualConClase('checkEntidadFinanciera'.$i,'Entidad financiera -exceptuadas las entidades de pago- domiciliada en la Unión Europea o en <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>país tercero equivalente</a>, que sea objeto de supervisión para garantizar el cumplimiento de las obligaciones de prevención del blanqueo de capitales y de la financiación del terrorismo','','susceptibleSimplificadas');
								campoCheckIndividualConClase('checkSurcusalFilial'.$i,'Sucursal o filial de entidades financieras -exceptuadas las entidades de pago-, domiciliada en la Unión Europea o en <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>país tercero equivalente</a>, cuando esté sometida por la matriz a procedimientos de prevención del blanqueo de capitales y de la financiación del terrorismo.','','susceptibleSimplificadas');
								campoCheckIndividualConClase('checkSociedadCotizada'.$i,'Sociedad cotizada cuyos valores se admiten a negociación en un mercado regulado de la Unión Europea o de <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>país tercero equivalente</a>, sus sucursales y filiales participadas mayoritariamente','','susceptibleSimplificadas');
								campoCheckIndividualConClase('checkSociedadMera'.$i,'Sociedad de mera tenencia de activos ','','checkReforzadas');
								campoCheckIndividualConClase('checkSociedadCuya'.$i,'Sociedad cuya estructura accionarial y de control no sea transparente o resulte inusual o excesivamente compleja','','checkReforzadas');
								echo '</div>';
								echo '<br/>';
								areaTexto('observacionesJuridica'.$i,'Observaciones');
				  			echo '</td></tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaPersonasJuridicas\");'><i class='icon-plus'></i> Añadir persona</button> 
				</div>
			</div>
			</center>";

	cierraBD();
}

function creaTablaPersonasSinJuridica($datos=false){
	conexionBD();

	echo "	              
			<center>
			Deja el campo 'Nombre y apellidos' vacío para borrar la persona     
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaPersonasSinJuridicas'>
				  	<thead>
				    	<tr>
				            <th> DATOS DE PARTÍCIPE DE ENTIDAD SIN PERSONALIDAD JURÍDICA </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM personas_sinjuridicas_informes_clientes WHERE codigoInforme=".$datos['codigo']);
				  			while($persona=mysql_fetch_assoc($consulta)){
				  				echo '<tr><td><br/>';
				  				campoOculto($persona['codigo'],'codigoExisteSinJuridica'.$i);
				  				campoTexto('nombreSinJuridica'.$i,'Nombre y apellidos',$persona['nombreSinJuridica']);
								campoSelect('tipoDocumentoSinJuridica'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),$persona['tipoDocumentoSinJuridica'],'selectpicker span8 tipoDocumento show-tick');
								campoTexto('numeroDocumentoSinJuridica'.$i,'Número',$persona['numeroDocumentoSinJuridica']);
								abreColumnaCampos();
									campoTexto('calleSinJuridica'.$i,'Dirección',$persona['calleSinJuridica']);
									echo '<div class="hide">';
									campoTexto('numeroSinJuridica'.$i,'Nº',$persona['numeroSinJuridica'],'input-mini');
									echo '</div>';
								cierraColumnaCampos();
								abreColumnaCampos();
									campoTexto('cpSinJuridica'.$i,'CP',$persona['cpSinJuridica'],'input-small');
									campoTexto('localidadSinJuridica'.$i,'Localidad',$persona['localidadSinJuridica']);
									campoTexto('ciudadSinJuridica'.$i,'Ciudad',$persona['ciudadSinJuridica']);
									selectPaises('paisSinJuridica'.$i,$persona['paisSinJuridica'],true);
								cierraColumnaCampos();
								campoSelect('actividadLaboralSinJuridica'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),$persona['actividadLaboralSinJuridica'],'selectpicker span3 show-tick actividadlaboral');
								campoCheckIndividualConClase('checkCargoPublicoSinJuridica'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',$persona['checkCargoPublicoSinJuridica'],'checkReforzadas');
								campoCheckIndividualConClase('checkMenorSinJuridica'.$i,'Menor de edad',$persona['checkMenorSinJuridica'],'checkReforzadas');
								campoCheckIndividualConClase('checkIncapacitadoSinJuridica'.$i,'Incapacitado',$persona['checkIncapacitadoSinJuridica'],'checkReforzadas');
								echo '<br/>';
								areaTexto('observacionesSinJuridica'.$i,'Observaciones',$persona['observacionesSinJuridica']);
				  			echo '</td></tr>';
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr><td><br/>';
				  				campoTexto('nombreSinJuridica'.$i,'Nombre y apellidos');
								campoSelect('tipoDocumentoSinJuridica'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','Otro documento de identidad personal'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN','EXTERIORES','OTROS'),'','selectpicker span8 tipoDocumento show-tick');
								campoTexto('numeroDocumentoSinJuridica'.$i,'Número');
								abreColumnaCampos();
									campoTexto('calleSinJuridica'.$i,'Dirección');
									echo '<div class="hide">';
									campoTexto('numeroSinJuridica'.$i,'Nº','','input-mini');
									echo '</div>';
								cierraColumnaCampos();
								abreColumnaCampos();
									campoTexto('cpSinJuridica'.$i,'CP','','input-small');
									campoTexto('localidadSinJuridica'.$i,'Localidad');
									campoTexto('ciudadSinJuridica'.$i,'Ciudad');
									selectPaises('paisSinJuridica'.$i,'',true);
								cierraColumnaCampos();
								campoSelect('actividadLaboralSinJuridica'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),'','selectpicker span3 show-tick actividadlaboral');
								campoCheckIndividualConClase('checkCargoPublicoSinJuridica'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público','','checkReforzadas');
								campoCheckIndividualConClase('checkMenorSinJuridica'.$i,'Menor de edad','','checkReforzadas');
								campoCheckIndividualConClase('checkIncapacitadoSinJuridica'.$i,'Incapacitado','','checkReforzadas');
								echo '<br/>';
								areaTexto('observacionesSinJuridica'.$i,'Observaciones');
				  			echo '</td></tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaPersonasSinJuridicas\");'><i class='icon-plus'></i> Añadir entidad</button> 
				</div>
			</div>
			</center>";

	cierraBD();
}

function creaTablaFideocomisos($datos=false){
	conexionBD();

	echo "	              
			<center>
			Deja el campo 'Nombre y apellidos' vacío para borrar la persona      
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaFideocomisos'>
				  	<thead>
				    	<tr>
				            <th> DATOS DE PERSONAS FÍSICAS QUE INTERVIENEN EN FIDEICOMISO</th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM personas_fideocomisos_informes_clientes WHERE codigoInforme=".$datos['codigo']);
				  			while($persona=mysql_fetch_assoc($consulta)){
				  				echo '<tr><td><br/>';
				  				campoOculto($persona['codigo'],'codigoExisteFideocomiso'.$i);
				  				campoTexto('nombreFideocomiso'.$i,'Nombre y apellidos',$persona['nombreFideocomiso']);
								campoSelect('tipoDocumentoFideocomiso'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN'),$persona['tipoDocumentoFideocomiso'],'selectpicker span8 tipoDocumento show-tick');
								campoTexto('numeroDocumentoFideocomiso'.$i,'Número',$persona['numeroDocumentoFideocomiso']);
								abreColumnaCampos();
									campoTexto('calleFideocomiso'.$i,'Dirección',$persona['calleFideocomiso']);
									echo '<div class="hide">';
									campoTexto('numeroFideocomiso'.$i,'Nº',$persona['numeroFideocomiso'],'input-mini');
									echo '</div>';
								cierraColumnaCampos();
								abreColumnaCampos();
									campoTexto('cpFideocomiso'.$i,'CP',$persona['cpFideocomiso'],'input-small');
									campoTexto('localidadFideocomiso'.$i,'Localidad',$persona['localidadFideocomiso']);
									campoTexto('ciudadFideocomiso'.$i,'Ciudad',$persona['ciudadFideocomiso']);
									selectPaises('paisFideocomiso'.$i,$persona['paisFideocomiso'],true);
								cierraColumnaCampos();
								campoSelect('condicionFideocomiso'.$i,'Condición',array('Fideicomitente','Fideicomisario','Protector','Beneficiarios','Control efectivo final sobre el fideicomiso'),array('FIDEICOMITENTE','FIDEICOMISARIO','PROTECTOR','BENEFICIARIOS','CONTROL'),$persona['condicionFideocomiso']);
								campoSelect('actividadLaboralFideocomiso'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),$persona['actividadLaboralFideocomiso'],'selectpicker span3 show-tick actividadlaboral');
								campoCheckIndividualConClase('checkCargoPublicoFideocomiso'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',$persona['checkCargoPublicoFideocomiso'],'checkReforzadas');
								campoCheckIndividualConClase('checkMenorFideocomiso'.$i,'Menor de edad',$persona['checkMenorFideocomiso'],'checkReforzadas');
								campoCheckIndividualConClase('checkIncapacitadoFideocomiso'.$i,'Incapacitado',$persona['checkIncapacitadoFideocomiso'],'checkReforzadas');
								campoCheckIndividualConClase('checkFideocomisarioFideocomiso'.$i,'Fideicomisario no declara su condición de tal, siendo determinada por el sujeto obligado',$persona['checkFideocomisarioFideocomiso'],'checkExamen checkFide');
								echo '<br/>';
								areaTexto('observacionesFideocomiso'.$i,'Observaciones',$persona['observacionesFideocomiso']);
				  			echo '</td></tr>';
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr><td><br/>';
				  				campoTexto('nombreFideocomiso'.$i,'Nombre y apellidos');
								campoSelect('tipoDocumentoFideocomiso'.$i,'Tipo de documento',array('DNI','Pasaporte','Tarjeta de residencia','Tarjeta de identidad de extranjero','Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea'),array('DNI','PASAPORTE','RESIDENCIA','EXTRANJERO','ORIGEN'),'','selectpicker span8 tipoDocumento show-tick');
								campoTexto('numeroDocumentoFideocomiso'.$i,'Número');
								abreColumnaCampos();
									campoTexto('calleFideocomiso'.$i,'Dirección');
									echo '<div class="hide">';
									campoTexto('numeroFideocomiso'.$i,'Nº','','input-mini');
									echo '</div>';
								cierraColumnaCampos();
								abreColumnaCampos();
									campoTexto('cpFideocomiso'.$i,'CP','','input-small');
									campoTexto('localidadFideocomiso'.$i,'Localidad');
									campoTexto('ciudadFideocomiso'.$i,'Ciudad');
									selectPaises('paisFideocomiso'.$i,'',true);
								cierraColumnaCampos();
								campoSelect('condicionFideocomiso'.$i,'Condición',array('Fideicomitente','Fideicomisario','Protector','Beneficiarios','Control efectivo final sobre el fideicomiso'),array('FIDEICOMITENTE','FIDEICOMISARIO','PROTECTOR','BENEFICIARIOS','CONTROL'));
								campoSelect('actividadLaboralFideocomiso'.$i,'Actividad laboral',array('Socio','Empresario individual','Profesional ejerciente','Trabajador por cuenta ajena','Jubilado','Pensionista','Desempleado','Cargo público','Cónyuge de otro interviniente','Sin actividad laboral'),array('SOCIO','EMPRESARIO','PROFESIONAL','TRABAJADOR','JUBILADO','PENSIONISTA','DESEMPLEADO','CARGO','CONYUGE','SIN'),'','selectpicker span3 show-tick actividadlaboral');
								campoCheckIndividualConClase('checkCargoPublicoFideocomiso'.$i,'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público','','checkReforzadas');
								campoCheckIndividualConClase('checkMenorFideocomiso'.$i,'Menor de edad','','checkReforzadas');
								campoCheckIndividualConClase('checkIncapacitadoFideocomiso'.$i,'Incapacitado','','checkReforzadas');
								campoCheckIndividualConClase('checkFideocomisarioFideocomiso'.$i,'Fideicomisario no declara su condición de tal, siendo determinada por el sujeto obligado','','checkExamen checkFide');
								echo '<br/>';
								areaTexto('observacionesFideocomiso'.$i,'Observaciones');
				  			echo '</td></tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaFideocomisos\");'><i class='icon-plus'></i> Añadir fideocomiso</button> 
				</div>
			</div>
			</center>";

	cierraBD();
}

function creaTablaProfesionales($datos=false){
	conexionBD();

	echo "	              
			<center> 
			Deja el campo vacío para borrar el cliente  
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaProfesionales'>
				  	<thead>
				    	<tr>
				            <th> Profesionales INTERVINIENTES</th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM profesionales_informes_clientes WHERE codigoInforme=".$datos['codigo']);
				  			while($persona=mysql_fetch_assoc($consulta)){
				  				echo '<tr>';
				  					campoTextoTabla('profesional'.$i,$persona['profesional'],'span6');
				  				echo '</tr>';
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr>';
				  				campoTextoTabla('profesional'.$i,'','span6');
				  			echo '</tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaProfesionales\");'><i class='icon-plus'></i> Añadir profesional</button> 
				</div>
			</div>
			</center>";

	cierraBD();
}

function creaTablaFirmados($datos=false){
	conexionBD();

	echo "	              
			Deja el campo vacío para borrar la persona
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaFirmado'>
				  	<thead>
				    	<tr>
				            <th> Personas</th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM firmado_informes_clientes WHERE codigoInforme=".$datos['codigo']);
				  			while($firmado=mysql_fetch_assoc($consulta)){
				  				echo '<tr>';
				  					campoTextoTabla('firmado'.$i,$firmado['firmado'],'span6');
				  				echo '</tr>';
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr>';
				  				campoTextoTabla('firmado'.$i,'','span6');
				  			echo '</tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='crearFila(\"tablaFirmado\");'><i class='icon-plus'></i> Añadir persona</button> 
				</div>
			</div>";

	cierraBD();
}

function compruebaItem($tabla){
	if(isset($_REQUEST['codigo'])){//OJO: con $_REQUEST porque puede venir desde el listado ($_GET) o desde el alta
		$datos=datosRegistro($tabla,$_REQUEST['codigo'],'codigoTrabajo');
		if($datos){
			campoOculto($datos);
			campoOculto($datos,'codigoTrabajo');
		} else {
			campoOculto($_REQUEST['codigo'],'codigoTrabajo');
		}
	}
	else{
		$datos=false;
	}
	return $datos;
}

function botonesNav($siguiente=false,$anterior=false){

	if(!!$siguiente){
	echo "<br clear='all'><br/><a href='#".$siguiente."' style='float:right;' class='btn btn-propio btnSiguiente noAjax' data-toggle='tab'>Siguiente <i class='icon-arrow-right'></i></a>";
	}	
	if(!!$anterior){
		echo "<a href='#".$anterior."' style='float:left;' class='btn btn-propio btnAnterior noAjax' data-toggle='tab'><i class='icon-arrow-left'></i> Anterior</a>";
	}
}



function articulo6(){
	$texto='1. Se considerarán documentos fehacientes, a efectos de identificación formal, los siguientes:
a) Para las personas físicas de nacionalidad española, el Documento Nacional de Identidad.
Para las personas físicas de nacionalidad extranjera, la Tarjeta de Residencia, la Tarjeta de Identidad de Extranjero, el Pasaporte o, en el caso de ciudadanos de la Unión Europea o del Espacio Económico Europeo, el documento, carta o tarjeta oficial de identidad personal expedido por las autoridades de origen. Será asimismo documento válido para la identificación de extranjeros el documento de identidad expedido por el Ministerio de Asuntos Exteriores y de Cooperación para el personal de las representaciones diplomáticas y consulares de terceros países en España.
Excepcionalmente, los sujetos obligados podrán aceptar otros documentos de identidad personal expedidos por una autoridad gubernamental siempre que gocen de las adecuadas garantías de autenticidad e incorporen fotografía del titular.
b) Para las personas jurídicas, los documentos públicos que acrediten su existencia y contengan su denominación social, forma jurídica, domicilio, la identidad de sus administradores, estatutos y número de identificación fiscal.
En el caso de personas jurídicas de nacionalidad española, será admisible, a efectos de identificación formal, certificación del Registro Mercantil provincial, aportada por el cliente u obtenida mediante consulta telemática.
2. En los casos de representación legal o voluntaria la identidad del representante y de la persona o entidad representada, será comprobada documentalmente. A estos efectos, deberá obtenerse copia del documento fehaciente a que se refiere el apartado precedente correspondiente tanto al representante como a la persona o entidad representada, así como el documento público acreditativo de los poderes conferidos. Será admisible la comprobación mediante certificación del Registro Mercantil provincial, aportada por el cliente, u obtenida mediante consulta telemática.
3. Los sujetos obligados identificarán y comprobarán mediante documentos fehacientes la identidad de todos los partícipes de las entidades sin personalidad jurídica. No obstante, en el supuesto de entidades sin personalidad jurídica que no ejerzan actividades económicas bastará, con carácter general, con la identificación y comprobación mediante documentos fehacientes de la identidad de la persona que actúe por cuenta de la entidad.
En el supuesto de fondos de inversión, la obligación de identificación y comprobación de la identidad de los partícipes se realizará conforme a lo dispuesto en el artículo 40.3 de la Ley 35/2003, de 4 de noviembre, de Instituciones de Inversión Colectiva.
En los fideicomisos anglosajones («trusts») u otros instrumentos jurídicos análogos que, no obstante carecer de personalidad jurídica, puedan actuar en el tráfico económico, los sujetos obligados requerirán el documento constitutivo, sin perjuicio de proceder a la identificación y comprobación de la identidad de la persona que actúe por cuenta de los beneficiarios o de acuerdo con los términos del fideicomiso, o instrumento jurídico. A estos efectos, los fideicomisarios comunicarán su condición a los sujetos obligados cuando, como tales, pretendan establecer relaciones de negocio o intervenir en cualesquiera operaciones. En aquellos supuestos en que un fideicomisario no declare su condición de tal y se determine esta circunstancia por el sujeto obligado, se pondrá fin a la relación de negocios, procediendo a realizar el examen especial a que se refiere el artículo 17 de la Ley 10/2010, de 28 de abril.
4. Los documentos de identificación deberán encontrarse en vigor en el momento de establecer relaciones de negocio o ejecutar operaciones ocasionales. En el supuesto de personas jurídicas, la vigencia de los datos consignados en la documentación aportada deberá acreditarse mediante una declaración responsable del cliente.
';
	return $texto;
}

function articulo21(){
	$texto='1.	Los sujetos obligados podrán establecer relaciones de negocio o ejecutar operaciones a través de medios telefónicos, electrónicos o telemáticos con clientes que no se encuentren físicamente presentes, siempre que concurra alguna de las siguientes circunstancias:
a)	La identidad del cliente quede acreditada de conformidad con lo dispuesto en la normativa aplicable sobre firma electrónica.
b)	La identidad del cliente quede acreditada mediante copia del documento de identidad, de los establecidos en el <a href="#" class="enlacePopOver2 noAjax" position="bottom" title="Artículo 6 RLPBC: Documentos fehacientes a efectos de identificación formal <button class=\'cierraPopover2 btn btn-propio\'><i class=\'icon-times\'></i></button>">artículo 6 RLPBC</a><div class="hide">'.articulo6().'</div>, que corresponda, siempre que dicha copia esté expedida por un fedatario público.
c)	El primer ingreso proceda de una cuenta a nombre del mismo cliente abierta en una entidad domiciliada en España, en la Unión Europea o en <a href=\'..\documentos\paises.pdf\' class=\'noAjax\' target=\'_blank\'>países terceros equivalentes</a>.
d)	La identidad del cliente quede acreditada mediante el empleo de otros procedimientos seguros de identificación de clientes en operaciones no presenciales, siempre que tales procedimientos hayan sido previamente autorizados por el Servicio Ejecutivo de la Comisión de Prevención del Blanqueo de Capitales e Infracciones Monetarias (en adelante, Servicio Ejecutivo de la Comisión).
En todo caso, en  el plazo de un mes desde el establecimiento de la relación de negocios no presencial, los sujetos obligados deberán obtener de estos clientes una copia de los documentos necesarios para practicar la diligencia debida.
2.	Los criterios para la acreditación de la identidad del cliente en relación con los sujetos obligados sometidos a la Ley 13/2011, de 27 de mayo, de regulación del juego, y en su normativa de desarrollo, se determinarán en el proceso de concesión de licencias generales por la Dirección General de Ordenación del Juego, previo informe favorable del Servicio Ejecutivo de la Comisión. 
';

	return $texto;
}

function articulo20(){
	$texto='1.	En los supuestos de riesgo superior al promedio previstos en el artículo precedente o que se hubieran determinado por el sujeto obligado conforme a su análisis de riesgo, <u>los sujetos obligados comprobarán en todo caso las actividades declaradas por sus clientes y la identidad del titular real</u>, en los términos previstos en los artículos <a href="#" class="enlacePopOver2 noAjax" position="bottom" title="Artículo 9.1 RLPBC: Identificación del titular real <button class=\'cierraPopover2 btn btn-propio\'><i class=\'icon-times\'></i></button>">9.1</a><div class="hide">'.articulo9().'</div> y <a href="#" class="enlacePopOver2 noAjax" position="bottom" title="Artículo 10.2 RLPBC <button class=\'cierraPopover2 btn btn-propio\'><i class=\'icon-times\'></i></button>">10.2</a><div class="hide">'.articulo10().'</div>.
Adicionalmente se aplicarán, en función del riesgo, una o varias de las siguientes medidas:

a)	Actualizar los datos obtenidos en el proceso de aceptación del cliente.
b)	Obtener documentación o información adicional sobre el propósito e índole de la relación de negocios.
c)	Obtener documentación o información adicional sobre el origen de los fondos.
d)	Obtener documentación o información adicional sobre el origen del patrimonio del cliente.
e)	Obtener documentación o información sobre el propósito de las operaciones.
f)	Obtener autorización directiva para establecer o mantener la relación de negocios o ejecutar la operación.
g)	Realizar un seguimiento reforzado de la relación de negocio, incrementando el número y frecuencia de los controles aplicados y seleccionando patrones de operaciones para examen.
h)	Examinar y documentar la congruencia de la relación de negocios o de las operaciones con la documentación e información disponible sobre el cliente.
i)	Examinar y documentar la lógica económica de las operaciones.
j)	Exigir que los pagos o ingresos se realicen en una cuenta a nombre del cliente, abierta en una entidad de crédito domiciliada en la Unión Europea o en países terceros equivalentes.
k)	Limitar la naturaleza o cuantía de las operaciones o los medios de pago empleados.

2.	Los sujetos obligados incluirán al beneficiario de la póliza de seguro de vida como un factor de riesgo relevante a efectos de determinar la procedencia de aplicar medidas reforzadas de diligencia debida. En los casos en que el beneficiario presente un riesgo superior al promedio, las medidas reforzadas de diligencia debida incluirán medidas adecuadas para identificar y comprobar la identidad del titular real del beneficiario con carácter previo al pago de la prestación derivada del contrato o al ejercicio por el tomador de los derechos de rescate, anticipo o pignoración conferidos por la póliza.
';

	return $texto;
}

function articulo9(){
	$texto='1. Los sujetos obligados identificarán al titular real y adoptarán medidas adecuadas en función del riesgo a fin de comprobar su identidad con carácter previo al establecimiento de relaciones de negocio, la ejecución de transferencias electrónicas por importe superior a 1.000 euros o a la ejecución de otras operaciones ocasionales por importe superior a 15.000 euros.
La identificación y comprobación de la identidad del titular real podrá realizarse, con carácter general, mediante una declaración responsable del cliente o de la persona que tenga atribuida la representación de la persona jurídica. A estos efectos, los administradores de las sociedades y otras personas jurídicas deberán obtener y mantener información adecuada, precisa y actualizada sobre la titularidad real de las mismas.
No obstante lo dispuesto en el párrafo anterior, será preceptiva la obtención por el sujeto obligado de documentación adicional o de información de fuentes fiables independientes cuando el cliente, el titular real, la relación de negocios o la operación presentes riesgos superiores al promedio.
';

	return $texto;
}

function articulo10(){
	$texto='Los sujetos obligados comprobarán las actividades declaradas por los clientes en los siguientes supuestos:
a)	Cuando el cliente o la relación de negocios presenten riesgos superiores al promedio, por disposición normativa o porque así se desprenda del análisis de riesgo del sujeto obligado.
b)	Cuando del seguimiento de la relación de negocios resulte que las operaciones activas o pasivas del cliente no se corresponden con su actividad declarada o con sus antecedentes operativos.

';

	return $texto;
}

function articulo25(){
	$texto='1.	El proceso de examen especial se realizará de modo estructurado, documentándose las fases de análisis, las gestiones realizadas y las fuentes de información consultadas. En todo caso, el proceso de examen especial tendrá naturaleza integral, debiendo analizar toda la operativa relacionada, todos los intervinientes en la operación y toda la información relevante obrante en el sujeto obligado y, en su caso, en el grupo empresarial.
2.	Concluido el análisis técnico, el representante ante el Servicio Ejecutivo de la Comisión adoptará, motivadamente y sin demora, la decisión sobre si procede o no la comunicación al Servicio Ejecutivo de la Comisión, en función de la concurrencia en la operativa de indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo.
No obstante lo dispuesto en el párrafo anterior, el procedimiento de control interno del sujeto obligado podrá prever que la decisión sea sometida, previamente, a la consideración del órgano de control interno. En estos casos, el órgano de control interno adoptará la decisión por mayoría, debiendo constar expresamente en el acta, el sentido y motivación del voto de cada uno de los miembros.
Las decisiones sobre comunicación deberán responder, en todo caso, a criterios homogéneos, haciéndose constar la motivación en el expediente de examen especial.
En aquellos supuestos en que la detección de la operación derive de la comunicación interna de un empleado, agente o directivo de la entidad, la decisión final adoptada sobre si procede o no la comunicación por indicio de la operación, será puesta en conocimiento del comunicante.
3.	Los sujetos obligados mantendrán un registro en el que, por orden cronológico, se recogerán para cada expediente de examen especial realizado, entre otros datos, sus fechas de apertura y cierre, el motivo que generó su realización, una descripción de la operativa analizada, la conclusión alcanzada tras el examen y las razones en que se basa.
Asimismo se hará constar la decisión sobre su comunicación o no al Servicio Ejecutivo de la Comisión y su fecha, así como la fecha en que, en su caso, se realizó la comunicación.
4.	Los sujetos obligados conservarán los expedientes de examen especial durante el plazo de diez años.

';

	return $texto;
}

function comprobarCheck($datos,$campos=array()){
	foreach ($campos as $campo) {
		if(!isset($datos[$campo])){
			$datos[$campo]='NO';
		}
	}
	return $datos;
}

function campoCheckIndividualConClase($nombreCampo,$textoCampo,$valor=false,$clase='',$valorCampo='SI',$salto=true){//MODIFICACIÓN 05/05/2015: creación de nueva función para crear campos check individuales (los name no llevan índice)
	$valor=compruebaValorCampo($valor,$nombreCampo);

    echo "<label class='checkbox inline'>
    		<input type='checkbox' name='$nombreCampo' id='$nombreCampo' value='".$valorCampo."' class='".$clase."'";

    if($valor!=false && $valor==$valorCampo){
    	echo " checked='checked'";
    }
    echo ">".$textoCampo."</label>";

    if($salto){
    	echo "<br />";
    }
}

function informeRiesgo(){
	$datos=arrayFormulario();
	if($datos['cliente']=='NO'){
		$cliente=datosRegistro('usuarios_clientes',$datos['codigo'],'codigoUsuario');
		$datos['codigo']=$cliente['codigoCliente'];
	}
	$res=array();
	$res['texto']='Aún no se le ha hecho un análisis previo a este cliente';
	$res['sepblac']='';
	$analisis=consultaBD('SELECT * FROM analisis_previo WHERE codigoCliente='.$datos['codigo'].' ORDER BY fecha DESC',true);
	$analisis=mysql_fetch_assoc($analisis);
	if($analisis){
		if($analisis['empleados']==50 || $analisis['volumen']==3){
			$res['texto']='Concluido el análisis técnico, el órgano de control interno  adoptará, motivadamente y sin demora, la decisión sobre si procede o no la comunicación al SEPBLAC, en función de la concurrencia en la operativa de indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo. El órgano de control interno adoptará la decisión por mayoría, debiendo constar expresamente en el acta, el sentido y motivación del voto de cada uno de los miembros';
			$res['sepblac']=$analisis['representante'];
		} else {
			$res['texto']='Concluido el análisis técnico, el representante ante el SEPBLAC adoptará, motivadamente y sin demora, la decisión sobre si procede o no la comunicación al SEPBLAC, en función de la concurrencia en la operativa de indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo';
		}
	}

	echo json_encode($res);
}


function filtroServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Referencia');
	campoTexto(1,'Servicio','','span3');
	campoTextoSimbolo(2,'Precio','€');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(3,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function eliminaExcel(){
	global $_CONFIG;
	$excel=datosRegistro('informes_clientes_excel',$_POST['codigo']);
	if($excel['excel']!='NO' && $excel['excel']!=''){
		unlink('./documentos/'.$excel['excel']);
	}
	$sql='DELETE FROM informes_clientes_excel WHERE codigo='.$excel['codigo'];
	$consulta=consultaBD($sql,true);
}

function eliminaComprobacion(){
	extract($_POST);
	if($eliminar=='SI'){
		$res=consultaBD('INSERT comprobaciones_informes_clientes_eliminadas VALUES(NULL,'.$codigoInforme.',"'.$campo.'");',true);
	} else {
		$res=consultaBD('DELETE FROM comprobaciones_informes_clientes_eliminadas WHERE codigoInforme='.$codigoInforme.' AND campo="'.$campo.'";',true);
	}
	if(!$res){
		$res='error';
	}
	echo $res;
}

function campoCheckMedidas($datos){
	if($datos){
		$checks=array('checkMedidasSimplificadas'=>'NO','checkMedidasNormales'=>'NO','checkMedidasReforzadas'=>'NO','checkMedidasExamen'=>'NO');
		$medidas=explode('&$&', $datos['medidas']);
		if(in_array('SIMPLIFICADAS', $medidas)){
			$checks['checkMedidasSimplificadas']='SI';
		}
		if(in_array('NORMALES', $medidas)){
			$checks['checkMedidasNormales']='SI';
		}
		if(in_array('REFORZADAS', $medidas)){
			$checks['checkMedidasReforzadas']='SI';
		}
		if(in_array('EXAMEN', $medidas)){
			$checks['checkMedidasExamen']='SI';
		}
	} else {
		$checks=array('checkMedidasSimplificadas'=>'NO','checkMedidasNormales'=>'SI','checkMedidasReforzadas'=>'NO','checkMedidasExamen'=>'NO');
	}
	echo '<div id="divMedidas">';
		campoCheckIndividual('checkMedidasSimplificadas','Simplificadas',$checks);
		campoCheckIndividual('checkMedidasNormales','Normales',$checks);
		campoCheckIndividual('checkMedidasReforzadas','Reforzadas',$checks);
		campoCheckIndividual('checkMedidasExamen','Examen especial',$checks);
	echo '</div>';
}

function obtieneArrayServicios($conNull=false){
	if($conNull){
		return array(''=>'',1=>'Auditoría de cuentas',2=>'Contabilidad externa',3=>'Asesoría fiscal',4=>'Concepción, realización o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de bienes inmuebles',5=>'Concepción, realización o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de entidades comerciales',6=>'Gestión de fondos, valores u otros activos',7=>'Apertura y/o gestión de cuentas corrientes, cuentas de ahorros o cuentas de valores',8=>'Organización de aportaciones para la creación, funcionamiento o gestión de empresas',9=>'Creación, funcionamiento o gestión de fideicomisos (trusts), sociedades o estructuras análogas',17=>'Actuación por cuenta de cliente en operación financiera',18=>'Actuación por cuenta de cliente en operación inmobiliaria',10=>'Constitución de sociedad u otra persona jurídica',11=>'Dirección o secretaría de sociedad, socio de una asociación, funciones similares con otras personas jurídicas',12=>'Facilitar un domicilio social o una dirección comercial, postal, administrativa y otros servicios afines a una sociedad, asociación o cualquier otro instrumento o persona jurídicos',13=>'Ejercer funciones de fideicomisario en un fideicomiso expreso o instrumento jurídico similar',14=>'Disponer que otra persona ejerza funciones de fideicomisario en un fideicomiso expreso o instrumento jurídico similar',15=>'Ejercer funciones de accionista por cuenta de otra persona',16=>'Disponer qué persona ha de ejercer las funciones de accionista',19=>'Intermediación en la concesión de préstamos o créditos',20=>'Concesión/gestión de préstamos',21=>'Préstamos personales',22=>'Concesión/gestión de créditos',23=>'Crédito al consumo',24=>'Crédito hipotecario',25=>'Financiación de transacciones comerciales',26=>'Factoring, con o sin recurso',27=>'Arrendamiento financiero/actividades complementarias previstas en el párrafo 8 de la D.A. 7ª de la Ley 26/1988, de 29 de julio, de Disciplina e Intervención de las Entidades de Crédito',28=>'Emisión y gestión de tarjetas de crédito',29=>'Concesión de avales y garantías/suscripción de compromisos similares');
	} else {
		return array(1=>'Auditoría de cuentas',2=>'Contabilidad externa',3=>'Asesoría fiscal',4=>'Concepción, realización o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de bienes inmuebles',5=>'Concepción, realización o asesoramiento en operaciones por cuenta de clientes relativas a la compraventa de entidades comerciales',6=>'Gestión de fondos, valores u otros activos',7=>'Apertura y/o gestión de cuentas corrientes, cuentas de ahorros o cuentas de valores',8=>'Organización de aportaciones para la creación, funcionamiento o gestión de empresas',9=>'Creación, funcionamiento o gestión de fideicomisos (trusts), sociedades o estructuras análogas',17=>'Actuación por cuenta de cliente en operación financiera',18=>'Actuación por cuenta de cliente en operación inmobiliaria',10=>'Constitución de sociedad u otra persona jurídica',11=>'Dirección o secretaría de sociedad, socio de una asociación, funciones similares con otras personas jurídicas',12=>'Facilitar un domicilio social o una dirección comercial, postal, administrativa y otros servicios afines a una sociedad, asociación o cualquier otro instrumento o persona jurídicos',13=>'Ejercer funciones de fideicomisario en un fideicomiso expreso o instrumento jurídico similar',14=>'Disponer que otra persona ejerza funciones de fideicomisario en un fideicomiso expreso o instrumento jurídico similar',15=>'Ejercer funciones de accionista por cuenta de otra persona',16=>'Disponer qué persona ha de ejercer las funciones de accionista',19=>'Intermediación en la concesión de préstamos o créditos',20=>'Concesión/gestión de préstamos',21=>'Préstamos personales',22=>'Concesión/gestión de créditos',23=>'Crédito al consumo',24=>'Crédito hipotecario',25=>'Financiación de transacciones comerciales',26=>'Factoring, con o sin recurso',27=>'Arrendamiento financiero/actividades complementarias previstas en el párrafo 8 de la D.A. 7ª de la Ley 26/1988, de 29 de julio, de Disciplina e Intervención de las Entidades de Crédito',28=>'Emisión y gestión de tarjetas de crédito',29=>'Concesión de avales y garantías/suscripción de compromisos similares');
	}
}

//Fin parte de servicios
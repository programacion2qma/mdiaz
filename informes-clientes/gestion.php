<?php
  $seccionActiva=50;
  include_once("../cabecera.php");
  if(isset($_GET['renovacion'])){
    gestionRenovacion();
  } else if(isset($_GET['servicios'])){
    gestionServicios();
  } else { 
    gestionInformes();
  }
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>

<script type="text/javascript">
  var servicios=0;

  $(document).ready(function(){
  //var solicitaServicio=$('input[name=solicitaServicio]:checked').val();
	  $('select').selectpicker();
	  $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    marcarPestania();

    document.addEventListener("keydown", function(event) {
      
      // TAB detectado
      if (event.keyCode == 9) {
        // Código para la tecla TAB
        $('.hasDatepicker').datepicker('hide');
      }

    });
	
    $('.btnSiguiente').click(function(){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
      asignaPagina();
      $('html, body').animate({scrollTop:0}, 'slow');
    });

	  $('.btnAnterior').click(function(){
      $('.nav-tabs > .active').after('li').find('a').trigger('click');
      asignaPagina();
      $('html, body').animate({scrollTop:0}, 'slow');
    });

    $('#pestania4 .btnSiguiente').click(function(){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
      asignaPagina();
      $('html, body').animate({scrollTop:0}, 'slow');
    });

    $('#pestania7 .btnSiguiente').click(function(){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
      asignaPagina();
      $('html, body').animate({scrollTop:0}, 'slow');
    });

    $('#pestania10 .btnSiguiente').click(function(){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
      asignaPagina();
      $('html, body').animate({scrollTop:0}, 'slow');
    });

    $('#pestania16 .btnSiguiente').click(function(){
      $('.nav-tabs > .active').next('li').find('a').trigger('click');
      asignaPagina();
      $('html, body').animate({scrollTop:0}, 'slow');
    });

    $(".nav-tabs li a").click(function() {
      var pagina=$(this).attr('href');
      pagina = pagina.replace('#','');
      $("#pagina").val(pagina);
    });

    if($('#codigo').length){
      comprobarMedidas();
    }

    $('#marcarTodos').unbind();
    $('#marcarTodos').click(function(e){
      e.preventDefault();
      var resultado = 'PRESTAR';

      if($('#checkMedidasNormales').attr('checked')=='checked'){
        $('#bloque1 input[type=checkbox]').attr('checked','checked');
        $('#bloque2 input[type=checkbox]').attr('checked','checked');
      } else if($('#checkMedidasSimplificadas').attr('checked')=='checked'){
        $('#bloque1 input[type=checkbox]').attr('checked','checked');
      }

      if($('#checkMedidasReforzadas').attr('checked')=='checked'){
        $('#bloque1 input[type=checkbox]').attr('checked','checked');
        $('#bloque2 input[type=checkbox]').attr('checked','checked');
        $('#bloque3 input[type=checkbox]').attr('checked','checked');
      }

      if($('#checkMedidasExamen').attr('checked')=='checked'){
        $('#bloque1 input[type=checkbox]').attr('checked','checked');
        $('#bloque2 input[type=checkbox]').attr('checked','checked');
        $('#bloque3 input[type=checkbox]').attr('checked','checked');
        $('#bloque4 input[type=checkbox]').attr('checked','checked');
        resultado='EXAMEN';
      }
    
      $('#resultado').val(resultado);
      $('#resultado').selectpicker('refresh');
      mostrarPrestania4(false);
    });

    $(".selectPaises").change(function(){
      comprobarMedidas();
    });

    $(".actividadlaboral").change(function(){
      comprobarMedidas();
    });

    $(".checkReforzadas").change(function(){
      comprobarMedidas();
    });

    $(".checkExamen").change(function(){
      comprobarMedidas();
    });

    $(".susceptibleSimplificadas").change(function(){
      comprobarMedidas();
    });

    $('.eliminaComprobacion').unbind();
    $('.eliminaComprobacion').click(function(e){
      e.preventDefault();
      var codigoInforme=$('#codigo').val();
      var campo=$(this).attr('campo');
      $('#'+campo+'SI').addClass('hide');
      $('#'+campo+'NO').removeClass('hide');
      $('#'+campo+'Eliminado').val('SI');
      eliminaComprobacion('SI',codigoInforme,campo);
    
      if(comprobaciones()){
        var resultado = 'PRESTAR';

        if($('#checkMedidasExamen').attr('checked')=='checked'){
          resultado='EXAMEN';
        }

        $('#resultado').val(resultado);
        $('#resultado').selectpicker('refresh');
      } else {
        $('#resultado').val('SIN');
        $('#resultado').selectpicker('refresh');
      }

    });

    $('.activaComprobacion').unbind();
    $('.activaComprobacion').click(function(e){
      e.preventDefault();
      var codigoInforme=$('#codigo').val();
      var campo=$(this).attr('campo');
      var campo=$(this).attr('campo');
      $('#'+campo+'NO').addClass('hide');
      $('#'+campo+'SI').removeClass('hide');
      $('#'+campo+'Eliminado').val('NO');
      eliminaComprobacion('NO',codigoInforme,campo);
      
      if(comprobaciones()){
        var resultado='PRESTAR';
        if($('#checkMedidasExamen').attr('checked')=='checked'){
          resultado='EXAMEN';
        }
      
        $('#resultado').val(resultado);
        $('#resultado').selectpicker('refresh');
      } else {
        $('#resultado').val('SIN');
        $('#resultado').selectpicker('refresh');
      }
    });

    mostrarPrestania($('.divSolicitaServicio input[type=radio]:checked').val());
  
    $('.divSolicitaServicio input[type=radio]').change(function(){
      val = $(this).val();
      // if(val > 1 && servicios > 1){
      //   alert('No puedes seleccionar representar a 3º personas si tiene más de un servicio');
      //   var id=$(this).attr('name');
      //   $('input[name='+id+'][value=1]').attr('checked','checked');
      // } else {
        mostrarPrestania(val);
      // }
    });

    mostrarPrestania3();
    $(".checkFide").change(function(){
      mostrarPrestania3();
    });

    mostrarTablaFisica($('#checkPersonasFisicas:checked').val());
    $('#checkPersonasFisicas').change(function(){
      mostrarTablaFisica($('#checkPersonasFisicas:checked').val());
      if($(this).attr('checked')=='checked'){
        if($('input[name=solicitaServicio]:checked').val()==1){
          $('input[name=solicitaServicio][value=3]').attr('checked','checked');
        }
      } else {
        $('input[name=solicitaServicio][value=1]').attr('checked','checked');
      }
    });

    mostrarTablaJuridica($('#checkPersonasJuridicas:checked').val());
    $('#checkPersonasJuridicas').change(function(){
      mostrarTablaJuridica($('#checkPersonasJuridicas:checked').val());
    });

    mostrarTablaSinJuridicas($('#checkPersonasSinJuridicas:checked').val());
    $('#checkPersonasSinJuridicas').change(function(){
      mostrarTablaSinJuridicas($('#checkPersonasSinJuridicas:checked').val());
    });

    mostrarTablaFidecomisos($('#checkFideocomisos:checked').val());
    $('#checkFideocomisos').change(function(){
      mostrarTablaFidecomisos($('#checkFideocomisos:checked').val());
    });

    mostrarPrestania4($('#resultado').val());
  
    for(i=1;i<=29;i++){
      id='checkServicio'+i;
      if($('#'+id).attr('checked')=='checked'){
        $('#'+id).parent().css('display','block');
        id='falso'+id;
        $('#'+id).parent().css('display','none');
        servicios++;
      }
    } 
  
    $('.sujetos .fijos input[type=checkbox]').change(function(){         
      pasarServicios($(this).attr('id'), false);
    });

    $('.contratados .fijos input[type=checkbox]').change(function(){ 
      pasarServicios($(this).attr('id'),true);
    });

    $("#tablaServiciosPersonalizadosFalso").on("keyup",".nombreServicioPersonalizado",function(){
      var valor=$(this).val();
      var id=$(this).attr('id').replace('falso','');
      $('#'+id).val(valor);
    });

    $("#tablaServiciosPersonalizadosFalso").on("change","input[type=checkbox]",function(e){
      pasarServiciosPersonalizados($(this).attr('id'));
    });

    $("#tablaServiciosPersonalizados").on("change","input[type=checkbox]",function(e){
      pasarServiciosPersonalizados($(this).attr('id'),true);
    });

    $('.tipoDocumento').change(function(){
      if($(this).val()=='OTROS'){
        alert('Excepcionalmente, los sujetos obligados podrán aceptar otros documentos de identidad personal expedidos por una autoridad gubernamental siempre que gocen de las adecuadas garantías de autenticidad e incorporen fotografía del titular -art. 6.1.a) RLPBC-');
      }
    });
   
    if(comprobaciones()){
      var resultado='PRESTAR';
      if($('#checkMedidasExamen').attr('checked')=='checked'){
        resultado='EXAMEN';
      }
      $('#resultado').val(resultado);
      $('#resultado').selectpicker('refresh');
    } else {
      $('#resultado').val('SIN');
      $('#resultado').selectpicker('refresh');
    }

    $('#resultado').change(function(){
      if($(this).val()=='PRESTAR'){
        if(!comprobaciones()){
          alert('No puedes prestar el servicio sin marcar todas las comprobaciones');
          $('#resultado').val('SIN');
          $('#resultado').selectpicker('refresh');
        }
      }
    });

    $('.comprobaciones input[type=checkbox]').change(function(){
      if(comprobaciones()){
        var resultado='PRESTAR';
        if($('#checkMedidasExamen').attr('checked')=='checked'){
          resultado='EXAMEN';
        }
        $('#resultado').val(resultado);
        $('#resultado').selectpicker('refresh');
      } else {
        $('#resultado').val('SIN');
        $('#resultado').selectpicker('refresh');
      }
    });

    if($('#codigoCliente').length){
      informeRiesgo($('#codigoCliente').val(),'SI');
    } else {
      informeRiesgo($('#codigoUsuario').val(),'NO');
    }

    $('#codigoCliente').change(function(){
      informeRiesgo($(this).val(),'SI');
    });

    conclusion($('#conclusion').val());
    $('#conclusion').change(function(){
      conclusion($(this).val());
    });

    interrupcion($('#interrupcion').val());
    $('#interrupcion').change(function(){
      interrupcion($(this).val());
    });

	  $('.enlacePopOver').each(function(){
      $(this).popover({//Crea el popover
        title: $(this).attr('title'),
        content: $(this).next().html().replace(/[\r\n|\n]/g,'<br />'),
        placement:$(this).attr('position'),
        thisrigger:'manual',
        trigger:'manual'
      });

      $('.enlacePopOver').click(function(e){

        e.preventDefault();
        $('.enlacePopOver').not($(this)).popover('hide');
        $(this).popover('show');
        
        $('.cierraPopover').click(function(){
          $('.enlacePopOver').popover('hide');
        });

        $('.enlacePopOver2').each(function(){
          $(this).popover({//Crea el popover
            title: $(this).attr('title'),
            content: $(this).next().html().replace(/[\r\n|\n]/g,'<br />'),
            placement:$(this).attr('position'),
            thisrigger:'manual',
            trigger:'manual'
          });

          $('.enlacePopOver2').click(function(e){
            e.preventDefault();
            $('.enlacePopOver2').not($(this)).popover('hide');
            $(this).popover('show');
            $('.cierraPopover2').click(function(){
              $('.enlacePopOver2').popover('hide');
            });
          });  
        });
      });  

    });

    textoMedidas();
    $('input[name=medidas]').change(function(){
      textoMedidas();
      if(comprobaciones()){
        var resultado='PRESTAR';
        if($('#checkMedidasExamen').attr('checked')=='checked'){
          resultado='EXAMEN';
        }
        $('#resultado').val(resultado);
        $('#resultado').selectpicker('refresh');
      } else {
        $('#resultado').val('SIN');
        $('#resultado').selectpicker('refresh');
      }
    });

    $('#divMedidas input[type=checkbox]').change(function(){
      if($(this).attr('id')=='checkMedidasSimplificadas' && $(this).attr('checked')=='checked'){
        $('#checkMedidasNormales').attr('checked',false);
      } else if($(this).attr('id')=='checkMedidasNormales' && $(this).attr('checked')=='checked'){
        $('#checkMedidasSimplificadas').attr('checked',false);
      }

      comprobarMedidas();
    
      if(comprobaciones()){
        var resultado='PRESTAR';
        if($('#checkMedidasExamen').attr('checked')=='checked'){
          resultado='EXAMEN';
        }
        $('#resultado').val(resultado);
        $('#resultado').selectpicker('refresh');
      } else {
        $('#resultado').val('SIN');
        $('#resultado').selectpicker('refresh');
      }
    });

    mostrarPrestania5($('#conclusion option:selected').val());
    $("#conclusion").change(function(){
      mostrarPrestania5($(this).val());
    });

  });

  function eliminaComprobacion(eliminar,codigoInforme,campo){
    var consulta=$.post('../listadoAjax.php?include=informes-clientes&funcion=eliminaComprobacion();',{'eliminar':eliminar,'codigoInforme':codigoInforme,'campo':campo});
      consulta.done(function(respuesta){
        if(respuesta=='error'){
          alert('Ha ocurrido un error al hacer la gestión, consulte con el administrador.');
        }
    });
  }

  function asignaPagina(){
    var numero=$('.nav-tabs .active a').attr('href');
    var pagina=$('.nav-tabs .active a').attr('href');
    pagina = pagina.replace('#','');
    $("#pagina").val(pagina);
  }

  function marcarPestania(){
    if($("#pagina").length>0){
      var pagina=$("#pagina").val();
      $('.nav-tabs li').removeClass('active');
      $('.nav-tabs li:nth-child('+pagina+')').addClass('active');
    }
  }

  function mostrarPrestania(val){
    var res='NO';
    $(".divSolicitaServicio input[value=1]").each(function( index ) {
      if($(this).attr('checked')!='checked'){
        res='SI';
      }
    });
  
    if(res=='SI'){
      $('.nav-tabs li:nth-child(3)').css('display','inline');
      $('#pestania3').removeClass('hide');
      $('#pestania4').addClass('hide');
    } else {
      $('.nav-tabs li:nth-child(3)').css('display','none');
      $('#pestania4').removeClass('hide');
      $('#pestania3').addClass('hide');
    }
  }

  function mostrarPrestania2(val){
    if(val=='SI'){
      $('.nav-tabs li:nth-child(6)').css('display','inline');
      $('#pestania6').removeClass('hide');
      $('#pestania7').addClass('hide');
    } else {
      $('.nav-tabs li:nth-child(6)').css('display','none');
      $('#pestania7').removeClass('hide');
      $('#pestania6').addClass('hide');
    }
  }

  function mostrarPrestania3(val){
    var val='NO';
    $(".checkFide").each(function(){
      var checked=$(this).attr('checked');
      if(checked == 'checked'){ 
        val='SI';
      }
    });
  
    if(val=='SI'){
      $('.nav-tabs li:nth-child(9)').css('display','inline');
      $('#pestania9').removeClass('hide');
      $('#pestania10').addClass('hide');
    } else {
      $('.nav-tabs li:nth-child(9)').css('display','none');
      $('#pestania10').removeClass('hide');
      $('#pestania9').addClass('hide');
    }
  }

  function mostrarPrestania4(val){
    if($('#checkMedidasExamen').attr('checked')=='checked'){
      $('#pestania13').removeClass('hide');
      $('.nav-tabs li:nth-child(13)').css('display','inline');
      $('.nav-tabs li:nth-child(14)').css('display','inline');
      if($('#conclusion option:selected').val()==2){
        $('.nav-tabs li:nth-child(15)').css('display','inline');
      }
      $('.nav-tabs li:nth-child(16)').css('display','inline');
      $('#divTextoFinal').addClass('hide');
      //$('.nav-tabs li:nth-child(17)').css('display','inline');
      //$('.nav-tabs li:nth-child(18)').css('display','inline');
    } else {
      $('#pestania13').addClass('hide');
      $('#divTextoFinal').removeClass('hide');
      $('.nav-tabs li:nth-child(13)').css('display','none');
      $('.nav-tabs li:nth-child(14)').css('display','none');
      $('.nav-tabs li:nth-child(15)').css('display','none');
      $('.nav-tabs li:nth-child(16)').css('display','none');
      //$('.nav-tabs li:nth-child(17)').css('display','none');
      //$('.nav-tabs li:nth-child(18)').css('display','none');
    }
  }

function mostrarPrestania5(val){
  if(val==2){
    $('.nav-tabs li:nth-child(15)').css('display','inline');
    $('#pestania15').removeClass('hide');
    $('#pestania16').addClass('hide');
    $('#divPestania16_2').removeClass('hide');
    $('#divPestania16_2_otra').removeClass('hide');
    $('#divPestania16_1').addClass('hide');
  } else {
    $('.nav-tabs li:nth-child(15)').css('display','none');
    $('#pestania16').removeClass('hide');
    $('#pestania15').addClass('hide');
    $('#divPestania16_1').removeClass('hide');
    $('#divPestania16_2').addClass('hide');
    $('#divPestania16_2_otra').addClass('hide');
  }
}


function mostrarTablaFisica(val){
  if(val=='SI'){
    $('#divPersonasFisicas').removeClass('hide');
  } else {
    $('#divPersonasFisicas').addClass('hide');
  }
}

function mostrarTablaJuridica(val){
  if(val=='SI'){
    $('#divPersonasJuridicas').removeClass('hide');
  } else {
    $('#divPersonasJuridicas').addClass('hide');
  }
  mostrarPrestania2(val);
}

function mostrarTablaSinJuridicas(val){
  if(val=='SI'){
    $('#divPersonasSinJuridicas').removeClass('hide');
  } else {
    $('#divPersonasSinJuridicas').addClass('hide');
  }
}

function mostrarTablaFidecomisos(val){
  if(val=='SI'){
    $('#divFideocomisos').removeClass('hide');
  } else {
    $('#divFideocomisos').addClass('hide');
  }
}

function crearFila(tabla){
  $("#"+tabla+" tr:last input").each(function( index ) {
    if($(this).attr('id')==undefined){
      $(this).attr('id',$(this).attr('name'));
    }
  });
  insertaFila(tabla);
  $(".selectPaises").change(function(){
    comprobarMedidas();
  });
  $(".actividadlaboral").change(function(){
    comprobarMedidas();
  });
  $(".checkReforzadas").change(function(){
    comprobarMedidas();
  });
  $(".checkExamen").change(function(){
    comprobarMedidas();
  });
  $(".checkFide").change(function(){
    mostrarPrestania3();
  });

  $('#'+tabla+' tr:last .divSolicitaServicio input[type=radio]').change(function(){
    val=$(this).val();
    // if(val > 1 && servicios>1){
    //   alert('No puedes seleccionar representar a 3º personas si tiene más de un servicio');
    //   var id=$(this).attr('name');
    //   $('input[name='+id+'][value=1]').attr('checked','checked');
    // } else {
      mostrarPrestania(val);
    // }
  });
}

function pasarServicios(id,inversa=false){
  var val = 'SI';
  $(".divSolicitaServicio input[value=1]").each(function( index ) {
    if($(this).attr('checked')!='checked'){
      val='NO';
    }
  });
    if(inversa){
      $('#'+id).parent().css('display','none');
      $('#'+id).attr('checked',false);
      id='falso'+id;
      $('#'+id).parent().css('display','block');
      $('#'+id).attr('checked',false);
      servicios--;
    } else {
      // if(val == 'NO' && servicios >=1 ){        
      //   alert('No puedes seleccionar más de un servicio si representa a 3º personas');
      //   $('#'+id).attr('checked',false);
      // } else { 
        $('#'+id).parent().css('display','none');
        id=id.replace('falso','');
        $('#'+id).parent().css('display','block');
        $('#'+id).attr('checked','checked');
        servicios++;
      // }
    }
}

function pasarServiciosPersonalizados(id,inversa=false){
  var val = 'SI';
  $(".divSolicitaServicio input[value=1]").each(function( index ) {
    if($(this).attr('checked')!='checked'){
      val='NO';
    }
  });
    if(inversa){
      $('#'+id).parent().parent().parent().css('display','none');
      $('#'+id).attr('checked',false);
      id='falso'+id;
      $('#'+id).parent().parent().parent().css('display','block');
      $('#'+id).attr('checked',false);
      servicios--;
    } else {
      // if(val == 'NO' && servicios>=1){
      //   alert('No puedes seleccionar más de un servicio si representa a 3º personas');
      //   $('#'+id).attr('checked',false);
      // } else { 
      $('#'+id).parent().parent().parent().css('display','none');
      id=id.replace('falso','');
      $('#'+id).parent().parent().parent().css('display','block');
      $('#'+id).attr('checked','checked');
      servicios++;
      // }
    }
}

function comprobarMedidas(){
  var medidas = new Array();
  medidas['SIMPLIFICADAS'] = 'NO';
  medidas['NORMALES']      = 'SI';
  medidas['REFORZADAS']    = 'NO';
  medidas['EXAMEN']        = 'NO';
  
  var susceptibleSimplificadas = 'NO';
  
  $(".susceptibleSimplificadas").each(function(){
    var val = $(this).attr('checked');
    if (val == 'checked') { 
      medidas['SIMPLIFICADAS']  = 'SI';
      medidas['NORMALES']       = 'NO';
      susceptibleSimplificadas  = 'SI';
    }
  });

  $('.selectPaises').each(function(){
    // var val = $(this).find('option:selected').html();    
    var val = $(this).find('option:selected').attr('medidas'); 
    
    if(val!='' && val != 'null' && val != null) {      
      medidas[val] = 'SI';
    }
    /*var pais=$(this).find("option:selected").text();
    if(pais.indexOf("PARAISO FISCAL") != -1){ 
      medidas['REFORZADAS']='SI';
    }*/
  });

  $('.actividadlaboral').each(function() {
    var actividad = $(this).val();
    if(actividad == 'DESEMPLEADO' || actividad == 'CARGO' || actividad == 'SIN' ){ 
      medidas['REFORZADAS'] = 'SI';
    }
  });

  $(".checkReforzadas").each(function(){
    var val = $(this).attr('checked');
    if(val == 'checked'){ 
      medidas['REFORZADAS'] = 'SI';
    }
  });

  $(".checkExamen").each(function(){
    var val = $(this).attr('checked');
    if (val == 'checked') { 
      medidas['EXAMEN'] = 'SI';
      $('#resultado').val('EXAMEN');
      $('#resultado').selectpicker('refresh');
    }
  });
  

  /*
  if(susceptibleSimplificadas=='SI'){
    $('#susceptibleSimplificadas').removeClass('hide');
  } else {
    $('#susceptibleSimplificadas').addClass('hide');
  }*/
  
  //$('input[name=medidas][value='+medidas+']').attr('checked','checked');
  if(medidas['SIMPLIFICADAS'] == 'SI'){
    $('#checkMedidasSimplificadas').attr('checked','checked');
  } 
  else{
    $('#checkMedidasSimplificadas').attr('checked',false);
  }

  if(medidas['NORMALES'] == 'SI'){
    $('#checkMedidasNormales').attr('checked','checked');
  }
  else{
    $('#checkMedidasNormales').attr('checked',false);
  }

  if(medidas['REFORZADAS'] == 'SI'){
    $('#checkMedidasReforzadas').attr('checked','checked');
  }
  else{
    $('#checkMedidasReforzadas').attr('checked',false);
  }

  if(medidas['EXAMEN'] == 'SI') {
    $('#checkMedidasExamen').attr('checked','checked');
    mostrarPrestania4(medidas);
  }
  else {
    $('#checkMedidasExamen').attr('checked',false);
  }
  
  textoMedidas();
  
  if(comprobaciones()) {
    var resultado = 'PRESTAR';
    if($('#checkMedidasExamen').attr('checked') == 'checked') {
      resultado = 'EXAMEN';
    }
    
    $('#resultado').val(resultado);
    $('#resultado').selectpicker('refresh');
  } 
  else {
    $('#resultado').val('SIN');
    $('#resultado').selectpicker('refresh');
  }

}

function textoMedidas(){
  $('.textoMedidas').addClass('hide')
  $('.bloques').addClass('hide')
  if($('#checkMedidasNormales').attr('checked')=='checked'){
    $('#textoNormales').removeClass('hide');
    $('#bloque1').removeClass('hide');
    $('#bloque2').removeClass('hide');
  }
  if($('#checkMedidasSimplificadas').attr('checked')=='checked'){
    $('#textoSimplificadas').removeClass('hide');
    $('#bloque1').removeClass('hide');
  }
  if($('#checkMedidasReforzadas').attr('checked')=='checked'){ 
    $('#bloque1').removeClass('hide');
    $('#bloque2').removeClass('hide');
    if($('#checkNoPresencial').attr('checked')=='checked'){
      $('#textoReforzadas').removeClass('hide');
    } else {
      $('#textoReforzadas2').removeClass('hide');
    }
    $('#bloque3').removeClass('hide')
  }
  if($('#checkMedidasExamen').attr('checked')=='checked'){
    $('#bloque1').removeClass('hide');
    $('#bloque2').removeClass('hide');
    if($('#checkNoPresencial').attr('checked')=='checked'){
      //$('#textoReforzadas').removeClass('hide');
    } else {
      //$('#textoReforzadas2').removeClass('hide');
    }
    $('#bloque3').removeClass('hide')

    $('#textoExamen').removeClass('hide');
    $('#bloque4').removeClass('hide');
  }
}

function comprobaciones(){
  var res = true;
  medida  = $('input[name=medidas]:checked').val();
  if($('#checkMedidasNormales').attr('checked')=='checked'){
    var listado = [
      'checkDNI',
      'checkEscritura1',
      'checkPoderes',
      'checkDeclaracion1'
    ];
  } 
  else if($('#checkMedidasSimplificadas').attr('checked')=='checked'){
    var listado = [
      'checkDNI',
      'checkEscritura1',
      'checkPoderes'
    ];
  } 
  if($('#checkMedidasReforzadas').attr('checked')=='checked'){
    var listado = [
      'checkDNI',
      'checkEscritura1',
      'checkPoderes',
      'checkDeclaracion1',
      'checkIdentificacion1',
      'checkIdentificacion2',
      'checkIdentificacion3',
      'checkRenta',
      'checkReintegro',
      'checkAutorizacion1',
      'checkAutorizacion2',
      'checkBienes',
      'checkLibro',
      'checkImpuesto'
    ];
  } 
  if($('#checkMedidasExamen').attr('checked') == 'checked'){
    var listado = [
      'checkDNI',
      'checkEscritura1',
      'checkPoderes',
      'checkDeclaracion1',
      'checkIdentificacion1',
      'checkIdentificacion2',
      'checkIdentificacion3',
      'checkRenta',
      'checkReintegro',
      'checkAutorizacion1',
      'checkAutorizacion2',
      'checkBienes',
      'checkLibro',
      'checkImpuesto',
      'checkCertificado1',
      'checkContrato1',
      'checkCertificado2',
      'checkCertificacion1',
      'checkCertificacion2',
      'checkDeclaracion2',
      'checkFacturas',
      'checkContrato2',
      'checkDocumentoTC2',
      'checkDocumentoUnico',
      'checkInforme',
      'checkNota',
      'checkEscritura2'
    ];
  }

  if(listado != undefined) {
    for (var i=0; i < listado.length; i++) {
      if($('#'+listado[i]).attr('checked') != 'checked' && $('#'+listado[i]+'Eliminado').val() == 'NO'){
        res = false;
      }
    }
  } 
  else {
    res = false;
  }
  
  return res;

}


function informeRiesgo(codigo,cliente){
  var parametros = {
      "codigo" : codigo,
      "cliente": cliente
    };
  $.ajax({
    type: "POST",
    url: "../listadoAjax.php?include=informes-clientes&funcion=informeRiesgo();",
    dataType: "json",
    data: parametros
  }).
  done( function(response){
    if(response != false){
      $('#informeRiesgo').text(response.texto);
      //$('#decision').text(response.sepblac);
    }
  });
}

function conclusion(val){
  if(val==1){
    $('#textoConclusion').text("CONCURREN EN LA OPERATIVA INDICIOS O CERTEZA DE RELACIÓN CON EL BLANQUEO DE CAPITALES O LA FINANCIACIÓN DEL TERRORISMO, PROCEDIENDO LA COMUNICACIÓN, SIN DILACIÓN, POR INDICIOS  AL SERVICIO EJECUTIVO DE LA COMISIÓN –soporte y formato establecido-.");
    $('#divInterrupcion').removeClass('hide');
  } else {
    $('#textoConclusion').text("NO CONCURREN EN LA OPERATIVA INDICIOS O CERTEZA DE RELACIÓN CON EL BLANQUEO DE CAPITALES O LA FINANCIACIÓN DEL TERRORISMO, PROCEDIENDO LA PRESTACIÓN DEL SERVICIO");
    $('#divInterrupcion').addClass('hide');
  }
}

function interrupcion(val){
  if(val=='NO'){
    $('#textoInterrupcion').removeClass('hide');
  } else {
    $('#textoInterrupcion').addClass('hide');
  }
}

function comprobarPais(val){
  var res = '';
  normales = [
    'Alemania',
    'Aruba',
    'Austria',
    'Bélgica',
    'Bonaire',
    'Brasil',
    'Canadá',
    'Checa, República',
    'Chipre',
    'Corea del Sur',
    'Dinamarca',
    'Eslovaquia',
    'Eslovenia',
    'España',
    'Estados Unidos',
    'Estonia',
    'Finlandia',
    'Francia',
    'Gambia',
    'Georgia',
    'Ghana',
    'Grecia',
    'Hungría',
    'India',
    'Irlanda',
    'Italia',
    'Japón',
    'Letonia',
    'Lituania',
    'Luxemburgo',
    'Malta',
    'Mayotte',
    'México',
    'Nueva Caledonia',
    'Países Bajos',
    'Polinesia Francesa',
    'Polonia',
    'Portugal',
    'Reino Unido',
    'Rumania',
    'Saba',
    'San Pedro y Miquelón',
    'Singapur',
    'Sint Eustatius',
    'Sint Maarten',
    'Suecia',
    'Suiza',
    'Wallis y Futuna'
  ];

  reforzadas = [
    'Abjasia',
    'Acrotiri y Dhekelia',
    'Albania',
    'Andorra',
    'Angola',
    'Antigua y Barbuda',
    'Arabia Saudita',
    'Argelia',
    'Argentina',
    'Armenia',
    'Australia',
    'Azerbaiyán',
    'Bahamas',
    'Bangladés',
    'Barbados',
    'Belice',
    'Benín',
    'Bielorrusia',
    'Birmania',
    'Bolivia',
    'Botsuana',
    'Burkina Faso',
    'Burundi',
    'Bután',
    'Cabo Verde',
    'Camboya',
    'Catar',
    'Centroafricana, República',
    'Chad',
    'Chile',
    'China',
    'Chipre del Norte',
    'Cocos, Islas',
    'Colombia',
    'Comoras',
    'Congo, República del',
    'Congo, República Democrática del',
    'Costa de Marfil',
    'Costa Rica',
    'Cuba',
    'Curazao',
    'Dominicana, República',
    'Ecuador',
    'Egipto',
    'El Salvador',
    'Emiratos Árabes Unidos',
    'Eritrea',
    'Etiopía',
    'Feroe, Islas',
    'Filipinas',
    'Gabón',
    'Gibraltar',
    'Groenlandia',
    'Guatemala',
    'Guinea',
    'Guinea-Bisáu',
    'Guinea Ecuatorial',
    'Haití',
    'Honduras',
    'Indonesia',
    'Islandia',
    'Israel',
    'Jamaica',
    'Kazajistán',
    'Kirguistán',
    'Kiribati',
    'Kosovo',
    'Kuwait',
    'Lesoto',
    'Libia',
    'Macedonia del Norte',
    'Madagascar',
    'Malasia',
    'Malaui',
    'Maldivas',
    'Malí',
    'Marruecos',
    'Marshall, Islas',
    'Mauritania',
    'Micronesia',
    'Moldavia',
    'Mongolia',
    'Montenegro',
    'Mozambique',
    'Nagorno Karabaj',
    'Navidad, Isla de',
    'Nepal',
    'Nicaragua',
    'Níger',
    'Niue',
    'Norfolk, Isla',
    'Noruega',
    'Nueva Rusia',
    'Nueva Zelanda',
    'Osetia del Sur',
    'Pakistán',
    'Palestina',
    'Papúa Nueva Guinea',
    'Paraguay',
    'Perú',
    'Pitcairn, Islas',
    'Puerto Rico',
    'Ruanda',
    'Sahara Occidental',
    'San Bartolomé',
    'San Cristóbal y Nieves',
    'San Marino',
    'San Martín',
    'Santa Elena, Ascensión y Tristán de Acuña',
    'Santo Tomé y Príncipe',
    'Senegal',
    'Serbia',
    'Seychelles',
    'Sierra Leona',
    'Somalia',
    'Somalilandia',
    'Sri Lanka',
    'Eswatini',
    'Sudán',
    'Sudán del Sur',
    'Surinam',
    'Svalbard',
    'Tailandia',
    'Taiwán',
    'Tanzania',
    'Tayikistán',
    'Timor Oriental',
    'Togo',
    'Tokelau',
    'Tonga',
    'Transnistria',
    'Túnez',
    'Turkmenistán',
    'Turquía',
    'Tuvalu',
    'Ucrania',
    'Uganda',
    'Uruguay',
    'Uzbekistán',
    'Vaticano, Ciudad del',  
    'Vietnam',
    'Yibuti',
    'Zambia',
    'Zimbabue'
  ];

  examen = [
    'Afganistán---PARAISO FISCAL---',
    'Anguila---No cooperador con UE---',
    'Baréin---PARAISO FISCAL--',
    'Bermudas---PARAISO FISCAL---',
    'Bosnia y Herzegovina---PARAISO FISCAL---',    
    'Bulgaria---PARAISO FISCAL---',
    'Brunéi---PARAISO FISCAL---',
    'Caimán, Islas---PARAISO FISCAL---',    
    'Camerún---PARAISO FISCAL---',
    'Cook, Islas---PARAISO FISCAL---',
    'Corea del Norte---PARAISO FISCAL---',
    'Croacia---PARAISO FISCAL---',
    'Dominica---PARAISO FISCAL---',
    'Fiyi---No cooperador con UE---',
    'Granada---PARAISO FISCAL---',
    'Guam---No cooperador con UE---',
    'Guernsey---PARAISO FISCAL---',
    'Guyana---PARAISO FISCAL---',
    'Hong Kong---PARAISO FISCAL---',
    'Irak---PARAISO FISCAL---',
    'Irán---PARAISO FISCAL---',
    'Jersey---PARAISO FISCAL---',
    'Jordania---PARAISO FISCAL---',
    'Kenia---PARAISO FISCAL---',
    'Laos---PARAISO FISCAL---',
    'Líbano---PARAISO FISCAL---',
    'Liberia---PARAISO FISCAL---',
    'Liechtenstein---PARAISO FISCAL---',
    'Macao---PARAISO FISCAL---',
    'Malvinas, Islas---PARAISO FISCAL---',
    'Man, Isla de---PARAISO FISCAL---',
    'Marianas del Norte, Islas---PARAISO FISCAL---',
    'Mauricio---PARAISO FISCAL---',
    'Mónaco---PARAISO FISCAL---',
    'Montserrat---PARAISO FISCAL---',
    'Nauru---PARAISO FISCAL---',
    'Namibia---PARAISO FISCAL---',
    'Nigeria---PARAISO FISCAL---',
    'Omán---PARAISO FISCAL---',
    'Palaos---No cooperador con UE---',
    'Panamá---No cooperador con UE---',
    'Rusia---No cooperador con UE---',
    'Salomón, Islas---PARAISO FISCAL---',
    'Samoa---No cooperador con UE---',
    'Samoa Americana---No cooperador con UE---',
    'San Vicente y las Granadinas---PARAISO FISCAL---',
    'Santa Lucía---PARAISO FISCAL---',
    'Siria---PARAISO FISCAL---',
    'Sudáfrica---PARAISO FISCAL---',
    'Trinidad y Tobago---No cooperador con UE---',
    'Turcas y Caicos, Islas---PARAISO FISCAL---',
    'Vanuatu---No cooperador con UE---',  
    'Venezuela---PARAISO FISCAL---',
    'Vírgenes Británicas, Islas---PARAISO FISCAL---',
    'Vírgenes de los Estados Unidos, Islas---No cooperador con UE---',
    'Yemen---PARAISO FISCAL---',
  ];

  if(normales.indexOf(val)>-1){
    res='NORMALES';
  } else if(reforzadas.indexOf(val)>-1){
    res='REFORZADAS';
  } else if(examen.indexOf(val)>-1){
    res='EXAMEN';
  }
  return res;
}

function insertaFilaServiciosPersonalizados(tabla){
  insertaFila(tabla);
  insertaFila(tabla.replace('Falso',''));
}

function eliminaFilaServiciosPersonalizados(tabla){
  eliminaFila(tabla.replace('Falso',''));
  eliminaFila(tabla);
}
</script>
</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
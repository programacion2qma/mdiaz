<?php

@include_once('funciones.php');
compruebaSesion();

$contenido=generaInforme();
$nombre='informe_de_riesgo.pdf';

require_once('../../api/html2pdf/html2pdf.class.php');
$html2pdf=new HTML2PDF('P','A4','es');
$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->WriteHTML($contenido);
$html2pdf->Output($nombre);


function generaInforme(){

	desencriptaGET();
	
    $datos = datosRegistro("informes_clientes",$_GET['codigo']);
	if($_SESSION['tipoUsuario'] != 'ADMIN'){
		compruebaAcceso($datos['codigoUsuario']);
	}	

    $cliente = datosRegistro('usuarios_clientes',$datos['codigoUsuario'],'codigoUsuario');	
    $cliente = datosRegistro('clientes',$cliente['codigoCliente']);

	
	$anchoCeldaTitulo='100%;';
	$logoCliente='';
	if($cliente['ficheroLogo']!='' && $cliente['ficheroLogo']!='NO'){
		
		$logoCliente="
		<td class='tdLogo' rowspan='2'>
			<img src='../documentos/logos-clientes/".$cliente['ficheroLogo']."' />
		</td>";

		$anchoCeldaTitulo='81%;';
	}

	$documentos=array('DNI'=>'DNI','PASAPORTE'=>'Pasaporte','RESIDENCIA'=>'Tarjeta de residencia','EXTRANJERO'=>'Tarjeta de identidad de extranjero','ORIGEN'=>'Tarjeta oficial de identidad personal expedido por las autoridades de origen (ciudadanos de la Unión Europea','EXTERIORES'=>'Documento de identidad expedido por el ministerio de asuntos exteriores de cooperación (personal de representaciones diplomáticas y consulares','OTROS'=>'Otro documento de identidad personal');
	$actividades=array('SOCIO'=>'Socio','EMPRESARIO'=>'Empresario individual','PROFESIONAL'=>'Profesional ejerciente','TRABAJADOR'=>'Trabajador por cuenta ajena','JUBILADO'=>'Jubilado','PENSIONISTA'=>'Pensionista','DESEMPLEADO'=>'Desempleado','CARGO'=>'Cargo público','SIN'=>'Sin actividad laboral','CONYUGE'=>'Cónyuge de otro interviniente');
	$solicitaServicio=array(1=>'En nombre propio',2=>'En nombre propio y de 3º/s',3=>'En representacion de tercero/s');
	$paises = preparaArrayPaises();
	$ejercido='Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público';
	$noPresencial='Solicita servicio de forma no presencial';

	$checkGeneral=array(
		'checkCargoPublico'=>'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',
		'checkNoPresencial'=>'Solicita servicio de forma no presencial',
		'checkCargoPublicoJuridica'=>'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público',
		'checkMenorJuridica'=>'Menor de edad',
		'checkIncapacitadoJuridica'=>'Incapacitado');
	
	$checkCircunstancias=array(
		'checkPortador'=>'Relaciones de negocio y operaciones con clientes que emplean habitualmente medios de pago al portador',
		'checkIntermediarios'=>'Relaciones de negocio y operaciones ejecutadas a través de intermediarios',
		'checkFondos'=>'Se transfieren fondos de o hacía países, territorios o jurisdicciones de riesgos',
		'checkNaturaleza'=>'La naturaleza de la operación u operaciones, activas o pasivas, del cliente no se corresponden con su actividad o antecedentes operativos',
		'checkVolumen'=>'El volumen de la operación u operaciones, activas o pasivas, del cliente no se corresponden con su actividad o antecedentes operativos',
		'checkOperativa'=>'Operativa con agentes que, por su naturaleza, volumen, cuantía, zona geográfica u otras características de las operaciones, difieran significativamente de las usuales u ordinarias del sector o de las propias del sujeto obligado',
		'checkPrestacion1'=>'En la prestación del servicio se detecta que una misma cuenta, sin causa que lo justifique, está siendo abonada mediante ingresos en efectivo por un número elevado de personas',
		'checkPrestacion2'=>'En la prestación del servicio se detecta que una misma cuenta, sin causa que lo justifique, recibe múltiples ingresos en efectivo de la misma persona',
		'checkPrestacion3'=>'En la prestación del servicio se detecta pluralidad de transferencias realizadas por varios ordenantes a un mismo beneficiario en el exterior o por un único ordenante en el exterior a varios beneficiarios en España, sin que se aprecie relación de negocio entre los intervinientes',
		'checkPrestacion4'=>'En la prestación del servicio se detectan transferencias en las que no se contiene la identidad del ordenante o el número de cuenta origen de la transferencia');

	if($datos['checkCargoPublico']=='NO'){
		$ejercido='No ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público';
	}

	if($datos['checkNoPresencial']=='NO'){
		$noPresencial='No solicita servicio de forma no presencial';
	}

	$conclusiones=array(''=>'',1=>'No se aprecia justificación económica,  profesional o de negocio para la realización de las operaciones',2=>'Se aprecia justificación económica, profesional o de negocio para la realización de las operaciones');
	$texto="";
	$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        .cabecera{
	        	border:1px solid #000;
	        	width:100%;
	        	border-collapse:collapse;
	        }

	        .cabecera td{
	        	border:1px solid #000;
	        }

	        .cabecera .tdLogo{
	        	background:#56A4C0;
	        	width:19%;
	        	text-align:center;
	        	border-right:0px;
	        }

	        .cabecera .tdLogo img{
	        	width:100%;
	        }

	        .cabecera .tdTitulo{
	        	width:".$anchoCeldaTitulo.";
	        	text-align:center;
	        }

	        .cabecera .tdCliente{
	        	text-align:left;
	        	padding-left:10px;
	        	background:#56A4C0;
	        	color:#FFF;
	        	border-left:1px solid #56A4C0;
	        }

	        .tablaDatos{
	    		border:1px solid #000;
	        	width:100%;
	        	border-collapse:collapse;
	        	margin-top:30px;
	    	}

	    	.tablaDatos th{
	    		background:#56A4C0;
	        	color:#FFF;
	        	text-align:center;
	        	border:1px solid #000;
	        	width:100%;
	        	padding-top:10px;
	    	}

	    	.tablaDatos td{
	    		border:1px solid #000;
	    		padding-top:3px;
	    		padding-bottom:3px;
	    	}

	    	.tablaDatos .tituloDato{
	    		width:30%;
	    		padding-left:10px;
	        	background:#56A4C0;
	        	color:#FFF;
	    	}

	    	.tablaDatos .dato{
	    		width:70%;
	    		padding-left:10px;
	    	}

	    	.tablaDatos .tdCheck{
	    		width:100%;
	    		padding-left:10px;
	    	}

	    	.tablaDatos .a15{
	    		width:15%;
	    		padding:0px;   	
	    	}

	    	.tablaDatos .a20{
	    		width:20%;
	    		padding:0px;   	
	    	}

	    	.tablaDatos .a70{
	    		width:70%;
	    		padding:0px;   	
	    	}

	    	.tablaDatos .a80{
	    		width:80%;	
	    	}

	    	.tablaDatos .a95{
	    		width:95%;  	
	    	}

	    	.tablaDatos .a5{
	    		width:5%;
	    		padding:0px;   	
	    	}

	    	.tablaDatos .a83{
	    		width:83.33%;
	    		padding:0px;   	
	    	}

	    	.tablaDatos .a16{
	    		width:16.66%;
	    		padding:0px;   	
	    	}

	    	.tablaDatos .a100{
	    		width:100%;
	    			
	    	}

	    	.tablaDatos .a40{
	    		width:40%;
	    		padding:0px;   	
	    	}

	    	.tablaDatos .a60{
	    		width:60%; 	
	    	}

	    	.tablaDatos .centro{
	    		text-align:center;
	    	}

	    	.tablaDatos .tdSeparacion{
	    		width:100%;
	        	background:#56A4C0;
	        	padding:2px
	    	}

	    	.tablaDatos .tdMarcado{
	        	background:#56A4C0;
	    	}

	    	.tablaDatos .a10{
	    		width:10%; 	
	    	}

	    	.tablaServicios .a15{
	    		text-align:center; 	
	    	}

	    	.tablaServicios td{
	    		padding-left:20px !important;  	
	    	}

	    	.textoPie{
	    		font-size:11px;
	    		position:absolute;
	    		bottom:-13px;
	    		font-style:italic;
	    	}

	-->
	</style>
	<page footer='page'>
	<table class='cabecera'>
		<tr>
			".$logoCliente."
			<td class='tdTitulo tdCliente'><b>".$cliente['razonSocial']."</b></td>	
		</tr>
		<tr>
			<td class='tdTitulo'><h1>INFORME DE RIESGO</h1></td>	
		</tr>
	</table>
	<table class='tablaDatos'>
		<tr>
			<td class='tituloDato'>Referencia</td>
			<td class='dato'>".$datos['referencia']." / ".substr($datos['anio'], 2)."</td>
		</tr>
		<tr>
			<td class='tituloDato'>Fecha del informe</td>
			<td class='dato'>".formateaFechaWeb($datos['fecha'])."</td>
		</tr>
	</table>

	<table class='tablaDatos'>
		<tr>
			<th class='tituloDato' colspan='2'>PERSONA FÍSICA QUE SOLICITA EL SERVICIO</th>
		</tr>";

	$personas=consultaBD('SELECT * FROM informes_clientes_fisicas WHERE codigoInforme='.$datos['codigo'],true);
	while($persona=mysql_fetch_assoc($personas)){
		$contenido.="
			<tr>
				<td class='tituloDato'>Nombre y apellidos</td>
				<td class='dato'>".$persona['nombre']."</td>
			</tr>
			<tr>
				<td class='tituloDato'>Tipo de documento </td>
				<td class='dato'>".$documentos[$persona['tipoDocumento']]."</td>
			</tr>
			<tr>
				<td class='tituloDato'>Número </td>
				<td class='dato'>".$persona['numeroDocumento']."</td>
			</tr>
			<tr>
				<td class='tituloDato'>Residencia </td>
				<td class='dato'>".$persona['calle']." - ".$persona['localidad']." (".$persona['cp'].") - ".$persona['ciudad']." <b>".$paises[$persona['pais']]."</b></td>
			</tr>
			<tr>
				<td class='tituloDato'>Actividad laboral </td>
				<td class='dato'>".$actividades[$persona['actividadLaboral']]."</td>
			</tr>
			<tr>
				<td class='tituloDato'>Solicita el servicio </td>
				<td class='dato'>".$solicitaServicio[$persona['solicitaServicio']]."</td>
			</tr>";

		if($persona['checkCargoPublico']=='SI'){
			$contenido.="<tr>
							<td class='tdCheck' colspan='2'>".$checkGeneral['checkCargoPublico']."</td>
						</tr>";
		}

		if($persona['checkNoPresencial']=='SI'){
			$contenido.="<tr>
							<td class='tdCheck' colspan='2'>".$checkGeneral['checkNoPresencial']."</td>
						</tr>";
		}

		$contenido.="
			<tr>
				<td class='tituloDato'>Observaciones </td>
				<td class='dato'>".$persona['observaciones']."</td>
			</tr>";
		$contenido.="<tr>
						<td class='tdSeparacion' colspan='2'></td>
					</tr>";
	}

	$contenido.="</table>";
	if($datos['checkPersonasFisicas']=='SI'){
		$contenido.="<table class='tablaDatos'>
		<tr>
			<th class='tituloDato' colspan='2'>PERSONA FÍSICA REPRESENTADA</th>
		</tr>";
		$personas=consultaBD('SELECT * FROM personas_fisicas_informes_clientes WHERE codigoInforme='.$datos['codigo'],true);
		$check=array('checkCargoPublicoFisica'=>'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público','checkMenorFisica'=>'Menor de edad','checkIncapacitadoFisica'=>'Incapacitado');
		while($persona=mysql_fetch_assoc($personas)){
			$contenido.="<tr>
							<td class='tituloDato'>Nombre</td>
							<td class='dato'>".$persona['nombreFisica']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Tipo de documento</td>
							<td class='dato'>".$documentos[$persona['tipoDocumentoFisica']]."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Número</td>
							<td class='dato'>".$persona['numeroDocumentoFisica']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Residencia</td>
							<td class='dato'>".$persona['calleFisica']." - ".$persona['localidadFisica']." (".$persona['cpFisica'].") - ".$persona['ciudadFisica']." <b>".$paises[$persona['paisFisica']]."</b></td>
						</tr>
						<tr>
							<td class='tituloDato'>actividadLaboral</td>
							<td class='dato'>".$actividades[$persona['actividadLaboralFisica']]."</td>
						</tr>";

			foreach ($check as $key => $value) {
				if($persona[$key]=='SI'){
					$contenido.="<tr>
									<td class='tdCheck' colspan='2'>".$value."</td>
								</tr>";
				}
			}

			if($persona['observacionesFisica']!=''){
				$contenido.="<tr>
								<td class='tituloDato'>Observaciones</td>
								<td class='dato'>".$persona['observacionesFisica']."</td>
							</tr>";
			}

			$contenido.="<tr>
							<td class='tdSeparacion' colspan='2'></td>
						</tr>";
					
		}

		$contenido.="</table>";
	}

	if($datos['checkPersonasJuridicas']=='SI'){
		$contenido.="<table class='tablaDatos'>
		<tr>
			<th class='tituloDato' colspan='2'>PERSONA JURIDÍCA REPRESENTADA</th>
		</tr>";
		$personas=consultaBD('SELECT * FROM personas_juridicas_informes_clientes WHERE codigoInforme='.$datos['codigo'],true);
		$check=array('checkEntidadDerecho'=>'Entidad de derecho público de Estado miembro de la Unión Europea o de país tercero equivalente','checkSociedadPersona'=>'Sociedad o persona jurídica controlada o participada mayoritariamente por entidades de derecho público de los Estados miembros de la Unión Europea o de países tercero equivalente','checkEntidadFinanciera'=>'Entidad financiera -exceptuadas las entidades de pago- domiciliada en la Unión Europea o en país tercero equivalente, que sea objeto de supervisión para garantizar el cumplimiento de las obligaciones de prevención del blanqueo de capitales y de la financiación del terrorismo','checkSucursalFilial'=>'Sucursal o filial de entidades financieras -exceptuadas las entidades de pago-, domiciliada en la Unión Europea o en país tercero equivalente, cuando esté sometida por la matriz a procedimientos de prevención del blanqueo de capitales y de la financiación del terrorismo.','checkSociedadCotizada'=>'Sociedad cotizada cuyos valores se admiten a negociación en un mercado regulado de la Unión Europea o de país tercero equivalente, sus sucursales y filiales participadas mayoritariamente','checkSociedadMera'=>'Sociedad de mera tenencia de activos ','checkSociedadCuya'=>'Sociedad cuya estructura accionarial y de control no sea transparente o resulte inusual o excesivamente compleja');
		
		while($persona=mysql_fetch_assoc($personas)){
			$contenido.="<tr>
							<td class='tituloDato'>Nombre comercial</td>
							<td class='dato'>".$persona['nombreJuridica']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Razón social</td>
							<td class='dato'>".$persona['razonSocial']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Forma jurídica</td>
							<td class='dato'>".$persona['formaJuridica']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>CIF</td>
							<td class='dato'>".$persona['numCIF']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Objeto social</td>
							<td class='dato'>".$persona['objetoSocial']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Residencia</td>
							<td class='dato'>".$persona['calleJuridica']." - ".$persona['localidadJuridica']." (".$persona['cpJuridica'].") - ".$persona['ciudadJuridica']." <b>".$paises[$persona['paisJuridica']]."</b></td>
						</tr>";

			foreach ($check as $key => $value) {
				if($persona[$key]=='SI'){
					$contenido.="<tr>
									<td class='tdCheck' colspan='2'>".$value."</td>
								</tr>";
				}
			}

			if($persona['observacionesJuridica']!=''){
				$contenido.="<tr>
								<td class='tituloDato'>Observaciones</td>
								<td class='dato'>".$persona['observacionesJuridica']."</td>
							</tr>";
			}

			$contenido.="<tr>
							<td class='tdSeparacion' colspan='2'></td>
						</tr>";
					
		}

		$contenido.="</table>
					<table class='tablaDatos'>
						<tr>
							<th class='tituloDato' colspan='2'>TITULAR DE LA PERSONA JURIDÍCA REPRESENTADA</th>
						</tr>";

		$listado=consultaBD('SELECT * FROM informes_clientes_juridicas WHERE codigoInforme='.$datos['codigo'],true);

		while($persona=mysql_fetch_assoc($listado)){
			$contenido.="
						<tr>
							<td class='tituloDato'>Nombre y apellidos</td>
							<td class='dato'>".$persona['nombre']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Tipo de documento </td>
							<td class='dato'>".$documentos[$persona['tipoDocumento']]."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Número </td>
							<td class='dato'>".$persona['numeroDocumento']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Residencia </td>
							<td class='dato'>".$persona['calle']." - ".$persona['localidad']." (".$persona['cp'].") - ".$persona['ciudad']." <b>".$paises[$persona['pais']]."</b></td>
						</tr>
						<tr>
							<td class='tituloDato'>Actividad laboral </td>
							<td class='dato'>".$actividades[$persona['actividadLaboral']]."</td>
						</tr>";

			if($persona['checkCargoPublico']=='SI'){
				$contenido.="<tr>
							<td class='tdCheck' colspan='2'>".$checkGeneral['checkCargoPublicoJuridica']."</td>
						</tr>";
			}

			if($persona['checkMenor']=='SI'){
				$contenido.="<tr>
							<td class='tdCheck' colspan='2'>".$checkGeneral['checkMenorJuridica']."</td>
						</tr>";
			}

			if($persona['checkIncapacitado']=='SI'){
				$contenido.="<tr>
							<td class='tdCheck' colspan='2'>".$checkGeneral['checkIncapacitadoJuridica']."</td>
						</tr>";
			}

			$contenido.="<tr>
							<td class='tdSeparacion' colspan='2'></td>
						</tr>";
		}

		$contenido.="</table>";
	}

	if($datos['checkPersonasSinJuridicas']=='SI'){
		$contenido.="<table class='tablaDatos'>
		<tr>
			<th class='tituloDato' colspan='2'>PERSONA FÍSICA QUE ACTÚA EN REPRESENTACIÓN DE ENTIDAD SIN PERSONALIDAD JURÍDICA</th>
		</tr>";

		$personas=consultaBD('SELECT * FROM personas_sinjuridicas_informes_clientes WHERE codigoInforme='.$datos['codigo'],true);

		$check=array('checkCargoPublicoSinJuridica'=>'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público','checkMenorSinJuridica'=>'Menor de edad','checkIncapacitadoSinJuridica'=>'Incapacitado');

		while($persona=mysql_fetch_assoc($personas)){
			$contenido.="<tr>
							<td class='tituloDato'>Nombre</td>
							<td class='dato'>".$persona['nombreSinJuridica']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Tipo de documento</td>
							<td class='dato'>".$documentos[$persona['tipoDocumentoSinJuridica']]."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Número</td>
							<td class='dato'>".$persona['numeroDocumentoSinJuridica']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Residencia</td>
							<td class='dato'>".$persona['calleSinJuridica']." - ".$persona['localidadSinJuridica']." (".$persona['cpSinJuridica'].") - ".$persona['ciudadSinJuridica']." <b>".$paises[$persona['paisSinJuridica']]."</b></td>
						</tr>
						<tr>
							<td class='tituloDato'>Actividad laboral</td>
							<td class='dato'>".$actividades[$persona['actividadLaboralSinJuridica']]."</td>
						</tr>";
			
			foreach ($check as $key => $value) {
				if($persona[$key]=='SI'){
					$contenido.="<tr>
									<td class='tdCheck' colspan='2'>".$value."</td>
								</tr>";
				}
			}
			
			if($persona['observacionesSinJuridica']!=''){
				$contenido.="<tr>
								<td class='tituloDato'>Observaciones</td>
								<td class='dato'>".$persona['observacionesSinJuridica']."</td>
							</tr>";
			}
			
			$contenido.="<tr>
							<td class='tdSeparacion' colspan='2'></td>
						</tr>";
					
		}

		$contenido.="</table>";
	}

	if($datos['checkFideocomisos']=='SI'){
		$fide=array('FIDEICOMITENTE'=>'Fideicomitente','FIDEICOMISARIO'=>'Fideocomisario','PROTECTOR'=>'Protector','BENEFICIARIOS'=>'Beneficiarios','CONTROL'=>'Control efectivo final sobre el fideicomiso');
		$contenido.="<table class='tablaDatos'>
		<tr>
			<th class='tituloDato' colspan='6'>PERSONAS FÍSICAS QUE INTERVIENEN EN FIDEICOMISO</th>
		</tr>";
		$personas=consultaBD('SELECT * FROM personas_fideocomisos_informes_clientes WHERE codigoInforme='.$datos['codigo'],true);
		$check=array('checkCargoPublicoFideocomiso'=>'Ha ejercido cargo público en los dos años anteriores, es familiar o allegado de cargo público','checkMenorFideocomiso'=>'Menor de edad','checkIncapacitadoFideocomiso'=>'Incapacitado','checkFideocomisarioFideocomiso'=>'Fideicomisario no declara su condición de tal, siendo determinada por el sujeto obligado');
		
		while($persona=mysql_fetch_assoc($personas)){
			$contenido.="<tr>
							<td class='tituloDato'>Nombre</td>
							<td class='dato'>".$persona['nombreFideocomiso']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Tipo de documento</td>
							<td class='dato'>".$documentos[$persona['tipoDocumentoFideocomiso']]."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Número</td>
							<td class='dato'>".$persona['numeroDocumentoFideocomiso']."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Residencia</td>
							<td class='dato'>".$persona['calleFideocomiso']." - ".$persona['localidadFideocomiso']." (".$persona['cpFideocomiso'].") - ".$persona['ciudadFideocomiso']." <b>".$paises[$persona['paisFideocomiso']]."</b></td>
						</tr>
						<tr>
							<td class='tituloDato'>Condición</td>
							<td class='dato'>".$fide[$persona['condicionFideocomiso']]."</td>
						</tr>
						<tr>
							<td class='tituloDato'>Actividad laboral</td>
							<td class='dato'>".$actividades[$persona['actividadLaboralFideocomiso']]."</td>
						</tr>";

			foreach ($check as $key => $value) {
				if($persona[$key]=='SI'){
					$contenido.="<tr>
									<td class='tdCheck' colspan='2'>".$value."</td>
								</tr>";
				}
			}

			if($persona['observacionesFideocomiso']!=''){
				$contenido.="<tr>
								<td class='tituloDato'>Observaciones</td>
								<td class='dato'>".$persona['observacionesFideocomiso']."</td>
							</tr>";
			}
		
			$contenido.="<tr>
							<td class='tdSeparacion' colspan='2'></td>
						</tr>";
					
		}

		$contenido.="</table>";
	}

	$sql = 'SELECT * FROM servicios_informes_clientes WHERE codigoInforme='.$datos['codigo'];
	$servicios=consultaBD($sql,true,true);
	$hay=false;

	// 26/04/2022 para los servicios no personalizados que no cargan sus fechas, los guardamos si hay
	$fechaContratacion = array();
	$fechaFinalizacion = array();

	$serviciosContratados=array();
	for ($i=1; $i <= 29 ; $i++) { 
		if($servicios['checkServicio'.$i]=='SI'){
			$hay=true;

			// $i corresponde al codigo del servicio
			array_push($serviciosContratados, $i.'_FIJO');
			$fechaContratacion[$i] = $servicios['fechaContratacioncheckServicio'.$i];
			$fechaFinalizacion[$i] = $servicios['fechaFinalizacioncheckServicio'.$i];
		}
	}
	$servicios=consultaBD('SELECT * FROM servicios_informes_clientes_personalizados_seleccionados WHERE codigoInforme='.$datos['codigo'],true);
	while($s=mysql_fetch_assoc($servicios)){
		$hay=true;
		array_push($serviciosContratados, $s['codigoServicio'].'_PERS');
	}



	if($hay){
		$arrayServicios=obtieneArrayServicios(true);
		$contenido.="
		<table class='tablaDatos tablaServicios'>
		<tr>
			<th class='tituloDato' colspan='3'>SERVICIOS CONTRATADOS POR EL CLIENTE</th>
		</tr>
		<tr>
			<td class='tituloDato a70'>Servicio</td>
			<td class='tituloDato a15'>Contratación</td>
			<td class='tituloDato a15'>Finalización</td>
		</tr>";
		foreach ($serviciosContratados as $value) {
			$value=explode('_',$value);
			if($value[1]=='FIJO'){
				// VALUE[0] pasa a ser la posicion del contador Es decir, saca la fecha tal del servicio x que se almacena en value 0
				$contenido.="
						<tr>
							<td class='dato a70'><b>".$arrayServicios[$value[0]]."</b></td>
							<td class='dato a15'>".formateaFechaWeb($fechaContratacion[$value[0]])."</td>
							<td class='dato a15'>".formateaFechaWeb($fechaFinalizacion[$value[0]])."</td>
						</tr>";
			} else {
				$serviP=consultaBD('SELECT s1.nombre, s2.fechaContratacion, s2.fechaFinalizacion FROM servicios_informes_clientes_personalizados s1 INNER JOIN servicios_informes_clientes_personalizados_seleccionados s2 ON s1.codigo=s2.codigoServicio WHERE codigoInforme='.$datos['codigo'].' AND s1.codigo='.$value[0],true,true);
				$contenido.="
						<tr>
							<td class='dato a70'><b>".$serviP['nombre']."</b></td>
							<td class='dato a15'>".formateaFechaWeb($serviP['fechaContratacion'])."</td>
							<td class='dato a15'>".formateaFechaWeb($serviP['fechaFinalizacion'])."</td>
						</tr>";
			}
			$listado=consultaBD('SELECT * FROM servicios_personas_informes_clientes WHERE servicio='.$value[0].' AND codigoInforme='.$datos['codigo'].' AND tipoServicio="'.$value[1].'";',true);
			$personas='';
			$tablas=array('FISICAS'=>'personas_fisicas_informes_clientes','JURIDICAS'=>'personas_juridicas_informes_clientes','SINJURIDICAS'=>'personas_sinjuridicas_informes_clientes','FIDEOCOMISOS'=>'personas_fideocomisos_informes_clientes');
			$campos=array('FISICAS'=>'nombreFisica','JURIDICAS'=>'razonSocial','SINJURIDICAS'=>'nombreSinJuridica','FIDEOCOMISOS'=>'nombreFideocomiso');
	
			while($item=mysql_fetch_assoc($listado)){
				if($personas==''){
					$personas="<tr><td class='tdSeparacion' colspan='3'></td></tr>";
				}
				$partes=explode('_',$item['codigoPersona']);
				$persona=consultaBD('SELECT * FROM '.$tablas[$partes[1]].' WHERE codigo='.$partes[0],true,true);
				$personas.='<tr><td class="dato a100" colspan="3">'.$persona[$campos[$partes[1]]].'</td></tr>';
			}
			$contenido.=$personas."<tr>
								<td class='tdSeparacion' colspan='3'></td>
								</tr>";

		}
	
		if($datos['observaciones2']){
			$contenido.="<tr>
							<td class='tituloDato a100' colspan='3'>Observaciones</td>
						</tr>
						<tr>
							<td class='dato a100' colspan='3'>".$datos['observaciones2']."</td>
						</tr>";
		}
		$contenido.="</table>";
	}
	$hay=false;
	$arrayCircunstancias=array();
	foreach ($checkCircunstancias as $key => $value) {
		if($datos[$key]=='SI'){
			$hay=true;
			$arrayCircunstancias[$key]=$value;
		}
	}
	if($hay){
		$contenido.="<table class='tablaDatos'>
		<tr>
			<th class='tituloDato a100' colspan='2'>CIRCUNSTANCIAS</th>
		</tr>";
		foreach ($arrayCircunstancias as $key => $value) {
			$contenido.="<tr>
							<td class='dato a100' colspan='2'>".$value."</td>
						</tr>";
		}
		//if($datos['observaciones2']!=''){
			$contenido.="<tr>
							<td class='tituloDato a20'> Observaciones</td>
							<td class='tdCheck a80'>".$datos['observaciones2']."</td>
						</tr>";
		//			}
		$contenido.="</table>";
	}
	$medidas=array('NORMALES'=>'Medidas normales','SIMPLIFICADAS'=>'Medidas simplificadas','REFORZADAS'=>'Medidas reforzadas','EXAMEN'=>'Examen especial');
	$medidas2=array('NORMALES'=>'CLIENTE SUSCEPTIBLE DE APLICACIÓN DE MEDIDAS NORMALES DE DILIGENCIA DEBIDA, POR LO QUE HABRÁ QUE IDENTIFICAR Y COMPROBAR LA IDENTIDAD DE LA PERSONA QUE SOLICITE EL SERVICIO ASÍ COMO LA DEL TITULAR REAL, REGISTRAR LA ACTIVIDAD PROFESIONAL O EMPRESARIAL DECLARADA POR EL CLIENTE Y REALIZAR UN SEGUIMIENTO CONTINUO DE LA RELACIÓN DE NEGOCIOS MANTENIDA CON EL CLIENTE –SECCIÓN 1ª DEL CAPÍTULO II DEL RLPBC- ','SIMPLIFICADAS'=>'CLIENTE SUSCEPTIBLE DE APLICACIÓN DE MEDIDAS SIMPLIFICADAS DE DILIGENCIA DEBIDA -ART. 15 DEL RLPBC- PROCEDIENDO TAN SOLO A LA COMPROBACIÓN DE LA IDENTIDAD DE LAS PERSONAS FÍSICAS QUE NOS SOLICITAN EL SERVICIO Y LA ACREDITACIÓN DE LA REPRESENTACIÓN','REFORZADAS'=>'CLIENTE SUSCEPTIBLE DE APLICACIÓN DE MEDIDAS REFORZADAS DE DILIGENCIA DEBIDA, DEBIENDO IDENTIFICARSE AL CLIENTE MEDIANTE ALGUNO DE LOS MEDIOS ESTABLECIDOS EN EL ARTÍCULO 21 RLPBC','EXAMEN'=>'CLIENTE SUSCEPTIBLE DE SOMETIMIENTO A PROCESO INTEGRAL DE EXAMEN ESPECIAL, DEBIENDO ANALIZARSE TODA LA OPERATIVA RELACIONADA, TODOS LOS INTERVINIENTES EN LA OPERACIÓN Y TODA LA INFORMACIÓN RELEVANTE OBRANTE EN EL SUJETO OBLIGADO Y, EN SU CASO, EN EL GRUPO EMPRESARIAL – ART. 25 RLPBC');
	if($datos['medidas']==''){
		$datos['medidas']='NORMALES';
	} else {
		$datos['medidas']=explode('&$&', $datos['medidas']);
	}
	if(in_array('NORMALES',$datos['medidas'])){
		$comprobaciones=array(
			'checkDNI'=>'DNI/ PASAPORTE/ TARJETA DE RESIDENCIA/ TARJETA DE IDENTIDAD DE EXTRANJERO/ TARJETA OFICIAL DE IDENTIDAD PERSONAL EXPEDIDO POR LAS AUTORIDADES DE ORIGEN (ciudadanos de la Unión Europea)/ DOCUMENTO DE IDENTIDAD EXPEDIDO POR EL MINISTERIO DE ASUNTOS EXTERIORES Y DE COOPERACIÓN (personal de representaciones diplomáticas y consulares)/ Otro documento de identidad personal (excepcionalmente): Para comprobar la identidad real del cliente.',
			'checkEscritura1'=>'Escritura de la persona jurídica: Para conocer la estructura de propiedad o control, actividad principal, distribución del capital social, domicilio social, cuantía del capital social y forma de desembolso.',
			'checkPoderes'=>'Poderes: Para que quede acreditada la representación.',
			'checkDeclaracion1'=>'Declaración responsable del cliente ó de la persona que tenga atribuida la representación de la persona jurídica: Para identificar y comprobar la identidad del titular real de la persona jurídica.');
	} else if(in_array('SIMPLIFICADAS',$datos['medidas'])){
		$comprobaciones=array(
			'checkDNI'=>'DNI/ PASAPORTE/ TARJETA DE RESIDENCIA/ TARJETA DE IDENTIDAD DE EXTRANJERO/ TARJETA OFICIAL DE IDENTIDAD PERSONAL EXPEDIDO POR LAS AUTORIDADES DE ORIGEN (ciudadanos de la Unión Europea)/ DOCUMENTO DE IDENTIDAD EXPEDIDO POR EL MINISTERIO DE ASUNTOS EXTERIORES Y DE COOPERACIÓN (personal de representaciones diplomáticas y consulares)/ Otro documento de identidad personal (excepcionalmente): Para comprobar la identidad real del cliente.',
			'checkEscritura1'=>'Escritura de la persona jurídica: Para conocer la estructura de propiedad o control, actividad principal, distribución del capital social, domicilio social, cuantía del capital social y forma de desembolso.',
			'checkPoderes'=>'Poderes: Para que quede acreditada la representación.');
	} 
	if(in_array('REFORZADAS',$datos['medidas'])){
		if($datos['checkNoPresencial']=='NO'){
			$medidas2['REFORZADAS']='CLIENTE SUSCEPTIBLE DE APLICACIÓN DE MEDIDAS REFORZADAS DE DILIGENCIA DEBIDA, PROCEDIENDO EN TODO CASO LA COMPROBACIÓN DE LAS ACTIVIDADES DECLARADAS POR EL CLIENTE ASÍ COMO LA IDENTIDAD DEL TITULAR REAL - ARTÍCULO 20 RLPBC';
		}
		$comprobaciones=array(
			'checkDNI'=>'DNI/ PASAPORTE/ TARJETA DE RESIDENCIA/ TARJETA DE IDENTIDAD DE EXTRANJERO/ TARJETA OFICIAL DE IDENTIDAD PERSONAL EXPEDIDO POR LAS AUTORIDADES DE ORIGEN (ciudadanos de la Unión Europea)/ DOCUMENTO DE IDENTIDAD EXPEDIDO POR EL MINISTERIO DE ASUNTOS EXTERIORES Y DE COOPERACIÓN (personal de representaciones diplomáticas y consulares)/ Otro documento de identidad personal (excepcionalmente): Para comprobar la identidad real del cliente.',
			'checkEscritura1'=>'Escritura de la persona jurídica: Para conocer la estructura de propiedad o control, actividad principal, distribución del capital social, domicilio social, cuantía del capital social y forma de desembolso.',
			'checkPoderes'=>'Poderes: Para que quede acreditada la representación.',
			'checkDeclaracion1'=>'Declaración responsable del cliente ó de la persona que tenga atribuida la representación de la persona jurídica: Para identificar y comprobar la identidad del titular real de la persona jurídica.',
			'checkIdentificacion1'=>'Identificación del cliente no presencial: Firma electrónica; ó',
			'checkIdentificacion2'=>'Identificación del cliente no presencial: Primer ingreso procedente de una cuenta a nombre del mismo cliente abierta en una entidad domiciliada en España, en la Unión Europea o en países terceros equivalentes; ó',
			'checkIdentificacion3'=>'Identificación del cliente no presencial: Copia del documento de identidad, de los establecidos en el artículo 6 RLPBC, que corresponda, siempre que dicha copia esté expedida por un fedatario público.',
			'checkRenta'=>'Declaración de Renta: Para conocer los ingresos del cliente.',
			'checkReintegro'=>'Reintegro o ingreso en cuenta bancaria: Para conocer la entidad bancaria, el titular de la cuenta, origen (propio o préstamo) y destino de los fondos, así como su importe.',
			'checkAutorizacion1'=>'Autorización para el movimiento de dinero en España: Para acreditar el traslado de fondos dentro del territorio nacional.',
			'checkAutorizacion2'=>'Autorización para sacar dinero de España: Para acreditar los fondos que salen del territorio nacional.',
			'checkBienes'=>'Declaración de bienes: Para conocer los bienes del cliente.',
			'checkLibro'=>'Libro de familia: Para identificar a los familiares más próximos (cónyuge, padres e hijos).',
			'checkImpuesto'=>'Impuesto de sociedades: Para conocer los bienes, obligaciones y valor del patrimonio neto.');
	}
	if(in_array('EXAMEN',$datos['medidas'])){
		$comprobaciones=array(
			'checkDNI'=>'DNI/ PASAPORTE/ TARJETA DE RESIDENCIA/ TARJETA DE IDENTIDAD DE EXTRANJERO/ TARJETA OFICIAL DE IDENTIDAD PERSONAL EXPEDIDO POR LAS AUTORIDADES DE ORIGEN (ciudadanos de la Unión Europea)/ DOCUMENTO DE IDENTIDAD EXPEDIDO POR EL MINISTERIO DE ASUNTOS EXTERIORES Y DE COOPERACIÓN (personal de representaciones diplomáticas y consulares)/ Otro documento de identidad personal (excepcionalmente): Para comprobar la identidad real del cliente.',
			'checkEscritura1'=>'Escritura de la persona jurídica: Para conocer la estructura de propiedad o control, actividad principal, distribución del capital social, domicilio social, cuantía del capital social y forma de desembolso.',
			'checkPoderes'=>'Poderes: Para que quede acreditada la representación.',
			'checkDeclaracion1'=>'Declaración responsable del cliente ó de la persona que tenga atribuida la representación de la persona jurídica: Para identificar y comprobar la identidad del titular real de la persona jurídica.',
			'checkIdentificacion1'=>'Identificación del cliente no presencial: Firma electrónica; ó',
			'checkIdentificacion2'=>'Identificación del cliente no presencial: Primer ingreso procedente de una cuenta a nombre del mismo cliente abierta en una entidad domiciliada en España, en la Unión Europea o en países terceros equivalentes; ó',
			'checkIdentificacion3'=>'Identificación del cliente no presencial: Copia del documento de identidad, de los establecidos en el artículo 6 RLPBC, que corresponda, siempre que dicha copia esté expedida por un fedatario público.',
			'checkRenta'=>'Declaración de Renta: Para conocer los ingresos del cliente.',
			'checkReintegro'=>'Reintegro o ingreso en cuenta bancaria: Para conocer la entidad bancaria, el titular de la cuenta, origen (propio o préstamo) y destino de los fondos, así como su importe.',
			'checkAutorizacion1'=>'Autorización para el movimiento de dinero en España: Para acreditar el traslado de fondos dentro del territorio nacional.',
			'checkAutorizacion2'=>'Autorización para sacar dinero de España: Para acreditar los fondos que salen del territorio nacional.',
			'checkBienes'=>'Declaración de bienes: Para conocer los bienes del cliente.',
			'checkLibro'=>'Libro de familia: Para identificar a los familiares más próximos (cónyuge, padres e hijos).',
			'checkImpuesto'=>'Impuesto de sociedades: Para conocer los bienes, obligaciones y valor del patrimonio neto.',
			'checkCertificado1'=>'Certificado de vida laboral Para conocer las entidades, el régimen y periodos de tiempo en los que ha trabajado cotizando a la Seguridad Social.',
			'checkContrato1'=>'Contrato laboral: Para conocer el último puesto de trabajo, periodo en el que permaneció desarrollando la actividad y condiciones de contratación del cliente.',
			'checkCertificado2'=>'Certificado de empadronamiento: Para conocer la residencia actual del interviniente, tercero y/o titular real.',
			'checkCertificacion1'=>'Certificación diplomática de residencia en el extranjero: Para acreditar la residencia de españoles en el extranjero.',
			'checkCertificacion2'=>'Certificación literal de nacimiento: Para acreditar fecha y lugar de nacimiento e identidad de los padres.',
			'checkDeclaracion2'=>'Declaración censal: Para conocer el domicilio social y domicilio fiscal de la entidad.',
			'checkFacturas'=>'Facturas de consumos (teléfono, luz, agua, etc.) de personas jurídicas: Para conocer el domicilio social y volumen de la actividad en base a los consumos realizados.',
			'checkContrato2'=>'Contrato de alquiler: Para verificar el domicilio social donde se realiza la actividad y verificar su coincidencia con el domicilio fiscal de la entidad.',
			'checkDocumentoTC2'=>'Documento TC2: Para conocer el domicilio social y volumen de la entidad en base al número de empleados.',
			'checkDocumentoUnico'=>'Documento Único Administrativo (DUA/DAE): Para conocer los intercambios (importaciones y/o exportaciones) de mercancías entre miembros comunitarios y terceros países, pudiendo también comprobar la identidad de la persona que firma el documento.',
			'checkInforme'=>'Informe externo: Para conocer bienes patrimoniales, valor de adquisición y antigüedad, cargos societarios y otros datos de interés de la persona jurídica.',
			'checkNota'=>'Nota simple del Registro de la Propiedad: Para conocer el propietario de un determinado bien, transmisiones realizadas y posibles cargas que tenga.',
			'checkEscritura2'=>'Escritura del inmueble: Para acreditar la titularidad del inmueble y los derechos reales sobre el mismo.');
	}
	$textoMedidas1='';
	$textoMedidas2='';
	foreach ($datos['medidas'] as $key => $value) {
		if($textoMedidas1!=''){
			$textoMedidas1.='<br/>';
			$textoMedidas2.='<br/>';
		}
		$textoMedidas1.=$medidas[$value];
		$textoMedidas2.=$medidas2[$value];
	}

	$contenido.="<table class='tablaDatos'>
		<tr>
			<td class='tituloDato a40'>Medidas de diligencia debida aplicadas</td>
			<td class='dato a60'>".$textoMedidas1."</td>
		</tr>
		<tr>
			<td class='dato' style='padding-bottom:-25px;' colspan='2'>".$textoMedidas2."</td>
		</tr></table>";

	$contenido.="<table class='tablaDatos'>
		<tr>
			<th class='tituloDato' colspan='2'>COMPROBACIONES</th>
		</tr>";
		if(in_array('NORMALES',$datos['medidas'])){
			$listado=array('checkDNI','checkEscritura1','checkPoderes','checkDeclaracion1');
  		} else if(in_array('SIMPLIFICADAS',$datos['medidas'])){
    		$listado=array('checkDNI','checkEscritura1','checkPoderes');
  		} 
  		if(in_array('REFORZADAS',$datos['medidas'])){
    		$listado=array('checkDNI','checkEscritura1','checkPoderes','checkDeclaracion1','checkIdentificacion1','checkIdentificacion2','checkIdentificacion3','checkRenta','checkReintegro','checkAutorizacion1','checkAutorizacion2','checkBienes','checkLibro','checkImpuesto');
  		} 
  		if(in_array('EXAMEN',$datos['medidas'])){
    		$listado=array('checkDNI','checkEscritura1','checkPoderes','checkDeclaracion1','checkIdentificacion1','checkIdentificacion2','checkIdentificacion3','checkRenta','checkReintegro','checkAutorizacion1','checkAutorizacion2','checkBienes','checkLibro','checkImpuesto','checkCertificado1','checkContrato1','checkCertificado2','checkCertificacion1','checkCertificacion2','checkDeclaracion2','checkFacturas','checkContrato2','checkDocumentoTC2','checkDocumentoUnico','checkInforme','checkNota','checkEscritura2');
    	}
		$comp=consultaBD('SELECT * FROM comprobaciones_informes_clientes WHERE codigoInforme='.$datos['codigo'],true,true);
		foreach ($comprobaciones as $key => $value) {
			if(in_array($key, $listado)){
				$eliminada=consultaBD('SELECT * FROM comprobaciones_informes_clientes_eliminadas WHERE campo="'.$key.'" AND codigoInforme='.$datos['codigo'],true,true);
				if(!$eliminada){
					$clase=$comp[$key]=='SI'?'tdMarcado':'';
					$contenido.="<tr>
								<td class='dato a5 ".$clase."'> </td>
								<td class='dato a95'>".$value."</td>
							</tr>";
				}
			}
		}

		if($comp['otraDocumentacion']!=''){
			$contenido.="<tr>
							<td class='dato a5 tdMarcado'> </td>
							<td class='dato a95'>".$comp['otraDocumentacion']."</td>
						</tr>";
		}
		
	$contenido.="</table>";
	$resultados=array('SIN'=>'Sin finalizar, pendiente de comprobaciones','PRESTAR'=>'Prestar el servicio al no detectar indicios de blanqueo de capitales','EXAMEN'=>'Proceder a examen especial');
	$contenido.="<table class='tablaDatos'>
		<tr>
			<th class='tituloDato' colspan='2'>FINALIZACIÓN PRESTACIÓN SERVICIO</th>
		</tr>
		<tr>
			<td class='tituloDato a40'>Fecha finalización prestación servicio</td>
			<td class='dato a60'>".formateaFechaWeb($datos['fechaFinalizacion'])."</td>
		</tr>";
		$profesionales=consultaBD('SELECT * FROM profesionales_informes_clientes WHERE codigoInforme='.$datos['codigo'],true);
		$contenido.="<tr>
						<th class='tituloDato a100' colspan='2'>PROFESIONALES INTERVINIENTES</th>
					</tr>";
		while($profesional=mysql_fetch_assoc($profesionales)){
			$contenido.="<tr>
							<td class='dato a100' colspan='2'>".$profesional['profesional']."</td>
						</tr>";
		}
		$contenido.="<tr>
						<td class='tdSeparacion' colspan='2'></td>
					</tr>";
	$contenido.="<tr>
			<td class='tituloDato a40'>Resultado del analisis objeto del presente informe </td>
			<td class='dato a60'>".$resultados[$datos['resultado']]."</td>
		</tr>
		<tr>
			<td class='tituloDato a40'>El informe ha sido elaborado por</td>
			<td class='dato a60'>".$datos['elaborado']."</td>
		</tr>";
	$textoFinal='Documento firmado por los profesionales intervinientes en el presente Informe de Riesgo.';
	if(in_array('EXAMEN',$datos['medidas'])){
	$textoFinal='Documento firmado por los profesionales intervinientes en el presente Informe de Riesgo y por los profesionales que realizan el correspondiente examen especial.';
	$analisis=consultaBD('SELECT * FROM analisis_previo WHERE codigoCliente='.$cliente['codigo'].' ORDER BY fecha DESC',true,true);
	if($analisis){
		if($analisis['empleados']==50 || $analisis['volumen']==3){
			$texto='Concluido el análisis técnico, el órgano de control interno  adoptará, motivadamente y sin demora, la decisión sobre si procede o no la comunicación al SEPBLAC, en función de la concurrencia en la operativa de indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo. El órgano de control interno adoptará la decisión por mayoría, debiendo constar expresamente en el acta, el sentido y motivación del voto de cada uno de los miembros';
		} else {
			$texto='Concluido el análisis técnico, el representante ante el SEPBLAC adoptará, motivadamente y sin demora, la decisión sobre si procede o no la comunicación al SEPBLAC, en función de la concurrencia en la operativa de indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo';
		}
	}
	$motivaciones=array(1=>'Se aprecia justificación económica, profesional o de negocio para la realización de las operaciones, por lo que no concurren en la operativa indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo',2=>'No se aprecia justificación económica,  profesional o de negocio para la realización de las operaciones, concurriendo en la operativa  indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo');
	$motivacion=$motivaciones[$datos['conclusion']];
	if($datos['otraMotivacion']!=''){
		$motivacion.='<br/><br/>'.$datos['otraMotivacion'];
	}
	$fechaSEPBLAC='';
	if($datos['conclusion']==2){
		$fechaSEPBLAC="<tr>
			<td class='tituloDato a40'>Fecha comunicación al SEPBLAC</td>
			<td class='dato a60'>".formateaFechaWeb($datos['fechaSEPBLAC'])."</td>
		</tr>";
	}
	$decisiones=consultaBD('SELECT * FROM firmado_informes_clientes WHERE codigoInforme='.$datos['codigo'],true);
	$decisionTexto='';
	while($d=mysql_fetch_assoc($decisiones)){
		if($decisionTexto!=''){
			$decisionTexto.=', ';
		}
		$decisionTexto.=$d['firmado'];
	}
	$td='';
	if($datos['conclusion']==1){
		$examen='Procede la prestación del servicio solicitado al considerar que no concurren en la operativa indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo';
	} else {
		$examen='Procede la comunicación, sin dilación, al SEPBLAC  por considerar que concurren en la operativa indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo';
		if($datos['interrupcion']=='NO'){
			$td="<tr>
			<td class='tituloDato a40'>Motivos que justifican que no se interrumpa la relación de negocios</td>
			<td class='dato a60'>".$datos['motivosJustifican']."</td>
		</tr>";
		}
	}
	$contenido.="
		<tr>
			<th class='tituloDato a100' colspan='2'>EXAMEN ESPECIAL</th>
		</tr>
		<tr>
			<td class='dato a100' colspan='2'>Los sujetos obligados examinarán con especial atención cualquier hecho y operación, con independencia de su cuantía, que, por su naturaleza, pueda estar relacionado con el blanqueo de capitales o la financiación del terrorismo, reseñando por escrito los resultados del examen. En particular, los sujetos obligados examinarán con especial atención toda operación o pauta de comportamiento compleja, inusual o sin un propósito económico o lícito aparente, o que presente indicios de simulación o fraude –art. 17 LPBC-<br/><br/>".$texto."</td>
		</tr>
		<tr>
			<td class='tituloDato a40'>Motivación </td>
			<td class='dato a60'>".$motivacion."</td>
		</tr>
		<tr>
			<td class='tituloDato a40'>Resultado del examen especial </td>
			<td class='dato a60'>".$examen."</td>
		</tr>
		".$td."
		<tr>
			<td class='tituloDato a40'>Profesionales que realizan el examen especial </td>
			<td class='dato a60'>".$decisionTexto."</td>
		</tr>
		<tr>
			<td class='dato a100' colspan='2'>En el supuesto en que la detección de la operación derive de la comunicación interna de un empleado, agente o directivo de la entidad, la decisión final adoptada sobre si procede o no la comunicación por indicio de la operación, será puesta en conocimiento del comunicante</td>
		</tr>
		<tr>
			<td class='tituloDato a40'>Comunicante de la operación</td>
			<td class='dato a60'>".$datos['comunicante']."</td>
		</tr>
		<tr>
			<td class='tituloDato a40'>Fecha finalización examen especial</td>
			<td class='dato a60'>".formateaFechaWeb($datos['fechaDecision'])."</td>
		</tr>".$fechaSEPBLAC;
	}
	$contenido.="</table><br/><br/>".$textoFinal."<br/><br/>
	El presente informe se debe descargar, imprimir y firmar por las personas que adoptan la decisión, conservándolo durante 10 años junto a la documentación que lo fundamente";


	
$contenido.="	
	<page_footer>
		<div class='textoPie'>
			Documento generado por UVED Asesores Consultores
		</div>
	</page_footer>
</page>";

return $contenido;
}


?>
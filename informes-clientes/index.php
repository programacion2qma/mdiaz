<?php
  $seccionActiva=50;
  include_once('../cabecera.php');
  $_SESSION['espacio']='NO';
  operacionesInformes();
  $examen='NO';
  if(isset($_GET['filtro'])){
    $examen=$_GET['filtro'];
  }
  $codigoRenovacion='0';
  if(isset($_GET['codigoRenovacion'])){
    $codigoRenovacion=$_GET['codigoRenovacion'];
  }
  $titulo='Informes registrados';
  $tabla='tablaInformesClientes';
  if($examen=='EXAMEN'){
    $tabla='tablaInformesClientesExamen';
    $titulo='Registro de Informes de Examen Especial';
  }
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
          creaBotonesGestion();
          creaBotonEspecial($examen);
        ?>
		
      Referencias: N => No finalizado, M => Finalizado<br/><br/>
      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3><?php echo $titulo; ?></h3>
              <div class="pull-right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                //filtroServicios();
              ?>
              <table class="table table-striped table-bordered datatable" id="<?php echo $tabla;?>">
                <thead>
                  <tr>
                    <?php
                      campoOculto($examen,'examen');
                      campoOculto($codigoRenovacion,'codigoRenovacion');
                      if($_SESSION['tipoUsuario']=='ADMIN'){
                         echo '<th> Cliente </th>'; 
                      }
                    ?>
                    <th> Ref </th>
                    <th> Nº </th>
                    <th> Fecha </th>
                    <th> Solicitante </th>
                    <th> En representación de </th>
                    <?php if($examen=='RENOVACION'){?>
                      <th class="centro"></th>
                    <?php } else { ?>
                      <th> Finalizado </th>
                    <?php } ?>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->


<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    var examen=$('#examen').val();
    var codigoRenovacion=$('#codigoRenovacion').val();
    listadoTabla('#<?php echo $tabla;?>','../listadoAjax.php?include=informes-clientes&funcion=listadoTrabajos(\''+examen+'\');');
    if(examen=='RENOVACION'){
      if(codigoRenovacion==0){
        alert('Una vez decidido si los informes tienen o no indicios de blanqueo, pulse el botón Volver (botón azul con una flecha) para acceder de nuevo al listado de informes');
      }
      $('#btnVolver').unbind();
      $('#btnVolver').click(function(e){
        e.preventDefault();
        alert('Acceda ahora al botón Acciones para renovar los informes en los que haya aclarado si hay indicios o no blanqueo');
        window.location=$(this).attr('href');
      })
    }
      $('.enlacePopOver').each(function(){
          $(this).popover({//Crea el popover
            title: $(this).attr('title'),
            content: $(this).next().html().replace(/[\r\n|\n]/g,'<br />'),
            placement:$(this).attr('position'),
            thisrigger:'manual',
            trigger:'manual'
          });
      });
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
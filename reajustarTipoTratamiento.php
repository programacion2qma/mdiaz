<?php
	include_once('funciones.php');
	$tiposArray=array('Recogida'=>'checkTipoTratamiento1','Registro'=>'checkTipoTratamiento2','Estructuración'=>'checkTipoTratamiento3','Modificación'=>'checkTipoTratamiento4','Conservación'=>'checkTipoTratamiento5','Interconexión'=>'checkTipoTratamiento6','Consulta'=>'checkTipoTratamiento7','Cotejo'=>'checkTipoTratamiento8','Análisis de conducta'=>'checkTipoTratamiento9','Comunicación'=>'checkTipoTratamiento10','Supresión'=>'checkTipoTratamiento11','Destrucción'=>'checkTipoTratamiento12','Comunicación por transmisión al responsable del fichero'=>'checkTipoTratamiento13','Comunicación a la Administración Pública competente'=>'checkTipoTratamiento14','Comunicación permitida por ley'=>'checkTipoTratamiento15');
	$listado=consultaBD('SELECT * FROM declaraciones',true);
	while($item=mysql_fetch_assoc($listado)){
		echo 'Declaración: '.$item['codigo'].'<br/>';
		$tipos=explode(',',$item['tipoTratamiento']);
		$ultimo=count($tipos);
		$i=0;
		foreach ($tipos as $key => $value) {
			$i++;
			if($i==$ultimo){
				$tipos2=ultimo($value);
				if($tipos2>1){
					foreach ($tipos2 as $key2 => $value2) {
						echo '-- '.existe(sanear($value2),$tiposArray,$item['codigo']).'<br/>';
					}
				} else {
					echo '-- '.existe(sanear($value),$tiposArray,$item['codigo']).'<br/>';
				}
			} else {
				echo '-- '.existe(sanear($value),$tiposArray,$item['codigo']).'<br/>';
			}
		}
		echo '-------------------<br/>';
	}

	function sanear($texto){
		$texto=trim($texto);
		$texto=str_replace('.','',$texto);
		$texto=mb_strtoupper($texto,'UTF-8');
		return $texto;
	}

	function ultimo($value){
		$res=$value;
		$tipos2=explode(' o ',$value);
		if(count($tipos2)>1){
			$res=$tipos2;
		} else {
			$tipos2=explode(' O ',$value);
			if(count($tipos2)>1){
				$res=$tipos2;
			} else {
				$tipos2=explode(' y ',$value);
				if(count($tipos2)>1){
					$res=$tipos2;
				}
			}
		}
		return $res;
	}

	function existe($texto,$tipos,$codigo){
		$res=$texto;
		foreach ($tipos as $key => $value) {
			if($texto==mb_strtoupper($key,'UTF-8')){
				$res=$value;
				$res=consultaBD('UPDATE declaraciones SET '.$res.'="SI" WHERE codigo='.$codigo,true);
			}
		}
		return $res;
	}
?>
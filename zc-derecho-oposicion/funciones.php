<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesInteresados(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaInteresado();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaInteresado();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('derecho_oposicion');
	}

	mensajeResultado('nombre', $res, 'Interesado');
    mensajeResultado('elimina', $res, 'Interesado', true);
}

function insertaInteresado(){
	$res = true;
	responsable();
	$res = insertaDatos('derecho_oposicion');
	$codigo = $res;
	$res = $res && insertaCesionarios($codigo);
	return $res;
}

function actualizaInteresado(){
	$res = true;
	responsable();
	$res = actualizaDatos('derecho_oposicion');
	$res = $res && insertaCesionarios($_POST['codigo']);
	return $res;
}

function insertaCesionarios($codigo) {
	$res = true;
	$datos = arrayFormulario();
	$i = 0;

	conexionBD();
	$res = $res && consultaBD("DELETE FROM cesionarios WHERE codigoDerecho = ".$codigo.";");

	while (isset($datos['cesionario'.$i])) {
		if ($datos['cesionario'.$i] != '') {
			$res = $res && consultaBD("INSERT INTO cesionarios VALUES(NULL, ".$codigo.", '".$datos['cesionario'.$i]."');");
		}
		$i++;
	}
	cierraBD();
	return $res;
}

function responsable(){
	$_POST['responsable'] = '';
	$_POST['responsableDireccion'] = '';
	if($_POST['tipoRespuesta'] == 'supuesto3' && $_POST['selectResponsable'] != 'NULL') {
		$i = $_POST['selectResponsable'];
		$_POST['responsable'] = $_POST['responsable'.$i];
		$_POST['responsableDireccion'] = $_POST['direccion'.$i];
	}
}

function listadoInteresados(){
	global $_CONFIG;

	$columnas = array(
		'referencia',
		'nombre',
		'fechaRecep',
		'fechaRespu',
		'tipoRespuesta'
	);

	$respuestas = array(
		'' => '',
		'supuesto1' => 'La solicitud no reúne requisitos de identificación',
		'supuesto2' => 'No figuran datos personales del interesado',		
		'supuesto3' => 'Comunicación al Responsable de Tratamientos',
		'supuesto4' => 'Datos personales bloqueados',
		'supuesto5' => 'Denegación a la portabilidad de los datos ejercitada',
		'supuesto6' => 'Otorgamiento a la portabilidad de datos ejercitada',
	);	
	
	$cliente = obtenerCodigoCliente(true);
	$having  = obtieneWhereListado("HAVING codigoCliente=".$cliente, $columnas);
	$orden   = obtieneOrdenListado($columnas);
	$limite  = obtieneLimitesListado();
	
	$sql = "SELECT 
				*,
				DATE_FORMAT(fechaRecepcion, '%d/%m/%Y') AS fechaRecep,
				DATE_FORMAT(fechaRespuesta, '%d/%m/%Y') AS fechaRespu
			FROM
				derecho_oposicion
			$having";
	
	conexionBD();
	$consulta = consultaBD($sql." $orden $limite;");
	$consultaPaginacion=consultaBD($sql);
	cierraBD();

	$res = inicializaArrayListado($consulta, $consultaPaginacion);
	while($datos = mysql_fetch_assoc($consulta)){

		$fila=array(
			"<div class='centro'>".$datos['referencia']."</div>",
			$datos['nombre'],
			"<div class=centro>".$datos['fechaRecep']."</div>",
			"<div class=centro>".$datos['fechaRespu']."</div>",
			$respuestas[$datos['tipoRespuesta']],
			botonListado($datos),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function botonListado($datos) {

	$opciones = array('Detalles');
	$enlaces  = array("zc-derecho-oposicion/gestion.php?codigo=".$datos['codigo']);
	$iconos   = array('icon-search-plus');
	$clase    = array(0);

	if(isset($datos['tipoRespuesta']) && $datos['tipoRespuesta'] != '') {
		array_push($opciones, 'Descargar respuesta');
		array_push($enlaces, "zc-derecho-oposicion/generaRespuesta.php?codigo=".$datos['codigo']);
		array_push($iconos, 'icon-cloud-download');
		array_push($clase, 2);
	}

	if (count($opciones) > 1) {
		$res = botonAcciones($opciones, $enlaces, $iconos, $clase);
	} else {
		$res = creaBotonDetalles($enlaces[0]);
	}

	return $res;
}

function gestionInteresados() {

	$camposSelectNombres = array(
		'',
		'La solicitud no reúne requisitos de identificación -artículo 11 y 12.6 RGPD-',
		'No figuran datos personales del interesado',		
		'Comunicación al Responsable de Tratamientos',
		'Datos personales bloqueados',
		'Denegación de oposición -artículo 21 RGPD-',
		'Otorgamiento de oposición -artículo 21 RGPD-',		
	);
	$camposSelectvalores = array(
		'',
		'supuesto1',
		'supuesto2',
		'supuesto3',
		'supuesto4',
		'supuesto5',
		'supuesto6',
	);

	operacionesInteresados();

	abreVentanaGestion('Gestión de Interesados que ejercen el derecho de oposición -incluida elaboración de perfiles-','?','','icon-edit','margenAb');
	
		if(isset($_GET['codigo'])) {
			if(compruebaCliente($_GET['codigo'], 'derecho_oposicion')) {
				$datos = compruebaDatos('derecho_oposicion');
				$referencia = $datos['referencia'];
			}
		} else {
			$datos = false;
			$consulta = consultaBD("SELECT IFNULL(MAX(CAST(referencia AS DECIMAL)), 0) + 1 AS n FROM derecho_oposicion WHERE codigoCliente = ".obtenerCodigoCliente(true), true, true);
			$referencia = $consulta['n'];
		}
			
		abreColumnaCampos();
			campoOculto($datos, 'codigoCliente', obtenerCodigoCliente(true));
			campoTexto('referencia', 'Referencia', $referencia, 'input-mini');
			campoTexto('nombre', 'Nombre y apellidos del interesado', $datos, 'span7');
			campoTexto('nif', 'NIF del interesado', $datos, 'input-small');		
			campoTexto('nombre_representante', 'Representante legal', $datos, 'span7');
			campoTexto('nif_representante', 'NIF del representante legal', $datos, 'input-mini');
			campoTexto('direccion_notificaciones', 'Dirección a efecto de notificaciones', $datos, 'span7');
   			campoTexto('email', 'Correo electrónico', $datos, 'span7');
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoFecha('fechaRecepcion', 'Fecha de Recepción', $datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFecha('fechaRespuesta', 'Fecha de Respuesta', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoRadio('medioElectronico', 'La solicitud se ha presentado por medios electrónicos', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos();
			echo '<div id="divOtroMedio" class="hide">';
				campoRadio('otroMedioComunicacion','El interesado solicita que se le responda por otros medios diferentes al correo electrónico',$datos);
			echo '</div>';
		cierraColumnaCampos();

		abreColumnaCampos();
			echo '<div id="divOtroMedioTexto" class="hide">';
				campoTexto('otroMedioTexto','Indicar',$datos);
			echo '</div>';
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoSelect('tipoRespuesta', 'Respuesta', $camposSelectNombres, $camposSelectvalores, $datos, 'selectpicker span6 show-tick');
		cierraColumnaCampos(true);

		abreColumnaCampos('span3 divInformacion');
			
		cierraColumnaCampos(true);

		abreColumnaCampos('span3 divResponsable hide');
			// Esto viene heredado del desarrollador antiguo, los responsables no tienen tabla propia
			// y tenemos que recuperar los datos de los registros
			$responsables = array();
			$direcciones  = array();
			
			$provincias = array(
				''   => '',
				'01' => 'Álava',
				'02' => 'Albacete',
				'03' => 'Alicante',
				'04' => 'Almería',
				'05' => 'Ávila',
				'06' => 'Badajoz',
				'07' => 'Illes Balears',
				'08' => 'Barcelona',
				'09' => 'Burgos',
				'10' => 'Cáceres',
				'11' => 'Cádiz',
				'12' => 'Castellón de la Plana',
				'13' => 'Ciudad Real',
				'14' => 'Córdoba',
				'15' => 'A coruña',
				'16' => 'Cuenca',
				'17' => 'Girona',
				'18' => 'Granada',
				'19' => 'Guadalajara',
				'20' => 'Guipúzcoa',
				'21' => 'Huelva',
				'22' => 'Huesca',
				'23' => 'Jaén',
				'24' => 'León',
				'25' => 'Lleida',
				'26' => 'La Rioja',
				'27' => 'Lugo',
				'28' => 'Madrid',
				'29' => 'Málaga',
				'30' => 'Murcia',
				'31' => 'Navarra',
				'32' => 'Ourense',
				'33' => 'Asturias',
				'34' => 'Palencia',
				'35' => 'Las Palmas',
				'36' => 'Pontevedra',
				'37' => 'Salamanca',
				'38' => 'Santa Cruz de Tenerife',
				'39' => 'Cantabria',
				'40' => 'Segovia',
				'41' => 'Sevilla',
				'42' => 'Soria',
				'43' => 'Tarragona',
				'44' => 'Teruel',
				'45' => 'Toledo',
				'46' => 'Valencia',
				'47' => 'Valladolid',
				'48' => 'Vizcaya',
				'49' => 'Zamora',
				'50' => 'Zaragoza',
				'51' => 'Ceuta',
				'52' => 'Melilla'
			);

			conexionBD();
		    $declaraciones = consultaBD('SELECT * FROM declaraciones WHERE codigoCliente = '.obtenerCodigoCliente(false));
			while ($d = mysql_fetch_assoc($declaraciones)){
				if (!in_array($d['n_razon'], $responsables)) {
					array_push($responsables, $d['n_razon']);
					array_push($direcciones, $d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$provincias[$d['provincia_responsableFichero']]);
				}

				$otros = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion = '.$d['codigo']);
				while($o = mysql_fetch_assoc($otros)){
					if ($o['razonSocial'] != '' && !in_array($o['razonSocial'], $responsables)) {
						array_push($responsables, $o['razonSocial']);
						array_push($direcciones, $o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$provincias[$o['provincia']]);
					}
				}
			}
			cierraBD();
			
			$nombres = array('');
			$valores = array('NULL');
			foreach ($responsables as $key => $value) {
				campoOculto($value, 'responsable'.$key);
				array_push($nombres, $value);
				array_push($valores, $key);
			}
			foreach ($direcciones as $key => $value) {
				campoOculto($value, 'direccion'.$key);
			}
			if($datos && $datos['responsable'] != ''){
				campoDato('Responsable de tratamiento', $datos['responsable']);
			}
			campoSelect('selectResponsable','Selecciona responsable',$nombres,$valores);
		cierraColumnaCampos(true);
		
		abreColumnaCampos();
			campoRadio('mercadotecnia', '¿El tratamiento de los datos personales tiene por objeto la mercadotecnia directa?', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos('span9 divInformacion2 hide');
			echo '	
				<p style="margin-left:85px">
					INFORMACIÓN DE INTERÉS: El responsable podrá conservar los datos identificativos del 
					afectado necesarios con el fin de impedir tratamientos futuros para fines de 
					mercadotecnia directa <a id="articulo15" class="btnModal noAjax">- Artículo 15.2 LOPD-GDD -</a>
				</p>		
			';
		cierraColumnaCampos(true);

		abreColumnaCampos('datosSupuesto6');
			campoRadio('derivaSupresion', 'Del ejercicio del derecho de oposición ejercitado, ¿deriva la supresión de los datos?', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos('datosSupuesto6');
			campoRadio('cedenSupresion', 'Previamente a la solicitud de oposición, ¿se cedieron los datos objeto de la supresión?', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos('span3');
			echo '<a id="articulo19" style="margin-left:85px" class="btnModal noAjax">Artículo 19 RGPD</a>';
		cierraColumnaCampos(true);

		tablaCesionarios($datos);

		abreColumnaCampos();
			areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 
		cierraColumnaCampos(true);
		
		abreVentanaModal('información');
			echo "<div id='textoModal'>";
			echo "</div>";
		cierraVentanaModal('','','',false,'Cerrar');				

	cierraVentanaGestion('index.php',true);
}

function textoModalAJAX() {
	$opcion = $_POST['opcion'];
	$texto  = '';

	switch ($opcion) {
		case 'articulo11':
			$texto = "
				<i><b>Artículo 11 RGPD.</b> Tratamiento que no requiere identificación.</i><br>
				<ol>
					<li><i>Si los fines para los cuales un responsable trata datos personales no requieren o ya no requieren la identificación de un interesado por el responsable, este no estará obligado a mantener, obtener o tratar información adicional con vistas a identificar al interesado con la única finalidad de cumplir el presente Reglamento.</i></li>
					<li><i>Cuando, en los casos a que se refiere el apartado 1 del presente artículo, el responsable sea capaz de demostrar que no está en condiciones de identificar al interesado, le informará en consecuencia, de ser posible. En tales casos no se aplicarán los artículos 15 a 20, excepto cuando el interesado, a efectos del ejercicio de sus derechos en virtud de dichos artículos, facilite información adicional que permita su identificación.</i></<li>			
				</ol>
			";
			break;
		case 'articulo12':
			$texto = "
				<i><b>Artículo 12.6 RGPD.</b> 6. Sin perjuicio de lo dispuesto en el artículo 11, cuando el responsable del tratamiento tenga dudas razonables en relación con la identidad de la persona física que cursa la solicitud a que se refieren los artículos 15 a 21, podrá solicitar que se facilite la información adicional necesaria para confirmar la identidad del interesado.</i>
			";
			break;
		case 'articulo21':
			$texto = "
				<i><b>Artículo 21 RGPD.</b> Derecho de oposición.</i><br>
				<ol>
					<li>
						<i>El interesado tendrá derecho a oponerse en cualquier momento, por motivos 
						relacionados con su situación particular, a que datos personales que le conciernan sean objeto 
						de un tratamiento basado en lo dispuesto en el artículo 6, apartado 1, letras e) ó f), incluida la 
						elaboración de perfiles sobre la base de dichas disposiciones. El responsable del tratamiento 
						dejará de tratar los datos personales, salvo que acredite motivos legítimos imperiosos para el 
						tratamiento que prevalezcan sobre los intereses, los derechos y las libertades del interesado, o 
						para la formulación, el ejercicio o la defensa de reclamaciones.</i>
					</li>
					<li>
						<i>Cuando el tratamiento de datos personales tenga por objeto la mercadotecnia directa, el 
						interesado tendrá derecho a oponerse en todo momento al tratamiento de los datos personales 
						que le conciernan, incluida la elaboración de perfiles en la medida en que esté relacionada con 
						la citada mercadotecnia.</i>
					</li>
					<li>
						<i>Cuando el interesado se oponga al tratamiento con fines de mercadotecnia directa, los 
						datos personales dejarán de ser tratados para dichos fines.</i>
					</li>
					<li>
						<i>A más tardar en el momento de la primera comunicación con el interesado, el derecho 
						indicado en los apartados 1 y 2 será mencionado explícitamente al interesado y será 
						presentado claramente y al margen de cualquier otra información.</i>
					</li>
					<li>
						<i>En el contexto de la utilización de servicios de la sociedad de la información, y no 
						obstante lo dispuesto en la Directiva 2002/58/CE, el interesado podrá ejercer su derecho a 
						oponerse por medios automatizados que apliquen especificaciones técnicas.</i>
					</li>
					<li>
						<i>Cuando los datos personales se traten con fines de investigación científica o histórica o 
						fines estadísticos de conformidad con el artículo 89, apartado 1, el interesado tendrá derecho, 
						por motivos relacionados con su situación particular, a oponerse al tratamiento de datos 
						personales que le conciernan, salvo que sea necesario para el cumplimiento de una misión 
						realizada por razones de interés público.</i>
					</li>
				</ol>
			";
			break;
		case 'articulo22':
			$texto = "
				<i><b>Artículo 22  RGPD.</b> Decisiones individuales automatizadas, incluida la elaboración de perfiles.</i>
				<ol>
					<li><i>Todo interesado tendrá derecho a no ser objeto de una decisión basada únicamente en el tratamiento automatizado, incluida la elaboración de perfiles, que produzca efectos jurídicos en él o le afecte significativamente de modo similar.</i></li>
			 		<li><i>El apartado 1 no se aplicará si la decisión:</i></li> 
					<ol style='list-style-type:lower-latin;'>
						<li><i>es necesaria para la celebración o la ejecución de un contrato entre el interesado y un responsable del tratamiento;</i></li>
			 			<li><i>está autorizada por el Derecho de la Unión o de los Estados miembros que se aplique al responsable del tratamiento y que establezca asimismo medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado, o</i></li>
			 			<li><i>se basa en el consentimiento explícito del interesado.</i></li>
					</ol>
			 		<li><i>En los casos a que se refiere el apartado 2, letras a) y c), el responsable del tratamiento adoptará las medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado, como mínimo el derecho a obtener intervención humana por parte del responsable, a expresar su punto de vista y a impugnar la decisión. </i></li>
					<li><i>Las decisiones a que se refiere el apartado 2 no se basarán en las categorías especiales de datos personales contempladas en el artículo 9, apartado 1, salvo que se aplique el artículo 9, apartado 2, letra a) o g), y se hayan tomado medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado.</i></li>
				<ol>
			";
			break;
		case 'articulo15':
			$texto = "
				<i><b>Artículo 15.2 LOPD-GDD.</b> Cuando la supresión derive del ejercicio del derecho de oposición con arreglo al artículo 21.2 
				del Reglamento (UE) 2016/679, el responsable podrá conservar los datos identificativos del afectado necesarios con el fin de impedir 
				tratamientos futuros para fines de mercadotecnia directa.</i>
			";
			break;
		case 'articulo19':
			$texto = "
				<i><b>Artículo 19 RGPD. Obligación de notificación relativa a la rectificación o supresión de 
				datos personales o la limitación del tratamiento.</b> El responsable del tratamiento 
				comunicará cualquier rectificación o supresión de datos personales o limitación del 
				tratamiento efectuada con arreglo al artículo 16, al artículo 17, apartado 1, y al artículo 18 
				a cada uno de los destinatarios a los que se hayan comunicado los datos personales, salvo 
				que sea imposible o exija un esfuerzo desproporcionado. El responsable informará al 
				interesado acerca de dichos destinatarios, si este así lo solicita.</i>
			";
			break;
		default:
			break;	
	}

	echo $texto;

}

function valoresSelectResponsables() {
	$responsables = array();
	$direcciones  = array();
	
	conexionBD();
	$declaraciones = consultaBD('SELECT * FROM declaraciones WHERE codigoCliente = '.obtenerCodigoCliente(false));
	while ($d = mysql_fetch_assoc($declaraciones)){
		if(!in_array($d['n_razon'], $responsables)){
			array_push($responsables, $d['n_razon']);
			array_push($direcciones, $d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$d['provincia_responsableFichero']);
		}
		$otros = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion = '.$d['codigo']);
		while($o = mysql_fetch_assoc($otros)){
			if($o['razonSocial'] != '' && !in_array($o['razonSocial'], $responsables)){
				array_push($responsables, $o['razonSocial']);
				array_push($direcciones, $o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$o['provincia']);
			}
		}
	}
	cierraBD();
		
	return $responsables;
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function tablaCesionarios($datos) {
    echo"
        <center>
	 		<div class='table-responsive'>
				<table class='table hide' id='tablaCesionarios'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
							<th> Cesionario </th>
							<th></th>
                    	</tr>
                  	</thead>
                  	<tbody>";

    $i = 0;
    $j = 1;

	if($datos){

		$sql = "SELECT * FROM cesionarios WHERE codigoDerecho = ".$datos['codigo'].";";
		$cesionarios = consultaBD($sql, true);
		
		while($cesionario = mysql_fetch_assoc($cesionarios)){
		 	echo "
				<tr>
			";
				campoTextoTabla('cesionario'.$i, $cesionario['nombre'], 'span9');		
			echo "
					<td style='text-align:center;'>
						<input class='prod' type='checkbox' name='filasTabla[]' value=$j>
		        	</td>
				</tr>
			";
			
			$i++;
			$j++;
		}
	}

	if($i==0 || !$datos){
		echo "
			<tr>
		";

		campoTextoTabla('cesionario0', '', 'span9');
		
		echo "
				<td style='text-align:center;'>
					<input class='prod' type='checkbox' name='filasTabla[]' value=$j>
				</td>
			</tr>
		";
	}

	echo "
					</tbody>
				</table>
			</div>
		</center>
		<br>
		<center>
			<div class='hide' id='botonesTabla'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCesionarios\");'><i class='icon-plus'></i> Añadir</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCesionarios\");'><i class='icon-minus'></i> Eliminar</button> 
			</div>
		</center>
		<br>
	";
}

//Fin parte de agrupaciones
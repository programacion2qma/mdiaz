<?php
  $seccionActiva = 59;
  include_once('../cabecera.php');
  
  operacionesInteresados();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
          creaBotonesGestion();
          creaBotonDescarga('generaListado.php');          
        ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Interesados que ejercen su derecho de oposición -incluida la elaboración de perfiles</h3>
              <div class="pull-right">
                <!--<button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>-->
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                //filtroUsuarios();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaAplicaciones">
                <thead>
                  <tr>
                    <th>REF.</th>
                    <th>Interesado</th>
                    <th>Fecha de Recepción</th>
                    <th>Fecha de Respuesta</th>
                    <th>Respuesta</th>
                    <th class='centro'></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaAplicaciones','../listadoAjax.php?include=zc-derecho-oposicion&funcion=listadoInteresados();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaUsuarios');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
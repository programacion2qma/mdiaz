<?php
  $seccionActiva = 59;
  include_once("../cabecera.php");
  gestionInteresados();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

  tipoRespuesta($('#tipoRespuesta').val());
  $('#tipoRespuesta').on('change', function(e) {
    var opcion = $(this).val();
    
    tipoRespuesta(opcion);
  });

  $('.btnModal').unbind();
	$(document).on('click', '.btnModal', function(e){
    var consulta = $.post('../listadoAjax.php?include=zc-derecho-oposicion&funcion=textoModalAJAX();',{'opcion':$(this).attr('id')}
      ,function(respuesta){
        $('#textoModal').empty();
        $('#textoModal').append(respuesta);

        $('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
    });
  });

  medioElectronico($('input[name=medioElectronico]:checked').val());
  $('input[name=medioElectronico]').on('change', function(e) {
    var opcion = $('input[name=medioElectronico]:checked').val();
    medioElectronico(opcion);    
  });

  otroMedio($('input[name=otroMedioComunicacion]:checked').val());
  $('input[name=otroMedioComunicacion]').on('change', function(e) {
    var opcion = $('input[name=otroMedioComunicacion]:checked').val();
    otroMedio(opcion);    
  });

  mercadotecnia($('input[name=mercadotecnia]:checked').val());
  $('input[name=mercadotecnia]').on('change', function(e) {
    var opcion = $('input[name=mercadotecnia]:checked').val();
    mercadotecnia(opcion);    
  });

  // cedenSupresion($('input[name=cedenSupresion]:checked').val());
  $('input[name=cedenSupresion]').on('change', function(e) {
    var opcion = $('input[name=cedenSupresion]:checked').val();
    cedenSupresion(opcion);    
  });
});

function tipoRespuesta(opcion) {
  var enlace = '';
    
  $('.divInformacion').empty();

  switch (opcion) {
    case 'supuesto1':
      $('.divResponsable').addClass('hide');
      $('.datosSupuesto6').addClass('hide');
      cedenSupresion('NO');
      enlace = '<a id="articulo11" style="margin-left:85px" class="btnModal noAjax">Artículo 11 RGPD</a><br><a id="articulo12" style="margin-left:85px" class="btnModal noAjax">Artículo 12.6 RGPD</a>';
      break;
    case 'supuesto3':
      $('.divResponsable').removeClass('hide');
      $('.datosSupuesto6').addClass('hide');
      cedenSupresion('NO');
      break;
    case 'supuesto5':
      $('.divResponsable').addClass('hide');
      $('.datosSupuesto6').addClass('hide');
      cedenSupresion('NO');
      enlace = '<a id="articulo21" style="margin-left:85px" class="btnModal noAjax">Artículo 21 RGPD</a>';
      break;
    case 'supuesto6':
      $('.divResponsable').addClass('hide');
      $('.datosSupuesto6').removeClass('hide');
      cedenSupresion('NO');
      enlace = '<a id="articulo21" style="margin-left:85px" class="btnModal noAjax">Artículo 21 RGPD</a>';
      break;
    default:
      $('.divResponsable').addClass('hide');
      $('.datosSupuesto6').addClass('hide');
      cedenSupresion('NO');
      break;
  }
  
  $('.divInformacion').append(enlace);

}

function medioElectronico(opcion) {
  if (opcion == 'SI') {
      $('#divOtroMedio').removeClass('hide');
    } else {
      $('#divOtroMedio').addClass('hide');
      $('input[name=otroMedioComunicacion][value="NO"]').attr('checked', true);
      $('#divOtroMedioTexto').addClass('hide');
      $('#otroMedioTexto').val('');
    }
}

function otroMedio(opcion) {
  if (opcion == 'SI') {
      $('#divOtroMedioTexto').removeClass('hide');
    } else {
      $('#divOtroMedioTexto').addClass('hide');
      $('#divOtroMedioTexto').val('');
      $('input[name=otroMedioComunicacion][value="NO"]').attr('checked', true);
    }
}

function mercadotecnia(opcion) {
  if (opcion == 'SI') {
      $('.divInformacion2').removeClass('hide');
    } else {
      $('.divInformacion2').addClass('hide');
    }
}

function cedenSupresion(opcion) {
  if (opcion == 'SI') {
      $('#tablaCesionarios').removeClass('hide');
      $('#botonesTabla').removeClass('hide');
    } else {
      $('#tablaCesionarios').addClass('hide');
      $('#botonesTabla').addClass('hide');      
      $('input[name=cedenSupresion][value="NO"]').attr('checked',true);
    }
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
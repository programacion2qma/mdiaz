<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de empleados

function operacionesGestiones(){
	$res=true;

	if(isset($_POST['codigo'])){
    	$res=actualizaGestion();
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatosConFicheroAPI('gestion_evaluacion_general','documentacion','../documentos/riesgos');
	}

	mensajeResultado('fecha',$res,'Gestión');
    mensajeResultado('elimina',$res,'Gestión', true);
}

function actualizaGestion(){
	$res=true;
	if(isset($_FILES['documentacion'])){
		$_POST['documentacion']=subeDocumento('documentacion',time(),'../documentos/riesgos');
	}
	$res=actualizaDatos('gestion_evaluacion_general');
	$res=$res && insertaMedidasGestionRiesgo(); 
	return $res;
}

function insertaMedidasGestionRiesgo(){
	$res=true;
	$datos=arrayFormulario();

	$res=consultaBD("DELETE FROM medidas_riesgos_evaluacion_general WHERE codigoGestion=".$datos['codigo']);

	for($i=0;isset($datos['recomendacion'.$i]);$i++){
		if(isset($datos['ejecutada'.$i])){
			$ejecutada = 'SI';
		} else {
			$ejecutada = 'NO';
		}
		/*$area='';
		if(isset($datos['area'.$i])){
        	$select = $datos['area'.$i];
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$area .= "&$&";
					}
					$area .= $select[$j];
				}
			}
        }*/
        $responsable='';
		if(isset($datos['responsable'.$i])){
        	$select = $datos['responsable'.$i];
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$responsable .= "&$&";
					}
					$responsable .= $select[$j];
				}
			}
        }

		$res=$res && consultaBD("INSERT INTO medidas_riesgos_evaluacion_general VALUES(NULL, '".$responsable."', '".$datos['recomendacion'.$i]."','".$datos['plazo'.$i]."','".$datos['otroPlazo'.$i]."','".$ejecutada."',".$datos['codigo'].");");
	}

	return $res;
}

function imprimeGestiones($codigoEvaluacion,$codigoCliente=false){
	global $_CONFIG;

	$codigoS=$_SESSION['codigoU'];
	$where='';

	if($codigoCliente!=false){
		$where="WHERE e.codigoCliente=".$codigoCliente;

		if($codigoEvaluacion>0){
			$where="AND r.codigoEvaluacion=".$codigoEvaluacion;
		}

		$consulta=consultaBD("SELECT g.codigo, g.fecha AS fechaGestion, e.fechaEvaluacion, e.codigo AS codigoEvaluacion, r2.nombre AS nombreRiesgo, (SELECT COUNT(codigo) FROM medidas_riesgos_evaluacion_general m WHERE m.codigoGestion=g.codigo) AS medidas , (SELECT COUNT(codigo) FROM medidas_riesgos_evaluacion_general m WHERE m.codigoGestion=g.codigo AND checkEjecutada='SI') AS medidasEjecutadas 
			FROM gestion_evaluacion_general g 
			INNER JOIN riesgos_evaluacion_general r ON g.codigoEvaluacion=r.codigo 
			INNER JOIN evaluacion_general e ON r.codigoEvaluacion=e.codigo
			INNER JOIN riesgos r2 ON r.codigoRiesgo=r2.codigo
			".$where, true);

		while($datos=mysql_fetch_assoc($consulta)){
			$enlace="gestion.php?codigo=".$datos['codigo'];
			if($codigoEvaluacion>0){
				$enlace.="&codigoEvaluacionGeneral=".$datos['codigoEvaluacion'];
			}
			echo "
			<tr>
				<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
				<td>".formateaFechaWeb($datos['fechaGestion'])."</td>
				<td>".$datos['nombreRiesgo']."</td>
				<td class='centro'>".$datos['medidas']." / ".$datos['medidasEjecutadas']."</td>
	        	<td class='td-actions'>
	        		<a href='".$enlace."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
	        	</td>
	        	<td>
	                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	            </td>
	    	</tr>";
		}		
	}else{
		if($codigoEvaluacion>0){
			$where="WHERE r.codigoEvaluacion=".$codigoEvaluacion;
		}

		$consulta=consultaBD("SELECT g.codigo, g.fecha AS fechaGestion, e.fechaEvaluacion, e.codigo AS codigoEvaluacion, r2.nombre AS nombreRiesgo, (SELECT COUNT(codigo) FROM medidas_riesgos_evaluacion_general m WHERE m.codigoGestion=g.codigo) AS medidas , (SELECT COUNT(codigo) FROM medidas_riesgos_evaluacion_general m WHERE m.codigoGestion=g.codigo AND checkEjecutada='SI') AS medidasEjecutadas 
			FROM gestion_evaluacion_general g 
			INNER JOIN riesgos_evaluacion_general r ON g.codigoEvaluacion=r.codigo 
			INNER JOIN evaluacion_general e ON r.codigoEvaluacion=e.codigo
			INNER JOIN riesgos r2 ON r.codigoRiesgo=r2.codigo
			".$where, true);

		while($datos=mysql_fetch_assoc($consulta)){
			$enlace="gestion.php?codigo=".$datos['codigo'];
			if($codigoEvaluacion>0){
				$enlace.="&codigoEvaluacionGeneral=".$datos['codigoEvaluacion'];
			}
			echo "
			<tr>
				<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
				<td>".formateaFechaWeb($datos['fechaGestion'])."</td>
				<td>".$datos['nombreRiesgo']."</td>
				<td class='centro'>".$datos['medidas']." / ".$datos['medidasEjecutadas']."</td>
	        	<td class='td-actions'>
	        		<a href='".$enlace."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
	        	</td>
	        	<td>
	                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	            </td>
	    	</tr>";
		}
	}

}

function gestionGestiones(){
	operacionesGestiones();

	abreVentanaGestion('Gestiónes de evaluaciones de riesgos','?','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('gestion_evaluacion_general');
	/*if($datos){
		campoOculto(recogerPermiso(44,'checkModificar'),'permisoModificar');
	}*/
	$enlace='index.php';
	if(isset($_GET['codigoEvaluacionGeneral'])){
		campoOculto($_GET['codigoEvaluacionGeneral'],'codigoEvaluacionGeneral');
		$enlace.='?codigoEvaluacion='.$_GET['codigoEvaluacionGeneral'];
	}
	campoFecha('fecha','Fecha de Gestión',$datos);
	creaTablaRecomendacionesRiesgo($datos);
	campoFichero('documentacion','Subir documentación',0,$datos,'../documentos/riesgos/','Descargar');

	cierraVentanaGestion($enlace);
}

function creaTablaRecomendacionesRiesgo($datos=false){
	echo "
	<br />
	<h3 class='apartadoFormulario'>Medidas a implantar</h3>
	<table class='table table-striped table-bordered' class='tablaMedidas' id='tablaMedidas'>
      <thead>
        <tr>
          <th> Responsable</th>
          <th> Recomendación </th>
          <th> Plazo </th>
          <th> Ejecutada </th>
          <th> </th>
        </tr>
      </thead>
      <tbody>";
  	
  		$i=0;

  		conexionBD();
  		if($datos!=false){
  			$consulta=consultaBD("SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='".$datos['codigo']."'",true);
  			while($datosG=mysql_fetch_assoc($consulta)){
				imprimeCamposTablaRecomendacionesRiesgo($i,$datosG);
				$i++;
  			}
  		}
  		
  		if($i==0){
  			imprimeCamposTablaRecomendacionesRiesgo();
  		}
  		cierraBD();

    echo "
      </tbody>
    </table>
    <center>
		<button type='button' id='anadirMedida' class='btn btn-small btn-success' onclick='insertaFila2(\"tablaMedidas\");'><i class='icon-plus'></i> Añadir medida</button> 
		<button type='button' id='eliminarrMedida' class='btn btn-small btn-danger' onclick='eliminaFila2(\"tablaMedidas\");'><i class='icon-trash'></i> Eliminar medida</button>
	</center><br />";
}

function imprimeCamposTablaRecomendacionesRiesgo($i,$datos=false){
	$j=$i+1;
	$textos=array();
	$valores=array();
	$z=0;
	$textos[$z] = '';
	$valores[$z] = 'NULL';
	$consulta=consultaBD("SELECT codigo, funcion FROM puestosTrabajo ORDER BY funcion",true);
	while($datosPuestos=mysql_fetch_assoc($consulta)){
		$textos[$z] = $datosPuestos['funcion'];
		$valores[$z] = $datosPuestos['codigo'];
		$z++;
 	}
 	$textos1=array();
	$valores1=array();
	$z=0;
	$textos1[$z] = '';
	$valores1[$z] = 'NULL';
 	$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellidos) AS nombrecompleto FROM personal ORDER BY nombre",true);
	while($datosPersona=mysql_fetch_assoc($consulta)){
		$textos1[$z] = $datosPersona['nombrecompleto'];
		$valores1[$z] = $datosPersona['codigo'];
		$z++;
 	};
	echo "<tr class='trMedida'>
			<td>
				<table class='centro sinBorde'>
					<tr>";
						campoTextoTabla('responsable'.$i,$datos['responsable']);
						//campoSelectMultiple('area'.$i,'',$textos,$valores,$datos['area'],'selectpicker span3 show-tick',"data-live-search='true'",1);
	echo "			</tr>
					<tr>";
						//campoSelectMultiple('responsable'.$i,'',$textos1,$valores1,$datos['responsable'],'selectpicker span3 show-tick',"data-live-search='true'",1);
	echo "			</tr>
				</table>
			</td>";
	areaTextoTabla('recomendacion'.$i,$datos['recomendacion'],'areaTextoTablaMedidas');
	echo "<td>
				<table class='centro sinBorde'>
					<tr>";
						campoSelect('plazo'.$i,'',array('6 Meses','12 meses','18 meses','Otros'),array(6,12,18,1),$datos['plazo'],'selectpicker span2 show-tick selectPlazo','',1);
	echo "			</tr>
					<tr>";
	$class = $datos && $datos['plazo'] == 1 ? 'input-small textOtroPlazo':'input-small hidden';
						campoTextoTabla('otroPlazo'.$i,$datos['otroPlazo'],$class);
	echo "			</tr>
				</table>
			</td>";
	campoCheckTabla('ejecutada'.$i,$datos['checkEjecutada']);
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

function estadisticasGestiones($codigoEvaluacion,$codigoCliente=false){
	$res=array();
	$where='';

	if($codigoCliente!=false){

		$where="WHERE e.codigoCliente=".$codigoCliente;

		if($codigoEvaluacion>0){
			$where='AND r.codigoEvaluacion='.$codigoEvaluacion;
		}
		$consulta=consultaBD("SELECT COUNT(g.codigo) AS total 
			FROM gestion_evaluacion_general g
			INNER JOIN riesgos_evaluacion_general r ON g.codigoEvaluacion=r.codigo
			INNER JOIN evaluacion_general e ON r.codigoEvaluacion=e.codigo
			".$where,true,true);
		$res['total']=$consulta['total'];

		$consulta=consultaBD("SELECT g.codigo
			FROM gestion_evaluacion_general g
			INNER JOIN riesgos_evaluacion_general r ON g.codigoEvaluacion=r.codigo
			INNER JOIN evaluacion_general e ON r.codigoEvaluacion=e.codigo
			".$where,true);
		$res['finalizadas']=0;
		while($gestion=mysql_fetch_assoc($consulta)){
			$finalizada=true;
			$medidas=consultaBD('SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='.$gestion['codigo'],true);
			while($medida=mysql_fetch_assoc($medidas)){
				if($medida['checkEjecutada']=='NO'){
					$finalizada=false;
				}
			}
			if($finalizada){
				$res['finalizadas']++;
			}
		}
	}else{
		if($codigoEvaluacion>0){
			$where='WHERE r.codigoEvaluacion='.$codigoEvaluacion;
		}
		$consulta=consultaBD("SELECT COUNT(g.codigo) AS total 
			FROM gestion_evaluacion_general g
			INNER JOIN riesgos_evaluacion_general r ON g.codigoEvaluacion=r.codigo
			INNER JOIN evaluacion_general e ON r.codigoEvaluacion=e.codigo
			".$where,true,true);
		$res['total']=$consulta['total'];

		$consulta=consultaBD("SELECT g.codigo
			FROM gestion_evaluacion_general g
			INNER JOIN riesgos_evaluacion_general r ON g.codigoEvaluacion=r.codigo
			INNER JOIN evaluacion_general e ON r.codigoEvaluacion=e.codigo
			".$where,true);
		$res['finalizadas']=0;
		while($gestion=mysql_fetch_assoc($consulta)){
			$finalizada=true;
			$medidas=consultaBD('SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='.$gestion['codigo'],true);
			while($medida=mysql_fetch_assoc($medidas)){
				if($medida['checkEjecutada']=='NO'){
					$finalizada=false;
				}
			}
			if($finalizada){
				$res['finalizadas']++;
			}
		}
	}

	return $res;
}

//Fin parte de empleados



<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionGestiones();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="../js/funciones25.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.selectpicker').selectpicker();

		$('.selectPlazo').change(function(){
        	oyentePlazos($(this));
    	});
	});

function oyentePlazos(elem){
    var valor = elem.val();
    var id = elem.attr('id');
    id = "#otroP"+id.substring(1,id.length);
    if(valor == 1){
        $(id).removeClass('hidden');
    } else {
        $(id).addClass('hidden');
    }
}

function insertaFila2(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody .trMedida:last").clone();
    //Obtengo el atributo name para los inputs y selects
   $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
  		return parts[1] + ++parts[2];
    }).attr("id", function(){//Hago lo mismo con los IDs
        var parts = this.id.match(/(\D+)(\d*)$/);
  		return parts[1] + ++parts[2];
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value",""); //MODIFICACION OFICINA 12/06/2015
    $tr.find('.bootstrap-select').remove();

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody .trMedida:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $('#'+tabla).find(".selectpicker").selectpicker('refresh');
    }
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectPlazo').change(function(){
        oyentePlazos($(this));
    });
}

function eliminaFila2(tabla){
  if($('#'+tabla).find("tbody .trMedida").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' .trMedida').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' .trMedida:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' .trMedida:not(:first)').eq(i).find("input:not([type=checkbox],.input-block-level),select,textarea").attr("name", function(){
                //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
                var parts = this.id.match(/(\D+)(\d*)$/);
                //Creo un nombre nuevo incrementando el número de fila (++parts[2])
                return parts[1] + i;
                //Hago lo mismo con los IDs
            }).attr("id", function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                return parts[1] + i;
            });

            $('#'+tabla+' .trMedida:not(:first)').eq(i).find("input[type=checkbox]").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
      }
  }
  else{
    alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
  }
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
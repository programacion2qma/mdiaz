<?php
@include_once('config.php');//Carga del archivo de configuración
@include_once('../api/nucleo.php');//Carga del núcleo de funciones comunes

//Inicio de funciones específicas

transformaValoresPOST();//Llamada a función que debe de ejecutarse siempre que se envien parámetros por $_POST
//La siguiente función transforma todos los datos pasados por $_POST a mayúsculas, a excepción del email (que va en minúsculas) y las observaciones
function transformaValoresPOST(){
    /*foreach($_POST as $nombre=>$valor){
        if(is_array($_POST[$nombre])){
            $select = $_POST[$nombre];
            $text = array();
            if(!empty($select)){
                for($k=0;$k<count($select);$k++){
                    $text[$k] = strtoupper($select[$k]);
                }
            }
            $_POST[$nombre]=$text;
        } else {
            if(substr_count($nombre,'email')==1){//Para pasar el email a minúsculas
                $_POST[$nombre]=strtolower($_POST[$nombre]);
            }
            elseif(substr_count($nombre,'observaciones')==0 && substr_count($nombre,'firma')==0
                && substr_count($nombre,'consulta')==0 && substr_count($nombre,'busqueda')==0
                && substr_count($nombre,'usuario')==0  && substr_count($nombre,'clave')==0 
                && substr_count($nombre,'camposModificados')==0 && substr_count($nombre,'pregunta')==0
                && substr_count($nombre,'temario')==0 && !is_array($_POST[$nombre])
                && substr_count($nombre,'tabla')==0 && substr_count($nombre,'campo')==0 
                && substr_count($nombre,'campoCodigo')==0 && substr_count($nombre,'url')==0
                && substr_count($nombre,'fichero')==0){//Para cambiar a mayúsculas todo lo demás, excepto las observaciones, las consultas, las credenciales de usuario y los array

                $mayusculas=strtoupper($_POST[$nombre]);

                $mayusculas=str_replace('á','Á',$mayusculas);
                $mayusculas=str_replace('é','É',$mayusculas);
                $mayusculas=str_replace('í','Í',$mayusculas);
                $mayusculas=str_replace('ó','Ó',$mayusculas);
                $mayusculas=str_replace('ú','Ú',$mayusculas);
                $mayusculas=str_replace('Ê','Ú',$mayusculas);//Por algún motivo, la Ú mayúscula se convierte en Ê cuando ya está escrita así en el campo
                $mayusculas=str_replace('ñ','Ñ',$mayusculas);
            
                $_POST[$nombre]=$mayusculas;
            }
        
            $_POST[$nombre]=str_replace("'",'"',$_POST[$nombre]);//Sustituye la comilla simple por tilde para que no haya conflictos de sintaxis
        }
    }*/
}

//Función específica de login para el software de "La Academia Empresas" (comprueba que el usuario esté activo)

function loginAcademia($usuario,$clave){
    global $_CONFIG;

    $usuario=addslashes($usuario);
    $clave=addslashes($clave);
    if($usuario!='' && $clave!=''){
        conexionBD();
        $consulta=consultaBD("SELECT codigo, usuario, tipo FROM usuarios WHERE BINARY usuario='$usuario' AND BINARY clave='$clave' AND activo='SI';");
        cierraBD();
        if(mysql_num_rows($consulta)==1){
            $datos=mysql_fetch_assoc($consulta);
            $_SESSION['codigoU']=$datos['codigo'];
            $_SESSION['usuario']=$datos['usuario'];
            $_SESSION['tipoUsuario']=$datos['tipo'];
            $_SESSION['ejercicio']=date('Y');//Define por defecto el filtro de ejercicios al año actual
            $error=1;
            $venta=false;
            if($_SESSION['tipoUsuario']=='CLIENTE'){
                $codigoCliente=obtenerCodigoCliente(true);
                $venta=consultaBD('SELECT * FROM ventas_servicios WHERE (estado="INCIDENTADA" OR estado="ANULADA") AND codigoCliente='.$codigoCliente,true,true);
            }
            if($venta){
                $error=2;
            } else {
                if(isset($_POST['sesion']) && $_POST['sesion']=="si"){//Crea cookie de Sesión
                    $valor=md5(rand());
                    conexionBD();
                    $consulta=consultaBD("UPDATE usuarios SET sesion='$valor' WHERE usuario = '$usuario' AND clave = BINARY '$clave';");
                    cierraBD();
                // Caduca en un año
                    setcookie($_CONFIG['nombreCookie'],$valor, time() + 365 * 24 * 60 * 60);
                }
        
                if($usuario!='soporte'){//MODIFICACIÓN 04/09/2015: para incluir el usuario "fantasma" de soporte
                    registraAcceso();
                }

                header('Location: '.$_CONFIG['raiz'].'inicio.php');
            }
        }
    }
    return $error;
}

//Fin función específica de login


//Parte de inicio

function creaEstadisticasInicio(){
	$res=array();

	conexionBD();

	$datos=consultaBD("SELECT COUNT(codigo) AS total FROM servicios WHERE activo='SI';",false,true);
	$res['servicios']=$datos['total'];

    $datos=consultaBD("SELECT COUNT(codigo) AS total FROM comerciales WHERE activo='SI';",false,true);
    $res['agentes']=$datos['total'];

    $ejercicio=obtieneWhereEjercicioEstadisticas('fechaAlta',true);
    $datos=consultaBD("SELECT COUNT(codigo) AS total FROM clientes WHERE activo='SI' AND posibleCliente='NO' $ejercicio;",false,true);
    $res['clientes']=$datos['total'];

	cierraBD();

	return $res;
}

function creaBotonesInicio(){
    echo '
    <a class="btn-floating btn-large btn-primary btn-creacion noAjax" id="target-1" title="Guía rápida"><i class="icon-book"></i></a>
    <a class="btn-floating btn-large btn-propio btn-eliminacion" href="cerrarSesion.php" id="target-2" title="Cerrar sesión"><i class="icon-power-off"></i></a>';
}

//Fin parte de inicio


//Parte de manejo de listados

function obtieneLimitesListado(){
	$limite='';
	if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength']!='-1') {
        $limite='LIMIT '.(int)$_GET['iDisplayStart'].', '.(int)$_GET['iDisplayLength'];
    }
    return $limite;
}

function obtieneOrdenListado($columnas){
    $orden = '';
    if (isset($_GET['iSortCol_0'])) {
        $orden = 'ORDER BY  ';
        for ($i=0 ; $i<(int)$_GET['iSortingCols'] ; $i++) {
            if ( $_GET[ 'bSortable_'.(int)$_GET['iSortCol_'.$i] ] == 'true' ) {
                $orden .= $columnas[ (int)$_GET['iSortCol_'.$i] ].' '.
                    ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
            }
        }

        $orden = substr_replace($orden, '', -2);
        if ($orden == 'ORDER BY') {
            $orden = '';
        }
    }
    return $orden;
}

/*
    El algoritmo de creación del WHERE/HAVING de esta función es específico de "La Academia Empresas", ya que tiene búsquedas por columnas.
    El filtrado de usuario, determinado por el parámetro $compruebaPerfil, se realiza siguiendo los siguientes criterios:
        Perfil COMERCIAL, ADMINISTRACION2 y TELEMARKETING: se filtran los registros por el código del usuario actual.
        Perfil DIRECTOR y DELEGADO: se filtran los registros cuyos código de usuario dependan del que tiene el usuario actual
        Resto de perfiles: no se comprueba el usuario.
*/
function obtieneWhereListado($where,$columnas,$compruebaPerfil=false,$listadoClientes=false,$ejercicio=true){
	$camposFecha=array();//Este array sirve para ir almacenando las columnas de tipo fecha, e identificar así si la condición debe ser >= ó <=
    $res=' AND(';
    if($ejercicio && !$listadoClientes){
        $res=obtieneWhereEjercicioListado($columnas,$listadoClientes);
    }

    for($i=0; $i<count($columnas) ; $i++){
        $columnas[$i]=str_replace("'",'"',$columnas[$i]);//Sustituye la comilla simple por la tilde, para que no haya conflictos de sintaxis

        if(isset($_GET['sSearch']) && $_GET['sSearch']!='') {//Búsqueda normal (global)
            $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch']."%' USING latin1) AS binary) USING utf8)  OR  ";
        }

        if(isset($_GET['sSearch_'.$i]) && $_GET['sSearch_'.$i]!=''){//Búsqueda por columnas (nótese el índice _$i)
            if($_GET['sSearch_'.$i]=='ISNUL'){//Para los casos de NULL
                $res.=$columnas[$i]." IS NULL AND ";
            }
            elseif($_GET['sSearch_'.$i]=='NOTNUL'){//Para los casos de NOT NULL
                $res.=$columnas[$i]." IS NOT NULL AND ";
            }
            elseif(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$camposFecha)){
                $campoFecha=formateaFechaBD($_GET['sSearch_'.$i]);
                $res.=$columnas[$i].">='".$campoFecha."' AND ";
                
                array_push($camposFecha,$columnas[$i]);
            }
            elseif(substr_count($columnas[$i],'fecha')>0){
                $campoFecha=formateaFechaBD($_GET['sSearch_'.$i]);
                $res.=$columnas[$i]."<='".$campoFecha."' AND ";
            }
            elseif(substr_count($columnas[$i],'precio')>0 || substr_count($columnas[$i],'importe')>0){//Para que la búsqueda de 9,99 coincida con el valor 9.99
                $campoNumero=formateaNumeroFiltro($_GET['sSearch_'.$i]);
                $res.=$columnas[$i]." LIKE '%".$campoNumero."%' AND ";
            }
            elseif(substr_count($columnas[$i],'codigo')>0 || substr_count($columnas[$i],'estado')>0){//Cuando se está filtrando por un código, la ocurrencia debe ser exacta (si se usa LIKE, el código 123 está contenido en el 7364123).
                $res.=$columnas[$i]."='".$_GET['sSearch_'.$i]."' AND ";
            }
            else{//Casos normales de cadenas (hay que hacerle conversiones de codificación de caracteres para que no haga cosas raras con tildes y ñ)
                $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch_'.$i]."%' USING latin1) AS binary) USING utf8) AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
            }
        }
    }

    $res=substr_replace($res,'',-4);
    //if($ejercicio && !$listadoClientes){
        $res.=')';
    //}

    if($res==' )'){//No se ha realizado búsquedas...
        $res='';
    }

    if($compruebaPerfil){
        $res.=filtraListadoPorUsuario();
    }

    return $where.$res;
}

function obtieneWhereListadoEntradas($where,$columnas,$compruebaPerfil=false,$listadoClientes=false,$ejercicio=true){
    $camposFecha=array();//Este array sirve para ir almacenando las columnas de tipo fecha, e identificar así si la condición debe ser >= ó <=
    $res='';
    if($ejercicio && !$listadoClientes){
        $res=obtieneWhereEjercicioListado($columnas,$listadoClientes);
    }

    for($i=0; $i<count($columnas) ; $i++){
        $columnas[$i]=str_replace("'",'"',$columnas[$i]);//Sustituye la comilla simple por la tilde, para que no haya conflictos de sintaxis

        if(isset($_GET['sSearch']) && $_GET['sSearch']!='') {//Búsqueda normal (global)
            $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch']."%' USING latin1) AS binary) USING utf8)  OR  ";
        }

        if(isset($_GET['sSearch_'.$i]) && $_GET['sSearch_'.$i]!=''){//Búsqueda por columnas (nótese el índice _$i)
            if($_GET['sSearch_'.$i]=='ISNUL'){//Para los casos de NULL
                $res.=$columnas[$i]." IS NULL AND ";
            }
            elseif($_GET['sSearch_'.$i]=='NOTNUL'){//Para los casos de NOT NULL
                $res.=$columnas[$i]." IS NOT NULL AND ";
            }
            elseif(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$camposFecha)){
                $campoFecha=formateaFechaBD($_GET['sSearch_'.$i]);
                $res.=' '.$columnas[$i].">='".$campoFecha."' AND ";
                
                array_push($camposFecha,$columnas[$i]);
            }
            elseif(substr_count($columnas[$i],'fecha')>0){
                $campoFecha=formateaFechaBD($_GET['sSearch_'.$i]);
                $res.=' '.$columnas[$i]."<='".$campoFecha."' AND ";
            }
            elseif(substr_count($columnas[$i],'precio')>0 || substr_count($columnas[$i],'importe')>0){//Para que la búsqueda de 9,99 coincida con el valor 9.99
                $campoNumero=formateaNumeroFiltro($_GET['sSearch_'.$i]);
                $res.=' '.$columnas[$i]." LIKE '%".$campoNumero."%' AND ";
            }
            elseif(substr_count($columnas[$i],'codigo')>0 || substr_count($columnas[$i],'estado')>0){//Cuando se está filtrando por un código, la ocurrencia debe ser exacta (si se usa LIKE, el código 123 está contenido en el 7364123).
                $res.=' '.$columnas[$i]."='".$_GET['sSearch_'.$i]."' AND ";
            }
            else{//Casos normales de cadenas (hay que hacerle conversiones de codificación de caracteres para que no haga cosas raras con tildes y ñ)
                $res.=" CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$_GET['sSearch_'.$i]."%' USING latin1) AS binary) USING utf8) AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
            }
        }
    }

    $res=substr_replace($res,'',-4);
    if($ejercicio && !$listadoClientes){
        $res.=')';
    }

    if($res==' )'){//No se ha realizado búsquedas...
        $res='';
    }

    if($compruebaPerfil){
        $res.=filtraListadoPorUsuario();
    }

    if($res==''){
        $where=substr_replace($where,'',-4);        
    }

    return $where.$res;
}

function formateaNumeroFiltro($numero){
    $res=$numero;

    if(substr_count($numero,',')>0){
        $res=str_replace(',','.',$numero);
    }

    return $res;
}

function filtraListadoPorUsuario(){
    $res='';
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='COMERCIAL'){
        $res=" AND comerciales.codigoUsuarioAsociado=$codigoUsuario";
    }
    elseif($perfil=='ADMINISTRACION2'){
        $res=" AND codigoUsuario=$codigoUsuario";
    }
    elseif($perfil=='TELEMARKETING'){
        $res=" AND telemarketing.codigoUsuarioAsociado=$codigoUsuario";
    }
    /*elseif($perfil=='DELEGADO' || $perfil=='DIRECTOR'){
        $datos=consultaBD("SELECT codigo FROM comerciales WHERE codigoUsuarioAsociado='$codigoUsuario';",true,true);
        $codigoComercial=$datos['codigo'];

        $res="  AND (comerciales.codigo=$codigoComercial OR comerciales.codigoComercial=$codigoComercial
                      OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c1 WHERE c1.codigoComercial=$codigoComercial)
                      OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c2 WHERE c2.codigoComercial IN(SELECT codigo FROM comerciales c3 WHERE c3.codigoComercial=$codigoComercial))
                      OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c4 WHERE c4.codigoComercial IN(SELECT codigo FROM comerciales c5 WHERE c5.codigoComercial IN(SELECT codigo FROM comerciales c6 WHERE c6.codigoComercial=$codigoComercial)))
                      OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c7 WHERE c7.codigoComercial IN(SELECT codigo FROM comerciales c8 WHERE c8.codigoComercial IN(SELECT codigo FROM comerciales c9 WHERE c9.codigoComercial IN(SELECT codigo FROM comerciales c10 WHERE c10.codigoComercial=$codigoComercial))))
                      OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c11 WHERE c11.codigoComercial IN(SELECT codigo FROM comerciales c12 WHERE c12.codigoComercial IN(SELECT codigo FROM comerciales c13 WHERE c13.codigoComercial IN(SELECT codigo FROM comerciales c14 WHERE c14.codigoComercial IN(SELECT codigo FROM comerciales c15 WHERE c15.codigoComercial=$codigoComercial)))))
                      OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c16 WHERE c16.codigoComercial IN(SELECT codigo FROM comerciales c17 WHERE c17.codigoComercial IN(SELECT codigo FROM comerciales c18 WHERE c18.codigoComercial IN(SELECT codigo FROM comerciales c19 WHERE c19.codigoComercial IN(SELECT codigo FROM comerciales c20 WHERE c20.codigoComercial IN(SELECT codigo FROM comerciales c21 WHERE c21.codigoComercial=$codigoComercial))))))
                )";
    }*/
    elseif($perfil=='DELEGADO'){
        //Explicación de la consulta: la primera condición selecciona el comercial del usuario, y la segunda todos los comerciales cuyo campo "Depende de" tienen como comercial el asociado al usuario
        $res=" AND (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u2 ON c2.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario)))";
    }
    elseif($perfil=='DIRECTOR'){
        //Explicación de la consulta: la primera condición selecciona el comercial del usuario, la segunda todos los Delegados (comerciales) cuyo campo "Depende de" tienen como comercial el asociado al usuario, y la tercera todos los comerciales que tienen por Delegados los seleccionados anteriormente
        $res=" AND (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u1 ON c2.codigoUsuarioAsociado=u1.codigo WHERE u1.codigo=$codigoUsuario)) OR comerciales.codigo IN(SELECT c3.codigo FROM comerciales AS c3 WHERE c3.codigoComercial IN(SELECT c4.codigo FROM comerciales AS c4 WHERE c4.codigoComercial IN(SELECT c5.codigo FROM comerciales AS c5 INNER JOIN usuarios AS u2 ON c5.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario))))";
    }


    return $res;
}

function inicializaArrayListado($consulta,$consultaPaginacion){
	$intervalo=25;
	if(isset($_GET['sEcho'])){
		$intervalo=intval($_GET['sEcho']);
	}

	$seleccionado=mysql_num_rows($consulta);
	$total=mysql_num_rows($consultaPaginacion);

	return array(
        'sEcho' => $intervalo,
        'iTotalRecords' => $seleccionado,
        'iTotalDisplayRecords' => $total,
        'aaData' => array()
    );
}

//Fin parte manejo listados


//Parte de satisfacción de clientes


function creaTablaSatisfaccion(){
    echo '
    <center>
        <table class="table table-striped table-bordered mitadAncho">
          <tbody>
            <tr>
              <th> Apartado Analizado </th>
              <th> Valoración </th>
            </tr>';
}

function cierraTablaSatisfaccion(){
    echo '</tbody>
        </table>
    </center>';
}

function creaPreguntaTablaEncuesta($texto,$numero,$datos=false){    
    echo "
    <tr>
        <td class='justificado'>$texto</td>
        <td class='centro'>
            <input type='number' name='pregunta".$numero."' id='pregunta".$numero."' class='rating' data-min='1' data-max='5'";
    
    if($datos!=false){      
        echo "value=".$datos['pregunta'.$numero];       
    }

    echo" />
        </td>
    </tr>";
}

function creaAreaTextoTablaEncuesta($datos=false){
    $valor=compruebaValorCampo($datos,'comentarios');
    echo "
    <tr>
        <td colspan='2'>Si desea realizar algún comentario adicional, por favor, hágalo a continuación:</td>
    </tr>
    <tr>
        <td colspan='2'>
            <textarea name='comentarios' class='textarea-amplia' id='comentarios'>$valor</textarea>
        </td>
    </tr>";
}

function textoCuestionario(){
    global $_CONFIG;

    echo "
    <p class='justificado parrafo-margenes'>
      <span class='negritaCursiva'>Estimado cliente:</span><br />en ".$_CONFIG['tituloGeneral']." nos preocupa mucho su satisfacción con los servicios
      contratados con nuestra entidad y la calidad final de los proyectos ejecutados. Por ello, le agradecemos que pueda 
      hacernos llegar la presente encuesta de satisfacción, indicando además en el apartado observaciones cualquier 
      comentario que crea que pueda ayudarnos a mejorar la calidad de nuestros servicios o a ajustar los mismos a sus 
      necesidades. Sin duda, creemos firmemente que en sus opiniones y sugerencias se encuentran nuestras mayores 
      oportunidades de mejorar.<br />
      Gracias por anticipado.<br /><br />
      <strong>Puntúe de en una escala del 1 al 5 cada uno de los siguientes aspectos:</strong>
    </p>";
}

//Fin parte de satisfacción de clientes

//Parte de agenda/tareas


function imprimeTareasHoy(){
    $prioridad=array('normal'=>"<span class='label label-info'>Normal</span>",'alta'=>"<span class='label label-danger'>Alta</span>",'baja'=>"<span class='label label-success'>Baja</span>");

    $consulta=consultaBD("SELECT tareas.*, clientes.razonSocial FROM tareas LEFT JOIN clientes ON tareas.codigoCliente=clientes.codigo WHERE tareas.fechaInicio=CURDATE() AND codigoUsuario='".$_SESSION['codigoU']."';",true);
    while($datos=mysql_fetch_assoc($consulta)){
        echo "
        <tr>
            <td>".$datos['tarea']."</td>
            <td>".formateaHoraWeb($datos['horaInicio'])."</td>
            <td>".formateaHoraWeb($datos['horaFin'])."</td>
            <td>".$datos['razonSocial']."</td>
        </tr>";
    }
}



function operacionesAgenda(){
    $res=true;

    if(isset($_POST['repetir'])){
        $res=repiteEventoSemanalmente();
    }
    elseif(isset($_GET['realizada'])){
        $res=cambiaValorCampo('estado','REALIZADA',$_GET['tarea'],'tareas');
    }
    elseif(isset($_POST['codigo'])){
        $res=actualizaDatos('tareas');
    }
    elseif(isset($_POST['tarea'])){
        $res=insertaDatos('tareas');
    }
    elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
        $res=eliminaDatos('tareas');
    }

    mensajeResultado('tarea',$res,'sesión');
    mensajeResultado('repetir',$res,'sesión');
    mensajeResultado('elimina',$res,'sesión', true);
}

//Fin parte de agenda/tareas

//Funciones comunes en varias secciones

function compruebaCampoRadio($indice){
    $res='RADIO';

    if(isset($_POST[$indice])){
        $res=$_POST[$indice];
    }

    return $res;
}

function campoSelectProvincia($valor=false,$nombreCampo='provincia',$texto='Provincia',$tipo=0,$obligatorio='obligatorio'){
    $provincias=array('','ÁLAVA','ALBACETE','ALICANTE','ALMERÍA','ÁVILA','BADAJOZ','ILLES BALEARS','BARCELONA','BURGOS','CÁCERES','CÁDIZ','CASTELLÓN DE LA PLANA','CIUDAD REAL','CÓRDOBA','A CORUÑA','CUENCA','GIRONA','GRANADA','GUADALAJARA','GUIPÚZCOA','HUELVA','HUESCA','JAÉN','LEÓN','LLEIDA','LA RIOJA','LUGO','MADRID','MÁLAGA','MURCIA','NAVARRA','OURENSE','ASTURIAS','PALENCIA','LAS PALMAS','PONTEVEDRA','SALAMANCA','SANTA CRUZ DE TENERIFE','CANTABRIA','SEGOVIA','SEVILLA','SORIA','TARRAGONA','TERUEL','TOLEDO','VALENCIA','VALLADOLID','VIZCAYA','ZAMORA','ZARAGOZA','CEUTA','MELILLA');
    $provincias2=array();
    foreach ($provincias as $value) {
        array_push($provincias2, convertirMinuscula($value));
    }
    campoSelect($nombreCampo,$texto,$provincias2,$provincias,$valor,"selectpicker $obligatorio span3 show-tick",'data-live-search="true"',$tipo);
}

function campoSelectProvinciaAndalucia($valor=false,$nombreCampo='provincia',$texto='Provincia',$tipo=0,$obligatorio='obligatorio'){
    $provincias=array('','ALMERÍA','CÁDIZ','CÓRDOBA','GRANADA','HUELVA','JAÉN','MÁLAGA','SEVILLA');
    $provincias2=array();
    foreach ($provincias as $value) {
        array_push($provincias2, convertirMinuscula($value));
    }
    campoSelect($nombreCampo,$texto,$provincias2,$provincias,$valor,"selectpicker $obligatorio span3 show-tick",'data-live-search="true"',$tipo);
}

function formateaPrecioBDD($indice){
    $_POST[$indice]=formateaNumeroWeb($_POST[$indice],true);
}

function creaBotonDetalles($url,$texto='Detalles',$icono='icon-search-plus'){
    global $_CONFIG;
    return "<div class='centro'><a class='btn btn-propio' href='".$_CONFIG['raiz'].$url."'><i class='$icono'></i> $texto</a></div>";
}

/* 
La siguiente función comprueba si el registro actual tiene el campo "activo" a No, en cuyo caso le añade al checkbox la clase bloqueado para usarlo
en el callback fnDrawCallback de js/filtroTablaAJAXBusqueda.js
*/
function obtieneCheckTabla($datos,$indiceCodigo='codigo'){
    $res="<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos[$indiceCodigo]."'></div>";
    return $res;
}


function campoFormaPago($datos,$nombreCampo='medioPago',$tipo=0){
    $mediosPago=array('','Pagaré','Transferencia','Recibo SEPA','Descontado');
    $valores=array('','PAGARÉ','TRANSFERENCIA','RECIBO SEPA','DESCONTADO');
    campoSelect($nombreCampo,'Medio de pago',$mediosPago,$valores,$datos,'selectpicker span3 show-tick','',$tipo);
}


function insertaImagenFirmaWord($firma,$documento,$nombreImagen='image2.png'){
    if(trim($firma)!=''){
        $imagenFirma=sigJsonToImage($firma, array('imageSize'=>array(1000, 500),'drawMultiplier'=>1));
        imagepng($imagenFirma,'../documentos/firmas/'.$nombreImagen);

        $documento->replaceImage('../documentos/firmas/',$nombreImagen);
    }
}

function compruebaDuplicidadCampo(){
    $res='ok';

    $tabla=$_POST['tabla'];
    $campo=$_POST['campo'];
    $valor=$_POST['valor'];
    $campoCodigo=$_POST['campoCodigo'];
    $valorCodigo=$_POST['valorCodigo'];

    $whereDetalles='';
    if($valorCodigo!='NO'){
        $whereDetalles="AND $campoCodigo!=$valorCodigo";
    }

    /*La siguiente consulta extrae los registros que tengan en el campo indicado el valor indicado
      Si además se está en los detalles de una ficha, se pasa otra condición al where para que no dé coincidencia con el registro que se está viendo. */
    $consulta=consultaBD("SELECT codigo FROM $tabla WHERE $campo='$valor' $whereDetalles",true);
    if(mysql_num_rows($consulta)>0){
        $res='error';
    }

    echo $res;
}

//Duplicación de la función campoTextoValidador, para usar solo en La Academia Empresas. Contiene el atributo $tabla
function campoTextoValidador($nombreCampo,$texto,$valor='',$clase='input-large',$tabla='',$campoCodigo='codigo',$readonly=false,$tipo=0){
    $des='';
    if($readonly){
        $des='readonly';
    }
    $valor=compruebaValorCampo($valor,$nombreCampo);

    if($tipo==0){
        echo "
        <div class='control-group'>                     
          <label class='control-label' for='$nombreCampo'>$texto:</label>
          <div class='controls'>";
    }
    elseif($tipo==1){
        echo "$texto: ";
    }
    elseif($tipo==2){
        echo "<td>";
    }
        
    
    echo "<input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' tabla='$tabla' campoCodigo='$campoCodigo' value='$valor' $des>";
    
    if($tipo==0){
        echo "
          </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==2){
        echo "</td>";
    }
}

function campoTextoSimboloValidador($nombreCampo,$texto,$simbolo,$valor='',$clase='input-mini pagination-right',$tabla='',$campoCodigo='codigo',$tipo=0,$disabled=false){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    $des='';
    if($disabled){
        $des="readonly='readonly'";
    }

    if($tipo==0){
        echo "
        <div class='control-group'>                     
          <label class='control-label' for='$nombreCampo'>$texto:</label>
          <div class='controls'>";
    }
    elseif($tipo==1){
        echo "
        <td>";
    }

    echo "
        <div class='input-prepend input-append nowrap'>
          <input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' tabla='$tabla' campoCodigo='$campoCodigo' value='$valor' $des>
          <span class='add-on'> $simbolo</span>
        </div>";

    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "
        </td>";
    }

}


function campoTextoTablaValidador($nombreCampo,$valor='',$clase='input-large',$tabla='',$campoCodigo='codigo',$disabled=false){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    $des='';
    if($disabled){
        $des="readonly='readonly'";
    }

    echo "<td>
            <input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' tabla='$tabla' campoCodigo='$campoCodigo' value='$valor' $des>
          </td>";
}

function compruebaPerfil($perfil,$html){
    $res='<div class="alert alert-info"><i class="icon-info-circle"></i> <strong> Sin acciones disponibles para el usuario actual </strong></div>';

    if($_SESSION['tipoUsuario']==$perfil){
        $res=$html;
    }

    echo $res;
}

function cierraVentanaGestionPerfil($perfil,$destino,$columnas=false){
    if(is_array($perfil) && in_array($_SESSION['tipoUsuario'],$perfil)){
        cierraVentanaGestion($destino,$columnas);
    }
    elseif((!is_array($perfil) && $_SESSION['tipoUsuario']==$perfil) || $_SESSION['tipoUsuario']=='ADMIN'){
        cierraVentanaGestion($destino,$columnas);
    }
    else{
        cierraVentanaGestion($destino,$columnas,false,'','');
    }
}

function cierraVentanaGestionInforme($recargar,$destino,$columnas=false,$botonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left',$salto=true){
    if($columnas){
        echo "        </fieldset>
                      <fieldset class='sinFlotar'>";
    }
    if($salto){
        echo "          <br />";
    }


    echo "
                        
                        <div class='form-actions'>";
    if($botonVolver){
        echo "            <a href='$destino' class='btn btn-default'><i class='$iconoVolver'></i> $textoVolver</a>";
    }

    if($botonGuardar){                      
        echo "            <button type='submit' class='btn btn-propio'><i class='$icono'></i> $texto</button>";
    }
    if($recargar!=''){  
        echo "            <a href='".$recargar."' class='btn btn-propio noAjax'><i class='icon-refresh'></i> Recargar informe</a>";
    }
    echo "
                        </div> <!-- /form-actions -->
                      </fieldset>
                    </form>
                    </div>


                </div>
                <!-- /widget-content --> 
              </div>

          </div>
        </div>
        <!-- /container --> 
      </div>
      <!-- /main-inner --> 
    </div>
    <!-- /main -->";
}

/*function creaEstadisticasClientes($condicion){
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];
    $condicion.=obtieneWhereEjercicioEstadisticas('fechaAlta',true);

    if($perfil=='COMERCIAL'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo WHERE codigoUsuarioAsociado=$codigoUsuario AND ".$condicion,true,true);
    }
    elseif($perfil=='ADMINISTRACION2'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo WHERE codigoUsuario=$codigoUsuario AND ".$condicion,true,true);
    }
    elseif($perfil=='TELEMARKETING'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes LEFT JOIN telemarketing ON clientes.codigoTelemarketing=telemarketing.codigo WHERE codigoUsuarioAsociado=$codigoUsuario AND ".$condicion,true,true);
    }
    elseif($perfil=='DELEGADO'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes WHERE codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario) AND ".$condicion,true,true);
    }
    elseif($perfil=='DIRECTOR'){
        $res=consultaBD("SELECT COUNT(clientes.codigo) AS total FROM clientes WHERE codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario IN (SELECT usuarios.codigo FROM usuarios WHERE usuarios.codigoUsuario=$codigoUsuario)) AND ".$condicion,true,true);
    }
    else{
        $res=estadisticasGenericas('clientes',false,$condicion);
    }

    return $res;
}*/

function campoSelectComercial($datos,$creacion=true,$nombreCampo='codigoComercial',$obligatorio='obligatorio'){
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='COMERCIAL'){
        $comercial=consultaBD("SELECT codigo, nombre FROM comerciales WHERE codigoUsuarioAsociado=$codigoUsuario",true,true);
        campoDato('Comercial',$comercial['nombre']);
        campoOculto($comercial['codigo'],$nombreCampo);
    }
    else{
        $query=obtieneQuerySelectComercial($codigoUsuario,$perfil,$creacion);

        if($creacion){//Para poner un campoSelect con el botón de creación
            campoSelectConsultaPlus($nombreCampo,'Comercial',$query,$datos,"selectpicker selectPlus $obligatorio show-tick","data-live-search='true'",'',0,true,'btn-primary',"<i class='icon-plus'></i> Nuevo");
        }
        else{
            campoSelectConsulta($nombreCampo,'Comercial',$query,$datos,'selectpicker span3 show-tick');
        }
    }
}


function obtieneQuerySelectComercial($codigoUsuario,$perfil,$creacion){
    if($perfil=='COMERCIAL'){
        $res="SELECT codigo, nombre FROM comerciales WHERE codigoUsuarioAsociado=$codigoUsuario;";
    }
    elseif($perfil=='ADMINISTRACION2'){
        $res="SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' AND codigoUsuario=$codigoUsuario ORDER BY nombre;";
    }
    elseif($perfil=='DELEGADO' && $creacion){
        $res="SELECT codigo, nombre AS texto FROM comerciales WHERE (codigoUsuarioAsociado=$codigoUsuario OR codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u2 ON c2.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario))) ORDER BY nombre;";
    }
    elseif($perfil=='DIRECTOR' && $creacion){
        $res="SELECT codigo, nombre AS texto FROM comerciales WHERE (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u1 ON c2.codigoUsuarioAsociado=u1.codigo WHERE u1.codigo=$codigoUsuario)) OR comerciales.codigo IN(SELECT c3.codigoComercial FROM comerciales AS c3 WHERE c3.codigo IN(SELECT c4.codigo FROM comerciales AS c4 WHERE c4.codigoComercial IN(SELECT c5.codigo FROM comerciales AS c5 INNER JOIN usuarios AS u2 ON c5.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario)))) ORDER BY nombre;";
    }
    else{
        $res="SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre;";
    }

    return $res;
}

function campoSelectColaborador($datos,$creacion=true,$nombreCampo='codigoColaborador'){
    $perfil=$_SESSION['tipoUsuario'];

    if($creacion){
      campoSelectConsultaAjax($nombreCampo,'Colaborador',"SELECT colaboradores.codigo, colaboradores.nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY colaboradores.nombre ;",$datos,'colaboradores/gestion.php','selectpicker span3 show-tick');  
    }
    else{
        campoSelectConsulta($nombreCampo,'Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre;",$datos);
    }
}

function campoSelectConsultor($datos,$nombreCampo='codigoConsultor'){
    campoSelectConsultaPlus($nombreCampo,'Consultor',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE tipo='CONSULTOR' ORDER BY nombre;",$datos,'selectpicker selectPlus show-tick',"data-live-search='true'",'',0,true,'btn-primary',"<i class='icon-plus'></i> Nuevo");  
}

function campoSelectClienteFiltradoPorUsuario($datos,$texto='Cliente',$nombreCampo='codigoCliente',$obligatorio='',$posibleCliente=false){
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    $wherePosibleCliente="AND posibleCliente='NO'";
    if($datos){
        $wherePosibleCliente="AND (posibleCliente='NO' OR clientes.codigo = '".$datos[$nombreCampo]."')";
    }
    if($posibleCliente){
        $wherePosibleCliente="";
    }

    if($perfil=='ADMINISTRACION2'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT clientes.codigo, razonSocial AS texto FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente WHERE 1=1 $wherePosibleCliente AND codigoComercial IN(SELECT codigo FROM comerciales WHERE codigoUsuario=$codigoUsuario) GROUP BY clientes.codigo ORDER BY razonSocial;",$datos,'posibles-clientes/gestion.php',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    elseif($perfil=='COMERCIAL'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT clientes.codigo, razonSocial AS texto FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente WHERE 1=1  $wherePosibleCliente AND codigoComercial IN(SELECT codigo FROM comerciales WHERE codigoUsuarioAsociado=$codigoUsuario) GROUP BY clientes.codigo ORDER BY razonSocial;",$datos,'posibles-clientes/gestion.php',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    elseif($perfil=='TELEMARKETING'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT clientes.codigo, razonSocial AS texto FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente WHERE 1=1  $wherePosibleCliente AND codigoTelemarketing IN(SELECT codigo FROM telemarketing WHERE codigoUsuarioAsociado=$codigoUsuario) ORDER BY razonSocial;",$datos,'posibles-clientes/gestion.php',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    elseif($perfil=='DELEGADO'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT clientes.codigo, razonSocial AS texto FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo WHERE 1=1  $wherePosibleCliente AND (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u2 ON c2.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario))) GROUP BY clientes.codigo ORDER BY razonSocial;",$datos,'posibles-clientes/gestion.php',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    elseif($perfil=='DIRECTOR'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT clientes.codigo, razonSocial AS texto FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo WHERE 1=1  $wherePosibleCliente AND (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u1 ON c2.codigoUsuarioAsociado=u1.codigo WHERE u1.codigo=$codigoUsuario)) OR comerciales.codigo IN(SELECT c3.codigoComercial FROM comerciales AS c3 WHERE c3.codigo IN(SELECT c4.codigo FROM comerciales AS c4 WHERE c4.codigoComercial IN(SELECT c5.codigo FROM comerciales AS c5 INNER JOIN usuarios AS u2 ON c5.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario)))) GROUP BY clientes.codigo ORDER BY razonSocial;",$datos,'posibles-clientes/gestion.php',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
    else{
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT clientes.codigo, razonSocial AS texto FROM clientes WHERE 1=1  $wherePosibleCliente ORDER BY razonSocial;",$datos,'clientes/gestion.php',"span3 selectAjax selectpicker show-tick $obligatorio");
    }
}

function campoSelectCliente($datos,$texto='Cliente',$nombreCampo='codigoCliente'){
        campoSelectConsultaAjax($nombreCampo,$texto,"SELECT clientes.codigo, razonSocial AS texto FROM clientes ORDER BY razonSocial;",$datos,'clientes/gestion.php',"span3 selectAjax selectpicker show-tick");
}


function campoTelemarketing($datos,$nombreCampo='codigoTelemarketing'){
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='TELEMARKETING'){
        $tele=consultaBD("SELECT codigo, nombre FROM telemarketing WHERE codigoUsuarioAsociado=$codigoUsuario",true,true);
        campoDato('Telemarketing',$tele['nombre']);
        campoOculto($tele['codigo'],$nombreCampo);
    }
    else{
        campoSelectConsulta($nombreCampo,'Telemarketing',"SELECT codigo, nombre AS texto FROM telemarketing WHERE activo='SI' ORDER BY nombre;",$datos);
    }
}

function campoActivoVenta($datos){
    $perfil=$_SESSION['tipoUsuario'];

    if($perfil=='COMERCIAL' || $perfil=='TELEMARKETING' || $perfil=='DELEGADO' || $perfil=='DIRECTOR'){
        campoOculto($datos,'activo','PENDIENTE');
    }
    else{
        campoRadio('activo','Activo',$datos,'SI',array('Si','Pendiente de validar','No'),array('SI','PENDIENTE','NO'),true);
    }
}


//Función para detectar los cambios realizados en el formulario y guardar los campos modificados en "camposModificados"
function detectaCambios($tabla,$datos){
    $perfil=$_SESSION['tipoUsuario'];

    if($perfil=='COMERCIAL' || $perfil=='TELEMARKETING' || $perfil=='DELEGADO' || $perfil=='DIRECTOR'){//Solo para los perfiles COMERCIAL y derivados...
        $camposModificados='';

        $consulta=consultaBD("SELECT * FROM $tabla WHERE codigo=".$datos['codigo'],true,true);

        foreach($consulta as $indice => $valor){
            $datos=formateaDatoComprobacionCambios($datos,$indice);

            /* Utilizo isset en combinación con $valor==SI para detectar los cambios en los checks. 
               Además, el substr_count hace que si el comercial entra una 2ª vez en el formulario, no se marquen los campos como no modificados */
            if(substr_count($indice,'camposModificados')==0 && ((isset($datos[$indice]) && $datos[$indice]!=$valor) || (!isset($datos[$indice]) && $valor=='SI') || substr_count($datos['camposModificados'],$indice)==1)){
                $camposModificados.=$indice.';';
            }
        }

        if($camposModificados!=''){
            $camposModificados=quitaUltimaComa($camposModificados,1);
            $_POST['camposModificados']=$camposModificados;//Lo añado a $_POST para que actualizaDatos lo obtenga
            $_POST['activo']='PENDIENTE';
        }
    }
    elseif($datos['activo']=='SI'){
        $_POST['camposModificados']='';//Para que si el perfil de administración ha activado el registro, se limpie el campo de cambios
    }
}

function detectaCambiosSubTabla($indiceReferencia,$campoCodigo,$tabla,$datos){
    $perfil=$_SESSION['tipoUsuario'];

    if($perfil=='COMERCIAL' || $perfil=='TELEMARKETING' || $perfil=='DELEGADO' || $perfil=='DIRECTOR'){//Solo para los perfiles COMERCIAL y derivados...
        $camposModificados='';

        conexionBD();

        for($i=0;isset($datos[$indiceReferencia.$i]);$i++){
            $consulta=consultaBD("SELECT * FROM $tabla WHERE $campoCodigo=".$datos['codigo']." LIMIT $i,1",false,true);

            if(!$consulta){
                $camposModificados.=$indiceReferencia.$i.';';
            }
            else{
                foreach($consulta as $indice => $valor){
                    $datos=formateaDatoComprobacionCambios($datos,$indice,true,$i);

                    //Condición similar a la utilizada en detectaCambios(), solo que se añade una comprobación de que el campo actual no se haya añadido ya a $_POST['camposModificados']
                    if((isset($datos[$indice.$i]) && $datos[$indice.$i]!=$valor) || (!isset($datos[$indice.$i]) && $valor=='SI') || (substr_count($datos['camposModificados'],$indice.$i)==1 && substr_count($_POST['camposModificados'],$indice.$i)==0)){
                        $camposModificados.=$indice.$i.';';
                    }
                }
            }
        }

        cierraBD();

        if($camposModificados!=''){
            $camposModificados=quitaUltimaComa($camposModificados,1);
            if($_POST['camposModificados']!=''){
                $_POST['camposModificados'].=';'.$camposModificados;//Lo añado a $_POST para que actualizaDatos lo obtenga
            }
            else{
                $_POST['camposModificados']=$camposModificados;//Lo añado a $_POST para que actualizaDatos lo obtenga
            }

            $_POST['activo']='PENDIENTE';
        }
    }
}

function formateaDatoComprobacionCambios($datos,$indice,$subtabla=false,$i=false){
    $indiceForm=$indice;
    if($subtabla){
        $indiceForm=$indice.$i;
    }

    if(substr_count($indice,'hora')==1){
        $datos[$indiceForm].=':00';//Para que no identifique la misma hora como diferente porque del formulario no viene con los segundos
    }
    elseif(substr_count($indice,'coste')==1 || substr_count($indice,'precio')==1 || substr_count($indice,'importe')==1){
        $datos[$indiceForm]=formateaNumeroWeb($datos[$indiceForm],true);//Lo mismo para los importes
    }
    elseif(isset($datos[$indiceForm]) && substr_count($indice,'codigo')==1 && $datos[$indiceForm]=='NULL'){
        $datos[$indiceForm]=NULL;//Similar para los códigos, para que 'NULL' coincida con NULL
    }

    return $datos;
}

function campoAccionFormativa($valor=false,$clase=''){
    global $_CONFIG;
    $whereActivo='';

    if($valor){
        $valor=compruebaValorCampo($valor,'codigoAccionFormativa');
    }
    else{
        $whereActivo="WHERE activo='SI'";
    }

    echo "
    <div class='control-group'>                     
        <label class='control-label' for='codigoAccionFormativa'>Acción Formativa:</label>
        <div class='controls nowrap'>

            <select name='codigoAccionFormativa' consulta=\"SELECT codigo, CONCAT(accionFormativa,' - ',accion) AS texto FROM acciones_formativas WHERE activo='SI' ORDER BY accion;\" class='selectpicker selectAjax show-tick obligatorio $clase' id='codigoAccionFormativa' data-live-search='true'>
            <option value='NULL'></option>";
        
    $consulta=consultaBD("SELECT codigo, CONCAT(accionFormativa,' - ',accion) AS texto FROM acciones_formativas $whereActivo ORDER BY accion;",true);
    while($datos=mysql_fetch_assoc($consulta)){

        if($valor!=false && $valor==$datos['codigo']){//Por defecto solo se muestra el valor seleccionado, si existiese
            echo "<option value='".$datos['codigo']."' selected='selected' >".$datos['texto']."</option>";
        }
    }
        
    echo "</select> 
          <a enlace='".$_CONFIG['raiz']."acciones-formativas/gestion.php' href='#' class='btn btn-primary btn-small botonSelectAjax noAjax' target='_blank' id='boton-codigoAccionFormativa'><i class='icon-plus'></i></a>

        </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
    
}

function campoID($datos){
    if($datos){
        campoDato('ID',$datos['codigo']);
    }
}


function campoLogo($nombreCampo,$texto,$tipo=0,$valor=false,$ruta=false,$nombreDescarga='',$claseContenedor=''){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    if(!$valor || $valor=='NO'){
        if($tipo==0){
            echo "
            <div class='control-group $claseContenedor' id='contenedor-$nombreCampo'>                     
                <label class='control-label' for='$nombreCampo'>$texto:</label>
                <div class='controls'>
                    <input type='file' name='$nombreCampo' id='$nombreCampo' /><div class='tip'>Solo se admiten ficheros de 1 MB como máximo</div>
                </div> <!-- /controls -->       
            </div> <!-- /control-group -->";
        }
        elseif($tipo==1){
            echo "<td class='$claseContenedor' id='contenedor-$nombreCampo'><input type='file' name='$nombreCampo' id='$nombreCampo' /></td>";
        }
        else{
            echo "<span class='$claseContenedor' id='contenedor-$nombreCampo'><input type='file' name='$nombreCampo' id='$nombreCampo' /></span>";
        }
    }
    else{
        campoDescargaLogo($nombreCampo,$texto,$ruta,$valor,$tipo,$nombreDescarga);
        campoOculto($valor,$nombreCampo);
    }
}

function campoDescargaLogo($nombreCampo,$texto,$ruta,$valor,$tipo=0,$nombreDescarga=''){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    
    if($nombreDescarga==''){
        $nombreDescarga=$valor;
    }

    
    if($valor=='NO' || $valor==''){
        campoLogo($nombreCampo,$texto,$tipo,$valor,$ruta);
    }
    else{
        if($tipo==0){
            echo "
                <div class='control-group' id='contenedor-$nombreCampo'>                     
                  <label class='control-label'>$texto:</label>
                  <div class='controls'>
                    <a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>
                    <button type='button' class='iconoBorrado' id='elimina-$nombreCampo'><i class='icon-trash'></i> Eliminar</button>
                  </div> <!-- /controls -->       
                </div> <!-- /control-group -->";
        }
        elseif($tipo==1){
            echo "<td><a class='btn btn-propio noAjax descargaFichero' href='$ruta$valor' target='_blank' nombre='$nombreCampo'><i class='icon-cloud-download'></i> $nombreDescarga</a></td>";
        }
        else{
            echo "<a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>";
        }

        campoLogo($nombreCampo.'Oculto',$texto,$tipo,false,$ruta,'','hide');
    }
}

function generaCampoCheckTipoIconoParaTabla($nombreCampo,$datos){//Para poner los checks estilo iconos en las tablas de vencimientos y XML Acciones (los iconos se ponen vía CSS)
    $valor=compruebaValorCampo($datos,$nombreCampo);

    $res="<div class='centro'><label class='checkbox inline'>
            <input type='checkbox' name='$nombreCampo".$datos['codigo']."' id='$nombreCampo".$datos['codigo']."' value='SI'";

    if($valor!=false && $valor=='SI'){
        $res.=" checked='checked'";
    }
    $res.="></label></div>";

    return $res;
}


function campoSelectAccionFormativaVenta($datos){
    $perfilUsuario=$_SESSION['tipoUsuario'];
    $whereActivo='';

    if(!$datos){
        $whereActivo="WHERE activo='SI'";
    }

    if($perfilUsuario!='COMERCIAL' && $perfilUsuario!='TELEMARKETING'){
        campoAccionFormativa($datos,'span7');
    }
    else{
        //Si el usuario actual es comercial o telemarketing, le quito el enlace del campo a la ficha de la acción formativa (porque no tienen acceso a ese área)
        campoSelectConsultaAjax('codigoAccionFormativa','Acción formativa',"SELECT codigo, CONCAT(accionFormativa,' - ',accion) AS texto FROM acciones_formativas $whereActivo ORDER BY accion;",$datos,'','selectpicker selectAjax span7 show-tick obligatorio');
    }
}


function compruebaCampoNecesarioValidacionAjax($valor,$nombre){
    $res='';

    if($valor==NULL || trim($valor)=='' || $valor=='0000' || $valor=='0000-00-00'){
        $res=$nombre.', ';
    }

    return $res;
}



function compruebaUsuarioComercialFacturacion(){
    $res=true;

    $tipoUsuario=$_SESSION['tipoUsuario'];

    if($tipoUsuario=='COMERCIAL' || $tipoUsuario=='DELEGADO' || $tipoUsuario=='DIRECTOR'){
        $res=false;
    }

    return $res;
}


function obtieneWhereListadoExcel($where,$columnas){
    $datos=arrayFormulario();
    $camposFecha=array();

    $res=obtieneWhereEjercicioListado($columnas,false);

    for($i=0; $i<count($columnas) ; $i++){
        if(isset($datos['global']) && $datos['global']!='') {//Búsqueda normal (global)
            $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$datos['global']."%' USING latin1) AS binary) USING utf8)  OR  ";
        }

        if(isset($datos[$i]) && $datos[$i]!='' && $datos[$i]!='NULL'){//Condición extra con respecto a función original: $datos[$i]!='NULL'
            if(substr_count($columnas[$i],'fecha')>0){
                $datos[$i]=formateaFechaBD($datos[$i]);
            }

            if($datos[$i]=='ISNUL'){
                $res.=$columnas[$i]." IS NULL AND ";
            }
            elseif($datos[$i]=='NOTNUL'){
                $res.=$columnas[$i]." IS NOT NULL AND ";
            }
            elseif(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$camposFecha)){
                $res.=$columnas[$i].">='".$datos[$i]."' AND ";
                
                array_push($camposFecha,$columnas[$i]);
            }
            elseif(substr_count($columnas[$i],'fecha')>0){
                $res.=$columnas[$i]."<='".$datos[$i]."' AND ";
            }
            elseif(substr_count($columnas[$i],'precio')>0 || substr_count($columnas[$i],'importe')>0){
                $datos[$i]=formateaNumeroFiltro($datos[$i]);
                $res.=$columnas[$i]." LIKE '%".$datos[$i]."%' AND ";
            }
            elseif(substr_count($columnas[$i],'codigo')>0 || substr_count($columnas[$i],'estado')>0){//Cuando se está filtrando por un código, la ocurrencia debe ser exacta (si se usa LIKE, el código 123 está contenido en el 7364123).
                $res.=$columnas[$i]."='".$datos[$i]."' AND ";
            }
            else{
                $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$datos[$i]."%' USING latin1) AS binary) USING utf8) AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
            }
        }
    }

    $res=substr_replace($res,'',-4);
    $res.=')';

    if($res==' )'){//No se ha realizado búsquedas...
        $res='';
    }

    return $where.$res;
}

// Función para evitar el uso de mysql_insert_id()
function ultimoRegistro($tabla, $conexion = false){
    $res = consultaBD("SELECT codigo FROM ".$tabla." ORDER BY codigo DESC LIMIT 1;", $conexion, true);
    $res = $res['codigo'];
    return $res;			
}

//Fin funciones comunes en varias secciones

//Parte común para los listados con filtros personalizados

function abreCajaBusqueda($id="cajaFiltros"){
    echo '<div class="cajaFiltros form-horizontal hide" id="'.$id.'">
            <h3 class="apartadoFormulario">Filtrar por:</h3>';
}

function cierraCajaBusqueda(){
    echo '</div>';
}

function campoSelectSiNoFiltro($nombreCampo,$texto){
    campoSelect($nombreCampo,$texto,array('','Si','No'),array('','SI','NO'),false,'selectpicker span1 show-tick','');
}

function campoSelectTieneFiltro($nombreCampo,$texto){
    campoSelect($nombreCampo,$texto,array('','Si','No'),array('','NOTNUL','ISNUL'),false,'selectpicker span1 show-tick');//Uso "NUL" porque datatables quita la cadena "NULL" de los valores
}


function creaBotonesGestion($codigo=false,$nombre='',$eliminar=true){
    /*
        Secciones con sus propios botones de gestión:
        Tutorías pendientes
        Tutorías terminadas
        Clientes
        Clientes > Listado servicios
        Facturas
        Series facturas
        Histórico facturas
        Facturas pendientes
        Abonos
        Inicio
        Consultorías > Gestión
        Comercial > Ventas
        Tutores > Listado cursos
        Facturación > Remesas
        Almacén > Envío claves
        Almacén > Documentación
        Almacén > Documentación > Etiquetas
        Almacén > Documentación > Carpetas
        Incidencias > Incidencias software
    */
    $parametro='';
    if($codigo!=false){
        $parametro='?'.$nombre.'='.$codigo;
    }
    echo '
    <a class="btn-floating btn-large btn-success btn-creacion" href="gestion.php'.$parametro.'" title="Nuevo registro"><i class="icon-plus"></i></a>';
    if($eliminar){
    echo '
        <a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
    }
}

function creaBotonEspecial($examen='NO'){
    if($examen=='NO'){
        echo '<a class="btn-floating btn-large btn-warning btn-creacion2" href="index.php?filtro=EXAMEN" title="Registro de informes de examen especial"><i class="icon-exclamation"></i></a>';
        echo '<a class="btn-floating btn-large btn-inverse btn-creacion3" href="index.php?filtro=RENOVACION" title="Informes por renovar"><i class="icon-refresh"></i></a>';
    } else {
        echo '<a id="btnVolver" class="btn-floating btn-large btn-info btn-creacion2" href="index.php" title="Volver"><i class="icon-arrow-left"></i></a>';
    }
}

//Fin parte común para los listados con filtros personalizados

//Parte común de los selectpicker con AJAX

function campoSelectConsultaAjax($nombreCampo,$texto,$consulta,$valor=false,$enlace='',$clase='selectpicker selectAjax span2 show-tick',$disabled='',$tipo=0,$conexion=true){
    global $_CONFIG;

    $valor=compruebaValorCampo($valor,$nombreCampo);
    if($disabled){
        $disabled="disabled='disabled'";
    }

    if($tipo==0){
        echo "
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls nowrap'>";
    }
    elseif($tipo==1){
        echo "<td>";
    }

    echo "<select name='$nombreCampo' consulta=\"$consulta\" class='$clase' id='$nombreCampo' $disabled data-live-search='true'>
            <option value='NULL'></option>";
        
    $consulta=consultaBD($consulta,$conexion);
    while($datos=mysql_fetch_assoc($consulta)){

        if($valor!=false && $valor==$datos['codigo']){//Por defecto solo se muestra el valor seleccionado, si existiese
            echo "<option value='".$datos['codigo']."' selected='selected' >".$datos['texto']."</option>";
        }
    }
        
    echo "</select>";

    if(trim($enlace)!=''){
        echo " <a enlace='".$_CONFIG['raiz']."$enlace' href='#' class='btn btn-primary btn-small botonSelectAjax noAjax' target='_blank' id='boton-$nombreCampo'><i class='icon-plus'></i></a>";
    }

    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "</td>";
    }
}

function cargaBusquedaSelectAjax(){
    $consulta=$_POST['consulta'];
    $busqueda=$_POST['busqueda'];

    $partesConsulta=explode('ORDER BY',$consulta);
    if(isset($partesConsulta[1])){//La consulta está ordenada...
        $consulta=$partesConsulta[0]." HAVING texto LIKE '%$busqueda%' ORDER BY ".$partesConsulta[1];
    }
    else{
        $consulta=$partesConsulta[0]." HAVING texto LIKE '%$busqueda%'";//Uso HAVING para utilizar el alias del campo a filtrar
    }
    
    $res="<option value='NULL'></option>";
    $consulta=consultaBD($consulta,true);
    while($datos=mysql_fetch_assoc($consulta)){
        $res.="<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
    }

    echo $res;
}

//Fin parte común de los selectpicker con AJAX

//Parte común de participantes e inicios de grupos

function creaTutoriasInicioGrupo($codigoGrupo,$datos){
    $res=true;
    $trabajadores=array();

    $fechaSiguiente=obtieneFechaSiguienteLlamada($datos['codigoAccionFormativa'],$datos['fechaInicio'],$datos['fechaFin']);

    //if(isset($datos['yaTramitado']) && $datos['yaTramitado']=='NO'){
        for($i=0;isset($datos['codigoCliente'.$i]);$i++){
            for($j=0;isset($datos['codigoTrabajador'.$i.'-'.$j]);$j++){
                $codigoTrabajador=$datos['codigoTrabajador'.$i.'-'.$j];

                if(compruebaTutoriaTrabajador($codigoGrupo,$codigoTrabajador)){
                    $res=$res && consultaBD("INSERT INTO tutorias VALUES(NULL,$codigoGrupo,$codigoTrabajador,'NO','','SI','NO','NO','$fechaSiguiente','NO','0','0','0','0','SI');");
                }

                array_push($trabajadores,$codigoTrabajador);
            }
        }

        $res=consultaBD("DELETE FROM tutorias WHERE codigoGrupo=$codigoGrupo AND codigoTrabajador NOT IN(".implode(',',$trabajadores).");");

        //$res=$res && consultaBD("UPDATE grupos SET yaTramitado='SI' WHERE codigo=$codigoGrupo;");
    //}

    return $res;
}


function obtieneFechaSiguienteLlamada($codigoAccion,$fechaInicio,$fechaFin){
    $res=$fechaInicio;

    if($codigoAccion!='NULL'){
        $accion=consultaBD("SELECT modalidad FROM acciones_formativas WHERE codigo='$codigoAccion';");
        if($accion['modalidad']=='PRESENCIAL'){
            $res=$fechaFin;
        }
    }

    return $res;
}

function compruebaTutoriaTrabajador($codigoGrupo,$codigoTrabajador){
    $res=true;

    $consulta=consultaBD("SELECT codigo FROM tutorias WHERE codigoGrupo=$codigoGrupo AND codigoTrabajador=$codigoTrabajador");
    if(mysql_num_rows($consulta)>0){
        $res=false;
    }

    return $res;
}

//Campos formulario de Servicios

function completarDatosLOPD($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	$formulario['pregunta3'] = $formulario['pregunta3'] == '' ? $cliente['razonSocial'] : $formulario['pregunta3']; 
	$formulario['pregunta4'] = $formulario['pregunta4'] == '' ? $cliente['domicilio'] : $formulario['pregunta4']; 
	$formulario['pregunta5'] = $formulario['pregunta5'] == '' ? $cliente['localidad'] : $formulario['pregunta5']; 
	$formulario['pregunta6'] = $formulario['pregunta6'] == '' ? $cliente['telefono'] : $formulario['pregunta6']; 
	$formulario['pregunta7'] = $formulario['pregunta7'] == '' ? $cliente['email'] : $formulario['pregunta7']; 
	$formulario['pregunta8'] = $formulario['pregunta8'] == '' ? $cliente['administrador'] : $formulario['pregunta8']; 
	$formulario['pregunta627'] = !isset($formulario['pregunta627']) || $formulario['pregunta627'] == '' ? $cliente['apellido1'] : $formulario['pregunta627']; 
	$formulario['pregunta628'] = !isset($formulario['pregunta628']) || $formulario['pregunta628'] == '' ? $cliente['apellido2'] : $formulario['pregunta628']; 
	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['cif'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['cp'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? convertirMinuscula($cliente['provincia']) : convertirMinuscula($formulario['pregunta11']); 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['fax'] : $formulario['pregunta12']; 
	$formulario['pregunta13'] = $formulario['pregunta13'] == '' ? $cliente['actividad'] : $formulario['pregunta13']; 
	$formulario['pregunta14'] = $formulario['pregunta14'] == '' ? $cliente['nifAdministrador'] : $formulario['pregunta14']; 

	return $formulario;
}

function recogerFormularioServicios($trabajo){

    $formulario = split('&{}&', $trabajo['formulario']);
    $preguntas = array();

    if(count($formulario) > 1){
        foreach ($formulario as $pregunta){
            $parts=split('=>', $pregunta);
            $preguntas[$parts[0]] = $parts[1];
        }
    } else {
        for($i=1;$i<600;$i++){
            $preguntas['pregunta'.$i] = '';
        }
    }

    return $preguntas;
    
}

function recogerTrabajo($cliente,$servicio){
    $servicio=consultaBD("SELECT * FROM servicios WHERE codigo='".$servicio."'",true,true);
    $trabajo = consultaBD("SELECT * FROM trabajos WHERE codigoCliente=".$cliente." AND codigoServicio=".$servicio['codigo'],true,true);
    return $trabajo;
}

function campoTextoFormulario($numero,$indice,$texto,$valor='',$clase='input-large',$readonly=false,$tipo=0){
    campoTexto('pregunta'.$indice.$numero,$texto,$valor['pregunta'.$numero],$clase,$readonly,$tipo);
}

function campoTextoFormularioConLogo($numero,$indice,$texto,$valor='',$logo='',$clase='input-large',$readonly=false,$tipo=0){
    echo "";
    campoTexto('pregunta'.$indice.$numero,$texto,$valor['pregunta'.$numero],$clase,$readonly,$tipo);
    echo "</div>";
}

function campoTextoTablaFormulario($numero,$indice,$valor='',$clase='input-large',$disabled=false){
    campoTextoTabla('pregunta'.$indice.$numero,$valor['pregunta'.$numero],$clase,$disabled);
}


function campoFechaFormulario($numero,$indice,$texto,$valor=false,$solo=false,$disabled=false){//Modificada por Jose Luis el 15/09/2014 (añadido opción disabled).
    if(!$valor){
        //$valor=fecha();
    }
    else{
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }

    if($solo){
        campoTextoSolo('pregunta'.$indice.$numero,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
    else{
        campoTexto('pregunta'.$indice.$numero,$texto,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
}

function campoFechaTablaFormulario($numero,$indice,$valor=false){
    echo "<td>";
    campoFechaFormulario($numero,$indice,'',$valor,true);
    echo "</td>";
}

function campoRadioFormulario($numero,$indice,$texto,$valor='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){
    if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }
    echo "
    <div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$indice."".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function campoRadioEncargado($numero,$indice,$texto,$valor='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){
    if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }
    echo "
    <div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls nowrap radioEncargado'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$indice."".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function campoRadioFormularioConLogo($numero,$indice,$texto,$valor='',$logo='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){
     if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }

    echo "
    <div class='control-group'>                     
      <label class='control-label' style='background:url(../img/".$logo.") 95% bottom no-repeat;padding-right:50px;'>$texto:</label>
      <div class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$indice."".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function campoRadioTablaFormulario($numero,$indice,$valor='',$valorPorDefecto='NO',$textosCampos=array('Si','No'),$valoresCampos=array('SI','NO'),$salto=false){//MODIFICACIÓN 09/04/2015 NO RETROCOMPATIBLE: añadido el parámetro $valorPorDefecto | MODIFICACIÓN 05/01/2015: añadido el parámetro $salto | MODIFICACIÓN 21/07/2014 DE OFICINA: $valor antes que textos. $campos, $textosCampos y $valoresCampos arrays con misma longitud
     if($valor['pregunta'.$numero]!=''){
        $valor=compruebaValorCampo($valor,'pregunta'.$numero);
    }
    else{
        $valor=$valorPorDefecto;
    }

    echo "
      <td class='controls nowrap'>";//MODIFICACIÓN 29/06/2015: añadida la clase "nowrap"

    for($i=0;$i<count($textosCampos);$i++){
        echo "<label class='radio inline'>
                <input type='radio' name='pregunta".$indice."".$numero."' value='".$valoresCampos[$i]."'";
        if($valoresCampos[$i]==$valor){
            echo " checked='checked'";
        } 
        echo ">".$textosCampos[$i]."</label>";
        if($salto){
            echo "<br />";
        }
    }
    echo "
      </td> <!-- /controls -->";
}

function areaTextoFormulario($numero,$indice,$texto,$valor='',$clase='areaTexto',$bloqueado=false){
    areaTexto('pregunta'.$indice.$numero,$texto,$valor['pregunta'.$numero],$clase,$bloqueado);
}

function areaTextoTablaFormulario($numero,$indice,$valor='',$clase='',$bloqueado=false){
    areaTextoTabla('pregunta'.$indice.$numero,$valor['pregunta'.$numero],$clase,$bloqueado);
}

function campoFirmaFormulario($numero,$indice,$texto,$valor){
    campoFirma($valor['pregunta'.$numero],'pregunta'.$indice.$numero,$texto);
}

function campoSelectFormulario($numero,$indice,$texto='',$valor=false,$tipo=0,$nombres=array('Si','NO'),$valores=array('SI','NO'),$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'"){
    if($tipo == 1){
        $clase=' selectpicker span2 show-tick';
    }
    campoSelect('pregunta'.$indice.$numero,$texto,$nombres,$valores,$valor['pregunta'.$numero],$clase,$busqueda,$tipo);
}

function campoSelectMultipleFormulario($numero,$indice,$texto='',$nombres=array(),$valores=array(),$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0){
    if($tipo == 1){
        $clase=' selectpicker span2 show-tick';
    }
    campoSelectMultiple('pregunta'.$indice.$numero,$texto,$nombres,$valores,$valor['pregunta'.$numero],$clase,$busqueda,$tipo);
}

function campoSelectMultipleFormularioFicheros($numero,$indice,$texto='',$nombres=array(),$valores=array(),$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0,$cliente){

    $opciones=array();
    $valores=array();

    conexionBD();
    $consulta=consultaBD("SELECT codigo, nombreFichero FROM declaraciones WHERE codigoCliente='$cliente' AND (fechaBaja='0000-00-00' OR fechaBaja>'".date('Y-m-d')."');");
    while($datosFicheros=mysql_fetch_assoc($consulta)){
        array_push($opciones, $datosFicheros['nombreFichero']);
        array_push($valores, $datosFicheros['codigo']);        
    }
    cierraBD();

    if($tipo == 1){
        $clase=' selectpicker span2 show-tick';
    }
    campoSelectMultiple('pregunta'.$indice.$numero,$texto,$opciones,$valores,$valor['pregunta'.$numero],$clase,$busqueda,$tipo);
}

function campoSelectProvinciaFormulario($numero,$indice,$datos){
    $valores=array();
    $nombres=array();
    $provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
    
    foreach ($provinciasAgencia as $codigo => $nombre) {
        array_push($valores,$codigo);
        array_push($nombres,convertirMinuscula($nombre));
    }
    $datos['pregunta'.$numero]=isset($datos['pregunta'.$numero])?$datos['pregunta'.$numero]:'';
    campoSelectFormulario($numero,$indice,'Provincia',$datos,0,$nombres,$valores);
}

//Parte de clientes y posibles clientes

function creaTablaCuentasSSCliente($datos){
    echo "
    <div class='control-group'>                     
        <label class='control-label'>Cuenta/s Seg. Social:</label>
        <div class='controls'>
            <table class='table table-striped table-bordered mitadAncho' id='tablaCuentaSS'>
                <thead>
                    <tr>
                        <th> Nº Cuenta </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>";
        
            $i=0;

            if($datos!=false){
                $consulta=consultaBD("SELECT cuentaSS FROM cuentas_ss_clientes WHERE codigoCliente='".$datos['codigo']."'",true);
                while($datosC=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaCuentasSS($i,$datosC);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaCuentasSS(0,false);
            }
      
    echo "      </tbody>
            </table>
            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCuentaSS\");'><i class='icon-plus'></i> Añadir cuenta</button> 
            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCuentaSS\");'><i class='icon-trash'></i> Eliminar cuenta</button>
        </div>
    </div>";
}

function imprimeLineaTablaCuentasSS($i,$datos){
    $j=$i+1;

    echo "<tr>";
        campoTextoValidador('cuentaSS'.$i,'',$datos['cuentaSS'],'input-large pagination-right cuentaSS','cuentas_ss_clientes','codigoCliente',false,2);//La clase CSS cuentaSS no tiene efecto estético, la utilizo como selector para validar vía JS
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function camposSeguimientoCliente($datos){
    echo '<h3 class="apartadoFormulario">Seguimiento</h3>';
    
    cierraColumnaCampos();
    abreColumnaCampos('span3 camposSeguimiento');

    if($datos){
        campoRadio('procedeDeListado','Procede de listado',$datos);
        echo "<div class='hide' id='cajaProcedencia'>";
        campoSelect('procedenciaListado','Procedencia del listado',array('','Colaborador/Asesor 1º Nivel','Colaborador/Asesor 2º Nivel','Puerta Fría','Base de datos comprada','Cartera comercial','Histórico Cartera Academia','Otros'),array('','COLABORADOR1','COLABORADOR2','PUERTAFRIA','BASEDATOS','CARTERA','HISTORICO','OTROS'),$datos);
        echo "<div class='hide' id='cajaTextoProcedencia'>";
        campoTexto('textoProcedencia','Nombre comercial',$datos);
        echo "</div></div>";
    }
    else{
        campoOculto('NO','procedeDeListado');
        campoOculto('','procedenciaListado');
        campoOculto('','textoProcedencia');
    }

    campoRadio('llamado','Llamado',$datos);
    campoRadio('visita','Visita',$datos,'SI',array('Si','Seguimiento','No'),array('SI','SEGUIMIENTO','NO'));

    cierraColumnaCampos();
    abreColumnaCampos('span3 camposSeguimiento');

    campoRadio('resultadoVisita','Resultado Visita',$datos,'PREVENTA',array('Preventa','Seguimiento','No'),array('PREVENTA','SEGUIMIENTO','NO'));
    campoRadio('resultadoPreventa','Resultado Preventa',$datos,'OK',array('Ok','Faltan Datos','No'),array('OK','FALTANDATOS','NO'));
    echo '<br /><br />';

    cierraColumnaCampos();
    abreColumnaCampos('sinFlotar');//Se cierra en el formulario de la función de gestión, de forma que estás columnas quedan de forma transparente a ella    
}

function campoSelectContactoAsesoria($datos){
    if($datos && $datos['codigoAsesoria']!=NULL){
        campoSelectConsulta('codigoContactoAsesoria','Contacto de asesoría',"SELECT codigo, contacto AS texto FROM contactos_asesorias WHERE codigoAsesoria=".$datos['codigoAsesoria'],$datos);
    }
    else{
        campoSelect('codigoContactoAsesoria','Contacto de asesoría',array(''),array('NULL'),false);
    }
}

function obtieneContactosAsesoria(){
    $codigoAsesoria=$_POST['codigoAsesoria'];
    $res="<option value='NULL'>&nbsp;</option>";

    $consulta=consultaBD("SELECT codigo, contacto AS texto FROM contactos_asesorias WHERE codigoAsesoria=$codigoAsesoria",true);
    if($consulta){
        while($datos=mysql_fetch_assoc($consulta)){
            $res.="<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
        }
    }
    else{
        $res='fallo';
    }

    echo $res;
}  

function creaTablaCreditosFormacion($datos){
    echo "
    <h3 class='apartadoFormulario'>Créditos para formación</h3>
    <div class='centro'>
        <table class='table table-striped tabla-simple' id='tablaCreditos'>
          <thead>
            <tr>
              <th> Ejercicio </th>
              <th> Plantilla </th>
              <th> Crédito </th>
              <th> En otras empresas </th>
              <th> Form. Tramitada </th>
              <th> Form. Bonificada </th>
              <th> Crédito disponible </th>
              <th> Horas PIF </th>
              <th> Fecha firma </th>
              <th> Validado </th>
              <th> Agrupación </th>
              <th> </th>
            </tr>
          </thead>
          <tbody>";
        
            $i=0;

            if($datos!=false){
                $consulta=consultaBD("SELECT * FROM creditos_cliente WHERE codigoCliente='".$datos['codigo']."'",true);
                while($datosP=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaCreditos($i,$datosP);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaCreditos(0,false);
            }
      
    echo "</tbody>
        </table>
        <button type='button' class='btn btn-small btn-success' onclick='insertaFilaCreditos();'><i class='icon-plus'></i> Añadir crédito</button> 
        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCreditos\");'><i class='icon-trash'></i> Eliminar crédito</button>
    </div>
    <br />";
}

function imprimeLineaTablaCreditos($i,$datos){
    $j=$i+1;

    echo "<tr>";
        campoTextoTabla('ejercicio'.$i,$datos['ejercicio'],'input-mini2 pagination-right');
        campoTextoTabla('plantilla'.$i,$datos['plantilla'],'input-mini2 pagination-right');
        campoTextoSimbolo('credito'.$i,'','€',formateaNumeroCredito($datos['credito'],true),'input-mini pagination-right calculo',1);
        campoTextoSimbolo('creditoOtras'.$i,'','€',formateaNumeroCredito($datos['creditoOtras'],true),'input-mini pagination-right calculo',1);
        campoTextoSimbolo('formacionTramitada'.$i,'','€',formateaNumeroCredito($datos['formacionTramitada'],true),'input-mini pagination-right calculo',1);
        campoTextoSimbolo('formacionBonificada'.$i,'','€',formateaNumeroCredito($datos['formacionBonificada'],true),'input-mini pagination-right calculo',1);
        campoTextoSimbolo('creditoDisponible'.$i,'','€',formateaNumeroCredito($datos['creditoDisponible'],true),'input-mini pagination-right',1);
        campoTextoTabla('horasPIF'.$i,$datos['horasPIF'],'input-mini2 pagination-right');
        campoTextoTabla('fechaFirma'.$i,formateaFechaWeb($datos['fechaFirma']),'input-small campoFecha',true);
        campoCheckTabla('checkValidado'.$i,$datos['checkValidado']);
        campoSelectConsulta('codigoAgrupacion'.$i,'',"SELECT codigo, agrupacion AS texto FROM agrupaciones WHERE activo='SI' ORDER BY agrupacion;",$datos['codigoAgrupacion'],'selectpicker span2 show-tick','data-live-search="true"','',1,false);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}



function insertaCreditosCliente($datos,$codigoCliente,$actualizacion=false){
    $res=eliminaDatosCliente('creditos_cliente',$codigoCliente,$actualizacion);

    for($i=0;isset($datos['ejercicio'.$i]);$i++){
        $ejercicio=$datos['ejercicio'.$i];
        $plantilla=$datos['plantilla'.$i];
        $credito=formateaNumeroCredito($datos['credito'.$i]);
        $creditoOtras=formateaNumeroCredito($datos['creditoOtras'.$i]);
        $formacionTramitada=formateaNumeroCredito($datos['formacionTramitada'.$i]);
        $formacionBonificada=formateaNumeroCredito($datos['formacionBonificada'.$i]);
        $creditoDisponible=formateaNumeroCredito($datos['creditoDisponible'.$i]);
        $horasPIF=$datos['horasPIF'.$i];
        $fechaFirma=$datos['fechaFirma'.$i];
        $checkValidado=compruebaCheckCreditoValidado($datos,$i);
        $codigoAgrupacion=$datos['codigoAgrupacion'.$i];
        

        $res=$res && consultaBD("INSERT INTO creditos_cliente VALUES(NULL, '$ejercicio', '$plantilla', '$credito', '$creditoOtras', '$formacionTramitada', '$formacionBonificada', '$creditoDisponible', '$horasPIF', '$fechaFirma', '$checkValidado', $codigoCliente, $codigoAgrupacion);");
    }

    return $res;
}

function compruebaCheckCreditoValidado($datos,$i){
    if(isset($datos['checkValidado'.$i])){
        $res='SI';
    }
    else{
        $res='NO';
    }

    return $res;
}


function formateaNumeroCredito($numero,$web=false){
    $res=$numero;

    if($res!='' && !$web){
        $res=formateaNumeroWeb($res,true);
    }
    elseif($res!='' && $web){
        $res=formateaNumeroWeb($res);
    }

    return $res;
}

function ventanaCreacionAsesoria(){
    abreVentanaModal('Asesorías','cajaAsesoria');
    campoTexto('asesoria','Nombre asesoría',false,'span3');
    campoTextoSimboloValidador('telefonoPrincipal','Teléfono principal','<i class="icon-phone"></i>',false,'input-small','asesorias');
    campoTextoSimboloValidador('emailPrincipal','eMail principal','<i class="icon-envelope"></i>',false,'input-large','asesorias');
    areaTexto('observaciones','Observaciones');
    cierraVentanaModal('registraAsesoria');
}

function ventanaCreacionComercial(){
    abreVentanaModal('Comerciales','cajaComercial');
    campoTexto('nombre','Nombre',false,'span3');
    campoTextoValidador('dni','DNI',false,'input-small validaDNI','comerciales');
    campoSelectProvincia(false,'provincia');
    campoSelectConsulta('codigoCategoria','Categoría',"SELECT codigo, categoria AS texto FROM categorias_agentes WHERE activo='SI' ORDER BY categoria");
    campoSelectConsulta('codigoComercial','Depende del agente',"SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre");
    campoSelectConsulta('codigoUsuario','Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') AND activo='SI' ORDER BY nombre,apellidos");
    campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',false,'input-small','comerciales');
    campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',false,'input-small','comerciales');
    campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',false,'input-large','comerciales');
    cierraVentanaModal('registraComercial');
}

function ventanaCreacionColaborador(){
    abreVentanaModal('Colaboradores','cajaColaborador');
    campoTexto('nombre','Nombre/Razón Social',false,'span3');
    campoTextoValidador('cif','NIF/CIF',false,'input-small validaCIF','colaboradores');
    campoSelectProvincia(false,'provincia');
    campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',false,'input-small','colaboradores');
    campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',false,'input-large','colaboradores');
    cierraVentanaModal('registraColaborador');
}

function ventanaCreacionTelemarketing(){
    abreVentanaModal('Telemarketing','cajaTelemarketing');
    campoTexto('nombre','Nombre',false,'span3');
    campoTextoValidador('dni','DNI',false,'input-small validaDNI','telemarketing');
    campoSelectProvincia(false,'provincia');
    campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',false,'input-small','telemarketing');
    campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',false,'input-large','telemarketing');
    cierraVentanaModal('registraTelemarketing');
}

function ventanaCreacionConsultor(){
    abreVentanaModal('Consultor','cajaConsultor');
    campoTexto('nombre','Nombre',false,'span3');
    campoTexto('apellidos','Apellidos',false,'span3');
    campoTextoSimbolo('email','Email','<i class="icon-envelope"></i>',false,'span3');
    campoTexto('usuario','Usuario');
    campoClave('clave','Clave');
    cierraVentanaModal('registraConsultor');
}


function creaAsesoriaCliente(){
    $datos=arrayFormulario();

    creaRegistroExtraCliente("INSERT INTO asesorias(codigo,asesoria,telefonoPrincipal,emailPrincipal,observaciones,activo) VALUES(
    NULL,'".$datos['asesoria']."','".$datos['telefonoPrincipal']."','".$datos['emailPrincipal']."','".$datos['observaciones']."','SI');");
}

function creaComercialCliente(){
    $datos=arrayFormulario();
    
    creaRegistroExtraCliente("INSERT INTO comerciales(codigo,nombre,dni,provincia,codigoCategoria,codigoComercial,codigoUsuario,telefono,movil,email,activo)
    VALUES(NULL,'".$datos['nombre']."','".$datos['dni']."','".$datos['provincia']."',".$datos['codigoCategoria'].",".$datos['codigoComercial'].",
    ".$datos['codigoUsuario'].",'".$datos['telefono']."','".$datos['movil']."','".$datos['email']."','SI');");
}

function creaColaboradorCliente(){
    $datos=arrayFormulario();
    
    creaRegistroExtraCliente("INSERT INTO colaboradores(codigo,nombre,cif,provincia,telefono,email,activo)
    VALUES(NULL,'".$datos['nombre']."','".$datos['cif']."','".$datos['provincia']."','".$datos['telefono']."',
    '".$datos['email']."','SI');");
}

function creaTelemarketingCliente(){
    $datos=arrayFormulario();

    creaRegistroExtraCliente("INSERT INTO telemarketing(codigo,nombre,dni,provincia,telefono,email,activo)
    VALUES(NULL,'".$datos['nombre']."','".$datos['dni']."','".$datos['provincia']."','".$datos['telefono']."','".$datos['email']."','SI');");
}

function creaConsultorCliente(){
    $datos=arrayFormulario();

    creaRegistroExtraCliente("INSERT INTO usuarios(codigo,nombre,apellidos,email,usuario,clave,tipo) 
    VALUES(NULL,'".$datos['nombre']."','".$datos['apellidos']."','".$datos['email']."','".$datos['usuario']."','".$datos['clave']."','CONSULTOR');");
}

function creaRegistroExtraCliente($query){
    $res='fallo';

    conexionBD();
    
    $consulta=consultaBD($query);

    if($consulta){
        $res=mysql_insert_id();
    }

    cierraBD();

    echo $res;
}


function campoAsesoriaClientes($valor=false){
    global $_CONFIG;
    $whereActivo='';

    if($valor){
        $valor=compruebaValorCampo($valor,'codigoAsesoria');
    }
    else{
        $whereActivo="WHERE activo='SI'";
    }

    echo "
    <div class='control-group'>                     
        <label class='control-label' for='codigoAsesoria'>Asesoría:</label>
        <div class='controls nowrap'>

            <select name='codigoAsesoria' consulta=\"SELECT codigo, asesoria AS texto FROM asesorias WHERE activo='SI' ORDER BY asesoria;\" class='selectpicker selectAjax show-tick' id='codigoAsesoria' data-live-search='true'>
            <option value='NULL'></option>";
        
    $consulta=consultaBD("SELECT codigo, asesoria AS texto FROM asesorias $whereActivo ORDER BY asesoria;",true);
    while($datos=mysql_fetch_assoc($consulta)){

        if($valor!=false && $valor==$datos['codigo']){//Por defecto solo se muestra el valor seleccionado, si existiese
            echo "<option value='".$datos['codigo']."' selected='selected' >".$datos['texto']."</option>";
        }
    }
        
    echo "</select> 
          <a enlace='".$_CONFIG['raiz']."asesorias/gestion.php?codigo=' href='#' class='btn btn-primary btn-small botonSelectAjax noAjax' target='_blank' id='boton-codigoAsesoria'><i class='icon-external-link'></i></a>
          <button type='button' class='btn btn-primary btn-small botonSelectAjax2' id='crea-codigoAsesoria'><i class='icon-plus'></i></a>

        </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
    
}


function campoFirma($datos,$nombreCampo='firma',$texto='Firma para documentos',$campoFecha=true){
    echo "
    <div id='contenedor-$nombreCampo'>
        <h3 class='apartadoFormulario'>$texto</h3>";

    if($campoFecha){
        campoFecha('fechaFirmaDocumentos','Fecha firma',$datos);
    }

    $datos=compruebaValorCampo($datos,$nombreCampo);
    echo "
        <!-- Área de firma -->
        <div class='centro ".$nombreCampo."'>";
        campoOculto($datos,$nombreCampo,'','hide output');
        echo "
            <div class='clearButton'><a href='#clear' class='btn btn-danger noAjax'><i class='icon-trash'></i> Borrar</a></div>
            <canvas class='pad' width='1000' height='500'></canvas>
        </div>
        <!-- Fin ára de firma -->
        <br /><br />
    </div>";
}

function camposRLT($datos){
    echo "<div id='cajaRLT' class='hide'>";

            campoTexto('nombreRLT','Nombre RLT',$datos);
            campoTexto('nifRLT','NIF RLT',$datos,'input-small');
            campoTexto('numCentros','Número centros',$datos,'input-mini pagination-right');
            campoTexto('ubicacionCentros','Ubicación centros',$datos);

    echo "</div>";
}

function creaTablaEmisoresDocumentosCliente($datos){
    echo "
    <div class='control-group'>                     
        <label class='control-label'>Emisor/es de documentación:</label>
        <div class='controls'>
            <table class='table table-striped table-bordered mitadAncho' id='tablaEmisores'>
                <thead>
                    <tr>
                        <th> Emisor </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>";
        
            $i=0;

            conexionBD();
            if($datos!=false){
                $consulta=consultaBD("SELECT codigoEmisor FROM emisores_documentos_cliente WHERE codigoCliente=".$datos['codigo']);
                while($datosC=mysql_fetch_assoc($consulta)){
                    imprimeLineaTablaEmisoresDocumentosCliente($i,$datosC);
                    $i++;
                }
            }
            
            if($i==0){
                imprimeLineaTablaEmisoresDocumentosCliente(0,false);
            }
            cierraBD();
      
    echo "      </tbody>
            </table>
            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmisores\");'><i class='icon-plus'></i> Añadir emisor</button> 
            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaEmisores\");'><i class='icon-trash'></i> Eliminar emisor</button>
        </div>
    </div>";
}

function imprimeLineaTablaEmisoresDocumentosCliente($i,$datos){
    $j=$i+1;

    echo "<tr>";
          campoSelectConsulta('codigoEmisor'.$i,'','SELECT codigo, razonSocial AS texto FROM emisores ORDER BY razonSocial',$datos['codigoEmisor'],'selectpicker span3 show-tick','data-live-search="true"','',1,false);
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function insertaEmisoresDocumentacionCliente($datos,$codigoCliente,$actualizacion=false){
    $res=eliminaDatosCliente('emisores_documentos_cliente',$codigoCliente,$actualizacion);

    for($i=0;isset($datos['codigoEmisor'.$i]);$i++){
        $codigoEmisor=$datos['codigoEmisor'.$i];
        

        $res=$res && consultaBD("INSERT INTO emisores_documentos_cliente VALUES(NULL, $codigoEmisor, $codigoCliente);");
    }

    return $res;
}

function eliminaDatosCliente($tabla,$codigoCliente,$actualizacion){
    $res=true;

    if($actualizacion){
        $res=consultaBD("DELETE FROM $tabla WHERE codigoCliente=$codigoCliente");
    }

    return $res;
}


function creaVentanaEmisorDescargaDocumentos(){
    abreVentanaModal('Documentos','cajaDocumentos');
    campoOculto('','url');
    campoOculto(date('Y'),'anioActual');
    
    campoSelect('codigoEmisor','Emisor',array(''),array('NULL'));
    campoTexto('ejercicioDocumento','Ejercicio','','input-mini pagination-right');
    cierraVentanaModal('descargaDocumento','Descargar','icon-cloud-download');
}

function creaVentanaImportarFichero(){
    abreVentanaModalConFichero('Fichero','cajaFichero');
    campoOculto('','codigoClienteImportar');
    campoFichero('importado','Fichero');
    echo 'Listado de ficheros subidos';
    echo '<ul id="ulFicheros">';
    echo '<li>No hay fichero subidos para este cliente</li>';
    echo '</ul>';
    cierraVentanaModal('importaFichero','Importar','icon-upload');
}

function obtieneEmisoresCliente(){
    $res="<option value='NULL'></option>";
    $codigoCliente=$_POST['codigoCliente'];
    $url=$_POST['url'];

    $consulta=consultaBD("SELECT emisores.codigo, razonSocial, cif FROM emisores INNER JOIN emisores_documentos_cliente ON emisores.codigo=emisores_documentos_cliente.codigoEmisor WHERE codigoCliente=$codigoCliente",true);
    while($datos=mysql_fetch_assoc($consulta)){
        if(substr_count($url,'generaSepa')==1){//Si el documento a generar es el SEPA, muestra todos los emisores asociados al cliente
            $res.="<option value='".$datos['codigo']."'>".$datos['razonSocial']."</option>";
        }
        elseif($datos['cif']!='B75010686'){//Si el documento a generar es otro, no muestra el emisor con CIF B75010686
            $res.="<option value='".$datos['codigo']."'>".$datos['razonSocial']."</option>";   
        }
    }

    echo $res;
}

function creaTablaComercialesCliente($datos){
    if($_SESSION['tipoUsuario']=='COMERCIAL'){
        campoSelectComercial($datos,true,'codigoComercial0');//Al ser el perfil COMERCIAL, esta función dibujará un campoDato normal y se omitirá la tabla de clientes (para que no vean más de la cuenta...)
        campoOculto(date('Y'),'anio0');
        campoOculto('','observacionesComercial0');
    }
    else{
        echo "
        <h3 class='apartadoFormulario'>Comerciales asignados</h3>
        <div class='centro'>
            <table class='table table-striped tabla-simple mitadAncho' id='tablaComerciales'>
              <thead>
                <tr>
                  <th> Comercial </th>
                  <th> Año </th>
                  <th> Observaciones </th>
                  <th> </th>
                </tr>
              </thead>
              <tbody>";

                $i=0;

                conexionBD();

                if($datos!=false){
                    $consulta=consultaBD("SELECT * FROM comerciales_cliente WHERE codigoCliente='".$datos['codigo']."' ORDER BY anio DESC");
                    while($datosC=mysql_fetch_assoc($consulta)){
                        imprimeLineaTablaComercialesCliente($i,$datosC);
                        $i++;
                    }
                }
                
                if($i==0){
                    imprimeLineaTablaComercialesCliente(0,false);
                }

                cierraBD();
          
        echo "</tbody>
            </table>
            <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaComerciales\");'><i class='icon-plus'></i> Añadir comercial</button> 
            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaComerciales\");'><i class='icon-trash'></i> Eliminar comercial</button>
        </div>";
    }
}

function imprimeLineaTablaComercialesCliente($i,$datos){
    $j=$i+1;

    echo "<tr>";
        campoSelectComercialTablaClientes($i,$datos);
        campoTextoTabla('anio'.$i,$datos['anio'],'input-mini pagination-right');
        campoTextoTabla('observacionesComercial'.$i,$datos['observacionesComercial'],'span3');
    echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
    </tr>";
}

function campoSelectComercialTablaClientes($i,$datos){
    if($datos){
        $query="SELECT codigo, nombre AS texto FROM comerciales ORDER BY nombre;";
    }
    else{
        $query=obtieneQuerySelectComercial($_SESSION['codigoU'],$_SESSION['tipoUsuario'],false);
    }

    campoSelectConsulta('codigoComercial'.$i,'',$query,$datos['codigoComercial'],'selectpicker span3 show-tick','data-live-search="true"','',1,false);
}

function insertaComercialesCliente($datos,$codigoCliente,$actualizacion=false){
    $res=true;

    if(!$actualizacion || ($actualizacion && $_SESSION['tipoUsuario']!='COMERCIAL')){//Si se actualiza un cliente con un usuario de tipo COMERCIAL, no se realizan gestiones en la tabla comerciales_cliente
        $res=eliminaDatosCliente('comerciales_cliente',$codigoCliente,$actualizacion);

        for($i=0;isset($datos['codigoComercial'.$i]);$i++){
            $codigoComercial=$datos['codigoComercial'.$i];
            $anio=$datos['anio'.$i];
            $observaciones=$datos['observacionesComercial'.$i];
            

            $res=$res && consultaBD("INSERT INTO comerciales_cliente VALUES(NULL, $codigoCliente, $codigoComercial, '$anio', '$observaciones');");
        }
    }

    return $res;
}

//Fin parte de clientes y posibles clientes

//Parte común de facturas y abonos

function insertaVencimientosFactura($datos,$codigoFactura){
    //Los registros de vencimientos_facturas no se pueden eliminar al actualizar de forma normal, pues están vinculados con las remesas

    $res=true;
    $datos=arrayFormulario();

    conexionBD();

    for($i=0;isset($datos["codigoVencimiento$i"]) || isset($datos["medioPago$i"]);$i++){
        if(isset($datos['codigoVencimiento'.$i]) && isset($datos['medioPago'.$i])){//Actualización
            $medioPago=$datos['medioPago'.$i];
            $fechaVencimiento=$datos['fechaVencimiento'.$i];
            $importeVencimiento=formateaNumeroWeb($datos['importeVencimiento'.$i],true);
            $estado=$datos['estado'.$i];
            $concepto=$datos['conceptoVencimiento'.$i];
            $fechaDevolucion=$datos['fechaDevolucion'.$i];
            $gastosDevolucion=formateaNumeroWeb($datos['gastosDevolucion'.$i],true);
            $llamadaAsesoria=compruebaValorCheckVencimiento('llamadaAsesoria'.$i,$datos);
            $llamadaEmpresa=compruebaValorCheckVencimiento('llamadaEmpresa'.$i,$datos);

            $res=$res && consultaBD("UPDATE vencimientos_facturas SET medioPago='$medioPago', fechaVencimiento='$fechaVencimiento', fechaDevolucion='$fechaDevolucion', gastosDevolucion='$gastosDevolucion', importe=$importeVencimiento, estado='$estado', concepto='$concepto' WHERE codigo=".$datos['codigoVencimiento'.$i].";");
        }
        elseif(!isset($datos['codigoVencimiento'.$i]) && isset($datos['medioPago'.$i])){//Inserción
            $medioPago=$datos['medioPago'.$i];
            $fechaVencimiento=$datos['fechaVencimiento'.$i];
            $importeVencimiento=formateaNumeroWeb($datos['importeVencimiento'.$i],true);
            $estado=$datos['estado'.$i];
            $concepto=$datos['conceptoVencimiento'.$i];
            $fechaDevolucion=$datos['fechaDevolucion'.$i];
            $gastosDevolucion=formateaNumeroWeb($datos['gastosDevolucion'.$i],true);
            $llamadaAsesoria=compruebaValorCheckVencimiento('llamadaAsesoria'.$i,$datos);
            $llamadaEmpresa=compruebaValorCheckVencimiento('llamadaEmpresa'.$i,$datos);

            $res=$res && consultaBD("INSERT INTO vencimientos_facturas VALUES(NULL,$codigoFactura,'$medioPago','$fechaVencimiento','$fechaDevolucion','$gastosDevolucion',$importeVencimiento,'$estado','$concepto','$llamadaAsesoria','$llamadaEmpresa','');");
        }
        else{//Eliminación
            $res=$res && consultaBD("DELETE FROM vencimientos_facturas WHERE codigo='".$datos['codigoVencimiento'.$i]."';");
        }
    }

    cierraBD();

    return $res;
}

function compruebaValorCheckVencimiento($indice,$datos){
    $res='NO';

    if(isset($datos[$indice])){
        $res=$datos[$indice];
    }

    return $res;
}

function revisaFechaVencimientos(){//Se llama en inicio.php para actualizar el estado de los vencimientos cuando se accede
    $hoy=fechaBD();

    conexionBD();    
    //Primero, actualizo las remesas y sus vencimientos asociados para que aparezcan como pagados. Luego, los vencimientos con fecha pasada y que queden pendientes los paso a "Impagados"
    if(!consultaBD("UPDATE vencimientos_facturas, remesas, vencimientos_en_remesas SET vencimientos_facturas.estado='DOMICILIADO', remesas.estado='DOMICILIADA' WHERE remesas.fechaVencimiento<='$hoy' && remesas.estado='PENDIENTE' AND vencimientos_facturas.codigo=vencimientos_en_remesas.codigoVencimiento AND vencimientos_en_remesas.codigoRemesa=remesas.codigo") ||
       !consultaBD("UPDATE vencimientos_facturas SET estado='IMPAGADO GESTIÓN DE COBRO' WHERE fechaVencimiento<='$hoy' AND estado='PENDIENTE PAGO';")){
        mensajeError('no se ha podido actualizar el estado de los recibos vencidos');
    }
    cierraBD();
}

//Fin parte común de facturas y abonos

//Parte de filtro por ejercicios

function cajaFiltroEjercicio(){
    defineFiltroEjercicio();
    
    echo '
    <ul class="nav pull-right cajaUsuario" id="cajaFiltroEjercicio" data-date="01-01-2016" data-date-format="dd-mm-yyyy">
        <li class="dropdown"  id="target-5"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-calendar"></i> Ejercicio: ';
            
            campoFiltroEjercicio();

    echo '</a>
        </li>
    </ul>';
}

function campoFiltroEjercicio(){
    $valor=obtieneEjercicioFiltro();
    campoTexto('filtroEjercicio','',$valor,'filtroEjercicio',true,2);
}

function obtieneEjercicioFiltro(){
    $res='Todos';

    if(isset($_SESSION['ejercicio'])){
        $res=$_SESSION['ejercicio'];
    }

    return $res;
}

function obtieneEjercicioParaWhere(){//Similar a obtieneEjercicioFiltro(). Usar cuando se necesite poner el resultado directamente como condición para un campo fecha en un WHERE (en cuyo caso 'Todos' no valdría)
    $res=obtieneEjercicioFiltro();
    
    if($res=='Todos'){
        $res=date('Y');
    }

    return $res;
}

function defineFiltroEjercicio(){
    if(isset($_GET['ejercicio']) && $_GET['ejercicio']=='Todos'){
        unset($_SESSION['ejercicio']);
    }
    elseif(isset($_GET['ejercicio'])){
        $_SESSION['ejercicio']=$_GET['ejercicio'];
    }
}

//La siguiente función genera una condición WHERE/HAVING para todos los campos fecha de los listados
function obtieneWhereEjercicioListado($columnas,$listadoClientes){
    $cadenaColumnas=implode(',',$columnas);
    $res=' AND(';

    if(isset($_SESSION['ejercicio']) && strpos($cadenaColumnas,'fecha')){//Si se ha activado el filtro de ejercicios y el listado contiene fechas...
        $anio=$_SESSION['ejercicio'];
        $operador=defineOperadorFiltroEjercicios($listadoClientes);
        $fechasSinFiltro=array(     'fechaDevolucion',
                                    'trabajos.fechaTomaDatos',
                                    'trabajos.fechaRevision',
                                    'trabajos.fechaEnvio',
                                    'trabajos.fechaMantenimiento',
                                    'trabajos.fechaPrevista',
                                    'trabajos.fechaEmision',
                                    'trabajos.fechaEntrega',
                                    'fechaCobroListadoVentas',
                                    'fechaFacturaListadoVentas',
                                    'fechaInicio',
                                    'fechaFin',
                                    'clientes.fechaAlta',
                                    'fechaAltaUsuarioLOPD',
                                    'fechaBajaUsuarioLOPD',
                                    'fechasUsuarioLOPD'
                        );

        for($i=0,$filtradas=0;$i<count($columnas);$i++){


            if(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$fechasSinFiltro)){
                $res.="(YEAR(".$columnas[$i].")$operador $anio OR YEAR(".$columnas[$i].") IS NULL OR YEAR(".$columnas[$i].")=0) AND ";//...se aplica utilizando la función YEAR() de MySQL
                $filtradas++;
            }
        }

        if($filtradas>0 && isset($_GET['sSearch']) && $_GET['sSearch']!=''){//Si existe una búsqueda global, cierro la apertura del paréntesis y abro otro nuevo, porque en la búsqueda global se usa OR
            $res=quitaUltimaComa($res,5);
            $res.=') AND(';
        }
    }

    return $res;
}

function obtieneWhereEjercicioEstadisticas($campo,$estadisticasCliente=false){
    $res='';

    if(isset($_SESSION['ejercicio'])){//Si se ha activado el filtro de ejercicios...
        $operador=defineOperadorFiltroEjercicios($estadisticasCliente);
        $anio=$_SESSION['ejercicio'];

        $res=" AND ((YEAR($campo)$operador '$anio') OR (YEAR($campo) IS NULL) OR (YEAR($campo)=0))";
    }

    return $res;
}

function defineOperadorFiltroEjercicios($seccionClientes){
    $res='=';

    if($seccionClientes){
        $res='<=';
    }
    
    return $res;
}

function mensajeResultadoAdicional($indice,$res,$campo,$eliminacion=false){
    if(isset($_REQUEST[$indice])){
        if($res && $eliminacion){
            mensajeOk("Eliminación realizada correctamente."); 
        }
        elseif($res && !$eliminacion){
            if($_REQUEST[$indice] == 'Guardar'){
                mensajeOk("Datos de $campo guardados correctamente."); 
            } else {
                mensajeOk("Datos de $campo guardados correctamente y gestión finalizada."); 
            }
        }
        else{
          mensajeError("se ha producido un error al gestionar los datos. Compruebe la información introducida."); 
        }
    }
}

function cierraVentanaGestionAdicional($textoFinalizado,$destino,$columnas=false,$botonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left'){
    if($columnas){
        echo "        </fieldset>
                      <fieldset class='sinFlotar'>";
    }

    echo "
                        <br />                      
                        <div class='form-actions'>";
    if($botonVolver){
        echo "            <a href='$destino' class='btn btn-default'><i class='$iconoVolver'></i> $textoVolver</a>";
    }

    if($botonGuardar){                      
        echo "            <button type='button' name='Guardar' value='Guardar' class='btn btn-propio submit'><i class='$icono'></i> $texto</button>";
    }
    
        /*echo "            <button type='button' name='Finalizar' value='Finalizar' class='btn btn-propio submit'><i class='$icono'></i> $textoFinalizado</button>";*/


    echo "
                        </div> <!-- /form-actions -->
                      </fieldset>
                    </form>
                    </div>


                </div>
                <!-- /widget-content --> 
              </div>

          </div>
        </div>
        <!-- /container --> 
      </div>
      <!-- /main-inner --> 
    </div>
    <!-- /main -->";
}

//Fin parte de filtro por ejercicios

//Parte común de grupos y ventas

function campoSelectMultiple($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    campoOculto('NULL',$nombreCampo);
    if($tipo==0){
        echo "
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls'>";
    }
    elseif($tipo==1){
        echo "<td>";
    }
    
    if($valor != ''){
        $select=explode('&$&',$valor);
    } else {
        $select = '';
    }

    echo "<select multiple name='".$nombreCampo."[]' id='".$nombreCampo."' class='$clase selectMultiple' $busqueda>";
        
    for($i=0;$i<count($nombres);$i++){
        echo "<option value='".$valores[$i]."'";

        if($select!='' && in_array($valores[$i], $select)){
            echo " selected='selected'";
        }

        echo ">".$nombres[$i]."</option>";
    }
        
    echo "</select>";

    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "</td>";
    }
}

function campoSelectConsultaMultiple($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0,$cliente){

    $opciones=array();
    $valores=array();

    conexionBD();
    $consulta=consultaBD("SELECT codigo, nombreSoporteLOPD FROM soportes_lopd_nueva WHERE codigoCliente='$cliente';");
    while($datosSoportes=mysql_fetch_assoc($consulta)){
        array_push($opciones, $datosSoportes['nombreSoporteLOPD']);
        array_push($valores, $datosSoportes['codigo']);        
    }
    cierraBD(); 

    campoSelectMultiple($nombreCampo,$texto,$opciones,$valores,$valor,$clase,$busqueda,$tipo);
}

function campoSelectConsultaMultipleFicheros($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0,$cliente){

    $opciones=array();
    $valores=array();

    conexionBD();
    $consulta=consultaBD("SELECT codigo, nombreFichero FROM declaraciones WHERE codigoCliente='$cliente' AND (fechaBaja='0000-00-00' OR fechaBaja>'".date('Y-m-d')."');");
    while($datosFicheros=mysql_fetch_assoc($consulta)){
        array_push($opciones, $datosFicheros['nombreFichero']);
        array_push($valores, $datosFicheros['codigo']);        
    }
    cierraBD(); 

    campoSelectMultiple($nombreCampo,$texto,$opciones,$valores,$valor,$clase,$busqueda,$tipo);
}

function campoObservacionesParaFactura($datos,$nombreCampo='observacionesParaFactura'){
    if($_SESSION['tipoUsuario']!='COMERCIAL'){
        echo "<br /><h3 class='apartadoFormulario'>Observaciones para contratos</h3>";
        areaTexto($nombreCampo,'Observaciones',$datos,'observacionesParaFactura');
    }
    else{
        campoOculto($datos,$nombreCampo);
    }
}

//Fin parte común de grupos y ventas

//Parte de gestión de la BDD de Totara

function conexionTotara(){
    global $_CONFIG;

    $conexion=mysql_pconnect($_CONFIG['servidorTotara'],$_CONFIG['usuarioTotara'],$_CONFIG['claveTotara']);//MODIFICACIÓN 19/03/2015 EXPERIMENTAL: conexiones persistentes (innecesario el uso de cierraBD())
    if(!$conexion){ 
        echo "Error estableciendo la conexi&oacute;n a la BBDD.<br />";
    }
    else{
        if(!mysql_select_db($_CONFIG['nombreTotara'])){
            echo "Error seleccionando base de datos del software.<br />";
        }
    }

}

//Fin parte de gesttión de la BDD de Totara

//Parte común de ventas, preventas y trabajadores

function consultaDatosClienteVenta(){
    $codigoCliente=$_POST['codigoCliente'];

    conexionBD();
    $datos=consultaBD("SELECT codigoColaborador FROM clientes WHERE codigo=$codigoCliente",false,true);
    $comercial=consultaBD("SELECT codigoComercial FROM comerciales_cliente WHERE codigoCliente=$codigoCliente",false,true);
    $trabajo=consultaBD("SELECT * FROM trabajos WHERE codigoVenta IS NULL AND codigoCliente=".$codigoCliente,false,true);
    cierraBD();

    $datos['codigoComercial']=$comercial['codigoComercial'];
    $datos['codigoServicio']=$trabajo['codigoServicio'];

    echo json_encode($datos);
}

function insertaTrabajoDesdeVenta($estado,$cliente,$servicio,$venta,$fecha, $conexion = true){
    array_push($_SESSION['trabajos'], $servicio);
    $res = true;
    $fechaRevision      = obtieneFechaLimiteTrabajoVenta($fecha,'P21D');
    $fechaPrevista      = obtieneFechaLimiteTrabajoVenta($fechaRevision,'P51D');
    $fechaTomaDatos     = obtieneFechaLimiteTrabajoVenta($fecha,'P14D');
    $fechaEnvio         = obtieneFechaLimiteTrabajoVenta($fecha,'P28D');
    $fechaMantenimiento = obtieneFechaLimiteTrabajoVenta($fecha,'P11M');

    $servicios = array();
    
    $consulta = consultaBD('SELECT * FROM trabajos WHERE codigoVenta='.$venta, $conexion);
    
    while($serv = mysql_fetch_assoc($consulta)){
        array_push($servicios, $serv['codigoServicio']);
    }

    if(!in_array($servicio, $servicios)) {
        
        $trabajo = consultaBD('SELECT * FROM trabajos WHERE codigoVenta IS NULL AND codigoCliente='.$cliente, $conexion, true);
        
        if($trabajo){
            $res = $res && consultaBD('UPDATE trabajos SET codigoVenta="'.$venta.'" WHERE codigo='.$trabajo['codigo'], $conexion);
        } 
        else {
            $familia = consultaBD("SELECT * FROM servicios WHERE codigo = ".$servicio.";", $conexion, true);
            $trabajo = consultaBD('SELECT * FROM trabajos WHERE codigoCliente='.$cliente , $conexion,true);
            
            if($trabajo){
                $res = $res && consultaBD('UPDATE trabajos SET codigoServicio='.$servicio.' WHERE codigo='.$trabajo['codigo'], $conexion);
                $trabajo = $trabajo['codigo'];
            } 
            else {
                $res = consultaBD("INSERT INTO trabajos VALUES(NULL,'".$venta."','".$fecha."','".$fechaPrevista."',".$cliente.",'".$servicio."','','NO','NO','NO','NO','NO','NO','".$fechaTomaDatos."','".$fechaRevision."','".$fechaRevision."','".$fechaEnvio."','".$fechaEnvio."','".$fechaMantenimiento."','NO','NO','');", $conexion);
                $trabajo = mysql_insert_id();
            }
            
            $datos = consultaBD('SELECT * FROM clientes WHERE codigo='.$_POST['codigoCliente'], $conexion, true);
            
            if ($datos['pblc'] == 'SI') {
                $persona = $datos['checkAutonomo'] == 'SI' ? 'FISICA' : 'JURIDICA';
                $manual  = consultaBD('SELECT * FROM manuales_pbc WHERE codigoTrabajo='.$trabajo, $conexion, true);
                if(!$manual){
                    $res = consultaBD("INSERT INTO manuales_pbc(codigo,codigoTrabajo,fechaInicio,fechaFin,sujeto,actividad,servicios,empleados, volumen,tomo,folio,hoja,libro,seccion,representante,nifRepresentante) VALUES (NULL,".$trabajo.",'".$datos['fechaInicio']."','".$datos['fechaFin']."','".$persona."','".$datos['actividad']."','".$datos['servicios']."','".$datos['empleados']."','".$datos['volumen']."','".$datos['tomo']."','".$datos['folio']."','".$datos['hoja']."','".$datos['libro']."','".$datos['seccion']."','".$datos['administrador']." ".$datos['apellido1']." ".$datos['apellido2']."','".$datos['nifAdministrador']."')", $conexion);
                }
            }
        }
    }

    return $res;
}

function obtieneFechaLimiteTrabajoVenta($fecha,$suma){
    $fecha = new DateTime($fecha);
    $fecha->add(new DateInterval($suma));
    $fechaFinal = $fecha->format('d/m/Y');
    return $fecha->format('Y-m-d');
}


function campoSelectCuentaSSTrabajador($datos){
    if($datos && $datos['codigoCliente']!=NULL){
        campoSelectConsulta('cuentaSS','C. Cotización (empresa)',"SELECT cuentaSS AS codigo, cuentaSS AS texto FROM cuentas_ss_clientes WHERE codigoCliente=".$datos['codigoCliente'],$datos);
    }
    else{
        campoSelect('cuentaSS','C. Cotización (empresa)',array(),array());
    }
}

function campoFechaNacimiento($datos){
    if(!$datos){
        campoFecha('fechaNacimiento','Fecha nacimiento','0000-00-00');
    }
    else{
        campoFecha('fechaNacimiento','Fecha nacimiento',$datos);
    }
}

function campoSelectCategoriaProfesional($datos){
    $categoriasProfesionales=array('','1. DIRECTIVO','2. MANDO INTERMEDIO','3. TÉCNICO','4. TRABAJADOR CUALIFICADO','5. TRABAJADOR NO CUALIFICADO');
    campoSelect('categoriaProfesional','Categoría profesional',$categoriasProfesionales,$categoriasProfesionales,$datos);
}

function campoSelectGrupoCotizacion($datos){
    $grupos=array('','01. INGENIEROS Y LICENCIADOS','02. INGENIEROS TÉCNICOS, PERITOS Y AYUDANTES TITULADOS','03. JEFES ADMINISTRATIVOS Y DE TALLER','04. AYUDANTES NO TITULADOS','05. OFICIALES ADMINISTRATIVOS','06. SUBALTERNOS','07. AUXILIARES ADMINISTRATIVOS','08. OFICIALES DE PRIMERA Y SEGUNDA','09. OFICIALES DE TERCERA Y ESPECIALISTAS','10. TRABAJADORES MAYORES DE 18 AÑOS NO CUALIFICADOS','11. TRABAJADORES MENORES DE 18 AÑOS');
    campoSelect('grupoCotizacion','Grupo de cotización',$grupos,$grupos,$datos);
}

function campoSelectNivelEstudios($datos){
    $estudios=array('','01. MENOS QUE PRIMARIA','02. EDUCACIÓN PRIMARIA','03. PRIMERA ETAPA DE EDUCACIÓN SECUNDARIA (TÍTULO DE PRIMER Y SEGUNDO CICLO DE LA ESO, EGB, GRADUADO ESCOLAR, CERTIFICADOS DE PROFESIONALIDAD DE NIVEL 1 Y 2)','04. SEGUNDA ETAPA DE EDUCACIÓN SECUNDARIA (BACHILLERATO, FP DE GRADO MEDIO, BUP, FPI Y FPII)','05. EDUCACIÓN POSTSECUNDARIA NO SUPERIOR (CERTIFICADOS DE PROFESIONALIDAD DE NIVEL 3)','06. TÉCNICO SUPERIOR/FP GRADO SUPERIOR Y EQUIVALENTES','07. ESTUDIOS UNIVERSITARIOS DE 1º CICLO (DIPLOMATURA-GRADOS)','08. ESTUDIOS UNIVERSITARIOS DE 2º CICLO (LICENCIATURA-MÁSTER)','09. ESTUDIOS UNIVERSITARIOS DE 3º CICLO (DOCTORADO)','10. OTRAS TITULACIONES');
    campoSelect('nivelEstudios','Nivel de estudios',$estudios,$estudios,$datos,'selectpicker show-tick span4');
}

function consultaCuentaSSCliente(){
    $res="<option value=''></option>";

    $codigoCliente=$_POST['codigoCliente'];

    $consulta=consultaBD("SELECT cuentaSS FROM cuentas_ss_clientes WHERE codigoCliente=$codigoCliente",true);
    $numeroFilas=mysql_num_rows($consulta);

    while($datos=mysql_fetch_assoc($consulta)){
        $res.="<option value='".$datos['cuentaSS']."'";
        if($numeroFilas==1){
            $res.=" selected='selected'";
            $numeroFilas=0;
        }
        $res.=">".$datos['cuentaSS']."</option>";
    }
    
    echo $res;
}

function ventanaCreacionTrabajador(){
    abreVentanaModal('Trabajadores','cajaTrabajador','span3');
    
    campoTextoValidador('nombre','Nombre',false,'input-large obligatorio','trabajadores_cliente');
    campoTextoValidador('apellido1','Apellido 1',false,'input-large obligatorio','trabajadores_cliente');
    campoTextoValidador('apellido2','Apellido 2',false,'input-large','trabajadores_cliente');
    campoTextoValidador('nif','DNI',false,'input-small validaDNI obligatorio','trabajadores_cliente');
    campoTextoValidador('niss','NISS (alumno)',false,'input-small pagination-right','trabajadores_cliente');
    campoSelectCuentaSSTrabajador(false);
    campoFechaNacimiento(false);
    campoRadio('sexo','Sexo',false,'HOMBRE',array('Hombre','Mujer'),array('HOMBRE','MUJER'));
    campoRadio('discapacidad','Discapacidad');
    campoRadio('terrorismo','Víctima terrorismo');
    campoRadio('violencia','Víc. violencia género');
    campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',false,'input-large','trabajadores_cliente');

    cierraColumnaCampos();
    abreColumnaCampos();

    campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',false,'input-small','trabajadores_cliente');
    campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',false,'input-small','trabajadores_cliente');
    campoSelectCategoriaProfesional(false);
    campoSelectGrupoCotizacion(false);
    campoSelectNivelEstudios(false);
    campoTextoSimbolo('salarioBruto','Salario bruto anual','€',false,'input-small pagination-right');
    campoTexto('horasConvenio','Horas convenio',false,'input-mini pagination-right');
    campoTextoSimbolo('costeHora','Coste/hora','€',false,'input-small pagination-right');
    campoTexto('usuarioPlataforma','Usuario plataforma',false,'input-small');
    campoTexto('clavePlataforma','Contraseña');
    campoTexto('observacionesTrabajador','Observaciones','','span4');
    campoOculto('NULL','codigoClienteTrabajador');
    divOculto('0','indiceCliente');

    cierraVentanaModal('creaTrabajador');
}

function creaTrabajadorVenta(){
    $res='fallo';
    
    compruebaRadiosTrabajador();

    $insercion=insertaDatos('trabajadores_cliente');
    if($insercion){
        $res=$insercion;
    }

    echo $res;
}

function compruebaRadiosTrabajador(){
    $_POST['sexo']=compruebaCampoRadio('sexo');
    $_POST['discapacidad']=compruebaCampoRadio('discapacidad');
    $_POST['terrorismo']=compruebaCampoRadio('terrorismo');
    $_POST['violencia']=compruebaCampoRadio('violencia');
}

function creaAccionFormativaVenta(){
    $res='fallo';
    $accion=$_POST['accion'];

    conexionBD();

    $consulta=consultaBD("INSERT INTO acciones_formativas(codigo,codigoCentroGestor,accion,codigoCentroPresencial,privado,activo) VALUES(NULL,NULL,'$accion',NULL,'PRIVADA','SI');");
    
    if($consulta){
        $res=mysql_insert_id();
    }

    cierraBD();

    echo $res;
}


function campoTrabajadorVenta($i,$valor=false){
    global $_CONFIG;
    $whereActivo='';

    if($valor){
        $valor=compruebaValorCampo($valor,'codigoTrabajador');
    }
    else{
        $whereActivo="WHERE activo='SI'";
    }

    echo "
    <td>
        <select name='codigoTrabajador$i' consulta=\"SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI'\" class='selectpicker span6 selectAjax selectAlumno show-tick obligatorio' id='codigoTrabajador$i' data-live-search='true'>
            <option value='NULL'></option>";
        
    $consulta=consultaBD("SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI';");
    while($datos=mysql_fetch_assoc($consulta)){

        if($valor!=false && $valor==$datos['codigo']){//Por defecto solo se muestra el valor seleccionado, si existiese
            echo "<option value='".$datos['codigo']."' selected='selected' >".$datos['texto']."</option>";
        }
    }
        
    echo "</select> 
          <a enlace='".$_CONFIG['raiz']."trabajadores/gestion.php?codigo=' href='#' class='btn btn-primary btn-small botonSelectAjax noAjax' target='_blank' id='boton-codigoTrabajador$i'><i class='icon-external-link'></i></a>
          <button type='button' class='btn btn-primary btn-small botonSelectAjax2 crea-codigoTrabajador' id='crea-codigoTrabajador$i'><i class='icon-plus'></i></a>

        </td>";
}


function consultaPrecioComplemento(){
    $codigoComplemento=$_POST['codigoComplemento'];

    $datos=consultaBD("SELECT precio FROM complementos WHERE codigo=$codigoComplemento;",true,true);

    echo formateaNumeroWeb($datos['precio']);
}



function cierraVentanaGestionVentaFormacion($textoFinalizado,$destino,$columnas=false,$botonGuardar=true,$texto='Guardar',$icono='icon-check',$botonVolver=true,$textoVolver='Volver',$iconoVolver='icon-chevron-left'){
   if($columnas){
       echo "        </fieldset>
                     <fieldset class='sinFlotar'>";
   }

   echo "
                       <br />                      
                       <div class='form-actions'>";
   if($botonVolver){
       echo "            <a href='$destino' class='btn btn-default'><i class='$iconoVolver'></i> $textoVolver</a>";
   }

   if($botonGuardar){                      
       echo "            <button type='submit' id='guardar' class='btn btn-propio submit'><i class='$icono'></i> $texto</button>";
   }
   
       echo "            <!--button type='submit' id='guardarNuevo' class='btn btn-propio submit'><i class='$icono'></i> $textoFinalizado</button-->";


   echo "
                       </div> <!-- /form-actions -->
                     </fieldset>
                   </form>
                   </div>


               </div>
               <!-- /widget-content --> 
             </div>

         </div>
       </div>
       <!-- /container --> 
     </div>
     <!-- /main-inner --> 
   </div>
   <!-- /main -->";
}

//Fin parte común de ventas, preventas y trabajadores


//Parte común entre XML de Inicio y de Fin

function generaNombreXML($prefijo,$fecha,$hora,$agrupacion){
    $fecha=explode('-',$fecha);
    $hora=explode(':',$hora);

    $res=$prefijo.$fecha[0].$fecha[1].$fecha[2].'_'.$hora[0].$hora[1].'_'.$agrupacion.'.xml';

    return $res;
}

//Fin parte común entre XML de Inicio y de Fin

//Parte común de gestor documental y sus subsecciones

function compruebaUsuarioAdministrador(){
    $res=false;

    if($_SESSION['tipoUsuario']=='ADMIN'){
        $res=true;
    }

    return $res;
}

function celdaCabeceraAdministrador($texto='Franquiciado'){
    if(compruebaUsuarioAdministrador()){
        echo "<th> $texto </th>";
    }
}

//Fin parte común de gestor documental y sus subsecciones

//Parte común de importaciones

function compruebaFilaExcel($documento,$columnas,$fila){
    $res=false;
    $comprobacion=obtieneValorFilaExcel($documento,$columnas,$fila,false);

    if(str_replace("'","",$comprobacion)!=''){
        $res=true;
    }

    return $res;
}

function obtieneValorFilaExcel($documento,$columnas,$fila,$insercion=true){
    $res='';

    foreach($columnas as $columna){
        $celda=$columna.$fila;
        $res.=obtieneValorCeldaExcel($documento,$celda,$insercion);

        if($insercion){
            $res.=', ';
        }
    }

    return $res;
}


function obtieneValorCeldaExcel($documento,$celda,$escape=true){
    $res=trim($documento->getActiveSheet()->getCell($celda)->getValue());

    if($escape && $res!=''){
        $res="'".addslashes($res)."'";
    }
    elseif($res==''){
        $res="''";
    }

    return $res;
}


function obtieneValorCeldasCodigoExcel($documento,$columnas,$fila,$insercion=true){
    $res='';

    foreach($columnas as $columna){
        $celda=$columna.$fila;
        $res.=obtieneValorCeldaCodigoExcel($documento,$celda);

        if($insercion){
            $res.=', ';
        }
    }

    return $res;
}


function obtieneValorCeldaCodigoExcel($documento,$celda){
    $res=trim($documento->getActiveSheet()->getCell($celda)->getValue());

    if($res==''){
        $res="NULL";
    }

    return $res;
}

//Fin parte común de importaciones

//Parte común entre preventas y ventas

function creaTablaEmpresasVenta($datos){
    echo "  
    <br />
    <h3 class='apartadoFormulario apartadoGrupos'>Participantes</h3>
    <div class='table-responsive centro'>
        <table class='table tabla-simple tablaEmpresasVenta' id='tablaEmpresasParticipantes'>
            <thead>
                <tr>
                    <th class='cabecera1'> Empresa </th>
                    <th class='cabecera2'> Nº Participantes </th>
                    <th class='cabecera3'> Importe factura </th>
                    <th class='cabecera4'> </th>
                </tr>
            </thead>
            <tbody>";

                $i=0;
                conexionBD();
                if($datos){
                    $arrayFecha=explode('-',$datos['fechaAlta']);
                    $anio=$arrayFecha[0];

                    $consulta=consultaBD("SELECT clientes_grupo.*, plantilla, creditoDisponible 
                                          FROM clientes_grupo LEFT JOIN creditos_cliente ON clientes_grupo.codigoCliente=creditos_cliente.codigoCliente AND ejercicio='$anio'
                                          WHERE clientes_grupo.codigoCliente IS NOT NULL AND codigoGrupo=".$datos['codigo']);

                    while($datosParticipante=mysql_fetch_assoc($consulta)){
                        imprimeLineaTablaEmpresasVenta($datos['codigo'],$datosParticipante,$i);
                        $i++;
                    }
                }

                if($i==0){
                    imprimeLineaTablaEmpresasVenta(false,false,$i);
                }
                cierraBD();
    echo "                      
            </tbody>
        </table>
        <div class='centro'>
            <button type='button' class='btn btn-small btn-success' onclick='insertaParticipante();'><i class='icon-plus'></i> Añadir empresa</button> 
            <button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaParticipante(\"tablaEmpresasParticipantes\");'><i class='icon-trash'></i> Eliminar empresa</button>
        </div>
    </div>";

    //El siguiente div oculto sirve para guardar la consulta de selección de alumnos, que será obtenida y modificada en JS para filtrar por empresas.
    divOculto("SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI'",'consultaAlumnos');
}

function imprimeLineaTablaEmpresasVenta($codigoGrupo,$datos,$i){
    //$j=$i+1;//No utilizo $j para el check de eliminación porque el selector de eliminaFila contiene tbody, por lo que ya no se tiene en cuenta el tr de la cabecera

    echo "
    <tr>
        <td colspan='3' class='celdaConTabla'>
            <table class='table tabla-simple tabla-interna tablaPadre' id='tablaAlumnos$i'>
                <tbody>
                    <tr>
                        <td class='centro celdaCliente'>";
                            campoSelectClienteListadoFiltradoPorUsuario($i,$datos['codigoCliente'],2);
    echo "                  <br />
                            <button type='button' class='btn btn-small btn-success' onclick='insertaAlumno(\"tablaAlumnos$i\");'><i class='icon-plus'></i> Añadir alumno</button> 
                        </td>";

                        campoTextoTabla('numParticipantes'.$i,$datos['numParticipantes'],'input-mini pagination-right participantes');
                        campoTextoSimbolo('precioFactura'.$i,'','€',formateaNumeroWeb($datos['precioFactura']),'input-mini precioFactura pagination-right',1);//El campo precioFactura no tiene valor CSS, es para usarlo de selector en el JS
    echo "          </tr>";

                    creaSubTablaAlumnosVenta($codigoGrupo,$datos['codigoCliente'],$i);

    echo "

                </tbody>
            </table>
        </td>
        <td>";
            campoOculto($datos['precioBonificado'],'precioBonificadoAnterior'.$i);//Sirve para compararlo con el nuevo precio bonificado a la hora de enviar el formulario, y determinar así si hay que hacer una actualización del crédito disponible
    echo "
            <div id='plantilla$i' class='hide'>".$datos['plantilla']."</div>
            <div id='creditoDisponible$i' class='hide'>".$datos['creditoDisponible']."</div>
            <input type='checkbox' name='filasTabla[]' value='$i'>
         </td>
    </tr>";
}


function creaSubTablaAlumnosVenta($codigoGrupo,$codigoCliente,$i){
    $j=0;
    if($codigoCliente){
        $consulta=consultaBD("SELECT participantes.* FROM participantes INNER JOIN trabajadores_cliente ON participantes.codigoTrabajador=trabajadores_cliente.codigo WHERE codigoGrupo=$codigoGrupo AND codigoCliente=$codigoCliente");
        while($datosAlumno=mysql_fetch_assoc($consulta)){
            imprimeLineaSubtablaAlumnosVenta($datosAlumno,$j,$i);
            $j++;
        }
    }

    if($j==0){
        imprimeLineaSubtablaAlumnosVenta(false,$j,$i);
    }
}


function imprimeLineaSubtablaAlumnosVenta($datos,$j,$i){
    echo "<tr id='$i-$j' class='filaSubtabla'>
            <td class='celdaInvisible'></td>
            <td colspan='2' class='celdaConTabla'>
                <table class='table tabla-simple tabla-interna subtabla'>
                    <tbody>
                        <tr>";
                            campoTrabajadorVenta("$i-$j",$datos['codigoTrabajador']);
    echo "                  <td class='centro'><button type='button' onclick='eliminaAlumno(\"$i-$j\")' class='btn btn-small btn-danger' title='Eliminar alumno'><i class='icon-trash'></i></button></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>";
}

function campoSelectClienteListadoFiltradoPorUsuario($i,$valor,$tipo=1,$nombreCampo='codigoCliente'){
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='ADMINISTRACION2'){
        $query="SELECT clientes.codigo, razonSocial AS texto FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo WHERE clientes.activo='SI' AND codigoUsuario=$codigoUsuario GROUP BY clientes.codigo";
    }
    elseif($perfil=='COMERCIAL'){
        $query="SELECT clientes.codigo, razonSocial AS texto FROM clientes LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo WHERE clientes.activo='SI' AND codigoUsuarioAsociado=$codigoUsuario GROUP BY clientes.codigo";
    }
    elseif($perfil=='TELEMARKETING'){
        $query="SELECT clientes.codigo, razonSocial AS texto FROM clientes LEFT JOIN telemarketing ON clientes.codigoTelemarketing=telemarketing.codigo WHERE clientes.activo='SI' AND codigoUsuarioAsociado=$codigoUsuario";
    }
    elseif($perfil=='DELEGADO'){
        $query="SELECT clientes.codigo, razonSocial AS texto FROM clientes  LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente WHERE clientes.activo='SI' AND (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u2 ON c2.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario))) GROUP BY clientes.codigo";
    }
    elseif($perfil=='DIRECTOR'){
        $query="SELECT clientes.codigo, razonSocial AS texto FROM clientes  LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente WHERE clientes.activo='SI' AND (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u1 ON c2.codigoUsuarioAsociado=u1.codigo WHERE u1.codigo=$codigoUsuario)) OR comerciales.codigo IN(SELECT c3.codigoComercial FROM comerciales AS c3 WHERE c3.codigo IN(SELECT c4.codigo FROM comerciales AS c4 WHERE c4.codigoComercial IN(SELECT c5.codigo FROM comerciales AS c5 INNER JOIN usuarios AS u2 ON c5.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario)))) GROUP BY clientes.codigo";
    }
    else{
        $query="SELECT codigo, razonSocial AS texto FROM clientes WHERE activo='SI'";
    }

    campoSelectConsultaAjax($nombreCampo.$i,'',$query,$valor,'clientes/gestion.php?codigo=','selectpicker selectAjax span3 show-tick','',$tipo,false);
    divOculto($query,'consultaComplementos');//Este div oculto almacena la consulta ha realizar en el select de clientes de la tabla complementos, de forma que pueda modificarla para que solo se puedan seleccionar los clientes añadidos al grupo.
}



function obtieneClienteListadoVenta($datos){
    $res="<a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".wordwrap($datos['razonSocial'],15,'<br />')."</a>";

    if($datos['tipoVenta']=='FORMACION'){
        $res='';
        
        $consulta=consultaBD("SELECT clientes.codigo, razonSocial FROM clientes INNER JOIN clientes_grupo ON clientes.codigo=clientes_grupo.codigoCliente WHERE codigoGrupo=".$datos['codigo']." ORDER BY razonSocial");
        while($cliente=mysql_fetch_assoc($consulta)){
            $res.="<a href='../clientes/gestion.php?codigo=".$cliente['codigo']."' class='noAjax' target='_blank'>".wordwrap($cliente['razonSocial'],15,'<br />')."</a><br />";
        }

        $res=quitaUltimaComa($res,6);
    }

    return $res;
}

function campoFechaVenta($datos,$nombreCampo='fecha',$obligatorio=''){
    if($datos){
        $valor=formateaFechaWeb($datos[$nombreCampo]);
    }
    else{
        $valor=fecha();
    }

    campoTexto($nombreCampo,'Fecha de venta',$valor,"input-small fechaVenta $obligatorio");
}


function campoVencimientoVenta($datos){
    campoSelect('tipoVencimiento','Tipo de vencimiento',array('','A bonificación','Quincenas','Día 1 pasados dos meses','En la fecha'),array('','A BONIFICACIÓN','QUINCENAS','DÍA 1 PASADOS DOS MESES','EN LA FECHA'),$datos,'selectpicker span3 show-tick obligatorio');
}


function campoFechaVencimientoVenta($datos,$nombreCampo='fechaFacturacion'){
    echo "<div class='hide' id='cajaFechaFacturacion'>";
         campoFecha($nombreCampo,'Fecha vencimiento',$datos);
    echo "</div>";
}

function insertaEmpresasParticipantesVentaFormacion($codigoGrupo,$datos,$ventaFinal=false,$actualizacion=false){
    $res=true;
    $clientesAnteriores=array();
    $totalParticipantes=0;

    if($actualizacion){
        $clientesAnteriores=consultaArray("SELECT codigoCliente FROM clientes_grupo WHERE codigoGrupo=$codigoGrupo");
    }

    for($i=0;isset($datos['codigoCliente'.$i]);$i++){
        $codigoCliente=$datos['codigoCliente'.$i];
        $numParticipantes=$datos['numParticipantes'.$i];
        $precioFactura=formateaNumeroWeb($datos['precioFactura'.$i],true);

        if(in_array($codigoCliente, $clientesAnteriores)){
            $res=$res && consultaBD("UPDATE clientes_grupo SET numParticipantes='$numParticipantes', precioFactura='$precioFactura' WHERE codigoCliente=$codigoCliente AND codigoGrupo=$codigoGrupo;");
            unset($clientesAnteriores[array_search($codigoCliente,$clientesAnteriores)]);//Elimino el cliente actual del array
        }
        else{
            $res=$res && consultaBD("INSERT INTO clientes_grupo(codigo,codigoGrupo,codigoCliente,numParticipantes,precioFactura) VALUES(NULL,$codigoGrupo,$codigoCliente,'$numParticipantes',$precioFactura);");
        }

        if($ventaFinal){
            $res=$res && actualizaEstadoCliente($codigoCliente);
        }

        $totalParticipantes+=$numParticipantes;
    }

    $res=$res && consultaBD("UPDATE grupos SET numParticipantes='$totalParticipantes' WHERE codigo=$codigoGrupo");//Actualización del campo "participantes" de la tabla grupos

    if(!empty($clientesAnteriores)){
        foreach ($clientesAnteriores as $codigoCliente){
            if($codigoCliente!=''){
                $res=$res && consultaBD("DELETE FROM clientes_grupo WHERE codigoCliente=$codigoCliente AND codigoGrupo=$codigoGrupo");
            }
        }
    }

    return $res;
}


function insertaAlumnosVentaFormacion($codigoGrupo,$datos,$actualizacion=false){
    $res=true;
    $participantesAnteriores=array();

    if($actualizacion){
        $participantesAnteriores=consultaArray("SELECT codigoTrabajador FROM participantes WHERE codigoGrupo=$codigoGrupo");
    }

    for($i=0;isset($datos['codigoCliente'.$i]);$i++){
        for($j=0;isset($datos['codigoTrabajador'.$i.'-'.$j]);$j++){
            $codigoTrabajador=$datos['codigoTrabajador'.$i.'-'.$j];

            if(in_array($codigoTrabajador, $participantesAnteriores)){
                unset($participantesAnteriores[array_search($codigoTrabajador,$participantesAnteriores)]);//Elimino el participante actual del array, para no borrarlo luego
            }
            else{
                $res=$res && consultaBD("INSERT INTO participantes(codigo,codigoGrupo,codigoTrabajador) VALUES(NULL,$codigoGrupo,$codigoTrabajador);");
                $codigoParticipante=mysql_insert_id();
            }            
        }

        //$res=$res && consultaBD("UPDATE clientes_grupo SET numParticipantes='$j' WHERE codigoGrupo=$codigoGrupo AND codigoCliente=".$datos['codigoCliente'.$i]);//Actualización del campo "participantes" de la tabla clientes_grupo
    }

    //$res=$res && consultaBD("UPDATE grupos SET numParticipantes='$i' WHERE codigo=$codigoGrupo");//Actualización del campo "participantes" de la tabla grupos

    if(!empty($participantesAnteriores)){
        foreach ($participantesAnteriores as $codigoTrabajador){
            if($codigoTrabajador!=''){
                $res=$res && consultaBD("DELETE FROM participantes WHERE codigoTrabajador=$codigoTrabajador AND codigoGrupo=$codigoGrupo");
            }
        }
    }


    //Parte de complementos. TODO: mejorar, estoy cansado...
    $res=$res && consultaBD("DELETE FROM complementos_participantes WHERE codigoParticipante IN(SELECT codigo FROM participantes WHERE codigoGrupo=$codigoGrupo);");//Limpieza de la tabla complementos_participantes
    for($i=0;isset($datos['codigoCliente'.$i]);$i++){
        for($j=0;isset($datos['codigoTrabajador'.$i.'-'.$j]);$j++){
            $codigoTrabajador=$datos['codigoTrabajador'.$i.'-'.$j];

            $participante=consultaBD("SELECT codigo FROM participantes WHERE codigoTrabajador=$codigoTrabajador AND codigoGrupo=$codigoGrupo",false,true);
            $res=$res && insertaComplementosParticipanteVentaFormacion($participante['codigo'],$datos['codigoCliente'.$i],$datos);
        }
    }
    //Fin parte de complementos.

    return $res;
}


function insertaComplementosParticipanteVentaFormacion($codigoParticipante,$codigoCliente,$datos){
    $res=true;

    for($i=0;isset($datos['codigoClienteComplemento'.$i]);$i++){

        if($datos['codigoClienteComplemento'.$i]==$codigoCliente){
            $codigoComplemento=$datos['codigoComplemento'.$i];
            $precioComplemento=formateaNumeroWeb($datos['precioComplemento'.$i],true);
            $observaciones=$datos['observaciones'.$i];

            $res=$res && consultaBD("INSERT INTO complementos_participantes VALUES(NULL,$codigoComplemento,$precioComplemento,'NO','0000-00-00','0000-00-00','0000-00-00','0000-00-00','$observaciones',$codigoParticipante);");
        }
    }

    return $res;

    /* IMPLEMENTACIÓN EXPERIMENTAL CON FALLOS
    $res=true;
    $complementosAnteriores=array();
    $participantes=consultaArray("SELECT codigo FROM participantes WHERE codigoGrupo=$codigoGrupo");
    $participantesParaConsulta=implode(',',$participantes);

    if($actualizacion){
        $complementosAnteriores=consultaArray("SELECT codigoComplemento FROM complementos_participantes WHERE codigoParticipante IN($participantesParaConsulta);");
    }

    for($i=0;isset($datos['codigoComplemento'.$i]);$i++){
        $codigoComplemento=$datos['codigoComplemento'.$i];
        $precioComplemento=formateaNumeroWeb($datos['precioComplemento'.$i],true);
        $observaciones=$datos['observaciones'.$i];

        if(in_array($codigoComplemento, $complementosAnteriores)){
            $res=$res && consultaBD("UPDATE complementos_participantes SET precioComplemento='$precioComplemento', observaciones='$observaciones'
                                     WHERE codigoComplemento=$codigoComplemento AND codigoParticipante IN($participantesParaConsulta);");
            
            unset($complementosAnteriores[array_search($codigoComplemento,$complementosAnteriores)]);
        }
        else{
            foreach ($participantes as $codigoParticipante){
                $res=$res && consultaBD("INSERT INTO complementos_participantes(codigo,codigoComplemento,precioComplemento,observaciones,codigoParticipante) 
                                         VALUES(NULL,$codigoComplemento,'$precioComplemento','$observaciones',$codigoParticipante);");
            }
        }
    }


    if(!empty($complementosAnteriores)){
        foreach($complementosAnteriores as $codigoComplemento){
            if($codigoComplemento!=''){
                $res=$res && consultaBD("DELETE FROM complementos_participantes WHERE codigoComplemento=$codigoComplemento AND codigoParticipante IN($participantesParaConsulta);");
            }
        }
    }


    return $res;
    */
}


function creaTablaComplementosFormacion($datos,$venta=false){
    echo "
    <br />
    <h3 class='apartadoFormulario'>Complementos</h3>

    <table class='table table-striped tabla-simple' id='tablaComplementos'>
        <thead>
            <tr>
                <th> Cliente </th>
                <th> Complemento <!--button type='button' class='btn btn-primary btn-small' id='crearComplemento'><i class='icon-plus-circle'></i> Crear complemento</button--></th>
                <th> Precio </th>
                <th> Observaciones </th>";

        if(!$venta){
            echo "
                <th> Co. Enviado </th>
                <th> F. Envío </th>";
        }

    echo "      <th> </th>
            </tr>
        </thead>
        <tbody>";
            
        conexionBD();

        $i=0;
        if($datos!=false){
            $consulta=consultaBD("SELECT complementos_participantes.codigo, codigoCliente, codigoComplemento, precioComplemento, complementos_participantes.observaciones, complementoEnviado, fechaEnvio FROM participantes INNER JOIN trabajadores_cliente ON participantes.codigoTrabajador=trabajadores_cliente.codigo LEFT JOIN complementos_participantes ON participantes.codigo=complementos_participantes.codigoParticipante WHERE codigoGrupo=".$datos['codigo']." GROUP BY codigoCliente, codigoComplemento ORDER BY codigoCliente");
            while($participante=mysql_fetch_assoc($consulta)){
                imprimeLineaTablaComplementosFormacion($i,$participante,$venta);
                $i++;
            }
        }

        if($i==0){
            imprimeLineaTablaComplementosFormacion($i,false,$venta);
        }

        cierraBD();

    echo "
        </tbody>
    </table>
    <div class='centro'>
        <button type='button' class='btn btn-small btn-success' onclick='insertaComplemento();'><i class='icon-plus'></i> Añadir complemento</button> 
        <button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaComplementos\");'><i class='icon-trash'></i> Eliminar complemento</button>
    </div>";
}

function imprimeLineaTablaComplementosFormacion($i,$datos,$venta){
    $j=$i+1;
    if($datos['fechaEnvio']==NULL){//Cuando se inserta una venta sin complementos, la consulta extrae la fechaEnvio a NULL. Si se pasa así directamente al campoFechaTabla, podrá la fecha actual y desde LAE no quieren eso
        $fechaEnvio='0000-00-00';
    }
    else{
        $fechaEnvio=$datos['fechaEnvio'];
    }

    echo "<tr>";
            campoSelectClienteListadoFiltradoPorUsuario($i,$datos['codigoCliente'],1,'codigoClienteComplemento');
            campoSelectConsulta('codigoComplemento'.$i,'',"SELECT codigo, complemento AS texto FROM complementos WHERE activo='SI' ORDER BY complemento",$datos['codigoComplemento'],'selectpicker span3 show-tick selectComplemento','data-live-search="true"','',1,false);
            campoTextoSimbolo('precioComplemento'.$i,'','€',formateaNumeroWeb($datos['precioComplemento']),'input-mini pagination-right',1);
            areaTextoTabla('observaciones'.$i,$datos['observaciones']);
            
        if(!$venta){
            campoCheckTabla('complementoEnviado'.$i,$datos['complementoEnviado']);
            campoFechaTabla('fechaEnvio'.$i,$fechaEnvio);
        }

    echo "  <td><input type='checkbox' name='filasTabla[]' value='$j'></td>
        </tr>";
}


function obtieneWhereListadoVentas($columnas,$indice='11'){
    $res='';
    $perfil=$_SESSION['tipoUsuario'];

    if(($perfil=='DIRECTOR' || $perfil=='DELEGADO') && isset($_GET['sSearch_'.$indice]) && $_GET['sSearch_'.$indice]!='NULL' && $_GET['sSearch_'.$indice]!=''){
        $codigoComercial=$_GET['sSearch_'.$indice];

        $res=obtieneCondicionFiltradoComercialListadoVentas($codigoComercial);

        unset($_GET['sSearch_'.$indice]);//Debe hacerse ANTES de la llamada a obtieneWhereListado(), para que no tenga el cuenta el campo restantes a la hora de construir el WHERE (porque daría fallo por uso incorrecto de función de agrupación)
    }

    return $res;
}

function obtieneCondicionFiltradoComercialListadoVentas($codigoComercial){//Esta función es distinta a obtieneCondicionFiltradoComercial, la cual crea un HAVING. La otra, además de usarse en los listados de ventas y comisiones, se usa en para filtrar los comerciales en el campo comercial de los filtros

    //Según el organigrama de LAE (facilitado en Excel), desde el comercial hacía arriba pueden realizarse un máximo de 8 saltos:
    $res=" AND (comerciales.codigo=$codigoComercial OR comerciales.codigoComercial=$codigoComercial
              OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c1 WHERE c1.codigoComercial=$codigoComercial)
              OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c2 WHERE c2.codigoComercial IN(SELECT codigo FROM comerciales c3 WHERE c3.codigoComercial=$codigoComercial))
              OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c4 WHERE c4.codigoComercial IN(SELECT codigo FROM comerciales c5 WHERE c5.codigoComercial IN(SELECT codigo FROM comerciales c6 WHERE c6.codigoComercial=$codigoComercial)))
              OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c7 WHERE c7.codigoComercial IN(SELECT codigo FROM comerciales c8 WHERE c8.codigoComercial IN(SELECT codigo FROM comerciales c9 WHERE c9.codigoComercial IN(SELECT codigo FROM comerciales c10 WHERE c10.codigoComercial=$codigoComercial))))
              OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c11 WHERE c11.codigoComercial IN(SELECT codigo FROM comerciales c12 WHERE c12.codigoComercial IN(SELECT codigo FROM comerciales c13 WHERE c13.codigoComercial IN(SELECT codigo FROM comerciales c14 WHERE c14.codigoComercial IN(SELECT codigo FROM comerciales c15 WHERE c15.codigoComercial=$codigoComercial)))))
              OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c16 WHERE c16.codigoComercial IN(SELECT codigo FROM comerciales c17 WHERE c17.codigoComercial IN(SELECT codigo FROM comerciales c18 WHERE c18.codigoComercial IN(SELECT codigo FROM comerciales c19 WHERE c19.codigoComercial IN(SELECT codigo FROM comerciales c20 WHERE c20.codigoComercial IN(SELECT codigo FROM comerciales c21 WHERE c21.codigoComercial=$codigoComercial))))))
            )";


    return $res;
}

//Fin parte común entre preventas y ventas

//Parte de tutorías

function revisaFechasTutorias(){
    $res=consultaBD("UPDATE tutorias, grupos SET fechaSiguiente=CURDATE() WHERE tutorias.codigoGrupo=grupos.codigo AND fechaSiguiente<CURDATE() AND terminada='NO' AND fechaFin>=CURDATE();",true);
    if(!$res){
        mensajeError('no se ha podido actualizar la fecha de la siguiente llamada de una o más tutorías.');
    }
}

//Fin parte de tutorías



//Parte común entre preventas, ventas, listado de ventas y listado de comisiones

function campoSelectComercialFiltroVentas($nombreCampo){
    $query=obtieneQuerySelectComercialFiltroVentas($_SESSION['codigoU'],$_SESSION['tipoUsuario']);

    campoSelectConsulta($nombreCampo,'Agente/Agrupación',$query);
}

function obtieneQuerySelectComercialFiltroVentas($codigoUsuario,$perfil){
    $res='';

    if($perfil!='ADMIN' && $perfil!='ADMINISTRACION1' && $perfil!='ADMINISTRACION2'){
        $comercial=consultaBD("SELECT codigo FROM comerciales WHERE codigoUsuarioAsociado='".$codigoUsuario."';",true,true);
        
        if($comercial!=false){
            $having=obtieneCondicionFiltradoComercial($comercial['codigo']);
            $res="SELECT codigo, nombre AS texto, comerciales.codigo, comerciales.codigoComercial FROM comerciales $having ORDER BY nombre;";
        }
    }


    if($res==''){//Si el perfil es de ADMIN, alguno de administración ó el usuario no tiene comercial asignado, se muestran todos los comerciales:
        $res="SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre;";
    }

    return $res;
}


function obtieneCondicionFiltradoComercial($codigoComercial){

    //Según el organigrama de LAE (facilitado en Excel), desde el comercial hacía arriba pueden realizarse un máximo de 8 saltos:
    $res="HAVING comerciales.codigo=$codigoComercial OR comerciales.codigoComercial=$codigoComercial
          OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c1 WHERE c1.codigoComercial=$codigoComercial)
          OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c2 WHERE c2.codigoComercial IN(SELECT codigo FROM comerciales c3 WHERE c3.codigoComercial=$codigoComercial))
          OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c4 WHERE c4.codigoComercial IN(SELECT codigo FROM comerciales c5 WHERE c5.codigoComercial IN(SELECT codigo FROM comerciales c6 WHERE c6.codigoComercial=$codigoComercial)))
          OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c7 WHERE c7.codigoComercial IN(SELECT codigo FROM comerciales c8 WHERE c8.codigoComercial IN(SELECT codigo FROM comerciales c9 WHERE c9.codigoComercial IN(SELECT codigo FROM comerciales c10 WHERE c10.codigoComercial=$codigoComercial))))
          OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c11 WHERE c11.codigoComercial IN(SELECT codigo FROM comerciales c12 WHERE c12.codigoComercial IN(SELECT codigo FROM comerciales c13 WHERE c13.codigoComercial IN(SELECT codigo FROM comerciales c14 WHERE c14.codigoComercial IN(SELECT codigo FROM comerciales c15 WHERE c15.codigoComercial=$codigoComercial)))))
          OR comerciales.codigoComercial IN(SELECT codigo FROM comerciales c16 WHERE c16.codigoComercial IN(SELECT codigo FROM comerciales c17 WHERE c17.codigoComercial IN(SELECT codigo FROM comerciales c18 WHERE c18.codigoComercial IN(SELECT codigo FROM comerciales c19 WHERE c19.codigoComercial IN(SELECT codigo FROM comerciales c20 WHERE c20.codigoComercial IN(SELECT codigo FROM comerciales c21 WHERE c21.codigoComercial=$codigoComercial))))))";


    return $res;
}

function eliminaClientes(){
    $res=true;
    $datos=arrayFormulario();
    conexionBD();
    for($i=0;isset($datos['codigo'.$i]);$i++){
        $usuario=consultaBD("SELECT * FROM usuarios_clientes WHERE codigoCliente='".$datos['codigo'.$i]."';",false,true);
        $consulta=consultaBD("DELETE FROM usuarios WHERE codigo='".$usuario['codigoUsuario']."';");
        $consulta=consultaBD("DELETE FROM clientes WHERE codigo='".$datos['codigo'.$i]."';");
        if(!$consulta){
            $res=false;
            echo mysql_error();
        }
    }
    cierraBD();

    return $res;
}

function campoCheckIndividualConFechas($nombreCampo,$textoCampo,$valor=false,$valorCampo='SI',$salto=true){
    $fecha1=$valor == false ? '0000-00-00' : $valor['fechaContratacion'.$nombreCampo];
    $fecha2=$valor == false ? '0000-00-00' : $valor['fechaFinalizacion'.$nombreCampo];
    $valor=compruebaValorCampo($valor,$nombreCampo);
    echo "<label class='checkbox'>
            <input type='checkbox' name='$nombreCampo' id='$nombreCampo' value='".$valorCampo."'";

    if($valor!=false && $valor==$valorCampo){
        echo " checked='checked'";
    }
    echo ">".$textoCampo;
    echo '<table>
        <tr>
            <td>Contratación</td>
            <td>Finalización</td>
        </tr><tr>';
    campoFechaTabla('fechaContratacion'.$nombreCampo,$fecha1);
    campoFechaTabla('fechaFinalizacion'.$nombreCampo,$fecha2);
    echo "</tr></table></label>";

    if($salto){
        echo "<br />";
    }

    
}

function selectPaises($nombreCampo, $datos = false, $clase = false, $texto = 'País', $busqueda = "data-live-search='true'"){
    
    if (!$datos){
        $datos[$nombreCampo] = 74;
    }

    if ($clase){
        $clase = 'selectpicker span3 show-tick selectPaises';
    } 
    else {
        $clase = 'selectpicker span3 show-tick';
    }

    $valor = compruebaValorCampo($datos, $nombreCampo);
    
    $listado = consultaBD("SELECT * FROM paises ORDER BY nombre", true);

    echo "
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls'>
                <select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda>
    ";

    while ($pais = mysql_fetch_assoc($listado)) {
        echo "<option value='".$pais['codigoInterno']."' medidas='".$pais['medidas']."'";

        if($valor == $pais['codigoInterno']){
            echo " selected='selected'";
        }

        echo ">".$pais['nombre']."</option>";
    }
    
    echo "
                </select>
            </div> <!-- /controls -->       
		</div> <!-- /control-group -->
    ";
}

function preparaArrayPaises() {

	$res = ['' => ''];

	$listado = consultaBD("SELECT * FROM paises ORDER BY nombre", true);

	while ($pais = mysql_fetch_assoc($listado)) {
		$res[$pais['codigoInterno']] = $pais['nombre'];
	}

	return $res;

}

function selectMeses($nombreCampo,$datos,$texto='Mes',$tipo=0){
    $nombres=array('ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE');
    $valores=array('01','02','03','04','05','06','07','08','09','10','11','12');
    campoSelect($nombreCampo,$texto,$nombres,$valores,$datos,'selectpicker span2 show-tick','data-live-search="true"',$tipo);
}

function campoTextoVarios($nombreCampo,$texto,$datos,$separaciones=array(1),$clase='input-small'){
    $valor='';
    $val='';
    if($datos){
        $valor=$datos[$nombreCampo];
    }
    echo "
        <div class='control-group'>                     
          <label class='control-label' for='$nombreCampo'>$texto:</label>
          <div class='controls'>";
    $i=0;
    foreach ($separaciones as $separacion) {
        if(is_array($clase)){
            $claseSep=$clase[$i];
        } else {
            $claseSep=$clase;
        }
        if($valor!=''){
            $val=substr($valor, 0,$separacion);
            $valor=substr($valor, $separacion);
        }
        echo "<input type='text' class='$claseSep' id='$nombreCampo$i' name='$nombreCampo$i' value=\"$val\"> ";
        $i++;
    }

    echo "
          </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
}

function recogeItem(){
    $codigo=$_POST['codigo'];
    $tabla=$_POST['tabla'];
    $item=datosRegistro($tabla,$codigo);
    echo json_encode($item);
}

function abreVentanaModalConFichero($titulo,$id='cajaGestion',$claseColumna=''){
    echo "
    <!-- Caja de gestión -->
    <div id='$id' class='modal hide fade'> 
      <form class='form-horizontal sinMargenAb noAjax' method='post' action='?' enctype='multipart/form-data'>
        <div class='modal-header'> 
          <h3> <i class='icon-list'></i><i class='icon-chevron-right'></i><i class='icon-edit'></i> &nbsp; Gestión de $titulo </h3> 
        </div> 
        <div class='modal-body'>
            <fieldset class='$claseColumna'>";
}

function campoClaveUsuario($datos){
    if(!$datos || ($datos && $_SESSION['tipoUsuario']=='ADMIN')){
        campoClave('clave','Contraseña',$datos,'btn-propio','input-small obligatorio');
    }
}

function obtenerCodigoCliente($conexion){
    $cliente=consultaBD('SELECT * FROM usuarios_clientes WHERE codigoUsuario='.$_SESSION['codigoU'],$conexion,true);
    return $cliente['codigoCliente'];
}

function obtenerUsuarioCliente($codigoCliente){
    $usuario=consultaBD('SELECT * FROM usuarios_clientes WHERE codigoCliente='.$codigoCliente,true,true);
    $res=datosRegistro('usuarios',$usuario['codigoUsuario']);
    echo json_encode($res);
}

function creaOpcionMenuMDiaz($indice,$nombre,$enlace,$icono,$seccionActiva,$perfiles=array('ADMIN')){
    global $_CONFIG;

    $res='<li class="';
    $res.=compruebaSeccionActivaMenu($indice,$seccionActiva);
    $res.='"';

    $atributos=compruebaOpcionMenuMDiaz($_CONFIG['raiz'].$enlace);

    $res.="><a href='".$_CONFIG['raiz'].$enlace."' $atributos><i class='$icono'></i><span>$nombre</span> </a></li>";

    if(in_array($_SESSION['tipoUsuario'],$perfiles)){
        echo $res;
    }
}

function creaOpcionMenuMDiazDocumento($indice,$nombre,$enlace,$icono,$seccionActiva,$perfiles=array('ADMIN')){
    global $_CONFIG;

    $res='<li class="';
    $res.=compruebaSeccionActivaMenu($indice,$seccionActiva);
    $res.='"';

    $atributos=compruebaOpcionMenuMDiaz($_CONFIG['raiz'].$enlace);
    $cliente=obtenerCodigoCliente(true);
    $cliente=datosRegistro('clientes',$cliente);
    if($cliente['empleados']==10){
        $res.="><a class='menuDocumento10 noAjax' target='_blank' href='".$_CONFIG['raiz'].$enlace."' $atributos><i class='$icono'></i><span>$nombre</span> </a></li>";
    } else {
        $res.="><a class='menuDocumento noAjax' target='_blank' href='".$_CONFIG['raiz'].$enlace."' $atributos><i class='$icono'></i><span>$nombre</span> </a></li>";
    }
    if(in_array($_SESSION['tipoUsuario'],$perfiles)){
        echo $res;
    }
}

function creaOpcionDesplegableMenuMDiaz($indice,$nombre,$icono,$opciones,$enlaces,$iconos,$seccionActiva,$perfiles=array('ADMIN')){
    
    global $_CONFIG;

    $res = "
        <li class='dropdown 
    ";
    
    $res .= compruebaSeccionActivaMenu($indice, $seccionActiva);

    $res.="'>   <a href='javascript:void;' class='dropdown-toggle' data-toggle='dropdown'> <i class='$icono'></i><span>$nombre</span> <b class='caret'></b></a>";
    $res.='     <ul class="dropdown-menu">';

    $atributos=compruebaOpcionMenuMDiaz($_CONFIG['raiz'].$enlaces[0]);

    $res.="         <li><a href='".$_CONFIG['raiz'].$enlaces[0]."' $atributos><i class='".$iconos[0]."'></i><span>".$opciones[0]."</span> </a></li>";

    for($i=1;$i<count($opciones);$i++){
        $atributos=compruebaOpcionMenuMDiaz($_CONFIG['raiz'].$enlaces[$i]);

        $res.="     <li class='divider'></li>";
        $res.="     <li><a href='".$_CONFIG['raiz'].$enlaces[$i]."' $atributos><i class='".$iconos[$i]."'></i><span>".$opciones[$i]."</span> </a></li>";        
    }
    
    $res.="     </ul>
        </li>";

    if(in_array($_SESSION['tipoUsuario'],$perfiles)){
        echo $res;
    }
}


function compruebaOpcionMenuMDiaz($enlace){
    $res='';
    
    $partesEnlace=explode('?',$enlace);//Si utilizamos por ejemplo el enlace clientes/index.php?activos='SI', no reconoce el fichero index.php
    if(isset($partesEnlace[1])){//Si el enlace tenía ?...
        $enlace=$partesEnlace[0];//... lo dejo solo con el nombre real del fichero
    }

    $ruta=$_SERVER['DOCUMENT_ROOT'].$enlace;//Para obtener la ruta absoluta del enlace

    return $res;
}

function creaBotonDescarga($url,$titulo='Descargar',$posicion='btn-creacion2'){
    echo '<a class="btn-floating btn-large btn-info '.$posicion.' noAjax" href="'.$url.'" title="'.$titulo.'"><i class="icon-download"></i></a>';

}

function cajaHistoricoSQL(){
   global $_CONFIG;

   if($_SESSION['codigoU']==2){//El código 1 corresponde a soporte/soporte15
       echo '
       <div class="nav-collapse">
           <ul class="nav pull-right cajaUsuario" id="cajaFiltroEjercicio" data-date="01-01-2016" data-date-format="dd-mm-yyyy">
               <li class="dropdown">
                   <a href="'.$_CONFIG['raiz'].'historico-sql/" class="noAjax"><i class="icon-database"></i> Histórico SQL</a>
               </li>
           </ul>
       </div>';
   }
}

function botonesGestion($seccion,$permiso=1,$texto='',$gestion='gestion.php'){
    global $_CONFIG;
    $res='';
    if($permiso==1){
        //if(recogerPermiso($seccion,'checkCrear')=='SI'){
            //$opcion=datosRegistro('menu_principal',$seccion);
            $opcion='';
            $texto = $texto == '' ? 'Nuevo':$texto;
            //if(substr_count($opcion['directorio'],'/')==0){
                $opcion.='indicadores/';
            //}
            $res='<a href="'.$_CONFIG['raiz'].$opcion.$gestion.'" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">'.$texto.'</span> </a>';
        //}
    } else if($permiso==2){
        //if(recogerPermiso($seccion,'checkEliminar')=='SI'){
            $texto = $texto == '' ? 'Eliminar':$texto;
            $res='<a href="javascript:void" id="eliminar" class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>';
        //}
    }
    echo $res;
}

function creaTablaAuditoria(){
    echo '
    <table class="table table-striped table-bordered tablaAuditoria">
      <tbody>
        <tr>
          <th> Apartado Analizado </th>
          <th> Resultado </th>
          <th> Observaciones, comentarios y evidencias </th>
        </tr>';
}

function cierraTablaAuditoria(){
    echo '
      </tbody>
    </table>
    ';
}

function campoSelectSolo($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'"){
    $valor=compruebaValorCampo($valor,$nombreCampo);

    echo "<select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda>";
        
    for($i=0;$i<count($nombres);$i++){
        echo "<option value='".$valores[$i]."'";

        if($valor==$valores[$i]){
            echo " selected='selected'";
        }

        echo ">".$nombres[$i]."</option>";
    }
    echo "</select>";
}

function campoFechaBlanco($nombreCampo,$texto,$valor=false,$solo=false,$disabled=false){//Modificada por Jose Luis el 15/09/2014 (añadido opción disabled).
    if(!$valor){
        $valor='';
    }
    else{
        $valor=compruebaValorCampo($valor,$nombreCampo);
        $valor=formateaFechaWeb($valor);
    }

    if($solo){
        campoTextoSolo($nombreCampo,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
    else{
        campoTexto($nombreCampo,$texto,$valor,'input-small datepicker hasDatepicker',$disabled);
    }
}

//Parte común entre preventas, ventas, listado de ventas y listado de comisiones


//Parte de comunicación interna

function compruebaMensajesPorLeer(){
    $inicio=false;
    date_default_timezone_set('Europe/Madrid');//Necesario para que el servidor no dé error.
    
    if(isset($_SESSION['ultimaComprobacion'])){//Si ya se ha comprobado anteriormente
        $ultimaComprobacion=$_SESSION['ultimaComprobacion'];
        $sinLeer=$_SESSION['mensajesSinLeer'];
    }
    else{//Si es la primera vez que se comprueba
        $ultimaComprobacion=new DateTime();
        $sinLeer=0;
        $inicio=true;
    }

    $ahora=new DateTime();

    $diferencia=date_diff($ultimaComprobacion,$ahora);
    $diferencia=date_interval_format($diferencia,'%i');//Transformación del intervalo en minutos

    if($diferencia>5 || $inicio){//Comprueba que haya mensajes nuevos cada 5 minutos
        $codigoU=$_SESSION['codigoU'];
        //La siguiente consulta contabiliza todos los mensajes no leídos de las conversaciones a los que el usuario está asociado, sin tener en cuenta sus propios mensajes
        $consulta=consultaBD("SELECT conversaciones.codigo AS sinLeer FROM (conversaciones INNER JOIN destinatarios_conversaciones ON conversaciones.codigo=destinatarios_conversaciones.codigoConversacion) INNER JOIN mensajesConversaciones ON conversaciones.codigo=mensajesConversaciones.codigoConversacion WHERE (conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversaciones.codigoUsuario='$codigoU') AND leido='NO' AND mensajesConversaciones.codigoUsuario!='$codigoU' GROUP BY destinatarios_conversaciones.codigoConversacion;",true);
        $sinLeer=mysql_num_rows($consulta);
    }

    $_SESSION['ultimaComprobacion']=$ahora;//Actualizo la variable temporal de referencia
    $_SESSION['mensajesSinLeer']=$sinLeer;//Actualizo la variable de mensajes nuevos

    echo $sinLeer;
}


function imprimeConversaciones(){
    $codigoU=$_SESSION['codigoU'];

    conexionBD();
    $consulta=consultaBD("SELECT conversaciones.codigo AS codigo, fecha, hora, asunto, nombre, apellidos FROM (conversaciones INNER JOIN usuarios ON conversaciones.codigoUsuario=usuarios.codigo) INNER JOIN destinatarios_conversaciones ON conversaciones.codigo=destinatarios_conversaciones.codigoConversacion WHERE conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversaciones.codigoUsuario='$codigoU' GROUP BY conversaciones.codigo;");
    while($datos=mysql_fetch_assoc($consulta)){
        $sinLeer=compruebaMensajesSinLeerConversacion($codigoU,$datos['codigo']);

        echo "
        <tr>
            <td> ".$datos['asunto']." $sinLeer</td>
            <td> ".formateaFechaWeb($datos['fecha'])." </td>
            <td> ".formateaHoraWeb($datos['hora'])." </td>
            <td> ".$datos['apellidos'].", ".$datos['nombre']." </td>
            <td class='centro'>
                <a href='conversaciones.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Ver mensajes</i></a>
            </td>
            <td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
        </tr>";
    }
    cierraBD();
}

function compruebaMensajesSinLeerConversacion($codigoU,$codigoC){
    $sinLeer='';
    
    //$numSinLeer=consultaBD("SELECT COUNT(conversaciones.codigo) AS sinLeer FROM (conversaciones INNER JOIN destinatarios_conversaciones ON conversaciones.codigo=destinatarios_conversaciones.codigoConversacion) INNER JOIN mensajesConversaciones ON conversaciones.codigo=mensajesConversaciones.codigoConversacion WHERE (conversaciones.codigoUsuario='$codigoU' OR destinatarios_conversaciones.codigoUsuario='$codigoU') AND leido='NO' AND conversaciones.codigo='$codigoC' AND mensajesConversaciones.codigoUsuario!='$codigoU';",false,true);//YA SE HACE EL FILTRADO DE DESTINATARIO/AUTOR ARRIBA!!!
    $numSinLeer=consultaBD("SELECT COUNT(codigo) AS sinLeer FROM mensajesConversaciones WHERE codigoUsuario!='$codigoU' AND leido='NO' AND codigoConversacion='6';",false,true);
    if($numSinLeer['sinLeer']>0){
        $sinLeer="<span class='badge'>".$numSinLeer['sinLeer']."</span>";
    }

    return $sinLeer;
}

function abreVentanaModalDocumentacion($titulo,$id='cajaGestion',$claseColumna='',$action='generaDocumento.php'){
    echo "
    <!-- Caja de gestión -->
    <div id='$id' class='modal hide fade'> 
      <form class='form-horizontal sinMargenAb noAjax' method='post' action='$action'>
        <div class='modal-header'> 
          <h3> <i class='icon-list'></i><i class='icon-chevron-right'></i><i class='icon-edit'></i> &nbsp; Gestión de $titulo </h3> 
        </div> 
        <div class='modal-body'>
            <fieldset class='$claseColumna'>";
}

function cierraVentanaModalDocumentacion($id='enviar',$textoGuardar='Guardar',$iconoGuardar='icon-check',$botonGuardar=true,$textoCancelar='Cancelar'){
    echo "
            </fieldset>
        </div> 
        <div class='modal-footer'> 
          <button type='button' class='btn btn-default' data-dismiss='modal'><i class='icon-remove'></i> $textoCancelar</button>";

        if($botonGuardar){
          echo "
          <button type='submit' class='btn btn-propio' id='$id'><i class='$iconoGuardar'></i> $textoGuardar</button> ";
        }

    echo "
        </div> 
      </form>
    </div>

    <!-- Fin caja de gestión -->";
}

function campoFicheroInf($nombreCampo,$texto,$tipo=0,$valor=false,$ruta=false,$nombreDescarga=''){
    $valor=compruebaValorCampo($valor,$nombreCampo);//MODIFICACIÓN 14/04/2015: añadida comprobación del valor del campo
    if(!$valor || $valor=='NO'){//MODIFICACIÓN 09/03/2015: añadida a la comprobación que el valor no es "NO", ya que de lo contrario pasaría al else y se provocaría un bucle infinito de llamadas con la función campoDescarga.
        if($tipo==0){
            echo "
            <div class='control-group'>                     
                <label class='control-label' for='$nombreCampo'>$texto:</label>
                <div class='controls'>
                    <input type='file' name='$nombreCampo' id='$nombreCampo' /><div class='tip'>Solo se admiten ficheros de 5 MB como máximo</div>
                </div> <!-- /controls -->       
            </div> <!-- /control-group -->";
        }
        elseif($tipo==1){//MODIFICACIÓN 25/09/2015: sustituido else por elseif para que haya una tercera opción
            echo "<td><input type='file' name='$nombreCampo' id='$nombreCampo' /></td>";
        }
        else{
            echo "<input type='file' name='$nombreCampo' id='$nombreCampo' />";
        }
    }
    else{
        campoDescargaInf($nombreCampo,$texto,$ruta,$valor,$tipo,$nombreDescarga);
        campoOculto($valor,$nombreCampo);
    }
}

function campoDescargaInf($nombreCampo,$texto,$ruta,$valor,$tipo=0,$nombreDescarga=''){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    
    if($nombreDescarga==''){//MODIFICACIÓN 21/08/2015: añadido el parámetro $nombreDecarga para utilizarlo como nombre en el enlace de descarga
        $nombreDescarga=$valor;
    }

    //MODIFICACIÓN 28/09/2015: reestructuración del bloque if
    if(trim($valor)=='NO' || $valor==''){//MODIFICACIÓN 05/05/2015: añadida nueva condición al if y sustuido el datoSinInput por campoFichero
        campoFichero($nombreCampo,$texto,$tipo,$valor,$ruta);
    }
    elseif($tipo==0){
        echo "
            <div class='control-group $nombreCampo'>                     
              <label class='control-label'>$texto:</label>
              <div class='controls'>
                <a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>
                <a style='margin-right:10px;cursor:pointer;' id='$nombreCampo' class='eliminaDocumentacion noAjax'> <i class='icon-trash'></i></a>
              </div> <!-- /controls -->       
            </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "<td><a class='btn btn-propio noAjax descargaFichero' href='$ruta$valor' target='_blank' nombre='$nombreCampo'><i class='icon-cloud-download'></i> $nombreDescarga</a></td>";
    }
    else{
        echo "<a class='btn btn-propio noAjax' href='$ruta$valor' target='_blank'><i class='icon-cloud-download'></i> $nombreDescarga</a>";
    }

}

function convertirMinuscula($texto){
    $primeraLetra=mb_substr($texto,0,1,'UTF-8');
    $res=$primeraLetra.mb_strtolower(mb_substr($texto,1),'UTF-8');
    return $res;
}

function comprobarServiciosVenta(){
    $pblc='NO';
    $lopd='NO';
    $i=0;
    while(isset($_POST['codigoServicio'.$i])){
        if(in_array($_POST['codigoServicio'.$i], array(5,6,7,8,9,10,11,12,14,15))){
            $pblc='SI';
        }
        if(in_array($_POST['codigoServicio'.$i], array(1,3,4,6,8,11,12,15))){
            $lopd='SI';
        }
        $i++;
    }
    $res=consultaBD('UPDATE clientes SET pblc="'.$pblc.'",lopd="'.$lopd.'" WHERE codigo='.$_POST['codigoCliente'],true);
}

function campoCheckIndividualDevolver($nombreCampo,$textoCampo,$valor=false,$valorCampo='SI',$salto=true){
    $valor=compruebaValorCampo($valor,$nombreCampo);

    $res="<label class='checkbox inline'>
            <input type='checkbox' name='$nombreCampo' id='$nombreCampo' value='".$valorCampo."'";

    if($valor!=false && $valor==$valorCampo){
        $res.=" checked='checked'";
    }
    $res.=">".$textoCampo."</label>";

    if($salto){
        $res.="<br />";
    }
    return $res;
}

function campoSelectDelegaciones($nombreCampo,$texto,$nombres,$valores,$valor=false,$valor2=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0){
    $nombreCampo2=str_replace('facultades','otras',$nombreCampo);
    $valor=compruebaValorCampo($valor,$nombreCampo);
    $valor2=compruebaValorCampo($valor2,$nombreCampo2);
    campoOculto('NULL',$nombreCampo);
    if($tipo==0){
        echo "
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls'>";
    }
    elseif($tipo==1){
        echo "<td>";
    }
    
    if($valor != ''){
        $select=explode('&$&',$valor);
    } else {
        $select = '';
    }

    echo "<select multiple name='".$nombreCampo."[]' id='".$nombreCampo."' class='$clase selectMultiple selectDelegaciones' $busqueda>";
        
    for($i=0;$i<count($nombres);$i++){
        echo "<option value='".$valores[$i]."'";

        if($select!='' && in_array($valores[$i], $select)){
            echo " selected='selected'";
        }

        echo ">".$nombres[$i]."</option>";
    }
        
    echo "</select>";
    echo '<div id="div_'.$nombreCampo.'" class="hide">';
    campoTextoSolo($nombreCampo2,$valor2);
    echo '</div>';
    if($tipo==0){
        echo "
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        echo "</td>";
    }
}

function campoTextoTablaMostrar($nombreCampo,$valor='',$clase='input-large',$disabled=false){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    $des='';
    if($disabled){
        $des='readonly';
    }
    $res="<td>";
    $res.="<input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' value=\"$valor\" $des>";
    $res.="</td>";
    return $res;
}

function campoSelectMostrar($nombreCampo,$texto,$nombres,$valores,$valor=false,$clase='selectpicker span3 show-tick',$busqueda="data-live-search='true'",$tipo=0,$multiple=false){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    
    if($multiple){
        $valor=explode(';',$valor);//Para los select múltiples, tomamos la convención de que los valores se serializan en base de datos separados por ;
        $multiple="multiple='multiple'";
    }
    $res='';
    if($tipo==0){
        $res="
        <div class='control-group'>                     
            <label class='control-label' for='$nombreCampo'>$texto:</label>
            <div class='controls'>";
    }
    elseif($tipo==1){
        $res="<td>";
    }

    $res.="<select name='$nombreCampo' id='$nombreCampo' class='$clase' $busqueda $multiple>";
        
    for($i=0;$i<count($nombres);$i++){
        $res.="<option value='".$valores[$i]."'";

        if(is_array($valor) && in_array($valores[$i],$valor)){
            $res.=" selected='selected'";
        }
        elseif($valor==$valores[$i]){
            $res.=" selected='selected'";
        }

        $res.=">".$nombres[$i]."</option>";
    }
        
    $res.="</select>";

    if($tipo==0){
        $res.="
            </div> <!-- /controls -->       
        </div> <!-- /control-group -->";
    }
    elseif($tipo==1){
        $res.="</td>";
    }
    return $res;
}

function campoFechaTablaMostrar($nombreCampo,$valor=false,$bloqueado=false){
    return campoTextoTablaMostrar($nombreCampo,$valor,'input-small datepicker hasDatepicker',$bloqueado);
}

function campoOcultoMostrar($valor,$nombreCampo='codigo',$valorPorDefecto='',$clase='hide'){
    $valor=compruebaValorCampo($valor,$nombreCampo);
    if($valor=='' || !$valor){
        $valor=$valorPorDefecto;
    }
    elseif($valor==NULL){
        $valor='NULL';
    }
    return "<input type='hidden' class='$clase' name='$nombreCampo' value='$valor' id='$nombreCampo' />";
}

function obtieneReferenciaDerecho($tipo,$codigoCliente){
    $res=consultaBD('SELECT IFNULL(MAX(referencia),0)+1 AS n FROM derechos WHERE tipo="'.$tipo.'" AND codigoCliente='.$codigoCliente,true,true);
    return $res['n'];
}

function wordListadoDerechos($PHPWord,$codigoCliente,$tipo){
    global $_CONFIG;
    $fichero=explode(' ',$tipo);
    $fichero=$fichero[0].'.docx';
    $datos=datosRegistro('clientes',$codigoCliente);
    $documento=$PHPWord->loadTemplate('../documentos/derechos/plantillaListadoDerechos.docx');
    $documento->setValue("true","");
    $documento->setValue("cliente",utf8_decode(sanearCaracteresDerechos($datos['razonSocial'])));  
    $documento->setValue("titulo",utf8_decode("REGISTRO DE DERECHOS DE ".mb_strtoupper($tipo)." A ".date("d/m/Y")));

    reemplazarLogoDerechos($documento,$datos,'image2.png'); 

    $tablaEntradas=fechaTablaDerechos(date("d/m/Y"),'REGISTRO DE DERECHOS DE '.mb_strtoupper($tipo).' RECIBIDOS');

    $tablaEntradas.='<w:tbl><w:tblPr><w:tblW w:w="13716" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="675"/><w:gridCol w:w="4253"/><w:gridCol w:w="2268"/><w:gridCol w:w="2268"/><w:gridCol w:w="4252"/></w:tblGrid><w:tr w:rsidR="00F44EB4" w:rsidRPr="008F5EA5" w14:paraId="667C2B27" w14:textId="77777777" w:rsidTr="00923E21"><w:tc><w:tcPr><w:tcW w:w="675" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="290D6DD6" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="002D6056" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4253" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="033B68A3" w14:textId="5FD3B458" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00923E21" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>INTERESADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="1929EA0B" w14:textId="7AEAED12" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00923E21" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA DE RECEPCIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="78045DA5" w14:textId="67DE692D" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="002D6056" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t xml:space="preserve">FECHA </w:t></w:r><w:r w:rsidR="00923E21"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>DE RESPUESTA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4252" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="5A8975E1" w14:textId="115159C2" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00923E21" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>RESPUESTA</w:t></w:r></w:p></w:tc></w:tr>';

    $i=1;   
    $listado=consultaBD("SELECT * FROM derechos WHERE tipo='".$tipo."' AND codigoCliente='".$datos['codigo']."';",true);
    if($tipo=='Acceso'){
        $respuestas=array('',
        'La solicitud no reúne los requisitos ',
        'No figuran datos personales del interesado',
        'Procede la visualización interesada',
        'Comunicación al Responsable de Tratamientos',
        'Datos personales bloqueados',
        'Denegación de acceso',
        'Otorgamiento de acceso');
    } else if($tipo=='Rectificación'){
        $respuestas=array('',
        'La solicitud no reúne los requisitos ',
        'No figuran datos personales del interesado',
        'Comunicación al Responsable de Tratamientos',
        'Datos personales bloqueados',
        'Denegación de rectificación',
        'Otorgamiento de rectificación');
    } else if($tipo=='Supresión'){
        $respuestas=array('',
        'La solicitud no reúne los requisitos de identificación',
        'No figuran datos personales del interesado',
        'Comunicación al Responsable de Tratamientos',
        'Datos personales bloqueados',
        'Denegación de supresión',
        'Otorgamiento de supresión');
    } else if($tipo=='Limitación del tratamiento'){
        $respuestas=array('',
        'La solicitud no reúne los requisitos de identificación',
        'No figuran datos personales del interesado',
        'Comunicación al Responsable de Tratamientos',
        'Datos personales bloqueados',
        'Denegación a la limitación del tratamiento solicitada',
        'Otorgamiento a la limitación del tratamiento solicitada');
    }
    while($datos=mysql_fetch_assoc($listado)){
        $ref=$datos['referencia']<10?'0'.$datos['referencia']:$datos['referencia'];
        $otrosDatos="";
        if($datos['respuesta']==7 && $datos['otrosDatos']!=''){
            $otrosDatos=' - '.$datos['otrosDatos'];
        }
        $tablaEntradas.='<w:tr w:rsidR="00304DA2" w14:paraId="46834B5C" w14:textId="77777777" w:rsidTr="00923E21"><w:trPr><w:trHeight w:val="382"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="675" w:type="dxa"/></w:tcPr><w:p w14:paraId="44905F05" w14:textId="0404D5BA" w:rsidR="00304DA2" w:rsidRDefault="00F55262" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4253" w:type="dxa"/></w:tcPr><w:p w14:paraId="719A1336" w14:textId="438326CD" w:rsidR="00304DA2" w:rsidRDefault="00F55262" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$datos['interesado'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/></w:tcPr><w:p w14:paraId="3EDFFB00" w14:textId="0912F533" w:rsidR="00304DA2" w:rsidRDefault="00F55262" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($datos['fechaRecepcion']).'</w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/></w:tcPr><w:p w14:paraId="31ACF68F" w14:textId="224FADF2" w:rsidR="00304DA2" w:rsidRPr="002B0C55" w:rsidRDefault="00F55262" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($datos['fechaRespuesta']).'</w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4252" w:type="dxa"/></w:tcPr><w:p w14:paraId="537B9155" w14:textId="1873A1B8" w:rsidR="00304DA2" w:rsidRDefault="00F55262" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$respuestas[$datos['respuesta']].'</w:t></w:r></w:p></w:tc></w:tr>';

        $i++;       
    } 
    $total=$i-1;
    $tablaEntradas.='</w:tbl>'.pieTablaDerechos($total);

    $documento->setValue("tablaEntradas",utf8_decode($tablaEntradas));

    $documento->save('../documentos/derechos/'.$fichero);
}

function sanearCaracteresDerechos($texto){
    $texto = str_replace( '&', '&#38;', $texto);
    $texto = str_replace( '–', '&#45;', $texto);
    $texto = str_replace( '<br />', '<w:br/>', $texto);
    $texto = str_replace( '"', '', $texto);
    return $texto;
}

function reemplazarLogoDerechos($documento,$datos,$imagen='image1.png',$campo='ficheroLogo'){
    $hayLogo=str_replace('../documentos/logos-clientes/', '', $datos[$campo]);
    if($hayLogo!='NO' && $hayLogo!=''){
        $logo = '../documentos/logos-clientes/'.$datos[$campo];
        $nuevo_logo = '../documentos/logos-clientes/'.$imagen;  
        if (!copy($logo, $nuevo_logo)) {
            echo "Error al copiar $fichero...\n";
        }   
        $documento->replaceImage('../documentos/logos-clientes/',$imagen);  
    }
}

function fechaTablaDerechos($fecha,$texto){
    return '<w:p w14:paraId="1060A2A2" w14:textId="7A44C6D1" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="00397BF1" w:rsidP="0016753F"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">REGISTRO DE '.$texto.' A </w:t></w:r><w:r w:rsidR="00D303CF"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$fecha.'</w:t></w:r></w:p>';
}

function pieTablaDerechos($total){
    return '<w:p w14:paraId="2CF98860" w14:textId="77777777" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="001F0F57" w:rsidP="001F0F57"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="right"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>Nº total de re</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">gistros </w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$total.'</w:t></w:r></w:p>';
}

function compruebaAcceso($codigo) {
    
    if ($codigo != $_SESSION['codigoU']) {

        $archivo = fopen("compruebaAcceso.log", "a");
        fwrite($archivo, "---------------".date('Y-m-D')."---".date('H:m')."---------");
        fwrite($archivo, "\n");
        fwrite($archivo, $codigo.' --- '.$_SESSION['codigoU']);
        fwrite($archivo, "\n");
        fclose($archivo);

        header("HTTP/1.1 403");
        exit;
    }
}

// función para comprobar si el registro pertenece al usuario. Para evitar que clientes entren en registros q no son suyos al 
// editar la barra de direcciones
function compruebaCliente($codigo, $tabla) {
    global $_SESSION;
    $res = false;

    conexionBD();
    $registro = consultaBD("SELECT codigoCliente FROM $tabla WHERE codigo = $codigo", false, true);
    $cliente  = obtenerCodigoCliente(false);
    cierraBD();

    $res = $registro['codigoCliente'] == $cliente ? true : false;

    return $res;
}

//Fin parte de comunicación interna
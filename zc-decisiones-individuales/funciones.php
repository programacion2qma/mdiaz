<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesInteresados(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaInteresado();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaInteresado();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('clientes_interesados_nodecisiones');
	}

	mensajeResultado('nombre', $res, 'Interesado');
    mensajeResultado('elimina', $res, 'Interesado', true);
}

function insertaInteresado(){
	$res=true;
	responsable();
	$res=insertaDatos('clientes_interesados_nodecisiones');
	return $res;
}

function actualizaInteresado(){
	$res=true;
	responsable();
	$res=actualizaDatos('clientes_interesados_nodecisiones');
	return $res;
}

function responsable(){
	$_POST['responsable'] = '';
	$_POST['responsableDireccion'] = '';
	if($_POST['tipoRespuesta'] == 'supuesto3' && $_POST['selectResponsable'] != 'NULL') {
		$i = $_POST['selectResponsable'];
		$_POST['responsable'] = $_POST['responsable'.$i];
		$_POST['responsableDireccion'] = $_POST['direccion'.$i];
	}
}

function listadoInteresados(){
	global $_CONFIG;

	$columnas = array(
		'referencia',
		'nombre',
		'fechaRecep',
		'fechaRespu'
	);
	
	$cliente = obtenerCodigoCliente(true);
	$having  = obtieneWhereListado("HAVING codigoCliente=".$cliente, $columnas);
	$orden   = obtieneOrdenListado($columnas);
	$limite  = obtieneLimitesListado();
	
	$sql = "SELECT 
				*,
				DATE_FORMAT(fechaRecepcion, '%d/%m/%Y') AS fechaRecep,
				DATE_FORMAT(fechaRespuesta, '%d/%m/%Y') AS fechaRespu
			FROM
				clientes_interesados_nodecisiones
			$having";
	
	conexionBD();
	$consulta = consultaBD($sql." $orden $limite;");
	$consultaPaginacion=consultaBD($sql);
	cierraBD();

	$res = inicializaArrayListado($consulta, $consultaPaginacion);
	while($datos = mysql_fetch_assoc($consulta)){

		$fila=array(
			"<div class='centro'>".$datos['referencia']."</div>",
			$datos['nombre'],
			"<div class=centro>".$datos['fechaRecep']."</div>",
			"<div class=centro>".$datos['fechaRespu']."</div>",
			botonListado($datos),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function botonListado($datos) {

	$opciones = array('Detalles');
	$enlaces  = array("zc-decisiones-individuales/gestion.php?codigo=".$datos['codigo']);
	$iconos   = array('icon-search-plus');
	$clase    = array(0);

	if(isset($datos['tipoRespuesta']) && $datos['tipoRespuesta'] != '') {
		array_push($opciones, 'Descargar respuesta');
		array_push($enlaces, "zc-decisiones-individuales/generaRespuesta.php?codigo=".$datos['codigo']);
		array_push($iconos, 'icon-cloud-download');
		array_push($clase, 2);
	}

	if (count($opciones) > 1) {
		$res = botonAcciones($opciones, $enlaces, $iconos, $clase);
	} else {
		$res = creaBotonDetalles($enlaces[0]);
	}

	return $res;
}

function gestionInteresados() {

	$camposSelectNombres = array(
		'',
		'La solicitud no reúne requisitos de identificación -artículo 11 y 12.6 RGPD-',
		'No figuran datos personales del interesado',		
		'Comunicación al Responsable de Tratamientos',
		'Datos personales bloqueados',
		'Denegación de solicitud de ejercicio del derecho a no ser objeto de decisiones individuales automatizadas -artículo 22 RGPD-',
		'Otorgamiento de solicitud de ejercicio del derecho a no ser objeto de decisiones individuales automatizadas -artículo 22 RGPD-',		
	);
	$camposSelectvalores = array(
		'',
		'supuesto1',
		'supuesto2',
		'supuesto3',
		'supuesto4',
		'supuesto5',
		'supuesto6',
	);

	operacionesInteresados();

	abreVentanaGestion('Interesados que ejercen su derecho a no ser objeto de decisiones individuales automatizada','?','','icon-edit','margenAb');
	
		if(isset($_GET['codigo'])) {
			if(compruebaCliente($_GET['codigo'], 'clientes_interesados_nodecisiones')) {
				$datos = compruebaDatos('clientes_interesados_nodecisiones');
				$referencia = $datos['referencia'];
			}
		} else {
			$datos = false;
			$consulta = consultaBD("SELECT IFNULL(MAX(CAST(referencia AS DECIMAL)), 0) + 1 AS n FROM clientes_interesados_nodecisiones WHERE codigoCliente = ".obtenerCodigoCliente(true), true, true);
			$referencia = $consulta['n'];
		}
			
		abreColumnaCampos();
			campoOculto($datos, 'codigoCliente', obtenerCodigoCliente(true));
			campoTexto('referencia', 'Referencia', $referencia, 'input-mini');
			campoTexto('nombre', 'Nombre y apellidos', $datos, 'span7');
			campoTexto('nif', 'NIF', $datos, 'input-small');		
			campoTexto('nombre_representante', 'Representante nombre y apellidos', $datos, 'span7');
			campoTexto('nif_representante', 'Representante NIF', $datos, 'input-mini');
			campoTexto('direccion_notificaciones', 'Dirección a efecto de notificaciones', $datos, 'span7');
   			campoTexto('email', 'Correo electrónico', $datos, 'span7');
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoFecha('fechaRecepcion', 'Fecha de Recepción', $datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFecha('fechaRespuesta', 'Fecha de Respuesta', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoRadio('medioElectronico', 'La solicitud se ha presentado por medios electrónicos', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos();
			echo '<div id="divOtroMedio" class="hide">';
				campoRadio('otroMedioComunicacion','El interesado solicita que se le responda por otros medios diferentes al correo electrónico',$datos);
			echo '</div>';
		cierraColumnaCampos();

		abreColumnaCampos();
			echo '<div id="divOtroMedioTexto" class="hide">';
				campoTexto('otroMedioTexto','Indicar',$datos);
			echo '</div>';
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoSelect('tipoRespuesta', 'Respuesta', $camposSelectNombres, $camposSelectvalores, $datos, 'selectpicker span6 show-tick');
		cierraColumnaCampos(true);

		abreColumnaCampos('span3 divInformacion');
			
		cierraColumnaCampos(true);

		abreColumnaCampos('span3 divResponsable hide');
			// Esto viene heredado del desarrollador antiguo, los responsables no tienen tabla propia
			// tabla y tenemos que recuperar los datos de los registros
			$responsables = array();
			$direcciones  = array();
			
			conexionBD();
		    $declaraciones = consultaBD('SELECT * FROM declaraciones WHERE codigoCliente = '.obtenerCodigoCliente(false));
			while ($d = mysql_fetch_assoc($declaraciones)){
				if (!in_array($d['n_razon'], $responsables)) {
					array_push($responsables, $d['n_razon']);
					array_push($direcciones, $d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$d['provincia_responsableFichero']);
				}

				$otros = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion = '.$d['codigo']);
				while($o = mysql_fetch_assoc($otros)){
					if ($o['razonSocial'] != '' && !in_array($o['razonSocial'], $responsables)) {
						array_push($responsables, $o['razonSocial']);
						array_push($direcciones, $o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$o['provincia']);
					}
				}
			}
			cierraBD();
			
			$nombres = array('');
			$valores = array('NULL');
			foreach ($responsables as $key => $value) {
				campoOculto($value, 'responsable'.$key);
				array_push($nombres, $value);
				array_push($valores, $key);
			}
			foreach ($direcciones as $key => $value) {
				campoOculto($value, 'direccion'.$key);
			}
			if($datos && $datos['responsable'] != ''){
				campoDato('Responsable de tratamiento', $datos['responsable']);
			}
			campoSelect('selectResponsable','Selecciona responsable',$nombres,$valores);
		cierraColumnaCampos(true);

		abreColumnaCampos();
			areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 
		cierraColumnaCampos(true);
		
		abreVentanaModal('información');
			echo "<div id='textoModal'>";
			echo "</div>";
		cierraVentanaModal('','','',false,'Cerrar');				

	cierraVentanaGestion('index.php',true);
}

function textoModalAJAX() {
	$opcion = $_POST['opcion'];
	$texto  = '';

	switch ($opcion) {
		case 'articulo11':
			$texto = "
				<i><b>Artículo 11 RGPD.</b> Tratamiento que no requiere identificación.</i><br>
				<ol>
					<li><i>Si los fines para los cuales un responsable trata datos personales no requieren o ya no requieren la identificación de un interesado por el responsable, este no estará obligado a mantener, obtener o tratar información adicional con vistas a identificar al interesado con la única finalidad de cumplir el presente Reglamento.</i></li>
					<li><i>Cuando, en los casos a que se refiere el apartado 1 del presente artículo, el responsable sea capaz de demostrar que no está en condiciones de identificar al interesado, le informará en consecuencia, de ser posible. En tales casos no se aplicarán los artículos 15 a 20, excepto cuando el interesado, a efectos del ejercicio de sus derechos en virtud de dichos artículos, facilite información adicional que permita su identificación.</i></<li>			
				</ol>
			";
			break;
		case 'articulo12':
			$texto = "
				<i><b>Artículo 12.6 RGPD.</b> 6. Sin perjuicio de lo dispuesto en el artículo 11, cuando el responsable del tratamiento tenga dudas razonables en relación con la identidad de la persona física que cursa la solicitud a que se refieren los artículos 15 a 21, podrá solicitar que se facilite la información adicional necesaria para confirmar la identidad del interesado.</i>
			";
			break;
		case 'articulo22':
			$texto = "
				<i><b>Artículo 22  RGPD.</b> Decisiones individuales automatizadas, incluida la elaboración de perfiles.</i>
				<ol>
					<li><i>Todo interesado tendrá derecho a no ser objeto de una decisión basada únicamente en el tratamiento automatizado, incluida la elaboración de perfiles, que produzca efectos jurídicos en él o le afecte significativamente de modo similar.</i></li>
			 		<li><i>El apartado 1 no se aplicará si la decisión:</i></li> 
					<ol style='list-style-type:lower-latin;'>
						<li><i>es necesaria para la celebración o la ejecución de un contrato entre el interesado y un responsable del tratamiento;</i></li>
			 			<li><i>está autorizada por el Derecho de la Unión o de los Estados miembros que se aplique al responsable del tratamiento y que establezca asimismo medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado, o</i></li>
			 			<li><i>se basa en el consentimiento explícito del interesado.</i></li>
					</ol>
			 		<li><i>En los casos a que se refiere el apartado 2, letras a) y c), el responsable del tratamiento adoptará las medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado, como mínimo el derecho a obtener intervención humana por parte del responsable, a expresar su punto de vista y a impugnar la decisión. </i></li>
					<li><i>Las decisiones a que se refiere el apartado 2 no se basarán en las categorías especiales de datos personales contempladas en el artículo 9, apartado 1, salvo que se aplique el artículo 9, apartado 2, letra a) o g), y se hayan tomado medidas adecuadas para salvaguardar los derechos y libertades y los intereses legítimos del interesado.</i></li>
				<ol>
			";
			break;
		case 'articulo15':
			$texto = "
				<i><b>Artículo 15.2 LOPD-GDD.</b> Cuando la supresión derive del ejercicio del derecho de oposición con arreglo al artículo 21.2 
				del Reglamento (UE) 2016/679, el responsable podrá conservar los datos identificativos del afectado necesarios con el fin de impedir 
				tratamientos futuros para fines de mercadotecnia directa.</i>
			";

		default:
			break;	
	}

	echo $texto;

}

function valoresSelectResponsables() {
	$responsables = array();
	$direcciones  = array();
	
	conexionBD();
	$declaraciones = consultaBD('SELECT * FROM declaraciones WHERE codigoCliente = '.obtenerCodigoCliente(false));
	while ($d = mysql_fetch_assoc($declaraciones)){
		if(!in_array($d['n_razon'], $responsables)){
			array_push($responsables, $d['n_razon']);
			array_push($direcciones, $d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$d['provincia_responsableFichero']);
		}
		$otros = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion = '.$d['codigo']);
		while($o = mysql_fetch_assoc($otros)){
			if($o['razonSocial'] != '' && !in_array($o['razonSocial'], $responsables)){
				array_push($responsables, $o['razonSocial']);
				array_push($direcciones, $o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$o['provincia']);
			}
		}
	}
	cierraBD();
		
	return $responsables;
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
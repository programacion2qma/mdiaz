<?php

    include_once('funciones.php');
    compruebaSesion();
    
    require_once('../../api/phpword/PHPWord.php');

    $documento = respuestaDecisionesIndividuales($_GET['codigo']);

    header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header("Content-Disposition: attachment; filename=".$documento);
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/derechos/'.$documento);

    function respuestaDecisionesIndividuales($codigo) {
        global $_CONFIG;
        
        $sql = "SELECT 
                    c.razonSocial AS cliente,
                    CONCAT(c.domicilio, ', CP: ', c.cp, ' ', c.localidad, ' (', c.provincia, ')') AS direccionCliente,
                    cin.nombre AS interesado,
                    cin.fechaRecepcion,
                    cin.fechaRespuesta,
                    cin.direccion_notificaciones AS direccionInteresado,
                    cin.nombre_representante AS representante,
                    cin.tipoRespuesta,
                    cin.referencia,
                    cin.observaciones,
                    cin.responsable,
                    cin.responsableDireccion,
                    cin.codigoCliente
                FROM
                    clientes c,
                    clientes_interesados_nodecisiones cin 
                WHERE 
                    c.codigo = cin.codigoCliente
                AND 
                    cin.codigo =".$codigo.";";

        $datos = consultaBD($sql, true, true);

        $sql = "SELECT 
                    *
                FROM 
                    trabajos t                 
                WHERE 
                    t.codigoCliente = ".$datos['codigoCliente']."
                ORDER BY codigo DESC LIMIT 1;";
        $trabajo = consultaBD($sql, true, true);
        $formulario = recogerFormularioServicios($trabajo);

        switch ($datos['tipoRespuesta']) {
            case 'supuesto1':
                $res = respuestaSupuesto1($datos, $formulario);
                break;
            case 'supuesto2':
                $res = respuestaSupuesto2($datos, $formulario);
                break;
            case 'supuesto3':
                $res = respuestaSupuesto3($datos, $formulario);
                break;
            case 'supuesto4':
                $res = respuestaSupuesto4($datos, $formulario);
                break;
            case 'supuesto5':
                $res = respuestaSupuesto5($datos, $formulario);
                break;
            case 'supuesto6':
                $res = respuestaSupuesto6($datos, $formulario);
                break;
            default:
                break;
        }

        return $res;
    }

    function respuestaSupuesto1($datos, $formulario) {         
        $direccion = $formulario['pregunta4'].', CP '.$formulario['pregunta10'].', '.$formulario['pregunta5'].', '.$formulario['pregunta11'];
        $fecha     = isset($datos['fechaRespuesta']) && 
                     $datos['fechaRespuesta'] != ''  && 
                     $datos['fechaRespuesta'] != '0000-00-00' ? 
                     formateaFechaWeb($datos['fechaRespuesta']) : date('d/m/Y');
        $ref       = $datos['referencia'] < 10 ? '0'.$datos['referencia'] : $datos['referencia'];
        $fichero   = 'respuesta.docx';
        $PHPWord   = new PHPWord();
        $documento = $PHPWord->loadTemplate('../documentos/derechos/plantilla_R_DI_S1.docx');

        $documento->setValue("ref", $ref);
        $documento->setValue("cliente", utf8_decode(sanearCaracteresDerechos(trim($formulario['pregunta3']))));               
        $documento->setValue("direccionCliente", utf8_decode($direccion));
        $documento->setValue("interesado", utf8_decode($datos['interesado']));
        $documento->setValue("fecha", $fecha);

        if (isset($datos['representante']) && $datos['representante'] != '') {
            $texto = '<w:p w:rsidR="007A41F7" w:rsidRPr="00D7443B" w:rsidRDefault="007A41F7" w:rsidP="007A41F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r w:rsidRPr="00D7443B"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por '.$datos['representante'].',  en nombre y representación de D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'</w:t></w:r><w:r w:rsidR="00975283"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>.</w:t></w:r></w:p>';
        }
        else {
            $texto = '<w:p w:rsidR="007A41F7" w:rsidRPr="00D7443B" w:rsidRDefault="007A41F7" w:rsidP="007A41F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r w:rsidRPr="00D7443B"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'</w:t></w:r><w:r w:rsidR="00975283"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>.</w:t></w:r></w:p>';
        }

        $documento->setValue("texto", utf8_decode($texto));

        if (isset($datos['observaciones']) && $datos['observaciones'] != '') {
            $observaciones = '<w:p w:rsidR="004B7B67" w:rsidRDefault="00C17A5A" w:rsidP="004B7B67"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>Observaciones: '.$datos['observaciones'].'</w:t></w:r></w:p>';
        }
        else {
            $observaciones = '';
        }

        $documento->setValue("observaciones", utf8_decode($observaciones));
        
        $documento->save('../documentos/derechos/'.$fichero);
        return $fichero;
    }

    function respuestaSupuesto2($datos, $formulario) {
        $direccion = $formulario['pregunta4'].', CP '.$formulario['pregunta10'].', '.$formulario['pregunta5'].', '.$formulario['pregunta11'];
        $fecha     = isset($datos['fechaRespuesta']) && 
                     $datos['fechaRespuesta'] != ''  && 
                     $datos['fechaRespuesta'] != '0000-00-00' ? 
                     formateaFechaWeb($datos['fechaRespuesta']) : date('d/m/Y');
        $ref       = $datos['referencia'] < 10 ? '0'.$datos['referencia'] : $datos['referencia'];
        $fichero   = 'respuesta.docx';
        $PHPWord   = new PHPWord();
        $documento = $PHPWord->loadTemplate('../documentos/derechos/plantilla_R_DI_S2.docx');

        $documento->setValue("ref", $ref);
        $documento->setValue("cliente", utf8_decode(sanearCaracteresDerechos(trim($formulario['pregunta3']))));
        $documento->setValue("direccionCliente", utf8_decode($direccion));
        $documento->setValue("interesado", utf8_decode($datos['interesado']));
        $documento->setValue("fecha", $fecha);

        if (isset($datos['representante']) && $datos['representante'] != '') {
            $texto = '<w:p w:rsidR="007A41F7" w:rsidRPr="00D7443B" w:rsidRDefault="00BD3AA0" w:rsidP="007A41F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por '.$datos['representante'].' en nombre y representación de D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }
        else {
            $texto = '<w:p w:rsidR="007A41F7" w:rsidRPr="00D7443B" w:rsidRDefault="00BD3AA0" w:rsidP="007A41F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }

        $documento->setValue("texto", utf8_decode($texto));

        if (isset($datos['observaciones']) && $datos['observaciones'] != '') {
            $observaciones = '<w:p w:rsidR="004B7B67" w:rsidRDefault="00C17A5A" w:rsidP="004B7B67"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>Observaciones: '.$datos['observaciones'].'</w:t></w:r></w:p>';
        }
        else {
            $observaciones = '';
        }

        $documento->setValue("observaciones", utf8_decode($observaciones));
        
        $documento->save('../documentos/derechos/'.$fichero);
        return $fichero;
    }

    function respuestaSupuesto3($datos, $formulario) {  
        $direccion = $formulario['pregunta4'].', CP '.$formulario['pregunta10'].', '.$formulario['pregunta5'].', '.$formulario['pregunta11'];  
        $fecha     = isset($datos['fechaRespuesta']) && 
        $datos['fechaRespuesta'] != ''  && 
        $datos['fechaRespuesta'] != '0000-00-00' ? 
        formateaFechaWeb($datos['fechaRespuesta']) : date('d/m/Y');
        $ref       = $datos['referencia'] < 10 ? '0'.$datos['referencia'] : $datos['referencia'];
        $fichero   = 'respuesta.docx';
        $PHPWord   = new PHPWord();
        $documento = $PHPWord->loadTemplate('../documentos/derechos/plantilla_R_DI_S3.docx');

        $documento->setValue("ref", $ref);
        $documento->setValue("cliente", utf8_decode(sanearCaracteresDerechos(trim($formulario['pregunta3']))));
        $documento->setValue("direccionCliente", utf8_decode($direccion));
        $documento->setValue("interesado", utf8_decode($datos['interesado']));
        $documento->setValue("responsable", utf8_decode($datos['responsable']));
        $documento->setValue("responsableDireccion", utf8_decode($datos['responsableDireccion']));
        $documento->setValue("fecha", $fecha);

        if (isset($datos['representante']) && $datos['representante'] != '') {
            $texto1 = '<w:p w:rsidR="007A41F7" w:rsidRPr="002129E9" w:rsidRDefault="00606EFF" w:rsidP="007A41F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="002129E9"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.sanearCaracteresDerechos(trim($formulario['pregunta3'])).', en concepto de encargado del tratamiento de datos personales, de los cuales es responsable del tratamiento '.$datos['responsable'].', mediante el presente escrito informamos al responsable del tratamiento de la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por '.$datos['representante'].', en nombre y representación de D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'</w:t></w:r><w:r w:rsidR="007A41F7" w:rsidRPr="002129E9"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>.</w:t></w:r></w:p>';

            $texto2 = '<w:p w:rsidR="00401F0C" w:rsidRPr="002129E9" w:rsidRDefault="009848B3" w:rsidP="00401F0C"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="002129E9"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por '.$datos['responsable'].', en nombre y representación de D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }
        else {
            $texto1 = '<w:p w:rsidR="007A41F7" w:rsidRPr="002129E9" w:rsidRDefault="00606EFF" w:rsidP="007A41F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="002129E9"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.sanearCaracteresDerechos(trim($formulario['pregunta3'])).', en concepto de encargado del tratamiento de datos personales, de los cuales es responsable del tratamiento '.$datos['responsable'].', mediante el presente escrito informamos al responsable del tratamiento de la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'</w:t></w:r><w:r w:rsidR="007A41F7" w:rsidRPr="002129E9"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>.</w:t></w:r></w:p>';

            $texto2 = '<w:p w:rsidR="00401F0C" w:rsidRPr="002129E9" w:rsidRDefault="009848B3" w:rsidP="00401F0C"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="002129E9"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por '.$datos['responsable'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }

        $documento->setValue("texto1", utf8_decode($texto1));
        $documento->setValue("texto2", utf8_decode($texto2));

        if (isset($datos['observaciones']) && $datos['observaciones'] != '') {
            $observaciones = '<w:p w:rsidR="004B7B67" w:rsidRDefault="00C17A5A" w:rsidP="004B7B67"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>Observaciones: '.$datos['observaciones'].'</w:t></w:r></w:p>';
        }
        else {
            $observaciones = '';
        }

        $documento->setValue("observaciones", utf8_decode($observaciones));
        
        $documento->save('../documentos/derechos/'.$fichero);
        return $fichero;
    }

    function respuestaSupuesto4($datos, $formulario) {
        $direccion = $formulario['pregunta4'].', CP '.$formulario['pregunta10'].', '.$formulario['pregunta5'].', '.$formulario['pregunta11'];
        $fecha     = isset($datos['fechaRespuesta']) && 
                     $datos['fechaRespuesta'] != ''  && 
                     $datos['fechaRespuesta'] != '0000-00-00' ? 
                     formateaFechaWeb($datos['fechaRespuesta']) : date('d/m/Y');
        $ref       = $datos['referencia'] < 10 ? '0'.$datos['referencia'] : $datos['referencia'];
        $fichero   = 'respuesta.docx';
        $PHPWord   = new PHPWord();
        $documento = $PHPWord->loadTemplate('../documentos/derechos/plantilla_R_DI_S4.docx');

        $documento->setValue("ref", $ref);
        $documento->setValue("cliente", utf8_decode(sanearCaracteresDerechos(trim($formulario['pregunta3']))));
        $documento->setValue("direccionCliente", utf8_decode($direccion));
        $documento->setValue("interesado", utf8_decode($datos['interesado']));
        $documento->setValue("fecha", $fecha);

        if (isset($datos['representante']) && $datos['representante'] != '') {
            $texto = '<w:p w:rsidR="00EE5223" w:rsidRPr="00B5728A" w:rsidRDefault="0053122E" w:rsidP="00EE5223"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="00B5728A"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por D./ª '.$datos['representante'].', en nombre y representación de D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en C/ '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }
        else {
            $texto = '<w:p w:rsidR="00EE5223" w:rsidRPr="00B5728A" w:rsidRDefault="0053122E" w:rsidP="00EE5223"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="00B5728A"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en C/ '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }

        $documento->setValue("texto", utf8_decode($texto));

        if (isset($datos['observaciones']) && $datos['observaciones'] != '') {
            $observaciones = '<w:p w:rsidR="004B7B67" w:rsidRDefault="00C17A5A" w:rsidP="004B7B67"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>Observaciones: '.$datos['observaciones'].'</w:t></w:r></w:p>';
        }
        else {
            $observaciones = '';
        }

        $documento->setValue("observaciones", utf8_decode($observaciones));
        
        $documento->save('../documentos/derechos/'.$fichero);
        return $fichero;
    }

    function respuestaSupuesto5($datos, $formulario) {
        $direccion = $formulario['pregunta4'].', CP '.$formulario['pregunta10'].', '.$formulario['pregunta5'].', '.$formulario['pregunta11'];
        $fecha     = isset($datos['fechaRespuesta']) && 
                     $datos['fechaRespuesta'] != ''  && 
                     $datos['fechaRespuesta'] != '0000-00-00' ? 
                     formateaFechaWeb($datos['fechaRespuesta']) : date('d/m/Y');
        $ref       = $datos['referencia'] < 10 ? '0'.$datos['referencia'] : $datos['referencia'];
        $fichero   = 'respuesta.docx';
        $PHPWord   = new PHPWord();
        $documento = $PHPWord->loadTemplate('../documentos/derechos/plantilla_R_DI_S5.docx');

        $documento->setValue("ref", $ref);
        $documento->setValue("cliente", utf8_decode(sanearCaracteresDerechos(trim($formulario['pregunta3']))));
        $documento->setValue("direccionCliente", utf8_decode($direccion));
        $documento->setValue("interesado", utf8_decode($datos['interesado']));
        $documento->setValue("fecha", $fecha);

        if (isset($datos['representante']) && $datos['representante'] != '') {
            $texto = '<w:p w:rsidR="00923001" w:rsidRPr="0076387B" w:rsidRDefault="007953C5" w:rsidP="00923001"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="0076387B"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por D./ª '.$datos['representante'].', en nombre y representación de D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }
        else {
            $texto = '<w:p w:rsidR="00923001" w:rsidRPr="0076387B" w:rsidRDefault="007953C5" w:rsidP="00923001"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="0076387B"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }

        $documento->setValue("texto", utf8_decode($texto));

        if (isset($datos['observaciones']) && $datos['observaciones'] != '') {
            $observaciones = '<w:p w:rsidR="004B7B67" w:rsidRDefault="00C17A5A" w:rsidP="004B7B67"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>Observaciones: '.$datos['observaciones'].'</w:t></w:r></w:p>';
        }
        else {
            $observaciones = '';
        }

        $documento->setValue("observaciones", utf8_decode($observaciones));
        
        $documento->save('../documentos/derechos/'.$fichero);
        return $fichero;
    }

    function respuestaSupuesto6($datos, $formulario) {
        $direccion = $formulario['pregunta4'].', CP '.$formulario['pregunta10'].', '.$formulario['pregunta5'].', '.$formulario['pregunta11'];
        $fecha     = isset($datos['fechaRespuesta']) && 
                     $datos['fechaRespuesta'] != ''  && 
                     $datos['fechaRespuesta'] != '0000-00-00' ? 
                     formateaFechaWeb($datos['fechaRespuesta']) : date('d/m/Y');
        $ref       = $datos['referencia'] < 10 ? '0'.$datos['referencia'] : $datos['referencia'];
        $fichero   = 'respuesta.docx';
        $PHPWord   = new PHPWord();
        $documento = $PHPWord->loadTemplate('../documentos/derechos/plantilla_R_DI_S6.docx');

        $documento->setValue("ref", $ref);
        $documento->setValue("cliente", utf8_decode(sanearCaracteresDerechos(trim($formulario['pregunta3']))));
        $documento->setValue("direccionCliente", utf8_decode($direccion));
        $documento->setValue("interesado", utf8_decode($datos['interesado']));
        $documento->setValue("fecha", $fecha);

        if (isset($datos['representante']) && $datos['representante'] != '') {
            $texto = '<w:p w:rsidR="007A41F7" w:rsidRPr="008D7D6D" w:rsidRDefault="00EB4933" w:rsidP="007A41F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008D7D6D"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por D./ª '.$datos['representante'].', en nombre y representación de D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }
        else {
            $texto = '<w:p w:rsidR="007A41F7" w:rsidRPr="008D7D6D" w:rsidRDefault="00EB4933" w:rsidP="007A41F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008D7D6D"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">El presente escrito es para dar respuesta a la solicitud de fecha '.formateaFechaWeb($datos['fechaRecepcion']).' ejercitando el derecho a no ser objeto de decisiones individuales automatizadas (incluida la elaboración de perfiles), realizada por D./ª '.$datos['interesado'].', con domicilio a efectos de notificaciones en '.$datos['direccionInteresado'].'.</w:t></w:r></w:p>';
        }

        $documento->setValue("texto", utf8_decode($texto));

        if (isset($datos['observaciones']) && $datos['observaciones'] != '') {
            $observaciones = '<w:p w:rsidR="004B7B67" w:rsidRDefault="00C17A5A" w:rsidP="004B7B67"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cstheme="minorHAnsi"/></w:rPr><w:t>Observaciones: '.$datos['observaciones'].'</w:t></w:r></w:p>';
        }
        else {
            $observaciones = '';
        }

        $documento->setValue("observaciones", utf8_decode($observaciones));
        
        $documento->save('../documentos/derechos/'.$fichero);
        return $fichero;
    }
?>
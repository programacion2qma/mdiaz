<?php

    include_once('funciones.php');
    compruebaSesion();

    require_once('../../api/phpword/PHPWord.php');

    $codigoCliente = obtenerCodigoCliente(true);

    // $archivo = new PHPWord();
    wordListado($codigoCliente);

    header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header("Content-Disposition: attachment; filename=REGISTRO DECISIONES INDIVIDUALES AUTOMATIZADAS.docx");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/derechos/R.docx');

function wordListado($codigoCliente) {
    
    global $_CONFIG;

    $supuestoTexto = array(
        '' => '',
		'supuesto1' => 'La solicitud no reúne requisitos de identificación',
		'supuesto2' => 'No figuran datos personales del interesado',		
		'supuesto3' => 'Comunicación al Responsable de Tratamientos',
		'supuesto4' => 'Datos personales bloqueados',
		'supuesto5' => 'Denegación de solicitud de ejercicio del derecho a no ser objeto de decisiones individuales automatizadas',
		'supuesto6' => 'Otorgamiento de solicitud de ejercicio del derecho a no ser objeto de decisiones individuales automatizadas',
	);
    
    $titulo    = "REGISTRO DE SOLICITUDES DE EJERCICIO DEL DERECHO A NO SER OBJETO DE DECISIONES INDIVIDUALES AUTOMATIZADAS, INCLUIDA ELABORACIÓN DE PERFILES";    $fichero   = "R.docx";    
    $cliente   = datosRegistro('clientes', $codigoCliente);
    $PHPWord   = new PHPWord();
    $documento = $PHPWord->loadTemplate('../documentos/derechos/plantilla_listado.docx');    
        
    $documento->setValue("titulo", utf8_decode($titulo));  
    $documento->setValue("cliente", utf8_decode(sanearCaracteresDerechos($cliente['razonSocial'])));      
    $documento->setValue("fecha", date("d/m/Y"));

    reemplazarLogoDerechos($documento, $cliente,'image2.png'); 

    $texto = '<w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>REGISTRO DE SOLICITUDES DE EJERCICIO DEL DERECHO A NO SER OBJETO DE DECISIONES INDIVIDUALES AUTOMATIZADAS, INCLUIDA ELABORACIÓN DE PERFILES, RECIBIDAS '.date('d-m-Y').'</w:t></w:r></w:p>';

    $tabla = '<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1142"/><w:gridCol w:w="2577"/><w:gridCol w:w="3361"/><w:gridCol w:w="3222"/><w:gridCol w:w="3093"/></w:tblGrid><w:tr w:rsidR="00B92173" w:rsidRPr="008F5EA5" w:rsidTr="00990928"><w:trPr><w:tblHeader/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1142" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w:rsidR="00397BF1" w:rsidRPr="008F5EA5" w:rsidRDefault="00971C8A" w:rsidP="00052A6D"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2577" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w:rsidR="00841F33" w:rsidRPr="008F5EA5" w:rsidRDefault="00C90CCB" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>INTERESADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3361" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w:rsidR="00841F33" w:rsidRPr="008F5EA5" w:rsidRDefault="00D51C9E" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA DE RECEPCIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3222" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w:rsidR="00841F33" w:rsidRPr="008F5EA5" w:rsidRDefault="00D51C9E" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA DE RESPUESTA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3093" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w:rsidR="00841F33" w:rsidRPr="008F5EA5" w:rsidRDefault="00D51C9E" w:rsidP="00052A6D"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>RESPUESTA</w:t></w:r></w:p></w:tc></w:tr>';    

    $i = 0;

    $listado = consultaBD("SELECT * FROM clientes_interesados_nodecisiones WHERE codigoCliente = ".$cliente['codigo'].";", true);    
    
    while($item = mysql_fetch_assoc($listado)) {

        $ref = $item['referencia'] < 10 ? '0'.$item['referencia'] : $item['referencia'];
        
        $tabla .= '<w:tr w:rsidR="00D51C9E" w:rsidTr="00990928"><w:tc><w:tcPr><w:tcW w:w="1142" w:type="dxa"/></w:tcPr><w:p w:rsidR="00D51C9E" w:rsidRPr="00753EE7" w:rsidRDefault="00D51C9E" w:rsidP="00127A30"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="00753EE7"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2577" w:type="dxa"/></w:tcPr><w:p w:rsidR="00D51C9E" w:rsidRPr="00990928" w:rsidRDefault="00D51C9E" w:rsidP="00127A30"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="00990928"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.$item['nombre'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3361" w:type="dxa"/></w:tcPr><w:p w:rsidR="00D51C9E" w:rsidRPr="00990928" w:rsidRDefault="00D51C9E" w:rsidP="00127A30"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="00990928"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRecepcion']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3222" w:type="dxa"/></w:tcPr><w:p w:rsidR="00D51C9E" w:rsidRPr="00990928" w:rsidRDefault="00D51C9E" w:rsidP="00127A30"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="00990928"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.formateaFechaWeb($item['fechaRespuesta']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3093" w:type="dxa"/></w:tcPr><w:p w:rsidR="00D51C9E" w:rsidRPr="00990928" w:rsidRDefault="00D51C9E" w:rsidP="00127A30"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="00990928"><w:rPr><w:rFonts w:cstheme="minorHAnsi"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">'.$supuestoTexto[$item['tipoRespuesta']].'</w:t></w:r></w:p></w:tc></w:tr>';

        $i++;       
    } 

    $tabla .= '</w:tbl>';

    $documento->setValue("tabla", utf8_decode($tabla));
    $documento->setValue("i", $i);

    $documento->save('../documentos/derechos/'.$fichero);
}

?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de tutores

function operacionesSoportes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('soportes_lopd_nueva');
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=insertaDatos('soportes_lopd_nueva');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('soportes_lopd_nueva');
	}

	mensajeResultado('codigoCliente',$res,'Tutor');
    mensajeResultado('elimina',$res,'Tutor', true);
}


function listadoSoportes(){
	global $_CONFIG;

	$columnas=array('codigoCliente','nombreSoporteLOPD','contenidoSoporteLOPD','mecanismosAccesoSoporteLOPD','mecanismosAccesoRecursosSoporteLOPD');
	$having=obtieneWhereListadoEntradas("HAVING 1=1 AND",$columnas,false,false,false);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, codigoCliente, nombreSoporteLOPD, contenidoSoporteLOPD, mecanismosAccesoSoporteLOPD, mecanismosAccesoRecursosSoporteLOPD FROM soportes_lopd_nueva $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, codigoCliente, nombreSoporteLOPD, contenidoSoporteLOPD, mecanismosAccesoSoporteLOPD, mecanismosAccesoRecursosSoporteLOPD FROM soportes_lopd_nueva $having;");
	cierraBD();
//echo "SELECT codigo, codigoCliente, tipo, frecuencia, usuarioAutorizado, fechaPrimeraOperacion, fechaDevolucion FROM entradas_periodicas $having $orden $limite;";
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$datosCliente=datosRegistro('clientes',$datos['codigoCliente']);
		$fila=array(
			$datosCliente['razonSocial'],
			$datos['nombreSoporteLOPD'],
			$datos['contenidoSoporteLOPD'],			
			$datos['mecanismosAccesoSoporteLOPD'],
			$datos['mecanismosAccesoRecursosSoporteLOPD'],
        	botonAcciones(array('Detalles'),array('soportes/gestion.php?codigo='.$datos['codigo']),array("icon-search-plus")),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionSoportes(){
	operacionesSoportes();

	abreVentanaGestion('Gestión de Soportes','?','span3');
	$datos=compruebaDatos('soportes_lopd_nueva');

	campoSelectConsulta('codigoCliente','Clientes',"SELECT codigo, razonSocial AS texto FROM clientes ORDER BY razonSocial;",$datos);
	campoTexto('nombreSoporteLOPD','Soporte',$datos);
	campoTexto('marcaSoporteLOPD','Marca/Modelo',$datos);
	campoTexto('serieSoporteLOPD','Nº Serie',$datos);
	campoFecha('fechaSoporteLOPD','Fecha de Adquisición',$datos);
	campoTexto('ubicacionSoporteLOPD','Ubicación',$datos);
	campoTexto('sistemaSoporteLOPD','Sistema operativo',$datos);
	campoTexto('aplicacionesSoporteLOPD','Aplicaciones instaladas',$datos);
	campoTexto('conexionesSoporteLOPD','Conexiones remotas',$datos);
	campoTexto('responsableSoporteLOPD','Responsable',$datos);
	campoTexto('otrosSoporteLOPD','Otros usuarios',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto('contenidoSoporteLOPD','Contenido',$datos);	
	areaTexto('mecanismosAccesoSoporteLOPD','Mecanismos de Acceso al puesto',$datos);		
	areaTexto('mecanismosAccesoRecursosSoporteLOPD','Mecanismos de Acceso a los recursos',$datos);			

	cierraVentanaGestion('index.php',true);
}

function filtroSoportes(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoSelectConsulta(0,'Cliente',"SELECT codigo, razonSocial AS texto FROM clientes ORDER BY razonSocial;");
	campoTexto(1,'Soporte');
	campoTexto(2,'Contenido');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(3,'Mecanismos Acceso Puesto');
	campoTexto(4,'Mecanismos Acceso Recursos');	

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function obtieneNombreTutor(){
	$codigo=$_GET['codigo'];

	$datos=consultaBD("SELECT CONCAT(nombre,' ',apellido1,' ',apellido2) AS nombre FROM tutores WHERE codigo=$codigo",true,true);

	return "<a href='gestion.php?codigo=$codigo'>".$datos['nombre']."</a>";
}

function obtieneNombreListado(){
	$tutor=obtieneNombreTutor();

	$res="que $tutor está tutorizando actualmente";	

	return $res;
}

function listadoCursosTutor($codigoTutor){
	global $_CONFIG;

	$columnas=array('grupos.fechaAlta','accion','modalidad','accionFormativa','grupos.grupo','grupos.numParticipantes','fechaInicio','fechaFin','anulado','razonSocial','codigoTutor','grupos.fechaAlta');
	
	$where=obtieneWhereListado("WHERE tutores_grupo.codigoTutor=$codigoTutor",$columnas);//Utilizo WHERE por la agrupación de clientes y tutores (el HAVING realiza el filtrado TRAS la selección, y el WHERE ANTES)
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, grupos.fechaAlta, accion, modalidad, accionFormativa, grupos.grupo, grupos.numParticipantes, fechaInicio, fechaFin, anulado, razonSocial, codigoTutor, grupos.activo, grupos.codigo AS codigoGrupo
			FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo 
			LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo 
			LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo 
			LEFT JOIN tutores_grupo ON grupos.codigo=tutores_grupo.codigoGrupo 
			$where 
			GROUP BY grupos.codigo";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			formateaFechaWeb($datos['fechaAlta']),
			$datos['accion'],
			$datos['modalidad'],
			$datos['accionFormativa'],
			$datos['grupo'],
			$datos['numParticipantes'],
			formateaFechaWeb($datos['fechaInicio']),
			formateaFechaWeb($datos['fechaFin']),
			"<div class='centro'>".$datos['anulado']."</div>",
			creaBotonDetalles("inicios-grupos/gestion.php?codigo=".$datos['codigoGrupo'],'Ver curso'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function creaBotonesGestionListadoTutor(){
    echo '<a class="btn-floating btn-large btn-default btn-eliminacion noAjax" href="index.php" title="Volver"><i class="icon-chevron-left"></i></a>';
}



function listadoAFTutor($codigoTutor){
	global $_CONFIG;

	$columnas=array('accion','accionFormativa','codigo');
	$having=obtieneWhereListado("WHERE codigoTutor=$codigoTutor",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, accion, accionFormativa 
			FROM acciones_formativas INNER JOIN tutores_accion_formativa ON acciones_formativas.codigo=tutores_accion_formativa.codigoAccionFormativa 
			$having";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['accion'],
			$datos['accionFormativa'],
        	creaBotonDetalles("acciones-formativas/gestion.php?codigo=".$datos['codigo'],'Ver Acción Formativa'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function filtroCursosTutor(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoFecha(0,'Fecha desde');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(11,'Hasta');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de tutores
<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de empleados

function operacionesRiesgos(){
	$res=true;

	if(isset($_POST['codigo'])){
    	$res=actualizaDatos('riesgos');
  	}
  	elseif(isset($_POST['nombre'])){
    	$res=insertaDatos('riesgos');
  	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('riesgos');
	}

	mensajeResultado('nombre',$res,'Riesgos');
    mensajeResultado('elimina',$res,'Riesgos', true);
}

function imprimeRiesgos(){
	global $_CONFIG;

	$codigoS=$_SESSION['codigoU'];
	$consulta=consultaBD("SELECT codigo, nombre,tipo FROM riesgos;", true);

	while($datos=mysql_fetch_assoc($consulta)){
		if($datos['tipo'] == 'RIESGO'){
			$nombre = $datos['nombre'].' (R)';
		} else {
			$nombre = $datos['nombre'].' (O)';
		}
		echo "
		<tr>
			<td> ".$nombre." </td>
        	<td class='td-actions'>
        		<a href='gestion.php?codigo=".$datos['codigo']."' class='btn btn-propio'><i class='icon-search-plus'></i> Detalles</i></a>
        	</td>
        	<td>
                <input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
            </td>
    	</tr>";
	}
}

function gestionRiesgos(){
	operacionesRiesgos();

	abreVentanaGestion('Gestión de riesgos','?');
	$datos=compruebaDatos('riesgos');
	/*if($datos){
		campoOculto(recogerPermiso(26,'checkModificar'),'permisoModificar');
	}*/

	campoTexto('nombre','Nombre',$datos,'span4');
	campoTexto('codigoProceso','Proceso',$datos,'span4');
	//campoSelectConsulta('codigoProceso','Proceso',"SELECT codigo, nombre AS texto FROM indicadores ORDER BY nombre",$datos);
	campoTexto('actividad','Actividad',$datos,'span4');
	campoSelect('tipo','Tipo',array('Riesgo','Oportunidad'),array('RIESGO','OPORTUNIDAD'),$datos);
	areaTexto('descripcion','Descripción',$datos,'areaInforme');

	cierraVentanaGestion('index.php');
}

//Fin parte de empleados



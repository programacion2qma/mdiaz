<?php
  $seccionActiva=25;
  include_once('../cabecera.php');
  
  operacionesIncidenciasSoftware();
  $codigoCliente=$_CONFIG['codigoIncidenciasSoftware'];
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">

        <?php
            creaBotonesGestionIncidenciasSoftware();
        ?>

        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-th-list"></i>
              <h3>Incidencias de software pendientes o en desarrollo</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <th class='nowrap'> Notificación </th>
                    <th> Prioridad </th>
                    <th> Área </th>
                    <th> Descripción </th>
                    <th class='nowrap'> Reso. Prevista </th>
                    <th> Estado </th>
                    <th> </th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeIncidenciasSoftware($codigoCliente);

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>



    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- /contenido --></div>

<?php include_once('../pie.php'); ?>

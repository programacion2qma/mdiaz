<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de informe sobre seguimiento de colaborador

function listadoInforme(){
	$columnas=array('cif','nombre', 'provincia', 'contacto', 'llamadoColaborador', 'visitaColaborador', 'resultadoVisitaColaborador','codigo','codigo');

	$where=obtieneWhereListado("WHERE 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT * FROM colaboradores $where";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['cif'],
			"<a href='../colaboradores/gestion.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'>".$datos['nombre']."</a>",
			$datos['provincia'],
			$datos['contacto'],
			$datos['llamadoColaborador'],
			$datos['visitaColaborador'],
			$datos['resultadoVisitaColaborador'],
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function filtroInforme(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectSiNoFiltro(4,'Llamado');
	campoSelectSiNoFiltro(5,'Visitado');
	campoSelect(6,'Resultado visita',array('','Vendido','Seguimiento','No'),array('','VENDIDO','SEGUIMIENTO','NO'));

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectProvincia(false,2,'Provincia',0,'');
	campoTexto(3,'Contacto');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


//Fin parte de informe sobre seguimiento de colaborador
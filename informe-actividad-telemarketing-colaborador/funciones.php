<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de informe actividad telemarketing/colaborador

function listadoInforme(){
	$columnas=array('cif','razonSocial','llamado','visita','resultadoVisita','importeFormacion','importeServicios','resultadoPreventa','codigoTelemarketing','codigoColaborador','codigoComercial','procedenciaListado');

	$anioEjercicio=obtieneEjercicioParaWhere();
	$having=obtieneWhereListado("HAVING posibleCliente='SI'",$columnas);

	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT clientes.codigo, cif, razonSocial, llamado, visita, resultadoVisita, SUM(grupos.importeVenta) AS importeFormacion, SUM(ventas_servicios.total) AS importeServicios, resultadoPreventa,
			clientes.codigoTelemarketing, clientes.codigoColaborador, comerciales_cliente.codigoComercial, procedenciaListado, posibleCliente
			FROM clientes LEFT JOIN clientes_grupo ON clientes.codigo=clientes_grupo.codigoCliente
			LEFT JOIN grupos ON clientes_grupo.codigoGrupo=grupos.codigo
			LEFT JOIN ventas_servicios ON clientes.codigo=ventas_servicios.codigoCliente
			LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente AND comerciales_cliente.anio='$anioEjercicio'
			GROUP BY clientes.codigo
			$having";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['cif'],
			"<a href='../posibles-clientes/gestion.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'>".$datos['razonSocial']."</a>",
			$datos['llamado'],
			$datos['visita'],
			$datos['resultadoVisita'],
			"<div class='pagination-right'>".formateaNumeroWebPosibleNULL($datos['importeFormacion'])."</div>",
			"<div class='pagination-right'>".formateaNumeroWebPosibleNULL($datos['importeServicios'])."</div>",
			$datos['resultadoPreventa'],
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function formateaNumeroWebPosibleNULL($numero){
	$res='';

	if($numero!=NULL){
		$res=formateaNumeroWeb($numero).' €';
	}

	return $res;
}

function filtroInforme(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoSelectConsulta(8,'Telemarketing',"SELECT codigo, nombre AS texto FROM telemarketing WHERE activo='SI' ORDER BY nombre");
	campoSelectConsulta(9,'Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre");
	campoSelectConsulta(10,'Comercial',"SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre");
	campoSelectSiNoFiltro(2,'Llamado');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(3,'Visita');
	campoSelectSiNoFiltro(4,'Resultado visita');
	campoSelectSiNoFiltro(7,'Resultado preventa');
	campoSelect(11,'Procedencia del listado',array('','Colaborador/Asesor 1º Nivel','Colaborador/Asesor 2º Nivel','Puerta Fría','Base de datos comprada','Cartera comercial','Histórico Cartera Academia','Otros'),array('','COLABORADOR1','COLABORADOR2','PUERTAFRIA','BASEDATOS','CARTERA','HISTORICO','OTROS'));

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


//Fin parte de informe actividad telemarketing/colaborador
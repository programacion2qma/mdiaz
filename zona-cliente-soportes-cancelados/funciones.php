<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de tutores

function operacionesSoportesCancelados(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('soportes_cancelados');
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=insertaDatos('soportes_cancelados');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('soportes_cancelados');
	}

	mensajeResultado('codigoCliente',$res,'Soporte Cancelado');
    mensajeResultado('elimina',$res,'Soporte Cancelado', true);
}


function listadoSoportesCancelados(){
	global $_CONFIG;

	$columnas=array('soporte','fechaRecepcion','fechaContestacion');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas,false,false,false);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, codigoCliente, soporte, fechaRecepcion, fechaContestacion FROM soportes_cancelados $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, codigoCliente, soporte, fechaRecepcion, fechaContestacion FROM soportes_cancelados $having;");
	cierraBD();
//echo "SELECT codigo, codigoCliente, tipo, frecuencia, usuarioAutorizado, fechaPrimeraOperacion, fechaDevolucion FROM entradas_periodicas $having $orden $limite;";
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$datosCliente=datosRegistro('clientes',$datos['codigoCliente']);
		$fila=array(
			$datos['soporte'],
			formateaFechaWeb($datos['fechaRecepcion']),
			formateaFechaWeb($datos['fechaContestacion']),
        	botonAcciones(array('Detalles'),array('zona-cliente-soportes-cancelados/gestion.php?codigo='.$datos['codigo']),array("icon-search-plus")),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionSoportesCancelados(){
	operacionesSoportesCancelados();

	abreVentanaGestion('Gestión de Soportes Cancelados','?','span3');
	$datos=compruebaDatos('soportes_cancelados');

	if(!$datos){
    	campoOculto(obtenerCodigoCliente(true),'codigoCliente');
    }

	campoTexto('interesadoSolicitaCancelacion','Interesado Solicita Cancelación',$datos);
	campoFecha('fechaRecepcion','Fecha Recepción Solicitud',$datos);
	campoTexto('medioRecepcion','Medio Recepción',$datos);
	campoFecha('fechaContestacion','Fecha Contestación',$datos);
	campoTexto('medioContestacion','Medio Contestación',$datos);	

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta('codigoFichero','Fichero',"SELECT codigo, nombreFichero AS texto FROM declaraciones WHERE codigoCliente=".obtenerCodigoCliente(true)." ORDER BY nombreFichero;",$datos);
	campoTexto('soporte','Soporte',$datos);	
	campoFecha('fechaBloqueo','Fecha Bloqueo',$datos);
	campoFecha('fechaBorrado','Fecha Borrado/Destrucción',$datos);		
	campoTexto('formaRealizarBorrado','Forma Realizar Borrado/Destrucción',$datos);		

	cierraVentanaGestion('index.php',true);
}

function filtroSoportesCancelados(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoSelectConsulta(0,'Cliente',"SELECT codigo, razonSocial AS texto FROM clientes ORDER BY razonSocial;");
	campoTexto(1,'Soporte');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(2,'Fecha Recepción');
	campoFecha(3,'Fecha Contestación');	

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function obtieneNombreTutor(){
	$codigo=$_GET['codigo'];

	$datos=consultaBD("SELECT CONCAT(nombre,' ',apellido1,' ',apellido2) AS nombre FROM tutores WHERE codigo=$codigo",true,true);

	return "<a href='gestion.php?codigo=$codigo'>".$datos['nombre']."</a>";
}

function obtieneNombreListado(){
	$tutor=obtieneNombreTutor();

	$res="que $tutor está tutorizando actualmente";	

	return $res;
}

function listadoCursosTutor($codigoTutor){
	global $_CONFIG;

	$columnas=array('grupos.fechaAlta','accion','modalidad','accionFormativa','grupos.grupo','grupos.numParticipantes','fechaInicio','fechaFin','anulado','razonSocial','codigoTutor','grupos.fechaAlta');
	
	$where=obtieneWhereListado("WHERE tutores_grupo.codigoTutor=$codigoTutor",$columnas);//Utilizo WHERE por la agrupación de clientes y tutores (el HAVING realiza el filtrado TRAS la selección, y el WHERE ANTES)
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, grupos.fechaAlta, accion, modalidad, accionFormativa, grupos.grupo, grupos.numParticipantes, fechaInicio, fechaFin, anulado, razonSocial, codigoTutor, grupos.activo, grupos.codigo AS codigoGrupo
			FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo 
			LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo 
			LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo 
			LEFT JOIN tutores_grupo ON grupos.codigo=tutores_grupo.codigoGrupo 
			$where 
			GROUP BY grupos.codigo";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			formateaFechaWeb($datos['fechaAlta']),
			$datos['accion'],
			$datos['modalidad'],
			$datos['accionFormativa'],
			$datos['grupo'],
			$datos['numParticipantes'],
			formateaFechaWeb($datos['fechaInicio']),
			formateaFechaWeb($datos['fechaFin']),
			"<div class='centro'>".$datos['anulado']."</div>",
			creaBotonDetalles("inicios-grupos/gestion.php?codigo=".$datos['codigoGrupo'],'Ver curso'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function creaBotonesGestionListadoTutor(){
    echo '<a class="btn-floating btn-large btn-default btn-eliminacion noAjax" href="index.php" title="Volver"><i class="icon-chevron-left"></i></a>';
}



function listadoAFTutor($codigoTutor){
	global $_CONFIG;

	$columnas=array('accion','accionFormativa','codigo');
	$having=obtieneWhereListado("WHERE codigoTutor=$codigoTutor",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, accion, accionFormativa 
			FROM acciones_formativas INNER JOIN tutores_accion_formativa ON acciones_formativas.codigo=tutores_accion_formativa.codigoAccionFormativa 
			$having";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['accion'],
			$datos['accionFormativa'],
        	creaBotonDetalles("acciones-formativas/gestion.php?codigo=".$datos['codigo'],'Ver Acción Formativa'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function filtroCursosTutor(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoFecha(0,'Fecha desde');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(11,'Hasta');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function generaDocumento($codigoCliente){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
	
	conexionBD();

	anexoVIILOPD($codigoCliente, $PHPWord, 'anexoVII.docx');

	cierraBD();	

}

function datosPersonales($datos,$formulario){
	if($datos['familia']=='LOPD1'){
		$datos['razonSocial']=$formulario['pregunta3'];
		$datos['domicilio']=$formulario['pregunta4'];
		$datos['cp']=$formulario['pregunta10'];
		$datos['localidad']=$formulario['pregunta5'];
		$datos['provincia']=convertirMinuscula($formulario['pregunta11']);
		$datos['cif']=$formulario['pregunta9'];
	}

	return $datos;
}

function sanearCaracteres($texto){
	$texto = str_replace( '&', '&#38;', $texto);
	$texto = str_replace( '–', '&#45;', $texto);
	$texto = str_replace( '<br />', '<w:br/>', $texto);
	$texto = str_replace( '"', '', $texto);
	return $texto;
}

function reemplazarLogo($documento,$datos,$imagen='image1.png',$campo='ficheroLogo'){
	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos[$campo]);
    if($hayLogo!='NO' && $hayLogo!=''){
    	$logo = '../documentos/logos-clientes/'.$datos[$campo];
    	$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
		if (!copy($logo, $nuevo_logo)) {
    		echo "Error al copiar $fichero...\n";
		}	
		$documento->replaceImage('../documentos/logos-clientes/',$imagen);	
	}
}

function fechaTabla($fecha,$texto){
	return '<w:p w14:paraId="1060A2A2" w14:textId="7A44C6D1" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="00397BF1" w:rsidP="0016753F"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">REGISTRO DE '.$texto.' A </w:t></w:r><w:r w:rsidR="00D303CF"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$fecha.'</w:t></w:r></w:p>';
}

function pieTabla($total){
	return '<w:p w14:paraId="2CF98860" w14:textId="77777777" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="001F0F57" w:rsidP="001F0F57"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="right"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>Nº total de re</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">gistros </w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$total.'</w:t></w:r></w:p>';
}

function anexoVIILOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_VII_REGISTRO_SOPORTES_BLOQUEADOS.docx';
	$datos=consultaBD("SELECT clientes.codigo, clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.ficheroLogo, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE clientes.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",utf8_decode(date("d/m/Y")));

    reemplazarLogo($documento,$datos,'image1.png');	

	$tablaEntradasP=fechaTabla(date('d/m/Y'),'SOPORTES BLOQUEADOS');

	$tablaEntradasP.='<w:tbl><w:tblPr><w:tblW w:w="8897" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1340"/><w:gridCol w:w="3021"/><w:gridCol w:w="1984"/><w:gridCol w:w="2552"/></w:tblGrid><w:tr w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w14:paraId="284F0595" w14:textId="77777777" w:rsidTr="00164EEF"><w:tc><w:tcPr><w:tcW w:w="1340" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="4B0C3F59" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF</w:t></w:r><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>ERENCIA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3021" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2FE71F50" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1984" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="50A9072B" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA RECEPCIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2552" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="7F84E4EB" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="008F5EA5" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA CONTESTACIÓN</w:t></w:r></w:p></w:tc></w:tr>';

	$i=1;	
	$entradasP=consultaBD("SELECT * FROM soportes_cancelados WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entrada=mysql_fetch_assoc($entradasP)){
		$ref=$i<10?'0'.$i:$i;
		$tablaEntradasP.='<w:tr w:rsidR="00164EEF" w14:paraId="2F2C62BB" w14:textId="77777777" w:rsidTr="00164EEF"><w:tc><w:tcPr><w:tcW w:w="1340" w:type="dxa"/></w:tcPr><w:p w14:paraId="7A95E27E" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3021" w:type="dxa"/></w:tcPr><w:p w14:paraId="631EF2E8" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['soporte'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1984" w:type="dxa"/></w:tcPr><w:p w14:paraId="3559A74B" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($entrada['fechaRecepcion']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2552" w:type="dxa"/></w:tcPr><w:p w14:paraId="4B73A922" w14:textId="77777777" w:rsidR="00164EEF" w:rsidRPr="002B0C55" w:rsidRDefault="00164EEF" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($entrada['fechaContestacion']).'</w:t></w:r></w:p></w:tc></w:tr>';

		$i++;		
	}	
	$total=$i-1;
	$tablaEntradasP.='</w:tbl>'.pieTabla($total);

	$tablaTexto='';
	$j=1;
	$entradasAux=consultaBD("SELECT * FROM soportes_cancelados WHERE codigoCliente='".$datos['codigo']."';",true);
	while($entradasP=mysql_fetch_assoc($entradasAux)){
		$ref=str_pad($j,4,'0',STR_PAD_LEFT);
		$datosFichero=datosRegistro('declaraciones',$entradasP['codigoFichero']);
		$tablaTexto.='<w:p w:rsidR="007E394D" w:rsidRPr="00D21EE3" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00D21EE3"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">REFERENCIA  </w:t></w:r><w:r w:rsidRPr="00D21EE3"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/><w:u w:val="single"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>INTERESADO</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> QUE SOLICITA LA CANCELACIÓN</w:t></w:r><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.$entradasP['interesadoSolicitaCancelacion'].'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>FECHA RECEPCIÓN SOLICITUD</w:t></w:r><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaRecepcion']).'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="00D2466F" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>MEDIO RECEPCIÓN</w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>: '.$entradasP['medioRecepcion'].'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve">FECHA CONTESTACIÓN: </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaContestacion']).'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve">MEDIO CONTESTACIÓN: </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.$entradasP['medioContestacion'].'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> FICHERO: </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.$datosFichero['nombreFichero'].'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="00D2466F" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve">SOPORTE: </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.$entradasP['soporte'].'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>FECHA BLOQUEO:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaBloqueo']).'</w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p><w:p w:rsidR="007E394D" w:rsidRPr="00D2466F" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>FECHA BORRADO/DESTRUCCIÓN:</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaBorrado']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w:rsidR="007E394D" w:rsidRPr="009131BE" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00084966" w:rsidRDefault="007E394D" w:rsidP="007E394D"><w:r w:rsidRPr="009131BE"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve">FORMA DE </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t>REALIZAR EL BORRADO/DESTRUCCIÓN</w:t></w:r><w:r w:rsidRPr="00D2466F"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Times New Roman"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="18"/></w:rPr><w:t xml:space="preserve"> '.$entradasP['formaRealizarBorrado'].'</w:t></w:r></w:p><w:sectPr w:rsidR="00084966"><w:pgSz w:w="11906" w:h="16838"/><w:pgMar w:top="1417" w:right="1701" w:bottom="1417" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/><w:cols w:space="708"/><w:docGrid w:linePitch="360"/></w:sectPr>';

		$j++;
	}


	$documento->setValue("tablaEntradas",utf8_decode($tablaEntradasP));
	$documento->setValue("tablaTexto",utf8_decode($tablaTexto));	

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

//Fin parte de tutores
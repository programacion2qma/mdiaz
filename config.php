<?php

if(substr_count($_SERVER['REQUEST_URI'],'mdiaz2')){
	session_name('mdiaz-paralelo');
	$raiz='/mdiaz/';
}
else{
	session_name('mdiaz-normal');
	$raiz='/mdiaz/';
}

session_start();

//echo session_id().'<br />';


// =========================================================== Parámetros de configuración ====================================================================================

//Inicialización del array de configuración
$_CONFIG=array();

//Parámetros de conexión a la BDD
$_CONFIG['servidorBD']='localhost';

// $_CONFIG['usuarioBD']='root';
$_CONFIG['usuarioBD']='mdiaz-pruebas';

// $_CONFIG['claveBD']='root';
$_CONFIG['claveBD']='Writemaster7';

// $_CONFIG['nombreBD']='mdiaz';
$_CONFIG['nombreBD']='mdiaz-pruebas';

//Nombre de cookie de sesión
$_CONFIG['nombreCookie']='sesionMDiaz';

//Perfiles de usuario
$_CONFIG['perfiles']=array(
	'ADMIN'=>'Administrador',
	'ADMINISTRACION1'=>'Administración 1',
	'ADMINISTRACION2'=>'Administración 2',
	'CONTABILIDAD'=>'Contabilidad',
	'TUTOR'=>'Tutor',
	'DIRECTOR'=>'Director de Zona',
	'DELEGADO'=>'Delegado',
	'COMERCIAL'=>'Comercial',
	'TELEMARKETING'=>'Telemarketing',
	'CONSULTOR'=>'Consultor',
	'CLIENTE'=>'Cliente'
);

$_CONFIG['historicoSQL']=true;

//Parámetros de maquetación
$_CONFIG['textoAcceso']='Software de Gestión';

$_CONFIG['title']='UVED';

$_CONFIG['titulosSecciones']=array(
									'Inicio',
									'Asesorías',
									'Agrupaciones',
									'Trabajadores',
									'Inicios de Grupo',
									'Complementos',
									'Servicios',
									'Acciones Formativas',
									'Centros de Formación',
									'Tutores',
									'Gestión Consultoría',
									'Seguimiento Consultoría',
									'Configuración Consultoría',
									'Tareas Tutorías',
									'Tutorías',
									'Clientes potenciales',
									'Clientes',
									'Categorías',
									'Agentes',
									'Telemarketing',
									'Colaboradores',
									'Preventas',
									'Listado ventas detallado',
									'Pago comisiones',
									'Agenda',
									'Incidencias',
									'Exportar XML Acciones',
									'Exportar XML Inicio',
									'Emisores facturación',
									'Cuentas bancarias propias',
									'Facturas',
									'Vencimientos',
									'Recibos',
									'Remesas',
									'Importar clientes potenciales',
									'Operadores',
									'Ventas',
									'Listado de comisiones',
									'Exportar XML Fin',
									'Exportar horas tutorización',
									'Proveedores',
									'Pedidos',
									'Productos',
									'Envíos',
									'Informes',
									'Abonos',
									'Documentación',
									'Importar clientes',
									'Importar trabajadores',
									'Zona cliente',
									'Informes de clientes',
									'Convenios',
									'Acción comercial',
									'Análisis previos de riesgo',
									'Formación',
									'Documentación',
									'Comunicación interna',
									'Decisiones indiciduales automatizadas',
									'Auditorías Internas',
									'Derecho a oposición',
									'Derecho portabidad de datos', // 60
								);

$_CONFIG['altLogo']='UVED';

$_CONFIG['tituloSoftware']='- Software de gestión';

$_CONFIG['textoPie']='Software desarrollado por <a href="http://www.qmaconsultores.com" target="_blank">QMA Consultores</a>';

//Parámetros de estilos
$_CONFIG['colorPrincipal']='#56A4C0';

$_CONFIG['colorSecundario']='#4192CC';

$_CONFIG['colorPrincipalRGB']='rgba(86,164,192,0.2)';//126,178,22. Se puede usar la utilidad online http://www.javascripter.net/faq/hextorgb.htm para conversión desde hexadecimal

//Para enlaces/rutas

$_CONFIG['raiz']=$raiz;

$_CONFIG['enlaceCuestionario']='https://crmparapymes.com.es'.$_CONFIG['raiz'].'cuestionario-de-satisfaccion/';

//Para módulo de incidencias de software

$_CONFIG['codigoIncidenciasSoftware']='351';

$_CONFIG['nombreIncidenciasSoftware']='Muñoz y Díaz Asesores de Calidad, S.L.U.';


//Específico de La Academia Empresas:
$_CONFIG['codigoEmisorFormacion']=1;//Código del emisor por defecto para conceptos de formación (LAACADEMIA ESTUDIOS UNIVERSITARIOS, S.L.)

/*$_CONFIG['servidorTotara']='212.92.58.148';

$_CONFIG['usuarioTotara']='usr.laacademia';

$_CONFIG['claveTotara']='TpnfuTkfjem';

$_ONFIG['nombreTotara']='laacademiaempresas';*/
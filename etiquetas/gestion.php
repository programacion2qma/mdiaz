<?php
  $seccionActiva=46;
  include_once("../cabecera.php");
  gestionEtiqueta();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-colorpicker.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('#color').colorpicker();
});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
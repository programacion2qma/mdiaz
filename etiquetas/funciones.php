<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de etiquetas

function operacionesEtiquetas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('etiquetas');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('etiquetas');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('etiquetas');
	}

	mensajeResultado('nombre',$res,'Etiqueta');
	mensajeResultado('elimina',$res,'Etiqueta', true);
}



function gestionEtiqueta(){
	operacionesEtiquetas();

	abreVentanaGestion('Gestión de Etiquetas','?');
	$datos=compruebaDatos('etiquetas');

	campoTexto('nombre','Nombre',$datos);
	campoTexto('color','Color',$datos,'input-mini');

	cierraVentanaGestion('index.php',true);
}


function listadoEtiquetas(){
	$columnas=array('codigo','nombre','color');
	$having=obtieneWhereListado('WHERE 1=1',$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT * FROM etiquetas";
	
	conexionBD();
	$consulta=consultaBD($query." $having $orden $limite;");
	$consultaPaginacion=consultaBD($query." $having");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['codigo'],
			$datos['nombre'],
			"<span class='label colorEtiqueta' style='background-color:".$datos['color']."'>".$datos['color']."</span>",
			creaBotonDetalles('etiquetas/gestion.php?codigo='.$datos['codigo'],'Editar'),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function creaBotonesGestionEtiquetas(){
	if(compruebaUsuarioAdministrador()){
	    echo '
	    <a class="btn-floating btn-large btn-default btn-creacion2" href="../gestor-documentos/" title="Volver a carpetas"><i class="icon-chevron-left"></i></a>
	    <a class="btn-floating btn-large btn-success btn-creacion" href="gestion.php" title="Nueva etiqueta"><i class="icon-plus"></i></a>
	    <a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
	}
}

//Fin parte de etiquetas
<?php
  $seccionActiva=46;
  include_once('../cabecera.php');
  
  operacionesEtiquetas();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <?php
        creaBotonesGestionEtiquetas();
      ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Etiquetas para documentos registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaEtiquetas">
                <thead>
                  <tr>
                    <th> ID </th>
                    <th> Nombre </th>
                    <th> Color </th>
                    <th class="centro"></th>
                    <?php celdaCabeceraAdministrador("<div class='centro'><input type='checkbox' id='todo'></div>"); ?>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaEtiquetas','../listadoAjax.php?include=etiquetas&funcion=listadoEtiquetas();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaEtiquetas');

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
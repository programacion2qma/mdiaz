<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de ventas de servicios

function operacionesAuditorias(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaAuditoria();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=insertaAuditoria();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('auditorias_internas');
	}
	elseif(isset($_GET['codigoFinalizar'])){
		$res=finalizar($_GET['codigoFinalizar']);
	}

	mensajeResultado('codigoCliente',$res,'Auditoría interna');
    mensajeResultado('elimina',$res,'Auditoría interna', true);
}

function finalizar($codigo){
	$res=true;
	conexionBD();
	$fecha=consultaBD('SELECT fecha FROM auditorias_internas_fechas WHERE codigoAuditoria='.$codigo.' ORDER BY fecha DESC LIMIT 1',false,true);
	$res=consultaBD('UPDATE auditorias_internas SET finalizada="SI",fechaEmision="'.$fecha['fecha'].'" WHERE codigo='.$codigo);
	/*$declaraciones=consultaBD('SELECT codigo, nombreFichero, nivel, tratamiento, fechaBaja FROM declaraciones WHERE codigoCliente = (SELECT codigoCliente FROM auditorias_internas WHERE codigo='.$codigo.');',false);
	while($item=mysql_fetch_assoc($declaraciones)){
		$res=$res && consultaBD('INSERT INTO auditorias_internas_tratamientos VALUES(NULL,'.$codigo.','.$item['codigo'].',"'.$item['nombreFichero'].'","'.$item['nivel'].'","'.$item['tratamiento'].'","'.$item['fechaBaja'].'");');
	}*/
	cierraBD();
	return $res;
}

function insertaAuditoria(){
	$res=true;
	preparaFormulario();
	$res=insertaDatos('auditorias_internas');
	$codigo=$res;
	$res=$res && insertaFechas($codigo);
	$res=$res && insertaAuditores($codigo);
	$res=$res && insertaTratamientos($codigo);
	return $res;
}

function actualizaAuditoria(){
	$res=true;
	preparaFormulario();
	$res=actualizaDatos('auditorias_internas');
	$codigo=$_POST['codigo'];
	$res=$res && insertaFechas($codigo);
	$res=$res && insertaAuditores($codigo);
	$res=$res && insertaTratamientos($codigo,true);
	return $res;
}

function insertaFechas($codigo){
	$res = true;
	conexionBD();
	
	$res = $res && consultaBD('DELETE FROM auditorias_internas_fechas WHERE codigoAuditoria='.$codigo);
	
	$i = 0;
	
	while (isset($_POST['fecha'.$i])) {
		if($_POST['fecha'.$i] != '') {
			$res = $res && consultaBD('INSERT INTO auditorias_internas_fechas VALUES(codigo,'.$codigo.',"'.formateaFechaBD($_POST['fecha'.$i]).'");');
		}
		$i++;
	}
	
	cierraBD();
	
	return $res;

}

function insertaAuditores($codigo){
	$res = true;
	
	conexionBD();
	
	$res = $res && consultaBD('DELETE FROM auditorias_internas_auditores WHERE codigoAuditoria='.$codigo);
	$i = 0;
	
	while (isset($_POST['nombre'.$i])) {
		if($_POST['nombre'.$i]!=''){
			$res = consultaBD('INSERT INTO auditorias_internas_auditores VALUES(codigo,'.$codigo.',"'.$_POST['nombre'.$i].'","'.$_POST['nif'.$i].'","'.$_POST['cargo'.$i].'","'.$_POST['responsable'.$i].'","'.$_POST['delegado'.$i].'");');
		}
		
		if ($_POST['delegado'.$i] == 'SI') {
			
			$trabajos = consultaBD('SELECT trabajos.codigo,formulario FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE servicios.servicio LIKE "%LOPD%" AND codigoCliente='.$_POST['codigoCliente']);
			
			while ($trabajo = mysql_fetch_assoc($trabajos)) {
				
				$formulario = recogerFormularioServicios($trabajo);

				$formulario['pregunta604'] = $_POST['nombre'.$i];
				$formulario['pregunta606'] = $_POST['nif'.$i];
				$formulario['pregunta608'] = $_POST['cargo'.$i];
				
				$texto = '';
				
				foreach ($formulario as $key => $value) {
					if ($key != 'pregunta1') {
						$texto .= "&{}&";
					}
					
					$texto .= $key."=>".$value;
				}
				
				$res = $res && consultaBD('UPDATE trabajos SET formulario="'.$texto.'" WHERE codigo='.$trabajo['codigo']);
			}
		}

		$i++;
	}

	cierraBD();
	
	return $res;

}

function insertaTratamientos($codigo, $actualizar = false){
	$res = true;
	
	conexionBD();
	$i = 0;
	
	while (isset($_POST['nombreFichero'.$i])) {
		if ($actualizar) {
			$res = $res && consultaBD('UPDATE auditorias_internas_tratamientos SET nombreFichero="'.$_POST['nombreFichero'.$i].'",nivel="'.$_POST['nivel'.$i].'",tratamiento="'.$_POST['tratamiento'.$i].'",fechaBaja="'.formateaFechaBD($_POST['fechaBaja'.$i]).'" WHERE codigo='.$_POST['codigoTratamiento'.$i].';');
		} 
		else {
			$res = $res && consultaBD('INSERT INTO auditorias_internas_tratamientos VALUES(NULL,'.$codigo.','.$_POST['codigoTratamiento'.$i].',"'.$_POST['nombreFichero'.$i].'","'.$_POST['nivel'.$i].'","'.$_POST['tratamiento'.$i].'","'.formateaFechaBD($_POST['fechaBaja'.$i]).'");');
		}
		
		$i++;
	}
	
	cierraBD();
	
	return $res;

}


function preparaFormulario(){
	$_POST['formulario']='';
	$i=1;
	while(isset($_POST['pregunta'.$i])){
		if($i>1){
			$_POST['formulario'].='&{}&';
		}
		$_POST['formulario'].='pregunta'.$i.'=>'.$_POST['pregunta'.$i].'&{}&';
		$_POST['formulario'].='deficiencia'.$i.'=>'.$_POST['deficiencia'.$i].'&{}&';
		$_POST['formulario'].='medida'.$i.'=>'.$_POST['medida'.$i];
		if(isset($_POST['otro'.$i])){
			$_POST['formulario'].='&{}&otro'.$i.'=>'.$_POST['otro'.$i];
			if(isset($_POST['otro'.$i.'_2'])){
				$_POST['formulario'].='&{}&otro'.$i.'_2=>'.$_POST['otro'.$i.'_2'];
			}
		}
		$i++;
	}
}

function listadoAuditorias($finalizada){
	global $_CONFIG;

	//Uso 2 columnas y 2 where porque las 2 consultas no tienen las columnas con el mismo nombre
	$columnas=array('auditorias_internas_fechas.fecha','razonSocial','auditorias_internas_fechas.fecha','auditorias_internas_fechas.fecha');

	//Parte de WHERE
	$where=obtieneWhereListado("WHERE finalizada='".$finalizada."'",$columnas);
	//Fin parte de WHERE

	if($_SESSION['tipoUsuario']=='CLIENTE'){
		$where.=' AND codigoCliente='.obtenerCodigoCliente(true);
	}

	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
		

	$query="SELECT auditorias_internas.codigo, clientes.razonSocial, auditorias_internas.finalizada, auditorias_internas_fechas.fecha
	FROM auditorias_internas
	LEFT JOIN clientes ON auditorias_internas.codigoCliente=clientes.codigo 
	LEFT JOIN auditorias_internas_fechas ON auditorias_internas.codigo=auditorias_internas_fechas.codigoAuditoria
	$where
	GROUP BY auditorias_internas.codigo";

	conexionBD();

	$consulta=consultaBD($query." $orden $limite");
	$consultaPaginacion=consultaBD($query);

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$botones=obtieneBotones($datos);
		$fechas=obtieneFechas($datos['codigo']);
		$fila=array(
			$fechas,
			$datos['razonSocial'],
			$botones,
        	"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' /></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	cierraBD();

	echo json_encode($res);
}

function obtieneBotones($datos){

	$nombres=array('Detalles');
	$direcciones=array("auditorias-internas/gestion.php?codigo=".$datos['codigo']);
	$iconos=array('icon-search-plus');
	$enlaces=array(0);
	if($datos['finalizada']=='NO'){
		array_push($nombres, 'Finalizar', 'Descargar Cuestionario');
		array_push($direcciones,'auditorias-internas/index.php?finalizada=SI&codigoFinalizar='.$datos['codigo'], 'auditorias-internas/generaWord.php?codigo='.$datos['codigo'].'&cuestionario=SI');
		array_push($iconos, 'icon-check-circle', 'icon-download');
		array_push($enlaces, 0, 0);
	} else {
		array_push($nombres, 'Descargar Auditoría');
		array_push($direcciones, 'auditorias-internas/generaWord.php?codigo='.$datos['codigo']);
		array_push($iconos, 'icon-download');
		array_push($enlaces, 0);
	}

	$res=botonAcciones($nombres,$direcciones,$iconos, $enlaces);

	return $res;
}

function obtieneFechas($codigo){
	$res='';
	$fechas=consultaBD('SELECT * FROM auditorias_internas_fechas WHERE codigoAuditoria='.$codigo);
	while($fecha=mysql_fetch_assoc($fechas)){
		$res.='- '.formateaFechaWeb($fecha['fecha']).'<br/>';
	}
	return $res;

}

function gestionAuditorias(){
	operacionesAuditorias();

	abreVentanaGestion('Gestión de Auditorias internas','?','');
	$datos=compruebaDatos('auditorias_internas');
	$formulario=prepararPreguntas($datos);

	creaPestaniasAPI(array('Datos de la auditoría','I. Aspectos generales','II. Encargado de tratamiento','III. Prestación de servicio sin acceso a datos personales','IV. Delegación de autorizaciones','V. Régimen de trabajo fuera de los locales de la ubicación del fichero','VI. Ficheros temporales o copias de trabajo de documentos','VII. Documento de seguridad/Informe de riesgo','VIII. Funciones y obligaciones del personal','IX. Registro de incidencias','X. Control de acceso','XI. Gestión de soportes y documentos','XII. Identificación y autenticación','XIII. Copias de respaldo y recuperación','XIV. Registro de accesos','XV. Acceso a datos a traves de redes de comunicaciones','XVI. Auditoría','XVII. Criterios de archivo','XVIII. Almacenamiento de la información','XIX. Custodia de soportes'));
	if(isset($_POST['pestanaActiva'])){
		$pestana=$_POST['pestanaActiva'];
	} else {
		$pestana=1;
	}
	campoOculto($pestana,'pestanaActiva');

	abrePestaniaAPI(1,$pestana==1?true:false);
		if($_SESSION['tipoUsuario']=='CLIENTE'){
			campoOculto($datos,'codigoCliente',obtenerCodigoCliente(true));
		} else {
			campoSelectClienteFiltradoPorUsuario($datos,'Cliente','codigoCliente','',true);
		}
		campoTexto('lugar','Lugar de auditoría',$datos,'span6');
		creaTablaAuditores($datos);
		creaTablaFechas($datos);
		if($datos && $datos['finalizada']=='SI'){
			campoFecha('fechaEmision','Fecha emisión',$datos);
		} else {
			campoOculto(formateaFechaWeb($datos['fechaEmision']),'fechaEmision');
		}
		creaTablaTratamientos($datos);
		campoOculto($datos,'finalizada','NO');
		campoOculto($datos,'emitida','NO');
		echo '<br/><br/>Para la realización de la presente auditoría, el equipo auditor  ha utilizado los siguientes recursos:';
		echo '<ul id="ulRecursos">';
			echo '<li>Inspección ocular realizada en las propias oficinas o dependencias del Responsable de Tratamientos.</li>';
			echo '<li>Análisis de diversos documentos, tales como: Documento de Seguridad y sus Anexos, relación de tratamientos, estructura y contenido, Informe de Riesgo, Evaluación de Impacto, políticas de seguridad y procedimientos registrados (registro de incidencias, copias de respaldo y recuperación, identificación y autorización, cifrado, borrado de soportes), inventario de soportes y registro de entrada y salida de soportes, relación de usuarios, accesos autorizados y sus funciones; análisis del programa de gestión LOPD.</li>';
			echo '<li>';
			areaTexto('recursos','Otros',$datos);
			echo '</li>'; 
		echo '</ul>';
	cierraPestaniaAPI();
	abrePestaniaAPI(2,$pestana==2?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(1,$formulario);
			creaFilaTablaAuditoriaInterna(2,$formulario);
			creaFilaTablaAuditoriaInterna(3,$formulario);
			creaFilaTablaAuditoriaInterna(4,$formulario);
			creaFilaTablaAuditoriaInterna(5,$formulario);
			creaFilaTablaAuditoriaInterna(6,$formulario);
			creaFilaTablaAuditoriaInterna(7,$formulario);
			creaFilaTablaAuditoriaInterna(8,$formulario);
			creaFilaTablaAuditoriaInterna(9,$formulario);
			creaFilaTablaAuditoriaInterna(10,$formulario);
			creaFilaTablaAuditoriaInterna(11,$formulario);
			creaFilaTablaAuditoriaInterna(12,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(3,$pestana==3?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(13,$formulario);
			creaFilaTablaAuditoriaInterna(14,$formulario);
			creaFilaTablaAuditoriaInterna(15,$formulario);
			creaFilaTablaAuditoriaInterna(16,$formulario);
			creaFilaTablaAuditoriaInterna(17,$formulario);
			creaFilaTablaAuditoriaInterna(18,$formulario);
			creaFilaTablaAuditoriaInterna(19,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(4,$pestana==4?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(20,$formulario);
			creaFilaTablaAuditoriaInterna(21,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(5,$pestana==5?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(22,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(6,$pestana==6?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(23,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(7,$pestana==7?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(24,$formulario);
			creaFilaTablaAuditoriaInterna(25,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();
	abrePestaniaAPI(8,$pestana==8?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(26,$formulario);
			creaFilaTablaAuditoriaInterna(27,$formulario);
			creaFilaTablaAuditoriaInterna(28,$formulario);
			creaFilaTablaAuditoriaInterna(29,$formulario);
			creaFilaTablaAuditoriaInterna(30,$formulario);
			creaFilaTablaAuditoriaInterna(31,$formulario);
			creaFilaTablaAuditoriaInterna(32,$formulario);
			creaFilaTablaAuditoriaInterna(33,$formulario);
			creaFilaTablaAuditoriaInterna(34,$formulario);
			creaFilaTablaAuditoriaInterna(35,$formulario);
			creaFilaTablaAuditoriaInterna(36,$formulario);
			creaFilaTablaAuditoriaInterna(37,$formulario);
			creaFilaTablaAuditoriaInterna(38,$formulario);
			creaFilaTablaAuditoriaInterna(39,$formulario);
			creaFilaTablaAuditoriaInterna(40,$formulario);
			creaFilaTablaAuditoriaInterna(41,$formulario);
			creaFilaTablaAuditoriaInterna(42,$formulario);
			creaFilaTablaAuditoriaInterna(43,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(9,$pestana==9?true:false);
		creaTablaAuditoriaInterna('RESPECTO A TODOS LOS TRATAMIENTOS/FICHEROS');
			creaFilaTablaAuditoriaInterna(44,$formulario);
			creaFilaTablaAuditoriaInterna(45,$formulario);
			creaFilaTablaAuditoriaInterna(46,$formulario);
			creaFilaTablaAuditoriaInterna(47,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(10,$pestana==10?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(48,$formulario);
			creaFilaTablaAuditoriaInterna(49,$formulario);
			creaFilaTablaAuditoriaInterna(50,$formulario);
			creaFilaTablaAuditoriaInterna(51,$formulario);
			creaTablaAuditoriaInterna('TRATAMIENTOS AUTOMATIZADOS',false);
			creaFilaTablaAuditoriaInterna(52,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(11,$pestana==11?true:false);
		creaTablaAuditoriaInterna();
			creaFilaTablaAuditoriaInterna(53,$formulario);
			creaFilaTablaAuditoriaInterna(54,$formulario);
			creaFilaTablaAuditoriaInterna(55,$formulario);
			creaFilaTablaAuditoriaInterna(56,$formulario);
			creaFilaTablaAuditoriaInterna(57,$formulario);
			creaFilaTablaAuditoriaInterna(58,$formulario);
			creaTablaAuditoriaInterna('ARCHIVOS AUTOMATIZADOS DE NIVEL ALTO',false,true);
			creaFilaTablaAuditoriaInterna(59,$formulario,true);
			creaTablaAuditoriaInterna('ARCHIVOS NO AUTOMATIZADOS DE NIVEL ALTO',false,true);
			creaFilaTablaAuditoriaInterna(60,$formulario,true);
			creaFilaTablaAuditoriaInterna(61,$formulario,true);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(12,$pestana==12?true:false);
		creaTablaAuditoriaInterna('RESPECTO A TODOS LOS FICHEROS');
			creaFilaTablaAuditoriaInterna(62,$formulario);
			creaFilaTablaAuditoriaInterna(63,$formulario);
			creaFilaTablaAuditoriaInterna(64,$formulario);
			creaFilaTablaAuditoriaInterna(65,$formulario);
			creaFilaTablaAuditoriaInterna(66,$formulario);
			creaFilaTablaAuditoriaInterna(67,$formulario);
			creaFilaTablaAuditoriaInterna(68,$formulario);
			creaFilaTablaAuditoriaInterna(69,$formulario);
			creaFilaTablaAuditoriaInterna(70,$formulario);
			creaTablaAuditoriaInterna('RESPECTO A TODOS LOS FICHEROS DE NIVEL ALTO',false,true);
			creaFilaTablaAuditoriaInterna(71,$formulario,true);
			creaFilaTablaAuditoriaInterna(72,$formulario,true);
			creaFilaTablaAuditoriaInterna(73,$formulario,true);
			creaTablaAuditoriaInterna('FICHEROS AUTOMATIZADOS DE NIVEL ALTO',false,true);
			creaFilaTablaAuditoriaInterna(74,$formulario,true);
			creaFilaTablaAuditoriaInterna(75,$formulario,true);
			creaFilaTablaAuditoriaInterna(76,$formulario,true);
			creaFilaTablaAuditoriaInterna(77,$formulario,true);
			creaTablaAuditoriaInterna('NO AUTOMATIZADOS/ALTO',false,true);
			creaFilaTablaAuditoriaInterna(78,$formulario,true);
			creaFilaTablaAuditoriaInterna(79,$formulario,true);
			creaFilaTablaAuditoriaInterna(80,$formulario,true);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(13,$pestana==13?true:false);
		creaTablaAuditoriaInterna('RESPECTO A TODOS LOS FICHEROS AUTOMATIZADOS');
			creaFilaTablaAuditoriaInterna(81,$formulario);
			creaFilaTablaAuditoriaInterna(82,$formulario);
			creaFilaTablaAuditoriaInterna(83,$formulario);
			creaFilaTablaAuditoriaInterna(84,$formulario);
			creaFilaTablaAuditoriaInterna(85,$formulario);
			creaFilaTablaAuditoriaInterna(86,$formulario);
			creaTablaAuditoriaInterna('RESPECTO TRATAMIENTOS AUTOMATIZADOS DE NIVEL ALTO',false,true);
			creaFilaTablaAuditoriaInterna(87,$formulario,true);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(14,$pestana==14?true:false);
		creaTablaAuditoriaInterna('RESPECTO A TODOS LOS FICHEROS AUTOMATIZADOS');
			creaFilaTablaAuditoriaInterna(88,$formulario);
			creaFilaTablaAuditoriaInterna(89,$formulario);
			creaFilaTablaAuditoriaInterna(90,$formulario);
			creaFilaTablaAuditoriaInterna(91,$formulario);
			creaFilaTablaAuditoriaInterna(92,$formulario);
			creaTablaAuditoriaInterna('AUTOMATIZADOS/ ALTO',false,true);
			creaFilaTablaAuditoriaInterna(93,$formulario,true);
			creaFilaTablaAuditoriaInterna(94,$formulario,true);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(15,$pestana==15?true:false);
		creaTablaAuditoriaInterna('RESPECTO LOS FICHEROS AUTOMATIZADOS DE NIVEL ALTO');
			creaFilaTablaAuditoriaInterna(95,$formulario);
			creaFilaTablaAuditoriaInterna(96,$formulario);
			creaFilaTablaAuditoriaInterna(97,$formulario);
			creaFilaTablaAuditoriaInterna(98,$formulario);
			creaFilaTablaAuditoriaInterna(99,$formulario);
			creaFilaTablaAuditoriaInterna(100,$formulario);
			creaFilaTablaAuditoriaInterna(101,$formulario);
			creaTablaAuditoriaInterna('RESPECTO LOS FICHEROS NO AUTOMATIZADOS DE NIVEL ALTO',false);
			creaFilaTablaAuditoriaInterna(102,$formulario);
			creaFilaTablaAuditoriaInterna(103,$formulario);
			//creaFilaTablaAuditoriaInterna(104,$formulario);
			campoOculto('','pregunta104');
			campoOculto('','deficiencia104');
			campoOculto('','medida104');
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(16,$pestana==16?true:false);
		creaTablaAuditoriaInterna('RESPECTO A TODOS LOS TRATAMIENTOS AUTOMATIZADOS');
			creaFilaTablaAuditoriaInterna(105,$formulario);
			creaTablaAuditoriaInterna('RESPECTOS A LOS FICHEROS AUTOMATIZADOS DE NIVEL ALTO',false,true);
			creaFilaTablaAuditoriaInterna(106,$formulario,true);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(17,$pestana==17?true:false);
		creaTablaAuditoriaInterna('RESPECTO A TODOS LOS FICHEROS DE NIVEL ALTO');
			creaFilaTablaAuditoriaInterna(107,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(18,$pestana==18?true:false);
		creaTablaAuditoriaInterna('RESPECTO A TODOS LOS FICHEROS');
			creaFilaTablaAuditoriaInterna(108,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(19,$pestana==19?true:false);
		creaTablaAuditoriaInterna('RESPECTO TODOS LOS ARCHIVOS NO AUTOMATIZADOS');
			creaFilaTablaAuditoriaInterna(109,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	abrePestaniaAPI(20,$pestana==20?true:false);
		creaTablaAuditoriaInterna('NO AUTOMATIZADOS/ BÁSICO');
			creaFilaTablaAuditoriaInterna(110,$formulario);
		cierraTablaAuditoriaInterna();
	cierraPestaniaAPI();

	cierraPestaniasAPI();

	$enlace='index.php';
	if($datos && $datos['finalizada']=='SI'){
		$enlace='index.php?finalizada=SI';
	}
	cierraVentanaGestion($enlace,true);
}

function creaTablaAuditores($datos){
	conexionBD();

	echo "<br/>	
	<div class='control-group'>                     
		<label class='control-label'>Auditores:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaAuditores'>
				  	<thead>
				    	<tr>
				            <th> Auditor </th>
				            <th> NIF </th>
				            <th> Cargo </th>
				            <th> Responsable de Tratamientos </th>
				            <th> Delegado de Protección de datos </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM auditorias_internas_auditores WHERE codigoAuditoria=".$datos['codigo']);
				  			while($datosA=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaAuditoriasAuditores($datosA,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaAuditoriasAuditores(false,$i);
				  		}

	echo "				  		
				  	</tbody>
				</table>
					<button type='button' class='btn btn-small btn-success' id='insertaFilaTablaAuditores'><i class='icon-plus'></i> Añadir auditor</button> 
					<button type='button' class='btn btn-small btn-danger' id='eliminaFilaTablaAuditores'><i class='icon-trash'></i> Eliminar auditor</button>
			</div>
		</div>
	</div>";
	cierraBD();
}

function imprimeLineaTablaAuditoriasAuditores($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	campoTextoTabla('nombre'.$i,$datos['nombre']);
	campoTextoTabla('nif'.$i,$datos['nif'],'input-small');
	campoTextoTabla('cargo'.$i,$datos['cargo']);
	campoSelect('responsable'.$i,'',array('No','Si'),array('NO','SI'),$datos['responsable'],'selectpicker span2 show-tick',"",1);
	campoSelect('delegado'.$i,'',array('No','Si'),array('NO','SI'),$datos['delegado'],'selectpicker span2 show-tick',"",1);
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

function creaTablaTratamientos($datos){
	conexionBD();

	echo "<br/>	
	<div class='control-group'>                     
		<label class='control-label'>Tratamientos/Ficheros:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaTratamientos'>
				  	<thead>
				    	<tr>
				            <th> Tratamiento/Fichero </th>
				            <th> Nivel medidas de seguridad </th>
				            <th> Sistema de tratamiento </th>
				            <th> Fecha baja </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM auditorias_internas_tratamientos WHERE codigoAuditoria=".$datos['codigo']);
				  			while($datosA=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaTratamientos($datosA,$i);
				  				$i++;
				  			}
				  		}

	echo "				  		
				  	</tbody>
				</table>
			</div>
		</div>
	</div>";
	cierraBD();
}

function imprimeLineaTablaTratamientos($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	campoOculto($datos['codigo'],'codigoTratamiento'.$i);
	campoTextoTabla('nombreFichero'.$i,$datos['nombreFichero']);
	campoSelect('nivel'.$i,'Nivel de medidas de seguridad',array('Básico','Medio','Alto'),array(1,2,3),$datos['nivel'],'span2 selectpicker show-tick selectNivel',"data-live-search='true'",1);
	campoSelect('tratamiento'.$i,'Sistema de tratamiento',array('Automatizado','Manual','Mixto'),array('1','2','3'),$datos['tratamiento'],'selectpicker span2 show-tick',"data-live-search='true'",1);
	campoFechaTabla('fechaBaja'.$i,$datos['fechaBaja']);
	echo "</tr>";
}

function creaTablaFechas($datos){
	conexionBD();

	echo "<br/>	
	<div class='control-group'>                     
		<label class='control-label'>Fechas de auditoría:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaFechas'>
				  	<thead>
				    	<tr>
				            <th> Fecha </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM auditorias_internas_fechas WHERE codigoAuditoria=".$datos['codigo'].' ORDER BY fecha');
				  			while($datosA=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaAuditorias($datosA,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaAuditorias(false,$i);
				  		}

	echo "				  		
				  	</tbody>
				</table>
					<button type='button' class='btn btn-small btn-success' id='insertaFilaTabla'><i class='icon-plus'></i> Añadir fecha</button> 
					<button type='button' class='btn btn-small btn-danger' id='eliminaFilaTabla'><i class='icon-trash'></i> Eliminar fecha</button>
			</div>
		</div>
	</div>";
	cierraBD();
}

function imprimeLineaTablaAuditorias($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	campoFechaTabla('fecha'.$i,$datos['fecha']);
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

function creaBotonFinalizadas($finalizada='NO'){
    if($finalizada=='NO'){
        echo '<a class="btn-floating btn-large btn-warning btn-creacion2" href="index.php?finalizada=SI" title="Finalizadas"><i class="icon-check"></i></a>';
    } else {
        echo '<a class="btn-floating btn-large btn-info btn-creacion2" href="index.php" title="Volver"><i class="icon-arrow-left"></i></a>';
    }
}

function creaTablaAuditoriaInterna($texto='RESPECTO A TODOS LOS TRATAMIENTOS',$tabla=true,$alto=false){
	$clase='';
	if($alto){
		$clase='classAlto';
	}
	if($tabla){
		echo '<table class="table table-striped table-bordered tablaFormularioAuditoria '.$clase.'">';
	}
	echo '<tr class="'.$clase.'"><th colspan="2"><h3 style="text-align:center;">'.$texto.'</h3></th></tr>';
}

function creaFilaTablaAuditoriaInterna($num,$datos,$alto=false){
	if($datos=='' || !isset($datos['pregunta'.$num])){
		$datos['pregunta'.$num]='';
		$datos['deficiencia'.$num]='';
		$datos['medida'.$num]='';
	}
	$clase='';
	if($alto){
		$clase='classAlto';
	}
	echo '<tr class="'.$clase.'">';
	echo '<th colspan="2">'.listadoPreguntas($num,$datos).'</th>';
	echo '</tr><tr class="'.$clase.'">';
	areaTextoTablaColspan('pregunta'.$num,$datos['pregunta'.$num],'areaTexto'); 
	echo '</tr><tr class="'.$clase.'">';
	echo '<th>DEFICIENCIA</th>';
	echo '<th>MEDIDA CORRECTORA</th>';
	echo '</tr><tr class="trCierre '.$clase.'">';
	areaTextoTabla('deficiencia'.$num,$datos['deficiencia'.$num],'areaTexto'); 
	areaTextoTabla('medida'.$num,$datos['medida'.$num],'areaTexto'); 
	echo '</tr>';
}

function cierraTablaAuditoriaInterna(){
	echo '</table>';
}

function areaTextoTablaColspan($nombreCampo,$valor='',$clase='areaTexto',$colspan=2,$bloqueado=false){ //MODIFICACIÓN 17/08/2015: Se ha introducido el atributo bloqueado, el cual permitirá deshabilitar el textarea
	$valor=compruebaValorCampo($valor,$nombreCampo);
	$disabled='';
	if($bloqueado){
		$disabled='disabled';
	}
	echo "
	<td colspan='".$colspan."'>
        <textarea name='$nombreCampo' class='$clase' id='$nombreCampo' $disabled>$valor</textarea>
    </td>";
}

function prepararPreguntas($datos){
	$res = array();
	
	if ($datos) {
		$formulario = explode('&{}&', $datos['formulario']);
		
		foreach ($formulario as $key => $value) {
			$partes = explode('=>', $value);
			$res[$partes[0]] = $partes[1];
		}
	}
	 else {
		for($i = 1; $i <= 110; $i++) {
			$res['otro'.$i] = '';
			$res['otro'.$i.'_2'] = '';
		}
	}
	return $res;
}

function campoTextoOtro($nombreCampo,$valor='',$clase='input-small',$salto=false,$disabled=false){
	$valor=compruebaValorCampo($valor,$nombreCampo);
	$des='';
	if($disabled){
		$des='readonly';
	}
	$res="<input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' value=\"$valor\" $des>";
	if($salto){
		$res.='<br/>';
	}
	return $res;
}

function listadoPreguntas($num,$datos){
	$listado=array(
		1=>'1. ¿Ha previsto cómo establecer el registro de actividades de tratamiento en su organización? ',
		2=>'2. ¿Ha valorado si le es de aplicación alguna de las excepciones a esta obligación? '.campoTextoOtro('otro2',$datos,'span7',true).' ¿Ha previsto quién se encargará de mantener actualizado el registro? ',
		3=>'3. ¿Ha hecho una valoración de los riesgos que los tratamientos que desarrolla implican para los derechos y libertades de los ciudadanos -Informe de Riesgo-?',
		4=>'4. ¿Se han iniciado nuevos Tratamientos de datos de carácter personal o se han modificado o suprimido Tratamientos que se vinieran realizando, desde la última auditoría?',
		5=>'5. ¿Tiene establecida claramente cuál es la base legal de los tratamientos que realiza y ha documentado de alguna forma el modo en que la ha establecido?',
		6=>'6. Si alguno de los tratamientos que realiza está basado en el consentimiento de los interesados, ¿ha verificado que ese consentimiento reúne los requisitos que exige el RGDP? '.campoTextoOtro('otro6',$datos,'span7',true).' En caso contrario, ¿ha previsto cómo recabar el consentimiento de forma adaptada al RGPD o ha encontrado otra base legal adecuada para esos tratamientos?  ',
		7=>'7. La información que se proporciona a los interesados, ¿está presentada de forma clara, concisa, transparente y de fácil acceso?  ',
		8=>'8. ¿Contiene esa información todos los elementos que prevé el RGPD? ',
		9=>'9. ¿Dispone de mecanismos para el ejercicio de derechos visibles, accesibles y sencillos? ',
		10=>'10. ¿Pueden ejercerse los derechos por vía electrónica? ',
		11=>'11. ¿Tiene establecidos procedimientos o mecanismos que le permitan verificar la identidad de quienes solicitan acceso o ejercen los demás derechos ARCO? ',
		12=>'12. ¿Tiene establecidos procedimientos que le permitan responder a los ejercicios de derechos en los plazos previstos por el RGPD? ',
		//-------
		13=>'1. ¿Se realiza el tratamiento por persona distinta al responsable del mismo? '.campoTextoOtro('otro13',$datos).' Y, en caso afirmativo, ¿se ha formalizado mediante contrato conforme lo establecido en el artículo 28 RGPD? ',
		14=>'2. ¿Ha previsto cómo valorar si los encargados con los que haya contratado o vaya a contratar operaciones de tratamiento ofrecen garantías de cumplimiento del RGPD cuando sea de aplicación? ',
		15=>'3. Los contratos de encargo ¿incluyen la colaboración de los encargados en la respuesta a las solicitudes de los interesados? ',
		16=>'4. ¿Se ha hecho  constar en el Documento de Seguridad si la realización de este encargo se realiza en los locales del responsable, en locales propios del encargado de tratamiento (distintos de los del responsable) o si se realiza mediante acceso remoto a los sistemas del responsable? ',
		17=>'5. En el supuesto que la realización de este encargo se realice en los locales del responsable, ¿consta por escrito en el contrato, el compromiso del personal del encargado de tratamiento respecto al cumplimiento de las medidas de seguridad recogidas en el Documento de Seguridad del responsable? ',
		18=>'6. Cuando el tratamiento se realiza mediante acceso remoto a los sistemas del responsable, ¿se le ha prohibido al encargado de tratamiento la incorporación de los datos a sistemas o soportes distintos de los del responsable?  ',
		19=>'7. Si la prestación se hace en locales propios del encargado de tratamiento (distintos de los del responsable), ¿ha elaborado el encargado el documento de seguridad y el Informe de Riesgo?  '.campoTextoOtro('otro19',$datos).' ¿Identifica el fichero o tratamiento y el responsable del mismo y detalla las medidas de seguridad a implementar en relación con su tratamiento?  ',
		//--
		20=>'1.	Si la prestación del servicio contratado no implica tratamiento de datos personales, ¿se han adoptado las medidas necesarias para limitar el acceso del personal a los datos personales, soportes y recursos? ',
		21=>'2.	Si se trata de personal ajeno, ¿recoge el contrato la prohibición expresa de acceder a los datos personales, así como la obligación de secreto respecto a los datos que hubieran podido conocer con motivo de la prestación de servicio? ',
		//--
		22=>'1.	¿Se han delegado las autorizaciones que el Reglamento atribuye al responsable en otras personas? '.campoTextoOtro('otro22',$datos).'  ¿Se ha hecho constar en el Documento de Seguridad las personas habilitadas para otorgar estas autorizaciones y las personas en quienes recae dicha delegación?',
		//--
		23=>'1. El almacenamiento de datos personales en dispositivos portátiles o los tratamientos fuera de los locales del responsable o del encargado, ¿han sido autorizados expresamente por el responsable del tratamiento? '.campoTextoOtro('otro23',$datos).'  ¿Consta dicha autorización en el Documento de Seguridad? '.campoTextoOtro('otro23_2',$datos).' ¿Se garantiza el nivel de seguridad correspondiente al tipo de tratamiento realizado? ',
		//--
		24=>'1. ¿Cumplen el nivel de seguridad correspondiente? ',
		25=>'2.	¿Se han destruido o borrado cuando ya no han sido necesarios para los fines que motivaron su creación?',
		//--
		26=>'1.	¿Ha elaborado el responsable del tratamiento el Documento de Seguridad? ',
		27=>'2.	¿Ha revisado las medidas de seguridad que aplica a sus tratamientos a la luz de los resultados del análisis de riesgo de los mismos? '.campoTextoOtro('otro27',$datos).' ¿Considera que puede seguir aplicando las medidas de seguridad previstas en el Reglamento de la LOPD? '.campoTextoOtro('otro27_2',$datos).' ¿Ha valorado suficientemente la posibilidad de introducir medidas adicionales en función del tipo de tratamiento o del contexto en que se realiza? ',
		28=>'3.	¿Ha determinado qué medidas de responsabilidad activa corresponden a su situación de riesgo y cómo debe aplicarlas? ',
		29=>'4.	¿Ha valorado si los tratamientos que realiza requieren una Evaluación de Impacto sobre la Protección de Datos porque supongan un alto riesgo para los derechos y libertades de los interesados? ',
		30=>'5.	¿Dispone de una metodología para la realización de la Evaluación de Impacto? ',
		31=>'6.	Según el tipo de tratamiento que realiza y los resultados del análisis de riesgos previo, ¿tiene que nombrar un Delegado de Protección de Datos? ',
		32=>'7.	Independientemente del resultado del análisis de riesgos previo, ¿se ha decidido nombrar un Delegado de Protección de Datos? ',
		33=>'8.	El puesto de DPD tal y como está configurado en su organización, ¿respeta los requisitos de independencia en el ejercicio de las funciones, posición en el organigrama, ausencia de conflicto de intereses y disponibilidad de los recursos necesarios establecidos por el RGPD?',
		34=>'9.	¿Ha comunicado la designación del DPD a la autoridad de protección de datos? ',
		35=>'10. ¿Ha hecho públicos los datos de contacto del DPD? ',
		36=>'11. ¿Ha establecido procedimientos para que los interesados contacten con el DPD? ',
		37=>'12. ¿Se especifica cuál es el personal autorizado para la concesión, alteración o anulación de accesos autorizados sobre datos o recursos?  ',
		38=>'13. ¿Se especifica cuál es el personal autorizado para acceder a los lugares donde se almacenan los soportes informáticos? ',
		39=>'14. Si el tratamiento se realiza por cuenta de terceros, ¿se han reflejado los tratamientos afectados por el encargo, con referencia expresa  al contrato, así como la identificación del responsable y el periodo de vigencia? ',
		40=>'15. ¿Se han reflejado en el Documento de Seguridad si los datos personales se incorporan y tratan exclusivamente en los sistemas del encargado? ',
		41=>'16. ¿Contiene los procedimientos y controles periódicos a realizar para verificar el cumplimiento de lo dispuesto en el propio documento?  ',
		42=>'17. ¿Especifica qué medidas hay que adoptar en caso de desechado o reutilización de soportes? ',
		43=>'18. ¿Relaciona las personas que están autorizadas a acceder físicamente a los locales donde se ubican los sistemas de información? ',
		//--
		44=>'1.	¿Están las funciones y obligaciones del personal con acceso a datos de carácter personal y los sistemas de información claramente definidos? ',
		45=>'2. ¿Están documentadas y reflejadas en el documento de seguridad?  ',
		46=>'3. ¿Conoce el personal las medidas de seguridad que afectan al desarrollo de sus funciones así como las consecuencias de su incumplimiento? ',
		47=>'4. ¿Se han definido las funciones de control o autorizaciones delegadas por el responsable del tratamiento?  ',
		//--
		48=>'1. ¿Existe un procedimiento de notificación y gestión de incidencias de seguridad? '.campoTextoOtro('otro48',$datos).' ¿El procedimiento está bien diseñado y es eficaz? '.campoTextoOtro('otro48_2',$datos).' ¿Conoce todo el personal afectado dicho procedimiento? ',
		49=>'2. ¿Ha establecido procedimientos para notificar las violaciones de seguridad a las autoridades de protección de datos y, si fuera necesario, a los interesados? ',
		50=>'3. ¿Dispone de un registro o herramienta similar en que pueda documentar los incidentes de seguridad que se produzcan, aunque no sean notificados a las autoridades de protección de datos? ',
		51=>'4. ¿Se revisa periódicamente el registro de incidencias para su análisis y adopción de medidas correctoras de las incidencias anotadas?  ',
		52=>'1. ¿Se han anotado las ejecuciones de los procedimientos de recuperación de datos realizados? ',
		//--
		53=>'1. ¿Los accesos autorizados de los usuarios se corresponden exclusivamente a los datos y recursos que precisan para el desarrollo de sus funciones? ',
		54=>'2. ¿Existen mecanismos que impidan que los usuarios accedan a datos o recursos distintos de los autorizados? ',
		55=>'3. ¿Existe una relación de usuarios? '.campoTextoOtro('otro55',$datos).' ¿Especifica qué datos y recursos tiene autorizados para cada uno de ellos?  '.campoTextoOtro('otro55_2',$datos).' ¿Está actualizada? ',
		56=>'4. ¿La concesión, alteración o anulación de accesos autorizados sobre datos y recursos la realiza exclusivamente el personal autorizado para ello en el Documento de Seguridad? ',
		57=>'5. ¿Ha establecido el responsable del tratamiento los criterios conforme a los cuales se otorga la autorización de los accesos a los datos y a los recursos? ',
		58=>'6. El personal ajeno al responsable que tiene acceso a los datos y recursos de éste, ¿se encuentra sometido a las mismas condiciones y  obligaciones que el personal propio?  ',
		59=>'1. ¿El acceso a los locales donde se encuentran ubicados los sistemas de información, se realiza exclusivamente por el personal autorizado en el Documento de Seguridad? ',
		60=>'1. ¿Se encuentran los archivadores u otros elementos de almacenamiento en áreas de acceso restringido dotadas de sistemas de apertura mediante llave u otro dispositivo equivalente? '.campoTextoOtro('otro60',$datos).' ¿Están cerradas estas áreas mientras no sea preciso el acceso a los documentos incluidos en el fichero?  ',
		61=>'2. Si los locales del responsable no permiten disponer de un área de acceso restringido ¿ha adoptado el responsable medidas alternativas?',
		//--
		62=>'1.	¿Está identificado el tipo de información contenido en el soporte o documento? ',
		63=>'2.	¿Existe y se mantiene un inventario de soportes? ',
		64=>'3.	¿Se almacenan los soportes o documentos en lugares de acceso restringido? ',
		65=>'4.	¿Existen mecanismos por los que solamente puedan acceder las personas autorizadas en el Documento de Seguridad? '.campoTextoOtro('otro65',$datos).' ¿Funcionan adecuadamente estos mecanismos? ',
		66=>'5.	¿La salida de soportes y documentos fuera de los locales donde se ubica el fichero está siendo autorizada por el responsable del tratamiento? ',
		67=>'6.	¿Se están tomando las medidas adecuadas en el traslado de documentación para evitar la sustracción, pérdida o acceso indebido durante su transporte? ',
		68=>'7.	Cuando se desecha un soporte o documento conteniendo datos de carácter personal, ¿se adoptan las medidas adecuadas para evitar el acceso a la información o su recuperación posterior cuando se procede a su destrucción o borrado?   ',
		69=>'8.	¿Se dan de baja en el inventario estos soportes o documentos desechados?  ',
		70=>'9.	Para los soportes con datos de carácter personal considerados especialmente sensibles por la organización, ¿se utilizan sistemas de etiquetado que permitan la identificación de su contenido a las personas autorizadas y dificulten su identificación al resto? '.campoTextoOtro('otro70',$datos).' ¿Son adecuados y cumplen su finalidad? ',
		71=>'1.	¿Existe un registro de entrada de soportes o documentos así como un registro de salida? ',
		72=>'2.	¿Las personas encargadas de la recepción y la entrega de soportes están debidamente autorizadas? '.campoTextoOtro('otro72',$datos).' ¿Consta en el Documento de Seguridad dicha autorización? ',
		73=>'3. ¿Se han anotado todas las entradas y salidas de soportes? ',
		74=>'1.	¿Se utilizan sistema de etiquetado que permitan la identificación de su contenido a las personas autorizadas y dificulten su identificación al resto? '.campoTextoOtro('otro74',$datos).' ¿Son adecuados y cumplen su finalidad? ',
		75=>'2.	¿La distribución de soportes se realiza de forma cifrada, o por otro mecanismo que garantice que no sea inteligible o manipulable durante el transporte? ',
		76=>'3.	¿Se cifran los datos en los dispositivos portátiles cuando éstos salen de las instalaciones del responsable del tratamiento? '.campoTextoOtro('otro76',$datos).' ¿Qué medidas se adoptan para minimizar los riesgos derivados del tratamiento de dispositivos portátiles fuera de las instalaciones del Responsable del Tratamiento? ',
		77=>'4.	Si fuera imprescindible el tratamiento de datos en dispositivos portátiles que no permitan el cifrado de datos, ¿se ha hecho constar motivadamente en el Documento de Seguridad? '.campoTextoOtro('otro77',$datos).' ¿Se han adoptado medidas para minimizar los riesgos derivados de este tratamiento en entornos desprotegidos? '.campoTextoOtro('otro77_2',$datos).' ¿Son adecuadas? ',
		78=>'1.	¿Se adoptan medidas que impidan el acceso o manipulación de la información en los casos de traslado físico de la documentación contenida en un fichero? ',
		79=>'2.	La generación de copias o reproducción de documentos, ¿se realiza exclusivamente por el personal autorizado en el Documento de Seguridad? ',
		80=>'3.	¿Se destruyen las copias o reproducciones desechadas de forma que no se pueda acceder a la información contenida en las mismas? ',
		//--
		81=>'1.	¿Existe una relación de usuarios con acceso autorizado? '.campoTextoOtro('otro81',$datos).' ¿Se mantiene actualizada? ',
		82=>'2.	¿Existen procedimientos de identificación y autenticación para dicho acceso? '.campoTextoOtro('otro82',$datos).' ¿Garantiza la correcta identificación del usuario? ',
		83=>'3.	El mecanismo de acceso y verificación de autorización de los usuarios, ¿les identifica de forma inequívoca y personalizada? ',
		84=>'4.	¿Existe un procedimiento de asignación, distribución y almacenamiento de contraseñas? '.campoTextoOtro('otro84',$datos).' ¿Garantiza su confidencialidad e integridad? ',
		85=>'5. ¿Se cambian las contraseñas con la periodicidad establecida en el Documento de Seguridad?  ',
		86=>'6.	¿Se almacenan las contraseñas de forma ininteligible mientras están en vigor? ',
		87=>'1.	¿Se limita el intento reiterado de acceso no autorizado al sistema? '.campoTextoOtro('otro87',$datos).'  ¿Se anotan estos intentos en el registro de incidencias? ',
		//--
		88=>'1.	¿El responsable del tratamiento ha definido los procedimientos de realización de copias de respaldo y recuperación de los datos? '.campoTextoOtro('otro88',$datos).' ¿Es adecuada esta definición? ',
		89=>'2.	¿Están reflejados estos procedimientos? '.campoTextoOtro('otro89',$datos).' ¿Ha verificado el responsable del tratamiento la correcta aplicación de estos procedimientos? '.campoTextoOtro('otro89_2',$datos).' ¿Realiza esta verificación periódicamente? ',
		90=>'3.	¿Garantizan los procedimientos establecidos la reconstrucción de los datos al estado en que se encontraban antes de producirse la pérdida o destrucción? ',
		91=>'4.	Si esta pérdida o destrucción afecta a ficheros parcialmente automatizados, ¿se ha procedido a grabar manualmente los datos? '.campoTextoOtro('otro91',$datos).' ¿Queda constancia motivada de este hecho? ',
		92=>'5.	¿Se realizan copias de respaldo al menos semanalmente?  ',
		93=>'1.	¿Se conserva una copia de respaldo y de los procedimientos de recuperación de datos en lugar diferente al de los equipos que los tratan? ',
		94=>'2.	¿Cumple este lugar medidas de seguridad adecuadas? ',
		//
		95=>'1. ¿Existe un registro de accesos?   ',
		96=>'2. ¿Qué información se está recogiendo en este registro? ',
		97=>'3. ¿Los mecanismos que permiten el registro de estos accesos están directamente bajo el control del responsable del tratamiento? ',
		98=>'4. ¿Existe la posibilidad de desactivar estos mecanismos? ',
		99=>'5. ¿Se conservan los datos registrados por un período mínimo? ',
		100=>'6.	¿Revisa el responsable del tratamiento periódicamente la información registrada? ',
		101=>'7.	¿Realiza el responsable del tratamiento un informe periódicamente con el resultado de las revisiones realizadas y los problemas detectados? ',
		102=>'1.	¿El acceso a la documentación que contiene datos de nivel alto, se realiza exclusivamente por  personal autorizado? ',
		103=>'2.	¿Existen mecanismos para identificar los accesos realizados cuando los documentos son utilizados por múltiples usuarios?',
		104=>'3. ¿Se ha establecido un procedimiento para registrar el acceso de personas no incluidas en el caso anterior? ',
		//--
		105=>'1. ¿Los accesos a datos mediante redes de comunicaciones garantizan un nivel de seguridad equivalente a los accesos en modo local? ',
		106=>'1. ¿La transmisión de datos a través de redes se realiza de forma cifrada (o por cualquier otro mecanismo que garantice que la información no sea inteligible ni manipulada por  terceros)? '.campoTextoOtro('otro106',$datos).'¿Este mecanismo de cifrado es eficaz? ',
		//
		107=>'1. ¿Se realiza la actual auditoría en el plazo establecido por el Responsable del Tratamiento? ',
		//
		108=>'1. ¿Se han establecido protocolos internos específicos  con criterios para el archivo de soportes o documentos?'.campoTextoOtro('otro108',$datos).'<ul><li>¿Garantizan estos criterios la conservación de documentos, la localización y consulta de la información con carácter inmediato? '.campoTextoOtro('otro108_2',$datos).'</li><li>¿Posibilitan el ejercicio de los derechos de oposición, acceso, rectificación y cancelación? </li></ul>',
		//
		109=>'1. ¿Los dispositivos de almacenamiento de documentos disponen de mecanismos que obstaculicen su apertura? ',
		//
		110=>'1. ¿Se custodia correctamente la documentación cuando ésta no se encuentra archivada en los dispositivos de almacenamiento por estar en revisión o tramitación? '.campoTextoOtro('otro110',$datos).' ¿Se impide en todo momento que sea accedida por persona no autorizada? '
	);
	return $listado[$num];
}

function datosPersonales($datos,$formulario){
	$datos['razonSocial']=$formulario['pregunta3']!=''?$formulario['pregunta3']:$datos['razonSocial'];
	$datos['domicilio']=$formulario['pregunta4']!=''?$formulario['pregunta4']:$datos['domicilio'];
	$datos['cp']=$formulario['pregunta10']!=''?$formulario['pregunta10']:$datos['cp'];
	$datos['localidad']=$formulario['pregunta5']!=''?$formulario['pregunta5']:$datos['localidad'];
	$datos['provincia']=$formulario['pregunta11']!=''?convertirMinuscula($formulario['pregunta11']):convertirMinuscula($datos['provincia']);
	$datos['cif']=$formulario['pregunta9']!=''?$formulario['pregunta9']:$datos['cif'];

	return $datos;
}

function sanearCaracteres($texto){
	$texto = str_replace( '&', '&#38;', $texto);
	$texto = str_replace( '<', '&#60;', $texto);
	$texto = str_replace( '>', '&#62;', $texto);
	$texto = str_replace( '–', '&#45;', $texto);
	$texto = str_replace( '&#60;br /&#62;', '<w:br/>', $texto);
	$texto = str_replace( '"', '', $texto);
	return $texto;
}

function listadoPreguntasWord($num,$datos){
	$listado=array(
		1=>'1. ¿Ha previsto cómo establecer el registro de actividades de tratamiento en su organización? ',
		2=>'2. ¿Ha valorado si le es de aplicación alguna de las excepciones a esta obligación? ¿Ha previsto quién se encargará de mantener actualizado el registro? ',
		3=>'3. ¿Ha hecho una valoración de los riesgos que los tratamientos que desarrolla implican para los derechos y libertades de los ciudadanos –Informe de Riesgo-?',
		4=>'4. ¿Se han iniciado nuevos Tratamientos de datos de carácter personal o se han modificado o suprimido Tratamientos que se vinieran realizando, desde la última auditoría?',
		5=>'5. ¿Tiene establecida claramente cuál es la base legal de los tratamientos que realiza y ha documentado de alguna forma el modo en que la ha establecido?',
		6=>'6. Si alguno de los tratamientos que realiza está basado en el consentimiento de los interesados, ¿ha verificado que ese consentimiento reúne los requisitos que exige el RGDP? En caso contrario, ¿ha previsto cómo recabar el consentimiento de forma adaptada al RGPD o ha encontrado otra base legal adecuada para esos tratamientos?  ',
		7=>'7. La información que se proporciona a los interesados, ¿está presentada de forma clara, concisa, transparente y de fácil acceso?  ',
		8=>'8. ¿Contiene esa información todos los elementos que prevé el RGPD? ',
		9=>'9. ¿Dispone de mecanismos para el ejercicio de derechos visibles, accesibles y sencillos? ',
		10=>'10. ¿Pueden ejercerse los derechos por vía electrónica? ',
		11=>'11. ¿Tiene establecidos procedimientos o mecanismos que le permitan verificar la identidad de quienes solicitan acceso o ejercen los demás derechos ARCO? ',
		12=>'12. ¿Tiene establecidos procedimientos que le permitan responder a los ejercicios de derechos en los plazos previstos por el RGPD? ',
		//-------
		13=>'1. ¿Se realiza el tratamiento por persona distinta al responsable del mismo?  Y, en caso afirmativo, ¿se ha formalizado mediante contrato conforme lo establecido en el artículo 28 RGPD? ',
		14=>'2. ¿Ha previsto cómo valorar si los encargados con los que haya contratado o vaya a contratar operaciones de tratamiento ofrecen garantías de cumplimiento del RGPD cuando sea de aplicación? ',
		15=>'3. Los contratos de encargo ¿incluyen la colaboración de los encargados en la respuesta a las solicitudes de los interesados? ',
		16=>'4. ¿Se ha hecho  constar en el Documento de Seguridad si la realización de este encargo se realiza en los locales del responsable, en locales propios del encargado de tratamiento (distintos de los del responsable) o si se realiza mediante acceso remoto a los sistemas del responsable? ',
		17=>'5. En el supuesto que la realización de este encargo se realice en los locales del responsable, ¿consta por escrito en el contrato, el compromiso del personal del encargado de tratamiento respecto al cumplimiento de las medidas de seguridad recogidas en el Documento de Seguridad del responsable? ',
		18=>'6. Cuando el tratamiento se realiza mediante acceso remoto a los sistemas del responsable, ¿se le ha prohibido al encargado de tratamiento la incorporación de los datos a sistemas o soportes distintos de los del responsable?  ',
		19=>'7. Si la prestación se hace en locales propios del encargado de tratamiento (distintos de los del responsable), ¿ha elaborado el encargado el documento de seguridad y el Informe de Riesgo?  '.$datos['otro19'].' ¿Identifica el fichero o tratamiento y el responsable del mismo y detalla las medidas de seguridad a implementar en relación con su tratamiento?  ',
		//--
		20=>'1.	Si la prestación del servicio contratado no implica tratamiento de datos personales, ¿se han adoptado las medidas necesarias para limitar el acceso del personal a los datos personales, soportes y recursos? ',
		21=>'2.	Si se trata de personal ajeno, ¿recoge el contrato la prohibición expresa de acceder a los datos personales, así como la obligación de secreto respecto a los datos que hubieran podido conocer con motivo de la prestación de servicio? ',
		//--
		22=>'1.	¿Se han delegado las autorizaciones que el Reglamento atribuye al responsable en otras personas? '.$datos['otro22'].'  ¿Se ha hecho constar en el Documento de Seguridad las personas habilitadas para otorgar estas autorizaciones y las personas en quienes recae dicha delegación?',
		//--
		23=>'1. El almacenamiento de datos personales en dispositivos portátiles o los tratamientos fuera de los locales del responsable o del encargado, ¿han sido autorizados expresamente por el responsable del tratamiento? '.$datos['otro23'].'  ¿Consta dicha autorización en el Documento de Seguridad? '.$datos['otro23_2'].' ¿Se garantiza el nivel de seguridad correspondiente al tipo de tratamiento realizado? ',
		//--
		24=>'1. ¿Cumplen el nivel de seguridad correspondiente? ',
		25=>'2.	¿Se han destruido o borrado cuando ya no han sido necesarios para los fines que motivaron su creación?',
		//--
		26=>'1.	¿Ha elaborado el responsable del tratamiento el Documento de Seguridad? ',
		27=>'2.	¿Ha revisado las medidas de seguridad que aplica a sus tratamientos a la luz de los resultados del análisis de riesgo de los mismos? ¿Considera que puede seguir aplicando las medidas de seguridad previstas en el Reglamento de la LOPD? ¿Ha valorado suficientemente la posibilidad de introducir medidas adicionales en función del tipo de tratamiento o del contexto en que se realiza? ',
		28=>'3.	¿Ha determinado qué medidas de responsabilidad activa corresponden a su situación de riesgo y cómo debe aplicarlas? ',
		29=>'4.	¿Ha valorado si los tratamientos que realiza requieren una Evaluación de Impacto sobre la Protección de Datos porque supongan un alto riesgo para los derechos y libertades de los interesados? ',
		30=>'5.	¿Dispone de una metodología para la realización de la Evaluación de Impacto? ',
		31=>'6.	Según el tipo de tratamiento que realiza y los resultados del análisis de riesgos previo, ¿tiene que nombrar un Delegado de Protección de Datos? ',
		32=>'7.	Independientemente del resultado del análisis de riesgos previo, ¿se ha decidido nombrar un Delegado de Protección de Datos? ',
		33=>'8.	El puesto de DPD tal y como está configurado en su organización, ¿respeta los requisitos de independencia en el ejercicio de las funciones, posición en el organigrama, ausencia de conflicto de intereses y disponibilidad de los recursos necesarios establecidos por el RGPD?',
		34=>'9.	¿Ha comunicado la designación del DPD a la autoridad de protección de datos? ',
		35=>'10. ¿Ha hecho públicos los datos de contacto del DPD? ',
		36=>'11. ¿Ha establecido procedimientos para que los interesados contacten con el DPD? ',
		37=>'12. ¿Se especifica cuál es el personal autorizado para la concesión, alteración o anulación de accesos autorizados sobre datos o recursos?  ',
		38=>'13. ¿Se especifica cuál es el personal autorizado para acceder a los lugares donde se almacenan los soportes informáticos? ',
		39=>'14. Si el tratamiento se realiza por cuenta de terceros, ¿se han reflejado los tratamientos afectados por el encargo, con referencia expresa  al contrato, así como la identificación del responsable y el periodo de vigencia? ',
		40=>'15. ¿Se han reflejado en el Documento de Seguridad si los datos personales se incorporan y tratan exclusivamente en los sistemas del encargado? ',
		41=>'16. ¿Contiene los procedimientos y controles periódicos a realizar para verificar el cumplimiento de lo dispuesto en el propio documento?  ',
		42=>'17. ¿Especifica qué medidas hay que adoptar en caso de desechado o reutilización de soportes? ',
		43=>'18. ¿Relaciona las personas que están autorizadas a acceder físicamente a los locales donde se ubican los sistemas de información? ',
		//--
		44=>'1.	¿Están las funciones y obligaciones del personal con acceso a datos de carácter personal y los sistemas de información claramente definidos? ',
		45=>'2. ¿Están documentadas y reflejadas en el documento de seguridad?  ',
		46=>'3. ¿Conoce el personal las medidas de seguridad que afectan al desarrollo de sus funciones así como las consecuencias de su incumplimiento? ',
		47=>'4. ¿Se han definido las funciones de control o autorizaciones delegadas por el responsable del tratamiento?  ',
		//--
		48=>'1. ¿Existe un procedimiento de notificación y gestión de incidencias de seguridad? '.$datos['otro48'].' ¿El procedimiento está bien diseñado y es eficaz? '.$datos['otro48_2'].' ¿Conoce todo el personal afectado dicho procedimiento? ',
		49=>'2. ¿Ha establecido procedimientos para notificar las violaciones de seguridad a las autoridades de protección de datos y, si fuera necesario, a los interesados? ',
		50=>'3. ¿Dispone de un registro o herramienta similar en que pueda documentar los incidentes de seguridad que se produzcan, aunque no sean notificados a las autoridades de protección de datos? ',
		51=>'4. ¿Se revisa periódicamente el registro de incidencias para su análisis y adopción de medidas correctoras de las incidencias anotadas?  ',
		52=>'1. ¿Se han anotado las ejecuciones de los procedimientos de recuperación de datos realizados? ',
		//--
		53=>'1. ¿Los accesos autorizados de los usuarios se corresponden exclusivamente a los datos y recursos que precisan para el desarrollo de sus funciones? ',
		54=>'2. ¿Existen mecanismos que impidan que los usuarios accedan a datos o recursos distintos de los autorizados? ',
		55=>'3. ¿Existe una relación de usuarios? '.$datos['otro55'].' ¿Especifica qué datos y recursos tiene autorizados para cada uno de ellos?  '.$datos['otro55_2'].'. ¿Está actualizada? ',
		56=>'4. ¿La concesión, alteración o anulación de accesos autorizados sobre datos y recursos la realiza exclusivamente el personal autorizado para ello en el Documento de Seguridad? ',
		57=>'5. ¿Ha establecido el responsable del tratamiento los criterios conforme a los cuales se otorga la autorización de los accesos a los datos y a los recursos? ',
		58=>'6. El personal ajeno al responsable que tiene acceso a los datos y recursos de éste, ¿se encuentra sometido a las mismas condiciones y  obligaciones que el personal propio?  ',
		59=>'1. ¿El acceso a los locales donde se encuentran ubicados los sistemas de información, se realiza exclusivamente por el personal autorizado en el Documento de Seguridad? ',
		60=>'1. ¿Se encuentran los archivadores u otros elementos de almacenamiento en áreas de acceso restringido dotadas de sistemas de apertura mediante llave u otro dispositivo equivalente? '.$datos['otro60'].' ¿Están cerradas estas áreas mientras no sea preciso el acceso a los documentos incluidos en el fichero?  ',
		61=>'2. Si los locales del responsable no permiten disponer de un área de acceso restringido ¿ha adoptado el responsable medidas alternativas?',
		//--
		62=>'1.	¿Está identificado el tipo de información contenido en el soporte o documento? ',
		63=>'2.	¿Existe y se mantiene un inventario de soportes? ',
		64=>'3.	¿Se almacenan los soportes o documentos en lugares de acceso restringido? ',
		65=>'4.	¿Existen mecanismos por los que solamente puedan acceder las personas autorizadas en el Documento de Seguridad? '.$datos['otro65'].' ¿Funcionan adecuadamente estos mecanismos? ',
		66=>'5.	¿La salida de soportes y documentos fuera de los locales donde se ubica el fichero está siendo autorizada por el responsable del tratamiento? ',
		67=>'6.	¿Se están tomando las medidas adecuadas en el traslado de documentación para evitar la sustracción, pérdida o acceso indebido durante su transporte? ',
		68=>'7.	Cuando se desecha un soporte o documento conteniendo datos de carácter personal, ¿se adoptan las medidas adecuadas para evitar el acceso a la información o su recuperación posterior cuando se procede a su destrucción o borrado?   ',
		69=>'8.	¿Se dan de baja en el inventario estos soportes o documentos desechados?  ',
		70=>'9.	Para los soportes con datos de carácter personal considerados especialmente sensibles por la organización, ¿se utilizan sistemas de etiquetado que permitan la identificación de su contenido a las personas autorizadas y dificulten su identificación al resto? '.$datos['otro70'].' ¿Son adecuados y cumplen su finalidad? ',
		71=>'1.	¿Existe un registro de entrada de soportes o documentos así como un registro de salida? ',
		72=>'2.	¿Las personas encargadas de la recepción y la entrega de soportes están debidamente autorizadas? '.$datos['otro72'].' ¿Consta en el Documento de Seguridad dicha autorización? ',
		73=>'3. ¿Se han anotado todas las entradas y salidas de soportes? ',
		74=>'1.	¿Se utilizan sistema de etiquetado que permitan la identificación de su contenido a las personas autorizadas y dificulten su identificación al resto? '.$datos['otro74'].' ¿Son adecuados y cumplen su finalidad? ',
		75=>'2.	¿La distribución de soportes se realiza de forma cifrada, o por otro mecanismo que garantice que no sea inteligible o manipulable durante el transporte? ',
		76=>'3.	¿Se cifran los datos en los dispositivos portátiles cuando éstos salen de las instalaciones del responsable del tratamiento? '.$datos['otro76'].' ¿Qué medidas se adoptan para minimizar los riesgos derivados del tratamiento de dispositivos portátiles fuera de las instalaciones del Responsable del Tratamiento? ',
		77=>'4.	Si fuera imprescindible el tratamiento de datos en dispositivos portátiles que no permitan el cifrado de datos, ¿se ha hecho constar motivadamente en el Documento de Seguridad? '.$datos['otro77'].' ¿Se han adoptado medidas para minimizar los riesgos derivados de este tratamiento en entornos desprotegidos? '.$datos['otro77_2'].' ¿Son adecuadas? ',
		78=>'1.	¿Se adoptan medidas que impidan el acceso o manipulación de la información en los casos de traslado físico de la documentación contenida en un fichero? ',
		79=>'2.	La generación de copias o reproducción de documentos, ¿se realiza exclusivamente por el personal autorizado en el Documento de Seguridad? ',
		80=>'3.	¿Se destruyen las copias o reproducciones desechadas de forma que no se pueda acceder a la información contenida en las mismas? ',
		//--
		81=>'1.	¿Existe una relación de usuarios con acceso autorizado? '.$datos['otro81'].' ¿Se mantiene actualizada? ',
		82=>'2.	¿Existen procedimientos de identificación y autenticación para dicho acceso? '.$datos['otro82'].' ¿Garantiza la correcta identificación del usuario? ',
		83=>'3.	El mecanismo de acceso y verificación de autorización de los usuarios, ¿les identifica de forma inequívoca y personalizada? ',
		84=>'4.	¿Existe un procedimiento de asignación, distribución y almacenamiento de contraseñas? '.$datos['otro84'].' ¿Garantiza su confidencialidad e integridad? ',
		85=>'5. ¿Se cambian las contraseñas con la periodicidad establecida en el Documento de Seguridad?  ',
		86=>'6.	¿Se almacenan las contraseñas de forma ininteligible mientras están en vigor? ',
		87=>'1.	¿Se limita el intento reiterado de acceso no autorizado al sistema? '.$datos['otro87'].'  ¿Se anotan estos intentos en el registro de incidencias? ',
		//--
		88=>'1.	¿El responsable del tratamiento ha definido los procedimientos de realización de copias de respaldo y recuperación de los datos? '.$datos['otro88'].' ¿Es adecuada esta definición? ',
		89=>'2.	¿Están reflejados estos procedimientos? '.$datos['otro89'].' ¿Ha verificado el responsable del tratamiento la correcta aplicación de estos procedimientos? '.$datos['otro89_2'].' ¿Realiza esta verificación periódicamente? ',
		90=>'3.	¿Garantizan los procedimientos establecidos la reconstrucción de los datos al estado en que se encontraban antes de producirse la pérdida o destrucción? ',
		91=>'4.	Si esta pérdida o destrucción afecta a ficheros parcialmente automatizados, ¿se ha procedido a grabar manualmente los datos? '.$datos['otro91'].' ¿Queda constancia motivada de este hecho? ',
		92=>'5.	¿Se realizan copias de respaldo al menos semanalmente?  ',
		93=>'1.	¿Se conserva una copia de respaldo y de los procedimientos de recuperación de datos en lugar diferente al de los equipos que los tratan? ',
		94=>'2.	¿Cumple este lugar medidas de seguridad adecuadas? ',
		//
		95=>'1. ¿Existe un registro de accesos?   ',
		96=>'2. ¿Qué información se está recogiendo en este registro? ',
		97=>'3. ¿Los mecanismos que permiten el registro de estos accesos están directamente bajo el control del responsable de del tratamiento? ',
		98=>'4. ¿Existe la posibilidad de desactivar estos mecanismos? ',
		99=>'5. ¿Se conservan los datos registrados por un período mínimo? ',
		100=>'6.	¿Revisa el responsable del tratamiento periódicamente la información registrada? ',
		101=>'7.	¿Realiza el responsable del tratamiento un informe periódicamente con el resultado de las revisiones realizadas y los problemas detectados? ',
		102=>'1.	¿El acceso a la documentación que contiene datos de nivel alto, se realiza exclusivamente por  personal autorizado? ',
		103=>'2.	¿Existen mecanismos para identificar los accesos realizados cuando los documentos son utilizados por múltiples usuarios?',
		104=>'3. ¿Se ha establecido un procedimiento para registrar el acceso de personas no incluidas en el caso anterior? ',
		//--
		105=>'1. ¿Los accesos a datos mediante redes de comunicaciones garantizan un nivel de seguridad equivalente a los accesos en modo local? ',
		106=>'1. ¿La transmisión de datos a través de redes se realiza de forma cifrada (o por cualquier otro mecanismo que garantice que la información no sea inteligible ni manipulada por  terceros)? '.$datos['otro106'].'¿Este mecanismo de cifrado es eficaz? ',
		//
		107=>'1. ¿Se realiza la actual auditoría en el plazo establecido por el Responsable del Tratamiento? ',
		//
		108=>'1. ¿Se han establecido protocolos internos específicos  con criterios para el archivo de soportes o documentos?'.$datos['otro108'].'<ul><li>¿Garantizan estos criterios la conservación de documentos, la localización y consulta de la información con carácter inmediato? '.$datos['otro108_2'].'</li><li>¿Posibilitan el ejercicio de los derechos de oposición, acceso, rectificación y cancelación? </li></ul>',
		//
		109=>'1. ¿Los dispositivos de almacenamiento de documentos disponen de mecanismos que obstaculicen su apertura? ',
		//
		110=>'1. ¿Se custodia correctamente la documentación cuando ésta no se encuentra archivada en los dispositivos de almacenamiento por estar en revisión o tramitación? '.$datos['otro110'].' ¿Se impide en todo momento que sea accedida por persona no autorizada? '
	);
	return $listado[$num];
}

function revisarNivelAlto($ajax=true){
	$datos=arrayFormulario();
	$res='NO';
	if($_POST['finalizada']=='NO'){
		$declaraciones=consultaBD('SELECT COUNT(codigo) AS total FROM declaraciones WHERE (fechaBaja="0000-00-00" OR fechaBaja="" OR fechaBaja>"'.$datos['fecha'].'") AND nivel="3" AND codigoCliente='.$datos['codigoCliente'],true,true);
	} else {
		$declaraciones=consultaBD('SELECT COUNT(codigo) AS total FROM auditorias_internas_tratamientos WHERE (fechaBaja!="0000-00-00" OR fechaBaja!="" OR fechaBaja>"'.$datos['fecha'].'") AND nivel="3" AND codigoAuditoria='.$datos['codigoAuditoria'],true,true);
	}
	if($declaraciones['total']>0){
		$res='SI';
	}
	echo $res;
}

function obtenerTratamientosAjax(){
	$res='';
	$declaraciones=consultaBD('SELECT codigo, nombreFichero, nivel, tratamiento, fechaBaja FROM declaraciones WHERE (fechaBaja="0000-00-00" OR fechaBaja="" OR fechaBaja>"'.$_POST['fecha'].'") AND codigoCliente='.$_POST['codigoCliente'],true);
	$i=0;
	while($item=mysql_fetch_assoc($declaraciones)){
		$res.="<tr>";
		$res.=campoOcultoMostrar($item['codigo'],'codigoTratamiento'.$i);
		$res.=campoTextoTablaMostrar('nombreFichero'.$i,$item['nombreFichero']);
		$res.=campoSelectMostrar('nivel'.$i,'Nivel de medidas de seguridad',array('Básico','Medio','Alto'),array(1,2,3),$item['nivel'],'span2 selectpicker show-tick selectNivel',"data-live-search='true'",1);
		$res.=campoSelectMostrar('tratamiento'.$i,'Sistema de tratamiento',array('Automatizado','Manual','Mixto'),array('1','2','3'),$item['tratamiento'],'selectpicker span2 show-tick',"data-live-search='true'",1);
		$res.=campoFechaTablaMostrar('fechaBaja'.$i,formateaFechaWeb($item['fechaBaja']));
		$res.="</tr>";
		$i++;
	}
	echo $res;
}

//Fin parte de ventas de formación
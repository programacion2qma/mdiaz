<?php

@include_once('funciones.php');
compruebaSesion();

$contenido=generaInforme();
$nombre='Auditoria_interna.pdf';

require_once('../../api/html2pdf/html2pdf.class.php');
$html2pdf=new HTML2PDF('P','A4','es');
$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->setDefaultFont("calibri");
$html2pdf->WriteHTML($contenido);
$html2pdf->Output($nombre);


function generaInforme(){

conexionBD();
    	$datos = datosRegistro("auditorias_internas",$_GET['codigo']);
    	if($datos['emitida']=='NO'){
    		$consulta=consultaBD('UPDATE auditorias_internas SET emitida="SI" WHERE codigo='.$datos['codigo']);
    	}
    	$formulario=prepararPreguntas($datos);
    	$fechas=consultaBD('SELECT * FROM auditorias_internas_fechas WHERE codigoAuditoria='.$datos['codigo'].' ORDER BY fecha');
    	$cliente = datosRegistro('clientes',$datos['codigoCliente']);
    	$trabajo = consultaBD('SELECT * FROM trabajos WHERE codigoCliente='.$datos['codigoCliente'].' ORDER BY codigo DESC');
  		$trabajo=mysql_fetch_assoc($trabajo);
  		$ficheros=consultaBD('SELECT * FROM declaraciones WHERE codigoCliente='.$cliente['codigo'],$conexion=false,$assoc=false);
cierraBD();

$fechasTexto='';
$sesiones=0;
while($fecha=mysql_fetch_assoc($fechas)){
	if($fechasTexto!=''){
		$fechasTexto.=', ';
	} else {
		$primerDia=$fecha['fecha'];
	}
	$fechasTexto.=formateaFechaWeb($fecha['fecha']);
	$sesiones++;
}
$formularioTrabajo = recogerFormularioServicios($trabajo);
$meses=array(''=>'','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
$fechaPartida=explode('-', $datos['fechaEmision']);
$fechaEmisionTexto = $fechaPartida[2].' DE '.strtoupper($meses[$fechaPartida[1]]).' DE '.$fechaPartida[0];
$fechaPartida=explode('-', $primerDia);
$primerDiaTexto = $fechaPartida[2].' DE '.strtoupper($meses[$fechaPartida[1]]).' DE '.$fechaPartida[0];
$niveles=array(1=>'Básico',2=>'Medio',3=>'Alto');
$tratamientos=array(1=>'Automatizado',2=>'Manual',3=>'Mixto');
$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        .cabecera{
	        	border:0px solid #000;
	        	width:85%;
	        	border-collapse:collapse;
	        	margin-top:30px;
	        	margin-left:70px;
	        }

	        .cabecera td{
	        	border:0px solid #000;
	        }

	        .cabecera .tdLogo{
	        	width:19%;
	        	text-align:center;
	        	border-right:0px;
	        }

	        .cabecera .tdLogo img{
	        	width:100%;
	        }

	        .cabecera .tdTitulo{
	        	width:81%;
	        	padding-top:60px;
	        	text-align:center;
	        }

	        .cabecera .tdCliente{
	        	text-align:left;
	        	padding-left:10px;
	        	background:#56A4C0;
	        	color:#FFF;
	        	border-left:1px solid #56A4C0;
	        }

	        p{
	        	line-height:15px;
	        	margin-bottom:0px;
	        }

	        .pregunta{
	        	font-weight:bold;
	        }

	        .def{
	        	text-decoration:underline;
	        	font-weight:bold;
	        }

	        .respuesta{
	        	font-weight:normal;
	        }

	        .titulo{
	        	text-align:center;
	        	font-weight:bold;
	        	margin-top:20px;
	        }

	        .container{
	        	width:75%;
	        	margin-left:100px;
	        }

	        h1{
	        	text-align:center;
	        	font-size:16px;
	        }

	        .rojo{
	        	color:red;
	        }

	        .tablaFirma{
	        	margin-top:500px;
	        	width:100%;
	        }

	        .tablaFirma td{
	        	width:50%;
	        	text-align:center;
	        }

	        h2{
	        	text-align:center;
	        	line-height:40px;
	        }

	        .indice h1{
	        	text-align:left;
	        	margin:5px 0px;
	        }

	        .indice .subindice{
	        	padding-left:20px;
	        }

	        .divFicheros{
	        	border-left:1px solid #000;
	        	margin-top:10px;
	        }

	        .divFicheros p{
	        	margin-top:0px;
	        	margin-bottom:10px;
	        }

	        .divFicheros .total{
	        	margin-top:10px;
	        	margin-bottom:0px;
	        	background:#DDD;
	        	text-align:right;
	        	padding-right:5px;
	        }

	        .tablaFicheros{
	        	border:1px solid #000;
	        	border-collapse:collapse;
	        	margin-left:10px;
	        }

	        .tablaFicheros td,
	        .tablaFicheros th,{
	        	border:1px solid #000;
	        }

	        .tablaFicheros th{
	        	text-align:center;
	        	background:#CCC;
	        }

	        .a10{
	        	width:9%;
	        }

	        .a20{
	        	width:19%;
	        }

	        .a30{
	        	width:30%;
	        }

	        .a40{
	        	width:40%;
	        }

	        .centro{
	        	text-align:center;
	        }

	        .amarillo{
	        	background:#F8A700;
	        	padding-left:5px;
	        }
	-->
	</style>
	<page footer='page' backtop='40mm'>
	<page_header>
	<table class='cabecera'>
		<tr>
			<td class='tdLogo'>
				<img src='../img/logoInforme.png'>
			</td>
			<td class='tdTitulo'><h1>PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL</h1></td>	
		</tr>
	</table>
	</page_header>
	<div class='container'>
		<br/><br/><br/>
		<h2>INFORME DE AUDITORÍA DE LOS<bR/>SISTEMAS DE INFORMACIÓN E<br/>INSTALACIONES DE TRATAMIENTO DE<br/>DATOS DE CARÁCTER PERSONAL DE:</h2>
		<br/><br/><br/>
		<h2>".$formularioTrabajo['pregunta3']."<br/>CIF: ".$formularioTrabajo['pregunta9']."<br/>".$formularioTrabajo['pregunta4']."<br/>CP ".$formularioTrabajo['pregunta10']." ".$formularioTrabajo['pregunta5']."</h2>
		<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
		<h2>".$formularioTrabajo['pregunta5'].",  A ".$fechaEmisionTexto."</h2>
	</div>
	</page>
	<page footer='page' backtop='40mm'>
	<page_header>
	<table class='cabecera'>
		<tr>
			<td class='tdLogo'>
				<img src='../img/logoInforme.png'>
			</td>
			<td class='tdTitulo'><h1>PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL</h1></td>	
		</tr>
	</table>
	</page_header>
	<div class='container'>
		<h1>INDICE</h1>
		<div class='indice'>
		<h1>1º. DATOS IDENTIFICATIVOS DEL RESPONSABLE DEL TRATAMIENTO.</h1>
		<h1>2º. EQUIPO AUDITOR.</h1>
		<h1>3º. TRATAMIENTOS  OBJETO DE LA AUDITORÍA.</h1>
		<h1>4º. FECHA DE LA AUDITORÍA.</h1>
		<h1>5º. RECURSOS Y RECOLECCIÓN DE DATOS.</h1>
		<h1>6º. EVALUACIÓN DE LAS PRUEBAS:</h1>
		<div class='subindice'>
			<h1>I.	ASPECTOS GENERALES.</h1>
			<h1>II.	ENCARGADO DE TRATAMIENTO.</h1>
			<h1>III.	PRESTACIÓN DE SERVICIO SIN ACCESO A DATOS PERSONALES.</h1>
			<h1>IV.	DELEGACIÓN DE AUTORIZACIONES.</h1>
			<h1>V.	RÉGIMEN DE TRABAJO FUERA DE LOS LOCALES DE LA UBICACIÓN DEL FICHERO.</h1>
			<h1>VI.	FICHEROS TEMPORALES O COPIAS DE TRABAJO DE DOCUMENTOS.</h1>
			<h1>VII.	DOCUMENTO DE SEGURIDAD/ INFORME DE RIESGO.</h1>
			<h1>VIII.	FUNCIONES Y OBLIGACIONES DEL PERSONAL.</h1>
			<h1>IX.	REGISTRO DE INCIDENCIAS.</h1>
			<h1>X.	CONTROL DE ACCESO.</h1>
			<h1>XI.	GESTIÓN DE SOPORTES Y DOCUMENTOS.</h1>
			<h1>XII.	IDENTIFICACIÓN Y AUTENTICACIÓN.</h1>
			<h1>XIII.	COPIAS DE RESPALDO Y RECUPERACIÓN.</h1>
			<h1>XIV.	REGISTRO DE ACCESOS.</h1>
			<h1>XV.	ACCESO A DATOS A TRAVÉS DE REDES DE COMUNICACIONES.</h1>
			<h1>XVI.	AUDITORÍA.</h1>
			<h1>XVII.	CRITERIOS DE ARCHIVO.</h1>
			<h1>XVIII.	ALMACENAMIENTO DE LA INFORMACIÓN.</h1>
			<h1>XIX.	CUSTODIA DE SOPORTES.</h1>
		</div>
		<h1>7º. CONCLUSIONES. TRASLADO DE CONCLUSIONES AL RESPONSABLE DE TRATAMIENTOS.</h1>
		</div>
	</div>
	</page>
	<page footer='page' backtop='40mm'>
	<page_header>
	<table class='cabecera'>
		<tr>
			<td class='tdLogo'>
				<img src='../img/logoInforme.png'>
			</td>
			<td class='tdTitulo'><h1>PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL</h1></td>	
		</tr>
	</table>
	</page_header>
	<div class='container'>
		<h1>1º. DATOS IDENTIFICATIVOS DEL RESPONSABLE DE TRATAMIENTOS</h1>
		<p>Responsable de Tratamientos: ".$formularioTrabajo['pregunta3']." -en lo sucesivo “Responsable de Tratamientos”-</p>
		<p>CIF: ".$formularioTrabajo['pregunta9']."</p>
		<p>Dirección: ".$formularioTrabajo['pregunta4']."</p>
		<p>CP ".$formularioTrabajo['pregunta10']." ".$formularioTrabajo['pregunta5']."</p>
		<p>Representante legal: D./Dª  ".$formularioTrabajo['pregunta8']."</p>
		<p>Delegado de Protección de Datos (sólo en el supuesto que sea nombrado): ".$formularioTrabajo['pregunta604']."</p>
		<p>NIF/CIF del Delegado de Protección de Datos : ".$formularioTrabajo['pregunta606']."</p>
		<p>Datos de contacto del Delegado de Protección de Datos: ".$formularioTrabajo['pregunta605']."</p>
		<p>Actividad: ".$formularioTrabajo['pregunta13']."</p>
		<br/><br/><br/>
		<h1>2º. EQUIPO AUDITOR</h1>
		<p>D. ".$formularioTrabajo['pregunta604']." -".$formularioTrabajo['pregunta608']."- empleado/a del Responsable de Tratamientos /Delegado de Protección de Datos, con NIF ".$formularioTrabajo['pregunta6'].", es designado/a auditor/a de los sistemas de información e instalaciones de tratamiento y almacenamiento de datos del “Responsable de Tratamientos”, a efectos de comprobar si los mismos cumplen con lo establecido por el Reglamento 2016/679 del Parlamento Europeo y del Consejo de 27 de abril del 2016 (Reglamento General de Protección de Datos) y otros preceptos al respecto previstos en el referido texto.</p>
	</div>
	</page>
	<page footer='page' backtop='40mm'>
	<page_header>
	<table class='cabecera'>
		<tr>
			<td class='tdLogo'>
				<img src='../img/logoInforme.png'>
			</td>
			<td class='tdTitulo'><h1>PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL</h1></td>	
		</tr>
	</table>
	</page_header>
	<div class='container'>
		<h1>3º. TRATAMIENTOS/FICHEROS OBJETO DE LA AUDITORÍA</h1>
		<p>Son objeto de la presente Auditoría todos los Tratamientos que constan en el correspondiente Registro del Responsable de Tratamientos, a día de la primera sesión de auditoría -".formateaFechaWeb($primerDia)."-:</p>
		<div class='divFicheros'>
		<p class='amarillo'>REGISTRO DE ACTIVIDADES DE TRATAMIENTO A ".$primerDiaTexto."</p>
		
			<table class='tablaFicheros'>
				<tr>
					<th class='a10'>REF</th>
					<th class='a40'>ACTIVIDADES DE TRATAMIENTO<br/>–FICHEROS-</th>
					<th class='a20'>NIVEL DE<br/>SEGURIDAD</th>
					<th class='a30'>SISTEMA DE<br/>TRATAMIENTO</th>
				</tr>";
				$i=0;
				while($fichero=mysql_fetch_assoc($ficheros)){
					$i++;
					$ref=$i<10?'0'.$i:$i;
$contenido.="
				<tr>
					<td class='a10 centro'>".$ref."</td>
					<td class='a40'>".$fichero['nombreFichero']."</td>
					<td class='a20 centro'>".$niveles[$fichero['nivel']]."</td>
					<td class='a30'>".$tratamientos[$fichero['tratamiento']]."</td>
				</tr>";

				}
$contenido.="			
			</table>
			<div class='total'><p>Nº Total de registro: ".$i."</p></div>
		</div>
		<h1>4º. FECHA DE LA AUDITORÍA</h1>
		<p>El análisis de la situación y medidas de seguridad se ha desarrollado en ".$sesiones." sesiones, los días: ".$fechasTexto.", emitiéndose el presente Informe el día ".formateaFechaWeb($datos['fechaEmision']).".</p>
		<h1>5º. RECURSOS Y RECOLECCIÓN DE DATOS</h1>
		<p>Para la realización de la presente auditoría, el equipo auditor  ha utilizado los siguientes recursos:</p>
		<ul style='list-style-type:none;'>
			<li>- Inspección ocular realizada en las propias oficinas o dependencias del Responsable de Tratamientos.  </li>
			<li>- Análisis de diversos documentos, tales como: Documento de Seguridad y sus Anexos, relación de tratamientos, estructura y contenido, Informe de Riesgo, Evaluación de Impacto, políticas de seguridad y procedimientos registrados (registro de incidencias, copias de respaldo y recuperación, identificación y autorización, cifrado, borrado de soportes), inventario de soportes y registro de entrada y salida de soportes, relación de usuarios, accesos autorizados y sus funciones; análisis del programa de gestión LOPD.</li>
			<li>- Otros: ".$datos['recursos']."</li>
		</ul>
	</div>
	</page>
	";
$contenido.="
	<page footer='page' backtop='40mm' backleft='25mm' backright='25mm'> 
	<page_header>
	<table class='cabecera'>
		<tr>
			<td class='tdLogo'>
				<img src='../img/logoInforme.png'>
			</td>
			<td class='tdTitulo'><h1>PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL</h1></td>	
		</tr>
	</table>
	</page_header>
	<div>
	<h1>6º. EVALUACIÓN DE LAS PRUEBAS</h1>
	<p><b>A los efectos de verificar el cumplimiento de las obligaciones establecidas por el RGPD, se han realizado, por el equipo auditor, las siguientes comprobaciones:</b></p>";
		$titulos=array(1,13,20,22,23,24,26,44,48,52,53,59,60,62,71,74,78,81,87,88,93,95,102,105,106,107,108,109,110);
		for($i=1;$i<=110;$i++){
			$pregunta=listadoPreguntas($i,$formulario);
			$pregunta=explode('<input', $pregunta);
			if($i==108){
				$pregunta2=explode('<ul>', $pregunta[1]);
				$pregunta3=explode('</li>', $pregunta[2]);
				$pregunta=$pregunta[0].'<span class="respuesta">'.$formulario['otro'.$i].'</span><ul>'.$pregunta2[1].'<span class="respuesta">'.$formulario['otro'.$i.'_2'].'</span></li>'.$pregunta3[1].'</li></ul>';
			} else {
				if(count($pregunta)==1){
					$pregunta=$pregunta[0];
				} else if (count($pregunta)==2){
					$pregunta2=explode('>', $pregunta[1]);
					$pregunta=$pregunta[0].'<span class="respuesta">'.$formulario['otro'.$i].'</span>'.$pregunta2[1];
				} else if (count($pregunta)==3){
					$pregunta2=explode('>', $pregunta[1]);
					$pregunta3=explode('>', $pregunta[2]);
					$pregunta=$pregunta[0].'<span class="respuesta">'.$formulario['otro'.$i].'</span>'.$pregunta2[1].'<span class="respuesta">'.$formulario['otro'.$i.'_2'].'</span>'.$pregunta3[1];
				}
			}
			if(in_array($i, $titulos)){
				$contenido.='<p class="titulo">'.obtenerTitulo($i).'</p>';
			}
			$contenido.='<p class="pregunta">'.$pregunta.'</p>';
			$contenido.='<p>'.$formulario['pregunta'.$i].'</p>';
			if($i==19){
				$contenido.='<p><b>Art. 28.1 RGPD:</b> "Cuando se vaya a realizar un tratamiento por cuenta de un responsable del tratamiento, este elegirá únicamente un encargado que ofrezca garantías suficientes para aplicar medidas técnicas y organizativas apropiadas, de manera que el tratamiento sea conforme con los requisitos del presente Reglamento y garantice la protección de los derechos del interesado."<br/><b>Art. 28.3.h):</b> "El tratamiento por el encargado se regirá por un contrato u otro acto jurídico con arreglo al Derecho de la Unión o de los Estados miembros, que vincule al encargado respecto del responsable y establezca el objeto, la duración, la naturaleza y la finalidad del tratamiento, el tipo de datos personales y categorías de interesados, y las obligaciones y derechos del responsable. Dicho contrato o acto jurídico estipulará, en particular, que el encargado: a) … h) pondrá a disposición del responsable toda la información necesaria para demostrar el cumplimiento de las obligaciones establecidas en el presente artículo, así como para permitir y contribuir a la realización de auditorías, incluidas inspecciones, por parte del responsable o de otro auditor autorizado por dicho responsable. "</p>';
			}
			$contenido.='<p> <span class="def">DEFICIENCIA:</span> '.$formulario['deficiencia'.$i].'</p>';
			$contenido.='<p> <span class="def">MEDIDA CORRECTORA:</span> '.$formulario['medida'.$i].'</p>';
		}
$contenido.="</div></page>";
$contenido.="<page footer='page' backtop='40mm'>
	<page_header>
	<table class='cabecera'>
		<tr>
			<td class='tdLogo'>
				<img src='../img/logoInforme.png'>
			</td>
			<td class='tdTitulo'><h1>PROTECCIÓN DE DATOS DE CARÁCTER PERSONAL</h1></td>	
		</tr>
	</table>
	</page_header>
	<div class='container'>
		<h1>7º. CONCLUSIONES.  TRASLADO DE CONCLUSIONES<br/>AL RESPONSABLE DEL TRATAMIENTO</h1>
		<p>D./Dª ".$formularioTrabajo['pregunta604'].", Auditor/a en el presente Informe de Auditoría, manifiesta que los datos, hechos y observaciones en que se basan los dictámenes alcanzados en el mismo han sido corroborados en las distintas sesiones,  considerando necesaria la adopción, por parte del Responsable del Tratamiento, de las medidas correctoras y complementarias propuestas en el presente Informe para la subsanación de las deficiencias detectadas reflejadas en el mismo, por lo que procede al traslado del presente Informe al Responsable del Fichero, a través de su representante legal D. ".$formularioTrabajo['pregunta604'].",  para que adopte las medidas propuestas a la mayor brevedad posible, quedando a disposición de la Agencia Española de Protección de Datos si en algún momento lo requiriese.</p>
		<br/><br/>
		<p>Y expuesto cuanto antecede, se expide el presente Informe -que consta de [[page_nb]] páginas numeradas-  en ".$formularioTrabajo['pregunta604'].", a ".formateaFechaWeb($datos['fechaEmision']).", mostrando su conformidad con el contenido del mismo, mediante su firma, el/la Auditor/a así como el/la representante legal del Responsable del Tratamiento.</p>
	</div>
	<table class='tablaFirma'>
		<tr>
			<td>D. ".$formularioTrabajo['pregunta8']."<br/>Representante legal de<br/>".$formularioTrabajo['pregunta3']."</td>
			<td>Dª ".$formularioTrabajo['pregunta604']."<br/>Auditor/a</td>
		</tr>
	</table>";
$contenido.="</page>";

return $contenido;
}

function obtenerTitulo($i){
	$titulos=array(
		1=>'I. ASPECTOS GENERALES<br/>-RESPECTO A TODOS LOS TRATAMIENTOS-',
		13=>'II. ENCARGADO DE TRATAMIENTO<br/>-RESPECTO A TODOS LOS TRATAMIENTOS-',
		20=>'III. PRESTACIÓN DE SERVICIO SIN ACCESO A DATOS PERSONALES<br/>-RESPECTO A TODOS LOS TRATAMIENTOS-',
		22=>'IV. DELEGACIÓN DE AUTORIZACIONES<br/>- RESPECTO A TODOS LOS TRATAMIENTOS -',
		23=>'V. RÉGIMEN DE TRABAJO FUERA DE LOS LOCALES DE LA UBICACIÓN DE LOS TRATAMIENTOS<br/>- RESPECTO A TODOS LOS TRATAMIENTOS-',
		24=>'VI. FICHEROS TEMPORALES O COPIAS DE TRABAJO DE DOCUMENTOS<br/>- RESPECTO A TODOS LOS  TRATAMIENTOS -',
		26=>'VII. DOCUMENTO DE SEGURIDAD<br/>- RESPECTO A TODOS LOS TRATAMIENTOS-',
		44=>'VIII. FUNCIONES Y OBLIGACIONES DEL PERSONAL<br/>- RESPECTO A TODOS LOS TRATAMIENTOS/FICHEROS-',
		48=>'IX. REGISTRO DE INCIDENCIAS<br/>- RESPECTO A TODOS LOS TRATAMIENTOS -',
		52=>'REGISTRO DE INCIDENCIAS<br/>-TRATAMIENTOS AUTOMATIZADOS-',
		53=>'X. CONTROL DE ACCESO<br/>-RESPECTO A TODOS LOS TRATAMIENTOS-',
		59=>'CONTROL DE ACCESO<br/>-ARCHIVOS AUTOMATIZADOS DE NIVEL ALTO-',
		60=>'CONTROL DE ACCESO<br/>-ARCHIVOS NO AUTOMATIZADOS DE NIVEL ALTO-',
		62=>'XI. GESTIÓN DE SOPORTES Y DOCUMENTOS<br/>-RESPECTO A TODOS LOS FICHEROS-',
		71=>'GESTIÓN DE SOPORTES Y DOCUMENTOS<br/>-RESPECTO A TODOS LOS FICHEROS DE NIVEL ALTO-',
		74=>'GESTIÓN DE SOPORTES Y DOCUMENTOS<br/>-FICHEROS AUTOMATIZADOS DE NIVEL ALTO-',
		78=>'GESTIÓN DE SOPORTES Y DOCUMENTOS<br/>-NO AUTOMATIZADOS/ALTO-',
		81=>'XII. IDENTIFICACIÓN Y AUTENTICACIÓN<br/>-RESPECTO A TODOS LOS FICHEROS AUTOMATIZADOS-',
		87=>'IDENTIFICACIÓN Y AUTENTICACIÓN<br/>-RESPECTO TRATAMIENTOS AUTOMATIZADOS DE NIVEL ALTO-',
		88=>'XIII. COPIAS DE RESPALDO Y RECUPERACIÓN<br/>-RESPECTO TODOS LOS TRATAMIENTOS AUTOMATIZADOS-',
		93=>'COPIAS DE RESPALDO Y RECUPERACIÓN<br/>-AUTOMATIZADOS/ ALTO-',
		95=>'XIV. REGISTRO DE ACCESOS<br/>-RESPECTO LOS FICHEROS AUTOMATIZADOS DE NIVEL ALTO-',
		102=>'REGISTRO DE ACCESOS<br/>-RESPECTO LOS FICHEROS NO AUTOMATIZADOS DE NIVEL ALTO-',
		105=>'XV. ACCESO A DATOS A TRAVÉS DE REDES DE COMUNICACIONES<br/>-RESPECTO A TODOS LOS TRATAMIENTOS AUTOMATIZADOS-',
		106=>'ACCESO A DATOS A TRAVÉS DE REDES DE COMUNICACIONES<br/>-RESPECTOS A LOS FICHEROS AUTOMATIZADOS DE NIVEL ALTO-',
		107=>'XVI. AUDITORÍA<br/>-RESPECTO A TODOS LOS FICHEROS DE NIVEL ALTO-',
		108=>'XVII. CRITERIOS DE ARCHIVO<br/>-RESPECTO A TODOS LOS FICHEROS-',
		109=>'XVIII. ALMACENAMIENTO DE LA INFORMACIÓN<br/>-RESPECTO TODOS LOS ARCHIVOS NO AUTOMATIZADOS-',
		110=>'XIX. CUSTODIA DE SOPORTES<br/>-NO AUTOMATIZADOS/ BÁSICO');
	return $titulos[$i];
}
?>
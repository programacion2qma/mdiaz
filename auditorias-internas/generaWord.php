<?php

    include_once('funciones.php');
    compruebaSesion();

    include_once('../../api/js/firma/signature-to-image.php');
    require_once('../../api/phpword/PHPWord.php');
    require_once('../../api/html2pdf/html2pdf.class.php');

    $PHPWord = new PHPWord();

    $meses = array(
        '01' => 'Enero',
        '02' => 'Febrero',
        '03' => 'Marzo',
        '04' => 'Abril',
        '05' => 'Mayo',
        '06' => 'Junio',
        '07' => 'Julio',
        '08' => 'Agosto',
        '09' => 'Septiembre',
        '10' => 'Octubre',
        '11' => 'Noviembre',
        '12' => 'Diciembre'
    );
    
    $datos = datosRegistro("auditorias_internas", $_GET['codigo']);

    if ($datos['emitida'] == 'NO'){
        $consulta = consultaBD('UPDATE auditorias_internas SET emitida="SI" WHERE codigo='.$datos['codigo'], true);
    }

    $formulario = prepararPreguntas($datos);

    $sql = "SELECT 
                * 
            FROM 
                auditorias_internas_fechas 
            WHERE 
                codigoAuditoria = ".$datos['codigo']." ORDER BY fecha;";
    $fechas = consultaBD($sql, true);

    $cliente = datosRegistro('clientes',$datos['codigoCliente']);

    $sql = "SELECT 
                t.* 
            FROM 
                trabajos t 
            INNER JOIN servicios s ON 
                t.codigoServicio = s.codigo 
            WHERE 
                t.codigoCliente = ".$datos['codigoCliente']."
            AND 
                s.servicio LIKE '%LOPD%' 
            ORDER BY codigo DESC;";
    $trabajo = consultaBD($sql, true, true);

    $sql = "SELECT 
                * 
            FROM 
                auditorias_internas_tratamientos 
            WHERE 
                codigoAuditoria = ".$datos['codigo'].";";
    $ficheros = consultaBD($sql, true);
    
    $formularioTrabajo = recogerFormularioServicios($trabajo);
  
    $cliente      = datosPersonales($cliente, $formularioTrabajo);
    $fechaPartida = explode('-', $datos['fechaEmision']);
    $fechasTexto  = '';
    $sesiones     = 0;
    
    while ($fecha = mysql_fetch_assoc($fechas)) {
        if ($fechasTexto != '') {
            $fechasTexto .= ', ';
        } 
        else {
            $primerDia = formateaFechaWeb($fecha['fecha']);
        }

        $fechasTexto .= formateaFechaWeb($fecha['fecha']);
        
        $sesiones++;
    }

    $alto = 'NO';

    $sql = "SELECT 
                COUNT(codigo) AS total 
            FROM 
                auditorias_internas_tratamientos 
            WHERE 
                (fechaBaja = '0000-00-00' OR 
                 fechaBaja = '' OR 
                 fechaBaja > '".formateaFechaBD($primerDia)."') 
            AND 
                nivel = '3' 
            AND codigoAuditoria = ".$datos['codigo'].";";
    $declaraciones = consultaBD($sql, true, true);
    
    $alto = $declaraciones['total'] > 0 ? 'SI' : 'NO';
        
    if ($alto == 'SI') {
        if (isset($_GET['cuestionario']) && $_GET['cuestionario'] == 'SI') {
            $documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_cuestionario_auditoria.docx');
        } 
        else {
            $documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_auditoria.docx');
        }
    } else {
        if (isset($_GET['cuestionario']) && $_GET['cuestionario'] == 'SI') {
            $documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_cuestionario_auditoria_alto.docx');
        } 
        else {
            $documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_auditoria_alto.docx');
        }
    }

    $fechaEmisionTexto = $fechaPartida[2].' DE '.strtoupper($meses[$fechaPartida[1]]).' DE '.$fechaPartida[0];
    
    $documento->setValue("razonSocial",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
    $documento->setValue("cif",utf8_decode($cliente['cif']));
    $documento->setValue("domicilio",utf8_decode($cliente['domicilio']));
    $documento->setValue("cp",utf8_decode($cliente['cp']));
    $documento->setValue("localidad",utf8_decode($cliente['localidad']));
    $documento->setValue("provincia",utf8_decode($cliente['provincia']));
    $documento->setValue("fechaEmision",utf8_decode($fechaEmisionTexto));
    $documento->setValue("representante",utf8_decode($formularioTrabajo['pregunta8'].' '.$formularioTrabajo['pregunta627'].' '.$formularioTrabajo['pregunta628']));
    
    $dpo='';
    $delegado=consultaBD('SELECT * FROM auditorias_internas_auditores WHERE codigoAuditoria='.$datos['codigo'].' AND delegado="SI";',true,true);
    if($delegado){
        $dpo='Delegado de Protección de Datos : '.$delegado['nombre'].'<w:br/><w:br/>';
        $dpo.='NIF/CIF del Delegado de Protección de Datos : '.$delegado['nif'].'<w:br/><w:br/>';
        $dpo.='Datos de contacto del Delegado de Protección de Datos : '.$formularioTrabajo['pregunta605'].'<w:br/><w:br/>';
    }
    $documento->setValue("dpo",utf8_decode($dpo));

    // cambio en la actividad, incidencia https://qmaconsultores.com/crm-qma-2/mantenimiento-proyectos/gestion.php?codigo=9665
    $actividad = $cliente['actividad'] == 'OTRA' ? $cliente['servicios'] : $cliente['actividad'];
    // $documento->setValue("actividad",utf8_decode($formularioTrabajo['pregunta13']));
    $documento->setValue("actividad", utf8_decode($actividad));

    $auditores='';
    $auditores2='';
    $auditoresListado=array();
    $i=0;
    $listado=consultaBD('SELECT * FROM auditorias_internas_auditores WHERE codigoAuditoria='.$datos['codigo'],true);
    while($item=mysql_fetch_assoc($listado)){
        array_push($auditoresListado,$item);
        $i++;
    }
    foreach ($auditoresListado as $key => $value) {
        if($key>0){
            if($key==(count($auditoresListado)-1)){
                $auditores.=' y ';
                $auditores2.=' y ';
            } else {
                $auditores.=', ';
                $auditores2.=', ';
            }
        }
        $auditores.='D./Dña. '.$value['nombre'].', '.$value['cargo'];
        $auditores2.='D./Dña. '.$value['nombre'];
        if($value['responsable']=='SI'){
            $auditores.=', Responsable de tratamientos';
        }
        if($value['delegado']=='SI'){
            $auditores.=', Delegado de Protección de Datos';
        }
        $auditores.=', con NIF '.$value['nif'];
    }
    $auditores3=str_replace(', ','<w:br/>',$auditores2);
    $auditores3=str_replace(' y ','<w:br/>',$auditores3);
    if($i>1){
        $auditores.=' son designados auditores';
        $auditores2.=', auditores en el presente Informe de Auditoría, manifiestan';
        $auditores3.='<w:br/>Auditores';
    } else {
        $auditores.=' es designado/a auditor/a';
        $auditores2.=', auditor/a en el presente Informe de Auditoría, manifiesta';
        $auditores3.='<w:br/>Auditor/a';
    }
    $documento->setValue("auditores",utf8_decode($auditores));
    $documento->setValue("auditores2",utf8_decode($auditores2));
    $documento->setValue("auditores3",utf8_decode($auditores3));
    $documento->setValue("primerDia",utf8_decode($primerDia));
    $documento->setValue("sesiones",utf8_decode($sesiones));
    $documento->setValue("fechas",utf8_decode($fechasTexto));
    $documento->setValue("true",utf8_decode(''));
    $documento->setValue("fechaEmision2",utf8_decode(formateaFechaWeb($datos['fechaEmision'])));
    $documento->setValue("recursos",sanearCaracteres(utf8_decode($datos['recursos'])));
    $analisis='';
    $informes=consultaBD('SELECT COUNT(informes_clientes.codigo) AS total FROM informes_clientes INNER JOIN usuarios_clientes ON informes_clientes.codigoUsuario=usuarios_clientes.codigoUsuario WHERE usuarios_clientes.codigoCliente='.$cliente['codigo'],true,true);
    if($informes['total']>0){
        $analisis.=', Informe de riesgo';
    }
    $evaluaciones=consultaBD('SELECT COUNT(codigo) AS total FROM evaluaciones_reducidas WHERE codigoCliente='.$cliente['codigo'],true,true);
    if($evaluaciones['total']>0){
        $analisis.=', Evaluación de Impacto';
    } else {
        $evaluaciones=consultaBD('SELECT COUNT(codigo) AS total FROM evaluacion_general WHERE codigoCliente='.$cliente['codigo'],true,true);
        if($evaluaciones['total']>0){
            $analisis.=', Evaluación de Impacto';
        }
    }
    $documento->setValue("analisis",utf8_decode($analisis));
    $tabla='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="536"/><w:gridCol w:w="3336"/><w:gridCol w:w="2658"/><w:gridCol w:w="2190"/></w:tblGrid><w:tr w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w14:paraId="145D726F" w14:textId="77777777" w:rsidTr="000B6ED7"><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="785184BD" w14:textId="77777777" w:rsidR="000F2C50" w:rsidRPr="00031997" w:rsidRDefault="00D64B39" w:rsidP="000B6ED7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00031997"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="18"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="6A5E39BF" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="00031997" w:rsidRDefault="00D64B39" w:rsidP="000B6ED7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00031997"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="18"/></w:rPr><w:t>TRATAMIENTO/FICHERO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="3BDA5497" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="00031997" w:rsidRDefault="00D64B39" w:rsidP="000B6ED7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00031997"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="18"/></w:rPr><w:t>NIVEL DE MEDIDAS DE SEGURIDAD</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="41EBF687" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="00031997" w:rsidRDefault="00D64B39" w:rsidP="000B6ED7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00031997"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:b/><w:sz w:val="18"/></w:rPr><w:t>SISTEMA DE TRATAMIENTO</w:t></w:r></w:p></w:tc></w:tr>';
    $i=0;
    $niveles=array(1=>'Básico',2=>'Medio',3=>'Alto');
    $tratamientos=array(1=>'Automatizado',2=>'Manual',3=>'Mixto');
    while($fichero=mysql_fetch_assoc($ficheros)){
        $i++;
        $ref=$i<10?'0'.$i:$i;
        $tabla.='<w:tr w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w14:paraId="1D73B6DF" w14:textId="77777777" w:rsidTr="000B6ED7"><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="5FFBDA8C" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w:rsidRDefault="003A2435" w:rsidP="00D64B39"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="000B6ED7"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="5BFC1EF9" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w:rsidRDefault="000C4B78" w:rsidP="004D7484"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="000B6ED7"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr><w:t>'.$fichero['nombreFichero'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="723D28CB" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w:rsidRDefault="00DE2AA9" w:rsidP="00DE2AA9"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="000B6ED7"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr><w:t>'.$niveles[$fichero['nivel']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="2F52FAFE" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w:rsidRDefault="00DE2AA9" w:rsidP="00DE2AA9"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="000B6ED7"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr><w:t>'.$tratamientos[$fichero['tratamiento']].'</w:t></w:r></w:p></w:tc></w:tr>';
    }
    if($i==0){
        $tabla.='<w:tr w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w14:paraId="1D73B6DF" w14:textId="77777777" w:rsidTr="000B6ED7"><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="5FFBDA8C" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w:rsidRDefault="003A2435" w:rsidP="00D64B39"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="000B6ED7"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="5BFC1EF9" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w:rsidRDefault="000C4B78" w:rsidP="004D7484"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="000B6ED7"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="723D28CB" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w:rsidRDefault="00DE2AA9" w:rsidP="00DE2AA9"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="000B6ED7"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w14:paraId="2F52FAFE" w14:textId="77777777" w:rsidR="00D64B39" w:rsidRPr="000B6ED7" w:rsidRDefault="00DE2AA9" w:rsidP="00DE2AA9"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="000B6ED7"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cstheme="minorHAnsi"/><w:sz w:val="16"/><w:szCs w:val="20"/></w:rPr><w:t></w:t></w:r></w:p></w:tc></w:tr>';
    }
    $tabla.='</w:tbl>';
    $documento->setValue("tabla",utf8_decode($tabla));

    $auditoria='';
    $titulos=array(1,13,20,22,23,24,26,44,48,52,53,59,60,62,71,74,78,81,87,88,93,95,102,105,106,107,108,109,110);
    for($i=1;$i<=110;$i++){
        if($i==104){
            $i++;
        }
        $i=saltaNivelAlto($i,$alto);
        $pregunta=listadoPreguntas($i,$formulario);
        $pregunta=explode('<input', $pregunta);
        if($i==108){
            $pregunta2=explode('<ul>', $pregunta[1]);
            $pregunta3=explode('</li>', $pregunta[2]);
            $pregunta='<w:p w14:paraId="6E7D43EC" w14:textId="77777777" w:rsidR="00B576F7" w:rsidRPr="000C594F" w:rsidRDefault="000D1698" w:rsidP="000D2A00"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:color w:val="FF0000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r w:rsidRPr="000D1698"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:lastRenderedPageBreak/><w:t>'.$pregunta[0].'</w:t></w:r><w:r w:rsidRPr="000D1698"><w:rPr><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00B576F7"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:color w:val="000000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['otro'.$i]).'</w:t></w:r></w:p>';
            $pregunta2[1]=str_replace('<li>', '', $pregunta2[1]);
            $pregunta.='<w:p w14:paraId="56A3B8AF" w14:textId="77777777" w:rsidR="000D1698" w:rsidRPr="000C594F" w:rsidRDefault="000D1698" w:rsidP="00B576F7"><w:pPr><w:pStyle w:val="ListParagraph"/><w:ind w:left="1080"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:color w:val="FF0000"/><w:sz w:val="12"/><w:szCs w:val="12"/></w:rPr></w:pPr></w:p><w:p w14:paraId="61F0E677" w14:textId="77777777" w:rsidR="000D1698" w:rsidRDefault="000D1698" w:rsidP="000D2A00"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="9"/></w:numPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r w:rsidRPr="000C594F"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.$pregunta2[1].'</w:t></w:r><w:r w:rsidR="00300E12"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:sz w:val="12"/><w:szCs w:val="12"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00B576F7"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:color w:val="000000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['otro'.$i.'_2']).'</w:t></w:r></w:p>';
            $pregunta3[1]=str_replace('<li>', '', $pregunta3[1]);
            $pregunta.='<w:p w14:paraId="71C65BB6" w14:textId="77777777" w:rsidR="00B576F7" w:rsidRPr="00402A5B" w:rsidRDefault="00EA62B9" w:rsidP="000D2A00"><w:pPr><w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="9"/></w:numPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r w:rsidRPr="000C594F"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.$pregunta3[1].'</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00B576F7" w:rsidRPr="005D3DC5"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['pregunta'.$i]).'</w:t></w:r></w:p>';
        } else {
            if(count($pregunta)==1){
                $pregunta=$pregunta[0];
            } else if (count($pregunta)==2){
                $pregunta2=explode('>', $pregunta[1]);
                $j=1;
                if(count($pregunta2)==2){
                    $j=1;
                } else if(count($pregunta2)==3){
                    $j=2;
                }
                $pregunta=$pregunta[0].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:r w:rsidRPr="00055911"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['otro'.$i]).'</w:t></w:r><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="003E027B"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve">'.$pregunta2[$j];
            } else if (count($pregunta)==3){
                $pregunta2=explode('>', $pregunta[1]);
                $pregunta3=explode('>', $pregunta[2]);
                $pregunta=$pregunta[0].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:r w:rsidRPr="00055911"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['otro'.$i]).'</w:t></w:r><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="003E027B"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve">'.$pregunta2[1].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:r w:rsidRPr="00055911"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['otro'.$i.'_2']).'</w:t></w:r><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="003E027B"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve">'.$pregunta3[1];
            }
        }
        if(in_array($i, $titulos)){
            $titulo=obtenerTitulo($i,$alto);
            $titulo=split('<br/>', $titulo);
            // $auditoria.='<w:p w14:paraId="1B90F075" w14:textId="77777777" w:rsidR="00996BA4" w:rsidRDefault="00996BA4" w:rsidP="00996BA4"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p><w:p w14:paraId="22658AB4" w14:textId="77777777" w:rsidR="00996BA4" w:rsidRDefault="00996BA4" w:rsidP="000D2A00"><w:pPr><w:pStyle w:val="ListParagraph"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.$titulo[0].'</w:t></w:r></w:p>';
            $auditoria .= '<w:p w14:paraId="0CAACEFB" w14:textId="2AE4C424" w:rsidR="00EB316E" w:rsidRDefault="00EB316E" w:rsidP="00996BA4"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:ind w:left="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr></w:p><w:p w14:paraId="25455014" w14:textId="3EA50C8E" w:rsidR="00EB316E" w:rsidRDefault="00EB316E" w:rsidP="00996BA4"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:ind w:left="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.$titulo[0].'</w:t></w:r></w:p>';
            // $auditoria.='<w:p w14:paraId="2C86D8D5" w14:textId="77777777" w:rsidR="00996BA4" w:rsidRDefault="00C46CCE" w:rsidP="00996BA4"><w:pPr><w:pStyle w:val="ListParagraph"/><w:ind w:left="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:lastRenderedPageBreak/><w:t xml:space="preserve">'.$titulo[1].'</w:t></w:r></w:p>';
            $auditoria .= '<w:p w14:paraId="3D84EAC7" w14:textId="2227F43B" w:rsidR="00EB316E" w:rsidRDefault="00EB316E" w:rsidP="00996BA4"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:ind w:left="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.$titulo[1].'</w:t></w:r></w:p>';
        }
        if($i==108){
            $auditoria.=$pregunta;
        } else {
            $auditoria.='<w:p w14:paraId="4C393B1F" w14:textId="77777777" w:rsidR="00996BA4" w:rsidRDefault="00996BA4" w:rsidP="00996BA4"><w:pPr><w:pStyle w:val="ListParagraph"/><w:ind w:left="0"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="12"/><w:szCs w:val="12"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4A9DE2A3" w14:textId="77777777" w:rsidR="002652C7" w:rsidRPr="00402A5B" w:rsidRDefault="002652C7" w:rsidP="000D2A00"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r w:rsidRPr="003E027B"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve">'.$pregunta.'</w:t></w:r><w:r w:rsidR="005D3DC5" w:rsidRPr="005D3DC5"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:r><w:r w:rsidR="005D3DC5" w:rsidRPr="005D3DC5"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:br/></w:r><w:r w:rsidR="005D3DC5" w:rsidRPr="00FD68D5"><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['pregunta'.$i]).'</w:t></w:r></w:p>';
        }
        if($i==19){
            $auditoria.='<w:p w14:paraId="7E04A582" w14:textId="77777777" w:rsidR="003F372C" w:rsidRPr="00144E7D" w:rsidRDefault="003F372C" w:rsidP="00034AEE"><w:pPr><w:pStyle w:val="ListParagraph"/><w:ind w:left="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>Art. 28.1 RGPD: "</w:t></w:r><w:r w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>Cuando se vaya a realizar un tratamiento por cuenta de un responsable del tratamiento, este elegirá únicamente un encargado que ofrezca garantías suficientes para aplicar medidas técnicas y organizativas apropiadas, de manera que el tratamiento sea conforme con los requisitos del presente Reglamento y garantice la protección de los derechos del interesado."</w:t></w:r></w:p>';
            $auditoria.='<w:p w14:paraId="134E73A0" w14:textId="77777777" w:rsidR="00DF34E5" w:rsidRPr="00402A5B" w:rsidRDefault="00256632" w:rsidP="00F120D0"><w:pPr><w:pStyle w:val="ListParagraph"/><w:ind w:left="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>Art. 28.3.h): "</w:t></w:r><w:r w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>El tratamiento por el encargado se regirá por un contrato u otro acto jurídico con arreglo al Derecho de la Unión o de los Estados miembros, que vincule al encargado respecto del responsable y establezca el objeto, la duración, la naturaleza y la finalidad del tratamiento, el tipo de datos personales y categorías de interesados, y las obligaciones y derechos del responsable. Dicho contrato o acto jurídico estipulará, en particular, que el encargado:</w:t></w:r><w:r w:rsidR="006A4653" w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> a) ... h)</w:t></w:r><w:r w:rsidR="0022232C" w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="006A4653" w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>pondrá a disposición del responsable toda la información necesaria para demostrar el cumplimiento de las obligaciones establecidas en el presente artículo, así</w:t></w:r><w:r w:rsidR="0022232C" w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> como para permitir y contribuir a la realización de auditorías, incluidas inspecciones, por parte del responsable o de otro auditor autorizado por dicho responsable.</w:t></w:r><w:r w:rsidR="006A4653" w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00144E7D"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>"</w:t></w:r></w:p>';
        }
        $auditoria.='<w:p w14:paraId="43EE6804" w14:textId="77777777" w:rsidR="00332D50" w:rsidRPr="00CF26A8" w:rsidRDefault="003523DF" w:rsidP="003523DF"><w:pPr><w:pStyle w:val="ListParagraph"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:color w:val="FF0000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r w:rsidRPr="00FA6577"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/><w:u w:val="single"/></w:rPr><w:t>DEFICIENCIA</w:t></w:r><w:r w:rsidRPr="00FA6577"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidRPr="00B44986"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00CF26A8"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:color w:val="000000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['deficiencia'.$i]).'</w:t></w:r></w:p>';
        $auditoria.='<w:p w14:paraId="2C194316" w14:textId="77777777" w:rsidR="00332D50" w:rsidRDefault="00332D50" w:rsidP="003523DF"><w:pPr><w:pStyle w:val="ListParagraph"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/><w:u w:val="single"/></w:rPr></w:pPr></w:p><w:p w14:paraId="602F7077" w14:textId="77777777" w:rsidR="003523DF" w:rsidRPr="00855BE0" w:rsidRDefault="003523DF" w:rsidP="003523DF"><w:pPr><w:pStyle w:val="ListParagraph"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr></w:pPr><w:r w:rsidRPr="00FA6577"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/><w:u w:val="single"/></w:rPr><w:t>MEDIDA CORRECTORA</w:t></w:r><w:r w:rsidRPr="00FA6577"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>:</w:t></w:r><w:r w:rsidR="00CF26A8"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve">  </w:t></w:r><w:r w:rsidR="00CF26A8"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:b/><w:i/><w:color w:val="9BBB59" w:themeColor="accent3"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00CF26A8" w:rsidRPr="00CF26A8"><w:rPr><w:rFonts w:ascii="Verdana" w:hAnsi="Verdana"/><w:i/><w:color w:val="000000"/><w:sz w:val="24"/><w:szCs w:val="24"/></w:rPr><w:t>'.sanearCaracteres($formulario['medida'.$i]).'</w:t></w:r></w:p>';
    }

    $documento->setValue("auditoria",utf8_decode($auditoria));

    reemplazarLogoDerechos($documento, $cliente);

    $documento->save('../documentos/consultorias/Auditoria_interna.docx');

    header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header("Content-Disposition: attachment; filename=Auditoria_interna.docx");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/Auditoria_interna.docx');

function saltaNivelAlto($i,$alto){
    $saltos=array(59=>62,71=>81,87=>88,93=>105,106=>108);
    if($alto=='NO' && array_key_exists($i,$saltos)){
        $i=$saltos[$i];
    }
    return $i;
}
function obtenerTitulo($i,$alto){
    $titulos=array(
        1=>'I. ASPECTOS GENERALES<br/>-RESPECTO A TODOS LOS TRATAMIENTOS-',
        13=>'II. ENCARGADO DE TRATAMIENTO<br/>-RESPECTO A TODOS LOS TRATAMIENTOS-',
        20=>'III. PRESTACIÓN DE SERVICIO SIN ACCESO A DATOS PERSONALES<br/>-RESPECTO A TODOS LOS TRATAMIENTOS-',
        22=>'IV. DELEGACIÓN DE AUTORIZACIONES<br/>- RESPECTO A TODOS LOS TRATAMIENTOS -',
        23=>'V. RÉGIMEN DE TRABAJO FUERA DE LOS LOCALES DE LA UBICACIÓN DE LOS TRATAMIENTOS<br/>- RESPECTO A TODOS LOS TRATAMIENTOS-',
        24=>'VI. FICHEROS TEMPORALES O COPIAS DE TRABAJO DE DOCUMENTOS<br/>- RESPECTO A TODOS LOS  TRATAMIENTOS -',
        26=>'VII. DOCUMENTO DE SEGURIDAD<br/>- RESPECTO A TODOS LOS TRATAMIENTOS-',
        44=>'VIII. FUNCIONES Y OBLIGACIONES DEL PERSONAL<br/>- RESPECTO A TODOS LOS TRATAMIENTOS/FICHEROS-',
        48=>'IX. REGISTRO DE INCIDENCIAS<br/>- RESPECTO A TODOS LOS TRATAMIENTOS -',
        52=>'REGISTRO DE INCIDENCIAS<br/>-TRATAMIENTOS AUTOMATIZADOS-',
        53=>'X. CONTROL DE ACCESO<br/>-RESPECTO A TODOS LOS TRATAMIENTOS-',
        59=>'CONTROL DE ACCESO<br/>-ARCHIVOS AUTOMATIZADOS DE NIVEL ALTO-',
        60=>'CONTROL DE ACCESO<br/>-ARCHIVOS NO AUTOMATIZADOS DE NIVEL ALTO-',
        62=>'XI. GESTIÓN DE SOPORTES Y DOCUMENTOS<br/>-RESPECTO A TODOS LOS FICHEROS-',
        71=>'GESTIÓN DE SOPORTES Y DOCUMENTOS<br/>-RESPECTO A TODOS LOS FICHEROS DE NIVEL ALTO-',
        74=>'GESTIÓN DE SOPORTES Y DOCUMENTOS<br/>-FICHEROS AUTOMATIZADOS DE NIVEL ALTO-',
        78=>'GESTIÓN DE SOPORTES Y DOCUMENTOS<br/>-NO AUTOMATIZADOS/ALTO-',
        81=>'XII. IDENTIFICACIÓN Y AUTENTICACIÓN<br/>-RESPECTO A TODOS LOS FICHEROS AUTOMATIZADOS-',
        87=>'IDENTIFICACIÓN Y AUTENTICACIÓN<br/>-RESPECTO TRATAMIENTOS AUTOMATIZADOS DE NIVEL ALTO-',
        88=>'XIII. COPIAS DE RESPALDO Y RECUPERACIÓN<br/>-RESPECTO TODOS LOS TRATAMIENTOS AUTOMATIZADOS-',
        93=>'COPIAS DE RESPALDO Y RECUPERACIÓN<br/>-AUTOMATIZADOS/ ALTO-',
        95=>'XIV. REGISTRO DE ACCESOS<br/>-RESPECTO LOS FICHEROS AUTOMATIZADOS DE NIVEL ALTO-',
        102=>'REGISTRO DE ACCESOS<br/>-RESPECTO LOS FICHEROS NO AUTOMATIZADOS DE NIVEL ALTO-',
        105=>'XV. ACCESO A DATOS A TRAVÉS DE REDES DE COMUNICACIONES<br/>-RESPECTO A TODOS LOS TRATAMIENTOS AUTOMATIZADOS-',
        106=>'ACCESO A DATOS A TRAVÉS DE REDES DE COMUNICACIONES<br/>-RESPECTOS A LOS FICHEROS AUTOMATIZADOS DE NIVEL ALTO-',
        107=>'XVI. AUDITORÍA<br/>-RESPECTO A TODOS LOS FICHEROS DE NIVEL ALTO-',
        108=>'XVII. CRITERIOS DE ARCHIVO<br/>-RESPECTO A TODOS LOS FICHEROS-',
        109=>'XVIII. ALMACENAMIENTO DE LA INFORMACIÓN<br/>-RESPECTO TODOS LOS ARCHIVOS NO AUTOMATIZADOS-',
        110=>'XIX. CUSTODIA DE SOPORTES<br/>-NO AUTOMATIZADOS/ BÁSICO');
    //echo 'Alto '.$alto;
    if($alto=='NO'){
        $titulos[105]='XIV. ACCESO A DATOS A TRAVÉS DE REDES DE COMUNICACIONES<br/>-RESPECTO A TODOS LOS TRATAMIENTOS AUTOMATIZADOS-';
        $titulos[108]='XV. CRITERIOS DE ARCHIVO<br/>-RESPECTO A TODOS LOS FICHEROS-';
        $titulos[109]='XVI. ALMACENAMIENTO DE LA INFORMACIÓN<br/>-RESPECTO TODOS LOS ARCHIVOS NO AUTOMATIZADOS-';
        $titulos[110]='XVII. CUSTODIA DE SOPORTES<br/>-NO AUTOMATIZADOS/ BÁSICO';
    }
    return $titulos[$i];
}
?>
<?php
  $seccionActiva = 58;
  include_once("../cabecera.php");
  gestionAuditorias();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript" src="../../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.es-ES.js"></script>


<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
		$(this).datepicker('hide');
		if($('#codigo').length==0){
			obtenerTratamientosAjax();
			$('.datepicker').attr('autocomplete', 'off');
		}
		revisarNivelAlto();
	});
	$('#insertaFilaTabla').click(function(){
		insertaFila('tablaFechas');
	});

	$('textarea').attr('maxlength',1000);

	$('#eliminaFilaTabla').click(function(){
		eliminaFila('tablaFechas');
	});

	$('#insertaFilaTablaAuditores').click(function(){
		insertaFila('tablaAuditores');
	});

	$('#eliminaFilaTablaAuditores').click(function(){
		eliminaFila('tablaAuditores');
	});

	marcarPestania();
	$('.nav-tabs li a').click(function(){
		var pestana=$(this).attr('href').replace('#','');
		$('#pestanaActiva').val(pestana);
	});

	$('#tablaTratamientos').on('change','select.selectNivel',function(){
		revisarNivelAlto();
		$('.datepicker').attr('autocomplete', 'off');
	});

	revisarNivelAlto();
	$('#codigoCliente').change(function(){
		obtenerTratamientosAjax();
		$('.datepicker').attr('autocomplete', 'off');
	});

	$('.datepicker').attr('autocomplete', 'off');

	$('.datepicker').click(function(){		
		$(this).attr('autocomplete', 'off');
	});

});

function marcarPestania(){
  	var pagina=$('#pestanaActiva').val();;
  	$('.nav-tabs li').removeClass('active');
  	$('.nav-tabs li:nth-child('+pagina+')').addClass('active');
}

function obtenerTratamientosAjax(){
	var fecha=$('#fecha0').val();
	var codigoCliente=$('#codigoCliente').val();
	var codigoAuditoria=0;
	var finalizada=$('#finalizada').val();
	if(finalizada=='SI'){
		codigoAuditoria=$('#codigo').val();
	}
	if(fecha!='' && codigoCliente!='NULL'){
		var consulta=$.post('../listadoAjax.php?include=auditorias-internas&funcion=obtenerTratamientosAjax();',{'codigoCliente':codigoCliente,'fecha':fecha,'finalizada':finalizada,'codigoAuditoria':codigoAuditoria},function(respuesta){
			$('#tablaTratamientos tbody').html(respuesta);
			$('#tablaTratamientos tbody .hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
			revisarNivelAlto();
		});
	}	
}

function revisarNivelAlto(){
	var res='NO';
	$('select.selectNivel').each(function(){
		if($(this).val()==3){
			var i=obtieneFilaCampo($(this));
			var fechaBaja=$('#fechaBaja'+i).val();
			if(fechaBaja==''){
				res='SI';
			} else {
				var fechaHoy=new Date();
				fechaBaja=fechaBaja.split('/');
				fechaBaja=new Date(fechaBaja[2], fechaBaja[1]-1, fechaBaja[0]);
				if(fechaBaja>fechaHoy){
					res='SI';
				} 
			}
		}
	});
	if(res=='NO'){
		$('tr.classAlto').addClass('hide');
		$('.nav.nav-tabs li:nth-child(15)').addClass('hide');
		$('.nav.nav-tabs li:nth-child(17)').addClass('hide');
		$('.nav.nav-tabs li:nth-child(16) a').html('XIV. Acceso a datos a traves de redes de comunicaciones');
		$('.nav.nav-tabs li:nth-child(18) a').html('XV. Criterios de archivo');
		$('.nav.nav-tabs li:nth-child(19) a').html('XVI. Almacenamiento de la información');
		$('.nav.nav-tabs li:nth-child(20) a').html('XVII. Custodia de soportes');
	} else {
		$('tr.classAlto').removeClass('hide');
		$('.nav.nav-tabs li:nth-child(15)').removeClass('hide');
		$('.nav.nav-tabs li:nth-child(17)').removeClass('hide');
	}
}

//Fin parte de complementos
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
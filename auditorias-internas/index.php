<?php
  $seccionActiva = 58;
  include_once('../cabecera.php');
  $finalizada='NO';
  $texto='Auditorías internas en proceso';
  if(isset($_GET['finalizada'])){
    $finalizada=$_GET['finalizada'];
    if($finalizada=='SI'){
      $texto='Auditorías internas finalizadas';
    }
  }
  operacionesAuditorias();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <?php
        creaBotonFinalizadas($finalizada);
        creaBotonesGestion();
      ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3><?php echo $texto; ?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                //filtroVentasServicios();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaAuditorias">
                <thead>
                  <tr>
                    <th> Fechas auditoría </th>
                    <th> Cliente </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    var finalizada='<?php echo $finalizada; ?>';
    listadoTabla('#tablaAuditorias','../listadoAjax.php?include=auditorias-internas/&funcion=listadoAuditorias("'+finalizada+'");');
    /*$('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaVentasServicios');*/

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
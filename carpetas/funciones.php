<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de gestor de documentos

function obtieneDatosCarpeta(){
	if(isset($_GET['codigoCarpeta'])){
		global $_CONFIG;

		$datos=consultaBD("SELECT nombre FROM carpetas WHERE codigo='".$_GET['codigoCarpeta']."';",true,true);
		$_SESSION['codigoCarpeta']=$_GET['codigoCarpeta'];
		$_SESSION['nombreCarpeta']="<a href='".$_CONFIG['raiz']."gestor-documentos/gestion.php?codigo=".$_GET['codigoCarpeta']."'>".$datos['nombre']."</a>";
	}

	$res=array('codigo'=>$_SESSION['codigoCarpeta'],'nombre'=>$_SESSION['nombreCarpeta']);

	return $res;
}


function operacionesDocumentos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDocumento();
	}
	elseif(isset($_POST['nombre'])){
		$res=creaDocumento();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatosConFicheroAPI('documentos','fichero','../documentos/gestor-documentos/');
	}

	mensajeResultado('nombre',$res,'Documento');
    mensajeResultado('elimina',$res,'Documento', true);
}


function creaDocumento(){
	$res=insertaDatos('documentos',time(),'../documentos/gestor-documentos/');

	if($res){
		$codigoDocumento=$res;

		conexionBD();		
		$res=asignaDocumentoCarpetas($codigoDocumento);
		$res=$res && asignaEtiquetasDocumento($codigoDocumento);
		cierraBD();
	}

	return $res;
}

function actualizaDocumento(){
	$res=actualizaDatos('documentos',time(),'../documentos/gestor-documentos/');

	if($res){
		conexionBD();
		$res=asignaDocumentoCarpetas($_POST['codigo'],true);
		$res=$res && asignaEtiquetasDocumento($_POST['codigo']);
		cierraBD();
	}

	return $res;
}


function asignaDocumentoCarpetas($codigoDocumento,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	if($actualizacion){
		$res=consultaBD("DELETE FROM documentos_carpeta WHERE codigoDocumento='$codigoDocumento';");
	}

	$seleccionadas=0;

	for($i=0;$i<$datos['numeroCarpetas'];$i++){
		if(isset($datos['codigoCarpeta'.$i])){
			$res=$res && consultaBD("INSERT INTO documentos_carpeta VALUES(NULL,".$datos['codigoCarpeta'.$i].",$codigoDocumento);");
			$seleccionadas++;
		}
	}

	$res=$res && compruebaCarpetasDocumento($seleccionadas,$codigoDocumento,$datos['primeraCarpeta']);

	return $res;
}

function compruebaCarpetasDocumento($seleccionadas,$codigoDocumento,$codigoCarpeta){
	$res=true;

	if($seleccionadas==0){
		$res=consultaBD("INSERT INTO documentos_carpeta VALUES(NULL,$codigoCarpeta,$codigoDocumento);");
	}

	return $res;
}

function asignaEtiquetasDocumento($codigoDocumento){
	$res=true;

	if(isset($_POST['eliminaEtiqueta'])){
		$eliminaEtiquetas=$_POST['eliminaEtiqueta'];
		conexionBD();
		for($i=0;$i<count($eliminaEtiquetas);$i++){
			$res=$res && consultaBD("DELETE FROM etiquetas_documentos WHERE codigoDocumento=".$codigoDocumento." AND codigoEtiqueta=".$eliminaEtiquetas[$i]);
		}
		cierraBD(); 
	}

	if(isset($_POST['nuevasEtiquetas'])){
		$nuevasEtiquetas=$_POST['nuevasEtiquetas']; 
		$codigosEtiquetas=array();

		if(isset($_POST['codigoEtiquetas'])){
			$codigosEtiquetas=$_POST['codigoEtiquetas'];
		}

		conexionBD();
		for($i=0;$i<count($nuevasEtiquetas);$i++){
			if(!in_array($nuevasEtiquetas[$i],$codigosEtiquetas)){
				$res=$res && consultaBD("INSERT INTO etiquetas_documentos VALUES(NULL,$codigoDocumento,".$nuevasEtiquetas[$i].");");
			}
		}
		cierraBD();
	}

	return $res;
}



function listadoDocumentos(){
	$carpeta=obtieneDatosCarpeta();
	$extensiones=array('zip, rar, 7z'=>'icon-file-archive-o','mp3, 3gp, midi, aac, wav, wma'=>'icon-file-audio-o','php, html, css, js, java, c, cpp, py, xml, xhtml, htm'=>'icon-file-code-o','xls, xlsx, ods, ots'=>'icon-file-excel-o','png, jpg, jpeg, gif, bmp, psd, ai'=>'icon-file-image-o','mp4, mkv, avi, wmm, flv, mov'=>'icon-file-movie-o','pdf'=>'icon-file-pdf-o','odp, ppt, pptx'=>'icon-file-powerpoint-o','doc, docx, odt, ott'=>'icon-file-word-o');



	$columnas=array('documentos.codigo','nombre','fecha','fechaModificacion','codigoEtiqueta','fichero','fecha','fechaModificacion');
	$where=obtieneWhereListado('WHERE codigoCarpeta='.$carpeta['codigo'],$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT documentos.* FROM documentos LEFT JOIN documentos_carpeta ON documentos.codigo=documentos_carpeta.codigoDocumento LEFT JOIN etiquetas_documentos ON documentos.codigo=etiquetas_documentos.codigoDocumento";
	
	conexionBD();
	$consulta=consultaBD($query." $where GROUP BY documentos.codigo $orden $limite;");
	$consultaPaginacion=consultaBD($query." $where GROUP BY documentos.codigo");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	
	if(compruebaUsuarioAdministrador()){
		$res=imprimeListadoDocumentosAdministrador($res,$consulta,$extensiones);
	}
	else{
		$res=imprimeListadoDocumentosEmpleado($res,$consulta,$extensiones);
	}


	echo json_encode($res);
}

function imprimeListadoDocumentosAdministrador($res,$consulta,$extensiones){
	while($datos=mysql_fetch_assoc($consulta)){
		$icono=obtieneIconoDocumento($extensiones,$datos['fichero']);
		$etiquetas=obtieneEtiquetasDocumento($datos['codigo']);

		$fila=array(
			$datos['codigo'],
			"<a href='gestion.php?codigo=".$datos['codigo']."'><i class='".$icono."'></i> ".$datos['nombre']."</a>",
			formateaFechaWeb($datos['fecha']),
			formateaFechaWeb($datos['fechaModificacion']),
			$etiquetas,
			botonAcciones(array('Detalles','Descargar'),array('carpetas/gestion.php?codigo='.$datos['codigo'],'documentos/gestor-documentos/'.$datos['fichero']),array('icon-search-plus','icon-cloud-download'),array(0,1),false,'Acciones'),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	return $res;
}

function imprimeListadoDocumentosEmpleado($res,$consulta,$extensiones){
	global $_CONFIG;
	
	while($datos=mysql_fetch_assoc($consulta)){
		$icono=obtieneIconoDocumento($extensiones,$datos['fichero']);
		$etiquetas=obtieneEtiquetasDocumento($datos['codigo']);

		$fila=array(
			$datos['codigo'],
			"<a class='noAjax' href='".$_CONFIG['raiz']."documentos/gestor-documentos/".$datos['fichero']."' target='_blank'><i class='".$icono."'></i> ".$datos['nombre']."</a>",
			formateaFechaWeb($datos['fecha']),
			formateaFechaWeb($datos['fechaModificacion']),
			$etiquetas,
			"<div class='centro'><a class='btn btn-propio noAjax' href='".$_CONFIG['raiz']."documentos/gestor-documentos/".$datos['fichero']."' target='_blank'><i class='icon-cloud-download'></i> Descargar</a></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	return $res;
}


function obtieneIconoDocumento($extensiones,$nombre){
	$res='icon-file-text-o';
	$extensionFichero=explode('.',$nombre);
	$extensionFichero=end($extensionFichero);

	foreach($extensiones as $extension => $icono) {
		if(substr_count($extension,$extensionFichero)==1){
			$res=$icono;
		}
	}

	return $res;
}

function obtieneEtiquetasDocumento($codigoDocumento){
	$res="";

	$consulta=consultaBD("SELECT nombre, color FROM etiquetas INNER JOIN etiquetas_documentos ON etiquetas.codigo=etiquetas_documentos.codigoEtiqueta WHERE codigoDocumento='$codigoDocumento'");
	while($datosE=mysql_fetch_assoc($consulta)){
		$res.="<span class='label' style='background-color:".$datosE['color'].";'><i class='icon-tag'></i> ".$datosE['nombre']."</span> ";
	}

	return $res;
}

function gestionDocumento(){
	operacionesDocumentos();

	abreVentanaGestion('Gestión de documentos','?','span5','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('documentos');

	campoID($datos);
	campoTexto('nombre','Nombre fichero',$datos,'span4 obligatorio');
	campoFecha('fecha','Fecha de subida',$datos);
	campoFichero('fichero','Archivo',0,$datos,'../documentos/gestor-documentos/','Descargar');

	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoCarpetas($datos);
	campoEtiquetas($datos);
	areaTexto('observaciones','Observaciones',$datos);
	campoOculto(fecha(),'fechaModificacion');

	cierraVentanaGestion('index.php',true);

	creaVentanaEtiquetas();
}


function filtroDocumentos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(1,'Nombre');
	campoSelectEtiquetas(4);

	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoFecha(2,'Fecha desde');
	campoFecha(6,'Hasta');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function creaBotonesGestionDocumentos(){
	if(compruebaUsuarioAdministrador()){
	    echo '
	    <a class="btn-floating btn-large btn-default btn-creacion2" href="../gestor-documentos/" title="Volver a carpetas"><i class="icon-chevron-left"></i></a>
	    <a class="btn-floating btn-large btn-success btn-creacion" href="gestion.php" title="Nuevo registro"><i class="icon-plus"></i></a>
	    <a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
	}
	else{
		echo '
		<a class="btn-floating btn-large btn-default btn-eliminacion" href="../gestor-documentos/" title="Volver a carpetas"><i class="icon-chevron-left"></i></a>';
	}
}



function campoCarpetas($datos){
	$carpeta=obtieneDatosCarpeta();

	$valores=array();
	$valoresCampos=array();
	$textosCampos=array();

	conexionBD();

	$consulta=consultaBD("SELECT codigo, nombre FROM carpetas ORDER BY nombre;");
	while($datosC=mysql_fetch_assoc($consulta)){
		array_push($valoresCampos,$datosC['codigo']);
		array_push($textosCampos,$datosC['nombre']);
	}

	if(!isset($datos['codigo'])){
		array_push($valores,$carpeta['codigo']);
	}
	else{
		$valores=consultaArray("SELECT codigoCarpeta FROM documentos_carpeta WHERE codigoDocumento='".$datos['codigo']."';");
	}

	cierraBD();

	campoCheckCarpetas('codigoCarpeta','Carpeta/s',$valores,$textosCampos,$valoresCampos);
}


function campoCheckCarpetas($nombreCampo,$texto, $valor=array(), $textosCampos=array(),$valoresCampos=array(),$salto=true){
	$nombre=$nombreCampo;
	$primeraCarpeta=$valoresCampos[0];//Para asignarla por defecto en caso de que al usuario se le vaya la pinza y no elija ninguna carpeta para el documento

	echo "
	<div class='control-group'>                     
      <label class='control-label'>$texto:</label>
      <div class='controls'>";

    for($i=0;$i<count($textosCampos);$i++){
    	if(!is_array($nombreCampo)){
    		$nombre=$nombreCampo.$i;
    	}

    	echo "<label class='checkbox inline'>
    			<input type='checkbox' name='$nombre' value='".$valoresCampos[$i]."'";
    	if(is_array($valor) && in_array($valoresCampos[$i],$valor)){
    		echo " checked='checked'";
    	} 
    	echo ">".$textosCampos[$i]."</label>";
    	if($salto){
    		echo "<br />";
    	}
    }
    
    campoOculto($i,'numeroCarpetas');
    campoOculto($primeraCarpeta,'primeraCarpeta');

    echo "
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}

function campoEtiquetas($datos){
  echo "
  <div class='control-group'>                     
      <label class='control-label'>Etiquetas:</label>
      <div class='controls datoSinInput' id='campoEtiquetas'>";
      
    if(isset($datos['codigo'])){
      $consulta=consultaBD("SELECT etiquetas.codigo, nombre, color FROM etiquetas INNER JOIN etiquetas_documentos ON etiquetas.codigo=etiquetas_documentos.codigoEtiqueta WHERE codigoDocumento='".$datos['codigo']."'",true);
      while($datosE=mysql_fetch_assoc($consulta)){
        campoOculto($datosE['codigo'],'codigoEtiquetas[]','','hide etiqueta'.$datosE['codigo']);
        echo "<div class='cajaEtiqueta".$datosE['codigo']."'>";
        echo "<span class='label' style='background-color:".$datosE['color'].";'><i class='icon-tag'></i> ".$datosE['nombre']."</span><a href='#' class='eliminaEtiqueta noAjax' etiqueta='".$datosE['codigo']."'><i class='icon-times'></i></a><br/> ";
        echo "</div>";
      }
    }
    
    echo "
        <br />
        <button type='button' class='btn btn-default btn-small' id='insertarEtiqueta'><i class='icon-plus'></i> Añadir etiqueta</button> 
      </div> <!-- /controls -->       
    </div> <!-- /control-group -->";
}


function creaVentanaEtiquetas(){
	abreVentanaModal('etiquetas');
	campoSelectEtiquetas();
	cierraVentanaModal();
}

function campoSelectEtiquetas($nombreCampo='etiqueta'){
	echo "<div class='control-group'>                     
		      <label class='control-label' for='$nombreCampo'>Etiqueta:</label>
		      <div class='controls'>
        		<select name='$nombreCampo' id='$nombreCampo' class='selectpicker span4'>
        			<option value='NULL' data-content='&nbsp;'></option>";

		    $consulta=consultaBD("SELECT * FROM etiquetas ORDER BY nombre;",true);
		    while($datos=mysql_fetch_assoc($consulta)){
				echo "<option value='".$datos['codigo']."' data-content='<span class=\"label\" style=\"background-color:".$datos['color'].";\"><i class=\"icon-tag\"></i> ".$datos['nombre']."</span> '></option>";
		    }
	
	echo "     	</select>
      		</div> <!-- /controls -->       
    	</div> <!-- /control-group -->";
}
//Fin parte de gestor de documentos
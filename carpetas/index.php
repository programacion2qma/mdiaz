<?php
  $seccionActiva=46;
  include_once('../cabecera.php');
  
  $carpeta=obtieneDatosCarpeta();
  operacionesDocumentos();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <?php
        creaBotonesGestionDocumentos();
      ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Documentos subidos en la carpeta <?php echo $carpeta['nombre']; ?></h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroDocumentos();
              ?>
              <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaDocumentos">
                <thead>
                  <tr>
                    <th> ID </th>
                    <th> Nombre </th>
                    <th> Fecha </th>
                    <th> Última modificación </th>
                    <th> Etiquetas </th>
                    <th class="centro"></th>
                    <?php celdaCabeceraAdministrador("<div class='centro'><input type='checkbox' id='todo'></div>"); ?>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaDocumentos','../listadoAjax.php?include=carpetas&funcion=listadoDocumentos();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaDocumentos');

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
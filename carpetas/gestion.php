<?php
  $seccionActiva=46;
  include_once("../cabecera.php");
  gestionDocumento();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	//Oyentes gestión etiquetas
	$('#insertarEtiqueta').click(function(){
		$('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	$('.eliminaEtiqueta').click(function(){
	    oyenteEliminar($(this));
	});

	$('#enviar').click(function(){//Botón "Guardar" del modal de las etiquetas
		var codigoEtiqueta=$('#etiqueta').val();
		var etiqueta=$('#etiqueta option:selected').attr('data-content');
		etiqueta="<div class='cajaEtiqueta"+codigoEtiqueta+"'>"+etiqueta+"<a href='#' class='eliminaEtiqueta noAjax' etiqueta='"+codigoEtiqueta+"'><i class='icon-times'></i></a></div>";

		$('#cajaGestion').modal('hide');
		
		$('#campoEtiquetas').prepend(etiqueta);
		var campoOculto="<input type='hidden' class='hide' name='nuevasEtiquetas[]' value='"+codigoEtiqueta+"' />";
		$('#campoEtiquetas').prepend(campoOculto);

		$('.eliminaEtiqueta').click(function(){
			oyenteEliminar($(this));
		});
	});
	//Fin oyentes gestión etiquetas
});


function oyenteEliminar(elem){
	var codigoEtiqueta=elem.attr('etiqueta');
	var name='etiqueta'+elem.attr('etiqueta');

	if($('.'+name).length){
		$('.'+name).attr('id','eliminaEtiqueta[]');
		$('.'+name).attr('name','eliminaEtiqueta[]');
	}
	else if($('input[name="nuevasEtiquetas[]"][value="'+codigoEtiqueta+'"]').length){
		$('input[name="nuevasEtiquetas[]"][value="'+codigoEtiqueta+'"]').remove();
	}
	$('.cajaEtiqueta'+codigoEtiqueta).remove();
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
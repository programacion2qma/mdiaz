<?php

@include_once('funciones.php');
compruebaSesion();

$contenido=generaCertificado();
$nombre='Certificado.pdf';

require_once('../../api/html2pdf/html2pdf.class.php');
$html2pdf=new HTML2PDF('L','A4','es');
$html2pdf->pdf->SetDisplayMode('fullpage');
$html2pdf->WriteHTML($contenido);
$html2pdf->Output($nombre);


function generaCertificado(){

conexionBD();
    	$datos = datosRegistro("formacionPBC",$_GET['codigo']);
    	$cliente = consultaBD('SELECT clientes.* FROM clientes INNER JOIN usuarios_clientes ON clientes.codigo=usuarios_clientes.codigoCliente WHERE codigoUsuario='.$_SESSION['codigoU'],true,true);
cierraBD();
$fecha=explode('-', $datos['fecha']);
$meses=array('','01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	            
	        }

	        #container{
	        	width:100%;
	        	height:99%;
	        	border:2px solid #000;
	        }

	        #content{
	        	margin-top:20px;
	        	margin-left:20px;
	        	width:96%;
	        	height:93%;
	        	border:3px solid #000;
	        }

	        .logo{
	        	text-align:right;
	        	margin-top:30px;
	        	margin-right:30px;
	        }

	        h1{
	        	text-align:center;
	        	margin-top:10px;
	        	font-size:46px;
	        }

	        p{
	        	text-align:center;
	        	font-size:20px;
	        }

	        table{
	        	width:90%;
	        	margin-left:50px;
	        	margin-top:20px;
	        }

	        .a30{
	        	width:30%;
	        }

	        .a40{
	        	width:40%;
	        	text-align:center;
	        }

	        .firma td{
	        	height:60px;
	        }
	-->
	</style>
	<page backtop='10mm' backleft='10mm' backright='10mm' backbottom='10mm'>
	<div id='container'>
		<div id='content'>
			<div class='logo'>
				<img src='../img/logoInforme.png' />
			</div>
			<h1>DIPLOMA</h1>
			<p> <b>OTORGADO A<br/>
			D./Dña. ".$datos['alumno']."</b><br/><br/>
			Con DNI ".$datos['dni']." ha realizado con éxito el curso de formación<br/>
			DE LA LEY 10/2010 PREVENCION DE BLANQUEO DE CAPITALES<br/>
			con una duración de 60 horas en la modalidad de teleformación
			</p>
			<table>
				<tr>
					<td class='a30'></td>
					<td class='a40'>Antequera ".$fecha[2]." de ".$meses[$fecha[1]]." de ".$fecha[0]."</td>
					<td class='a30'></td>
				</tr>
				<tr>
					<td class='a30'>Empresa donde presta sus servicios</td>
					<td class='a40'></td>
					<td class='a30'>Empresa formadora</td>
				</tr>
				<tr>
					<td class='a30'>".$cliente['razonSocial']."</td>
					<td class='a40'></td>
					<td class='a30'>Muñoz y Díaz Asesores S.L.U. </td>
				</tr>
				<tr>
					<td class='a30'>Firma</td>
					<td class='a40'></td>
					<td class='a30'>Firma</td>
				</tr>
				<tr class='firma'>
					<td class='a30'></td>
					<td class='a40'></td>
					<td class='a30'><img style='width:200px' src='../img/firmaMariaMar.png' /></td>
				</tr>
				<tr>
					<td class='a30'>D. / Dña. ".$cliente['administrador']." </td>
					<td class='a40'></td>
					<td class='a30'>Dña. María del Mar Jiménez Aguado</td>
				</tr>
				<tr>
					<td class='a30'>Representante legal</td>
					<td class='a40'></td>
					<td class='a30'>Directora de formación</td>
				</tr>
			</table>
		</div>
	</div>
	</page>";

return $contenido;
}
?>
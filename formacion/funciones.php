<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios
function operacionesExamen(){
	if(isset($_POST['codigo'])){
		$res=actualizaExamen();
	} else if (isset($_POST['codigoUsuario'])){
		$res=insertaExamen();
	}
}

function insertaExamen(){
	$res=true;
	preparaCuestionario();
	$res=insertaDatos('formacionPBC');
	return $res;
}

function actualizaExamen(){
	$res=true;
	preparaCuestionario();
	$res=actualizaDatos('formacionPBC');
	return $res;
}

function preparaCuestionario(){
	$query='';
	for($i=1;$i<=20;$i++){
		if($i>1){
			$query.="&{}&";
		}
		$query.="pregunta$i=>".$_POST['pregunta'.$i];
	}
	$_POST['cuestionario']=$query;
}

function imprimeTemas(){
	$temas=array(1=>'Contexto de la normativa en prevención del blanqueo de capitales y de la financiacion del terrorismo',2=>'Ámbito de aplicación de la ley 10/2010, de 28 de Abril, de prevención del blanqueo de capitales y de la financiación del terrorismo',3=>'Abogados, asesores fiscales, contables y otro profesionales como sujeto obligatorio de PBLC',4=>'Obligaciones de diligencia debida',5=>'Obligaciones de información',6=>'Medidas de control interno');
	foreach ($temas as $key => $value) {
		echo "<tr>
				<td> ".$key." </td>
				<td> ".$value." </td>
				<td class='centro'>
					<a target='_blank' href='generaTema.php?tema=".$key."' class='btn btn-propio noAjax'><i class='icon-download'></i> Descargar</i></a>
				</td>
			 </tr>";
	}
	echo "<tr>
			<td> 6 </td>
			<td> Cuestionarios de evaluación </td>";
	echo "	<td class='centro'>
				<a href='gestion.php' class='btn btn-propio'><i class='icon-check'></i> Hacer examen</i></a>
			</td>
		</tr>";
	$listado=consultaBD('SELECT * FROM formacionPBC WHERE codigoUsuario='.$_SESSION['codigoU'],true);
	while($examen=mysql_fetch_assoc($listado)){
		echo "<tr>
				<td>".formateaFechaWeb($examen['fecha'])."</td>
				<td>".$examen['alumno']." DNI: ".$examen['dni']."</td>";
		if($examen['finalizado']=='NO'){
			if($examen['intentos']<3){
				echo "<td class='centro'>
						<div class='btn-group centro'>
							<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  		<ul class='dropdown-menu' role='menu'>
						    	<li><a href='gestion.php?codigo=".$examen['codigo']."'><i class='icon-search-plus'></i> Ver examen</i></a></li> 
						    	<li class='divider'></li>
						    	<li><a href='gestion.php?codigo=".$examen['codigo']."&rehacer'><i class='icon-undo'></i> Rehacer examen</i></a></li>
							</ul>
						</div>				
					</td>";
			} else {
				echo "<td class='centro'>
						<div class='btn-group centro'>
							<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  		<ul class='dropdown-menu' role='menu'>
						    	<li><a href='gestion.php?codigo=".$examen['codigo']."'><i class='icon-search-plus'></i> Ver examen</i></a></li> 
						    	<li class='divider'></li>
						    	<li><a href='javascript:void(0);' onclick='alert(\"Ya has utilizado los 3 intentos para hacer este examen. Contacte con el administrador\");' class='noAjax'><i class='icon-check'></i> Rehacer examen</i></a></li>
							</ul>
						</div>				
					</td>";
			}
		} else {
			echo "<td class='centro'>
						<div class='btn-group centro'>
							<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  		<ul class='dropdown-menu' role='menu'>
						    	<li><a href='gestion.php?codigo=".$examen['codigo']."'><i class='icon-search-plus'></i> Ver examen</i></a></li> 
						    	<li class='divider'></li>
						    	<li><a href='generaCertificado.php?codigo=".$examen['codigo']."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descargar certificado</i></a></li>
							</ul>
						</div>				
					</td>";
		}
		echo "</tr>";
	}
}

function gestionExamen(){
	operacionesExamen();

	abreVentanaGestion('Cuestionario de evaluación','?');
	$datos=compruebaDatos('formacionPBC');
	if($datos){
		$cuestionario=explode('&{}&', $datos['cuestionario']);
		foreach ($cuestionario as $value) {
			$parts=explode('=>', $value);
			if(isset($_GET['rehacer'])){
				$datos[$parts[0]]='';
			} else {
				$datos[$parts[0]]=$parts[1];
			}
		}
	}
	if($datos){
		$proximoIntento=$datos['intentos']+1;
		$intentos=3-$datos['intentos'];
		$fecha=formateaFechaWeb($datos['fecha']);
		campoOculto($datos,'codigo');
	} else {
		$proximoIntento=1;
		$intentos=3;
		$fecha=fecha();
	}
	$correcta=0;
	echo '<h3 class="apartadoFormulario">Datos del alumno</h3>';
	abreColumnaCampos();
		campoOculto($fecha,'fecha');
		campoOculto($datos,'codigoUsuario',$_SESSION['codigoU']);
		campoOculto($proximoIntento,'intentos');
		campoOculto($datos,'finalizado','NO');
		if(!$datos || $datos && $datos['finalizado']=='NO'){
			echo 'Dispone de '.$intentos.' intentos';
		}
		campoTexto('alumno','Alumno',$datos);
		campoTexto('dni','NIF',$datos);
	cierraColumnaCampos(true);
	echo '<h3 class="apartadoFormulario">Examen</h3>';
	echo '<table class="divExamen">';
		$opciones=array('a) 1989','b) 1898','c) 1970');
		$correcta+=preguntaExamen(1,'El Grupo de Acción Financiera (GAFI) fue creado en',$opciones,1,$datos);

		$opciones=array('a)	Es de aplicación directa e inmediata por cada Estado miembro','b) Básicamente incorpora al derecho comunitario las Recomendaciones del GAFI tras su revisión en 2003,   limitándose a establecer un marco general que ha de ser completado por los Estados miembros','Atiende a los riesgos concretos a los que están expuestos los Estados miembros');
		$correcta+=preguntaExamen(2,'La Tercera Directiva del Parlamento Europeo y del Consejo – Directiva 2005/60/CE-, relativa a la prevención de la utilización del sistema financiero para el blanqueo de capitales y para la financiación del terrorismo',$opciones,2,$datos);

		$opciones=array('a)	Entró en vigor el día siguiente a su publicación en el BOE','B) Entró en vigor el 30 de abril de 2010.','c)	Las dos respuestas anteriores son ciertas');
		$correcta+=preguntaExamen(3,'Ley 10/2010, de 28 de Abril, de Prevención del Blanqueo de Capitales y de la Financiación del Terrorismo:',$opciones,3,$datos);

		$opciones=array('a)	Tras la entrada en vigor de la Ley 10/2010, de 28 de abril,  en cuanto no resulte incompatible con aquella, hasta el día 6 de mayo del 2014','b) Hasta el 30 de abril del 2010, fecha en la que es derogado por la Ley 10/2010, de 28 de abril.','c) Hasta que se publica el Reglamento que desarrolla la Ley 12/2003, de 21 de mayo, de prevención y bloqueo de la financiación del terrorismo.');
		$correcta+=preguntaExamen(4,'El Real Decreto 925/1995, de 9 de Junio, por el que se aprueba el Reglamento de la Ley 19/1993, de 28 de diciembre, mantiene su vigencia',$opciones,1,$datos);

		$opciones=array('a)	Se aprueba por Real Decreto 403/2012, de 5 de mayo','b)	Se aprueba por Real Decreto 304/2014, de 5 de mayo','c)	Se aprueba por Real Decreto 304/2010, de 4 de noviembre');
		$correcta+=preguntaExamen(5,'El Reglamento que desarrolla la Ley 10/2010, de 28 de Abril, de Prevención del Blanqueo de Capitales y de la Financiación del Terrorismo',$opciones,2,$datos);

		$opciones=array('a)	Se considerará que hay blanqueo de capitales aun cuando las actividades que hayan generado los bienes se hubieran desarrollado en el territorio de otro Estado','b)	Se considerará que hay blanqueo de capitales sólo cuando las actividades que hayan generado los bienes se desarrollen en territorio español','c) Se considerará que hay blanqueo de capitales sólo cuando las actividades que hayan generado los bienes se desarrollen en algún Estado miembro de la Unión Europea');
		$correcta+=preguntaExamen(6,'A los efectos de la Ley 10/2010, de 28 de abril',$opciones,1,$datos);

		$opciones=array('a)	Exige como requisito que los bienes procedan de un delito castigado con pena superior a tres años','b) Es suficiente con que los bienes procedan de una “actividad delictiva”, no siendo requisito la condena previa','c) Es requisito la condena previa');
		$correcta+=preguntaExamen(7,'La Ley 10/2010, de 28 de abril, introduce como novedad',$opciones,2,$datos);

		$opciones=array('a)	Desde el día 4 de julio del 2003, por la modificación introducida a la Ley 19/1993, de 28 de diciembre, sobre determinadas medidas de prevención del blanqueo de capitales, mediante la Ley 19/2003, de 4 de julio','b)	Desde el 30 de abril de 2010, que entra en vigor la Ley 10/2010, de 28 de abril, de prevención del blanqueo de capitales y de la financiación del terrorismo','c) No son sujetos obligados de la prevención del blanqueo de capitales');
		$correcta+=preguntaExamen(8,'Auditores de cuentas, contables externos, asesores fiscales, notarios, abogados y procuradores son sujetos obligados de la prevención del blanqueo de capitales',$opciones,1,$datos);

		$opciones=array('a)	Cuando presta los servicios establecidos en el artículo 2.1 de la LPBC, en sus  letras m), ñ) y o)','b)	Siempre','c) Nunca');
		$correcta+=preguntaExamen(9,'El abogado es sujeto obligado de la Ley 10/2010, de 28 de abril',$opciones,1,$datos);

		$opciones=array('a)	Sí','b)	A veces','c) Nunca');
		$correcta+=preguntaExamen(10,'Son sujetos obligados de la Ley 10/2010, de 28 de abril: asesores fiscales, auditores de cuentas y contables externos',$opciones,1,$datos);

		$opciones=array('Sí, siempre','Algunas veces','En ningún caso');
		$correcta+=preguntaExamen(11,'El sujeto obligado de la prevención del blanqueo de capitales puede prestar sus servicios a personas físicas o jurídicas que no hayan sido debidamente identificadas',$opciones,3,$datos);

		$opciones=array('a) Sólo cuando se apliquen medidas reforzadas de diligencia debida','b) Es uno de los datos que se recaban en aplicación de las medidas normales de diligencia debida','c) Sólo cuando se apliquen medidas simplificadas de diligencia debida');
		$correcta+=preguntaExamen(12,'Con carácter previo al inicio de la relación de negocios, los sujetos obligados deben recabar de sus clientes información a fin de conocer la naturaleza de su actividad profesional o empresarial, dejando constancia mediante el registro correspondiente',$opciones,2,$datos);

		$opciones=array('a) Los sujetos obligados pueden recurrir a terceros sometidos a la LPBC para la aplicación de las medidas normales de diligencia debida, con excepción del seguimiento continuo de la relación de negocios','b) Los sujetos obligados pueden recurrir a terceros sometidos a la LPBC para la aplicación de las medidas de diligencia debida, eximiéndose de la responsabilidad respecto de la relación de negocios y operación cuando el incumplimiento sea imputable al tercero','c)Los sujetos obligados no pueden recurrir en ningún caso a terceros para la aplicación de las medidas normales de diligencia debida');
		$correcta+=preguntaExamen(13,'Aplicación por terceros de las medidas de diligencia debida',$opciones,1,$datos);

		$opciones=array('a) Se aplican en lugar de las medidas simplificadas de diligencia debida','b) Se aplican en lugar de las medidas normales de diligencia debida','c) Se aplican además de las medidas normales de diligencia debida');
		$correcta+=preguntaExamen(14,'Las medidas reforzadas de diligencia debida',$opciones,3,$datos);

		$opciones=array('a) Le aplica medidas simplificadas de diligencia debida','b) Le aplica medidas normales y medidas reforzadas de diligencia debida','c) Le aplica sólo medidas normales de diligencia debida');
		$correcta+=preguntaExamen(15,'Cuando el sujeto obligado presta un servicio a un cliente no presencial',$opciones,2,$datos);

		$opciones=array('a) Le aplica medidas simplificadas de diligencia debida.','b) Le aplica medidas normales y medidas reforzadas de diligencia debida','c) Le aplica sólo medidas normales de diligencia debida');
		$correcta+=preguntaExamen(16,'Si el cliente ejerce un cargo público, el sujeto obligado',$opciones,2,$datos);

		$opciones=array('Verdadero','Falso','Sólo en el supuesto de clientes no presenciales');
		$correcta+=preguntaExamen(17,'En el supuesto que el sujeto obligado detecte indicios o tenga la certeza de que la operación pueda estar vinculada al blanqueo de capitales o se trate de operaciones complejas, inusuales o que no tengan un motivo económico o lícito aparente, procederá a efectuar un examen especial de dicha operación, dejando constancia por escrito de su resultado',$opciones,1,$datos);

		$opciones=array('a) Antes de concluir el examen especial','b) Concluido el examen especial','c) Concluido el examen especial, si se determinara por el sujeto obligado la concurrencia en la operativa de indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo, se efectuará sin dilación la comunicación por indicio, en el soporte y formato establecido por el Servicio Ejecutivo de la Comisión');
		$correcta+=preguntaExamen(18,'La comunicación por indicio se efectuará',$opciones,3,$datos);

		$opciones=array('a) Diez años desde la terminación de la relación de negocio o la ejecución de la operación ocasional','b) Seis años desde la terminación de la relación de negocio o la ejecución de la operación ocasional','c) Diez años desde la contratación de la prestación del servicio');
		$correcta+=preguntaExamen(19,'Los sujetos obligados conservarán los documentos y mantendrán registros adecuados de todas las relaciones de negocio y operaciones, nacionales e internacionales, durante un periodo de',$opciones,1,$datos);

		$opciones=array('a) Será preceptiva siempre','b) No será preceptiva cuando los sujetos obligados sean empresarios o profesionales individuales','c) No será preceptiva en los sujetos obligados comprendidos en el artículo 2.1 i) y siguientes y en los corredores de comercio cuando, con inclusión de los agentes, ocupen a menos de 50 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 10 millones de euros, desempeñando en tales casos sus funciones el representante ante el Servicio Ejecutivo de la Comisión');
		$correcta+=preguntaExamen(20,'La constitución de un órgano de control interno',$opciones,3,$datos);
		if($datos && !isset($_GET['rehacer'])){
			if($correcta>=10){
				echo '<label class="label label-success label-examen">Ha finalizado con éxito la formación</label>';
				$consulta=consultaBD('UPDATE formacionPBC SET finalizado="SI" WHERE codigo='.$datos['codigo'],true);
			} else {
				echo '<label class="label label-danger label-examen">No aprobado</label>';
			}
		}
	echo '</table>';
	if($datos && !isset($_GET['rehacer'])){
		cierraVentanaGestion('index.php',false,false);
	} else {
		cierraVentanaGestion('index.php',false,true,'Guardar y enviar cuestionario');
	}
}

function preguntaExamen($n,$texto,$opciones,$correcta,$valor=false){
	$nombreCampo='pregunta'.$n;
	if($valor!=''){
		$valor=compruebaValorCampo($valor,$nombreCampo);
	}
	else{
		$valor='';
	}
	echo "
	<tr>                     
      <td class='a40'>$n. $texto:</td>
      <td class='a60'>";
    for($i=0;$i<count($opciones);$i++){
    	$j=$i+1;
    	echo "<label class='radio inline'>
    			<input type='radio' name='$nombreCampo' value='".$j."'";
    	if($j==$valor){
    		echo " checked='checked'";
    	}
    	if($valor!=''){
    		echo ' disabled="disabled"';
    	} 
    	echo ">".$opciones[$i];
    	if($valor!=''){
    		if($j==$correcta){
    			echo ' <i class="icon-check icon-success"></i>';
    		} elseif($j==$valor){
    			echo ' <i class="icon-times icon-danger"></i>';
    		}
    	}
    	echo "</label>";
    	echo "<br />";
    }
    campoOculto($correcta,'respuesta'.$n);
    echo "
      </td>     
    </tr>";
    return $valor==$correcta?1:0;
}

function generaTema($tema){
	$res=array('titulo'=>'','contenido'=>'');
	$temas=array(1=>'Contexto de la normativa en prevención del blanqueo de capitales y de la financiacion del terrorismo',2=>'Ámbito de aplicación de la ley 10/2010, de 28 de Abril, de prevención del blanqueo de capitales y de la financiación del terrorismo',3=>'Abogados, asesores fiscales, contables y otro profesionales como sujeto obligatorio de PBLC',4=>'Obligaciones de diligencia debida',5=>'Obligaciones de información',6=>'Medidas de control interno');
	$res['titulo']=$temas[$tema];
	switch ($tema) {
		case 1:
			$res['contenido']=tema1();
			break;
		case 2:
			$res['contenido']=tema2();
			break;
		case 3:
			$res['contenido']=tema3();
			break;
		case 4:
			$res['contenido']=tema4();
			break;
		case 5:
			$res['contenido']=tema5();
			break;
		case 6:
			$res['contenido']=tema6();
			break;
	}
	return $res;
}

function tema1(){
	$contenido='
	La política de prevención del blanqueo de capitales surgió a finales de la década de 1980, como reacción a la preocupación planteada por la criminalidad financiera derivada de organizaciones criminales.<br/><br/> 
El riesgo de penetración de importantes sectores del sistema financiero por parte de las organizaciones criminales, para el que no existía una respuesta adecuada por los instrumentos existentes en ese momento, ni estatales ni internacionales, dio lugar a una política internacional coordinada, siendo la manifestación más importante de ésta la creación en 1989 del Grupo de Acción Financiera (GAFI). <br/><br/>
Las Recomendaciones del GAFI, aprobadas en 1990, se convirtieron en estándar internacional en la materia, constituyéndose en inspiración directa de la Primera Directiva comunitaria (Directiva 91/308/CEE del Consejo, de 10 de junio de 1991).<br/><br/>
Obviamente, un conocimiento más profundo de las técnicas utilizadas por las redes de blanqueo de capitales así como la evolución y adaptación de la política pública al respecto, tienen como consecuencia cambios en los estándares internacionales y en el derecho comunitario.<br/><br/>
El blanqueo de capitales y la financiación  del terrorismo se presentan como fenómenos universales y globalizados, que aprovechan las ventajas que ofrece la economía internacional y la paulatina eliminación de barreras a los intercambios a nivel mundial. Por ello, la respuesta de la comunidad internacional a este fenómeno debe ser coordinada y global.<br/><br/>
La política de prevención del blanqueo de capitales y de la financiación del terrorismo se ha desarrollado en España en consonancia con la evolución de los estándares internacionales en esta materia, colaborando incluso activamente a través de su participación como miembro del GAFI desde su fundación, en 1989.<br/><br/>
En España, la ley actualmente en vigor en esta materia es la  <b>Ley 10/2010, de 28 de abril, de Prevención del Blanqueo de Capitales y de la Financiación del Terrorismo,</b>  publicada en el BOE el día 29 de abril de 2010 –en lo sucesivo en <b>LPBC</b>-, que transpone la 3ª DIRECTIVA DEL PARLAMENTO EUROPEO Y DEL CONSEJO – Directiva 2005/60/CE-, relativa a la prevención de la utilización del sistema financiero para el blanqueo de capitales y para la financiación del terrorismo.<br/><br/>
 La Tercera Directiva básicamente incorpora al derecho comunitario las Recomendaciones del GAFI tras su revisión en 2003, y que se limita a establecer un marco general que ha de ser completado por los Estados miembros, lo que implica que la Tercera Directiva es una norma de mínimos, que ha de ser reforzada o extendida por cada Estado miembro atendiendo a los riesgos concretos a los que esté expuesto.<br/><br/>
<b>La LPBC   entra en vigor el 30 de abril del 2010</b> y viene a derogar la Ley 19/1993, de 28 de diciembre, sobre determinadas medidas de prevención del blanqueo de capitales, si bien continúan siendo de aplicación las disposiciones sancionadoras de  la misma a los hechos cometidos con anterioridad a la entrada en vigor de la LPBC.<br/><br/>
 La Ley 19/1993  coexistía con la Ley 12/2003, de 21 de mayo, de prevención y bloqueo de la financiación del terrorismo. La LPBC  mantiene la Ley 12/2003, de 21 de mayo, en lo relativo al bloqueo, regulando de forma unitaria los aspectos preventivos tanto del blanqueo de capitales como de la financiación del terrorismo. Se mantiene en el ámbito del Ministerio del Interior el bloqueo, atribuyéndose a la Comisión de Prevención del Blanqueo de Capitales e Infracciones Monetarias –situada orgánicamente en la Secretaría de Estado de Economía y con participación de los supervisores financieros- la competencia para la incoación e instrucción de los expedientes sancionadores por incumplimiento de las obligaciones de prevención. Se mantiene la competencia de la Comisión de Vigilancia de Actividades de Financiación del Terrorismo para acordar el bloqueo o congelación de fondos cuando existan motivos que lo justifiquen.<br/><br/>
La LPBC establece en la Disposición final quinta el desarrollo reglamentario, habilitando al Gobierno para que, en el plazo de un año a contar desde la entrada en vigor de la misma, apruebe las disposiciones reglamentarias para su ejecución y desarrollo, si bien hasta la entrada en vigor del nuevo Reglamento, mantiene la vigencia del Reglamento de la Ley 19/1993 –aprobado por Real Decreto 925/1995, de 9 de junio- y sus normas de desarrollo, en cuanto no resulten incompatibles con la LPBC -DT 1ª de ésta-.<br/><br/>
Por tanto, el Real Decreto 925/1995 continúa en vigor en cuanto no se contradiga con lo establecido por la LPBC  hasta el día 6 de mayo del 2014, día en el que se publica en el BOE y entra en vigor el Real Decreto 304/2014, por el que se aprueba el Reglamento de la Ley 10/2010, de 28 de abril, de prevención del blanqueo de capitales y de la financiación del terrorismo –RLPBC-. Con la aprobación de este Real Decreto se incorporan a la normativa preventiva en España las principales novedades de la normativa internacional surgidas a partir de la aprobación de las nuevas Recomendaciones de GAFI, desarrollando el enfoque orientado al riesgo incorporado por la LPBC.<br/><br/><br/><br/>
El objetivo del <b>nuevo enfoque orientado al riesgo</b> es incrementar la eficiencia de las medidas preventivas a aplicar, constituyendo un <b>elemento de flexibilidad</b> de una norma que va dirigida a un <b>colectivo</b> muy <b>heterogéneo</b> de <b>sujetos obligados</b>.<br/><br/>
Por este motivo, se establecen unas obligaciones básicas y comunes para todos los sujetos obligados,  permitiendo un margen de adaptación de la aplicación de la norma a la realidad específica de la actividad  que cada sujeto desarrolla, el cual ha de prestar especial atención a aquellas situaciones, productos y clientes que presenten un nivel de riesgo superior.<br/><br/>
El incumplimiento de las obligaciones que impone la LPBC se sanciona en función de la calificación de la infracción como leve, grave o muy grave, ascendiendo a 60.000 euros la cuantía de la multa por infracción leve.<br/><br/>

En materia administrativa, las normas de mayor importancia son las siguientes:<br/><br/>
Ley 10/2010, de 28 de Abril, de Prevención del Blanqueo de Capitales y de la Financiación del Terrorismo –en lo sucesivo LPBC-, publicada el 29 de abril del mismo año y en vigor desde el 30 de abril de 2010.<br/><br/>
Real Decreto 304/2014, de 5 de mayo - por el que se aprueba el Reglamento que desarrolla la LPBC- en vigor desde el día 6 de mayo del 2014.<br/><br/>
En materia de régimen sancionador respecto de los hechos cometidos con anterioridad a la entrada en vigor de la Ley 10/2010 -es decir, a los hechos cometidos antes del 30 de abril del 2010-, continuará siendo de aplicación la Ley 19/1993, de 28 de Diciembre, sobre determinadas medidas de prevención del blanqueo de capitales<br/><br/>
El Real Decreto 925/1995, de 9 de Junio, por el que se aprueba el Reglamento de la Ley 19/1993, de 28 de diciembre, mantiene su vigencia tras la entrada en vigor de la LPBC  en cuanto no resulte incompatible con aquella, hasta el día 6 de mayo del 2014, que entró en vigor el Reglamento que desarrolla la LPBC.<br/><br/>
En materia penal, los artículos 301 a 304 del Código Penal, en los términos en que han quedado redactados tras la entrada en vigor de la Ley 5/2010 de reforma del Código Penal. <br/><br/>
En el ámbito penal, poner de manifiesto la relación entre la legislación administrativa y la penal, puesto que La falta de diligencia establecida por la LPBC puede constituir base para la imputación del delito de blanqueo de capitales tipificado en los articulos 301 a 304 CP.
';

	return $contenido;
}

function tema2(){
	$contenido='
	La Ley 10/2010, de 28 de abril –en lo sucesivo LPBC- tiene por objeto la protección de la integridad del sistema financiero y de otros sectores de actividad económica mediante el establecimiento de obligaciones de prevención del blanqueo de capitales y de la financiación del terrorismo.<br/><br/> 
Nos encontramos, por tanto, ante una ley administrativa de carácter preventivo, cuya finalidad es impedir el blanqueo de capitales y la financiación del terrorismo incidiendo sobre determinados sectores o profesionales implicados en los mismos, a los cuales se les impone una serie de obligaciones cuyo cumplimiento evitará que se cometa blanqueo de capitales o la financiación  del terrorismo. Por tanto, en el supuesto que el sujeto obligado de la LPBC no cumpla con las obligaciones impuestas por ésta, aún cuando el mismo no cometa ni intervenga en una operación de blanqueo de capitales, será sancionado por dicho incumplimiento.<br/><br/>
A los efectos de la LPBC, se consideran blanqueo de capitales las siguientes actividades:
<ul>
<li>La conversión o la transferencia de bienes, a sabiendas de que dichos bienes proceden de una actividad delictiva o de la participación en una actividad delictiva, con el propósito de ocultar o encubrir el origen ilícito de los bienes o de ayudar a personas que estén implicadas a eludir las consecuencias jurídicas de sus actos.</li>
<li>La ocultación o el encubrimiento de la naturaleza, el origen, la localización, la disposición, el movimiento o la propiedad real de bienes o derechos sobre bienes, a sabiendas de que dichos bienes proceden de una actividad delictiva o de la participación en una actividad delictiva.</li>
<li>La adquisición, posesión o utilización de bienes, a sabiendas, en el momento de la recepción de los mismos, de que proceden de una actividad delictiva o de la participación en una actividad delictiva.</li>
<li>La participación en alguna de las actividades relacionadas así como la asociación para cometer este tipo de actos, las tentativas de perpetrarlas y el hecho de ayudar, instigar o aconsejar a alguien para realizarlas o facilitar su ejecución.</li>
</ul>
Existirá blanqueo de capitales aun cuando las conductas descritas con anterioridad como tales sean realizadas por la persona o personas que cometieron la actividad delictiva que haya generado los bienes.<br/><br/>
Se considerará que hay blanqueo de capitales aun cuando las actividades que hayan generado los bienes se hubieran desarrollado en el territorio de otro Estado.<br/><br/>
A los efectos de la LPBC, se entenderá por bienes procedentes de una actividad delictiva todo tipo de activos cuya adquisición o posesión tenga su origen en un delito, tanto materiales como inmateriales, muebles o inmuebles, tangibles o intangibles, así como los documentos o instrumentos jurídicos con independencia de su  forma, incluidas la electrónica o la digital, que acrediten la propiedad de dichos activos o un derecho sobre los mismos, con inclusión de la cuota defraudada en el caso de los delitos contra la Hacienda Pública.<br/><br/>

En cuanto a la financiación del terrorismo, a los efectos de la Ley 10/2010, de 28 de abril, se entenderá por tal el suministro, el depósito, la distribución o la recogida de fondos o bienes, por cualquier medio, de forma directa o indirecta, con la intención de utilizarlos o con el conocimiento de que serán utilizados, íntegramente o en parte, para la comisión de cualquiera de los delitos de terrorismo tipificados en el Código Penal.<br/><br/>
Se considerará que existe financiación del terrorismo aun cuando el suministro o la recogida de fondos o bienes se hayan desarrollado en el territorio de otro Estado.<br/><br/>
La Ley 10/2010, de 28 de abril, introduce una serie de novedades entre las que destacamos las siguientes:
<ul>
<li>A diferencia de la normativa anterior, que exigía que los bienes procedieran de un delito castigado con pena superior a tres años, tras la entrada en vigor de la Ley 10/2010, de 28 de abril es suficiente con que los bienes procedan de una "actividad delictiva", no siendo requisito la condena previa.</li>
<li>La mera posesión o utilización de bienes de procedencia ilícita constituye blanqueo de capitales.</li>
<li>La inclusión de la figura del “autoblanqueo” como punible.</li>
<li>Entre los bienes objeto de blanqueo de capitales se encuentra la cuota defraudada, en el caso de los delitos contra la Hacienda Pública. En esta caso, podría ser requisito la existencia de un proceso penal por este delito.</li>
</ul><br/><br/><br/>
<b>2.2 SUJETOS OBLIGADOS.</b><br/><br/>
El <b>artículo 2 LPBC</b> establece que la misma será de aplicación a los siguientes sujetos obligados:
<ul style="list-style-type:none;">
<li>a)	Las entidades de crédito.</li>
<li>b)	Las entidades aseguradoras autorizadas para operar en el ramo de vida y los corredores de seguros cuando actúen en relación con seguros de vida u otros servicios relacionados con inversiones, con las excepciones que se establezcan reglamentariamente.</li>
<li>c)	Las empresas en servicios de inversión.</li>
<li>d)	Las sociedades gestoras de instituciones de inversión colectiva y las sociedades de inversión cuya gestión no esté encomendada a una sociedad gestora.</li>
<li>e)	Las entidades gestoras de fondos de pensiones.</li>
<li>f)	Las sociedades gestoras de entidades de capital-riesgo y las sociedades de capital-riesgo cuya gestión no esté encomendada a una sociedad gestora.</li>
<li>g)	Las sociedades de garantía recíproca.</li>
<li>h)	Las entidades de pago.</li>
<li>i)	Las personas que ejerzan profesionalmente actividades de cambio de moneda.</li>
<li>j)	Los servicios postales respecto de las actividades de giro o transferencia.</li>
<li>k)	Las personas dedicadas profesionalmente a la intermediación en la concesión de préstamos o créditos, así como las personas que, sin haber obtenido autorización como establecimientos financieros de crédito, desarrollen profesionalmente alguna de las actividades a que se refiere la Disposición adicional primera de la Ley 3/1994, de 14 de abril, por la que se adapta la legislación española en materia de Entidades de Crédito a la Segunda Directiva de Coordinación Bancaria y se introducen otras modificaciones relativas al Sistema Financiero.</li>
<li>l)	Los promotores inmobiliarios y quienes ejerzan profesionalmente actividades de agencia, comisión o intermediación en la compraventa de bienes inmuebles.</li>
<li>m)	Los <b>auditores de cuentas, contables externos o asesores fiscales.</b><br/> 
Es preciso tener en cuenta que el término “asesor fiscal” no designa ninguna profesión concreta sino más bien una actividad que es desarrollada por distintos profesionales, como economistas, titulados mercantiles, abogados… Por tanto, todo profesional que actúa prestando este tipo de asesoramiento, será sujeto obligado de la LPBC.</li>
<li>n)	<b>Los notarios y los registradores de la propiedad, mercantiles y de bienes muebles.</b></li>
<li>ñ)  <b>Los abogados, procuradores u otros profesionales independientes cuando participen en</b> la concepción, realización o asesoramiento de operaciones por cuenta de clientes relativas a la compraventa de bienes inmuebles o entidades comerciales, la gestión de fondos, valores u otros activos, la apertura o gestión de cuentas corrientes, cuentas de ahorros o cuentas de valores, la organización de las aportaciones necesarias para la creación, el funcionamiento o la gestión de empresas o la creación, el funcionamiento o la gestión de fideicomisos (&lt;trust&gt;), sociedades o estructuras análogas, o cuando actúen por cuenta de clientes en cualquier operación financiera o inmobiliaria.</li>
<li>o)	Las <b>personas que con carácter profesional y con arreglo a la normativa específica que en cada caso sea aplicable presten los siguientes servicios a terceros</b>: constituir sociedades u otras personas jurídicas; ejercer funciones de dirección o secretaría de una sociedad, socio de una asociación o funciones similares en relación con otras personas jurídicas o disponer que otra persona ejerza dichas funciones; facilitar un domicilio social o una dirección comercial, postal, administrativa y otros servicios afines a una sociedad, una asociación o cualquier otro instrumento o persona jurídicos; ejercer funciones de fideicomisario en un fideicomiso (&lt;trust&gt;) expreso o instrumento jurídico similar o disponer que otra persona ejerza dichas funciones; o ejercer funciones de accionista por cuenta de otra persona, exceptuando las sociedades que coticen en un mercado regulado y estén sujetas a requisitos de información conformes con el derecho comunitario o a normas internacionales equivalentes, o disponer que otra persona ejerza dichas funciones.</li>
<li>p)	Los casinos de juego.</li>
<li>q)	Las personas que comercien profesionalmente con joyas, piedras o metales preciosos.</li>
<li>r)	Las personas que comercien profesionalmente con objetos de arte o antigüedades.</li>
<li><br/><br/>s)	Las personas que ejerzan profesionalmente las actividades a que se refiere el artículo 1 de la Ley 43/2007, de 13 de diciembre, de protección de los consumidores en la contratación de bienes con oferta de restitución del precio.</li>
<li>t)	Las personas que ejerzan actividades de depósito, custodia o transporte profesional de fondos o medios de pago.</li>
<li>u)	Las personas responsables de la gestión, explotación y comercialización de loterías u otros juegos de azar respecto de las operaciones de pago de premios.</li>
<li>v)	Las personas físicas que realicen movimientos de medios de pago, en los términos establecidos en el artículo 34.</li>
<li>w)	Las personas que comercien profesionalmente con bienes, en los términos establecidos en el artículo 38.</li>
<li>x)	Las fundaciones y asociaciones, en los términos establecidos en el artículo 39.</li>
<li>y)	Los gestores de sistemas de pago y de  compensación y liquidación de valores y productos financieros derivados, así como los gestores de tarjetas de crédito o debito emitidas por otras entidades, en los términos establecidos en el artículo 40.<br/><br/>
	Tienen la consideración de <b>sujetos obligados</b> las personas físicas o jurídicas que desarrollen las actividades relacionadas, si bien, cuando las personas físicas actúen en calidad de empleados de una persona jurídica o le presten servicios permanentes o esporádicos, las obligaciones impuestas por la LPBC recaerán sobre dicha persona jurídica respecto de los servicios prestados.<br/><br/>
	Los sujetos obligados quedarán sometidos a las obligaciones establecidas en la LPBC respecto de las operaciones realizadas a través de agentes y otras personas que actúen como mediadores o intermediarios de aquéllos.</li>
</ul>
Se entenderán sujetos a la LPBC las personas o entidades no residentes que, a través de sucursales o agentes o mediante prestación de servicios sin establecimiento permanente, desarrollen en España actividades de igual naturaleza a las de las personas o entidades relacionadas.<br/><br/>

El artículo 2.3 LPBC contempla la posibilidad de excluir, reglamentariamente, determinadas actividades, lo que el Reglamento que desarrolla esta Ley hace efectivo a través de su artículo 3, excluyendo de la sujeción a la LPBC:
<ul>
<li>Los actos notariales y registrales que carezcan de contenido económico o patrimonial o no sean relevantes a efectos de prevención del blanqueo de capitales y de la financiación del terrorismo. A tal efecto, mediante Orden del Ministro de Economía y Competitividad, previo informe del Ministerio de Justicia, se establecerá la relación de tales actos.</li>
<li>La actividad de cambio de moneda extranjera realizada con carácter accesorio a la actividad principal del titular cuando concurran los siguientes requisitos: que la actividad de cambio de moneda extranjera se verifique, exclusivamente, como servicio proporcionado a los clientes de la actividad principal; que la cantidad cambiada por cliente no exceda de 1.000 euros en cada trimestre natural; que la actividad de cambio de moneda extranjera sea limitada en términos absolutos, sin que pueda exceder la cifra de 100.000 euros anuales; que la actividad de cambio de moneda extranjera sea accesoria a la actividad principal, considerándose como tal aquella que no exceda del 5 por ciento de la facturación anual del negocio.</li>
</ul>
';

	return $contenido;
}

function tema3(){
	$contenido='Auditores de cuentas, contables externos, asesores fiscales, notarios, abogados y procuradores son sujetos obligados de la prevención del blanqueo de capitales desde el día 4 de julio del 2003, por la modificación introducida a la Ley 19/1993, de 28 de diciembre, sobre determinadas medidas de prevención del blanqueo de capitales, mediante la Ley 19/2003, de 4 de julio.<br/><br/>
<b>3.1. OPERACIONES SUJETAS ESTABLECIDAS POR EL ARTÍCULO 2, LETRAS m) ñ) y o) LPBC.</b><br/><br/>
El artículo 2.1 de la LPBC, en sus  letras m), ñ) y o) establece que, serán sujetos obligados de la misma, quienes presten los servicios siguientes:
<ul>
<li>Auditorías de cuentas, contabilidad externa o asesoramiento fiscal –si bien el artículo 2.1 m) LPBC establece que serán sujetos obligados “Los auditores de cuentas, contables externos o asesores fiscales”, debemos tener en cuenta, al interpretar la norma, que estos términos no designan una profesión concreta, sino una actividad que es desarrollada por distintos profesionales, economistas, titulados mercantiles así como por abogados. En la medida que actúen en ese asesoramiento o servicio serán sujetos obligados-.</li>

<li>Los abogados, procuradores u otros profesionales independientes cuando participen en la concepción, realización o asesoramiento de operaciones por cuenta de clientes relativas a la compraventa de bienes inmuebles o entidades comerciales, la gestión de fondos, valores u otros activos, la apertura o gestión de cuentas corrientes, cuentas de ahorros o cuentas de valores, la organización de las aportaciones necesarias para la creación, el funcionamiento o la gestión de empresas o la creación, el funcionamiento o la gestión de fideicomisos (&lt;trust&gt;), sociedades o estructuras análogas, o cuando actúen por cuenta de clientes en cualquier operación financiera o inmobiliaria.</li>


<li>Las personas que con carácter profesional y con arreglo a la normativa específica que en cada caso sea aplicable presten los siguientes servicios a terceros: constituir sociedades u otras personas jurídicas; ejercer funciones de dirección o secretaría de una sociedad, socio de una asociación o funciones similares en relación con otras personas jurídicas o disponer que otra persona ejerza dichas funciones; facilitar un domicilio social o una dirección comercial, postal, administrativa y otros servicios afines a una sociedad, una asociación o cualquier otro instrumento o persona jurídicos; ejercer funciones de fideicomisario en un fideicomiso (&lt;trust&gt;) expreso o instrumento jurídico similar o disponer que otra persona ejerza dichas funciones; o ejercer funciones de accionista por cuenta de otra persona, exceptuando las sociedades que coticen en un mercado regulado y estén sujetas a requisitos de información conformes con el derecho comunitario o a normas internacionales equivalentes, o disponer que otra persona ejerza dichas funciones.</li>
</ul>
En cuanto a los <b>abogados</b> que sean sujetos obligados de la LPBC, el <b>artículo 22</b> de la misma establece un supuesto de <b>no sujeción</b>, excluyéndoles expresamente de su cumplimiento "con respecto a la información que reciban de uno de sus clientes u obtengan sobre él al determinar la posición jurídica en favor de su cliente o desempeñar su misión de defender a dicho cliente en procesos judiciales o en relación con ellos, incluido el asesoramiento  sobre la incoación o la forma de evitar un proceso, independientemente de si han recibido u obtenido dicha información antes, durante o después de tales procesos”, si bien “sin perjuicio de lo establecido en la presente Ley, los abogados guardarán el deber de secreto profesional de conformidad con la legislación vigente”.<br/><br/>

Dado lo heterogéneo que es el colectivo de sujetos obligados de la LPBC, tanto ésta como el Reglamento que la desarrolla establecen una serie de obligaciones básicas y comunes para todos los sujetos obligados, permitiendo un margen de adaptación de la aplicación de las normas a la actividad específica que el sujeto desarrolla.<br/><br/>

Además, en cuanto a las obligaciones de tipo procedimental exigidas a ciertos tipos de sujetos obligados, se limitan las obligaciones procedimentales para los sujetos de tamaño más reducido, incrementando la exigencia en función de la dimensión y volumen de negocio del sujeto obligado.<br/><br/>

<b>En las unidades didácticas siguientes, nos centraremos en las obligaciones que la LPBC y el Reglamento</b> que la desarrolla <b>impone</b> a los sujetos obligados establecidos en las letras  m), ñ) y o) del artículo 2 de la LPBC, es decir, las obligaciones impuestas en la materia <b>a auditores de cuentas, contables externos, asesores fiscales, abogados y profesionales</b> que presten alguno de los servicios establecidos en el referido artículo, no entrando en el estudio de las obligaciones que se imponen a otros sujetos obligados.<br/><br/>
<br/><br/><br/>
Para su estudio, dividiremos estas obligaciones en tres áreas:<br/><br/>

<b>1.	Obligaciones de diligencia debida.<br/><br/>

2.	Obligaciones de información.<br/><br/>


3.	De las medidas de control interno.</b>
';
	return $contenido;
}

function tema4(){
	$contenido='Las obligaciones de diligencia debida consisten en una serie de comprobaciones a realizar por el sujeto obligado antes de iniciar la prestación del servicio, cuya finalidad es  detectar cualquier indicio de  blanqueo de capitales para evitar su comisión, imponiéndole al profesional  la obligación de no intervenir así como de  comunicarlo de inmediato al SEPBLAC (Servicio Ejecutivo de Prevención del Blanqueo de Capitales) en el supuesto que detecte indicios de blanqueo de capitales.<br/><br/>
Las medidas de diligencia debida a aplicar al supuesto concreto se determinan en función del nivel de riesgo que presente la operación o el tipo de cliente, atendiendo a una serie de elementos o parámetros previstos en la legislación en vigor así como en la política de admisión de clientes que establezca el  sujeto obligado.<br/><br/>
De esta forma, el sujeto obligado debe analizar cada operación que se le encomiende en el ámbito de la normativa a fin de establecer un nivel de riesgo y, en función de éste, aplicar las medidas de diligencia debida que correspondan, dejando <u>constancia por escrito</u> de este <u>análisis de cada cliente u operación</u>, ya que solo así se podrá acreditar ante las autoridades competentes que se han adoptado las medidas de diligencia adecuadas.<br/><br/>
Se deberá conservar <u>durante diez años</u> los <u>documentos</u> que acrediten el cumplimiento de los deberes impuestos por la Ley. Dicho plazo se computará desde la ejecución de la operación o, en su caso, de la terminación de la relación de negocios.<br/><br/>
Dichas medidas se aplicarán tanto a clientes nuevos a partir de la entrada en vigor de la LPBC, el 30 de abril de 2010, como a los ya existentes a esa fecha, si bien respecto a estos últimos, la LPBC  concede un plazo de 5 años a contar desde la entrada en vigor de la misma.<br/><br/>
El sujeto obligado puede recurrir a terceros para la aplicación de las medidas de diligencia debida, si bien será plenamente responsable por cualquier incumplimiento de la Ley.<br/><br/>
Las medidas de diligencia debida se clasifican en:
<ul>
<li>Medidas normales.</li>
<li>Medidas reforzadas.</li>
<li>Medidas simplificadas.</li>
</ul>
Partimos de la base de que a todo cliente le aplicaremos las medidas normales y,  en el supuesto que concurran  determinadas circunstancias o elementos considerados de riesgo por la normativa al respecto o por el propio sujeto obligado, se aplicarán, además de las medidas normales, las medidas reforzadas de diligencia debida.<br/><br/>
El Reglamento que desarrolla la LPBC contempla, en sus artículos 15 y 16, una relación de clientes y de productos u operaciones, respectivamente, susceptibles de aplicarles, en lugar de las medidas normales, unas medidas simplificadas de diligencia debida que, en definitiva, lo que viene a suponer es un nivel de exigencia de comprobaciones menor que el que se exige en las medidas normales.<br/><br/>
No obstante, con independencia de cualquier excepción, exención o umbral, si durante la prestación del servicio el sujeto obligado detectara indicios o certeza de blanqueo de capitales o de financiación del terrorismo, procederá a cumplir con sus obligaciones de comunicación en el soporte y formato establecido por el Servicio Ejecutivo de la Comisión, previo análisis documentado en el correspondiente examen especial, de acuerdo a lo establecido en los artículos 17 y 18 de la LPBC y en los artículos 25 y 26 del RLPBC.<br/><br/>

<b>4.1. MEDIDAS NORMALES DE DILIGENCIA DEBIDA:</b><br/><br/>

<b>4.1.1. Identificación formal.</b><br/><br/>

Los sujetos obligados establecidos en las letras m) ñ) y o)  del artículo 2 de la LPBC identificarán y comprobarán la identidad de cuantas personas físicas o jurídicas pretendan establecer relaciones de negocio o intervenir en cualesquiera operaciones ocasionales cuyo importe sea igual o superior a 1.000 euros, mediante documentos fehacientes, con carácter previo al establecimiento de la relación de negocios o a la ejecución de cualesquiera operaciones.<br/><br/>

En el supuesto que no se pueda comprobar esta identidad mediante documentos fehacientes en un primer momento, siempre y cuando no existan elementos de riesgo en la operación, se le podrá conceder al cliente el plazo de un mes para aportarlos.<br/><br/>
<br/><br/>
En ningún caso los sujetos obligados prestarán sus servicios a personas físicas o jurídicas que no hayan sido debidamente identificadas.<br/><br/>

Se prohíbe expresamente la apertura, contratación o mantenimiento de cuentas, libretas, activos o instrumentos numerados, cifrados, anónimos o con nombres ficticios.<br/><br/>

Los documentos de identificación deben encontrarse en vigor en el momento de establecer relaciones de negocio o ejecutar operaciones ocasionales. En el supuesto de personas jurídicas, la vigencia de los datos consignados en la documentación aportada deberá acreditarse mediante una declaración responsable del cliente.<br/><br/>

Reglamentariamente – artículo 6 RLPBC- se establece que se consideran <b>documentos fehacientes</b>, a efectos de identificación formal, los siguientes:
<ul>
<li>Para las personas físicas de nacionalidad española, el Documento Nacional de Identidad.</li>
<li>Para las personas físicas de nacionalidad extranjera: la Tarjeta de Residencia, la Tarjeta de Identidad de Extranjero, el Pasaporte o, en el caso de ciudadanos de la Unión Europea o del Espacio Económico Europeo, el documento, carta o tarjeta oficial de identidad personal expedido por las autoridades de origen; documento de identidad expedido por el Ministerio de Asuntos Exteriores y de Cooperación para el personal de las representaciones diplomáticas y consulares de terceros países en España; excepcionalmente, se podrán aceptar por el sujeto obligado otros documentos de identidad personal expedidos por una autoridad gubernamental siempre que gocen de las adecuadas garantías de autenticidad e incorporen fotografía del titular.</li>
<li>Para las personas jurídicas: los documentos públicos que acrediten su existencia y contengan su denominación social, forma jurídica, domicilio, la identidad de sus administradores, estatutos y número de identificación fiscal; en el caso de personas jurídicas de nacionalidad española, será admisible certificación del Registro Mercantil provincial, aportada por el cliente u obtenida mediante consulta telemática.</li>
<li>En los casos de representación legal o voluntaria, la identidad del representante y de la persona o entidad representada, será comprobada documentalmente, para lo cual deberá obtenerse copia de cualquiera de los documentos fehacientes referenciados correspondientes tanto al representante como a la persona o entidad representada, así como el documento público acreditativo de los poderes conferidos.</li>
<li>Identificación en el supuesto de entidades sin personalidad jurídica: se identificará y comprobará mediante los documentos fehacientes referidos la identidad de todos los partícipes de la misma.</li>
<li>Identificación en el supuesto de entidades sin personalidad jurídica que no ejerzan actividades económicas: con carácter general, será suficiente con la identificación y comprobación mediante los documentos fehacientes referidos de  la identidad de la persona que actúe por cuenta de la entidad.</li>
<li>En los fideicomisos anglosajones (&lt;trusts&gt;) u otros instrumentos jurídicos análogos que, no obstante carecer de personalidad puedan actuar en el tráfico económico, los sujetos obligados requerirán el documento constitutivo, además de identificar y comprobar la identidad de la persona que actúa por cuenta de los beneficiarios o de acuerdo con los términos del fideicomiso o instrumento jurídico.</li>
</ul>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<b>4.1.2. Identificación del titular real.</b>
<ul>
<li>El sujeto obligado debe indagar si el cliente actúa por cuenta propia o de terceros y, en el supuesto que actúa por cuenta de terceros, deberá, con carácter previo a la aceptación del encargo, identificar y comprobar mediante los documentos fehacientes referenciados la identidad de la persona o personas físicas por cuya cuenta actúa.</li>
<li>En el supuesto que actúe por cuenta de una persona jurídica, el sujeto obligado deberá, con carácter previo al establecimiento de relaciones de negocio o a la ejecución de otras operaciones ocasionales por importe superior a 15.000 euros, identificar y comprobar la identidad de la persona o personas físicas que en último término posean o controlen, directa o indirectamente, un porcentaje superior al 25 por ciento del capital o de los derechos de voto de una persona jurídica, o que a través  de acuerdos o disposiciones estatutarias o por otros medios ejerzan el control, directo o indirecto, de la gestión de una persona jurídica.<br/>
Cuando no exista una persona física que posea o controle, directa o indirectamente un porcentaje superior al 25 por ciento del capital o de los derechos de boto de la persona jurídica, o que por otros medios ejerza el control, directo o indirecto, de la persona jurídica, se considerará que ejerce dicho control el administrador o administradores. Cuando el administrador designado fuera una persona jurídica, se entenderá que el control es ejercido por la persona física nombrada por el administrador persona jurídica.<br/>
La identificación y comprobación de la identidad del titular real podrá realizarse, con carácter general, mediante una declaración responsable del cliente o de la persona que tenga atribuida la representación de la persona jurídica. Será preceptiva la obtención por el sujeto obligado de documentación adicional o de información de fuentes fiables independientes cuando el cliente, el titular real o la relación de negocios o la operación presenten riesgos superiores al promedio.</li>
</ul>

<b>4.1.3. Propósito e índole de la relación de negocios.</b><br/>
Con carácter previo al inicio de la relación de negocios, los sujetos obligados deben recabar de sus clientes información a fin de conocer la naturaleza de su actividad profesional o empresarial, dejando constancia mediante el registro correspondiente.
Cuando el cliente o la relación de negocios presenten un riesgo superior al promedio así como cuando las operaciones no se correspondan con la actividad declarada por el cliente o con sus antecedentes operativos, el sujeto obligado comprobará las actividades declaradas por el cliente.<br/><br/>

<b>4.1.4. Seguimiento continuo de la relación de negocios.</b><br/>
Los sujetos obligados realizarán un escrutinio de las operaciones efectuadas a lo largo de la relación de negocio, comprobando que coincidan con la actividad profesional o empresarial del cliente y con sus antecedentes operativos. El escrutinio tendrá carácter integral, incorporando todos los productos del cliente con el sujeto obligado y, en su caso, con otras sociedades del grupo.<br/>
Los sujetos obligados revisarán periódicamente los documentos, datos e informaciones recabados al aplicar las medidas de diligencia, de forma que los mantenga actualizados y vigentes.<br/>
La periodicidad del proceso de revisión documental se determinará por el sujeto obligado en función del riesgo, si bien en clientes de riesgo superior al promedio será como mínimo anual.<br/>
Esta obligación sólo se aplica a los casos en que el sujeto obligado mantenga con el cliente una relación permanente y estable haciéndose cargo de una parte de sus asuntos.<br/><br/>

<b>4.1.5. Diligencia debida y prohibición de revelación.</b>
Independientemente de cualquier excepción, exención o umbral, si durante el establecimiento o en el curso de una relación de negocios o de la ejecución de operaciones surgieran indicios o certeza de blanqueo de capitales o de financiación del terrorismo, los sujetos obligados procederán a identificar y verificar la identidad del cliente y del titular real  con carácter previo a la realización del examen especial y de la comunicación por indicio al Servicio Ejecutivo de la Comisión de Prevención del Blanqueo de Capitales e Infracciones Monetarias (en adelante, SEPBLAC).<br/>
No obstante, en estos casos, el sujeto obligado deberá tener en cuenta el riesgo de revelación, pudiendo omitir la práctica de las medidas de diligencia debida referenciadas en el párrafo anterior si considera que revelaría al cliente o potencial cliente el   examen o comunicación de la operación.<br/>
No se podrá revelar al cliente ni a terceros el haber comunicado información al SEPBLAC. No constituye revelación el tratar de disuadir al cliente de una actividad ilegal.<br/><br/>
<b>4.1.6. Aplicación por terceros de las medidas de diligencia debida.</b><br/><br/>
Los sujetos obligados podrán recurrir a terceros sometidos a la LPBC para la aplicación de las medidas normales de diligencia debida, con excepción del seguimiento continuo de la relación de negocios.<br/><br/><br/><br/><br/>
No obstante, los sujetos obligados mantendrán la plena responsabilidad respecto de la relación de negocios u operación, aun cuando el incumplimiento sea imputable al tercero, sin perjuicio, en su caso, de la responsabilidad de éste.<br/>
Las respectivas obligaciones de las partes se incluirán en un acuerdo escrito de formalización de la aplicación por terceros de las medidas de diligencia debida.<br/>
Conforme al citado acuerdo, el sujeto obligado exigirá en todo caso al tercero:
<ul>
<li>Que le remita inmediatamente la información sobre el cliente.</li>
<li>Que le remita inmediatamente, cuando así lo solicite el sujeto obligado, copia de los documentos que acrediten la información suministrada sobre dicho cliente.<br/>
El sujeto obligado deberá comprobar que el tercero se encuentra sometido a las obligaciones en materia de prevención de blanqueo de capitales y de financiación del terrorismo y es objeto de supervisión en estas materias, adoptando medidas razonables para comprobar que cuenta con procedimientos adecuados para el cumplimiento de las medidas de diligencia debida y conservación de documentos.</li>
</ul>

<b>4.2. MEDIDAS REFORZADAS DE DILIGENCIA DEBIDA:</b><br/><br/>

Los sujetos obligados que son objeto de estudio aplicarán, <u>además</u> de las medidas normales de diligencia debida, medidas reforzadas en los siguientes supuestos:
<ul style="list-style-type:none;">
<li>a)	Relaciones de negocio y operaciones no presenciales. </li>
<li>b)	Los sujetos obligados aplicarán medidas reforzadas de diligencia debida en las relaciones de negocio u operaciones de personas con responsabilidad pública, de familiares o allegados de personas con responsabilidad pública, así como si ha desempeñado algún cargo con responsabilidad pública en los dos años anteriores.</li>
<li>c)	Relaciones de negocios y operaciones con sociedades con acciones al portador, que estén permitidas conforme a lo dispuesto en el artículo 4.4 de la LPBC.</li>
<li>d)	Relaciones de negocio y operaciones con clientes de países, territorios o jurisdicciones de riesgo o que supongan transferencia de fondos de o hacia tales países, territorios o jurisdicciones, incluyendo en todo caso, aquellos países para los que el GAFI exija la aplicación de medidas de diligencia reforzada.</li>
<li>e)	Transmisión de acciones o participaciones de sociedades preconstituidas. A estos efectos, se entenderá por sociedades preconstituidas aquellas constituidas sin actividad económica real para su posterior transmisión a terceros.</li>
<li>f)	En aquellas relaciones de negocio y operaciones que, conforme a su análisis de riesgo, el sujeto obligado determine que presentan un riesgo más elevado de blanqueo de capitales o de financiación de terrorismo.</li>
</ul>
Para la determinación de estos supuestos de riesgo superior, los sujetos obligados tendrán en consideración, entre otros, los siguientes factores:
<ul>
<li>Características del cliente: clientes no residentes en España; sociedades cuya estructura accionarial y de control no sea transparente o resulte inusual o excesivamente compleja; sociedades de mera tenencia de activos.</li>
<li>Características de la operación, relación de negocios o canal de distribución: relaciones de negocio y operaciones en circunstancias inusuales; relaciones de negocio y operaciones con clientes que empleen habitualmente medios de pago al portador; relaciones de negocio y operaciones ejecutadas a través de intermediarios.</li>
<li>Considerarán como países, territorios o jurisdicciones de riesgo los siguientes: países, territorios o jurisdicciones que no cuenten con sistemas adecuados de prevención del blanqueo de capitales y de la financiación del terrorismo; países, territorios o jurisdicciones sujetos a sanciones, embargos o medidas análogas aprobadas por la Unión Europea, las Naciones Unidas u otras organizaciones internacionales; países, territorios o jurisdicciones que presenten niveles significativos de corrupción u otras actividades criminales; aquellos en los que se facilite financiación u apoyo a actividades terroristas; aquellos que presenten un sector financiero extraterritorial significativo (centros <off-shore>); aquellos que tengan la consideración de paraísos fiscales.</li>
</ul><br/><br/>
<ul>
<li>En la determinación de los países, territorios o jurisdicciones de riesgo, los sujetos obligados recurrirán a fuentes creíbles, tales como los Informes de Evaluación Mutua del GAFI  o sus equivalentes regionales o los Informes de otros organismos internacionales. La Comisión publicará orientaciones para asistir a los sujetos obligados en la determinación del riesgo geográfico.</li>
</ul>
<b>MEDIDAS REFORZADAS</b>: <u>En los supuestos establecidos, así como en aquellos de riesgo superior al promedio</u> o en aquellos que se hubieran determinado por el sujeto obligado conforme a su análisis de riesgo, se comprobará en <b>todo caso</b> las actividades declaradas por sus clientes y la identidad del titular real.<br/><br/>
<b>Adicionalmente</b>, se aplicarán en función del riesgo, una o varias de las siguientes medidas:
<ul style="list-style-type:none;">
<li>a)	Actualizar los datos obtenidos en el proceso de aceptación del cliente.</li>
<li>b)	Obtener documentación o información adicional sobre el propósito e índole de la relación de negocios.</li>
<li>c)	Obtener documentación o información adicional sobre el origen de los fondos.</li>
<li>d)	Obtener documentación o información adicional sobre el origen del patrimonio del cliente.</li>
<li>e)	Obtener documentación o información sobre el propósito de las operaciones.</li>
<li>f)	Obtener autorización directiva para establecer o mantener la relación de negocios o ejecutar la operación.</li>
<li>g)	Realizar un seguimiento reforzado de la relación de negocio, incrementando el número y frecuencia de los controles aplicados y seleccionando patrones de operaciones para examen.</li>
<li>h)	Examinar y documentar la congruencia de la relación de negocios o de las operaciones con la documentación e información disponible sobre el cliente.</li>
<li>i)	Examinar y documentar la lógica económica de las operaciones.</li>
<li>j)	Exigir que los pagos o ingresos se realicen en una cuenta a nombre del cliente, abierta en una entidad de crédito domiciliada en la Unión Europea o en países terceros equivalentes.</li>
<li>k)	Limitar la naturaleza o cuantía de las operaciones o los medios de pago empleados.</li>
</ul>

En las <b>relaciones de negocio y operaciones no presenciales</b>: se exigirá que concurra alguno de los siguientes requisitos:
<ul style="list-style-type:none;">
<li>a)	Que la identidad del cliente quede acreditada de conformidad con lo dispuesto en la normativa aplicable sobre firma electrónica.</li>
<li>b)	Que la identidad del cliente quede acreditada mediante copia del documento de identidad, de los establecidos como "documentos fehacientes a efectos de identificación formal" –artículo 6 RLPBC-, que corresponda, siempre que dicha copia esté expedida por un fedatario público.</li>
<li>c)	Que el primer ingreso proceda de una cuenta a nombre del mismo cliente abierta en una entidad domiciliada en España, en la Unión Europea o en países terceros equivalentes.</li>
<li>d)	La identidad del cliente quede acreditada mediante el empleo de otros procedimientos seguros de identificación de clientes en operaciones no presenciales, siempre que tales procedimientos hayan sido previamente autorizados por el SEPBLAC.</li>
<li>e)	En todo caso, en el plazo de un mes desde el establecimiento de la relación de negocios no presencial, los sujetos obligados deberán obtener de estos clientes una copia de los documentos necesarios para practicar la diligencia debida.</li>
</ul>


<b>4.3. MEDIDAS SIMPLIFICADAS DE DILIGENCIA DEBIDA:</b><br/><br/>

Los sujetos obligados podrán aplicar, en función del riesgo, medias simplificadas de diligencia debida respecto de los clientes establecidos en el artículo 15 del RLPBC así como de los productos u operaciones artículo 16 del RLPBC.<br/>
Las medidas simplificadas de diligencia debida deberán ser congruentes con el riesgo, y su aplicación consiste en aplicar, en función del riesgo y en sustitución de las medidas normales de diligencia debida, una o varias de las siguientes medidas:
<br/><br/>
<ul style="list-style-type:none;"> 
<li>a)	Comprobar la identidad del cliente o del titular real únicamente cuando se supere un umbral cuantitativo con posterioridad al establecimiento de la relación de negocios.</li>
<li>b)	Reducir la periodicidad del proceso de revisión documental.</li>
<li>c)	Reducir el seguimiento de la relación de negocios y el escrutinio de las operaciones que no superen un umbral cuantitativo.</li>
<li>d)	No recabar información sobre la actividad profesional o empresarial del cliente, infiriendo el propósito y naturaleza por el tipo de operaciones o relación de negocios establecida.</li>
</ul>
No podrán aplicarse medidas simplificadas de diligencia debida o, en su caso, cesará la aplicación de las mismas, cuando concurran o surjan indicios o certeza de blanqueo de capitales o de financiación del terrorismo o riesgos superiores al promedio.<br/><br/>

<u><i>Clientes susceptibles de aplicación de medidas simplificadas de diligencia debida:</i></u>
<ul>
<li>Entidades de derecho público de los Estados miembros de la Unión Europea o de países terceros equivalentes.</li>
<li>Sociedades u otras personas jurídicas controladas o participadas mayoritariamente por entidades de derecho público de los Estados miembros de la Unión Europea o de países terceros equivalentes.</li>
<li>Entidades financieras, exceptuadas las entidades de pago, domiciliadas en la Unión Europa o en países terceros equivalentes que sean objeto de supervisión para garantizar el cumplimiento de las obligaciones de prevención del blanqueo de capitales y de la financiación del terrorismo.</li>
<li>Sucursales o filiales de entidades financieras, exceptuadas las entidades de pago, domiciliadas en la Unión Europea o en países terceros equivalentes, cuando estén sometidas por la matriz a procedimientos de prevención del blanqueo de capitales y de la financiación del terrorismo.</li>
<li>Sociedades cotizadas cuyos valores se admitan a negociación en un mercado regulado de la Unión Europea o de países terceros equivalentes así como sus sucursales y filiales participadas mayoritariamente.</li>
</ul>
<u><i>Productos u operaciones susceptibles de aplicación de medidas simplificadas de diligencia debida:</i></u>
<ul style="list-style-type:none;">
<li>a) Las pólizas de seguro de vida cuya prima anual no exceda de 1.000 euros o cuya prima única no exceda de 2.500 euros.</li>
<li>b) Los instrumentos de previsión social complementaria enumerados en el artículo 51 de la Ley 35/2006, de 28 de noviembre, del Impuesto sobre la Renta de las Personas Físicas y de modificación parcial de los Impuestos sobre Sociedades, sobre la Renta de No Residentes y sobre el Patrimonio, cuando la liquidez se encuentre limitada a los supuestos contemplados en la normativa de planes y fondos de pensiones y no puedan servir de garantía para un préstamo.</li>
<li>c) Los seguros colectivos que instrumenten compromisos por pensiones a que se refiere la disposición adicional primera del texto refundido de la Ley de Regulación de los Planes y Fondos de Pensiones, aprobado por Real Decreto Legislativo 1/2002, de 29 de noviembre, cuando cumplan los siguientes requisitos:</li>
<li>1.º Que instrumenten compromisos por pensiones que tengan su origen en un convenio colectivo o en un expediente de regulación de empleo, entendido como la extinción de las relaciones laborales en virtud de un despido colectivo del artículo 51 del texto refundido de la Ley del Estatuto de los Trabajadores, aprobado por Real Decreto Legislativo 1/1995, de 24 de marzo, o de resolución judicial adoptada en el seno de un procedimiento concursal.</li>
<li>2.º Que no admitan el pago de primas por parte del trabajador asegurado que, sumadas a las abonadas por el empresario tomador del seguro, supongan un importe superior a los límites establecidos por el artículo 52.1.b) de la Ley 35/2006, de 28 de noviembre, para los instrumentos de previsión social complementaria enumerados en su artículo 51.</li>
<li>3.º Que no puedan servir de garantía para un préstamo y no contemplen otros supuestos de rescate distintos a los excepcionales de liquidez recogidos en la normativa de planes de pensiones o a los recogidos en el artículo 29 del Real Decreto 1588/1999, de 15 de octubre, por el que se aprueba el Reglamento sobre la instrumentación de los compromisos por pensiones de las empresas con los trabajadores y beneficiarios.</li>
</ul><br/><br/>
<ul style="list-style-type:none;">
<li>d) Las pólizas del ramo de vida que garanticen exclusivamente el riesgo de fallecimiento, incluidas las que contemplen además garantías complementarias de indemnización pecuniaria por invalidez permanente o parcial, total o absoluta o incapacidad temporal, enfermedad grave y dependencia.</li>
<li>e) El dinero electrónico cuando no pueda recargarse y el importe almacenado no exceda de 250 euros o cuando, en caso de que pueda recargarse, el importe total disponible en un año natural esté limitado a 2.500 euros, salvo cuando el titular del dinero electrónico solicite el reembolso de una cantidad igual o superior a 1.000 euros en el curso de ese mismo año natural. Se excluye el dinero electrónico emitido contra entrega de los medios de pago a que se refiere el artículo 34.2.a) de la Ley 10/2010, de 28 de abril.</li>
<li>f) Los giros postales de las Administraciones Públicas o de sus organismos dependientes y los giros postales oficiales para pagos del Servicio Postal con origen y destino en el propio Servicio de Correos.</li>
<li>g) Los cobros o pagos derivados de comisiones generadas por reservas en el sector turístico que no superen los 1.000 euros.</li>
<li>h) Los contratos de crédito al consumo por importe inferior a 2.500 euros siempre que el reembolso se realice exclusivamente mediante cargo en una cuenta corriente abierta a nombre del deudor en una entidad de crédito domiciliada en la Unión Europea o en países terceros equivalentes.</li>
<li>i) Los préstamos sindicados en los que el banco agente sea una entidad de crédito domiciliada en la Unión Europea o en países terceros equivalentes, respecto de las entidades participantes que no tengan la condición de banco agente.</li>
<li>j) Los contratos de tarjeta de crédito cuyo límite no supere los 5.000 euros, cuando el reembolso del importe dispuesto únicamente pueda realizase desde una cuenta abierta a nombre del cliente en una entidad de crédito domiciliada en la Unión Europea o país tercero equivalente.</li>
</ul>
';
	return $contenido;
}

function tema5(){
	$contenido='
	<b>5.1   Examen especial.</b><br/><br/>
Los sujetos obligados analizarán  cada operación que se les encomiende, a fin de establecer un nivel de riesgo y aplicar las medidas de diligencia debida que correspondan, dejando constancia por escrito de este análisis de riesgo de cada cliente u operación para poder acreditar a las autoridades competentes que se han adoptado las medidas de diligencia adecuadas en función del riesgo.<br/><br/>
Cuando se detecten indicios o certeza de que la operación pueda estar vinculada al blanqueo de capitales o se trate de operaciones complejas, inusuales o que no tengan un motivo económico o lícito aparente, se procederá a efectuar un <b>examen especial</b> de dicha operación, dejando constancia por escrito de su resultado.<br/><br/>
El proceso de examen especial se realizará de modo estructurado, documentándose las fases de análisis, las gestiones realizadas y las fuentes de información consultadas. En todo caso, el proceso de examen especial tendrá naturaleza integral, debiendo analizar toda la operativa relacionada, todos los intervinientes en la operación y toda la información relevante obrante en el sujeto obligado y, en su caso, en el grupo empresarial.<br/><br/>

 Concluido el análisis técnico, el representante ante el Servicio Ejecutivo de la Comisión adoptará, motivadamente y sin demora, la decisión sobre si procede o no la <b>comunicación al Servicio Ejecutivo de la Comisión</b>, en función de la concurrencia en la operativa de indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo.<br/><br/>

No obstante lo dispuesto en el párrafo anterior, el procedimiento de control interno del sujeto obligado podrá prever que la decisión sea sometida, previamente, a la consideración del órgano de control interno. En estos casos, el órgano de control interno adoptará la decisión por mayoría, debiendo constar expresamente en el acta, el sentido y motivación del voto de cada uno de los miembros.<br/><br/>

Las decisiones sobre comunicación deberán responder, en todo caso, a criterios homogéneos, haciéndose constar la motivación en el expediente de examen especial.<br/><br/>

En aquellos supuestos en que la detección de la operación derive de la comunicación interna de un empleado, agente o directivo de la entidad, la decisión final adoptada sobre si procede o no la comunicación por indicio de la operación, será puesta en conocimiento del comunicante.<br/><br/>
Los sujetos obligados mantendrán un registro en el que, por orden cronológico, se recogerán para cada expediente de examen especial realizado, entre otros datos, sus fechas de apertura y cierre, el motivo que generó su realización, una descripción de la operativa analizada, la conclusión alcanzada tras el examen y las razones en que se basa. <br/><br/>

Asimismo se hará constar la decisión sobre su comunicación o no al Servicio Ejecutivo de la Comisión y su fecha, así como la fecha en que, en su caso, se realizó la comunicación.<br/><br/>

 Los sujetos obligados conservarán los expedientes de examen especial durante el plazo de diez años.<br/><br/>
<b>5.2  Operaciones susceptibles de estar relacionadas con el blanqueo de capitales o la financiación del terrorismo.</b><br/><br/>
Los sujetos obligados elaborarán y difundirán entre sus directivos, empleados y agentes una relación de operaciones susceptibles de estar relacionadas con el blanqueo de capitales o la financiación del terrorismo, que será objeto de revisión periódica.<br/><br/>
Los sujetos obligados establecerán un cauce de comunicación con los órganos de control interno, con instrucciones precisas a los directivos, empleados y agentes sobre cómo proceder en caso de detectar cualquier hecho u operación que pudiera estar relacionado con el blanqueo de capitales o la financiación del terrorismo y aprobarán un formulario orientativo del contenido mínimo que deberá incluir la comunicación interna de operaciones.<br/><br/>

Los sujetos obligados garantizarán la confidencialidad de las comunicaciones de operaciones de riesgo realizadas por los empleados, directivos o agentes y les proporcionará formación adecuada, de conformidad con lo prevenido en el artículo 39 del RLPBC.<br/><br/>
En la <b>relación de operaciones susceptibles de estar relacionadas con el blanqueo de capitales o la financiación del terrorismo</b>, se incluirán, en todo caso, entre otros, los siguientes supuestos:
<ul style="list-style-type:none;">
<li>a) Cuando la naturaleza o el volumen de las operaciones activas o pasivas de los clientes no se corresponda con su actividad o antecedentes operativos.</li>
<li>b) Cuando una misma cuenta, sin causa que lo justifique, venga siendo abonada mediante ingresos en efectivo por un número elevado de personas o reciba múltiples ingresos en efectivo de la misma persona.</li>
<li>c) Pluralidad de transferencias realizadas por varios ordenantes a un mismo beneficiario en el exterior o por un único ordenante en el exterior a varios beneficiarios en España, sin que se aprecie relación de negocio entre los intervinientes.</li>
<li>d) Movimientos con origen o destino en territorios o países de riesgo.</li>
<li>e) Transferencias en las que no se contenga la identidad del ordenante o el número de la cuenta origen de la transferencia.</li>
<li>f) Operativa con agentes que, por su naturaleza, volumen, cuantía, zona geográfica u otras características de las operaciones, difieran significativamente de las usuales u ordinarias del sector o de las propias del sujeto obligado.</li>
<li>g) Los tipos de operaciones que establezca la Comisión. Estas operaciones serán objeto de publicación o comunicación a los sujetos obligados, directamente o por medio de sus asociaciones profesionales.</li>
</ul>
Se incluirán asimismo las operaciones que, con las características anteriormente señaladas, se hubieran intentado y no ejecutado.<br/><br/>

<b>5.3  Comunicación por indicio.</b><br/><br/>
Concluido el examen especial, si se determinara por el sujeto obligado la concurrencia en la operativa de indicios o certeza de relación con el blanqueo de capitales o la financiación del terrorismo, se efectuará sin dilación la comunicación por indicio, en el soporte y formato establecido por el Servicio Ejecutivo de la Comisión.<br/><br/>

Sin perjuicio de efectuar la comunicación por indicio al Servicio Ejecutivo de la Comisión, el sujeto obligado adoptará inmediatamente medidas adicionales de gestión y mitigación del riesgo, que deberán tomar en consideración el riesgo de revelación.<br/><br/>

En las comunicaciones por indicio se incluirá información sobre la decisión adoptada o que previsiblemente se adoptará por el sujeto obligado respecto a la continuación o interrupción de la relación de negocios con el cliente o clientes que participen en la operación, así como la justificación de esta decisión.<br/><br/>
 En caso de que la no interrupción de la relación de negocios venga determinada por la necesidad de no interferir en una entrega vigilada acordada conforme a lo dispuesto en el artículo 263 bis de la Ley de Enjuiciamiento Criminal, se hará constar este hecho de forma expresa.<br/><br/>
Cuando los sujetos obligados eximidos de la obligación de nombramiento de representante ante el Servicio Ejecutivo de la Comisión realicen una comunicación por indicio, incluirán preceptivamente en dicha comunicación los datos identificativos del sujeto obligado, así como los datos identificativos y de contacto de la persona que lo represente.<br/><br/>
<b>5.4  Comunicación sistemática.</b><br/><br/>
El artículo 20 LPBC establece que los sujetos obligados comunicarán al Servicio Ejecutivo de la Comisión, con la periodicidad que se determine, las operaciones que se establezcan reglamentariamente, así como la posibilidad de exceptuar reglamentariamente de la obligación de comunicación sistemática de operaciones a determinadas categorías de sujetos obligados. <br/><br/>
El artículo 27 del RLPBC exceptúa de la obligación de comunicación sistemática a los corredores de seguros a los que se refiere el artículo 2.1  b) LPBC, a las empresas de asesoramiento financiero y a los sujetos obligados mencionados en los párrafos k) a y) de la misma Ley.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<b>5.5  Conservación de documentos.</b><br/><br/>
1. Los sujetos obligados conservarán toda la documentación obtenida o generada en aplicación de las medidas de diligencia debida, con inclusión, en particular, de las copias de los documentos fehacientes de identificación, las declaraciones del cliente, la documentación e información aportada por el cliente u obtenida de fuentes fiables independientes, la documentación contractual y los resultados de cualquier análisis efectuado, durante un periodo de <b>diez años</b> desde la terminación de la relación de negocio o la ejecución de la operación ocasional.<br/><br/>
Los sujetos obligados almacenarán las copias de los documentos fehacientes de identificación formal en soportes ópticos, magnéticos o electrónicos.<br/><br/>
	
Asimismo, podrán almacenarse en soportes ópticos, magnéticos o electrónicos las copias de los documentos acreditativos de la realización de operaciones de ingreso, retirada o traspaso de fondos desde una cuenta en una entidad de crédito y los que acrediten la orden o recepción de transferencias de fondos realizadas en entidades de pago u operaciones de cambio de moneda.<br/><br/>

Se exceptúan los sujetos obligados que, con inclusión de los agentes, ocupen a Menos de 10 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 2 millones de euros, que podrán optar por mantener copias físicas de los documentos de identificación. Esta excepción no será aplicable a los sujetos obligados integrados en un grupo empresarial que exceda dichas cifras.<br/><br/>

2. Los sujetos obligados conservarán los documentos y mantendrán registros adecuados de todas las relaciones de negocio y operaciones, nacionales e internacionales, durante un periodo de diez años desde la terminación de la relación de negocio o la ejecución de la operación ocasional. Los registros deberán permitir la reconstrucción de operaciones individuales para que puedan surtir, si fuera necesario, efecto probatorio.<br/><br/>

3. Los sujetos obligados conservarán durante un periodo de diez años los documentos en que se formalice el cumplimiento de sus obligaciones de comunicación y de control interno.<br/><br/>
<b>5.6  Requerimientos de las autoridades.</b><br/><br/>
La documentación e información obtenida o generada por los sujetos obligados podrá ser requerida por la Comisión, por sus órganos de apoyo o por cualquier otra autoridad pública o agente de la Policía Judicial de los Cuerpos y Fuerzas de Seguridad del Estado legalmente habilitado.
';
	return $contenido;
}

function tema6(){
	$contenido='
	<b>6.1  PROCEDIMIENTOS DE CONTROL INTERNO: ANÁLISIS DE RIESGO Y MANUAL DE PREVENCIÓN DEL BLANQUEO DE CAPITALES</b><br/><br/>

Los sujetos obligados aprobarán por escrito y aplicarán políticas y procedimientos adecuados de prevención del blanqueo de capitales y de la financiación del terrorismo.<br/>
Las políticas y procedimientos de prevención del blanqueo de capitales y de la financiación del terrorismo serán aprobados por el órgano de administración del sujeto obligado que, en los supuestos del artículo 2.1 n) de la Ley 10/2010, de 28 de abril, serán las organizaciones colegiales. <br/><br/>
En el supuesto de sujetos obligados cuyo volumen de negocios anual supere 50 millones de euros o cuyo balance general anual supere 43 millones de euros, los procedimientos a través de los cuales se implementen las políticas de prevención del blanqueo de capitales y la financiación del terrorismo podrán ser aprobados por el órgano de control interno.<br/><br/>
Los procedimientos de control interno se fundamentarán en un <b>previo análisis de riesgo</b> que será documentado por el sujeto obligado.<br/>
El análisis identificará y evaluará los riesgos del sujeto obligado por tipos de clientes, países o áreas geográficas, productos, servicios, operaciones y canales de distribución, tomando en consideración variables tales como el propósito de la relación de negocios, el nivel de activos del cliente, el volumen de las operaciones y la regularidad o duración de la relación de negocios.<br/>
El análisis de riesgo será revisado periódicamente y, en todo caso, cuando se verifique un cambio significativo que pudiera influir en el perfil de riesgo del sujeto obligado. Asimismo, será preceptiva la realización y documentación de un análisis de riesgo específico con carácter previo al lanzamiento de un nuevo producto, la prestación de un nuevo servicio, el empleo de un nuevo canal de distribución o el uso de una nueva tecnología por parte del sujeto obligado, debiendo aplicarse medidas adecuadas para gestionar y mitigar los riesgos identificados en el análisis.<br/>
Los procedimientos de control interno que establezcan los sujetos obligados serán documentados en un <b>manual de prevención del blanqueo de capitales y de la financiación del terrorismo.<br/><br/>
 Los corredores de comercio y los sujetos obligados comprendidos en el artículo 2.1 i) a u), ambos inclusive, que, con inclusión de los agentes, ocupen a Menos de 10 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 2 millones de euros, quedan exceptuados de las obligaciones referidas en este apartado. Esta excepción no será aplicable a los sujetos obligados integrados en un grupo empresarial que supere dichas cifras.</b><br/><br/>
El manual de prevención del blanqueo de capitales y de la financiación del terrorismo comprenderá, como mínimo, los siguientes aspectos:
<ul style="list-style-type:none;">
<li>a) La política de admisión de clientes del sujeto obligado, con una descripción precisa de los clientes que potencialmente puedan suponer un riesgo superior al promedio por disposición normativa o porque así se desprenda del análisis de riesgo, y de las medidas a adoptar para mitigarlo, incluida, en su caso, la negativa a establecer relaciones de negocio o a ejecutar operaciones o la terminación de la relación de negocios.</li>
<li>b) Un procedimiento estructurado de diligencia debida que incluirá la periódica actualización de la documentación e información exigibles. La actualización será, en todo caso, preceptiva cuando se verifique un cambio relevante en la actividad del cliente que pudiera influir en su perfil de riesgo.</li>
<li>c) Un procedimiento estructurado de aplicación de las medidas de diligencia debida a los clientes existentes en función del riesgo que tendrá en cuenta, en su caso, las medidas aplicadas previamente y la adecuación de los datos obtenidos.</li>
<li>d) Una relación de hechos u operaciones que, por su naturaleza, puedan estar relacionados con el blanqueo de capitales o la financiación del terrorismo, estableciendo su periódica revisión y difusión entre los directivos, empleados y agentes del sujeto obligado.</li>
<li>e) Una descripción detallada de los flujos internos de información, con instrucciones precisas a los directivos, empleados y agentes del sujeto obligado sobre cómo proceder en relación con los hechos u operaciones que, por su naturaleza, puedan estar relacionados con el blanqueo de capitales o la financiación del terrorismo.</li>
<li>f) Un procedimiento para la detección de hechos u operaciones sujetos a examen especial, con descripción de las herramientas o aplicaciones informáticas implantadas y de las alertas establecidas.</li>
</ul>
<br/><br/>
<ul style="list-style-type:none;">
<li>g) Un procedimiento estructurado de examen especial que concretará de forma precisa las fases del proceso de análisis y las fuentes de información a emplear, formalizando por escrito el resultado del examen y las decisiones adoptadas.</li>
<li>h) Una descripción detallada del funcionamiento de los órganos de control interno, que incluirá su composición, competencias y periodicidad de sus reuniones.</li>
<li>i) Las medidas para asegurar el conocimiento de los procedimientos de control interno por parte de los directivos, empleados y agentes del sujeto obligado, incluida su periódica difusión y la realización de acciones formativas de conformidad con un plan anual.</li>
<li>j) Las medidas a adoptar para verificar el cumplimiento de los procedimientos de control interno por parte de los directivos, empleados y agentes del sujeto obligado.</li>
<li>k) Los requisitos y criterios de contratación de agentes, que deberán obedecer a lo dispuesto en el artículo 37.2 RLPBC.</li>
<li>l) Las medidas a adoptar para asegurarse de que los corresponsales del sujeto obligado aplican procedimientos adecuados de prevención del blanqueo de capitales y de la financiación del terrorismo.</li>
<li>m) Un procedimiento de verificación periódica de la adecuación y eficacia de las medidas de control interno. En los sujetos obligados que dispongan de departamento de auditoría interna corresponderá a éste dicha función de verificación.</li>
<li>n) La periódica actualización de las medidas de control interno, a la luz de los desarrollos observados en el sector y del análisis del perfil de negocio y operativa del sujeto obligado.</li>
<li>ñ) Un procedimiento de conservación de documentos que garantice su adecuada gestión e inmediata disponibilidad.</li>
</ul>
-  Los procedimientos de control interno deberán permitir al sujeto obligado:
<ul style="list-style-type:none;">
<li>a) Centralizar, gestionar, controlar y almacenar de modo eficaz la documentación e información de los clientes y de las operaciones que se realicen.</li>
<li>b) Verificar la efectiva aplicación de los controles previstos y reforzarlos en caso necesario.</li>
<li>c) Adoptar y aplicar medidas reforzadas para gestionar y mitigar los riesgos más elevados.</li>
<li>d) Agregar las operaciones realizadas a fin de detectar potenciales fraccionamientos y operaciones conectadas.</li>
<li>e) Determinar, con carácter previo, si procede el conocimiento y verificación de la actividad profesional o empresarial del cliente.</li>
<li>f) Detectar cambios en el comportamiento operativo de los clientes o inconsistencias con su perfil de riesgo.</li>
<li>g) Impedir la ejecución de operaciones cuando no consten completos los datos obligatorios del cliente o de la operación.</li>
<li>h) Impedir la ejecución de operaciones por parte de personas o entidades sujetas a prohibición de operar.</li>
<li>i) Seleccionar para su análisis operaciones en función de alertas predeterminadas y adecuadas a su actividad.</li>
<li>j)  Mantener una comunicación directa del órgano de control interno con la red comercial.</li>
<li>k) Atender de forma rápida, segura y eficaz los requerimientos de documentación e información de la Comisión, de sus órganos de apoyo o de cualquier otra autoridad pública legalmente habilitada.</li>
<li>l) Cumplimentar la comunicación sistemática de operaciones al Servicio Ejecutivo de la Comisión o, en su caso, la comunicación semestral negativa.</li>
</ul><br/><br/><br/><br/><br/><br/><br/><br/>
<b>6.2   REPRESENTANTE ANTE EL SEPBLAC.</b><br/><br/>
       Los sujetos obligados designarán un representante ante el Servicio Ejecutivo de la Comisión, que será responsable del cumplimiento de las obligaciones de información establecidas en la Ley 10/2010, de 28 de abril. El representante podrá designar, asimismo, hasta dos personas autorizadas que actuarán bajo la dirección y responsabilidad del representante ante el Servicio Ejecutivo de la Comisión.<br/><br/>
La propuesta de nombramiento del representante y, en su caso, de los autorizados, acompañada de una descripción detallada de su trayectoria profesional, será comunicada al Servicio Ejecutivo de la Comisión que, de forma razonada, podrá formular reparos u observaciones. Asimismo, se comunicará al Servicio Ejecutivo de la Comisión el cese o sustitución del representante o personas autorizadas cuando tenga carácter disciplinario.<br/><br/>
<b>Los corredores de comercio y los sujetos obligados comprendidos en el artículo 2.1 i) a u), ambos inclusive, que, con inclusión de los agentes, ocupen a Menos de 10 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 2 millones de euros, quedan exceptuados de la obligación de designación y comunicación al SEPBLAC de representante ante el mismo. Esta excepción no será aplicable a los sujetos obligados integrados en un grupo empresarial que supere dichas cifras.</b><br/><br/>

<b>6.3  ÓRGANO DE CONTROL INTERNO.</b><br/><br/>
Los sujetos obligados establecerán un órgano de control interno responsable de la aplicación de los procedimientos de prevención del blanqueo de capitales y de la financiación del terrorismo.<br/>
La constitución de un órgano de control interno <b>no será preceptiva en los sujetos obligados comprendidos en el artículo 2.1 i) y siguientes y en los corredores de comercio cuando, con inclusión de los agentes, ocupen a menos de 50 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 10 millones de euros</b>, desempeñando en tales casos sus funciones el representante ante el Servicio Ejecutivo de la Comisión. Esta excepción no será aplicable a los sujetos obligados integrados en un grupo empresarial que supere dichas cifras.<br/><br/>


<b>6.4  UNIDAD TÉCNICA PARA EL TRATAMIENTO Y ANÁLISIS DE LA INFORMACIÓN.</b><br/><br/>
 
Los sujetos obligados, cuyo volumen de negocios anual exceda de 50 millones de euros o cuyo balance general anual exceda de 43 millones de euros, contarán con una unidad técnica para el tratamiento y análisis de la información.<br/>
La unidad técnica deberá contar con personal especializado, en dedicación exclusiva y con formación adecuada en materia de análisis.<br/><br/>

<b>6.5  EXAMEN POR EXPERTO EXTERNO.</b><br/><br/>
       Las medidas de control interno de los <u>sujetos obligados que no sean empresarios ni profesionales individuales</u>, serán objeto de examen anual por un experto externo.
Los informes de experto externo describirán y valorarán las medidas de control interno de los sujetos obligados a una fecha de referencia, de conformidad con lo dispuesto en el artículo 28 de la LPBC.<br/>
Los informes deberán emitirse, en todo caso, dentro de los dos meses siguientes a la fecha de referencia.<br/>
Los órganos de administración del sujeto obligado adoptarán sin dilación las medidas necesarias para solventar las deficiencias identificadas en los informes de experto externo.<br/>
En el caso de deficiencias que no sean susceptibles de resolución inmediata, los órganos de administración del sujeto obligado adoptarán, expresamente, un plan de remedio, que establecerá un calendario preciso para la implantación de las medidas correctoras. Dicho calendario no podrá exceder, con carácter general, de un año natural.<br/>
El examen externo incluirá todas las sucursales y filiales con participación mayoritaria del sujeto obligado. En relación con las sucursales y filiales situadas en países terceros, el experto verificará específicamente el efectivo cumplimiento de lo dispuesto en el artículo 31 de la LPBC.<br/>
<b>Los corredores de comercio y los sujetos obligados comprendidos en el artículo 2.1 i) a u), ambos inclusive, que, con inclusión de los agentes, ocupen a Menos de 10 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 2 millones de euros, quedan exceptuados de la obligación de someterse a examen por experto externo. Estas excepciones no serán aplicables a los sujetos obligados integrados en un grupo empresarial que supere dichas cifras.</b><br/><br/>
<br/><br/><br/><br/><br/><br/>
<b>6.6. FORMACIÓN</b><br/><br/>
Los sujetos obligados aprobarán un plan anual de formación en materia de prevención del blanqueo de capitales y de la financiación del terrorismo.<br/>
El plan de formación se fundamentará en los riesgos identificados y preverá acciones formativas específicas para los directivos, empleados y agentes del sujeto obligado. Tales acciones formativas, que deberán ser apropiadamente acreditadas, serán congruentes con el grado de responsabilidad de los receptores y el nivel de riesgo de las actividades que desarrollen.<br/>
 Anualmente, los sujetos obligados documentarán el grado de cumplimiento del plan de formación.<br/>
El examen por experto externo valorará la adecuación de las acciones formativas realizadas por el sujeto obligado.<br/>

<b>Los corredores de comercio y los sujetos obligados comprendidos en el artículo 2.1 i) a u), ambos inclusive, que, con inclusión de los agentes, ocupen a Menos de 10 personas y cuyo volumen de negocios anual o cuyo balance general anual no supere los 2 millones de euros, quedan exceptuados de la obligación de aprobar anualmente un plan de formación en materia de prevención del blanqueo de capitales y de la financiación del terrorismo para los directivos, empleados y agentes, <u>debiendo acreditar,  en su lugar,  que ha recibido formación externa adecuada para el ejercicio de sus funciones sólo el representante ante el SEPBLAC.</u></b><br/><br/>


<b>6.7. ALTOS ESTÁNDARES ÉTICOS EN LA CONTRATACIÓN DE DIRECTIVOS, EMPLEADOS Y AGENTES.</b><br/><br/>
Los sujetos obligados establecerán por escrito y aplicarán políticas y procedimientos adecuados para asegurar altos estándares éticos en la contratación de empleados, directivos y agentes. <br/><br/>

A estos efectos, se aplicarán a estos colectivos los criterios de idoneidad fijados por la normativa sectorial que les resulte de aplicación.<br/><br/>

En defecto de normativa específica, para la determinación de la concurrencia de altos estándares éticos en directivos, empleados o agentes del sujeto obligado, se tomará en consideración su trayectoria profesional, valorándose la observancia y respeto a las leyes mercantiles u otras que regulen la actividad económica y la vida de los negocios, así como a las buenas prácticas del sector de actividad de que se trate. <br/><br/>

No se considerará que concurren altos estándares éticos cuando el empleado, directivo o agente:
<ul style="list-style-type:none;">
<li>a) Cuente con antecedentes penales no cancelados ni susceptibles de cancelación por delitos dolosos contra el patrimonio, y contra el orden socioeconómico, contra la Hacienda Pública y Seguridad Social, delitos contra la Administración Pública y falsedades.</li>
<li>b) Haya sido sancionado mediante resolución administrativa firme con la suspensión o separación del cargo por infracción de la LPBC. Esta circunstancia se apreciará durante el tiempo que se prolongue la sanción.</li>
</ul>
<b>6.8. MEDIDAS DE CONTROL INTERNO A NIVEL DE GRUPO</b><br/><br/>
       Los sujetos obligados aprobarán y aplicarán en sus sucursales y filiales con participación mayoritaria situadas en terceros países medidas de prevención del blanqueo de capitales y de la financiación del terrorismo al menos equivalentes a las establecidas por el derecho comunitario.<br/><br/>
En estas políticas se incluirán, en todo caso, los procedimientos para la transmisión de información entre los miembros del grupo, estableciendo las cautelas adecuadas en relación con el uso de la información transmitida. Cuando el intercambio de información se haga con países que no ofrezcan un nivel de protección adecuado de conformidad con lo dispuesto en la normativa de protección de datos, será precisa la autorización de la transferencia internacional de datos por parte de la Agencia Española de Protección de Datos, en los términos establecidos en la Ley Orgánica 15/1999, de 13 de diciembre, y su normativa de desarrollo.<br/><br/>

Los procedimientos de control interno, se establecerán a nivel de grupo, siendo aplicables a todas las sucursales y filiales domiciliadas en España con participación mayoritaria del sujeto obligado.<br/><br/>

Los procedimientos de control interno a nivel de grupo deberán tener en cuenta los diferentes sectores de actividad, modelos de negocio y perfiles de riesgo y preverán los intercambios de información necesarios para una gestión integrada del riesgo. En particular, los órganos de control interno del grupo deberán tener acceso, sin restricción alguna, a cualquier información obrante en las filiales o sucursales que sea precisa para el desempeño de sus funciones de prevención del blanqueo de capitales y de la financiación del terrorismo.<br/><br/><br/><br/>

 A estos efectos, resulta de aplicación la definición de grupo recogida en el artículo 42 del Código de Comercio.<br/><br/>

Para la aplicación al grupo empresarial de los umbrales previstos en las excepciones de los artículos 31 y siguientes del RLPBC, se tendrán en consideración únicamente aquellas filiales o sucursales del grupo que tengan la consideración de sujetos obligados conforme al artículo 2.1 de la LPBC.<br/><br/>
<b>6.9. MEDIDAS DE CONTROL INTERNO DE APLICACIÓN A LOS AGENTES</b><br/><br/>
Los sujetos obligados, sin perjuicio de su responsabilidad directa, se asegurarán del efectivo cumplimiento por parte de sus agentes de las obligaciones de prevención del blanqueo de capitales y de la financiación del terrorismo.<br/>
A estos efectos, los sujetos obligados incluirán a los agentes en el ámbito de aplicación de sus procedimientos de control interno. Dichos procedimientos preverán, en particular, mecanismos específicos de seguimiento y control de las actividades de los agentes que se adaptarán al nivel de riesgo existente en función de las características concretas de la relación de agencia.<br/>
En aquellos supuestos en los que el sujeto obligado determine que un agente ha incumplido grave o sistemáticamente los procedimientos de control interno, deberá poner fin al contrato de agencia, procediendo a examinar la operativa del agente de conformidad con lo dispuesto en el artículo 17 de la LPBC.<br/>
Los procedimientos de control interno establecerán mecanismos específicos que garanticen la aplicación de altos estándares éticos en la contratación de agentes.<br/><br/>
La operativa de los nuevos agentes será objeto de seguimiento reforzado por parte del sujeto obligado.<br/>
Los sujetos obligados mantendrán a disposición de la Comisión, de sus órganos de apoyo o de cualquier otra autoridad pública legalmente habilitada una relación completa y actualizada de sus agentes, que incluirá todos los datos necesarios para su adecuada identificación y localización.<br/>
Lo dispuesto en el presente artículo será igualmente aplicable a las personas o entidades no residentes que desarrollen en España, a través de agentes, actividades sujetas a obligaciones de prevención del blanqueo de capitales y de la financiación del terrorismo.
';
	return $contenido;
}
//Fin parte de servicios
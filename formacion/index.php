<?php
  $seccionActiva=54;
  include_once('../cabecera.php');
  
  //operacionesConvenios();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
          //creaBotonDescarga('../documentos/curso.zip','Descargar Curso PBLC');
          //creaBotonesGestion();
        ?>
		    <div class="span12">        
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Temas</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <th> Nº </th>
                  <th> Temas </th>
                  <th class="centro"></th>
                </tr>
              </thead>
              <tbody>

                <?php
                  imprimeTemas();
                ?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    //listadoTabla('#tablaServicios','../listadoAjax.php?include=convenios&funcion=listadoConvenios();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaServicios');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
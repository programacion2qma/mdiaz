<?php
  $seccionActiva=554;
  include_once("../cabecera.php");
  gestionExamen();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();

	$('button[type=submit]').unbind();
	$('button[type=submit]').click(function(e){
		e.preventDefault();
		var enviar='SI';
		if($('#alumno').val()=='' || $('#dni').val()==''){
			enviar='NO';
			alert('No puedes enviar el examen sin rellenar los campos Alumno y NIF');
		}
		for($i=1;$i<=20;$i++){
			if($('input[name=pregunta'+$i+']:checked').val()==undefined){
				enviar='NO';
				alert('Debes responder todas las preguntas');
				break;
			}
		}
		if(enviar=='SI'){
			$('form').submit();
		}
	})
});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
    include_once("funciones.php");
    compruebaSesion();

    $res=generaTema($_GET['tema']);
    $contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: Calibri;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        #cabecera{
	        	margin-bottom:20px;
	    	}

	        #datosCabecera{
	        	width:50%;
	        	position:absolute;
	        	right:-20px;
	        	top:20px;
	    	}

	    	#datosCabecera td{
	    		padding-right:10px;
	    	}

	    	#datosCabecera .dato{
	    		border-bottom:1px solid #000;
	    		width:200px;
	    		padding:0px;
	    		text-align:right;
	    	}

	    	.titulo{
	    		font-size:17px;
	    		font-weight:bold;
	    		text-align:center;
	    		color:#15284B;
	    		background:#EDF1F9;
	    		padding:10px;
	    	}

	    	h1{
	    		color:#15284B;
	    		font-size:12px;
	    		text-decoration:underline;
	    		text-align:center;
	    		margin-bottom:10px;
	    		margin-top:15px;
	    	}
	    	
	    	p{
	    		line-height: 20px;
	    		margin-bottom:3px;
	    		margin-top:3px;
	    		text-align: justify;
	    	}

	    	#tablaFirmas{
	    		width:100%;
	    		margin-top:100px;
	    	}

	    	#tablaFirmas td{
	    		width:50%;
	    		text-align:center;
	    	}

	    	ul{
	    		margin-left:-50px;
	    	}

	    	ul li{
	    		padding-left:22px;
	    		padding-top:5px;
	    	}

	    	ul .marcado{
	    		background:url(../img/check.png) left top no-repeat;
	    	}

	    	.logo{
	    		width:100px;
	    	}

	    	#tablaCabecera{
	    		border:1px solid #000;
	    		border-collapse:collapse;
	    		width:82%;
	    		margin-left:70px;
	    	}

	    	#tablaCabecera td{
	    		border:1px solid #000;
	    		text-align:center;
	    	}

	    	#tablaCabecera .lado{
	    		width:20%;
	    	}
	    	#tablaCabecera .centro{
	    		width:60%;
	    	}
	    	#tablaCabecera .completa{
	    		width:100%;
	    	}
	-->
	</style>
	<page footer='page' backtop='22mm' backbottom='18mm' backleft='18mm' backright='18mm'><br/><br/>
		<page_header>
			<table id='tablaCabecera'>
				<tr>
					<td rowspan='2' class='lado'></td>
					<td class='centro'></td>
					<td rowspan='2' class='lado'><img class='logo' src='../img/logoMDiaz.png' alt='M&D Asesores'></td>
				</tr>
				<tr>
					<td class='centro' style='border-left:0px;'><b>Prevención de Blanqueo de capitales</b></td>
				</tr>
				<tr>
					<td colspan='3' class='completa'>".$res['titulo']."</td>
				</tr>
			</table>
		</page_header>
		".$res['contenido']."
	<page_footer>
	Documento generado por UVED Asesores Consultores
	</page_footer>
	</page>";

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->setDefaultFont("calibri");
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Tema_'.$_GET['tema'].'.pdf');
?>
<?php
  $seccionActiva = 61;
  $codigo = 7;
  include_once('../cabecera.php');  
  
  if ($_SESSION['tipoUsuario'] == 'CLIENTE') {
    $codigoCliente = consultaBD("SELECT * FROM usuarios_clientes WHERE codigoUsuario = ".$_SESSION['codigoU'].";", true, true);
    $codigoCliente = $codigoCliente['codigoCliente']; 
  }
  else {
    $codigoCliente = false;    
  }
  
  $res = operacionesEvaluaciones();
  $estadisticas = estadisticasEvaluacionesImpacto($codigoCliente);

?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre evaluación de riesgos:</h6>
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-dashboard"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Registradas</div>
                    <div class="stat"> <i class="icon-dashboard"></i> <span class="value"><?php echo $estadisticas['aceptable']?></span> <br>Aceptables</div>
                    <div class="stat"> <i class="icon-dashboard"></i> <span class="value"><?php echo $estadisticas['noAceptable']?></span> <br>No aceptables</div>
                  </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Evaluaciones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">                
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Registor de evaluaciones de impacto</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <?php if($_SESSION['tipoUsuario']!='CLIENTE'){?>
                    <th> Cliente </th>
                  <?php } ?>
                  <th> Ref </th>
                  <th> Fecha de evaluación previa </th>
                  <th> Fecha de evaluación de impacto </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
                  imprimeEvaluacionesRiesgo($codigoCliente);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>


<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/excanvas.min.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>
<script type="text/javascript">
  
</script>


<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
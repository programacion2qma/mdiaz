<?php
  $seccionActiva=3;
  include_once('../cabecera.php');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span3"></div>

        <div class="span6 margenAb">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class='icon-edit'></i><i class='icon-chevron-right'></i><i class="icon-search-plus"></i>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Por favor, seleccione un cliente del listado:</h6>
                    <form action="gestion.php" method="post" class="centro noAjax">
                      <?php
                        if($_SESSION['tipoUsuario'] == 'ADMIN'){
                            $sql="SELECT codigo, razonSocial AS texto FROM clientes WHERE posibleCliente = 'NO' ORDER BY razonSocial;";
                          } else {
                              $sql="SELECT codigo, razonSocial AS texto FROM clientes WHERE empleado LIKE ".$_SESSION['codigoU']." AND posibleCliente = 'NO' ORDER BY razonSocial;";
                          }

                        campoSelectConsulta('cliente','Cliente',$sql);                      
                        //campoSelectConsulta('codigoCliente','',"SELECT codigo, empresa AS texto FROM clientes ORDER BY empresa;",false,'selectpicker span4 show-tick',"data-live-search='true'",'',2);
                      ?>
                      <br />
                      <a href="index.php" class="btn btn-default"><i class="icon-chevron-left"></i> Volver</a>
                      <button type="submit" class="btn btn-propio">Continuar <i class="icon-chevron-right"></i></button>
                    </form>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('select').selectpicker();
  });
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
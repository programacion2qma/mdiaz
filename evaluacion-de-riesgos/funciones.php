<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de evaluación de riesgos


function operacionesEvaluaciones(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res = $res && actualizaEvaluacionRiesgo();
	}
	elseif(isset($_POST['fechaEvaluacion'])){
		$res = $res && creaEvaluacionRiesgo();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res = $res && eliminaDatos('evaluacion_general');
	}

	mensajeResultado('fechaEvaluacion',$res,'Evaluación');
    mensajeResultado('elimina',$res,'Evaluación', true);
}

function estadisticasEvaluacionesImpacto($codigoCliente) {
	
	$res = array(
		'total'       => 0,
		'aceptable'   => 0,
		'noAceptable' => 0
	);
	
	$where = $codigoCliente ? " WHERE codigoCliente = ".$codigoCliente : " WHERE 1 = 1";
	
	conexionBD();

	$consulta = consultaBD("SELECT count(*) AS valor FROM evaluacion_general".$where.";", false, true);
	$res['total'] = $consulta['valor'];

	$consulta = consultaBD("SELECT count(*) AS valor FROM evaluacion_general".$where." AND resultado = 'aceptable';", false, true);
	$res['aceptable'] = $consulta['valor'];

	$consulta = consultaBD("SELECT count(*) AS valor FROM evaluacion_general".$where." AND resultado = 'no_aceptable';", false, true);
	$res['noAceptable'] = $consulta['valor'];

	cierraBD();

	return $res;
}

function gestionEvaluacion(){
	
	operacionesEvaluaciones();	

	$pagina = isset($_POST['pagina']) ? $_POST['pagina'] : 1;

	$pestanias = array(
		'Fecha Evaluación de Impacto',
		'Ciclo de vida de los datos',
		'Análisis necesidad/proporcionalidad',
		'Riesgos del tratamiento/evaluación',
		'Resultado'
	);

	abreVentanaGestion('Gestión de Evaluaciones de Riesgo', '?','','icon-edit','',true,'noAjax');

		$datos = compruebaDatos('evaluacion_general');
		$codigoReducida = "NULL";
		$codigoCliente  = "NULL";

		if(!$datos && isset($_GET['codigoPrevia'])) {
			$codigoReducida = $_GET['codigoPrevia'];
			$consulta = consultaBD("SELECT codigoCliente FROM evaluaciones_reducidas WHERE codigo = ".$codigoReducida.";", true, true);
			$codigoCliente  = $consulta['codigoCliente'];			
		}

		campoOculto($pagina, 'pagina');
	
		creaPestaniasAPI($pestanias);

			abrePestaniaAPI(1, $pagina == 1);
		
				echo "									
					<div style='text-align:justify; padding:10px;'>
						<p>	
							Es posible que el tratamiento de datos personales no precise una evaluación de impacto, pero aún así desee llevarla a cabo. A continuación, se realizará dicha evaluación mediante el correspondiente análisis del ciclo de vida de los datos del tratamiento, de la necesidad y proporcionalidad del mismo así como de los riesgos del tratamiento.
						</p>
						<p>
							La Evaluación de Impacto en la Protección de Datos Personales permite evaluar anticipadamente cuáles son los potenciales riesgos a los que están expuestos los datos personales en función de las actividades de tratamiento que se llevan a cabo con los mismos.
						</p>						
						<p>
							El análisis de riesgos para un determinado tratamiento permite identificar los riesgos a los que están expuestos los datos personales de los interesados y establecer las medidas necesarias para reducirlos hasta un nivel de riesgo aceptable.
						</p>
						<p>
							El RGPD prevé que las Evaluaciones de Impacto se lleven a cabo antes del tratamiento en los casos en que sea probable que exista un alto riesgo para los derechos y libertades de los afectados.
						</p>
						<p>
							Es posible que el tratamiento de datos personales no requiera por imperativo legal una evaluación de impacto y, aún así, desee llevarla a cabo.
						</p>
				 		<p>
							A continuación, se realizará dicha evaluación mediante el  análisis del ciclo de vida de los datos del tratamiento, de la necesidad y proporcionalidad del mismo así como de los riesgos del tratamiento, si bien,  previamente a la presente Evaluación de Impacto, deberá haber realizado la correspondiente Evaluación Previa de las actividades de tratamiento objeto de análisis, constituyendo la misma el primer paso de la presente Evaluación de Impacto.
						</p>				 		
					</div>
					<br>
				";
				
				echo "
					<fieldset class='span3'></fieldset>
				";
				abreColumnaCampos();					
					campoOculto($datos, 'codigoEvaluacionReducida', $codigoReducida);
					campoOculto($datos, 'codigoCliente', $codigoCliente);
					campoOculto($datos, 'referencia', '');
					campoFecha('fechaEvaluacion', 'Fecha de la Evaluación de Impacto', $datos);
				cierraColumnaCampos();					
				echo "
					<fieldset class='span3'></fieldset>
					<fieldset class='sinFlotar'></fieldset>
				";

				botonesNav(2);
			cierraPestaniaAPI();

			abrePestaniaAPI(2, $pagina == 2);
				tablaCicloVida($datos);
				botonesNav(3, 2);
			cierraPestaniaAPI();

			abrePestaniaAPI(3, $pagina == 3);

				$valoresSelect = [0, 1, 2, 3, 4, 5];
				$nombresSelect = [
					'Consentimiento', 
					'Relación contractual', 
					'Intereses vitales del interesado o  de otras personas', 
					'Obligación legal para el responsable', 
					'Interés público o ejercicio de poderes públicos',
					'Intereses legítimos prevalentes del responsable o de terceros'
				];

				abreColumnaCampos('span6');
					campoSelect('legitimacion', 'Legitimación', $nombresSelect, $valoresSelect, $datos,'selectpicker span6 show-tick');
				cierraColumnacampos();
				
				abreColumnaCampos('span6');
					areaTexto('obsLegitimacion', 'Observaciones', $datos, 'areaTexto span6');
				cierraColumnacampos(true);				
				
				tablaIntereses($datos);
				botonesNav(4, 3);
			cierraPestaniaAPI();

			abrePestaniaAPI(4, $pagina == 4);
				echo "									
					<div style='text-align:justify; padding:10px;'>
						<p>
							EN PRIMER LUGAR, SELECCIONE LAS AMENAZAS Y LAS MEDIDAS DE CONTROL QUE CONSIDERE NECESARIAS PARA MITIGAR EL RIESGO DE LA AMENAZA QUE ESTIME PUEDE PONER EN RIESGO LOS DERECHOS DE LOS INTERESADOS/AS AL TRATAR SUS DATOS PERSONALES.
							EN SEGUNDO LUGAR, DEBE SELECCIONAR LA PROBABILIDAD Y EL IMPACTO DE QUE SUCEDA CADA AMENAZA SELECCIONADA.
							A CONTINUACIÓN, DEBE EVALUAR EL RIESGO RESIDUAL RESPECTO A CADA AMENAZA SELECCIONADA.
						</p>
					</div>
					<br>
				";

				tablaAmenazas($datos);

				botonesNav(5, 4);
			cierraPestaniaAPI();

			abrePestaniaAPI(5, $pagina == 5);

				campoOculto($datos, 'resultado');
				echo "
					<div id='textoResultado'>
					</div>
				";

				botonesNav(false, 4);
				
			cierraPestaniaAPI();

		cierraPestaniasAPI();

	cierraVentanaGestion('index.php');
}

function imprimeEvaluacionesRiesgo($codigoCliente = false){	
	global $_CONFIG;
	
	$enlaceGestion = "";
	$where = "";
	if ($codigoCliente) {
		$where = " WHERE eg.codigoCliente = ".$codigoCliente;
	}
	
	$sql = "SELECT 
				c.razonSocial,
				DATE_FORMAT(er.fecha, '%d/%m/%Y') as fechaPrevia,
				eg.*,
				CONCAT(eg.referencia, '/', DATE_FORMAT(eg.fechaEvaluacion, '%y')) AS ref 
			FROM 
				evaluacion_general eg  
			LEFT JOIN clientes c ON
				c.codigo = eg.codigoCliente
			LEFT JOIN evaluaciones_reducidas er ON
				er.codigo = eg.codigoEvaluacionReducida
			$where
			ORDER BY 
				fechaEvaluacion;";	
		
	$consulta = consultaBD($sql, true);

	while ($datos = mysql_fetch_assoc($consulta)) {			
		echo "
			<tr>
		";
		if(!$codigoCliente) {
			echo "
				<td>".$datos['razonSocial']." </td>
			";
		}
		echo "
				<td>".$datos['ref']."</td>
				<td>".$datos['fechaPrevia']."</td>
				<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
				<td class='centro'>
					<div class='btn-group'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
				  			<ul class='dropdown-menu' role='menu'>
								<li><a href='".$_CONFIG['raiz']."evaluacion-de-riesgos/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
								<li class='divider'></li>
								<li><a href='".$_CONFIG['raiz']."evaluacion-de-riesgos/generaDocumento.php?codigo=".$datos['codigo']."' class='noAjax'><i class='icon-download'></i> Descargar</i></a></li> 
							</ul>
					</div>					
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
				</td>
			</tr>
		";
	}		
}

function creaEvaluacionRiesgo() {

	preparaCuestionarios();
	
	$_POST['referencia'] = calculaReferencia();

	$res = insertaDatos('evaluacion_general');

	return $res;
}

function actualizaEvaluacionRiesgo(){

	preparaCuestionarios();

	$res=actualizaDatos('evaluacion_general');
	
	return $res;
}

function preparaCuestionarios() {

	$datos = arrayFormulario();

	$formularioCicloVida = "";
	$formularioIntereses = "";
	$formularioAmenazas  = "";	

	foreach($datos as $campo => $valor) {
		if(strpos($campo, 'preguntaCicloVida') !== false || 
		   strpos($campo, 'obsCicloVida')      !== false) {
			$formularioCicloVida .= $campo.'=>'.$valor.'&{}&';
		}
		else if(strpos($campo, 'preguntaIntereses') !== false || 
		        strpos($campo, 'obsIntereses')      !== false) {
		 	$formularioIntereses .= $campo.'=>'.$valor.'&{}&';
	 	}
		else if(strpos($campo, 'amenaza')       !== false || 
				strpos($campo, 'medida')        !== false ||
				strpos($campo, 'probabilidad')  !== false ||
				strpos($campo, 'impacto')       !== false ||
				strpos($campo, 'riesgoR')       !== false) {
			$formularioAmenazas .= $campo.'=>'.$valor.'&{}&';
		}
	}

	$_POST['formularioCicloVida'] = substr($formularioCicloVida, 0, -4);
	$_POST['formularioIntereses'] = substr($formularioIntereses, 0, -4);
	$_POST['formularioAmenazas']  = substr($formularioAmenazas,  0, -4);

}

function calculaReferencia() {

	$fecha = date('Y', strtotime($_POST['fechaEvaluacion']));

	$consulta = consultaBD("SELECT IFNULL(MAX(referencia), 0) + 1 AS valor FROM evaluacion_general WHERE DATE_FORMAT(fechaEvaluacion, '%Y') = '".$fecha."';", true, true);

	$res = $consulta['valor'];

	return $res;
}

function botonesNav($siguiente = false, $anterior = false) {

	if (!!$siguiente){
		echo "
			<br clear='all'><br/>
			<a href='#".$siguiente."' style='float:right;' class='btn btn-propio btnSiguiente noAjax' data-toggle='tab'>Siguiente <i class='icon-arrow-right'></i></a>
		";
	}	
	if(!!$anterior){
		echo "
			<a href='#".$anterior."' style='float:left;' class='btn btn-propio btnAnterior noAjax' data-toggle='tab'><i class='icon-arrow-left'></i> Anterior</a>
		";
	}
}


function tablaCicloVida($datos) {

	$datosCicloVida = recuperaFormularios($datos, 'formularioCicloVida');	

	$i = 0;

	$filas = array(		
		"1. CAPTURA DE DATOS" => array(
			"1.1	Actividades de los procesos de captura de datos<br>
			*Detallar actividades, tareas o acciones que se realizan con la finalidad de recabar los datos necesarios para el tratamiento.",
			"1.2	Categorías de datos adquiridos<br>
			* Listar las categorías de datos que se han recolectado para el tratamiento.",
			"1.3	Intervinientes en la captura de los datos<br>
			* Registrar si los datos han sido proporcionados únicamente por el interesado, por la misma entidad o si hay terceros que aporten la información.",
			"1.4	Tecnologías aplicadas<br>
			* Elaborar un listado completo de las tecnologías/métodos utilizados para recabar los datos de carácter personal tratados."
		),
		"2.	CLASIFICACIÓN/ALMACENAJE" => array(
			"2.1 Actividades de almacenamiento y clasificación<br>
			* Enumerar las actividades llevadas a cabo en el proceso de clasificación/almacenaje de la información.",
			"2.2 Categorías de datos almacenados o clasificados<br>
			* Realizar un inventario de los tipos de datos de carácter personal a clasificar/almacenar.",
			"2.3 Intervinientes en el proceso de almacenamiento y clasificación<br>
			* Registrar si en el proceso de clasificación/almacenamiento de datos de carácter personal interviene únicamente el responsable del tratamiento o si existen terceros involucrados en el proceso.",
			"2.4 Tecnologías aplicadas<br>
			* Registrar los métodos y/o el nombre de las tecnologías que se utilizan en el proceso de clasificación o almacenaje de los datos del interesado."
		),
		"3.	USO/TRATAMIENTO" => array(
			"3.1 Actividades del tratamiento<br>
			* Detallar acciones o labores desempeñadas al hacer uso de los datos de carácter personal.",
			"3.2 Categorías de datos tratados<br>
			* Listar qué categorías de datos se utilizan en el proceso del tratamiento",
			"3.3 Intervinientes involucrados<br>
			* Registrar si el tratamiento lo realiza el responsable del tratamiento y/o  terceros (encargados y/o subencargados del tratamiento).",
			"3.4 Tecnologías aplicadas<br>
			* Listar qué tecnologías son utilizadas para el proceso de tratamiento."
		),
		"4. CESIÓN DE DATOS A TERCERO" => array(
			"4.1 Actividades de transferencias<br>
			* Enumerar las actividades que se llevarán a cabo en el proceso de cesión o transferencia de la información.",
			"4.2 Categorías de datos transferidos<br>
			* Realizar un listado de las categorías de datos a ceder a terceros.",
			"4.3 Intervinientes en las transferencias<br>
			* Enumerar los intervinientes involucrados en los procesos de transferencias de datos.",
			"4.4 Tecnologías aplicadas<br>
			* Proporcionar el nombre de las tecnologías utilizadas en el proceso de cesión o transferencia de datos de carácter personal."
		),
		"5.	DESTRUCCIÓN" => array(
			"5.1 Actividades de la destrucción<br>
			* Enumerar las actividades llevadas a cabo en el proceso de destrucción de la información.",
			"5.2 Categorías de datos eliminados<br>
			* Realizar un listado de las categorías de datos que se pretenden destruir.",
			"5.3 Intervinientes en la destrucción<br>
			* Enumerar los terceros que se vean involucrados en las labores de destrucción y/o si interviene el responsable del tratamiento.",
			"5.4 Tecnologías aplicadas<br>
			* Listar tecnologías/métodos utilizados en el proceso de destrucción.",
		)
	);

    echo "
        <center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaAlumnos'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
							<th style='width:60%;'>CICLO DE VIDA DE LOS DATOS</th>
							<th> RESULTADO </th>
							<th> OBSERVACIONES </th>
                    	</tr>
                  	</thead>
                  	<tbody>
	";

	foreach ($filas as $titulo => $preguntas) {
		echo "
			<tr>
				<th colspan=3>".$titulo."</th>
			</tr>
		";

		foreach ($preguntas as $p) {
			echo "
				<tr>
					<td style='width:60%;'>".$p."</td>
			";	
			campoSelect('preguntaCicloVida'.$i, '', ['Sí', 'No'], ['SI', 'NO'], $datosCicloVida, 'selectpicker span1 show-tick', "data-live-search='true'", 1);
			areaTextoTabla('obsCicloVida'.$i, $datosCicloVida, 'obsCicloVida');
			echo "
				</tr>
			";

			$i++;
		}
	}

	echo "
					</tbody>
				</table>
			</div>
		</center>
	";



}

function tablaIntereses($datos) {
	 
	$datosIntereses = recuperaFormularios($datos, 'formularioIntereses');

	$i = 0;

	$filasInteresLegitimo = array(	
		"¿El tratamiento es conforme a la legislación nacional y de la UE?",
		"¿El tratamiento representa un interés real y actual de la entidad?",
		"¿Existen otros medios menos invasivos para alcanzar la finalidad prevista del tratamiento y satisfacer el interés legítimo del responsable del tratamiento?",
		"¿El responsable del tratamiento, los terceros o la comunidad en general puedan sufrir un perjuicio si no se realiza el tratamiento de datos?",
		"¿Existe un desequilibrio entre la situación del interesado y la del responsable del tratamiento?",
		"¿El interesado ha sido debidamente informado sobre las actividades del tratamiento?",
		"¿Pueden ocasionarse perjuicios al interesado?",
		"¿Se trata de un tratamiento normalizado en el sector?"
	);
	
	$filas = array(		
		"¿Los datos recogidos  se van a usar exclusivamente para la finalidad declarada y no para ninguna otra no informada ni incompatible con la legitimidad de su uso? -Principio de limitación de la finalidad-",	
		"La finalidad que se pretende cubrir ¿requiere de todos los datos a recabar y para todas las personas/interesados afectados? -Principio de minimización de datos-",		
		"Las tecnologías empleadas para el tratamiento ¿son adecuadas para la finalidad establecida desde el punto de vista del cumplimiento de los principios fundamentales de la privacidad?",		
		"¿Los datos se mantienen más tiempo del necesario para las finalidades del tratamiento? -Principio de limitación del plazo de conservación-"
	);

	
    echo "
		<br>
        <center>
	 		<div class='table-responsive' id='divTablaIntereses'>
				<table class='table' id='tablaInteresLegitimos'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
							<th style='width:60%;'>Evaluación del interés legítimo</th>
							<th> Respuesta </th>
							<th> Observaciones </th>
                    	</tr>
                  	</thead>
                  	<tbody>
	";	

	foreach ($filasInteresLegitimo as $p) {
		echo "
			<tr>
				<td style='width:60%;'>".$p."</td>
		";	
		campoSelect('preguntaIntereses'.$i, '', ['Sí', 'No'], ['SI', 'NO'], $datosIntereses, 'selectpicker span1 show-tick', "data-live-search='true'", 1);
		areaTextoTabla('obsIntereses'.$i, $datosIntereses, 'obsCicloVida');
		echo "
			</tr>
				";

		$i++;
	}

	echo "
					</tbody>
				</table>
			</div>
		</center>
	";
    
	echo "
        <center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaInteresOtros'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
							<th style='width:60%;'>Evaluación de la necesidad/proporcionalidad del tratamiento</th>
							<th> Respuesta </th>
							<th> Observaciones </th>
                    	</tr>
                  	</thead>
                  	<tbody>
	";	

	foreach ($filas as $p) {
		echo "
			<tr>
				<td style='width:60%;'>".$p."</td>
		";	
		campoSelect('preguntaIntereses'.$i, '', ['Sí', 'No'], ['SI', 'NO'], $datosIntereses, 'selectpicker span1 show-tick', "data-live-search='true'", 1);
		areaTextoTabla('obsIntereses'.$i, $datosIntereses, 'obsCicloVida');
		echo "
			</tr>
		";

		$i++;
	}

	echo "
					</tbody>
				</table>
			</div>
		</center>
	";
}

function tablaAmenazas($datos) {

	$datosAmenazas = recuperaFormularios($datos, 'formularioAmenazas');

	$i = 0;

	$filas = array(
		'1.No facilitar la información en materia de protección de datos o no redactarla de forma accesible y fácil de entender'  => array(
			'Cláusulas y locuciones para cumplir con el deber de información', 
			'Identificación de la finalidad del tratamiento'
		),
		'2. Carecer de una base jurídica sobre la que se sustenten los tratamientos realizados sobre los datos' => array(			
 			'Actividades del tratamiento',
 			'Identificación de la finalidad del tratamiento',
 			'Definición de la base legitimadora para el tratamiento'
		),
		'3. Tratar datos inadecuados y excesivos para la finalidad del tratamiento' => array(
			'Actividades del tratamiento',
			'Identificación de la finalidad del tratamiento',
			'Identificación de las categorías de los datos asociados a un tratamiento'
		),
		'4. Tratar datos personales con una finalidad distinta para la cual fueron recabados' => array(
			'Actividades del tratamiento',
			'Identificación de la finalidad del tratamiento',
			'Descripción del ciclo de vida del dato asociado a un tratamiento'
		),
		'5. Almacenar los datos por periodos superiores a los necesarios para la finalidad del tratamiento y a la legislación vigente' => array(
			'Actividades del tratamiento',
			'Identificación de la finalidad del tratamiento',
			'Definición de los plazos de conservación de los datos',
			'Descripción del ciclo de vida del dato asociado a un tratamiento'		   
		),
		'6. No disponer de una estructura organizativa, procesos y recursos para una adecuada gestión de la privacidad en la organización' => array(
			'Política de privacidad',
			'Gobierno de la privacidad'		   
		),
		'7. Realizar transferencias internacionales a países que no ofrezcan un nivel de protección adecuado' => array(
			'Cláusulas y locuciones para cumplir con el deber de información',
			'Transferencias a terceros países y organizaciones internacionales',
			'Realización de transferencias amparándose en garantías adecuadas',
			'Realización de transferencias amparándose en alguna de las excepciones'
		),
		'8. No tramitar o dificultar el ejercicio de los derechos de los interesados' => array(
			'Derechos del interesado'
		),
		'9. Resolución indebida del ejercicio de derechos de los interesados en tiempo, formato y forma' => array (
			'Derechos del interesado'
		),
		'10. Carecer de mecanismos de supervisión y control sobre las medidas que regulan la relación con un encargado el tratamiento' => array(
			'Gestión de terceros',
			'Seguridad ligada a los recursos humanos',
			'Revisiones de la seguridad de la información'
		),
		'11. No registrar la creación, modificación o cancelación de las actividades de tratamiento efectuadas bajo su responsabilidad' => array(
			'Actividades del tratamiento'
		) ,
		'12. Seleccionar o mantener una relación con un encargado de tratamiento sin disponer de las garantías adecuadas' => array(
			'Gestión de terceros',
			'Seguridad ligada a los recursos humanos',
			'Seguridad de la información en las relaciones con suministradores',
			'Cumplimiento de los requisitos legales y contractuales'		   
		),
		'13. No disponer de una estructura organizativa, procesos y recursos para una adecuada gestión de la seguridad en la organización' => array(
			'Directrices de la Dirección en seguridad de la información',
			'Organización interna de la seguridad de la información'  
		),
		'14. Fallas de seguridad en desarrollos de una nueva aplicación o de nuevas funcionalidades y/o parches de una aplicación ya existente' => array(
			'Gestión de cambios y configuración',
			'Seguridad en los procesos de desarrollo y soporte'		   
		),
		'15. Errores humanos en tareas de mantenimiento' => array(
			'Seguridad ligada a los recursos humanos',
			'Gestión de activos',
			'Gestión de cambios y configuración',
			'Mantenimiento de los equipos'		   
		),
		'16. Error en la configuración de un sistema, aplicación, estación de trabajo, impresora o componente de red' => array(
			'Gestión de cambios y configuración',
			'Mantenimiento de los equipos'		   
		),
		'17. Imposibilidad de atribuir a usuarios identificados todas las acciones que se llevan a cabo en un sistema de información' => array(
			'Control de accesos',
			'Registro de actividad y supervisión'		   
		),
		'18. Software malicioso (virus, troyanos, secuestradores de información)' => array(
			'Protección contra código malicioso',
			'Control del software en explotación'
		),
		'19. Fugas de información' => array(
			'Formación y capacitación en materia de protección de datos',
			'Seguridad ligada a los recursos humanos',
			'Clasificación de la información',
			'Control de accesos',
			'Cifrado',
			'Seguridad de los equipos',
			'Intercambio de información con partes externas',
			'Seguridad de la información en las relaciones con suministradores',
			'Gestión de incidentes en la seguridad de la información'		   
		),
		'20. Violaciones de la confidencialidad de los datos personales por parte de los empleados o personal externo de la organización' => array(
			'Formación y capacitación en materia de protección de datos',
			'Gestión de terceros',
			'Seguridad ligada a los recursos humanos',
			'Seguridad de la información en las relaciones con suministradores',
			'Control de accesos',
			'Cifrado',
			'Gestión de incidentes en la seguridad de la información'		   
		),
		'21. Manipulación o modificación no autorizada de la información' => array(
			'Organización interna de la seguridad de la información',
			'Control de accesos'
		),
		'22. Existencia de errores técnicos o fallos que ocasionen una indisponibilidad de los sistemas de información' => array(
			'Copias de seguridad de la información',
			'Gestión de incidentes en la seguridad de la información'		   
		),
		'23. Incapacidad para asegurar la disponibilidad de la información' => array(
			'Copias de seguridad de la información',
			'Aspectos de seguridad de la información en la gestión de la continuidad del negocio'
		),
		'24. Robo o extravío de equipos, soportes o dispositivos con datos personales' => array(			
			'Formación y capacitación en materia de protección de datos',
			'Dispositivos para movilidad y teletrabajo',
			'Seguridad ligada a los recursos humanos',
			'Gestión de activos',
			'Manejo de los soportes',
			'Seguridad física y ambiental',
			'Seguridad de los equipos',
			'Gestión de incidentes en la seguridad de la información'
		),
		'25. Catástrofes naturales que afectan a las infraestructuras (inundaciones, huracanes, terremotos, etc.)' => array(
			'Seguridad física y ambiental'
		) ,
		'26. Deficiencias en los protocolos de almacenamiento de los datos personales en formato físico' => array(
			'Seguridad física y ambiental',
			'Gestión de incidentes en la seguridad de la información'		   
		),
		'27. Medidas de control de acceso físico (tornos de acceso, arcos de seguridad, tarjetas de acceso, etc.) insuficientes o inexistentes' => array(
			'Seguridad física y ambiental',
			'Seguridad de los equipos'		   
		),
	);

    echo "
        <center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaAmenazas'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
							<th style='width:25%;'> AMENAZAS </th>
							<th style='width:25%;'> MEDIDAS DE CONTROL </th>
							<th style='width:25%;'> EVALUACIÓN DE PROBABILIDAD DE IMPACTO </th>
							<th style='width:25%;'> RIESGO RESIDUAL </th>
                    	</tr>
                  	</thead>
                  	<tbody>
	";

	foreach ($filas as $amenaza => $medidas) {
		imprimeFilaTablaAmenazas($i, $amenaza, $medidas, $datosAmenazas);
		$i++;
	}

	echo "
					</tbody>
				</table>
			</div>
		</center>
	";
}

function imprimeFilaTablaAmenazas($i, $amenaza, $medidas, $datos) {	
	
	$j = 0;

	echo "
		<tr>
			<td style='width:25%;'>
	";
	campoRadio('amenaza'.$i, $amenaza, $datos);
	echo "
		</td>
		<td style='width:25%;'>
	";

	foreach ($medidas as $m) {
		$valor = isset($datos['medida'.$i.$j]) ? $datos : false; // para evitar warnings 
		campoCheckIndividual('medida'.$i.$j, $m, $valor);
		$j++;
	}

	echo "
		</td>
		<td style='width:25%;'>
	";

	campoSelect('probabilidad'.$i, 'Probabilidad', ['Despreciable', 'Limitada', 'Significativa', 'Máxima'], ['Despreciable', 'Limitada', 'Significativa', 'Máxima'], $datos);
	campoSelect('impacto'.$i, 'Impacto', ['Despreciable', 'Limitado', 'Significativo', 'Máximo'], ['Despreciable', 'Limitado', 'Significativo', 'Máximo'], $datos);
	
	echo "
		</td>
		<td style='width:25%;'>
	";
	campoSelect('riesgoR'.$i, 'Riesgo Residual', ['Bajo', 'Medio', 'Alto', 'Muy Alto'], ['Bajo', 'Medio', 'Alto', 'Muy Alto'], $datos, 'selectpicker span3 show-tick riesgoR');

	echo "
			</td>
		</tr>
	";
}

function recuperaFormularios($datos = false, $tabla) {
	
	$res = false;

	if($datos) {
		$aux = explode('&{}&', $datos[$tabla]);
		foreach ($aux as $item) {
			$item = explode('=>', $item);
			$res[$item[0]] = $item[1];
		}
	}

	return $res;

}

function generaDocumento($codigo){

	conexionBD();
	$datos   = consultaBD("SELECT * FROM evaluacion_general WHERE codigo = ".$codigo.";", false, true);
	$cliente = consultaBD("SELECT * FROM clientes WHERE codigo = ".$datos['codigoCliente'].";", false, true);

	require_once '../../api/phpword/PHPWord.php';
    
	$PHPWord   = new PHPWord();
	$documento = $PHPWord->loadTemplate('../documentos/evaluaciones-de-riesgo/plantilla.docx');

	$texto1 = '<w:p w:rsidR="00BA29DD" w:rsidRPr="00514D8B" w:rsidRDefault="00514D8B" w:rsidP="00BA29DD"><w:pPr><w:ind w:left="360"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="8064A2" w:themeColor="accent4"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>EVALUACIÓN DE IMPACTO DE '.$cliente['razonSocial'].'</w:t></w:r></w:p><w:p w:rsidR="002B4E44" w:rsidRDefault="002B4E44" w:rsidP="002B4E44"><w:pPr><w:ind w:left="360"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="8064A2" w:themeColor="accent4"/></w:rPr></w:pPr><w:r w:rsidRPr="004A4260"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t xml:space="preserve">Fecha Evaluación de Impacto '.formateaFechaWeb($datos['fechaEvaluacion']).'</w:t></w:r></w:p><w:p w:rsidR="006327FF" w:rsidRDefault="006327FF" w:rsidP="006327FF"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="004A4260"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve">Es posible que el tratamiento de datos personales no precise una evaluación de impacto, pero aún así desee llevarla a cabo. A continuación, se realizará dicha evaluación mediante el correspondiente análisis del ciclo de vida de los datos del tratamiento, de la necesidad y proporcionalidad del mismo así como de los riesgos del tratamiento.”</w:t></w:r></w:p><w:p w:rsidR="006327FF" w:rsidRPr="009B68E3" w:rsidRDefault="006327FF" w:rsidP="006327FF"><w:pPr><w:pStyle w:val="text-align-justify"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:spacing w:before="0" w:beforeAutospacing="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="180909"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="009B68E3"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="180909"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>La Evaluación de Impacto en la Protección de Datos Personales permite evaluar anticipadamente cuáles son los potenciales riesgos a los que están expuestos los datos personales en función de las actividades de tratamiento que se llevan a cabo con los mismos.</w:t></w:r></w:p><w:p w:rsidR="006327FF" w:rsidRPr="009B68E3" w:rsidRDefault="006327FF" w:rsidP="006327FF"><w:pPr><w:pStyle w:val="text-align-justify"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:spacing w:before="0" w:beforeAutospacing="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="180909"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="009B68E3"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="180909"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>El análisis de riesgos para un determinado tratamiento permite identificar los riesgos a los que están expuestos los datos personales de los interesados y establecer las medidas necesarias para reducirlos hasta un nivel de riesgo aceptable.</w:t></w:r></w:p><w:p w:rsidR="006327FF" w:rsidRPr="009B68E3" w:rsidRDefault="006327FF" w:rsidP="006327FF"><w:pPr><w:pStyle w:val="text-align-justify"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:spacing w:before="0" w:beforeAutospacing="0"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="180909"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="009B68E3"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="180909"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>El RGPD prevé que las Evaluaciones de Impacto se lleven a cabo antes del tratamiento en los casos en que sea probable que exista un alto riesgo para los derechos y libertades de los afectados.</w:t></w:r></w:p><w:p w:rsidR="006327FF" w:rsidRDefault="006327FF" w:rsidP="006327FF"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="009B68E3"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>Es posible que el tratamiento de datos personales no requiera por imperativo legal una evaluación de impacto y, aún así desee llevarla a cabo.</w:t></w:r></w:p><w:p w:rsidR="006327FF" w:rsidRDefault="006327FF" w:rsidP="006327FF"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr><w:r w:rsidRPr="009B68E3"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t xml:space="preserve"> A continuación, se realizará dicha evaluación mediante el análisis del ciclo de vida de los datos del tratamiento, de la necesidad y proporcionalidad del mismo así como de los riesgos del tratamiento, si bien, previamente a la presente Evaluación de Impacto, deberá haber realizado la correspondiente Evaluación Previa de las actividades de tratamiento objeto de análisis, constituyendo la misma el primer paso de la presente Evaluación de Impacto.</w:t></w:r></w:p>';

	$tabla1 = contenidoTabla1($datos);
	$tabla2 = contenidoTabla2($datos);
	$tabla3 = contenidoTabla3($datos);
	$tabla4 = contenidoTabla4($datos);
	$tabla5 = contenidoTabla5($datos, $cliente);
	
	$documento->setValue("razonSocial", utf8_decode($cliente['razonSocial']));	
	$documento->setValue("fecha", formateaFechaWeb($datos['fechaEvaluacion']));	
	$documento->setValue("texto1", utf8_decode($texto1));
	$documento->setValue("tabla1", utf8_decode($tabla1));
	$documento->setValue("tabla2", utf8_decode($tabla2));
	$documento->setValue("tabla3", utf8_decode($tabla3));
	$documento->setValue("tabla4", utf8_decode($tabla4));
	$documento->setValue("tabla5", utf8_decode($tabla5));

	$nombreFichero="Analisis_de_Riesgos_Reducidos.docx";
	$documento->save('../documentos/evaluaciones-de-riesgo/'.$nombreFichero);

	cierraBD();
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/evaluaciones-de-riesgo/'.$nombreFichero);
}

function contenidoTabla1($datos) {

	$datosTabla = recuperaFormularios($datos, 'formularioCicloVida');

	$salto = '</w:t></w:r></w:p><w:p w:rsidR="000D4AF7" w:rsidRPr="004A4260" w:rsidRDefault="000D4AF7" w:rsidP="000D4AF7"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="212529"/><w:spacing w:val="24"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/></w:rPr></w:pPr><w:r w:rsidRPr="004A4260"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>';

	$res = '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="0" w:type="auto"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="4428"/><w:gridCol w:w="1245"/><w:gridCol w:w="3047"/></w:tblGrid><w:tr w:rsidR="0007281D" w:rsidRPr="004A4260" w:rsidTr="00985D0D"><w:trPr><w:tblHeader/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="0007281D" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w:rsidR="0007281D" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>CICLO DE VIDA DE LOS DATOS</w:t></w:r></w:p><w:p w:rsidR="0007281D" w:rsidRPr="004A4260" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="0007281D" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w:rsidR="0007281D" w:rsidRPr="004A4260" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="004A4260"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>RESULTADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="0007281D" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w:rsidR="0007281D" w:rsidRPr="004A4260" w:rsidRDefault="0007281D" w:rsidP="0007281D"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r w:rsidRPr="004A4260"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>OBSERVACIONES/COMENTARIOS</w:t></w:r></w:p><w:p w:rsidR="0007281D" w:rsidRPr="004A4260" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

	$filas = array(		
		"1. CAPTURA DE DATOS" => array(
			"1.1	Actividades de los procesos de captura de datos-*Detallar actividades, tareas o acciones que se realizan con la finalidad de recabar los datos necesarios para el tratamiento.",
			"1.2	Categorías de datos adquiridos-* Listar las categorías de datos que se han recolectado para el tratamiento.",
			"1.3	Intervinientes en la captura de los datos-* Registrar si los datos han sido proporcionados únicamente por el interesado, por la misma entidad o si hay terceros que aporten la información.",
			"1.4	Tecnologías aplicadas-* Elaborar un listado completo de las tecnologías/métodos utilizados para recabar los datos de carácter personal tratados."
		),
		"2.	CLASIFICACIÓN/ALMACENAJE" => array(
			"2.1 Actividades de almacenamiento y clasificación-* Enumerar las actividades llevadas a cabo en el proceso de clasificación/almacenaje de la información.",
			"2.2 Categorías de datos almacenados o clasificados-* Realizar un inventario de los tipos de datos de carácter personal a clasificar/almacenar.",
			"2.3 Intervinientes en el proceso de almacenamiento y clasificación-* Registrar si en el proceso de clasificación/almacenamiento de datos de carácter personal interviene únicamente el responsable del tratamiento o si existen terceros involucrados en el proceso.",
			"2.4 Tecnologías aplicadas-* Registrar los métodos y/o el nombre de las tecnologías que se utilizan en el proceso de clasificación o almacenaje de los datos del interesado."
		),
		"3.	USO/TRATAMIENTO" => array(
			"3.1 Actividades del tratamiento-* Detallar acciones o labores desempeñadas al hacer uso de los datos de carácter personal.",
			"3.2 Categorías de datos tratados-* Listar qué categorías de datos se utilizan en el proceso del tratamiento",
			"3.3 Intervinientes involucrados-* Registrar si el tratamiento lo realiza el responsable del tratamiento y/o  terceros (encargados y/o subencargados del tratamiento).",
			"3.4 Tecnologías aplicadas-* Listar qué tecnologías son utilizadas para el proceso de tratamiento."
		),
		"4. CESIÓN DE DATOS A TERCERO" => array(
			"4.1 Actividades de transferencias-* Enumerar las actividades que se llevarán a cabo en el proceso de cesión o transferencia de la información.",
			"4.2 Categorías de datos transferidos-* Realizar un listado de las categorías de datos a ceder a terceros.",
			"4.3 Intervinientes en las transferencias-* Enumerar los intervinientes involucrados en los procesos de transferencias de datos.",
			"4.4 Tecnologías aplicadas-* Proporcionar el nombre de las tecnologías utilizadas en el proceso de cesión o transferencia de datos de carácter personal."
		),
		"5.	DESTRUCCIÓN" => array(
			"5.1 Actividades de la destrucción-* Enumerar las actividades llevadas a cabo en el proceso de destrucción de la información.",
			"5.2 Categorías de datos eliminados-* Realizar un listado de las categorías de datos que se pretenden destruir.",
			"5.3 Intervinientes en la destrucción-* Enumerar los terceros que se vean involucrados en las labores de destrucción y/o si interviene el responsable del tratamiento.",
			"5.4 Tecnologías aplicadas-* Listar tecnologías/métodos utilizados en el proceso de destrucción.",
		)
	);

	$i = 0;

	foreach ($filas as $titulo => $preguntas) {
		
		$res .= '<w:tr w:rsidR="0007281D" w:rsidRPr="004A4260" w:rsidTr="00985D0D"><w:trPr><w:tblHeader/></w:trPr><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="0007281D" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr></w:p><w:p w:rsidR="0007281D" w:rsidRDefault="0007281D" w:rsidP="00C57ADF"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/></w:rPr><w:t>'.$titulo.'</w:t></w:r></w:p><w:p w:rsidR="003F6522" w:rsidRPr="004A4260" w:rsidRDefault="003F6522" w:rsidP="003F6522"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="003F6522" w:rsidRPr="004A4260" w:rsidRDefault="003F6522" w:rsidP="00545331"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="4F81BD" w:themeColor="accent1"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="003F6522" w:rsidRPr="004A4260" w:rsidRDefault="003F6522" w:rsidP="00545331"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="8064A2" w:themeColor="accent4"/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

		foreach ($preguntas as $p) {
			
			$p = str_replace('-', $salto, $p);

			$resultado = $datosTabla['preguntaCicloVida'.$i] == 'SI' ? 'Sí' : 'No';
			
			$res .= '<w:tr w:rsidR="00AC025F" w:rsidRPr="004A4260" w:rsidTr="00985D0D"><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="003F6522" w:rsidRPr="004A4260" w:rsidRDefault="003F6522" w:rsidP="00545331"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00C57ADF" w:rsidRPr="00746E3C" w:rsidRDefault="00E472B4" w:rsidP="00746E3C"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="00746E3C"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>'.$p.'</w:t></w:r></w:p><w:p w:rsidR="000D4AF7" w:rsidRPr="004A4260" w:rsidRDefault="000D4AF7" w:rsidP="000D4AF7"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:ind w:left="465"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="00C57ADF" w:rsidRPr="00FD625E" w:rsidRDefault="00C57ADF" w:rsidP="00545331"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00A5312A" w:rsidRPr="00FD625E" w:rsidRDefault="00BE1AE0" w:rsidP="00545331"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="00FD625E"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.$resultado.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="0" w:type="auto"/></w:tcPr><w:p w:rsidR="00C57ADF" w:rsidRPr="00FD625E" w:rsidRDefault="00C57ADF" w:rsidP="00545331"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E472B4" w:rsidRPr="00FD625E" w:rsidRDefault="00E472B4" w:rsidP="00545331"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="00FD625E"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>'.$datosTabla['obsCicloVida'.$i].'</w:t></w:r></w:p><w:p w:rsidR="00E472B4" w:rsidRPr="00FD625E" w:rsidRDefault="00E472B4" w:rsidP="00545331"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

			$i++;
		}
	}

	$res .= '</w:tbl>';

	return $res;
}

function contenidoTabla2($datos) {

	$legitimacion = [
		0 => 'Consentimiento', 
		1 => 'Relación contractual',  
		2 => 'Intereses vitales del interesado o  de otras personas',  
		3 => 'Obligación legal para el responsable',  
		4 => 'Interés público o ejercicio de poderes públicos', 
		5 => 'Intereses legítimos prevalentes del responsable o de terceros'
	];	

	$res = '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="0" w:type="auto"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="3077"/><w:gridCol w:w="3055"/><w:gridCol w:w="2588"/></w:tblGrid><w:tr w:rsidR="008C50B1" w:rsidRPr="008C50B1" w:rsidTr="00A11E36"><w:tc><w:tcPr><w:tcW w:w="3077" w:type="dxa"/></w:tcPr><w:p w:rsidR="00A11E36" w:rsidRPr="008C50B1" w:rsidRDefault="00A11E36" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00A11E36" w:rsidRPr="008C50B1" w:rsidRDefault="00A11E36" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>Legitimación</w:t></w:r></w:p><w:p w:rsidR="00A11E36" w:rsidRPr="008C50B1" w:rsidRDefault="00A11E36" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3055" w:type="dxa"/></w:tcPr><w:p w:rsidR="00A11E36" w:rsidRPr="008C50B1" w:rsidRDefault="00A11E36" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00A11E36" w:rsidRPr="008C50B1" w:rsidRDefault="00A11E36" w:rsidP="00FF3624"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.$legitimacion[$datos['legitimacion']].'</w:t></w:r></w:p><w:p w:rsidR="00A11E36" w:rsidRPr="008C50B1" w:rsidRDefault="00A11E36" w:rsidP="00FF3624"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2588" w:type="dxa"/></w:tcPr><w:p w:rsidR="00A11E36" w:rsidRPr="008C50B1" w:rsidRDefault="00A11E36" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00674014" w:rsidRPr="008C50B1" w:rsidRDefault="00674014" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.utf8_decode($datos['obsLegitimacion']).'</w:t></w:r></w:p><w:p w:rsidR="0082063D" w:rsidRPr="008C50B1" w:rsidRDefault="0082063D" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p></w:tc></w:tr></w:tbl>';

	return $res;

}

function contenidoTabla3($datos) {

	$datosTabla = recuperaFormularios($datos, 'formularioIntereses');

	$salto = '</w:t></w:r></w:p><w:p w:rsidR="000D4AF7" w:rsidRPr="004A4260" w:rsidRDefault="000D4AF7" w:rsidP="000D4AF7"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="212529"/><w:spacing w:val="24"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/></w:rPr></w:pPr><w:r w:rsidRPr="004A4260"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>';

	$res = '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="0" w:type="auto"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="2881"/><w:gridCol w:w="2881"/><w:gridCol w:w="2882"/></w:tblGrid><w:tr w:rsidR="008C50B1" w:rsidRPr="008C50B1" w:rsidTr="00D4341E"><w:trPr><w:tblHeader/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2881" w:type="dxa"/></w:tcPr><w:p w:rsidR="00645C35" w:rsidRPr="008C50B1" w:rsidRDefault="00645C35" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>Evaluación del interés legítimo</w:t></w:r></w:p><w:p w:rsidR="00645C35" w:rsidRPr="008C50B1" w:rsidRDefault="00645C35" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2881" w:type="dxa"/></w:tcPr><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00645C35" w:rsidRPr="008C50B1" w:rsidRDefault="00645C35" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>Respuesta</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2882" w:type="dxa"/></w:tcPr><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00645C35" w:rsidRPr="008C50B1" w:rsidRDefault="00645C35" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>Observaciones</w:t></w:r></w:p></w:tc></w:tr>';

	$filasInteresLegitimo = array(	
		"¿El tratamiento es conforme a la legislación nacional y de la UE?",
		"¿El tratamiento representa un interés real y actual de la entidad?",
		"¿Existen otros medios menos invasivos para alcanzar la finalidad prevista del tratamiento y satisfacer el interés legítimo del responsable del tratamiento?",
		"¿El responsable del tratamiento, los terceros o la comunidad en general puedan sufrir un perjuicio si no se realiza el tratamiento de datos?",
		"¿Existe un desequilibrio entre la situación del interesado y la del responsable del tratamiento?",
		"¿El interesado ha sido debidamente informado sobre las actividades del tratamiento?",
		"¿Pueden ocasionarse perjuicios al interesado?",
		"¿Se trata de un tratamiento normalizado en el sector?"
	);
	
	$filas = array(		
		"¿Los datos recogidos  se van a usar exclusivamente para la finalidad declarada y no para ninguna otra no informada ni incompatible con la legitimidad de su uso? -Principio de limitación de la finalidad-",	
		"La finalidad que se pretende cubrir ¿requiere de todos los datos a recabar y para todas las personas/interesados afectados? -Principio de minimización de datos-",		
		"Las tecnologías empleadas para el tratamiento ¿son adecuadas para la finalidad establecida desde el punto de vista del cumplimiento de los principios fundamentales de la privacidad?",		
		"¿Los datos se mantienen más tiempo del necesario para las finalidades del tratamiento? -Principio de limitación del plazo de conservación-"
	);
	
	$i = 0;

	foreach ($filas as $pregunta) {

		$resultado = $datosTabla['preguntaIntereses'.$i] == 'SI' ? 'Sí' : 'No';
		
		$res .= '<w:tr w:rsidR="008C50B1" w:rsidRPr="008C50B1" w:rsidTr="00015408"><w:tc><w:tcPr><w:tcW w:w="2881" w:type="dxa"/></w:tcPr><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00015408"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00015408"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">'.$pregunta.'</w:t></w:r></w:p><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2881" w:type="dxa"/></w:tcPr><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="003E6D59" w:rsidRPr="008C50B1" w:rsidRDefault="00EF1D05" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">'.$resultado.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2882" w:type="dxa"/></w:tcPr><w:p w:rsidR="003E6D59" w:rsidRPr="00FD625E" w:rsidRDefault="00836A2F" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.$datosTabla['obsIntereses'.$i].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';


		$i++;
	
	}

	if ($datos['legitimacion'] == 5) {
		foreach ($filasInteresLegitimo as $pregunta) {

			$resultado = $datosTabla['preguntaIntereses'.$i] == 'SI' ? 'Sí' : 'No';
			
			$res .= '<w:tr w:rsidR="008C50B1" w:rsidRPr="008C50B1" w:rsidTr="00015408"><w:tc><w:tcPr><w:tcW w:w="2881" w:type="dxa"/></w:tcPr><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00015408"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00015408"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">'.$pregunta.'</w:t></w:r></w:p><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2881" w:type="dxa"/></w:tcPr><w:p w:rsidR="00015408" w:rsidRPr="008C50B1" w:rsidRDefault="00015408" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr></w:p><w:p w:rsidR="003E6D59" w:rsidRPr="008C50B1" w:rsidRDefault="00EF1D05" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t xml:space="preserve">'.$resultado.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2882" w:type="dxa"/></w:tcPr><w:p w:rsidR="003E6D59" w:rsidRPr="00FD625E" w:rsidRDefault="00836A2F" w:rsidP="00CC0609"><w:pPr><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.$datosTabla['obsIntereses'.$i].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';
	
	
			$i++;
		
		}
	}

	$res .= '</w:tbl>';

	return $res;
}

function contenidoTabla4($datos) {

	$datosTabla = recuperaFormularios($datos, 'formularioAmenazas');

	$salto = '</w:t></w:r></w:p><w:p w:rsidR="000D4AF7" w:rsidRPr="004A4260" w:rsidRDefault="000D4AF7" w:rsidP="000D4AF7"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="212529"/><w:spacing w:val="24"/><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/></w:rPr></w:pPr><w:r w:rsidRPr="004A4260"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/></w:rPr><w:t>';

	$res = '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="5000" w:type="pct"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="2205"/><w:gridCol w:w="2171"/><w:gridCol w:w="2171"/><w:gridCol w:w="2173"/></w:tblGrid><w:tr w:rsidR="008C50B1" w:rsidRPr="008C50B1" w:rsidTr="00AB5DD6"><w:trPr><w:tblHeader/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1264" w:type="pct"/></w:tcPr><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>AMENAZAS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1245" w:type="pct"/></w:tcPr><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>MEDIDAS DE CONTROL</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1245" w:type="pct"/></w:tcPr><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>EVALUACIÓN DE PROBABILIDAD E IMPACTO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1246" w:type="pct"/></w:tcPr><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>RIESGO RESIDUAL</w:t></w:r></w:p></w:tc></w:tr>';

	$filas = array(
		'1.No facilitar la información en materia de protección de datos o no redactarla de forma accesible y fácil de entender'  => array(
			'Cláusulas y locuciones para cumplir con el deber de información', 
			'Identificación de la finalidad del tratamiento'
		),
		'2. Carecer de una base jurídica sobre la que se sustenten los tratamientos realizados sobre los datos' => array(			
 			'Actividades del tratamiento',
 			'Identificación de la finalidad del tratamiento',
 			'Definición de la base legitimadora para el tratamiento'
		),
		'3. Tratar datos inadecuados y excesivos para la finalidad del tratamiento' => array(
			'Actividades del tratamiento',
			'Identificación de la finalidad del tratamiento',
			'Identificación de las categorías de los datos asociados a un tratamiento'
		),
		'4. Tratar datos personales con una finalidad distinta para la cual fueron recabados' => array(
			'Actividades del tratamiento',
			'Identificación de la finalidad del tratamiento',
			'Descripción del ciclo de vida del dato asociado a un tratamiento'
		),
		'5. Almacenar los datos por periodos superiores a los necesarios para la finalidad del tratamiento y a la legislación vigente' => array(
			'Actividades del tratamiento',
			'Identificación de la finalidad del tratamiento',
			'Definición de los plazos de conservación de los datos',
			'Descripción del ciclo de vida del dato asociado a un tratamiento'		   
		),
		'6. No disponer de una estructura organizativa, procesos y recursos para una adecuada gestión de la privacidad en la organización' => array(
			'Política de privacidad',
			'Gobierno de la privacidad'		   
		),
		'7. Realizar transferencias internacionales a países que no ofrezcan un nivel de protección adecuado' => array(
			'Cláusulas y locuciones para cumplir con el deber de información',
			'Transferencias a terceros países y organizaciones internacionales',
			'Realización de transferencias amparándose en garantías adecuadas',
			'Realización de transferencias amparándose en alguna de las excepciones'
		),
		'8. No tramitar o dificultar el ejercicio de los derechos de los interesados' => array(
			'Derechos del interesado'
		),
		'9. Resolución indebida del ejercicio de derechos de los interesados en tiempo, formato y forma' => array (
			'Derechos del interesado'
		),
		'10. Carecer de mecanismos de supervisión y control sobre las medidas que regulan la relación con un encargado el tratamiento' => array(
			'Gestión de terceros',
			'Seguridad ligada a los recursos humanos',
			'Revisiones de la seguridad de la información'
		),
		'11. No registrar la creación, modificación o cancelación de las actividades de tratamiento efectuadas bajo su responsabilidad' => array(
			'Actividades del tratamiento'
		) ,
		'12. Seleccionar o mantener una relación con un encargado de tratamiento sin disponer de las garantías adecuadas' => array(
			'Gestión de terceros',
			'Seguridad ligada a los recursos humanos',
			'Seguridad de la información en las relaciones con suministradores',
			'Cumplimiento de los requisitos legales y contractuales'		   
		),
		'13. No disponer de una estructura organizativa, procesos y recursos para una adecuada gestión de la seguridad en la organización' => array(
			'Directrices de la Dirección en seguridad de la información',
			'Organización interna de la seguridad de la información'  
		),
		'14. Fallas de seguridad en desarrollos de una nueva aplicación o de nuevas funcionalidades y/o parches de una aplicación ya existente' => array(
			'Gestión de cambios y configuración',
			'Seguridad en los procesos de desarrollo y soporte'		   
		),
		'15. Errores humanos en tareas de mantenimiento' => array(
			'Seguridad ligada a los recursos humanos',
			'Gestión de activos',
			'Gestión de cambios y configuración',
			'Mantenimiento de los equipos'		   
		),
		'16. Error en la configuración de un sistema, aplicación, estación de trabajo, impresora o componente de red' => array(
			'Gestión de cambios y configuración',
			'Mantenimiento de los equipos'		   
		),
		'17. Imposibilidad de atribuir a usuarios identificados todas las acciones que se llevan a cabo en un sistema de información' => array(
			'Control de accesos',
			'Registro de actividad y supervisión'		   
		),
		'18. Software malicioso (virus, troyanos, secuestradores de información)' => array(
			'Protección contra código malicioso',
			'Control del software en explotación'
		),
		'19. Fugas de información' => array(
			'Formación y capacitación en materia de protección de datos',
			'Seguridad ligada a los recursos humanos',
			'Clasificación de la información',
			'Control de accesos',
			'Cifrado',
			'Seguridad de los equipos',
			'Intercambio de información con partes externas',
			'Seguridad de la información en las relaciones con suministradores',
			'Gestión de incidentes en la seguridad de la información'		   
		),
		'20. Violaciones de la confidencialidad de los datos personales por parte de los empleados o personal externo de la organización' => array(
			'Formación y capacitación en materia de protección de datos',
			'Gestión de terceros',
			'Seguridad ligada a los recursos humanos',
			'Seguridad de la información en las relaciones con suministradores',
			'Control de accesos',
			'Cifrado',
			'Gestión de incidentes en la seguridad de la información'		   
		),
		'21. Manipulación o modificación no autorizada de la información' => array(
			'Organización interna de la seguridad de la información',
			'Control de accesos'
		),
		'22. Existencia de errores técnicos o fallos que ocasionen una indisponibilidad de los sistemas de información' => array(
			'Copias de seguridad de la información',
			'Gestión de incidentes en la seguridad de la información'		   
		),
		'23. Incapacidad para asegurar la disponibilidad de la información' => array(
			'Copias de seguridad de la información',
			'Aspectos de seguridad de la información en la gestión de la continuidad del negocio'
		),
		'24. Robo o extravío de equipos, soportes o dispositivos con datos personales' => array(			
			'Formación y capacitación en materia de protección de datos',
			'Dispositivos para movilidad y teletrabajo',
			'Seguridad ligada a los recursos humanos',
			'Gestión de activos',
			'Manejo de los soportes',
			'Seguridad física y ambiental',
			'Seguridad de los equipos',
			'Gestión de incidentes en la seguridad de la información'
		),
		'25. Catástrofes naturales que afectan a las infraestructuras (inundaciones, huracanes, terremotos, etc.)' => array(
			'Seguridad física y ambiental'
		) ,
		'26. Deficiencias en los protocolos de almacenamiento de los datos personales en formato físico' => array(
			'Seguridad física y ambiental',
			'Gestión de incidentes en la seguridad de la información'		   
		),
		'27. Medidas de control de acceso físico (tornos de acceso, arcos de seguridad, tarjetas de acceso, etc.) insuficientes o inexistentes' => array(
			'Seguridad física y ambiental',
			'Seguridad de los equipos'		   
		),
	);
	
	$i = 0;

	foreach ($filas as $titulo => $amenaza) {

		$resultado = $datosTabla['amenaza'.$i] == 'SI' ? 'Sí' : 'No';
		
		$res .= '<w:tr w:rsidR="008C50B1" w:rsidRPr="008C50B1" w:rsidTr="00AB5DD6"><w:tc><w:tcPr><w:tcW w:w="1264" w:type="pct"/></w:tcPr><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:textAlignment w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00AB5DD6" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:textAlignment w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">'.$titulo.'</w:t></w:r></w:p><w:p w:rsidR="00AB5DD6" w:rsidRPr="008C50B1" w:rsidRDefault="00AB5DD6" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:textAlignment w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00AB5DD6" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:textAlignment w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>'.$resultado.'</w:t></w:r><w:r w:rsidR="00123F24" w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">  </w:t></w:r></w:p><w:p w:rsidR="00AB3010" w:rsidRPr="008C50B1" w:rsidRDefault="00AB3010" w:rsidP="00AB5DD6"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:textAlignment w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc>';

		$res .= '<w:tc><w:tcPr><w:tcW w:w="1245" w:type="pct"/></w:tcPr>';

		$j = 0;
		foreach ($amenaza as $a) {
			$valor = isset($datosTabla['medida'.$i.$j]) ? 'Sí' : 'No';
			
			$res .= '<w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00AB5DD6" w:rsidP="007618F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve">'.$a.': '.$valor.'</w:t></w:r></w:p><w:p w:rsidR="00AB5DD6" w:rsidRPr="008C50B1" w:rsidRDefault="00AB5DD6" w:rsidP="007618F7"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p>';

			$j++;
		}

		$res .= '</w:tc><w:tc><w:tcPr><w:tcW w:w="1245" w:type="pct"/></w:tcPr><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>PROBABILIDAD</w:t></w:r><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>- '.$datosTabla['probabilidad'.$i].'</w:t></w:r></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>IMPACTO</w:t></w:r></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>-'.$datosTabla['impacto'.$i].'</w:t></w:r></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="00055F6D"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1246" w:type="pct"/></w:tcPr><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>RIESGO RESIDUAL</w:t></w:r></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="007618F7"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>-'.$datosTabla['riesgoR'.$i].'</w:t></w:r></w:p><w:p w:rsidR="00123F24" w:rsidRPr="008C50B1" w:rsidRDefault="00123F24" w:rsidP="00055F6D"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc></w:tr>';

		$i++;
	
	}

	$res .= '</w:tbl>';

	return $res;
}

function contenidoTabla5($datos, $cliente) {

	if ($datos['resultado'] == 'aceptable') {
		$resultado = 'ACEPTABLE';
		$texto = 'El resultado de la presente gestión de riesgos deberá ser validado por el responsable del tratamiento, mediante la firma del documento que recoja el resultado del análisis realizado.';
	}
	else {
		$resultado = 'NO ACEPTABLE';
		$texto = 'Revise los riesgos identificados y adopte las medidas de control necesarias hasta el punto de disminuir el riesgo residual a un grado Bajo o Medio.';
	}
	
	$representante = $cliente['administrador'].' '.$cliente['apellido1'].' '.$cliente['apellido2'];
	$razonSocial   = $cliente['razonSocial'];

	$res = '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="0" w:type="auto"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="8644"/></w:tblGrid><w:tr w:rsidR="008C50B1" w:rsidRPr="008C50B1" w:rsidTr="00984E67"><w:tc><w:tcPr><w:tcW w:w="8644" w:type="dxa"/></w:tcPr><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>RESULTADO '.$resultado.'</w:t></w:r></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:tr w:rsidR="008C50B1" w:rsidRPr="008C50B1" w:rsidTr="00984E67"><w:tc><w:tcPr><w:tcW w:w="8644" w:type="dxa"/></w:tcPr><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00E76AC3"><w:pPr><w:pStyle w:val="Prrafodelista"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/></w:rPr><w:t>'.$texto.'</w:t></w:r></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00E76AC3"><w:pPr><w:shd w:val="clear" w:color="auto" w:fill="FFFFFF"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc></w:tr><w:tr w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidTr="00984E67"><w:tc><w:tcPr><w:tcW w:w="8644" w:type="dxa"/></w:tcPr><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>Fdo. '.$representante.'</w:t></w:r></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r w:rsidRPr="008C50B1"><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t>Representante legal de '.$razonSocial.'</w:t></w:r></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00E76AC3" w:rsidRPr="008C50B1" w:rsidRDefault="00E76AC3" w:rsidP="00984E67"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Calibri" w:eastAsia="Times New Roman" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000" w:themeColor="text1"/><w:spacing w:val="24"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc></w:tr></w:tbl>';

	return $res;
}

//Fin parte de evaluación de riesgos
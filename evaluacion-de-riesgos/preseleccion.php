<?php
  $seccionActiva=3;
  include_once('../cabecera.php');


  if(isset($_GET['cliente'])){
    $codigoCliente=$_GET['cliente'];
  } else if(isset($_GET['codigo'])){
    $codigoCliente=$_GET['codigo'];
  } else {
    $codigoCliente=$_POST['cliente'];
  }
  //$datosusuario=datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12 margenAb">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class='icon-edit'></i><i class='icon-chevron-right'></i><i class="icon-search-plus"></i>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Por favor, seleccione los riesgos que desea evaluar:</h6>
                    <form action="gestion.php" method="post" class="noAjax">
                      <?php
                      conexionBD();
                          if(isset($_GET['codigo'])){
                            //campoOculto(recogerPermiso(7,'checkModificar'),'permisoModificar');
                            campoOculto($_GET['codigo'],'codigoEvaluacion');
                            $consulta=consultaBD("SELECT * FROM riesgos_evaluacion_general WHERE codigoEvaluacion=".$_GET['codigo']);
                            $i=0;
                            while($reg=mysql_fetch_assoc($consulta)){
                              $riesgos[$i] = $reg['codigoRiesgo'];
                              $i++;
                            }
                            
                          }
                          $consulta=consultaBD("SELECT * FROM riesgos ORDER BY codigoProceso ASC, actividad ASC, nombre ASC");

                          $i=0;
                          $tipo='';
                          while($riesgo=mysql_fetch_assoc($consulta)){
                            $marcado = '';
                            if($tipo != $riesgo['codigoProceso']){
                              //$proceso=datosRegistro('indicadores',$riesgo['codigoProceso']);
                              $tipo = $riesgo['codigoProceso'];
                              echo "<hr style='margin:0px;margin-top:5px;clear:both;border:1px solid #000;'><p style='font-weigh:bold;font-size:20px;margin-top:5px;margin-top:0px;padding-left:33px;width:100%;'>".$tipo."</p>";
                            }
                            if(isset($_GET['codigo'])){
                                if(in_array($riesgo['codigo'], $riesgos)){
                                  $marcado = $riesgo['codigo'];
                                }
                            } else {
                              $marcado = $riesgo['codigo'];
                            }
                            if($riesgo['tipo'] == 'RIESGO'){
                              $nombre = $riesgo['nombre'].' <b>(R)</b>';
                            } else {
                              $nombre = $riesgo['nombre'].' <b>(O)</b>';
                            }
                            abreColumnaCampos('span3');
                            campoCheckIndividual('codigoRiesgo[]',$nombre,$marcado,$riesgo['codigo']);
                            cierraColumnaCampos();
                          }
                        campoOculto($codigoCliente,'codigoCliente');
                        cierraBD();
                      ?>
                      <br clear="all">
                      <br />
                      <div style='margin-left:33px;'>
                      <a href="index.php" class="btn btn-default"><i class="icon-chevron-left"></i> Volver</a>
                      <button type="submit" class="btn btn-propio">Continuar <i class="icon-chevron-right"></i></button>
                      </div>
                    </form>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/funciones25.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('select').selectpicker();
  });
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
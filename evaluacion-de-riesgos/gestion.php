<?php
  $seccionActiva = 61;
  include_once("../cabecera.php");
  gestionEvaluacion();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/funciones25.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js"></script>
<script src="../../api/js/funciones.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.selectpicker').selectpicker();
  $(":file").filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
	
  $(document).on('click', '.btnSiguiente', function(e) {    
    $('.nav-tabs > .active').next('li').find('a').trigger('click');
      $('html, body').animate({scrollTop:0}, 'slow');
  });

  $(document).on('click', '.btnAnterior', function(e) {    
    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
      $('html, body').animate({scrollTop:0}, 'slow');
  });

  mostrarTablaIntereses();
  $(document).on('change', '#legitimacion', function(e){
    mostrarTablaIntereses();
  });

  calculaAmenazas();
  oyenteAmenazas();
  
});

function mostrarTablaIntereses() {
  if ($('#legitimacion').val() == 5) {
    $('#divTablaIntereses').removeClass('hide');
  }
  else {
    $('#divTablaIntereses').addClass('hide');
  }

}

function oyenteAmenazas() {

  $('#tablaAmenazas input[type=radio], .riesgoR').on('change', function(e) {    
    calculaAmenazas();
  });

}

function calculaAmenazas() {

  var aceptable = true;

  $('#tablaAmenazas tbody tr').each(function(e) { 
    var amenaza = $(this).find('input[type=radio]:checked');
    var riesgo  = $(this).find('.riesgoR');

    if (aceptable) {
      if ($(amenaza).val() != 'NO'   && 
          ($(riesgo).val() != 'Bajo' &&
           $(riesgo).val() != 'Medio')
      ) {        
        aceptable = false;
      }
    }

  });

  if (aceptable) {
    $('#resultado').val('aceptable');
    var texto = "<h3>RESULTADO ACEPTABLE</h3><p>El resultado de la presente gestión de riesgos deberá ser validado por el responsable del tratamiento, mediante la firma del documento que recoja el resultado del análisis realizado.</p>";
  } 
  else {
    $('#resultado').val('no_aceptable');
    var texto = "<h3>RESULTADO NO ACEPTABLE</h3><p>Revise los riesgos identificados y adopte las medidas de control necesarias hasta el punto de disminuir el riesgo residual a un grado Bajo ó Medio.</p>";
  }

  $('#textoResultado').empty();
  $('#textoResultado').append(texto);

}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
  $seccionActiva = 60;
  include_once("../cabecera.php");
  gestionInteresados();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

  tipoRespuesta($('#tipoRespuesta').val());
  $('#tipoRespuesta').on('change', function(e) {
    var opcion = $(this).val();    
    
    tipoRespuesta(opcion);
  });

  $('.btnModal').unbind();
	$(document).on('click', '.btnModal', function(e){
    var consulta = $.post('../listadoAjax.php?include=zc-derecho-portabilidad&funcion=textoModalAJAX();',{'opcion':$(this).attr('id')}
      ,function(respuesta){
        $('#textoModal').empty();
        $('#textoModal').append(respuesta);

        $('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
    });
  });

  medioElectronico($('input[name=medioElectronico]:checked').val());
  $('input[name=medioElectronico]').on('change', function(e) {
    var opcion = $('input[name=medioElectronico]:checked').val();
    medioElectronico(opcion);    
  });

  otroMedio($('input[name=otroMedioComunicacion]:checked').val());
  $('input[name=otroMedioComunicacion]').on('change', function(e) {
    var opcion = $('input[name=otroMedioComunicacion]:checked').val();
    otroMedio(opcion);    
  });
  
});

function tipoRespuesta(opcion) {
  var enlace = '';
    
  $('.divInformacion').empty();

  switch (opcion) {
    case 'supuesto1':
      $('.divResponsable').addClass('hide');
      enlace = '<a id="articulo11" style="margin-left:85px" class="btnModal noAjax">Artículo 11 RGPD</a><br><a id="articulo12" style="margin-left:85px" class="btnModal noAjax">Artículo 12.6 RGPD</a>';
      break;
    case 'supuesto3':
      $('.divResponsable').removeClass('hide');
      break;
    case 'supuesto5':
      $('.divResponsable').addClass('hide');
      enlace = '<a id="articulo20" style="margin-left:85px" class="btnModal noAjax">Artículo 20 RGPD</a>';
      break;
    case 'supuesto6':
      $('.divResponsable').addClass('hide');
      enlace = '<a id="articulo20" style="margin-left:85px" class="btnModal noAjax">Artículo 20 RGPD</a>';
      break;
    default:
      $('.divResponsable').addClass('hide');
      break;
  }
  
  $('.divInformacion').append(enlace);

}

function medioElectronico(opcion) {
  if (opcion == 'SI') {
      $('#divOtroMedio').removeClass('hide');
    } else {
      $('#divOtroMedio').addClass('hide');
      $('input[name=otroMedioComunicacion][value="NO"]').attr('checked', true);
      $('#divOtroMedioTexto').addClass('hide');
      $('#otroMedioTexto').val('');
    }
}

function otroMedio(opcion) {
  if (opcion == 'SI') {
      $('#divOtroMedioTexto').removeClass('hide');
    } else {
      $('#divOtroMedioTexto').addClass('hide');
      $('#divOtroMedioTexto').val('');
      $('input[name=otroMedioComunicacion][value="NO"]').attr('checked', true);
    }
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesInteresados(){
	$res = true;

	if(isset($_POST['codigo'])){
		$res = actualizaInteresado();
	}
	elseif(isset($_POST['nombre'])){
		$res = insertaInteresado();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res = eliminaDatos('derecho_portabilidad');
	}

	mensajeResultado('nombre', $res, 'Interesado');
    mensajeResultado('elimina', $res, 'Interesado', true);
}

function insertaInteresado(){
	$res = true;
	responsable();
	$res = insertaDatos('derecho_portabilidad');
	return $res;
}

function actualizaInteresado(){
	$res = true;
	responsable();
	$res = actualizaDatos('derecho_portabilidad');
	return $res;
}

function responsable(){
	$_POST['responsable'] = '';
	$_POST['responsableDireccion'] = '';
	
	if($_POST['tipoRespuesta'] == 'supuesto3' && $_POST['selectResponsable'] != 'NULL') {
		$i = $_POST['selectResponsable'];
		$_POST['responsable'] = $_POST['responsable'.$i];
		$_POST['responsableDireccion'] = $_POST['direccion'.$i];
	}
}

function listadoInteresados(){
	global $_CONFIG;

	$columnas = array(
		'referencia',
		'nombre',
		'fechaRecep',
		'fechaRespu',
		'tipoRespuesta'
	);

	$respuestas = array(
		'' => '',
		'supuesto1' => 'La solicitud no reúne requisitos de identificación',
		'supuesto2' => 'No figuran datos personales del interesado',		
		'supuesto3' => 'Comunicación al Responsable de Tratamientos',
		'supuesto4' => 'Datos personales bloqueados',
		'supuesto5' => 'Denegación a la portabilidad de los datos ejercitada',
		'supuesto6' => 'Otorgamiento a la portabilidad de datos ejercitada',
	);	
	
	$cliente = obtenerCodigoCliente(true);
	$having  = obtieneWhereListado("HAVING codigoCliente=".$cliente, $columnas);
	$orden   = obtieneOrdenListado($columnas);
	$limite  = obtieneLimitesListado();
	
	$sql = "SELECT 
				*,
				DATE_FORMAT(fechaRecepcion, '%d/%m/%Y') AS fechaRecep,
				DATE_FORMAT(fechaRespuesta, '%d/%m/%Y') AS fechaRespu
			FROM
				derecho_portabilidad
			$having";
	
	conexionBD();
	$consulta = consultaBD($sql." $orden $limite;");
	$consultaPaginacion=consultaBD($sql);
	cierraBD();

	$res = inicializaArrayListado($consulta, $consultaPaginacion);
	while($datos = mysql_fetch_assoc($consulta)){

		$fila=array(
			"<div class='centro'>".$datos['referencia']."</div>",
			$datos['nombre'],
			"<div class=centro>".$datos['fechaRecep']."</div>",
			"<div class=centro>".$datos['fechaRespu']."</div>",
			$respuestas[$datos['tipoRespuesta']],
			botonListado($datos),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function botonListado($datos) {

	$opciones = array('Detalles');
	$enlaces  = array("zc-derecho-portabilidad/gestion.php?codigo=".$datos['codigo']);
	$iconos   = array('icon-search-plus');
	$clase    = array(0);

	if(isset($datos['tipoRespuesta']) && $datos['tipoRespuesta'] != '') {
		array_push($opciones, 'Descargar respuesta');
		array_push($enlaces, "zc-derecho-portabilidad/generaRespuesta.php?codigo=".$datos['codigo']);
		array_push($iconos, 'icon-cloud-download');
		array_push($clase, 2);
	}

	if (count($opciones) > 1) {
		$res = botonAcciones($opciones, $enlaces, $iconos, $clase);
	} else {
		$res = creaBotonDetalles($enlaces[0]);
	}

	return $res;
}

function gestionInteresados() {

	$camposSelectNombres = array(
		'',
		'La solicitud no reúne requisitos de identificación -artículo 11 y 12.6 RGPD-',
		'No figuran datos personales del interesado',		
		'Comunicación al Responsable de Tratamientos',
		'Datos personales bloqueados',
		'Denegación a la portabilidad de los datos ejercitada -artículo 20 RGPD-',
		'Otorgamiento a la portabilidad de datos ejercitada -artículo 20 RGPD-',		
	);
	$camposSelectvalores = array(
		'',
		'supuesto1',
		'supuesto2',
		'supuesto3',
		'supuesto4',
		'supuesto5',
		'supuesto6',
	);

	operacionesInteresados();

	abreVentanaGestion('Gestión de Interesados que ejercen el derecho a la portabilidad de los datos','?','','icon-edit','margenAb');
	
		if(isset($_GET['codigo'])) {
			if(compruebaCliente($_GET['codigo'], 'derecho_portabilidad')) {
				$datos = compruebaDatos('derecho_portabilidad');
				$referencia = $datos['referencia'];
			}
		} else {
			$datos = false;
			$consulta = consultaBD("SELECT IFNULL(MAX(CAST(referencia AS DECIMAL)), 0) + 1 AS n FROM derecho_portabilidad WHERE codigoCliente = ".obtenerCodigoCliente(true), true, true);
			$referencia = $consulta['n'];
		}
			
		abreColumnaCampos();
			campoOculto($datos, 'codigoCliente', obtenerCodigoCliente(true));
			campoTexto('referencia', 'Referencia', $referencia, 'input-mini');
			campoTexto('nombre', 'Nombre y apellidos del interesado', $datos, 'span7');
			campoTexto('nif', 'NIF del interesado', $datos, 'input-small');		
			campoTexto('nombre_representante', 'Representante legal', $datos, 'span7');
			campoTexto('nif_representante', 'NIF del representante legal', $datos, 'input-mini');
			campoTexto('direccion_notificaciones', 'Dirección a efecto de notificaciones', $datos, 'span7');
   			campoTexto('email', 'Correo electrónico', $datos, 'span7');
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoFecha('fechaRecepcion', 'Fecha de Recepción', $datos);
		cierraColumnaCampos();
		abreColumnaCampos();
			campoFecha('fechaRespuesta', 'Fecha de Respuesta', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoRadio('medioElectronico', 'La solicitud se ha presentado por medios electrónicos', $datos);
		cierraColumnaCampos(true);

		abreColumnaCampos();
			echo '<div id="divOtroMedio" class="hide">';
				campoRadio('otroMedioComunicacion', 'El interesado solicita que se le responda por otros medios diferentes al correo electrónico', $datos);
			echo '</div>';
		cierraColumnaCampos();

		abreColumnaCampos();
			echo '<div id="divOtroMedioTexto" class="hide">';
				campoTexto('otroMedioTexto','Indicar',$datos);
			echo '</div>';
		cierraColumnaCampos(true);

		abreColumnaCampos();
			campoSelect('tipoRespuesta', 'Respuesta', $camposSelectNombres, $camposSelectvalores, $datos, 'selectpicker span6 show-tick');
		cierraColumnaCampos(true);

		abreColumnaCampos('span3 divInformacion');
			
		cierraColumnaCampos(true);

		abreColumnaCampos('span3 divResponsable hide');
			// Esto viene heredado del desarrollador antiguo, los responsables no tienen tabla propia
			// y tenemos que recuperar los datos de los registros
			$responsables = array();
			$direcciones  = array();
			
			conexionBD();
			$provincias = array(
				''   => '',
				'01' => 'Álava',
				'02' => 'Albacete',
				'03' => 'Alicante',
				'04' => 'Almería',
				'05' => 'Ávila',
				'06' => 'Badajoz',
				'07' => 'Illes Balears',
				'08' => 'Barcelona',
				'09' => 'Burgos',
				'10' => 'Cáceres',
				'11' => 'Cádiz',
				'12' => 'Castellón de la Plana',
				'13' => 'Ciudad Real',
				'14' => 'Córdoba',
				'15' => 'A coruña',
				'16' => 'Cuenca',
				'17' => 'Girona',
				'18' => 'Granada',
				'19' => 'Guadalajara',
				'20' => 'Guipúzcoa',
				'21' => 'Huelva',
				'22' => 'Huesca',
				'23' => 'Jaén',
				'24' => 'León',
				'25' => 'Lleida',
				'26' => 'La Rioja',
				'27' => 'Lugo',
				'28' => 'Madrid',
				'29' => 'Málaga',
				'30' => 'Murcia',
				'31' => 'Navarra',
				'32' => 'Ourense',
				'33' => 'Asturias',
				'34' => 'Palencia',
				'35' => 'Las Palmas',
				'36' => 'Pontevedra',
				'37' => 'Salamanca',
				'38' => 'Santa Cruz de Tenerife',
				'39' => 'Cantabria',
				'40' => 'Segovia',
				'41' => 'Sevilla',
				'42' => 'Soria',
				'43' => 'Tarragona',
				'44' => 'Teruel',
				'45' => 'Toledo',
				'46' => 'Valencia',
				'47' => 'Valladolid',
				'48' => 'Vizcaya',
				'49' => 'Zamora',
				'50' => 'Zaragoza',
				'51' => 'Ceuta',
				'52' => 'Melilla'
			);

		    $declaraciones = consultaBD('SELECT * FROM declaraciones WHERE codigoCliente = '.obtenerCodigoCliente(false));
			while ($d = mysql_fetch_assoc($declaraciones)){
				if (!in_array($d['n_razon'], $responsables)) {
					array_push($responsables, $d['n_razon']);
					array_push($direcciones, $d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$provincias[$d['provincia_responsableFichero']]);
				}

				$otros = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion = '.$d['codigo']);
				while($o = mysql_fetch_assoc($otros)){
					if ($o['razonSocial'] != '' && !in_array($o['razonSocial'], $responsables)) {
						array_push($responsables, $o['razonSocial']);
						array_push($direcciones, $o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$provincias[$o['provincia']]);
					}
				}
			}
			cierraBD();
			
			$nombres = array('');
			$valores = array('NULL');
			foreach ($responsables as $key => $value) {
				campoOculto($value, 'responsable'.$key);
				array_push($nombres, $value);
				array_push($valores, $key);
			}
			foreach ($direcciones as $key => $value) {
				campoOculto($value, 'direccion'.$key);
			}
			if($datos && $datos['responsable'] != ''){
				campoDato('Responsable de tratamiento', $datos['responsable']);
			}
			campoSelect('selectResponsable','Selecciona responsable',$nombres,$valores);
		cierraColumnaCampos(true);

		abreColumnaCampos();
			areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 
		cierraColumnaCampos(true);
		
		abreVentanaModal('información');
			echo "<div id='textoModal'>";
			echo "</div>";
		cierraVentanaModal('','','',false,'Cerrar');				

	cierraVentanaGestion('index.php',true);
}

function textoModalAJAX() {
	$opcion = $_POST['opcion'];
	$texto  = '';

	switch ($opcion) {
		case 'articulo11':
			$texto = "
				<i><b>Artículo 11 RGPD.</b> Tratamiento que no requiere identificación.</i><br>
				<ol>
					<li><i>Si los fines para los cuales un responsable trata datos personales no requieren o ya no requieren la identificación de un interesado por el responsable, este no estará obligado a mantener, obtener o tratar información adicional con vistas a identificar al interesado con la única finalidad de cumplir el presente Reglamento.</i></li>
					<li><i>Cuando, en los casos a que se refiere el apartado 1 del presente artículo, el responsable sea capaz de demostrar que no está en condiciones de identificar al interesado, le informará en consecuencia, de ser posible. En tales casos no se aplicarán los artículos 15 a 20, excepto cuando el interesado, a efectos del ejercicio de sus derechos en virtud de dichos artículos, facilite información adicional que permita su identificación.</i></<li>
				</ol>
			";
			break;
		case 'articulo12':
			$texto = "
				<i><b>Artículo 12.6 RGPD.</b> 6. Sin perjuicio de lo dispuesto en el artículo 11, cuando el responsable del tratamiento tenga dudas razonables en relación con la identidad de la persona física que cursa la solicitud a que se refieren los artículos 15 a 21, podrá solicitar que se facilite la información adicional necesaria para confirmar la identidad del interesado.</i>
			";
			break;
		case 'articulo20':
			$texto = "
				<i><b>Artículo 20 RGPD.</b> Derecho a la portabilidad de los datos.</b></i><br>
				<ol>
					<li>
						<i>El interesado tendrá derecho a recibir los datos personales que le incumban, que haya facilitado a un responsable del tratamiento, en un formato estructurado, de uso común y lectura mecánica, y a transmitirlos a otro responsable del tratamiento sin que lo impida el responsable al que se los hubiera facilitado, cuando:</i>
						<ol style='list-style-type: lower-latin;'>
							<li><i>el tratamiento esté basado en el consentimiento con arreglo al artículo 6, apartado 1, letra a), o el artículo 9, apartado 2, letra a), o en un contrato con arreglo al artículo 6, apartado 1, letra b), y</i></li>
							<li><i>el tratamiento se efectúe por medios automatizados.</i></li>
						</ol>
					</li>
					<li><i>Al ejercer su derecho a la portabilidad de los datos de acuerdo con el apartado 1, el interesado tendrá derecho a que los datos personales se transmitan directamente de responsable a responsable cuando sea técnicamente posible.</i></li>
					<li><i>El ejercicio del derecho mencionado en el apartado 1 del presente artículo se entenderá sin perjuicio del artículo 17. Tal derecho no se aplicará al tratamiento que sea necesario para el cumplimiento de una misión realizada en interés público o en el ejercicio de poderes públicos conferidos al responsable del tratamiento.</i></li>
					<li><i>El derecho mencionado en el apartado 1 no afectará negativamente a los derechos y libertades de otros.</i></li>
				</ol>
			";
			break;		
		default:
			break;	
	}

	echo $texto;

}

function valoresSelectResponsables() {
	$responsables = array();
	$direcciones  = array();
	
	conexionBD();
	$declaraciones = consultaBD('SELECT * FROM declaraciones WHERE codigoCliente = '.obtenerCodigoCliente(false));
	while ($d = mysql_fetch_assoc($declaraciones)){
		if(!in_array($d['n_razon'], $responsables)){
			array_push($responsables, $d['n_razon']);
			array_push($direcciones, $d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$d['provincia_responsableFichero']);
		}
		$otros = consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion = '.$d['codigo']);
		while($o = mysql_fetch_assoc($otros)){
			if($o['razonSocial'] != '' && !in_array($o['razonSocial'], $responsables)){
				array_push($responsables, $o['razonSocial']);
				array_push($direcciones, $o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$o['provincia']);
			}
		}
	}
	cierraBD();
		
	return $responsables;
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de listado de ventas

/*
	Esta función sirve para eliminar los registros de la tabla ventas_en_facturas cuyo codigoFactura sea NULL.
	Como para que el UNION ALL funcione sin problemas se debe colocar a la segunda consulta un GROUP BY también,
	éste hará que los conceptos de ventas se agrupen en 1, cogiendo como codigoFactura como NULL en caso de que existan registros así.
	Esta función elimina esos registros inservibles para que eso no ocurra
*/
function limpiaServiciosEnFacturas(){
	consultaBD("DELETE FROM ventas_en_facturas WHERE codigoFactura IS NULL",true);
}

function listadoVentas(){
	$columnas=array(
		'fechaVenta',//grupos
		'fechaFacturaListadoVentas',
		'fechaVencimiento',
		'fechaFacturaListadoVentas',//Se sustituirá por las distintas fechas de cobro

		'clientes.razonSocial',
		'accionFormativa',
		'grupo',
		'accion',
		'alumno',
		'fechaInicio',
		'fechaFin',
		'numeroFactura',

		'emisores.razonSocial',
		'precioFactura',//clientes_grupo
		'precioFactura',//clientes_grupo (a la hora de imprimirlo le meto el IVA)
		'SUM(precioComplemento)',//participantes
		'precioFactura-SUM(precioComplemento)',
		'numeroAbono',//series_facturas AS series_abonos, facturas AS abonos
		'complemento',
		'complemento2',//complementos AS complementos2
		'vencimientos_facturas.medioPago',


		'estado',//vencimientos_facturas

		'comercial',
		'colaborador',
		'tele',
		'administrativo',
		'anulado',//Futuro FINALIZADO

		'anulado',
		'fechaInicio',
		'fechaFin',
		'codigoComercial',
		'codigoCategoria',
		'tipo',
		'fechaVenta',
		'tipoVenta',
		'estadoVenta'
	);

	$having=obtieneHavingListadoVentas($columnas);
	$orden=obtieneOrdenListadoVentas($columnas);
	$limite=obtieneLimitesListado();
	
	//Unifico en una misma consulta el las ventas de formación y de servicios, mediante UNION ALL. Casi ná
	/*$query="SELECT grupos.codigo, razonSocial, accionFormativa, grupos.grupo, accion, CONCAT(trabajadores_cliente.nombre,' ',apellido1,' ',apellido2) AS alumno, fechaInicio, fechaFin, 
	precioFactura/clientes_grupo.numParticipantes AS precio, complemento, IFNULL(CONCAT(serie,'/',numero),'-No emitida-') AS numero, 
	IFNULL(estado,'-') AS estado, anulado, vencimientos_facturas.fechaVencimiento, 'FORMACION' AS tipo, comerciales.codigo AS codigoComercial, 
	codigoCategoria, acciones_formativas.codigo AS codigoAccionFormativa, trabajadores_cliente.codigo AS codigoTrabajador, clientes.codigo AS codigoCliente

	FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo
	LEFT JOIN participantes ON grupos.codigo=participantes.codigoGrupo
	LEFT JOIN trabajadores_cliente ON participantes.codigoTrabajador=trabajadores_cliente.codigo
	LEFT JOIN complementos ON participantes.codigoComplemento=complementos.codigo
	LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo
	LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo
	LEFT JOIN grupos_en_facturas ON grupos.codigo=grupos_en_facturas.codigoGrupo
	LEFT JOIN facturas ON grupos_en_facturas.codigoFactura=facturas.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN vencimientos_facturas ON facturas.codigo=vencimientos_facturas.codigoFactura
	LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo

	GROUP BY participantes.codigo $having

	UNION ALL

	SELECT ventas_servicios.codigo, razonSocial, '-' AS accionFormativa, '-' AS grupo, servicio AS accion, '-' AS alumno, ventas_servicios.fecha AS fechaInicio, '0000-00-00' AS fechaFin,
	conceptos_venta_servicios.total AS precio, '-' AS complemento, IFNULL(CONCAT(serie,'/',numero),'-No emitida-') AS numero, 
	IFNULL(estado,'-') AS estado, anulado, vencimientos_facturas.fechaVencimiento, 'VENTA' AS tipo, comerciales.codigo AS codigoComercial, 
	codigoCategoria, '-' AS codigoAccionFormativa, '-' AS codigoTrabajador, clientes.codigo AS codigoCliente

	FROM ventas_servicios LEFT JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio
	LEFT JOIN clientes ON ventas_servicios.codigoCliente=clientes.codigo
	LEFT JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo
	LEFT JOIN ventas_en_facturas ON ventas_servicios.codigo=ventas_en_facturas.codigoVentaServicio
	LEFT JOIN facturas ON ventas_en_facturas.codigoFactura=facturas.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN vencimientos_facturas ON facturas.codigo=vencimientos_facturas.codigoFactura
	LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo

	GROUP BY conceptos_venta_servicios.codigo";*/

	//La columna "precio" no se usa actualmente (se usaba, y se ha dejado por si la vuelven ha pedir)
	$query="SELECT grupos.codigo, clientes.razonSocial, accionFormativa, grupos.grupo, accion, 
	CONCAT(trabajadores_cliente.nombre,' ',apellido1,' ',apellido2) AS alumno, 
	fechaInicio, fechaFin, precioFactura/clientes_grupo.numParticipantes AS precio,  
	IFNULL(CONCAT(series_facturas.serie,'/',facturas.numero),'-No emitida-') AS numeroFactura, IFNULL(vencimientos_facturas.estado,'-') AS estado, anulado, 
	vencimientos_facturas.fechaVencimiento, 'FORMACION' AS tipo, comerciales.codigo AS codigoComercial, 
	codigoCategoria, acciones_formativas.codigo AS codigoAccionFormativa, trabajadores_cliente.codigo AS codigoTrabajador, clientes.codigo AS codigoCliente,
	grupos.fechaAlta AS fechaVenta, facturas.fecha AS fechaFacturaListadoVentas, emisores.razonSocial AS emisor, precioFactura, SUM(precioComplemento) AS precioComplemento, 
	complementos.complemento, complementos.complemento AS complemento2, vencimientos_facturas.medioPago, comerciales.nombre AS comercial, 
	colaboradores.nombre AS colaborador, telemarketing.nombre AS tele, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS administrativo,
	IFNULL(CONCAT(series_abonos.serie,'/',abonos.numero),'-') AS numeroAbono, facturas.iva, facturas.codigo AS codigoFactura, participantes.codigo AS codigoParticipante,
	clientes_grupo.numParticipantes, comerciales.codigoUsuarioAsociado, comerciales.codigoComercial, grupos.privado AS tipoVenta, grupos.estado AS estadoVenta

	FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo
	LEFT JOIN participantes ON grupos.codigo=participantes.codigoGrupo
	LEFT JOIN trabajadores_cliente ON participantes.codigoTrabajador=trabajadores_cliente.codigo
	LEFT JOIN complementos_participantes ON participantes.codigo=complementos_participantes.codigoParticipante
	LEFT JOIN complementos ON complementos_participantes.codigoComplemento=complementos.codigo
	LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo AND trabajadores_cliente.codigoCliente=clientes_grupo.codigoCliente
	LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo
	LEFT JOIN facturas ON facturas.codigo=(SELECT codigoFactura FROM grupos_en_facturas LEFT JOIN facturas f2 ON grupos_en_facturas.codigoFactura=f2.codigo WHERE grupos_en_facturas.codigoGrupo=grupos.codigo AND codigoFactura IS NOT NULL AND f2.codigoCliente=clientes.codigo LIMIT 1)
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN vencimientos_facturas ON facturas.codigo=vencimientos_facturas.codigoFactura
	LEFT JOIN vencimientos_facturas vencimientos_aux ON vencimientos_facturas.codigoFactura=vencimientos_aux.codigoFactura AND vencimientos_facturas.codigo<vencimientos_aux.codigo
	LEFT JOIN comerciales ON grupos.codigoComercial=comerciales.codigo
	LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada
	LEFT JOIN series_facturas AS series_abonos ON abonos.codigoSerieFactura=series_abonos.codigo
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN colaboradores ON facturas.codigoColaborador=colaboradores.codigo
	LEFT JOIN telemarketing ON facturas.codigoTelemarketing=telemarketing.codigo
	LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo
	WHERE vencimientos_aux.codigo IS NULL AND grupos.privado!='OBSEQUIO' AND (grupos.estado='VALIDA' OR grupos.estado='ANULADA')
	GROUP BY participantes.codigo $having

	UNION ALL

	SELECT ventas_servicios.codigo, clientes.razonSocial, '-' AS accionFormativa,'-' AS grupo,servicio AS accion,'-' AS alumno,
	'0000-00-00' AS fechaInicio,'0000-00-00' AS fechaFin, '0' AS precio, 
	IFNULL(CONCAT(series_facturas.serie,'/',facturas.numero),'-No emitida-') AS numeroFactura, IFNULL(vencimientos_facturas.estado,'-') AS estado,
	anulado, vencimientos_facturas.fechaVencimiento, 'VENTA' AS tipo, comerciales.codigo AS codigoComercial, codigoCategoria, 
	'-' AS codigoAccionFormativa, '-' AS codigoTrabajador, clientes.codigo AS codigoCliente, ventas_servicios.fecha AS fechaVenta,
	facturas.fecha  AS fechaFacturaListadoVentas, emisores.razonSocial AS emisor, conceptos_venta_servicios.total AS precioFactura,
	SUM(precioComplemento) AS precioComplemento, complementos.complemento, '-' AS complemento2,
	vencimientos_facturas.medioPago, comerciales.nombre AS comercial, colaboradores.nombre AS colaborador, telemarketing.nombre AS tele,
	CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS administrativo, CONCAT(series_abonos.serie,abonos.numero) AS numeroAbono, facturas.iva,
	facturas.codigo AS codigoFactura, '-' AS codigoParticipante, '1' AS numParticipantes, comerciales.codigoUsuarioAsociado, comerciales.codigoComercial,
	tipoVentaServicio AS tipoVenta, ventas_servicios.estado AS estadoVenta

	FROM ventas_servicios LEFT JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio
	LEFT JOIN clientes ON ventas_servicios.codigoCliente=clientes.codigo
	LEFT JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo
	LEFT JOIN ventas_en_facturas ON ventas_servicios.codigo=ventas_en_facturas.codigoVentaServicio
	LEFT JOIN facturas ON ventas_en_facturas.codigoFactura=facturas.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN vencimientos_facturas ON facturas.codigo=vencimientos_facturas.codigoFactura
	LEFT JOIN vencimientos_facturas vencimientos_aux ON vencimientos_facturas.codigoFactura=vencimientos_aux.codigoFactura AND vencimientos_facturas.codigo<vencimientos_aux.codigo
	LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo
	LEFT JOIN complementos_venta_servicios ON ventas_servicios.codigo=complementos_venta_servicios.codigoVentaServicio
	LEFT JOIN complementos ON complementos_venta_servicios.codigoComplemento=complementos.codigo
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN colaboradores ON ventas_servicios.codigoColaborador=colaboradores.codigo
	LEFT JOIN telemarketing ON ventas_servicios.codigoTelemarketing=telemarketing.codigo
	LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada
	LEFT JOIN series_facturas AS series_abonos ON abonos.codigoSerieFactura=series_abonos.codigo
	LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo
	WHERE vencimientos_aux.codigo IS NULL AND tipoVentaServicio!='OBSEQUIO' AND (ventas_servicios.estado='VALIDA' OR ventas_servicios.estado='ANULADA')
	GROUP BY conceptos_venta_servicios.codigo";
	
	//echo $query." $having $orden $limite;";

	conexionBD();
	$consulta=consultaBD($query." $having $orden $limite;");
	//echo $query." $having $orden $limite;<br />";
	$consultaPaginacion=consultaBD($query." $having;");

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$datos=obtieneEnlaceBotonListadoVentas($datos);

		$numParticipantes=$datos['numParticipantes'];
		if($numParticipantes==0){
			$numParticipantes=1;
		}
		$precioFactura=round($datos['precioFactura']/$numParticipantes,2);


		$total=$precioFactura+$precioFactura*$datos['iva']/100;
		$fechasPago=obtieneFechasPagoFactura($datos['codigoFactura']);
		$finalizado=compruebaFinalizacionCursoVenta($datos['tipo'],$datos['fechaFin']);
		$segundoComplemento=obtieneSegundoComplemento($datos['codigoParticipante']);
		$precioComplemento=compruebaVentaObsequio($datos,$total);

		//$fechaVencimiento=obtieneFechaVencimientoConcepto($datos['codigoFactura'],$datos['accion']);

		$facturacionNeta=$total-$precioComplemento;

		$fila=array(
			formateaFechaWeb($datos['fechaVenta']),
			formateaFechaWeb($datos['fechaFacturaListadoVentas']),
			formateaFechaWeb($datos['fechaVencimiento']),
			$fechasPago,
			"<a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."'>".$datos['razonSocial']."</a>",
			$datos['accionFormativa'],
			$datos['grupo'],
			$datos['accion'],
			$datos['alumno'],
			formateaFechaWeb($datos['fechaInicio']),
			formateaFechaWeb($datos['fechaFin']),
			$datos['numeroFactura'],
			$datos['emisor'],
			"<div class='pagination-right'>".formateaNumeroWeb($precioFactura).' €</div>',
			"<div class='pagination-right'>".formateaNumeroWeb($total).' €</div>',
			"<div class='pagination-right'>".formateaNumeroWeb($precioComplemento).' €</div>',
			"<div class='pagination-right'>".formateaNumeroWeb($facturacionNeta).' €</div>',
			$datos['numeroAbono'],
			$datos['complemento'],
			$segundoComplemento,
			$datos['medioPago'],
			$datos['estado'],
			$datos['comercial'],
			$datos['colaborador'],
			$datos['tele'],
			$datos['administrativo'],
			$finalizado,
			$datos['anulado'],
			$datos['boton'],
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}
	cierraBD();

	echo json_encode($res);
}


function obtieneHavingListadoVentas($columnas){
	$having='HAVING 1=1';
	$filtroUsuario=true;

	if(isset($_GET['sSearch_30']) && $_GET['sSearch_30']!=''){
		$codigoComercial=$_GET['sSearch_30'];

		$having=obtieneCondicionFiltradoComercial($codigoComercial);

		unset($_GET['sSearch_30']);//Debe hacerse ANTES de la llamada a obtieneWhereListado(), para que no tenga el cuenta el campo restantes a la hora de construir el WHERE (porque daría fallo por uso incorrecto de función de agrupación)
		$filtroUsuario=false;//Pongo el filtro de usuarios a false para que no interfiera en el filtrado por categorías
	}

	$columnasWhere=$columnas;
	$columnasWhere[30]='comerciales.nombre';//Sustituyo el índice correspondiente a la columna de alumnos restantes por un campo neutro (si lo elimino se desvinculan los campos del filtro)

	$res=obtieneWhereListado($having,$columnasWhere,$filtroUsuario);

	return $res;
}

function compruebaVentaObsequio($datos,$total){//Si la venta de formación es de tipo OBSEQUIO, el importe de la venta se suma al total de complementos (para descontársela al comercial)
	$res=$datos['precioComplemento'];

	if($datos['tipoVenta']=='OBSEQUIO'){
		$res+=$total;
	}

	return $res;
}

function obtieneFechasVencimientoFactura($codigoFactura,$paraExcel=false){
	$res='';

	if($codigoFactura!=NULL){
		$consulta=consultaBD("SELECT fechaVencimiento FROM vencimientos_facturas WHERE codigoFactura=$codigoFactura");
		while($datos=mysql_fetch_assoc($consulta)){
			$res.=formateaFechaWeb($datos['fechaVencimiento']).'<br />';
		}

		if($paraExcel && $res!=''){
			$res=str_replace('<br />',', ',$res);
			$res=quitaUltimaComa($res);
		}
	}

	return $res;
}

function obtieneFechasPagoFactura($codigoFactura){
	$res='';

	if($codigoFactura!=NULL){
		$fechas=consultaArray("SELECT fechaCobro FROM cobros_facturas WHERE codigoFactura=$codigoFactura");
		if(count($fechas)>0){
			foreach ($fechas as $fechaCobro) {
				$res.=formateaFechaWeb($fechaCobro).', ';
			}

			$res=quitaUltimaComa($res);
		}
	}

	return $res;
}

function compruebaFinalizacionCursoVenta($tipo,$fechaFin){
	$res='-';

	if($tipo=='FORMACION'){
		$res='NO';
		if(comparaFechas(fechaBD(),$fechaFin)<1){
			$res='SI';
		}
	}

	return $res;
}

function obtieneOrdenListadoVentas($columnas){
	$res=obtieneOrdenListado($columnas);

	if($res==''){
		$res='ORDER BY fechaVencimiento DESC';//Para que siempre aparezcan los últimos datos rellenos
	}
	else{
		$res.=', fechaVencimiento DESC';
	}

	return $res;
}

function obtieneEnlaceBotonListadoVentas($datos){
	if($datos['tipo']=='FORMACION'){
		$datos['boton']=creaBotonDetalles('inicios-grupos/gestion.php?codigo='.$datos['codigo'],'');
		$datos['accion']="<a href='../acciones-formativas/gestion.php?codigo=".$datos['codigoAccionFormativa']."'>".$datos['accion']."</a>";
		$datos['alumno']="<a href='../trabajadores/gestion.php?codigo=".$datos['codigoTrabajador']."'>".$datos['alumno']."</a>";
	}
	else{
		$datos['boton']=creaBotonDetalles('preventas/gestion.php?codigo='.$datos['codigo'],'');
	}
	
	if($datos['anulado']=='SI' || $datos['estadoVenta']=='ANULADA'){
		$datos['boton'].='<div class="hide"><span class="bloqueado"></span></div>';
	}

	return $datos;
}

function obtieneSegundoComplemento($codigoParticipante){
	$res='-';

	if($codigoParticipante!='-' && $codigoParticipante!=NULL){
		$consulta=consultaBD("SELECT complemento FROM complementos INNER JOIN complementos_participantes ON complementos.codigo=complementos_participantes.codigoComplemento WHERE codigoParticipante=$codigoParticipante LIMIT 1,2",false,true);
		if($consulta){
			$res=$consulta['complemento'];
		}
	}

	return $res;
}

function filtroVentas(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelect(32,'Tipo de venta',array('','Formación','Servicios'),array('','FORMACION','VENTA'),false,'selectpicker span2 show-tick','');
	campoTexto(4,'Cliente');
	campoTexto(5,'Código AF','','input-mini');
	campoTexto(6,'Grupo','','input-mini');
	campoTexto(7,'Curso/Servicio');
	campoTexto(8,'Alumno');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(0,'Fecha venta desde');
	campoFecha(33,'Hasta');
	campoFecha(9,'Fecha inicio desde');
	campoFecha(28,'Hasta');
	campoFecha(10,'Fecha fin desde');
	campoFecha(29,'Hasta');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelect(34,'Tipo de venta',array('','Con coste (servicios)','Privada (formación)','Bonificada (formación)','Obsequio'),array('','NORMAL','PRIVADA','BONIFICADA','OBSEQUIO'),false,'selectpicker span2 show-tick','');
	campoSelectHTML(35,'Estado venta',array('&nbsp;',"<i class='icon-check-circle iconoFactura icon-success'></i> Válida","<i class='icon-times-circle iconoFactura icon-danger'></i> Anulada"),array('','VALIDA','ANULADA'));
	campoTextoSimbolo(14,'Importe','€');
	campoSelect(21,'Estado',array('','Anticipado','Descontado','Impagado definitivo','Impagado gestión de cobro','Pagado','Pendiente pago'),array('','ANTICIPADO','DESCONTADO','IMPAGADO DEFINITIVO','IMPAGADO GESTIÓN DE COBRO','PAGADO','PENDIENTE PAGO'));
	campoSelectSiNoFiltro(26,'Grupo anulado');
	campoSelectComercialFiltroVentas(30);
	//campoSelectConsulta(31,'Categoría',"SELECT codigo, categoria AS texto FROM categorias_agentes WHERE activo='SI';");
	campoSelectConsulta(25,'Administrativa',"SELECT CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS codigo, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS texto FROM usuarios WHERE tipo IN('ADMINISTRACION1','ADMINISTRACION2') AND activo='SI' ORDER BY nombre");

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function generaExcelListadoVentas($documento){
	$documento->setActiveSheetIndex(0);//Selección hoja
	$query=construyeConsultaExcelListadoVentas();
	$fila=2;

	conexionBD();

	$consulta=consultaBD($query);
	while($datos=mysql_fetch_assoc($consulta)){
		$numParticipantes=$datos['numParticipantes'];
		if($numParticipantes==0){
			$numParticipantes=1;
		}
		$precioFactura=round($datos['precioFactura']/$numParticipantes,2);


		$total=$precioFactura+$precioFactura*$datos['iva']/100;

		$fechasPago=obtieneFechasPagoFactura($datos['codigoFactura']);
		$finalizado=compruebaFinalizacionCursoVenta($datos['tipo'],$datos['fechaFin']);
		$segundoComplemento=obtieneSegundoComplemento($datos['codigoParticipante']);
		$precioComplemento=compruebaVentaObsequio($datos,$total);

		$facturacionNeta=$total-$precioComplemento;

		
		$documento->getActiveSheet()->SetCellValue('A'.$fila,formateaFechaWeb($datos['fechaVenta']));
		$documento->getActiveSheet()->SetCellValue('B'.$fila,formateaFechaWeb($datos['fechaFacturaListadoVentas']));
		$documento->getActiveSheet()->SetCellValue('C'.$fila,formateaFechaWeb($datos['fechaVencimiento']));
		$documento->getActiveSheet()->SetCellValue('D'.$fila,$fechasPago);
		$documento->getActiveSheet()->SetCellValue('E'.$fila,$datos['razonSocial']);
		$documento->getActiveSheet()->SetCellValue('F'.$fila,$datos['accionFormativa']);
		$documento->getActiveSheet()->SetCellValue('G'.$fila,$datos['grupo']);
		$documento->getActiveSheet()->SetCellValue('H'.$fila,$datos['accion']);
		$documento->getActiveSheet()->SetCellValue('I'.$fila,$datos['alumno']);
		$documento->getActiveSheet()->SetCellValue('J'.$fila,formateaFechaWeb($datos['fechaInicio']));
		$documento->getActiveSheet()->SetCellValue('K'.$fila,formateaFechaWeb($datos['fechaFin']));
		$documento->getActiveSheet()->SetCellValue('L'.$fila,$datos['numeroFactura']);
		$documento->getActiveSheet()->SetCellValue('M'.$fila,$datos['emisor']);
		$documento->getActiveSheet()->SetCellValue('N'.$fila,round($precioFactura,2));
		$documento->getActiveSheet()->SetCellValue('O'.$fila,round($total,2));
		$documento->getActiveSheet()->SetCellValue('P'.$fila,round($datos['precioComplemento'],2));
		$documento->getActiveSheet()->SetCellValue('Q'.$fila,round($facturacionNeta,2));
		$documento->getActiveSheet()->SetCellValue('R'.$fila,$datos['numeroAbono']);
		$documento->getActiveSheet()->SetCellValue('S'.$fila,$datos['complemento']);
		$documento->getActiveSheet()->SetCellValue('T'.$fila,$segundoComplemento);
		$documento->getActiveSheet()->SetCellValue('U'.$fila,$datos['medioPago']);
		$documento->getActiveSheet()->SetCellValue('V'.$fila,$datos['estado']);
		$documento->getActiveSheet()->SetCellValue('W'.$fila,$datos['comercial']);
		$documento->getActiveSheet()->SetCellValue('X'.$fila,$datos['colaborador']);
		$documento->getActiveSheet()->SetCellValue('Y'.$fila,$datos['tele']);
		$documento->getActiveSheet()->SetCellValue('Z'.$fila,$datos['administrativo']);
		$documento->getActiveSheet()->SetCellValue('AA'.$fila,$finalizado);
		$documento->getActiveSheet()->SetCellValue('AB'.$fila,$datos['anulado']);
			
		if($datos['anulado']=='SI' || $datos['estadoVenta']=='ANULADA'){
			$documento->getActiveSheet()->getStyle('A'.$fila.':AB'.$fila)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFE6E6');//Les pone el fondo morado
		}

		$fila++;
	}
	cierraBD();


	$columnas=array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB');
	for($i=0;$i<count($columnas);$i++){
		$documento->getActiveSheet()->getColumnDimension($columnas[$i])->setAutoSize(true);//Ajusta el ancho de la columna al contenido
	}

	$objWriter=new PHPExcel_Writer_Excel2007($documento);
	$objWriter->save('../documentos/listado-ventas/Listado-ventas-detallado.xlsx');
}


//Esta función contruye una consulta similar a la que se realiza en listadoVentas();
function construyeConsultaExcelListadoVentas(){
	$columnas=array('fechaVenta','fechaFacturaListadoVentas','fechaVencimiento','fechaFacturaListadoVentas','razonSocial','accionFormativa','grupo','accion','alumno','fechaInicio','fechaFin','numeroFactura','emisor','precioFactura','precioFactura','SUM(precioComplemento)','precioFactura-SUM(precioComplemento)','numeroAbono','complemento','complemento2','vencimientos_facturas.medioPago','estado','comerciales.nombre','colaboradores.nombre','telemarketing.nombre','administrativo','anulado','anulado','fechaInicio','fechaFin','codigoComercial','codigoCategoria','tipo','fechaVenta','tipoVenta','estadoVenta');

	$having=obtieneHavingListadoVentasExcel($columnas);
	$limite=obtieneLimitesListado();

	$query="SELECT grupos.codigo, clientes.razonSocial, accionFormativa, grupos.grupo, accion, 
	CONCAT(trabajadores_cliente.nombre,' ',apellido1,' ',apellido2) AS alumno, 
	fechaInicio, fechaFin, precioFactura/clientes_grupo.numParticipantes AS precio,  
	IFNULL(CONCAT(series_facturas.serie,'/',facturas.numero),'-No emitida-') AS numeroFactura, IFNULL(vencimientos_facturas.estado,'-') AS estado, anulado, 
	vencimientos_facturas.fechaVencimiento, 'FORMACION' AS tipo, comerciales.codigo AS codigoComercial, 
	codigoCategoria, acciones_formativas.codigo AS codigoAccionFormativa, trabajadores_cliente.codigo AS codigoTrabajador, clientes.codigo AS codigoCliente,
	grupos.fechaAlta AS fechaVenta, facturas.fecha AS fechaFacturaListadoVentas, emisores.razonSocial AS emisor, precioFactura, SUM(precioComplemento) AS precioComplemento, 
	complementos.complemento, complementos.complemento AS complemento2, vencimientos_facturas.medioPago, comerciales.nombre AS comercial, 
	colaboradores.nombre AS colaborador, telemarketing.nombre AS tele, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS administrativo,
	IFNULL(CONCAT(series_abonos.serie,'/',abonos.numero),'-') AS numeroAbono, facturas.iva, facturas.codigo AS codigoFactura, participantes.codigo AS codigoParticipante,
	clientes_grupo.numParticipantes, comerciales.codigoUsuarioAsociado, comerciales.codigoComercial, grupos.privado AS tipoVenta, grupos.estado AS estadoVenta

	FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo
	LEFT JOIN participantes ON grupos.codigo=participantes.codigoGrupo
	LEFT JOIN trabajadores_cliente ON participantes.codigoTrabajador=trabajadores_cliente.codigo
	LEFT JOIN complementos_participantes ON participantes.codigo=complementos_participantes.codigoParticipante
	LEFT JOIN complementos ON complementos_participantes.codigoComplemento=complementos.codigo
	LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo AND trabajadores_cliente.codigoCliente=clientes_grupo.codigoCliente
	LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo
	LEFT JOIN facturas ON facturas.codigo=(SELECT codigoFactura FROM grupos_en_facturas LEFT JOIN facturas f2 ON grupos_en_facturas.codigoFactura=f2.codigo WHERE grupos_en_facturas.codigoGrupo=grupos.codigo AND codigoFactura IS NOT NULL AND f2.codigoCliente=clientes.codigo LIMIT 1)
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN vencimientos_facturas ON facturas.codigo=vencimientos_facturas.codigoFactura
	LEFT JOIN vencimientos_facturas vencimientos_aux ON vencimientos_facturas.codigoFactura=vencimientos_aux.codigoFactura AND vencimientos_facturas.codigo<vencimientos_aux.codigo
	LEFT JOIN comerciales ON grupos.codigoComercial=comerciales.codigo
	LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada
	LEFT JOIN series_facturas AS series_abonos ON abonos.codigoSerieFactura=series_abonos.codigo
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN colaboradores ON facturas.codigoColaborador=colaboradores.codigo
	LEFT JOIN telemarketing ON facturas.codigoTelemarketing=telemarketing.codigo
	LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo
	WHERE vencimientos_aux.codigo IS NULL AND grupos.privado!='OBSEQUIO'
	GROUP BY participantes.codigo $having

	UNION ALL

	SELECT ventas_servicios.codigo, clientes.razonSocial, '-' AS accionFormativa,'-' AS grupo,servicio AS accion,'-' AS alumno,
	'0000-00-00' AS fechaInicio,'0000-00-00' AS fechaFin, '0' AS precio, 
	IFNULL(CONCAT(series_facturas.serie,'/',facturas.numero),'-No emitida-') AS numeroFactura, IFNULL(vencimientos_facturas.estado,'-') AS estado,
	anulado, vencimientos_facturas.fechaVencimiento, 'VENTA' AS tipo, comerciales.codigo AS codigoComercial, codigoCategoria, 
	'-' AS codigoAccionFormativa, '-' AS codigoTrabajador, clientes.codigo AS codigoCliente, ventas_servicios.fecha AS fechaVenta,
	facturas.fecha  AS fechaFacturaListadoVentas, emisores.razonSocial AS emisor, conceptos_venta_servicios.total AS precioFactura,
	SUM(precioComplemento) AS precioComplemento, complementos.complemento, '-' AS complemento2,
	vencimientos_facturas.medioPago, comerciales.nombre AS comercial, colaboradores.nombre AS colaborador, telemarketing.nombre AS tele,
	CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS administrativo, CONCAT(series_abonos.serie,abonos.numero) AS numeroAbono, facturas.iva,
	facturas.codigo AS codigoFactura, '-' AS codigoParticipante, '1' AS numParticipantes, comerciales.codigoUsuarioAsociado, comerciales.codigoComercial,
	tipoVentaServicio AS tipoVenta, ventas_servicios.estado AS estadoVenta

	FROM ventas_servicios LEFT JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio
	LEFT JOIN clientes ON ventas_servicios.codigoCliente=clientes.codigo
	LEFT JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo
	LEFT JOIN ventas_en_facturas ON ventas_servicios.codigo=ventas_en_facturas.codigoVentaServicio
	LEFT JOIN facturas ON ventas_en_facturas.codigoFactura=facturas.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN vencimientos_facturas ON facturas.codigo=vencimientos_facturas.codigoFactura
	LEFT JOIN vencimientos_facturas vencimientos_aux ON vencimientos_facturas.codigoFactura=vencimientos_aux.codigoFactura AND vencimientos_facturas.codigo<vencimientos_aux.codigo
	LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo
	LEFT JOIN complementos_venta_servicios ON ventas_servicios.codigo=complementos_venta_servicios.codigoVentaServicio
	LEFT JOIN complementos ON complementos_venta_servicios.codigoComplemento=complementos.codigo
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN colaboradores ON ventas_servicios.codigoColaborador=colaboradores.codigo
	LEFT JOIN telemarketing ON ventas_servicios.codigoTelemarketing=telemarketing.codigo
	LEFT JOIN facturas AS abonos ON facturas.codigo=abonos.codigoFacturaAsociada
	LEFT JOIN series_facturas AS series_abonos ON abonos.codigoSerieFactura=series_abonos.codigo
	LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo
	WHERE vencimientos_aux.codigo IS NULL AND tipoVentaServicio!='OBSEQUIO'
	GROUP BY conceptos_venta_servicios.codigo";
	

	return $query." $having $limite;";
}


function obtieneWhereListadoExcelVentas($where,$columnas){
	$datos=arrayFormulario();
	$camposFecha=array();

    $res=obtieneWhereEjercicioListado($columnas,false);

    for($i=0; $i<count($columnas) ; $i++){
        if(isset($datos['global']) && $datos['global']!='') {//Búsqueda normal (global)
            $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$datos['global']."%' USING latin1) AS binary) USING utf8)  OR  ";
        }

        if(isset($datos[$i]) && $datos[$i]!='' && $datos[$i]!='NULL'){//Condición extra con respecto a función original: $datos[$i]!='NULL'
            if(substr_count($columnas[$i],'fecha')>0){
                $datos[$i]=formateaFechaBD($datos[$i]);
            }

            if($datos[$i]=='ISNUL'){
                $res.=$columnas[$i]." IS NULL AND ";
            }
            elseif($datos[$i]=='NOTNUL'){
                $res.=$columnas[$i]." IS NOT NULL AND ";
            }
            elseif(substr_count($columnas[$i],'fecha')>0 && !in_array($columnas[$i],$camposFecha)){
                $res.=$columnas[$i].">='".$datos[$i]."' AND ";
                
                array_push($camposFecha,$columnas[$i]);
            }
            elseif(substr_count($columnas[$i],'fecha')>0){
                $res.=$columnas[$i]."<='".$datos[$i]."' AND ";
            }
            elseif(substr_count($columnas[$i],'precio')>0 || substr_count($columnas[$i],'importe')>0){
                $datos[$i]=formateaNumeroFiltro($datos[$i]);
                $res.=$columnas[$i]." LIKE '%".$datos[$i]."%' AND ";
            }
            else{
                $res.="CONVERT(CAST(CONVERT(".$columnas[$i]." USING latin1) AS binary) USING utf8) LIKE CONVERT(CAST(CONVERT('%".$datos[$i]."%' USING latin1) AS binary) USING utf8) AND ";//La búsqueda por columnas es con AND, para que los filtros sean acumulativos
            }
        }
    }

    $res=substr_replace($res,'',-4);
    $res.=')';

    if($res==' )'){//No se ha realizado búsquedas...
        $res='';
    }

    return $where.$res;
}


function obtieneHavingListadoVentasExcel($columnas){
	$having='HAVING 1=1';
	$filtroUsuario=true;

	if(isset($_POST['30']) && $_POST['30']!='NULL' && $_POST['30']!=''){
		$codigoComercial=$_POST['30'];

		$having=obtieneCondicionFiltradoComercial($codigoComercial);

		unset($_POST['30']);//Debe hacerse ANTES de la llamada a obtieneWhereListado(), para que no tenga el cuenta el campo restantes a la hora de construir el WHERE (porque daría fallo por uso incorrecto de función de agrupación)
		$filtroUsuario=false;//Pongo el filtro de usuarios a false para que no interfiera en el filtrado por categorías
	}

	$columnasWhere=$columnas;
	$columnasWhere[30]='comerciales.nombre';//Sustituyo el índice correspondiente a la columna de alumnos restantes por un campo neutro (si lo elimino se desvinculan los campos del filtro)

	$res=obtieneWhereListadoExcel($having,$columnasWhere);

	return $res;
}

//Fin parte de listado de ventas
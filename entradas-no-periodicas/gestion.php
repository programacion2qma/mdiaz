<?php
  $seccionActiva=9;
  include_once("../cabecera.php");
  gestionEntradasNoPeriodicas();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('select').selectpicker();

	oyenteOtrasTutorias();
	$('input[name=checkTipoTutoria3]').change(function(){
		oyenteOtrasTutorias();
	});
});

function oyenteOtrasTutorias(){
	if($('input[name=checkTipoTutoria3]').prop('checked')){
		$('#otrasTutorias').removeClass('hide');
	}
	else{
		$('#otrasTutorias').addClass('hide');	
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
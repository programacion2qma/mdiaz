<?php
  $seccionActiva=9;
  include_once('../cabecera.php');
  
  $nombreListado=obtieneNombreListado();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
		
        <?php
          creaBotonesGestionListadoTutor();
        ?>

        <div class="span12">
          <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                <h3>Listado de cursos <?php echo $nombreListado; ?></h3>
                <div class="pull-right">
                  <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
                </div>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <?php
                  filtroCursosTutor();
                ?>
                <table class="table table-striped table-bordered datatable" id="tablaCursos">
                  <thead>
                    <tr>
                      <th> Fecha </th>
                      <th> Acción </th>
                      <th> Modalidad </th>
                      <th> Código AF </th>
                      <th> Grupo </th>
                      <th> Nº participantes </th>
                      <th> F. Inicio </th>
                      <th> F. Fin </th>
                      <th> Anulado </th>
                      <th class="centro"></th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
              <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    listadoTabla('#tablaCursos','../listadoAjax.php?include=tutores&funcion=listadoCursosTutor("<?php echo $_GET["codigo"]; ?>");');
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaCursos');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
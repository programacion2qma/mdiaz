<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de trabajadores

function operacionesTrabajadores(){
	$res=true;

	if(isset($_POST['codigo'])){
		formateaCamposTrabajadores();
		$res=actualizaDatos('trabajadores_cliente');
	}
	elseif(isset($_POST['nombre'])){
		$res=creaTrabajador();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('trabajadores_cliente');
	}

	mensajeResultado('nombre',$res,'Trabajador');
    mensajeResultado('elimina',$res,'Trabajador', true);
}

function creaTrabajador(){
	formateaCamposTrabajadores();
	$res=insertaDatos('trabajadores_cliente');
	
	if($res){
		//enviaMensajeCredenciales();
	}

	return $res;
}

/*function creaEstadisticasTrabajadores(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='ADMINISTRACION2'){
    	$res=consultaBD("SELECT COUNT(trabajadores_cliente.codigo) AS total FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo WHERE codigoUsuario=$codigoUsuario",true,true);
    }
    else{
    	$res=estadisticasGenericas('trabajadores_cliente');
    }

    return $res;
}*/

function enviaMensajeCredenciales(){
	$datos=arrayFormulario();
	if($datos['sexo']=='HOMBRE'){
		$mensaje='Estimado ';
	}
	else{
		$mensaje='Estimada ';
	}

	$curso=obtieneDatosCurso($datos['codigoGrupo']);//TODO: desarrollar función según las especificaciones de formato de mensaje enviadas por Nadia


	$headers="From: tutor@laacademiaempresas.com\r\n";
	$headers.= "MIME-Version: 1.0\r\n";

	$mensaje.=$datos['nombre']."
			<br /><br />
			Nos ponemos en contacto contigo para facilitarte las claves de acceso al curso de IMPLANTACION DE LA LOPD EN LA EMPRESA que vas a realizar con nosotros.

			A continuación te indicamos los pasos que tienes que seguir para entrar en nuestra plataforma y poder iniciar el curso:

			1. Entrar en: http://campusvirtual.laacademiaempresas.com 
			2. Introducir el usuario y la contraseña de acceso a la plataforma:
					Usuario: ".$datos['usuarioPlataforma']."
					Contraseña: ".$datos['clavePlataforma']."

			La fecha de inicio de tu curso es el 27/06/2016 y tienes hasta el 31/08/2016 para poder terminarlo. 

			L@s tutor@s que te ayudarán a resolver todas las dudas que se te presenten son: Esther Calmeau y Marina Jiménez

			Te damos bienvenida al campus y te animamos a que comiences a formarte desde el primer día.

			Para cualquier duda relacionada con el curso o con el acceso a la plataforma puedes ponerte en contacto con nosotros a través del siguiente correo electrónico: tutor@laacademiaempresas.com o en el teléfono: 943 31 44 12.

			Un saludo.";	
	

	
	mail($cliente['email'],'Envío de Factura - Dental MG', $mensaje ,$headers);
}

function formateaCamposTrabajadores(){
	formateaPrecioBDD('salarioBruto');
	formateaPrecioBDD('costeHora');
	compruebaRadiosTrabajador();
}

function listadoTrabajadores(){
	global $_CONFIG;

	$columnas=array('trabajadores_cliente.nombre','apellido1','apellido2','nif','razonSocial','niss','trabajadores_cliente.codigoCliente','trabajadores_cliente.activo','codigoUsuario');
	$having=obtieneWhereListado("HAVING 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	$query="SELECT trabajadores_cliente.codigo AS codigoTrabajador, trabajadores_cliente.nombre, apellido1, apellido2, nif, razonSocial, niss, trabajadores_cliente.codigoCliente, trabajadores_cliente.activo, codigoUsuario, codigoUsuarioAsociado, comerciales.codigo
			FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo 
			LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente
			LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo
			GROUP BY trabajadores_cliente.codigo
			$having";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['nombre'],
			$datos['apellido1'],
			$datos['apellido2'],
			$datos['nif'],
			"<a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['razonSocial']."</a>",
			botonAcciones(array('Detalles','Duplicar','Historial formaciones'),array('trabajadores/gestion.php?codigo='.$datos['codigoTrabajador'],'trabajadores/gestion.php?codigo='.$datos['codigoTrabajador'].'&duplicar','trabajadores/historicoFormaciones.php?codigo='.$datos['codigoTrabajador']),array("icon-search-plus","icon-copy","icon-pencil")),
        	obtieneCheckTabla($datos,'codigoTrabajador'),
        	"DT_RowId"=>$datos['codigoTrabajador']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionTrabajador(){
	operacionesTrabajadores();

	abreVentanaGestion('Gestión de Trabajadores','?','span3');
	$datos=compruebaDatosTrabajador();

	campoSelectEmpresaTrabajador($datos);
	campoTextoValidador('nombre','Nombre',$datos,'input-large obligatorio','trabajadores_cliente');
	campoTextoValidador('apellido1','Apellido 1',$datos,'input-large obligatorio','trabajadores_cliente');
	campoTextoValidador('apellido2','Apellido 2',$datos,'input-large','trabajadores_cliente');
	campoTextoValidador('nif','DNI',$datos,'input-small validaDNI obligatorio','trabajadores_cliente');
	campoTextoValidador('niss','NISS (alumno)',$datos,'input-small pagination-right','trabajadores_cliente');
	
	//campoTexto('cuentaSS','C. Cotización (empresa)',$datos,'input-large',true);
	campoSelectCuentaSSTrabajador($datos);

	campoFechaNacimiento($datos);
	campoRadio('sexo','Sexo',$datos,'HOMBRE',array('Hombre','Mujer'),array('HOMBRE','MUJER'));
	campoRadio('discapacidad','Discapacidad',$datos);
	campoRadio('terrorismo','Víctima terrorismo',$datos);
	campoRadio('violencia','Víc. violencia género',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectCategoriaProfesional($datos);
	campoSelectGrupoCotizacion($datos);
	campoSelectNivelEstudios($datos);
	
	campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small','trabajadores_cliente');
	campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',$datos,'input-small','trabajadores_cliente');
	campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large','trabajadores_cliente');

	campoTextoSimbolo('salarioBruto','Salario bruto anual','€',formateaNumeroWeb($datos['salarioBruto']),'input-small pagination-right');
	campoTexto('horasConvenio','Horas convenio',$datos,'input-mini pagination-right');
	campoTextoSimbolo('costeHora','Coste/hora','€',formateaNumeroWeb($datos['costeHora']),'input-small pagination-right');

	campoTexto('usuarioPlataforma','Usuario plataforma',$datos,'input-small');
	campoTexto('clavePlataforma','Contraseña',$datos);

	campoRadio('activo','Activo',$datos,'SI');
	campoOculto(fecha(),'fechaActualizacionTrabajador');
	
	cierraColumnaCampos();
	
	abreColumnaCampos('span10');	
	areaTexto('observacionesTrabajador','Observaciones',$datos);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}

//Hago una función personalizada de comprobación de datos porque existe la opción de "duplicar", en cuyo caso el campoOculto no se crea (para hacer una inserción)
function compruebaDatosTrabajador(){
	if(isset($_REQUEST['codigo']) && !isset($_GET['duplicar'])){
		$datos=datosRegistro('trabajadores_cliente',$_REQUEST['codigo']);
		campoOculto($datos);
	}
	elseif(isset($_REQUEST['codigo']) && isset($_GET['duplicar'])){
		$datos=datosRegistro('trabajadores_cliente',$_REQUEST['codigo']);
	}
	else{
		$datos=false;
	}
	return $datos;
}

function campoSelectEmpresaTrabajador($datos){
	if(!$datos && isset($_GET['codigoEmpresa'])){
		$datos['codigoCliente']=$_GET['codigoEmpresa'];
	}

	campoSelectClienteFiltradoPorUsuario($datos,'Empresa','codigoCliente','obligatorio',true);
}

function filtroTrabajadores(){

	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nombre');
	campoTexto(1,'Apellido 1');
	campoTexto(2,'Apellido 2');
	campoDNI(3,'DNI');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(5,'NISS');
	campoSelectClienteFiltradoPorUsuario(false,'Empresa',6,'',true);
	campoSelectConsulta(8,'Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activo='SI' AND (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') ORDER BY nombre");
	campoSelectSiNoFiltro(7,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();

	filtroEmpresaCliente();
}

function filtroEmpresaCliente(){
	$filtroEmpresa='';
	if(isset($_GET['codigoEmpresa'])){
		$codigoEmpresa=$_GET['codigoEmpresa'];
		$datos=consultaBD("SELECT razonSocial FROM clientes WHERE codigo=$codigoEmpresa",true,true);
		$filtroEmpresa=$datos['razonSocial'];
	}
	divOculto($filtroEmpresa,'filtroEmpresa');
}

function listadoGruposTrabajador($trabajador){
	global $_CONFIG;

	$columnas=array('grupos.fechaAlta','accionFormativa','grupos.grupo','accion','modalidad','grupos.numParticipantes','fechaInicio','fechaFin','anulado','razonSocial','codigoTutor','grupos.privado','grupos.activo','grupos.fechaAlta','fechaInicio','fechaFin');
	$where=obtieneWhereListado("WHERE 1=1",$columnas,true);//Utilizo WHERE por la agrupación de clientes y tutores (el HAVING realiza el filtrado TRAS la selección, y el WHERE ANTES)
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT grupos.codigo, grupos.fechaAlta, accion, modalidad, accionFormativa, grupos.grupo, grupos.numParticipantes, fechaInicio, fechaFin, anulado, razonSocial, codigoTutor, 
			grupos.activo, acciones_formativas.codigo AS codigoAccionFormativa 
			FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo 
			LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo 
			LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo 
			LEFT JOIN tutores_grupo ON grupos.codigo=tutores_grupo.codigoGrupo 
			LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente
			LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo
			LEFT JOIN telemarketing ON clientes.codigoTelemarketing=telemarketing.codigo 
			$where AND grupos.codigo IN(SELECT codigoGrupo FROM participantes WHERE codigoTrabajador='$trabajador') 
			GROUP BY grupos.codigo";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			formateaFechaWeb($datos['fechaAlta']),
			$datos['accionFormativa'],
			$datos['grupo'],
			"<a href='../acciones-formativas/gestion.php?codigo=".$datos['codigoAccionFormativa']."'>".$datos['accion']."</a>",
			$datos['modalidad'],
			$datos['numParticipantes'],
			formateaFechaWeb($datos['fechaInicio']),
			formateaFechaWeb($datos['fechaFin']),
			"<div class='centro'>".$datos['anulado']."</div>",
			creaBotonDetalles("inicios-grupos/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function filtroGrupos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Alta desde','','input-small hasDatepicker');
	campoTexto(14,'Hasta','','input-small hasDatepicker');
	campoTexto(1,'Código AF',false,'input-mini pagination-right');
    campoTexto(2,'Grupo',false,'input-mini pagination-right');
    campoTexto(3,'Acción');
	campoSelect(4,'Modalidad',array('','Presencial','Teleformación','Mixta'),array('','Presencial','Teleformación','Mixta'));
    campoTexto(7,'Inicio desde','','input-small hasDatepicker');
    campoTexto(15,'Hasta','','input-small hasDatepicker');

	cierraColumnaCampos();
	abreColumnaCampos();

    campoTexto(8,'Fin desde','','input-small hasDatepicker');
    campoTexto(16,'Hasta','','input-small hasDatepicker');
	campoTexto(10,'Empresa');
	campoSelectConsulta(11,'Tutor',"SELECT codigo, CONCAT(nombre,' ',apellido1,' ',apellido2) AS texto FROM tutores WHERE activo='SI' ORDER BY nombre;");
	campoSelectSiNoFiltro(12,'Venta privada');
	campoSelectSiNoFiltro(9,'Anulado');
	campoSelect(13,'Activo',array('','Si','Pendiente','No'),array('','SI','PENDIENTE','NO'),false,'selectpicker span2 show-tick');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function consultaDuplicidadTrabajador(){
	$res='OK';
	$datos=arrayFormulario();

	$nombre=limpiaCadena($datos['nombre']);
	$apellido1=limpiaCadena($datos['apellido1']);
	$apellido2=limpiaCadena($datos['apellido2']);
	$whereNombre='';
	if($nombre!='' || $apellido1!='' || $apellido2!=''){
		$whereNombre="(nombre='$nombre' AND apellido1='$apellido1' AND apellido2='$apellido2')";
	}

	$nif=$datos['nif'];
	$whereNif='';
	if($nif!=''){
		$whereNif="OR nif='$nif'";
	}

	$niss=$datos['niss'];
	$whereNiss='';
	if($niss!=''){
		$whereNiss="OR niss='$niss'";
	}

	$whereCodigo='';
	if($datos['codigo']!='NO'){
		$whereCodigo='AND trabajadores_cliente.codigo!='.$datos['codigo'];
	}


	if($whereNombre!='' || $whereNif!='' || $whereNiss!=''){
		$consulta=consultaBD("SELECT nombre, apellido1, apellido2, razonSocial FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE ($whereNombre $whereNif $whereNiss) $whereCodigo",true,true);
		if($consulta){
			$res=$consulta['nombre'].' '.$consulta['apellido1'].' '.$consulta['apellido2'].' de la empresa '.$consulta['razonSocial'];
		}
	}

	echo $res;
}

//Fin parte de trabajadores
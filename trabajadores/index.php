<?php
  $seccionActiva=3;
  include_once('../cabecera.php');
  
  operacionesTrabajadores();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
          creaBotonesGestion();
        ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Trabajadores registrados</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroTrabajadores();//El div utilizado para fnFilter se encuentra en esta función
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaTrabajadores">
                <thead>
                  <tr>
                    <th> Nombre </th>
                    <th> Apellido 1 </th>
                    <th> Apellido 2 </th>
                    <th> DNI </th>
                    <th> Empresa </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>

<script src="../js/selectAjax.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaTrabajadores','../listadoAjax.php?include=trabajadores&funcion=listadoTrabajadores();');
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaTrabajadores');

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    var filtroEmpresa=$('#filtroEmpresa').text();
    if(filtroEmpresa!=''){
      $('#tablaTrabajadores').dataTable().fnFilter(filtroEmpresa);
    }
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
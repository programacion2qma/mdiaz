<?php
  $seccionActiva=3;
  include_once('../cabecera.php');

  $codigoTrabajador=$_GET['codigo'];
  $datosTrabajador=datosRegistro('trabajadores_cliente',$codigoTrabajador);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Formaciones realizadas por el trabajador <?="<a href='gestion.php?codigo=".$codigoTrabajador."'>".$datosTrabajador['nombre'].' '.$datosTrabajador['apellido1'].' '.$datosTrabajador['apellido2']."</a>";?></h3>
              <div class="pull-right">
				        <a href="index.php" type="button" class="btn btn-default btn-small"><i class="icon-chevron-left"></i> Volver</a>
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroGrupos();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaGrupos">
                <thead>
                  <tr>
                    <th> Fecha </th>
                    <th> Acción </th>
                    <th> Modalidad </th>
                    <th> Código AF </th>
                    <th> Grupo </th>
                    <th> Nº participantes </th>
                    <th> F. Inicio </th>
                    <th> F. Fin </th>
                    <th> Anulado </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    
    listadoTabla('#tablaGrupos','../listadoAjax.php?include=trabajadores&funcion=listadoGruposTrabajador(<?=$codigoTrabajador?>);');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaGrupos');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
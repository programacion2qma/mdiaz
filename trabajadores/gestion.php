<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionTrabajador();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	quitaObligatoriedadRadios();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	var flag=true;
	$('#niss').blur(function(){
		if($(this).val().length<12 && flag){
			alert('NISS erróneo: debe tener al menos 12 dígitos');
			flag=false;//Para evitar burlismos en el navegador
		}
	});

	creaBotonGrupos();

	compruebaCreacionDesdeClientes();
	$('#codigoCliente').change(function(){
		oyenteCliente($(this).val());
	});

	$('#salarioBruto,#horasConvenio').blur(function(){
		oyenteCosteHora();
	});

	$('#nombre,#apellido1,#nif').blur(function(){
		creaUsuarioClave();
	});


	//Parte de validación y comprobación de duplicado
	$(':submit').attr('id','enviar').prop('type','button');//Le pongo un ID al submit y le cambio el tipo, para que no salte primero por validador.js
	$('#enviar').unbind();//Elimino los oyentes asignados anteriormente en validador.js
	$('#enviar').click(function(e){
		e.preventDefault();
		compruebaTrabajadorDuplicado();
	});
	//Fin parte de validación y comprobación de duplicado
});

function oyenteCliente(codigoCliente){
	if(codigoCliente!='NULL'){
		var consulta=$.post('../listadoAjax.php?funcion=consultaCuentaSSCliente();',{'codigoCliente':codigoCliente});
		consulta.done(function(respuesta){
			$('#cuentaSS').html(respuesta);
			$('#cuentaSS').selectpicker('refresh');
		});
	}
}

function oyenteCosteHora(){
	var salarioBruto=$('#salarioBruto').val();
	var horasConvenio=$('#horasConvenio').val();

	if(salarioBruto!='' && horasConvenio!=''){
		salarioBruto=formateaNumeroCalculo(salarioBruto);
		horasConvenio=formateaNumeroCalculo(horasConvenio);

		var costeHora=salarioBruto/horasConvenio;
		$('#costeHora').val(formateaNumeroWeb(costeHora));
	}
}

function creaUsuarioClave(){
	var dni=$('#nif').val();
	var nombre=$('#nombre').val();
	var apellido=$('#apellido1').val();

	if(dni.trim()!=''){
		$('#usuarioPlataforma').val(dni.toLowerCase());
	}

	if(nombre.trim()!='' && apellido.trim()!=''){
		var clave=nombre.toLowerCase().substring(0,1);
		clave+=apellido.toLowerCase().replace(/ /g,'');

		$('#clavePlataforma').val(clave);
	}
}

function creaBotonGrupos(){
	if($('#codigo').val()!=undefined){
		var codigo=$('#codigo').val();
		var botones="<div class='pull-right'>";
		botones+="<a href='historicoFormaciones.php?codigo="+codigo+"' class='btn btn-small btn-primary'><i class='icon-pencil'></i> Formación realizada</a>";
		botones+="</div>";

		$('.widget-header h3').after(botones);
	}
}

function compruebaTrabajadorDuplicado(){
	var nombre=$('#nombre').val();
	var apellido1=$('#apellido1').val();
	var apellido2=$('#apellido2').val();
	var nif=$('#nif').val();
	var niss=$('#niss').val();
	
	var codigo=$('#codigo').val();//En la creación será undefined. Sirve para que no dé un falso positivo con el propio registro
	if(codigo==undefined){
		codigo='NO';
	}

	var consulta=$.post('../listadoAjax.php?include=trabajadores&funcion=consultaDuplicidadTrabajador();',{'nombre':nombre,'apellido1':apellido1,'apellido2':apellido2,'nif':nif,'niss':niss,'codigo':codigo});
	consulta.done(function(respuesta){
		if(respuesta=='OK' || (respuesta!='OK' && confirm('Los datos introducidos ya existen en el trabajador '+respuesta+'. ¿Desea continuar?'))){
			validaCamposObligatorios();
		}
	});
}

function compruebaCreacionDesdeClientes(){
	if($('#codigo').val()==undefined){
		oyenteCliente($('#codigoCliente').val());
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesAccion(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaAccion();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=insertaAccion();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('accion_comercial');
	}

	mensajeResultado('codigoCliente',$res,'Acción comercial');
    mensajeResultado('elimina',$res,'Acción comercial', true);
}

function insertaAccion(){
	$res=true;
	if($_POST['fechaProxima']!=''){
		$_POST['horaProxima']= $_POST['horaProxima'] == '' ? '09:00':$_POST['horaProxima'];
		$horaProximaFin=sumaMediaHora($_POST['horaProxima']);
		$res=consultaBD("INSERT INTO tareas VALUES(NULL,'PRÓXIMO CONTACTO','".formateaFechaBD($_POST['fechaProxima'])."','".formateaFechaBD($_POST['fechaProxima'])."','".$_POST['horaProxima']."','".$horaProximaFin."','','".$_POST['codigoCliente']."','".$_POST['codigoUsuario']."');",true);
		$_POST['tareaFechaProxima']=mysql_insert_id();
	} else {
		$_POST['tareaFechaProxima']='NULL';
	}

	if($_POST['fechaExplicacion']!=''){
		$_POST['horaExplicacion']= $_POST['horaExplicacion'] == '' ? '09:00':$_POST['horaExplicacion'];
		$horaExplicacionFin=sumaMediaHora($_POST['horaExplicacion']);
		$res=consultaBD("INSERT INTO tareas VALUES(NULL,'DEMO/EXPLICACIÓN PLATAFORMA','".formateaFechaBD($_POST['fechaExplicacion'])."','".formateaFechaBD($_POST['fechaExplicacion'])."','".$_POST['horaExplicacion']."','".$horaExplicacionFin."','','".$_POST['codigoCliente']."','".$_POST['codigoUsuario']."');",true);
		$_POST['tareaFechaExplicacion']=mysql_insert_id();
	} else {
		$_POST['tareaFechaExplicacion']='NULL';
	}

	if($_POST['fechaNuevaLlamada']!=''){
		$_POST['horaNuevaLlamada']= $_POST['horaNuevaLlamada'] == '' ? '09:00':$_POST['horaNuevaLlamada'];
		$horaNuevaLlamada=sumaMediaHora($_POST['horaNuevaLlamada']);
		$res=consultaBD("INSERT INTO tareas VALUES(NULL,'NUEVO INTENTO DE CONTACTAR','".formateaFechaBD($_POST['fechaNuevaLlamada'])."','".formateaFechaBD($_POST['fechaNuevaLlamada'])."','".$_POST['horaNuevaLlamada']."','".$horaNuevaLlamada."','','".$_POST['codigoCliente']."','".$_POST['codigoUsuario']."');",true);
		$_POST['tareaNuevaLlamada']=mysql_insert_id();
	} else {
		$_POST['tareaNuevaLlamada']='NULL';
	}

	creaCuentaIban();
	$res=insertaDatos('accion_comercial');
	$codigo=mysql_insert_id();
	insertaMantenimiento($codigo);
	insertaPymes($codigo);

	return $res;
}

function actualizaAccion(){
	$res=true;
	if($_POST['fechaProxima']!=''){
		$_POST['horaProxima']= $_POST['horaProxima'] == '' ? '09:00':$_POST['horaProxima'];
		$horaProximaFin=sumaMediaHora($_POST['horaProxima']);
		if(isset($_POST['tareaFechaProxima'])){
			$res=consultaBD('UPDATE tareas SET fechaInicio="'.formateaFechaBD($_POST['fechaProxima']).'",fechaFin="'.formateaFechaBD($_POST['fechaProxima']).'", horaInicio="'.$_POST['horaProxima'].'",horaFin="'.$horaProximaFin.'" WHERE codigo='.$_POST['tareaFechaProxima'],true);
		} else {
			$res=consultaBD("INSERT INTO tareas VALUES(NULL,'PRÓXIMO CONTACTO','".formateaFechaBD($_POST['fechaProxima'])."','".formateaFechaBD($_POST['fechaProxima'])."','".$_POST['horaProxima']."','".$horaProximaFin."','','".$_POST['codigoCliente']."','".$_POST['codigoUsuario']."');",true);
			$_POST['tareaFechaProxima']=mysql_insert_id();
		}
	} else {
		if(isset($_POST['tareaFechaProxima'])){
			$res=consultaBD('DELETE FROM tareas WHERE codigo='.$_POST['tareaFechaProxima'],true);
		}
		$_POST['tareaFechaProxima']='NULL';
	}

	if($_POST['fechaExplicacion']!=''){
		$_POST['horaExplicacion']= $_POST['horaExplicacion'] == '' || $_POST['horaExplicacion'] == '00:00' ? '09:00':$_POST['horaExplicacion'];
		$horaExplicacionFin=sumaMediaHora($_POST['horaExplicacion']);
		if(isset($_POST['tareaFechaExplicacion'])){
			$res=consultaBD('UPDATE tareas SET fechaInicio="'.formateaFechaBD($_POST['fechaExplicacion']).'",fechaFin="'.formateaFechaBD($_POST['fechaExplicacion']).'", horaInicio="'.$_POST['horaExplicacion'].'",horaFin="'.$horaExplicacionFin.'" WHERE codigo='.$_POST['tareaFechaExplicacion'],true);
		} else{
			$res=consultaBD("INSERT INTO tareas VALUES(NULL,'DEMO/EXPLICACIÓN PLATAFORMA','".formateaFechaBD($_POST['fechaExplicacion'])."','".formateaFechaBD($_POST['fechaExplicacion'])."','".$_POST['horaExplicacion']."','".$horaExplicacionFin."','','".$_POST['codigoCliente']."','".$_POST['codigoUsuario']."');",true);
			$_POST['tareaFechaExplicacion']=mysql_insert_id();
		}
	} else {
		if(isset($_POST['tareaFechaExplicacion'])){
			$res=consultaBD('DELETE FROM tareas WHERE codigo='.$_POST['tareaFechaExplicacion'],true);
		}
		$_POST['tareaFechaExplicacion']='NULL';
	}

	if($_POST['fechaNuevaLlamada']!=''){
		$_POST['horaNuevaLlamada']= $_POST['horaNuevaLlamada'] == '' || $_POST['horaNuevaLlamada'] == '00:00' ? '09:00':$_POST['horaNuevaLlamada'];
		$horaNuevaLlamada=sumaMediaHora($_POST['horaNuevaLlamada']);
		if(isset($_POST['tareaNuevaLlamada'])){
			$res=consultaBD('UPDATE tareas SET fechaInicio="'.formateaFechaBD($_POST['fechaNuevaLlamada']).'",fechaFin="'.formateaFechaBD($_POST['fechaNuevaLlamada']).'", horaInicio="'.$_POST['horaNuevaLlamada'].'",horaFin="'.$horaNuevaLlamada.'" WHERE codigo='.$_POST['tareaNuevaLlamada'],true);
		} else{
			$res=consultaBD("INSERT INTO tareas VALUES(NULL,'NUEVO INTENTO DE CONTACTAR','".formateaFechaBD($_POST['fechaNuevaLlamada'])."','".formateaFechaBD($_POST['fechaNuevaLlamada'])."','".$_POST['horaNuevaLlamada']."','".$horaNuevaLlamada."','','".$_POST['codigoCliente']."','".$_POST['codigoUsuario']."');",true);
			$_POST['tareaNuevaLlamada']=mysql_insert_id();
		}
	} else {
		if(isset($_POST['tareaNuevaLlamada'])){
			$res=consultaBD('DELETE FROM tareas WHERE codigo='.$_POST['tareaNuevaLlamada'],true);
		}
		$_POST['tareaNuevaLlamada']='NULL';
	}

	creaCuentaIban();
	$res=actualizaDatos('accion_comercial');
	insertaMantenimiento($_POST['codigo']);
	insertaPymes($_POST['codigo']);
	return $res;
}

function creaCuentaIban(){
	$_POST['cuenta']='';
	$_POST['iban']='';
	$i=0;
	while(isset($_POST['cuenta'.$i])){
		$_POST['cuenta'].=$_POST['cuenta'.$i];
		$i++;
	}
	$i=0;
	while(isset($_POST['iban'.$i])){
		$_POST['iban'].=$_POST['iban'.$i];
		$i++;
	}
}

function sumaMediaHora($hora){
	$horaFin=explode(':',$hora);
	if($horaFin[1]+30 >= 60){
		$horaFin[0]=$horaFin[0]+1;
		$totalMinutos=$horaFin[1]+30;
		$totalMinutos=$totalMinutos-60;
		$horaFin[1]=$totalMinutos;
	} else {
		$horaFin[1]=$horaFin[1]+30;
	}
	return $horaFin[0].':'.$horaFin[1];
}

function insertaMantenimiento($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$res=consultaBD('DELETE FROM accion_mantenimiento WHERE codigoAccion='.$codigo);
	$i=0;
	while(isset($datos['mantenimientoNumero'.$i])){
		$res=consultaBD("INSERT INTO accion_mantenimiento VALUES(NULL,".$codigo.",'".$datos['mantenimientoNumero'.$i]."','".$datos['mesAviso'.$i]."','".$datos['anioAviso'.$i]."','".$datos['mesCurso'.$i]."','".$datos['anioCurso'.$i]."',".$datos['asignado'.$i].",'".$datos['vigor'.$i]."');");
		$i++;
	}

	cierraBD();
	return $res;
}

function insertaPymes($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$res=consultaBD('DELETE FROM accion_pymes WHERE codigoAccion='.$codigo);
	$i=0;
	while(isset($datos['nombrePyme'.$i])){
		$res=consultaBD("INSERT INTO accion_pymes VALUES(NULL,".$codigo.",'".$datos['nombrePyme'.$i]."','".$datos['codCursoPyme'.$i]."','".$datos['nombreCursoPyme'.$i]."','".$datos['fechaCursoPyme'.$i]."','".$datos['numAlumnosPyme'.$i]."');");
		$i++;
	}

	cierraBD();
	return $res;
}

function listadoAccion(){
	$columnas=array('razonSocial','posibleCliente','codigoUsuario');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);//Uso de función personalizada
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT accion_comercial.codigo, accion_comercial.codigoCliente, razonSocial, accion_comercial.codigoUsuario, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS gestor, checkLOPD, checkPBC, checkCompliance, posibleCliente FROM accion_comercial INNER JOIN clientes ON accion_comercial.codigoCliente=clientes.codigo LEFT JOIN usuarios ON accion_comercial.codigoUsuario=usuarios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT accion_comercial.codigo, accion_comercial.codigoCliente, razonSocial, accion_comercial.codigoUsuario, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS gestor, checkLOPD, checkPBC, checkCompliance, posibleCliente FROM accion_comercial INNER JOIN clientes ON accion_comercial.codigoCliente=clientes.codigo LEFT JOIN usuarios ON accion_comercial.codigoUsuario=usuarios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	$iconoValidado=array('NO'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','SI'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['razonSocial'],
			'<div class="centro">'.$iconoValidado[$datos['posibleCliente']].'</div>',
			$datos['gestor'],
			botonAcciones(array('Detalles'),array('accion-comercial/gestion.php?codigo='.$datos['codigo']),array('icon-edit'),false,true,''),
			obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionAccion(){
	operacionesAccion();

	abreVentanaGestion('Gestión de Acción comercial','?','form-horizontal form-acciones','icon-edit','margenAb');
	$datos=compruebaDatos('accion_comercial');
	abreColumnaCampos('span5');
		 campoSelectConsultaAjax('codigoCliente','Cliente',"SELECT codigo, razonSocial AS texto FROM clientes ORDER BY razonSocial;",$datos,'posibles-clientes/gestion.php','selectpicker selectAjax span3 show-tick');
		campoDato('NIF/CIF','','cif');
		campoDato('Localidad/Provincia','','localidad');
		campoDato('Provincia','','provincia');
		campoDato('Persona de contacto','','contacto');
		campoDato('<i class="icon-envelope"></i> Email','','email');
		campoDato('<i class="icon-phone"></i> Teléfono/s','','telefono');
		campoDato('Tipo','','tipo');
		campoDato('Sector','','sector');
		campoDato('Usuario','','usuario');
		campoDato('Clave','','clave');
		
	cierraColumnaCampos();
	abreColumnaCampos('span6');
		campoDato('Fecha última gestión',formateaFechaWeb($datos['fechaUltimaGestion']),'fechaUltimaGestion');
		campoOculto(fecha(),'fechaUltimaGestion');
		campoSelectConsulta('codigoUsuario','Operador','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM usuarios WHERE tipo <> "CLIENTE"',$datos);
		
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario">Alta y contacto</h3>';
	abreColumnaCampos('span4');
		campoCheckIndividual('checkAlta','Alta de la empresa y asignación de claves',$datos);
		campoCheckIndividual('checkNoContactado','No contactado',$datos);
	cierraColumnaCampos();
	abreColumnaCampos('span4');
		echo '<div id="nuevaLlamada">';
		campoFecha('fechaNuevaLlamada','Fecha nueva llamada',$datos);
		campoHora('horaNuevaLlamada','Hora',$datos);
		if($datos && $datos['tareaNuevaLlamada'] != NULL){
			campoOculto($datos,'tareaNuevaLlamada');
		}
		echo '</div>';
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario">Llamada I</h3>';
	abreColumnaCampos('span4');
		campoCheckIndividual('checkProporcionar','Proporcionar claves',$datos);
		campoCheckIndividual('checkExplicarExcel','Explicar confección ficheros excel',$datos);
		campoCheckIndividual('checkTraspaso','Traspasado fichero excel',$datos);
		campoCheckIndividual('checkNoInteresado','No interesado en traspaso',$datos);
		echo '<div id="noInteresado">';
			areaTexto('observacionesNoInteresado','Motivo',$datos);
		echo '</div>';
	cierraColumnaCampos();
	abreColumnaCampos('span4');
		campoFecha('fechaProxima','Próxima fecha prevista para llamada',$datos);
		campoHora('horaProxima','Hora',$datos);
		if($datos && $datos['tareaFechaProxima'] != NULL){
			campoOculto($datos,'tareaFechaProxima');
		}
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario"></h3>';
	abreColumnaCampos('span4');
		campoCheckIndividual('checkGenerar','Generar relaciones de negocio (trabajo interno)',$datos);
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario">Llamada II</h3>';
	abreColumnaCampos('span4');
		campoCheckIndividual('checkRelacionesGeneradas','Mostrar relaciones generadas',$datos);
		campoCheckIndividual('checkObligaciones','Explicar resto de obligaciones',$datos);
		campoCheckIndividual('checkParticipacion','Explicar participacion por cursos a sus clientes',$datos);
		echo '<div id="participacion">';
			campoCheckIndividual('checkInteresadoFacilitar','Interesado en facilitar PYMES para cursos bonificados',$datos);
			campoCheckIndividual('checkInteresadoProporcionar','Interesado en proporcionar otros servicios a sus PYMES (establecer nuevas lineas de ingresos)',$datos);
		echo '</div>';
	cierraColumnaCampos();
	abreColumnaCampos('span4');
		campoCheckIndividual('checkMantenimiento','Explicar servicio de mantenimiento',$datos);
		campoCheckIndividual('checkF22','Enviar formulario F-22 y circular informativa',$datos);
		campoCheckIndividual('checkHacerDemo','Demo realizada',$datos);
		echo '<div id="hacerDemo">';
			campoFecha('fechaExplicacion','Demo/Explicación de la plataforma',$datos);
			campoHora('horaExplicacion','Hora',$datos);
			if($datos && $datos['tareaFechaExplicacion'] != NULL){
				campoOculto($datos,'tareaFechaExplicacion');
			}
		echo '</div>';
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario"></h3>';
	abreColumnaCampos('span4 cursos');
		campoCheckIndividual('checkDatosCurso','Obtener datos curso FT | |',$datos);
	cierraColumnaCampos();
	abreColumnaCampos('span4 cursos');
		campoCheckIndividual('checkCursoFT','Curso FT',$datos);
		echo '<div id="cursoFT">';
			selectMeses('mesCursoFT',$datos);
			campoTexto('anioCursoFT','Año',$datos,'input-mini');
		echo '</div>';
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario"></h3>';
	abreColumnaCampos('span4 cursos');
		campoCheckIndividual('checkAlumnos','Datos del alumno autónomo | |',$datos);
	cierraColumnaCampos();
	abreColumnaCampos('span4 cursos');
		campoCheckIndividual('checkCursoAutonomo','Curso autónomo',$datos);
		echo '<div id="cursoAutonomo">';
			selectMeses('mesCursoAutonomo',$datos);
			campoTexto('anioCursoAutonomo','Año',$datos,'input-mini');
		echo '</div>';
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario"></h3>';
	abreColumnaCampos('span4 cursos');
		campoCheckIndividual('checkAltaAsesor','Alta asesor',$datos);
		echo '<div id="altaAsesor">';
			selectMeses('mesAltaAsesor',$datos);
			campoTexto('anioAltaAsesor','Año',$datos,'input-mini');
		echo '</div>';
	cierraColumnaCampos();
	abreColumnaCampos('span4 cursos');
		campoCheckIndividual('checkBajaAsesor','Baja asesor',$datos);
		echo '<div id="bajaAsesor">';
			selectMeses('mesBajaAsesor',$datos);
			campoTexto('anioBajaAsesor','Año',$datos,'input-mini');
		echo '</div>';
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario"></h3>';
	abreColumnaCampos('span4');
		campoCheckIndividual('checkNoInteresadoServicio','No interesado en el servicio',$datos);
		echo '<div id="noInteresadoServicio">';
			areaTexto('motivoNoInteresadoServicio','Motivo',$datos);
		echo '</div>';
	cierraColumnaCampos();
	abreColumnaCampos();
		campoCheckIndividual('checkBaja','Baja de la empresa en M&D ASESORES',$datos);
	cierraColumnaCampos();
	echo '<br clear="all"><h3 class="apartadoFormulario"></h3>';
	abreColumnaCampos('span11');
		areaTexto('observaciones','Observaciones',$datos,'areaInforme');
		campoTextoVarios('cuenta','C.C.C',$datos,array(4,4,2,10),array('campoCuenta1','campoCuenta1','campoCuenta2','campoCuenta3'));
		campoTextoVarios('iban','IBAN',$datos,array(4,4,4,4,4,4),'campoCuenta1');
		campoCheckIndividual('checkRep','Representante legal de los trabajadores',$datos);
		campoTexto('numEmpleados','Num. Empleados',$datos,'input-small');
	cierraColumnaCampos();
	echo '<br clear="all"><br/>
	<div class="tablas">';
	creaTablaMantenimiento($datos);
	echo '<br/>';
	creaTablaPymes($datos);
	echo '</div>';
	

	echo '<div class="hide">';
	campoCheckIndividual('checkLOPD','Interesado en LOPD',$datos);
		campoCheckIndividual('checkPBC','Interesado en PBC',$datos);
		echo '<br/><br/>';
		campoFecha('fechaPrimer','Fecha de 1º contacto',$datos);
		campoFecha('fechaAlta','Alta en la plataforma',$datos);
		campoCheckIndividual('checkCompliance','Interesado en Compliance',$datos);
		echo '<br/><br/><br/>';
		campoFecha('fechaNoInteresado','No interesado',$datos);
		
		
		
	abreColumnaCampos();
		campoFecha('fechaBaja','Fecha de baja',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		areaTexto('observacionesBaja','Observciones',$datos);

	cierraColumnaCampos();
	echo '</div>';

	cierraVentanaGestion('index.php',true);
}

function creaTablaMantenimiento($datos){
	conexionBD();

	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Mantenimientos realizados/programados:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaMantenimiento'>
				  	<thead>
				    	<tr>
				            <th> Nº Rnv </th>
							<th> Mes aviso </th>
							<th> Año aviso </th>
							<th> Mes curso </th>
							<th> Año curso </th>
							<th> Asignado a</th>
							<th> En vigor </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM accion_mantenimiento WHERE codigoAccion=".$datos['codigo']);
				  			while($datosVenta=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaMantenimiento($datosVenta,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaMantenimiento(false,$i);
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' id='insertaFilaTablaMantenimiento'><i class='icon-plus'></i> Añadir mantenimiento</button> 
					<button type='button' class='btn btn-small btn-danger' id='eliminaFilaTablaMantenimiento' tabla='tablaMantenimiento'><i class='icon-trash'></i> Eliminar mantenimiento</button>
				</div>
			</div>
		</div>
	</div>";

	cierraBD();
}

function creaTablaPymes($datos){
	conexionBD();

	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Pymes con cursos del asesor:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaPymes'>
				  	<thead>
				    	<tr>
				            <th> Nombre de la PYME </th>
							<th> Cod. Curso </th>
							<th> Nombre del curso </th>
							<th> Fecha </th>
							<th> Num. alumnos </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM accion_pymes WHERE codigoAccion=".$datos['codigo']);
				  			while($datosVenta=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaPyme($datosVenta,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaPyme(false,$i);
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' id='insertaFilaTablaPymes'><i class='icon-plus'></i> Añadir curso PYME</button> 
					<button type='button' class='btn btn-small btn-danger' id='eliminaFilaTablaPymes' tabla='tablaPymes'><i class='icon-trash'></i> Eliminar curso PYME</button>
				</div>
			</div>
		</div>
	</div>";

	cierraBD();
}

function imprimeLineaTablaMantenimiento($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
		campoTextoTabla('mantenimientoNumero'.$i,$datos['mantenimientoNumero'],'input-mini pagination-right mantenimientoNumero');
		selectMeses('mesAviso'.$i,$datos['mesAviso'],'',1);
		campoTextoTabla('anioAviso'.$i,$datos['anioAviso'],'input-mini pagination-right');
		selectMeses('mesCurso'.$i,$datos['mesCurso'],'',1);
		campoTextoTabla('anioCurso'.$i,$datos['anioCurso'],'input-mini pagination-right');
		campoSelectConsulta('asignado'.$i,'','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM usuarios',$datos['asignado'],'selectpicker span3 show-tick','data-live-search="true"','',1);
		campoSelect('vigor'.$i,'',array('Si','No'),array('SI','NO'),$datos['vigor'],'selectpicker span2 show-tick','data-live-search="true"',1);
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

function imprimeLineaTablaPyme($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
		campoTextoTabla('nombrePyme'.$i,$datos['nombrePyme']);
		campoTextoTabla('codCursoPyme'.$i,$datos['codCursoPyme'],'input-mini pagination-right');
		campoTextoTabla('nombreCursoPyme'.$i,$datos['nombreCursoPyme']);
		campoFechaTabla('fechaCursoPyme'.$i,$datos['fechaCursoPyme']);
		campoTextoTabla('numAlumnosPyme'.$i,$datos['numAlumnosPyme'],'input-mini pagination-right');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

function filtroServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nombre','','span3');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function recogeDatos(){
	$codigo=$_POST['codigo'];
    $tabla=$_POST['tabla'];
    $item=datosRegistro($tabla,$codigo);
    $usuario=datosRegistro('usuarios_clientes',$codigo,'codigoCliente');
    $usuario=datosRegistro('usuarios',$usuario['codigoUsuario'],'codigo');
    $item['usuario']=$usuario ? $usuario['usuario']:'Sin usuario';
    $item['clave']=$usuario ? $usuario['clave']:'Sin clave';
    echo json_encode($item);
}

//Fin parte de servicios
<?php
  $seccionActiva=52;
  include_once("../cabecera.php");
  gestionAccion();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/maskedinput.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	$("#horaProxima").mask("99:99");
	$("#horaExplicacion").mask("99:99");
	$("#horaNuevaLlamada").mask("99:99");

	recogeDato($('#codigoCliente').val());
	$('#codigoCliente').change(function(){
		var codigo=$(this).val();
		recogeDato(codigo);
	});

	mostrarDiv($('#checkNoContactado').attr('checked'),'#nuevaLlamada');
	$('#checkNoContactado').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#nuevaLlamada');
	});

	mostrarDiv($('#checkNoInteresado').attr('checked'),'#noInteresado');
	$('#checkNoInteresado').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#noInteresado');
	});

	mostrarDiv($('#checkParticipacion').attr('checked'),'#participacion');
	$('#checkParticipacion').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#participacion');
	});

	mostrarDiv($('#checkHacerDemo').attr('checked'),'#hacerDemo',true);
	$('#checkHacerDemo').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#hacerDemo',true);
	});

	mostrarDiv($('#checkCursoFT').attr('checked'),'#cursoFT');
	$('#checkCursoFT').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#cursoFT');
	});

	mostrarDiv($('#checkCursoAutonomo').attr('checked'),'#cursoAutonomo');
	$('#checkCursoAutonomo').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#cursoAutonomo');
	});

	mostrarDiv($('#checkAltaAsesor').attr('checked'),'#altaAsesor');
	$('#checkAltaAsesor').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#altaAsesor');
	});

	mostrarDiv($('#checkBajaAsesor').attr('checked'),'#bajaAsesor');
	$('#checkBajaAsesor').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#bajaAsesor');
	});

	mostrarDiv($('#checkNoInteresadoServicio').attr('checked'),'#noInteresadoServicio');
	$('#checkNoInteresadoServicio').change(function(){
		var val=$(this).attr('checked');
		mostrarDiv(val,'#noInteresadoServicio');
	});

	$('#insertaFilaTablaMantenimiento').click(function(){
		insertaFila('tablaMantenimiento');
		var numero=$('#tablaMantenimiento').find('tr:last .mantenimientoNumero').val();
	});

	$('#eliminaFilaTablaMantenimiento').click(function(){
		var tabla=$(this).attr('tabla');
		eliminaFila(tabla);
	});

	$('#insertaFilaTablaPymes').click(function(){
		insertaFila('tablaPymes');
	});

	$('#eliminaFilaTablaPymes').click(function(){
		var tabla=$(this).attr('tabla');
		eliminaFila(tabla);
	});
});

function recogeDato(codigo){
	var parametros = {
			"codigo" : codigo,
			"tabla" :'clientes'
    };
	$.ajax({
		type: "POST",
		url: "../listadoAjax.php?include=accion-comercial&funcion=recogeDatos();",
		dataType: "json",
		data: parametros
	}).
	done( function(response){
		if(response != false){
			$('#cif').text(response.cif);
			$('#localidad').text(response.localidad);
			$('#provincia').text(response.provincia);
			$('#contacto').text(response.contacto);
			$('#email').text(response.email);
			$('#tipo').text(response.sector);
			$('#sector').text(response.tipo);
			$('#usuario').text(response.usuario);
			$('#clave').text(response.clave);
			var telefono='';
			if(response.telefono != ''){
				telefono=response.telefono;
			}
			if(response.movil != ''){
				if(telefono==''){
					telefono=response.movil;
				} else {
					telefono+=' - '+response.movil
				}
			}
			$('#telefono').text(telefono);
		}
	});
}

function mostrarDiv(val,div,invsersa=false){
	if(val=='checked'){
		if(invsersa){
			$(div).addClass('hide');
		} else {
			$(div).removeClass('hide');
		}
	} else {
		if(invsersa){
			$(div).removeClass('hide');
		} else {
			$(div).addClass('hide');
		}
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
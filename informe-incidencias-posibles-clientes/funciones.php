<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de informe incidencias posibles clientes

function listadoInforme(){
	$columnas=array('cif','razonSocial', 'observacionesTelemarketing', 'observacionesComercial', 'observacionesCentral', 'resultadoPreventa', 'incidencias.observaciones','clientes.codigoTelemarketing','clientes.codigoColaborador','comerciales_cliente.codigoComercial','procedenciaListado','codigoUsuario');

	$anioEjercicio=obtieneEjercicioParaWhere();
	$where=obtieneWhereListado("WHERE clientes.posibleCliente='SI'",$columnas);

	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT clientes.codigo, cif, razonSocial, resultadoPreventa, observacionesTelemarketing, observacionesComercial, observacionesCentral, clientes.codigoTelemarketing, 
			clientes.codigoColaborador, incidencias.codigoComercial, procedenciaListado, incidencias.observaciones AS incidencia, incidencias.codigo AS codigoIncidencia
			FROM clientes INNER JOIN incidencias ON clientes.codigo=incidencias.codigoCliente
			LEFT JOIN comerciales_cliente ON incidencias.codigoComercial=comerciales_cliente.codigoComercial
			LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo
			$where 
			GROUP BY clientes.codigo";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['cif'],
			"<a href='../posibles-clientes/gestion.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'>".$datos['razonSocial']."</a>",
			nl2br($datos['observacionesTelemarketing']),
			nl2br($datos['observacionesComercial']),
			nl2br($datos['observacionesCentral']),
			$datos['resultadoPreventa'],
			"<a href='../incidencias/gestion.php?codigo=".$datos['codigoIncidencia']."' class='noAjax' target='_blank'>".nl2br($datos['incidencia'])."</a>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function filtroInforme(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectConsulta(7,'Telemarketing',"SELECT codigo, nombre AS texto FROM telemarketing WHERE activo='SI' ORDER BY nombre");
	campoSelectConsulta(8,'Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre");
	campoSelectConsulta(9,'Comercial',"SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre");

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta(11,'Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activo='SI' AND (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') ORDER BY nombre");
	campoSelectSiNoFiltro(5,'Resultado preventa');
	campoSelect(10,'Procedencia del listado',array('','Colaborador/Asesor 1º Nivel','Colaborador/Asesor 2º Nivel','Puerta Fría','Base de datos comprada','Cartera comercial','Histórico Cartera Academia','Otros'),array('','COLABORADOR1','COLABORADOR2','PUERTAFRIA','BASEDATOS','CARTERA','HISTORICO','OTROS'));

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


//Fin parte de informe incidencias posibles clientes
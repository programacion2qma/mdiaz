<?php
  $seccionActiva=15;
  include_once("../cabecera.php");
  gestionCliente();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-filestyle.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>

<script src='../../api/js/firma/jquery.signaturepad.js'></script>
<script src='../../api/js/firma/assets/json2.min.js'></script>

<script src="../js/validador.js" type="text/javascript"></script>
<script src="../js/cuentaBancaria.js" type="text/javascript"></script>
<script src="../js/campoLogo.js" type="text/javascript"></script>

<script src="../js/funcionesClientes.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	quitaObligatoriedadRadios();

	$('#cif').blur(function(){
		validarNifoCif($(this));
	});
	mostrarFecha();
	mostrarFecha2();
	$('input[name=pblc]').change(function(){
		mostrarFecha();
	})
	$('input[name=aceptaContrato]').change(function(){
		mostrarFecha2();
	})

	$('#codigoColaborador option[value=NULL]').text('SIN COLABORADOR');
	$('#codigoColaborador').selectpicker('refresh');
	$('#codigoConvenio option[value=NULL]').text('EMPRESAS SIN CONVENIO');
	$('#codigoConvenio').selectpicker('refresh');
});

function mostrarFecha(){
	var val=$('input[name=pblc]:checked').val();
	if(val=='SI'){
		$('#fechasPBC').removeClass('hide');
	} else {
		$('#fechasPBC').addClass('hide');
	}
}

function mostrarFecha2(){
	var val=$('input[name=aceptaContrato]:checked').val();
	if(val=='SI'){
		$('#divAcepta').removeClass('hide');
	} else {
		$('#divAcepta').addClass('hide');
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de clientes

function operacionesClientes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaCliente();
	}
	elseif(isset($_POST['razonSocial'])){
		$res=creaCliente();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaClientes();
	}
	elseif(isset($_GET['desconfirmar'])){
		$res=cambiaValorCampo('posibleCliente','SI',$_GET['desconfirmar'],'clientes');
		$_POST['razonSocial']=true;//Para que entre en el mensajeResultado();
	}

	mensajeResultado('razonSocial',$res,'Cliente');
    mensajeResultado('elimina',$res,'Cliente', true);
}

function creaCliente(){
	$res=insertaDatos('clientes',time(),'../documentos/logos-clientes/');
	if($res){
		$codigoCliente=$res;
		$datos=arrayFormulario();
		$res=$res && creaUsuario($datos,$codigoCliente);
		$res=$res && creaPBC($datos,$codigoCliente);
		$res=$res && creaAccionComercial($datos,$codigoCliente);
	}
	return $res;
}

function actualizaCliente(){
	$res=actualizaDatos('clientes',time(),'../documentos/logos-clientes/');
	$datos=arrayFormulario();
	$res=actualizaUsuario($datos);
	$res=$res && creaPBC($datos,$_POST['codigo']);
	return $res;
}

function creaUsuario($datos,$codigoCliente){
	$res=consultaBD("INSERT INTO usuarios values(NULL,'".$datos['administrador']."','".$datos['apellido1']." ".$datos['apellido2']."','".$datos['nifAdministrador']."','".$datos['email']."','".$datos['telefono']."','','".$datos['usuario']."','".$datos['clave']."','SI','CLIENTE','');",true);
	$codigoUsuario=mysql_insert_id();
	$res=consultaBD("INSERT INTO usuarios_clientes values(NULL,".$codigoCliente.",".$codigoUsuario.");",true);
	return $res;
}

function actualizaUsuario($datos){
	$res=true;
	$usuario=consultaBD('SELECT usuarios.codigo FROM usuarios_clientes INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo WHERE usuarios_clientes.codigoCliente='.$datos['codigo'],true,true);
	if($usuario){
		$res=consultaBD('UPDATE usuarios SET nombre="'.$datos['administrador'].'",dni="'.$datos['nifAdministrador'].'",email="'.$datos['email'].'",telefono="'.$datos['telefono'].'",usuario="'.$datos['usuario'].'",clave="'.$datos['clave'].'",apellidos="'.$datos['apellido1'].' '.$datos['apellido2'].'" WHERE codigo='.$usuario['codigo'],true);
	} else {
		$res=$res && creaUsuario($datos,$datos['codigo']);
	}
	return $res;
}

function actualizaMantenimiento($datos){
	$res=true;
	if(isset($datos['codigoMantenimiento'])){
		$res=consultaBD('UPDATE clientes_mantenimiento SET fecha3meses="'.$datos['fecha3meses'].'",fecha7meses="'.$datos['fecha7meses'].'",fecha11meses="'.$datos['fecha11meses'].'" WHERE codigo='.$datos['codigoMantenimiento'],true);
		$res=consultaBD('UPDATE tareas SET fechaInicio="'.$datos['fecha3meses'].'",fechaFin="'.$datos['fecha3meses'].'
			",codigoUsuario="'.$datos['codigoConsultor'].'" WHERE codigo='.$datos['tarea3meses'],true);
		$res=consultaBD('UPDATE tareas SET fechaInicio="'.$datos['fecha7meses'].'",fechaFin="'.$datos['fecha7meses'].'",codigoUsuario="'.$datos['codigoConsultor'].'" WHERE codigo='.$datos['tarea7meses'],true);
		$res=consultaBD('UPDATE tareas SET fechaInicio="'.$datos['fecha11meses'].'",fechaFin="'.$datos['fecha11meses'].'",codigoUsuario="'.$datos['codigoConsultor'].'" WHERE codigo='.$datos['tarea11meses'],true);
	}
	return $res;
}


function compruebaCamposRadioClientes(){
	$_POST['procedeDeListado']=compruebaCampoRadio('procedeDeListado');
	$_POST['llamado']=compruebaCampoRadio('llamado');
	$_POST['visita']=compruebaCampoRadio('visita');
	$_POST['resultadoVisita']=compruebaCampoRadio('resultadoVisita');
	$_POST['resultadoPreventa']=compruebaCampoRadio('resultadoPreventa');
}

function insertaCuentasSSCliente($datos,$codigoCliente,$actualizacion=false){
	$res=eliminaDatosCliente('cuentas_ss_clientes',$codigoCliente,$actualizacion);

	for($i=0;isset($datos['cuentaSS'.$i]);$i++){
		$cuentaSS=$datos['cuentaSS'.$i];

		$res=$res && consultaBD("INSERT INTO cuentas_ss_clientes VALUES(NULL,'$cuentaSS',$codigoCliente);");
	}

	return $res;
}

function insertaCuentasCliente($datos,$codigoCliente,$actualizacion=false){
	//Los registros de cuentas_cliente no se pueden eliminar al actualizar de forma normal, pues están vinculados con la facturación
	//$res=eliminaDatosCliente('cuentas_cliente',$codigoCliente,$actualizacion);

	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos["codigoCuenta$i"]) || isset($datos["ccc$i"]);$i++){
		if(isset($datos['codigoCuenta'.$i]) && isset($datos['ccc'.$i])){//Actualización
			$res=$res && consultaBD("UPDATE cuentas_cliente SET ccc='".$datos["ccc$i"]."', iban='".$datos["iban$i"]."', bic='".$datos["bic$i"]."' WHERE codigo='".$datos['codigoCuenta'.$i]."';");
		}
		elseif(!isset($datos['codigoCuenta'.$i]) && isset($datos['ccc'.$i])){//Inserción
			$res=$res && consultaBD("INSERT INTO cuentas_cliente VALUES(NULL,'".$datos['ccc'.$i]."',
			'".$datos['iban'.$i]."','".$datos['bic'.$i]."',$codigoCliente)");
		}
		else{//Eliminación
			$res=$res && consultaBD("DELETE FROM cuentas_cliente WHERE codigo='".$datos['codigoCuenta'.$i]."';");
		}
	}

	cierraBD();

	return $res;
}

function creaPBC($datos,$cliente){
	$res=true;
	$trabajo=consultaBD('SELECT trabajos.* FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE codigoCliente='.$cliente,true,true);
	if($trabajo){
		$res=actualizaPBC($datos,$trabajo);
	} else {
		//$res=insertaPBC($datos,$cliente);
	}
	return $res;
}

function insertaPBC($datos,$cliente){
	$res=true;
	
	$fecha=date('Y-m-d');
	$fechaRevision = obtieneFechaLimiteTrabajoVenta($fecha,'P21D');
    $fechaPrevista = obtieneFechaLimiteTrabajoVenta($fechaRevision,'P51D');
    $fechaTomaDatos = obtieneFechaLimiteTrabajoVenta($fecha,'P14D');
    $fechaEnvio = obtieneFechaLimiteTrabajoVenta($fecha,'P28D');
    $fechaMantenimiento = obtieneFechaLimiteTrabajoVenta($fecha,'P11M');
    $servicio = obtenerCodigoServicio($datos);
	$res = consultaBD("INSERT INTO trabajos VALUES(NULL,NULL,'".$fecha."','".$fechaPrevista."',".$cliente.",".$servicio.",'','NO','NO','NO','NO','NO','NO','".$fechaTomaDatos."','".$fechaRevision."','".$fechaRevision."','".$fechaEnvio."','".$fechaEnvio."','".$fechaMantenimiento."','SI','NO','');", true);
	$trabajo=mysql_insert_id();
	if($datos['pblc']=='SI'){
		$persona=isset($_POST['checkAutonomo']) ? 'FISICA':'JURIDICA';
		$res = consultaBD("INSERT INTO manuales_pbc(codigo,codigoTrabajo,fechaInicio,fechaFin,sujeto,actividad,servicios,empleados, volumen,tomo,folio,hoja,libro,seccion,representante,nifRepresentante) VALUES (NULL,".$trabajo.",'".$datos['fechaInicio']."','".$datos['fechaFin']."','".$persona."','".$datos['actividad']."','".$datos['servicios']."','".$datos['empleados']."','".$datos['volumen']."','".$datos['tomo']."','".$datos['folio']."','".$datos['hoja']."','".$datos['libro']."','".$datos['seccion']."','".$datos['administrador']." ".$datos['apellido1']." ".$datos['apellido2']."','".$datos['nifAdministrador']."')",true);
	}
	
	return $res;
}

function actualizaPBC($datos,$trabajo){
	$servicio=obtenerCodigoServicio($datos);
	$res=true;
	$res=consultaBD("UPDATE trabajos SET codigoServicio='".$servicio."' WHERE codigo=".$trabajo['codigo'],true);
		$persona=isset($_POST['checkAutonomo']) ? 'FISICA':'JURIDICA';
		if($datos['pblc']=='SI'){
			$manual=datosRegistro('manuales_pbc',$trabajo['codigo'],'codigoTrabajo');
			if($manual){
				$res = consultaBD("UPDATE manuales_pbc SET sujeto = '".$persona."' ,actividad = '".$datos['actividad']."',servicios = '".$datos['servicios']."',empleados='".$datos['empleados']."', volumen='".$datos['volumen']."',tomo='".$datos['tomo']."',folio='".$datos['folio']."',hoja='".$datos['hoja']."',libro='".$datos['libro']."',seccion='".$datos['seccion']."',representante='".$datos['administrador']." ".$datos['apellido1']." ".$datos['apellido2']."',nifRepresentante='".$datos['nifAdministrador']."',fechaInicio='".$datos['fechaInicio']."',fechaFin='".$datos['fechaFin']."' WHERE codigoTrabajo=".$trabajo['codigo'],true);
				
			} else {
				$fecha=date('Y-m-d');
				$res = consultaBD("INSERT INTO manuales_pbc(codigo,codigoTrabajo,fechaInicio,fechaFin,sujeto,actividad,servicios,empleados, volumen,tomo,folio,hoja,libro,seccion,representante,nifRepresentante) VALUES (NULL,".$trabajo['codigo'].",'".$datos['fechaInicio']."','".$datos['fechaFin']."','".$persona."','".$datos['actividad']."','".$datos['servicios']."','".$datos['empleados']."','".$datos['volumen']."','".$datos['tomo']."','".$datos['folio']."','".$datos['hoja']."','".$datos['libro']."','".$datos['seccion']."','".$datos['administrador']." ".$datos['apellido1']." ".$datos['apellido2']."','".$datos['nifAdministrador']."')",true);
			}
		}
	return $res;
}

function obtenerCodigoServicio($datos){
	$servicio=0;
	$listado=array(0=>0);
	$consulta=consultaBD('SELECT * FROM servicios ORDER BY referencia',true);
	while($serv=mysql_fetch_assoc($consulta)){
		$listado[$serv['referencia']]=$serv['codigo'];
	}
	
	if($datos['pblc']=='SI'){
		if($datos['lopd']=='SI'){
			if(isset($datos['checkAutonomo'])){
				if($datos['empleados']==10){
					$servicio=5;
				} else if ($datos['empleados']==49) {
					$servicio=7;
				} else if ($datos['empleados']==50) {
					$servicio=13;	
				}
			} else {
				if($datos['empleados']==10){
					$servicio=10;
				} else if ($datos['empleados']==49) {
					$servicio=11;
				} else if ($datos['empleados']==50) {
					$servicio=13;
				}
			}
		} else {
			if(isset($datos['checkAutonomo'])){
				if($datos['empleados']==10){
					$servicio=4;
				} else if ($datos['empleados']==49) {
					$servicio=6;
				} else if ($datos['empleados']==50) {
					$servicio=12;
				}
			} else {
				if($datos['empleados']==10){
					$servicio=8;
				} else if ($datos['empleados']==49) {
					$servicio=9;
				} else if ($datos['empleados']==50) {
					$servicio=12;
				}
			}
		}
	} else {
		if($datos['lopd']=='SI'){
			if(!isset($datos['checkAutonomo'])){
				if($datos['empleados']==10){
					$servicio=1;
				} else {
					$servicio=2;
				}
			} else {
				if($datos['empleados']==10){
					$servicio=3;
				} else {
					//TODO
				}
			}
		} else {
			//TODO
		}
	}
	$servicio=$listado[$servicio];
	return $servicio;
}


function compruebaCamposRadioPosiblesClientes(){
	$_POST['pyme']=compruebaCampoRadio('pyme');
	$_POST['nuevaCreacion']=compruebaCampoRadio('nuevaCreacion');
	$_POST['trabajadoresCotizando']=compruebaCampoRadio('trabajadoresCotizando');
	$_POST['tieneRLT']=compruebaCampoRadio('tieneRLT');
	$_POST['activo']=compruebaCampoRadio('activo');
	$_POST['procedeDeListado']=compruebaCampoRadio('procedeDeListado');
	$_POST['llamado']=compruebaCampoRadio('llamado');
	$_POST['visita']=compruebaCampoRadio('visita');
	$_POST['resultadoVisita']=compruebaCampoRadio('resultadoVisita');
	$_POST['resultadoPreventa']=compruebaCampoRadio('resultadoPreventa');
}


function listadoClientes(){
	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Crédito validado"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Crédito no validado"></i>');

	//fechaAlta lo utilizo para el filtro por ejercicios
	$columnas=array('razonSocial','clientes.cif','administrador','clientes.telefono','clientes.email','fechaAlta','colaboradores.codigo','codigoConvenio', 'clientes.provincia');
	$where=obtieneWhereListado("HAVING posibleCliente='SI'",$columnas,false,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	$query="SELECT clientes.codigo AS codigoCliente, razonSocial, clientes.cif, administrador, clientes.telefono, clientes.email, posibleCliente, 
			 fechaAlta, colaboradores.nombre AS nombreColaborador, convenios.codigo AS codigoConvenio, convenios.nombre AS nombreConvenio, clientes.provincia, colaboradores.codigo
			FROM clientes 
			LEFT JOIN colaboradores ON clientes.codigoColaborador=colaboradores.codigo
			LEFT JOIN convenios ON clientes.codigoConvenio=convenios.codigo
			$where";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$botones=obtieneBotonesPosibleCliente($datos);
		if($datos['nombreColaborador']=='' || $datos['nombreColaborador']=='NULL'){
			$datos['nombreColaborador'] = 'SIN COLABORADOR';
		} 
			
		if($datos['nombreConvenio']=='' || $datos['codigoConvenio']=='NULL'){
			$datos['nombreConvenio']='EMPRESAS SIN CONVENIO';
		}

		$fila=array(
			"<a href='gestion.php?codigo=".$datos['codigoCliente']."'>".$datos['razonSocial']."</a>",
			$datos['cif'],
			$datos['nombreColaborador'],
			$datos['nombreConvenio'],
			convertirMinuscula($datos['provincia']),
			"<a href='tel:".$datos['telefono']."' class='nowrap'>".formateaTelefono($datos['telefono'])."</a>",
			"<a href='mailto:".$datos['email']."'>".$datos['email']."</a>",
			$botones,
        	obtieneCheckTabla($datos,'codigoCliente'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function obtieneWhereListadoPosiblesClientes($columnas){
	if($_SESSION['tipoUsuario']=='COMERCIAL' || $_SESSION['tipoUsuario']=='TELEMARKETING'){
		$res=obtieneWhereListado("WHERE posibleCliente='SI'",$columnas,false,true);
	}
	else{
		$res=obtieneWhereListado("WHERE posibleCliente='SI'",$columnas,true,true);	
	}

	return $res;
}

function compruebaCreditoCliente($datos){
	$res='';

	if(trim($datos['credito'])!=''){
		$res="<div class='pagination-right'>".$datos['credito']." €</div>";
	}

	return $res;
}

function obtieneBotonesPosibleCliente($datos){
	global $_CONFIG;
	$analisis=datosRegistro('analisis_previo',$datos['codigoCliente'],'codigoCliente');
	if($analisis){
		$url="<li><a href='".$_CONFIG['raiz']."analisis-previo/gestion.php?codigo=".$analisis['codigo']."&sesion=15'><i class='icon-exclamation-circle'></i> Ver Análisis previo de riesgo</i></a></li>
			<li class='divider'></li>
			<li><a href='".$_CONFIG['raiz']."analisis-previo/generaDocumento.php?codigo=".$analisis['codigo']."' target='_blank' class='noAjax'><i class='icon-download'></i> Descargar Análisis previo de riesgo</i></a></li>";
	} else {
		$url="<li><a href='".$_CONFIG['raiz']."analisis-previo/gestion.php?codigoCliente=".$datos['codigoCliente']."&sesion=15'><i class='icon-exclamation-circle'></i> Crear Análisis previo de riesgo</i></a></li>";
	}
	//La clase generacionDocumento no tiene valor en los estilos; sirve como selector en el JS ../js/filtroTablaAJAXBusqueda.js
	$res="
		<div class='btn-group'>
			<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
		  	<ul class='dropdown-menu' role='menu'>
			    <li><a href='".$_CONFIG['raiz']."posibles-clientes/gestion.php?codigo=".$datos['codigoCliente']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
			    <li class='divider'></li>
			    <li><a href='' codigoCliente='".$datos['codigoCliente']."' class='espacioTrabajo noAjax'><i class='icon-folder'></i> Espacio de cliente</i></a></li>";

	/*if($datos['tieneRLT']=='SI'){

		$res.="<li class='divider'></li>
			  <li><a href='".$_CONFIG['raiz']."clientes/generaInformeRLT.php?codigo=".$datos['codigo']."' class='generacionDocumento'><i class='icon-cloud-download'></i> Descargar RLT</i></a></li>";
	}*/

	$res.="</ul>
		</div>";

	return $res;
}

function obtieneOrdenListadoClientes($columnas){
	$res=obtieneOrdenListado($columnas);

	if($res==''){
		$res='ORDER BY creditos_cliente.codigo DESC';//Para que siempre aparezcan los últimos datos rellenos
	}
	else{
		$res.=', creditos_cliente.codigo DESC';
	}

	return $res;
}


function gestionCliente(){
	operacionesClientes();

	abreVentanaGestion('Gestión de clientes potenciales','?','','icon-edit','',true,'noAjax');
		$datos=compruebaDatos('clientes');
		$fecha=array();
		$fechas['fechaInicio']='';
		$fechas['fechaFin']='';
		if($datos){
			$usuario=consultaBD('SELECT usuario,clave FROM usuarios_clientes INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo WHERE usuarios_clientes.codigoCliente='.$datos['codigo'],true,true);
			$trabajo=datosRegistro('trabajos',$datos['codigo'],'codigoCliente');
			$manual=datosRegistro('manuales_pbc',$trabajo['codigo'],'codigoTrabajo');
			if($manual){
				$fechas['fechaInicio']=$manual['fechaInicio'];
				$fechas['fechaFin']=$manual['fechaFin'];
			}
		} else {
			$usuario=false;
		}
		if(!$datos){
			campoCheckIndividual('checkCreaConsultoria','Crear consultoría',$datos);
		}
		campoOculto($datos,'videovigilancia');
		campoCheckIndividual('checkAutonomo','Marcar si es persona física (autónomo)',$datos);
		campoCheckIndividual('checkColegiado','Colegiado',$datos);
		abreColumnaCampos();
			campoOculto(formateaFechaWeb($datos['fechaAlta']),'fechaAlta',fecha());
			campoOculto($datos,'posibleCliente','SI');
			campoTexto('razonSocial','Nombre de la empresa',$datos,'span3');
			campoTexto('sector','Sector',$datos);
			campoSelect('actividad','Actividad',array('Abogacía','Asesoramiento fiscal','Asesoramiento fiscal y contable','Contabilidad externa','Auditoría de cuentas','Promoción inmobiliaria','Otra'),array('ABOGACIA','FISCAL','CONTABLE','CONTABILIDAD','AUDITORIA','PROMOCION','OTRA'),$datos);
			areaTexto('servicios','Servicios habituales',$datos);
			campoRadio('pblc','¿Tiene PBLC?',$datos,'SI');
			echo '<div id="fechasPBC">';
			campoFecha('fechaInicio','Fecha inicio del manual de PBC',$fechas);
			campoFecha('fechaFin','Fecha fin del manual de PBC',$fechas);
			echo '</div>';
			campoSelect('empleados','Número de empleados',array('Menos de 10 personas','Entre 10 y 49 personas','50 personas o más'),array(10,49,50),$datos);
			campoSelect('volumen','Volumen de negocios anual',array('No supera los 2 millones de euros','Supera los 2 millones de euros sin superar los 10 millones de euros','Supera los 10 millones de euros'),array(1,2,3),$datos);
			campoRadio('lopd','Implantación de LOPD',$datos);
			campoRadio('aceptaContrato','Contrato aceptado',$datos);
			echo '<div id="divAcepta">';
				campoFecha('fechaAcepta','Fecha de aceptación del contrato',$datos);
			echo '</div>';
		cierraColumnaCampos();

		abreColumnaCampos();
			campoTextoValidador('cif','CIF/NIF',$datos,'input-small','clientes');
			campoLogo('ficheroLogo','Logo',0,$datos,'../documentos/logos-clientes/','Ver');
			campoTexto('administrador','Nombre',$datos);
			campoTexto('apellido1','Primer apellido',$datos);
			campoTexto('apellido2','Segundo apellido',$datos);
			campoTextoValidador('nifAdministrador','NIF',$datos,'input-small','clientes');
			campoTexto('tomo','Tomo',$datos);
			campoTexto('libro','Libro',$datos);
			campoTexto('folio','Folio',$datos);
			campoTexto('seccion','Sección',$datos);
			campoTexto('hoja','Hoja',$datos);
			echo '<br/>';
			campoTexto('domicilio','Dirección',$datos,'span3');
			campoTexto('cp','Código Postal',$datos,'input-mini pagination-right');
			campoTexto('localidad','Población',$datos,'span3');
			campoSelectProvincia($datos,'provincia','Provincia',0,'');
			selectPaises('pais',$datos);
		cierraColumnaCampos();

		echo '<br clear="all">';
		abreColumnaCampos();
			campoSelectConsulta('codigoConvenio','Convenio','SELECT codigo, nombre AS texto FROM convenios ORDER BY nombre',$datos);
			campoSelectConsultaAjax('codigoColaborador','Colaborador','SELECT codigo, nombre AS texto FROM colaboradores',$datos,'colaboradores/gestion.php','selectpicker selectAjax span3 show-tick');
			campoSelectConsulta('codigoConsultor','Operador','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM usuarios WHERE tipo <> "CLIENTE";',$datos);
		cierraColumnaCampos();

		abreColumnaCampos();
			campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small','clientes');
			campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',$datos,'input-small','clientes');
			campoTexto('contacto','Persona de contacto',$datos);
			campoTextoSimboloValidador('fax','Fax','<i class="icon-fax"></i>',$datos,'input-small','clientes');
			campoTextoSimboloValidador('email','eMail principal','<i class="icon-envelope"></i>',$datos,'input-large','clientes');
		cierraColumnaCampos();
		echo '<br clear="all">';
		abreColumnaCampos();
			campoTextoValidador('usuario','Usuario',$usuario,'input-large obligatorio','usuarios','usuario');
			campoClaveUsuario($usuario);
		cierraColumnaCampos();
		

		
	cierraVentanaGestion('index.php',true);

}


function creaTablaCuentaBancaria($datos){
	echo "
	<h3 class='apartadoFormulario'>Cuentas bancarias</h3>
	<div class='centro'>
		<table class='table table-striped table-bordered mitadAncho' id='tablaCuentas'>
	      <thead>
	        <tr>
	          <th> C.C.C. (20 dígitos) </th>
	          <th> IBAN (24 caracteres) </th>
	          <th> BIC (11 caracteres) </th>
	          <th> </th>
	        </tr>
	      </thead>
	      <tbody>";
	  	
	  		$i=0;

	  		if($datos!=false){
	  			$consulta=consultaBD("SELECT * FROM cuentas_cliente WHERE codigoCliente='".$datos['codigo']."'",true);
	  			while($datosP=mysql_fetch_assoc($consulta)){
					imprimeLineaTablaCuentaBancaria($i,$datosP);
					$i++;
	  			}
	  		}
	  		
	  		if($i==0){
	  			imprimeLineaTablaCuentaBancaria(0,false);
	  		}
      
    echo "</tbody>
    	</table>
		<button type='button' class='btn btn-small btn-success' onclick='insertaFilaCuenta();'><i class='icon-plus'></i> Añadir cuenta</button> 
		<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCuentas\");'><i class='icon-trash'></i> Eliminar cuenta</button>
	</div>";
}

function imprimeLineaTablaCuentaBancaria($i,$datos){
	$j=$i+1;

	echo "<tr>";
		campoTextoTabla('ccc'.$i,$datos['ccc'],'input-large numeroCuenta pagination-right');
		campoTextoTablaValidador('iban'.$i,$datos['iban'],'input-large','cuentas_cliente','codigoCliente');
		campoTextoTabla('bic'.$i,$datos['bic'],'input-medium');
	echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
	</tr>";
}

function filtroClientes(){

	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Razón social');
	campoTexto(1,'CIF','','input-small');
	campoTexto(2,'Representante legal');
	campoTexto(3,'Teléfono',false,'input-small pagination-right');
	campoTexto(4,'eMail');
	

	cierraColumnaCampos();
	abreColumnaCampos();
		campoSelectConsultaAjax(6,'Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre;",false,'colaboradores/gestion.php?codigo=',"span3 selectAjax selectpicker show-tick");
		campoSelectConsulta(7,'Convenio','SELECT codigo, nombre AS texto FROM convenios ORDER BY nombre');
		campoSelectProvinciaAndalucia(false,8);
	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function normaliza ($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ
ßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuy
bsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    $cadena = strtolower($cadena);
    return utf8_encode($cadena);
}

function creaBotonesGestionCliente(){
	echo '
    <a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
}

//Fin parte de clientes
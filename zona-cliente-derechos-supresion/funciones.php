<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesDerechos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDerecho();
	}
	elseif(isset($_POST['interesado'])){
		$res=insertaDerecho();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('derechos');
	}

	mensajeResultado('interesado',$res,'Derecho');
    mensajeResultado('elimina',$res,'Derecho', true);
}

function insertaDerecho(){
	$res=true;
	responsable();
	$res=insertaDatos('derechos');
	return $res;
}

function actualizaDerecho(){
	$res=true;
	responsable();
	$res=actualizaDatos('derechos');
	return $res;
}

function responsable(){
	$_POST['responsable']='';
	$_POST['responsableDireccion']='';
	if($_POST['respuesta']==3 && $_POST['selectResponsable']!='NULL'){
		$i=$_POST['selectResponsable'];
		$_POST['responsable']=$_POST['responsable'.$i];
		$_POST['responsableDireccion']=$_POST['direccion'.$i];
	}
}

function listadoDerechos(){
	global $_CONFIG;

	$columnas=array('interesado','fechaRecepcion','fechaRespuesta','respuesta','tipo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente." AND tipo='Supresión'",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having;");
	cierraBD();
	$respuestas=array('',
	'La solicitud no reúne los requisitos de identificación',
	'No figuran datos personales del interesado',
	'Comunicación al Responsable de Tratamientos',
	'Datos personales bloqueados',
	'Denegación de supresión',
	'Otorgamiento de supresión');
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$otrosDatos="";
		if($datos['respuesta']==7 && $datos['otrosDatos']!=''){
			$otrosDatos=' - '.$datos['otrosDatos'];
		}
		$opciones=array('Detalles','Descargar respuesta');
		$enlaces=array("zona-cliente-derechos-supresion/gestion.php?codigo=".$datos['codigo'],"zona-cliente-derechos-supresion/generaRespuesta.php?codigo=".$datos['codigo']);
		$iconos=array('icon-search-plus','icon-cloud-download');
		if($datos['visualizacion']=='SI'){
			array_push($opciones,'Descargar comunicación a cesionario');
			array_push($enlaces,"zona-cliente-derechos-supresion/generaRespuesta.php?cesionario&codigo=".$datos['codigo']);
			array_push($iconos,'icon-cloud-download');
		}
		$fila=array(
			$datos['referencia'],
			$datos['interesado'],
			formateaFechaWeb($datos['fechaRecepcion']),
			formateaFechaWeb($datos['fechaRespuesta']),
			$respuestas[$datos['respuesta']].$otrosDatos,
			botonAcciones($opciones,$enlaces,$iconos),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionDerechos(){
	operacionesDerechos();

	abreVentanaGestion('Gestión de interesados que ejercen el derecho de supresión','?','','icon-edit','margenAb');
	$datos=compruebaDatos('derechos');

    if(!$datos){
    	$codigoCliente=obtenerCodigoCliente(true);
    	$referencia=obtieneReferenciaDerecho('Supresión',$codigoCliente);
    	campoOculto($codigoCliente,'codigoCliente');
    }  else {
    	$codigoCliente=$datos['codigoCliente'];
    	$referencia=$datos['referencia'];
    }

    campoOculto('','horaVisualizacion');
    campoOculto('','otrosDatos');

	abreColumnaCampos();
		campoOculto('Supresión','tipo');
		campoTexto('referencia','Referencia',$referencia,'input-mini',true);
   		campoTexto('interesado','Nombre y apellido del interesado',$datos,'span7');
   		campoTexto('interesadoDni','NIF',$datos,'input-small');
   		campoTexto('representante','Representante legal',$datos,'span7');
   		campoTexto('representanteDni','NIF',$datos,'input-small');
   		campoTexto('direccion','Dirección a efecto de notificaciones',$datos,'span7');
   		campoTexto('email','Correo electrónico',$datos,'span7');
   	cierraColumnaCampos(true);
   	abreColumnaCampos();
		campoFecha('fechaRecepcion','Fecha de Recepción',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoFecha('fechaRespuesta','Fecha de Respuesta',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		campoRadio('mediosElectronicos','La solicitud se ha presentado por medios electrónicos',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		echo '<div id="divMediosDiferente" class="hide">';
			campoRadio('mediosDiferente','El interesado solicita que se le responda por otros medios diferentes al correo electrónico',$datos);
		echo '</div>';
	cierraColumnaCampos();
	abreColumnaCampos();
		echo '<div id="divOtroMedio" class="hide">';
			campoTexto('otroMedio','Indicar',$datos);
		echo '</div>';
	cierraColumnaCampos(true);
	abreColumnaCampos();
		$select=rellenaSelect();
		campoSelect('respuesta','Respuesta',$select['nombres'],$select['valores'],$datos,'selectpicker span6 show-tick');
	cierraColumnaCampos(true);
	$responsables=array();
	$direcciones=array();
	conexionBD();
	$declaraciones=consultaBD('SELECT * FROM declaraciones WHERE codigoCliente='.$codigoCliente);
	while($d=mysql_fetch_assoc($declaraciones)){
		if(!in_array($d['n_razon'],$responsables)){
			array_push($responsables,$d['n_razon']);
			array_push($direcciones,$d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$d['provincia_responsableFichero']);
		}
		$otros=consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion='.$d['codigo']);
		while($o=mysql_fetch_assoc($otros)){
			if($o['razonSocial']!='' && !in_array($o['razonSocial'],$responsables)){
				array_push($responsables,$o['razonSocial']);
				array_push($direcciones,$o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$o['provincia']);
			}
		}
	}
	cierraBD();
	abreColumnaCampos('span3 divResponsable hide');
		$nombres=array('');
		$valores=array('NULL');
		foreach ($responsables as $key => $value) {
			campoOculto($value,'responsable'.$key);
			array_push($nombres,$value);
			array_push($valores,$key);
		}
		foreach ($direcciones as $key => $value) {
			campoOculto($value,'direccion'.$key);
		}
		if($datos && $datos['responsable']!=''){
			campoDato('Responsable de tratamiento',$datos['responsable']);
		}
		campoSelect('selectResponsable','Selecciona responsable',$nombres,$valores);
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divOtrosDatos hide');
		echo '<a id="art17" style="margin-left:85px" class="btnModal noAjax">artículo 17 RGPD</a>';
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divTratamiento hide');
		campoSelectConsulta('codigoTratamiento','Fichero/Tratamiento','SELECT codigo, nombreFichero AS texto FROM declaraciones WHERE codigoCliente='.$codigoCliente,$datos);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoRadio('visualizacion','Previamente a la solicitud de supresión, ¿se cedieron los datos objeto de la supresión? <a id="art19" class="btnModal noAjax">-artículo 19 RGPD-</a>',$datos,'NO',array('No','Sí'),array('NO','SI'));
		echo '<div id="divProcede" class="hide">';
			campoTexto('nombreCesionario','Nombre del cesionario',$datos);
			campoFecha('fechaVisualizacion','Fecha comunicación al cesionario',$datos);
		echo '</div>';
	cierraColumnaCampos(true);

	abreColumnaCampos();
		areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 
	cierraColumnaCampos(true);

	cierraVentanaGestion('index.php',true);

	abreVentanaModal('información','cajaGestion17');
	echo '<i><b>Artículo 17 RGPD. Derecho de supresión (<el derecho al olvido>)</b>. 1. El interesado tendrá derecho a obtener sin dilación indebida del responsable del tratamiento la supresión de los datos personales que le conciernan, el cual estará obligado a suprimir sin dilación indebida los datos personales cuando concurra alguna de las circunstancias siguientes:<br/><br/>
		a) los datos personales ya no sean necesarios en relación con los fines para los que fueron recogidos o tratados de otro modo;<br/><br/>
		b) el interesado retire el consentimiento en que se basa el tratamiento de conformidad con el artículo 6, apartado 1, letra a), o el artículo 9, apartado 2, letra a), y este no se base en otro fundamento jurídico;<br/><br/>
		c) el interesado se oponga al tratamiento con arreglo al artículo 21, apartado 1, y no prevalezcan otros motivos legítimos para el tratamiento, o el interesado se oponga al tratamiento con arreglo al artículo 21, apartado 2;<br/><br/>
		d) los datos personales hayan sido tratados ilícitamente;<br/><br/>
		e) los datos personales deban suprimirse para el cumplimiento de una obligación legal establecida en el Derecho de la Unión o de los Estados miembros que se aplique al responsable del tratamiento;<br/><br/>
		f) los datos personales se hayan obtenido en relación con la oferta de servicios de la sociedad de la información mencionados en el artículo 8, apartado 1.<br/><br/>
		2. Cuando haya hecho públicos los datos personales y esté obligado, en virtud de lo dispuesto en el apartado 1, a suprimir dichos datos, el responsable del tratamiento, teniendo en cuenta la tecnología disponible y el coste de su aplicación, adoptará medidas razonables, incluidas medidas técnicas, con miras a informar a los responsables que estén tratando los datos personales de la solicitud del interesado de supresión de cualquier enlace a esos datos personales, o cualquier copia o réplica de los mismos.<br/><br/>
		3. Los apartados 1 y 2 no se aplicarán cuando el tratamiento sea necesario:<br/><br/>
		a) para ejercer el derecho a la libertad de expresión e información;<br/><br/>
		b) para el cumplimiento de una obligación legal que requiera el tratamiento de datos impuesta por el Derecho de la Unión o de los Estados miembros que se aplique al responsable del tratamiento, o para el cumplimiento de una misión realizada en interés público o en el ejercicio de poderes públicos conferidos al responsable;<br/><br/>
		c) por razones de interés público en el ámbito de la salud pública de conformidad con el artículo 9, apartado 2, letras h) e i), y apartado 3; d) con fines de archivo en interés público, fines de investigación científica o histórica o fines estadísticos, de conformidad con el artículo 89, apartado 1, en la medida en que el derecho indicado en el apartado 1 pudiera hacer imposible u obstaculizar gravemente el logro de los objetivos de dicho tratamiento, o<br/><br/>
		e) para la formulación, el ejercicio o la defensa de reclamaciones.</i>';
	cierraVentanaModal('','','',false,'Cerrar');

	abreVentanaModal('información','cajaGestion19');
	echo '<i><b>Artículo 19 RGPD. Obligación de notificación relativa a la rectificación o supresión de
datos personales o la limitación del tratamiento.</b> El responsable del tratamiento
comunicará cualquier rectificación o supresión de datos personales o limitación del
tratamiento efectuada con arreglo al artículo 16, al artículo 17, apartado 1, y al artículo 18
a cada uno de los destinatarios a los que se hayan comunicado los datos personales, salvo
que sea imposible o exija un esfuerzo desproporcionado. El responsable informará al
interesado acerca de dichos destinatarios, si este así lo solicita.</i>';
	cierraVentanaModal('','','',false,'Cerrar');
}

function rellenaSelect(){
	$res=array('valores'=>array(0,1,2,3,4,5,6),'nombres'=>array('',
	'La solicitud no reúne los requisitos de identificación ',
	'No figuran datos personales del interesado',
	'Comunicación al Responsable de Tratamientos',
	'Datos personales bloqueados',
	'Denegación de supresión',
	'Otorgamiento de supresión'));
	return $res;
}

function wordInteresadoDerechos($PHPWord,$codigoDerecho){
    global $_CONFIG;
    $datos=consultaBD('SELECT derechos.*, clientes.razonSocial, CONCAT(clientes.domicilio,", CP ",clientes.cp,", ",clientes.localidad,", ",clientes.provincia) AS direccionCliente, ficheroLogo, administrador FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo WHERE derechos.codigo='.$codigoDerecho,true,true);
    $tipo=$datos['tipo'];
    $ref=$datos['referencia']<10?'0'.$datos['referencia']:$datos['referencia'];
    $fichero='RESPUESTA_DERECHO_'.mb_strtoupper($tipo).'_INTERESADO_REF_'.$ref.'.docx';
    $documento=$PHPWord->loadTemplate('../documentos/derechos/plantillaDerechos.docx');
    $documento->setValue("true","");
    $documento->setValue("cliente",utf8_decode(sanearCaracteresDerechos($datos['razonSocial'])));  
    $documento->setValue("titulo",utf8_decode("RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO ".$datos['interesado']." REF. ".$ref));

    reemplazarLogoDerechos($documento,$datos,'image2.png'); 
    $respuestas=array('','LA SOLICITUD NO REÚNE LOS REQUISITOS NECESARIOS','NO FIGURAN DATOS PERSONALES DEL INTERESADO EN LOS FICHEROS DEL RESPONSABLE','COMUNICACIÓN AL RESPONSABLE DE TRATAMIENTOS ','DATOS PERSONALES BLOQUEADOS','DENEGACIÓN DE SUPRESIÓN','OTORGAMIENTO DE SUPRESIÓN');
    if($datos['respuesta']==3){
        $texto="RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": ".$respuestas[$datos['respuesta']]."<w:br/><w:br/>".$datos['razonSocial'].", en concepto de encargado del tratamiento de datos personales, de los cuales es responsable del tratamiento ".$datos['responsable'].", mediante el presente escrito es para dar respuesta a la petición de supresión recibida el día ".formateaFechaWeb($datos['fechaRecepcion'])." realizada por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['responsableDireccion']."<w:br/><w:br/>";
    } else {
        $texto="RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": ".$respuestas[$datos['respuesta']]."<w:br/><w:br/>El presente escrito es para dar respuesta a la petición de supresión de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['direccion']."<w:br/><w:br/>";
        
    }
    if($datos['representante']!=''){
        $texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    } else {
        $texto.=$datos['interesado'];
    }
    $texto.=$direccion;
    switch($datos['respuesta']){
        case 1:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que no se puede llevar a cabo la supresión solicitada debido a que no puede identificar al interesado/representación legal del interesado -artículo 11 del Reglamento General de Protección de Datos (UE)-; no obstante, si desea que el responsable del tratamiento proceda a la supresión interesada, puede facilitarnos información adicional que permita su identificación/representación legal del interesado:<w:br/><w:br/>1. Nombre y apellidos del interesado; fotocopia de su documento nacional de identidad, o de su pasaporte u otro documento válido que lo identifique y, en su caso, de la persona que lo represente, o instrumentos electrónicos equivalentes; así como el documento o instrumento electrónico acreditativo de tal representación. La utilización de firma electrónica identificativa del afectado eximirá de la presentación de las fotocopias del DNI o documento equivalente.<w:br/>2. Petición en que se concreta la solicitud<w:br/>3. Dirección a efectos de notificaciones, fecha y firma del solicitante";
        break;

        case 2:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>No figuran datos personales del interesado/a en nuestros sistemas de tratamiento de datos, por lo que no podemos proceder a la supresión de datos solicitada por el/la interesado/a.";
        break;

        case 3:
        $texto.="Se adjunta copia de la solicitud de supresión recibida, a fin de que el/la responsable del tratamiento resuelva sobre la misma.";
        break;

        case 4:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>Los datos personales del interesado se encuentran bloqueados, por lo que no pueden ser tratados excepto para su puesta a disposición de las Administraciones Públicas competentes, Jueces y Tribunales y Ministerio Fiscal. Una vez transcurrido el plazo de prescripción que corresponda, los datos personales bloqueados son eliminados de nuestros sistemas de tratamiento/archivos –\"Artículo 5.1.e) RGPD (limitación del plazo de conservación) y artículo 32 LOPD-GDD (\"Bloqueo de los datos. 1. El responsable del tratamiento estará obligado a bloquear los datos cuando proceda a su rectificación o supresión. 2. El bloqueo de los datos consiste en la identificación y reserva de los mismos, adoptando medidas técnicas y organizativas, para impedir su tratamiento, incluyendo su visualización, excepto para la puesta a disposición de los datos a los jueces y tribunales, el Ministerio Fiscal o las Administraciones Públicas competentes, en particular de las autoridades de protección de datos, para la exigencia de posibles responsabilidades derivadas del tratamiento y solo por el plazo de prescripción de las mismas. Transcurrido ese plazo deberá procederse a la destrucción de los datos. 3. Los datos bloqueados no podrán ser tratados para ninguna finalidad distinta de la señalada en el apartado anterior.\")-";
        break;

        case 5:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/><w:br/>Le deniega la solicitud de supresión planteada, en virtud del artículo 17.3 del Reglamento (UE) 2016/679 (RGPD), que establece lo siguiente:<w:br/><w:br/>\"Artículo 17 RGPD. Derecho de supresión (el derecho al olvido).<w:br/><w:br/>1. El interesado tendrá derecho a obtener sin dilación indebida del responsable del tratamiento la supresión de los datos personales que le conciernan, el cual estará obligado a suprimir sin dilación indebida los datos personales cuando concurra alguna de las circunstancias siguientes:<w:br/><w:br/>a) los datos personales ya no sean necesarios en relación con los fines para los que fueron recogidos o tratados de otro modo;<w:br/><w:br/>b) el interesado retire el consentimiento en que se basa el tratamiento de conformidad con el artículo 6, apartado 1, letra a), o el artículo 9, apartado 2, letra a), y este no se base en otro fundamento jurídico;<w:br/><w:br/>c) el interesado se oponga al tratamiento con arreglo al artículo 21, apartado 1, y no prevalezcan otros motivos legítimos para el tratamiento, o el interesado se oponga al tratamiento con arreglo al artículo 21, apartado 2;<w:br/><w:br/>d) los datos personales hayan sido tratados ilícitamente;<w:br/><w:br/>e) los datos personales deban suprimirse para el cumplimiento de una obligación legal establecida en el Derecho de la Unión o de los Estados miembros que se aplique al responsable del tratamiento;<w:br/><w:br/>f) los datos personales se hayan obtenido en relación con la oferta de servicios de la sociedad de la información mencionados en el artículo 8, apartado 1.<w:br/><w:br/>2. Cuando haya hecho públicos los datos personales y esté obligado, en virtud de lo dispuesto en el apartado 1, a suprimir dichos datos, el responsable del tratamiento, teniendo en cuenta la tecnología disponible y el coste de su aplicación, adoptará medidas razonables, incluidas medidas técnicas, con miras a informar a los responsables que estén tratando los datos personales de la solicitud del interesado de supresión de cualquier enlace a esos datos personales, o cualquier copia o réplica de los mismos.<w:br/><w:br/>3. Los apartados 1 y 2 no se aplicarán cuando el tratamiento sea necesario:<w:br/><w:br/>a) para ejercer el derecho a la libertad de expresión e información;<w:br/><w:br/>b)para el cumplimiento de una obligación legal que requiera el tratamiento de datos impuesta por el Derecho de la Unión o de los Estados miembros que se aplique al responsable del tratamiento, o para el cumplimiento de una misión realizada en interés público o en el ejercicio de poderes públicos conferidos al responsable;<w:br/><w:br/>c) por razones de interés público en el ámbito de la salud pública de conformidad con el artículo 9, apartado 2, letras h) e i), y apartado 3;<w:br/><w:br/>d) con fines de archivo en interés público, fines de investigación científica o histórica o fines estadísticos, de conformidad con el artículo 89, apartado 1, en la medida en que el derecho indicado en el apartado 1 pudiera hacer imposible u obstaculizar gravemente el logro de los objetivos de dicho tratamiento, o<w:br/><w:br/>e) para la formulación, el ejercicio o la defensa de reclamaciones.\"";
        break;

        case 6:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que en cumplimiento de lo establecido por los artículos 17 del Reglamento General de Protección de Datos y 15 de la LOPD-GDD, se ha procedido a la supresión de datos solicitada.";
        break;
    }
    $texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
   	if($datos['respuesta']==1){
    	$texto.="<w:br/><w:br/>Deberá subsanar este defecto ante ".$datos['razonSocial'].", con domicilio a efectos de notificaciones en ".$datos['direccion']." para que podamos atender su petición";
    }
    $texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];

    if($datos['respuesta']==3){
    	$texto.="<w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/>RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": COMUNICACIÓN AL INTERESADO EN CONCEPTO DE ENCARGADO/ DEL TRATAMIENTO<w:br/><w:br/>El presente escrito es para dar respuesta a la petición de supresión de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['direccion']."<w:br/><w:br/>";
        if($datos['representante']!=''){
        	$texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    	} else {
        	$texto.=$datos['interesado'];
    	}
    	$texto.=$direccion;
    	$texto.=$datos['razonSocial']." le informa que:<w:br/><w:br/>1º. El tratamiento que realiza respecto los datos personales que solicita se supriman, lo realizamos en concepto de encargado del tratamiento, por lo que deberá solicitar la supresión de dichos datos personales ante el Responsable del Tratamiento ".$datos['direccion']." con dirección en ".$datos['responsableDireccion'].".<w:br/><w:br/>2º. Se ha procedido a trasladar al Responsable del Tratamiento referenciado la solicitud de supresión referenciada en el presente escrito.";
    	$texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
    	$texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];
    }
    
    $documento->setValue("texto",utf8_decode($texto));

    $documento->save('../documentos/derechos/respuesta.docx');
    return $fichero;
}

function wordCesionario($PHPWord,$codigoDerecho){
    global $_CONFIG;
    $datos=consultaBD('SELECT derechos.*, clientes.razonSocial, CONCAT(clientes.domicilio,", CP ",clientes.cp,", ",clientes.localidad,", ",clientes.provincia) AS direccionCliente, ficheroLogo, administrador FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo WHERE derechos.codigo='.$codigoDerecho,true,true);
    $tipo=$datos['tipo'];
    $ref=$datos['referencia']<10?'0'.$datos['referencia']:$datos['referencia'];
    $fichero='COMUNICACIÓN_A_CESIONARIO_DE_'.mb_strtoupper($tipo).'_SOBRE_DATOS_INTERESADO_REF_'.$ref.'.docx';
    $documento=$PHPWord->loadTemplate('../documentos/derechos/plantillaDerechos.docx');
    $documento->setValue("true","");
    $documento->setValue("cliente",utf8_decode(sanearCaracteresDerechos($datos['razonSocial'])));  
    $documento->setValue("titulo",utf8_decode("COMUNICACIÓN A CESIONARIO DE ".mb_strtoupper($tipo)." SOBRE DATOS INTERESADO ".$datos['interesado']." REF. ".$ref));

    reemplazarLogoDerechos($documento,$datos,'image2.png'); 
    
    $texto="COMUNICACIÓN A CESIONARIO DE DATOS PERSONALES DE SUPRESIÓN EFECTUADA SOBRE DICHOS DATOS.<w:br/><w:br/><w:br/><w:br/>".$datos['razonSocial'].", en concepto de responsable del tratamiento de datos de carácter personal cedidos a ".$datos['nombreCesionario'].", mediante el presente escrito informamos al cesionario referenciado que hemos efectuado sobre los datos objeto de dicha cesión la supresión solicitada el día ".formateaFechaWeb($datos['fechaRecepcion'])." realizada por D./ª";
    $direccion=" con domicilio a efectos de notificaciones en ".$datos['responsableDireccion']."<w:br/><w:br/>";
    
    if($datos['representante']!=''){
        $texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    } else {
        $texto.=$datos['interesado'];
    }
    $texto.=$direccion;
    $texto.="<w:br/><w:br/>Adjuntamos copia de la solicitud de supresión recibida y efectuada, objeto del presente comunicado.";
    $texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
    $texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];

    $documento->setValue("texto",utf8_decode($texto));

    $documento->save('../documentos/derechos/respuesta.docx');
    return $fichero;
}
//Fin parte de agrupaciones
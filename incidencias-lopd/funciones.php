<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de incidencias

function operacionesIncidenciasLOPD(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('incidencias_lopd');
	   	$res= $res && insertaFicherosAdjuntos($_POST['codigo']);		
	}
	elseif(isset($_POST['incidente'])){
		$res=insertaDatos('incidencias_lopd');
	   	$res= $res && insertaFicherosAdjuntos($res);		
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('incidencias_lopd');
	}

	mensajeResultado('incidente',$res,'Incidencia LOPD');
    mensajeResultado('elimina',$res,'Incidencia LOPD', true);
}


function gestionIncidenciaLOPD(){
	operacionesIncidenciasLOPD();
	
	abreVentanaGestion('Gestión de Incidencias LOPD','?','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('incidencias_lopd');
	$cliente=datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');

	echo "<fieldset class='sinFlotar'>";
	abreColumnaCampos();
		if($datos!=false){
			campoOculto($datos,'referencia');
			$fecha=explode('-',$datos['fechaDeteccion']);
			campoTexto('referenciaMostrar','Referencia',str_pad($datos['referencia'],4,'0',STR_PAD_LEFT).'/'.$fecha[0],'input-small',true);
		}
		else{
			campoOculto('','referencia');
		    campoTexto('referenciaMostrar','Referencia','','input-small',true);
		}	
	//campoTexto('referencia','Referencia',$datos,'input-small');
	if($_SESSION['tipoUsuario']=='CLIENTE'){
		campoOculto($cliente['codigoCliente'],'codigoCliente');
	}elseif($_SESSION['tipoUsuario']=='ADMIN'){
		campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, razonSocial AS texto FROM clientes WHERE posibleCliente='NO';",$datos);		
	}else{
		campoSelectConsulta('codigoCliente','Cliente',"SELECT codigo, razonSocial AS texto FROM clientes WHERE posibleCliente='NO';",$datos);		
	}

	campoTexto('incidente','Incidente',$datos);
	campoFecha('fechaDeteccion','Fecha Detección',$datos);
	campoFecha('fechaOcurrio','Fecha en que ocurrió',$datos);
	campoTexto('ficherosDatosQuiebra','Fichero/s en el que están incluidos los datos objeto de quiebra de seguridad',$datos);
	campoTexto('naturalezaQuiebra','Naturaleza de la quiebra de seguridad',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
	campoTexto('categoriaDatosAfectados','Categorías de datos afectados',$datos);
	campoTexto('categoriaInteresadosAfectados','Categoría de interesados afectados',$datos);
	campoTexto('medidasAdoptadas','Medidas adoptadas para solventar la quiebra',$datos);
	campoTexto('medidasAplicadas','Medidas aplicadas para paliar posibles efectos negativos sobre los interesados',$datos);							
	campoFecha('fechaNotificadaAutoridad','Notificada a la autoridad de protección de datos competente el día',$datos);
	areaTexto('motivos','Motivos por los que no se ha notificado a la AEPD en el plazo de 72 horas',$datos); 
	campoFecha('fechaNotificadaInteresados','Notificada a los interesados el día',$datos);
	campoSelect('cerrada','Incidencia Cerrada',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick');
	echo "<div id='cajaFecha'>";
	campoFecha('fechaResolucion','Fecha de cierre y aprobación por el responsable del tratamiento',$datos);
	echo "</div>";
	cierraColumnaCampos();
	echo "</fieldset>";

	echo "<h3 class='apartadoFormulario'> DATOS ADJUNTOS:</h3>";
	echo "<fieldset class='sinFlotar'>";
	creaTablaFicheros($datos);
	echo "</fieldset>";

	cierraVentanaGestion('index.php',true);
}


function imprimeIncidenciasLOPD(){

	$cliente=datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');

	if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT *,CONCAT(LPAD(referencia,4,0),'/',YEAR(fechaDeteccion)) AS referenciaFormateada FROM incidencias_lopd",true);
	}elseif($_SESSION['tipoUsuario'] == 'CLIENTE'){
		$consulta=consultaBD("SELECT *,CONCAT(LPAD(referencia,4,0),'/',YEAR(fechaDeteccion)) AS referenciaFormateada FROM incidencias_lopd WHERE codigoCliente='".$cliente['codigoCliente']."'",true);
	} 
	else{
		$consulta=consultaBD("SELECT *,CONCAT(LPAD(referencia,4,0),'/',YEAR(fechaDeteccion)) AS referenciaFormateada FROM incidencias_lopd",true);
	}	
	//$consulta=consultaBD("SELECT * FROM incidencias_lopd",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$datosCliente=datosRegistro('clientes',$datos['codigoCliente']);
		if($datos['fechaDeteccion']!=''){
			$notificadoAutoridad='SI';
		}else{
			$notificadoAutoridad='NO';			
		}

		echo "<tr>
				<td> ".$datos['referenciaFormateada']." </td>
				<td> ".$datosCliente['razonSocial']." </td>				
				<td> ".$datos['incidente']." </td>				
				<td> ".formateaFechaWeb($datos['fechaDeteccion'])." </td>
				<td> $notificadoAutoridad </td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						</ul>
					</div>				
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

function obtieneDepartamentoIncidencia($departamento){
	$departamentos=array('ADMINISTRACION'=>'Administración','COMERCIAL'=>'Comercial','TECNICO'=>'Técnico');
	return $departamentos[$departamento];
}

function obtienePrioridadIncidencia($prioridad){
	$prioridades=array('NORMAL'=>"<span class='label label-info'>Normal</span>",'ALTA'=>"<span class='label label-danger'>Alta</span>",'BAJA'=>"<span class='label'>Baja</span>");
	return $prioridades[$prioridad];
}

function obtieneEstadoIncidencia($estado){
	$estados=array('PENDIENTE'=>"<span class='label label-danger'>Pendiente</span>",'RESUELTA'=>"<span class='label label-success'>Resuelta</span>",'ENPROCESO'=>"<span class='label label-warning'>En proceso</span>");
	return $estados[$estado];
}




function generaDatosGraficoIncidencias(){
	$datos=array('PENDIENTE'=>0,'RESUELTA'=>0);

	$cliente=datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');

	if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT * FROM incidencias_lopd",true);
	}elseif($_SESSION['tipoUsuario'] == 'CLIENTE'){
		$consulta=consultaBD("SELECT * FROM incidencias_lopd WHERE codigoCliente='".$cliente['codigoCliente']."'",true);
	} 
	else{
		$consulta=consultaBD("SELECT * FROM incidencias_lopd",true);
	}	

	//$consulta=consultaBD("SELECT * FROM incidencias_lopd;",true);	
	while($datosConsulta=mysql_fetch_assoc($consulta)){
		$datos=clasificaEstadoIncidencia($datosConsulta['cerrada'],$datos);
	}

	return $datos;
}

function clasificaEstadoIncidencia($estado,$datos){
	if($estado=='SI'){
		$datos['RESUELTA']++;
	}
	else{
		$datos['PENDIENTE']=1;	
	}

	return $datos;
}

function creaTablaFicheros($datos){
	echo '<table class="table table-striped table-bordered" style="width:50%;margin-left:220px;" id="tablaAdjuntos">
	  		<thead>
				<tr>
					<th class="centro">Ficheros</th>
					<th></th>
				</tr>
			</thead>
			<tbody>';
				$i=0;

				if($datos){
					$consulta=consultaBD("SELECT * FROM incidencias_lopd_adjuntos WHERE codigoIncidenciaLopd='".$datos['codigo']."';", true);				  
					while($datosAdjuntos=mysql_fetch_assoc($consulta)){
					 	echo "<tr>";
						campoFichero('ficheroAdjunto'.$i,'',1,$datosAdjuntos['ficheroAdjunto'],$datosAdjuntos['directorio']);
						echo"
								<td>
									<input type='checkbox' name='filasTabla[]' value='$i'>
					        	</td>
							</tr>";
						$i++;
					}
				}


				if($i==0){
				echo '<tr>';
						campoFichero('ficheroAdjunto'.$i,'',1);
						echo "<td>
                                <input type='checkbox' name='filasTabla[]' value='1'>
                             </td>";
				echo '</tr>';
				}

				echo '</tbody>
				</table>
				<center>
        			<button type="button" class="btn btn-success" onclick="insertaFila(\'tablaAdjuntos\');"><i class="icon-plus"></i></button> 
        			<button type="button" class="btn btn-danger" onclick="eliminaFila(\'tablaAdjuntos\');"><i class="icon-minus"></i></button> 
    			</center>';
}

function insertaFicherosAdjuntos($codigo){
	global $_CONFIG;
	$res=true;
	$i=0;
	
	$res = $res && consultaBD("DELETE FROM incidencias_lopd_adjuntos WHERE codigoIncidenciaLopd='$codigo'", true);	
	
	conexionBD();
	while(isset($_FILES['ficheroAdjunto'.$i])){
		if($_FILES['ficheroAdjunto'.$i]['tmp_name']!=''){
			$fichero=subeDocumento('ficheroAdjunto'.$i,time().'-'.$i,'../documentos/incidencias-lopd/');
			$res=consultaBD('INSERT INTO incidencias_lopd_adjuntos VALUES(NULL,'.$codigo.',"'.$fichero.'","../documentos/incidencias-lopd/")');
		}
		$i++;
	}
	cierraBD();
	return $res;
}

function generaDocumento($codigoCliente){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
	
	conexionBD();

	anexoXLOPD($codigoCliente, $PHPWord, 'anexoX.docx');

	cierraBD();	

}

function anexoXLOPD($codigo, $PHPWord, $nombreFichero=''){
	global $_CONFIG;
	$fichero='ANEXO_X_INCIDENCIAS.docx';
	$datos=consultaBD("SELECT clientes.codigo, clientes.razonSocial, clientes.domicilio, clientes.ficheroLogo, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE clientes.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",utf8_decode(date("d/m/Y")));

	reemplazarLogo($documento,$datos,'image2.png');


	$tablaIncidencias=fechaTabla(date('d/m/Y'),'QUIEBRAS DE SEGURIDAD');

	$tablaIncidencias.='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="796"/><w:gridCol w:w="1832"/><w:gridCol w:w="2012"/><w:gridCol w:w="2015"/><w:gridCol w:w="2065"/></w:tblGrid><w:tr w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w14:paraId="51541499" w14:textId="77777777" w:rsidTr="00A07FBD"><w:tc><w:tcPr><w:tcW w:w="1139" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="5D5FC966" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2618" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3EBE3515" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>INCIDENTE PRODUCIDO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3330" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2207371B" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3220" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="09D9BD6F" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>NOTIFICA A LA AUTORIDAD</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3088" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="01450EC4" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="008F5EA5" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>NOTIFICADO A LOS INTERESADOS</w:t></w:r></w:p></w:tc></w:tr>';

	$i=1;	
	$incidencias=consultaBD("SELECT * FROM incidencias_lopd WHERE codigoCliente='".$datos['codigo']."';",true);
	while($incidencia=mysql_fetch_assoc($incidencias)){
		$ref=str_pad($i,2,'0',STR_PAD_LEFT);
		if($incidencia['fechaNotificadaAutoridad']!=''){
			$notificada='SI';
		}else{
			$notificada='NO';			
		}
		$tablaIncidencias.='<w:tr w:rsidR="00E17BBB" w14:paraId="64E5EF4C" w14:textId="77777777" w:rsidTr="00E17BBB"><w:trPr><w:trHeight w:val="624"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1139" w:type="dxa"/></w:tcPr><w:p w14:paraId="21A70F8C" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2618" w:type="dxa"/></w:tcPr><w:p w14:paraId="210C2442" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="00E17BBB" w:rsidRDefault="00E17BBB" w:rsidP="00E17BBB"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>'.$incidencia['incidente'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3330" w:type="dxa"/></w:tcPr><w:p w14:paraId="69D7CCDE" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>'.formateaFechaWeb($incidencia['fechaOcurrio']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>7</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3220" w:type="dxa"/></w:tcPr><w:p w14:paraId="16DB5923" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRPr="002B0C55" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>'.formateaFechaWeb($incidencia['fechaNotificadaAutoridad']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3088" w:type="dxa"/></w:tcPr><w:p w14:paraId="77F74A2F" w14:textId="77777777" w:rsidR="00E17BBB" w:rsidRDefault="00E17BBB" w:rsidP="00A07FBD"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/></w:rPr><w:t>'.formateaFechaWeb($incidencia['fechaNotificadaInteresados']).'</w:t></w:r></w:p></w:tc></w:tr>';

		$i++;		
	}	
	$total=$i-1;
	$tablaIncidencias.='</w:tbl>'.pieTabla($total);

	$tablaTexto='';
	$j=1;
	$incidenciasAux=consultaBD("SELECT * FROM incidencias_lopd WHERE codigoCliente='".$datos['codigo']."';",true);
	while($incidenciaAux=mysql_fetch_assoc($incidenciasAux)){
		$ref=str_pad($j,4,'0',STR_PAD_LEFT);
		$tablaTexto.='<w:p w14:paraId="152D5665" w14:textId="77777777" w:rsidR="00FF760B" w:rsidRDefault="00FF760B" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Verdana" w:eastAsia="Times New Roman" w:hAnsi="Verdana" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr></w:p><w:p w14:paraId="0758240E" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/><w:color w:val="000000"/><w:u w:val="single"/></w:rPr><w:t>Referencia</w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/><w:u w:val="single"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p><w:p w14:paraId="5B6BEBE0" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="5644D275" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">INCIDENTE </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['incidente'].'</w:t></w:r></w:p><w:p w14:paraId="294AA36E" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="2F6C6D08" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Fecha  </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.formateaFechaWeb($incidenciaAux['fechaOcurrio']).'</w:t></w:r></w:p><w:p w14:paraId="6D9F9CFA" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1F561131" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Fichero en el que están incluidos los datos objeto de quiebra de seguridad  </w:t></w:r><w:r w:rsidR="00396720" w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">  </w:t></w:r><w:r w:rsidR="00396720" w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['ficherosDatosQuiebra'].'</w:t></w:r></w:p><w:p w14:paraId="43DE4DB5" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4257E87A" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Naturaleza de la quiebra de seguridad    </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['naturalezaQuiebra'].'</w:t></w:r></w:p><w:p w14:paraId="417E745A" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="2B4266FB" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Categorías de datos afectados  </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['categoriaDatosAfectados'].'</w:t></w:r></w:p><w:p w14:paraId="2D756D32" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="0CEAF1FB" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Categoría de interesados afectados    </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['categoriaInteresadosAfectados'].'</w:t></w:r></w:p><w:p w14:paraId="2C51E35B" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="5C8BE43C" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Medidas adoptadas para solventar la quiebra  </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['medidasAdoptadas'].'</w:t></w:r></w:p><w:p w14:paraId="7DBFCB41" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="41E56750" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Medidas aplicadas para paliar posibles efectos negativos sobre los interesados  </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['medidasAplicadas'].'</w:t></w:r></w:p><w:p w14:paraId="18B934B8" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="1428235B" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Notificada a la autoridad de protección de datos competente el día </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.formateaFechaWeb($incidenciaAux['fechaNotificadaAutoridad']).'</w:t></w:r></w:p><w:p w14:paraId="62E1097D" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="6E35D96C" w14:textId="77777777" w:rsidR="00EA637D" w:rsidRPr="00EC6CFD" w:rsidRDefault="00EA637D" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t xml:space="preserve">Notificada a los interesados el día </w:t></w:r><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.formateaFechaWeb($incidenciaAux['fechaNotificadaInteresados']).'</w:t></w:r></w:p><w:p w14:paraId="78F79C66" w14:textId="77777777" w:rsidR="00A72B20" w:rsidRPr="00EC6CFD" w:rsidRDefault="00A72B20" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr></w:p><w:p w14:paraId="22E2FC59" w14:textId="77777777" w:rsidR="00A72B20" w:rsidRPr="00EC6CFD" w:rsidRDefault="00A72B20" w:rsidP="00EA637D"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="FF0000"/></w:rPr></w:pPr><w:r w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>Motivos por los que no se ha notificado a la AEPD en el plazo de 72 horas</w:t></w:r><w:r w:rsidR="00EC4C14" w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidR="00EC4C14" w:rsidRPr="00EC6CFD"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:color w:val="000000"/></w:rPr><w:t>'.$incidenciaAux['motivos'].'</w:t></w:r></w:p>';

		$j++;
	}


	$documento->setValue("tabla",utf8_decode($tablaIncidencias));
	$documento->setValue("tablaTexto",utf8_decode($tablaTexto));	

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

function fechaTabla($fecha,$texto){
	return '<w:p w14:paraId="1060A2A2" w14:textId="7A44C6D1" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="00397BF1" w:rsidP="0016753F"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">REGISTRO DE '.$texto.' A </w:t></w:r><w:r w:rsidR="00D303CF"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$fecha.'</w:t></w:r></w:p>';
}

function pieTabla($total){
	return '<w:p w14:paraId="2CF98860" w14:textId="77777777" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="001F0F57" w:rsidP="001F0F57"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="right"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>Nº total de re</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">gistros </w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$total.'</w:t></w:r></w:p>';
}

function reemplazarLogo($documento,$datos,$imagen='image1.png',$campo='ficheroLogo'){
	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos[$campo]);
    if($hayLogo!='NO' && $hayLogo!=''){
    	$logo = '../documentos/logos-clientes/'.$datos[$campo];
    	$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
		if (!copy($logo, $nuevo_logo)) {
    		echo "Error al copiar $fichero...\n";
		}	
		$documento->replaceImage('../documentos/logos-clientes/',$imagen);	
	}
}

function datosPersonales($datos,$formulario){
	if($datos['familia']=='LOPD1'){
		$datos['razonSocial']=$formulario['pregunta3'];
		$datos['domicilio']=$formulario['pregunta4'];
		$datos['cp']=$formulario['pregunta10'];
		$datos['localidad']=$formulario['pregunta5'];
		$datos['provincia']=convertirMinuscula($formulario['pregunta11']);
		$datos['cif']=$formulario['pregunta9'];
	}

	return $datos;
}

function sanearCaracteres($texto){
	$texto = str_replace( '&', '&#38;', $texto);
	$texto = str_replace( '–', '&#45;', $texto);
	$texto = str_replace( '<br />', '<w:br/>', $texto);
	$texto = str_replace( '"', '', $texto);
	return $texto;
}

function obtieneReferenciaIncidenciaLOPD(){
	$datos=arrayFormulario();
	$fecha=explode('-',$datos['fechaDeteccion']);
	$consulta=consultaBD("SELECT MAX(referencia) AS max FROM incidencias_lopd WHERE codigoCliente=".$datos['codigoCliente']." AND YEAR(fechaDeteccion)='".$fecha[0]."' ORDER BY referencia DESC LIMIT 1;",true,true);
	$res=array();
	$res['referencia']=$consulta['max']+1;
	$res['referenciaMostrar']=str_pad($res['referencia'],4,'0',STR_PAD_LEFT).'/'.$fecha[0];
	echo json_encode($res);
}

//Fin parte de incidencias
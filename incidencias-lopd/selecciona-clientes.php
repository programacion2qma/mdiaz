<?php
  $seccionActiva=2;
  include_once('../cabecera.php');

  $cliente=datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');

?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span3"></div>

        <div class="span6 margenAb">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class='icon-edit'></i><i class='icon-chevron-right'></i><i class="icon-search-plus"></i>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Por favor, seleccione un cliente del listado:</h6>
                    <form action="generaDocumento.php" method="post" class="centro noAjax">
                      <?php
                          if($_SESSION['tipoUsuario'] == 'ADMIN'){
                              $sql="SELECT DISTINCT c.codigo, c.razonSocial AS texto FROM clientes c INNER JOIN incidencias_lopd f ON c.codigo=f.codigoCliente ORDER BY razonSocial;";
                              campoSelectConsulta('cliente','Cliente',$sql);
                          }
                          elseif($_SESSION['tipoUsuario']=='CLIENTE'){
                              $sql="SELECT DISTINCT c.codigo, c.razonSocial AS texto FROM clientes c INNER JOIN incidencias_lopd f ON c.codigo=f.codigoCliente WHERE codigoCliente='".$cliente['codigoCliente']."' ORDER BY razonSocial;";
                              campoSelectConsulta('cliente','Cliente',$sql,$cliente['codigoCliente']);
                          } 
                          else{
                                $sql="SELECT DISTINCT c.codigo, c.razonSocial AS texto FROM clientes c INNER JOIN incidencias_lopd f ON c.codigo=f.codigoCliente WHERE c.empleado LIKE ".$_SESSION['codigoU']." ORDER BY razonSocial;";
                                campoSelectConsulta('cliente','Cliente',$sql);
                          }

                          //campoSelectConsulta('cliente','Cliente',$sql);
                      ?>
                      <br />
                      <a href="index.php" class="btn btn-default"><i class="icon-chevron-left"></i> Volver</a>
                      <button type="submit" class="btn btn-propio">Descargar Documento <i class="icon-cloud-download"></i></button>
                    </form>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('select').selectpicker();
  });
</script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
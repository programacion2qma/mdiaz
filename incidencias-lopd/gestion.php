<?php
  $seccionActiva=5;
  include_once("../cabecera.php");
  gestionIncidenciaLOPD();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){
      $(this).datepicker('hide');
      obtenerReferencia();
    });
    $('.selectpicker').selectpicker();

    oyenteResolucion();
    $('select[name=cerrada]').change(function(){
    	oyenteResolucion();
    });

    obtenerReferencia();
    $('#codigoCliente').change(function(){
      obtenerReferencia();
    });
  });

  function oyenteResolucion(){
  	if($('select[name=cerrada]').val()=='SI'){
  		$('#cajaFecha').removeClass('hide');
  	}
  	else{
  		$('#cajaFecha').addClass('hide');
  	}
  }

function obtenerReferencia(){
  if($('#codigo').length==0 && $('#codigoCliente')!='NULL'){
    if($('#codigoCliente.hide').length || $('#codigoCliente option:selected').val()!='NULL'){
      if($('#codigoCliente.hide').length){
        var codigoCliente=$('#codigoCliente.hide').val();
      } else {
        var codigoCliente=$('#codigoCliente option:selected').val();
      }
      var consulta=$.post('../listadoAjax.php?include=incidencias-lopd&funcion=obtieneReferenciaIncidenciaLOPD();',{'codigoCliente':codigoCliente,'fechaDeteccion':$('#fechaDeteccion').val()},
      function(respuesta){
        $('#referencia').val(respuesta.referencia);
       $('#referenciaMostrar').val(respuesta.referenciaMostrar);
      },'json');
    } else {
      $('#referencia').val('');
      $('#referenciaMostrar').val('');
    }
  }
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de series de facturas

function operacionesSeriesFacturas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('series_facturas');
	}
	elseif(isset($_POST['serie'])){
		$res=insertaDatos('series_facturas');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('series_facturas');
	}

	mensajeResultado('serie',$res,'Serie');
    mensajeResultado('elimina',$res,'Serie', true);
}


function listadoSeriesFacturas(){
	global $_CONFIG;

	$columnas=array('serie','observaciones','activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT * FROM series_facturas $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT * FROM series_facturas $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['serie'],
			$datos['observaciones'],
			creaBotonDetalles("series-facturas/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionSerieFactura(){
	operacionesSeriesFacturas();

	abreVentanaGestion('Gestión de series de facturas','?','span3','icon-edit','margenAb');
	$datos=compruebaDatos('series_facturas');

	campoTexto('serie','Serie (2 caracteres)',$datos,'input-mini');
	areaTexto('observaciones','Observaciones',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();
	
	campoRadio('activo','Activo',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}

function filtroSeriesFacturas(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoTexto(0,'Serie','','input-mini');
	campoTexto(1,'Observaciones','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(2,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function creaBotonesGestionSeriesFacturas(){
	global $_CONFIG;
	
    echo '  
    <a class="btn-floating btn-large btn-default btn-creacion2" href="'.$_CONFIG['raiz'].'facturas/" title="Volver a Facturas"><i class="icon-chevron-left"></i></a>
    <a class="btn-floating btn-large btn-success btn-creacion" href="gestion.php" title="Nuevo registro"><i class="icon-plus"></i></a>
    <a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
}

//Fin parte de series de facturas
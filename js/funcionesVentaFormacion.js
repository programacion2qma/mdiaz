$(document).ready(function(){
	quitaObligatoriedadRadios('#cajaTrabajador');

	if(!$('#observacionesParaFactura').hasClass('hide')){
		$('#observacionesParaFactura').wysihtml5({locale: "es-ES"});
	}

	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	oyenteAccionFormativa();
	$('#codigoAccionFormativa').change(function(){
		oyenteAccionFormativa();
	});

	$('#crearEmpresa').click(function(){
		$('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	$('#crea-codigoAccionFormativa').click(function(){
		$('#cajaAccionFormativa').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	$('#enviar').click(function(){
		creaEmpresaParticipante();
	});

	$('#creaAccionFormativa').click(function(){
		creaAccionFormativa();
	});

	oyenteDatosCliente();
	oyenteAlumnos();


	//Oyentes botones de guardado
	$('#guardar').click(function(){
		$('form#edit-profile').attr('action','index.php');
		if($('#confirmada').val()=='NO' || ($('#confirmada').val()=='NO' && $('#privado').val()!=undefined)){
			$('form#edit-profile').submit();
		}
	});

	$('#guardarNuevo').click(function(){
		$('form#edit-profile').attr('action','?');
		if($('#confirmada').val()=='NO' || ($('#confirmada').val()=='NO' && $('#privado').val()!=undefined)){
			$('form#edit-profile').submit();
		}
	});
	//Fin oyentes botones de guardado


	//Parte de complementos
	oyenteComplemento('#tablaComplementos .selectComplemento');
	//Fin parte de complementos

	//Oyentes botón creación alumno
	oyenteVentanaCreacionTrabajador();

	$('#creaTrabajador').click(function(){
		creaTrabajador();
	});
	//Fin oyentes botón creación alumno

	//Oyente estado (para no dejar marcar "Válida" si no se ha llamado al cliente)
	$('#estado').change(function(){
		oyenteEstado();
	});

	$(document).on('change','.celdaCliente select',function(){
		oyenteEstado();
	});
	//Fin oyente estado


	//Oyente de obligatoriedad del campo "Cofinanciación" (solo en ventas confirmadas)
	oyenteTipoVenta();
	$('input[name=privado]').change(function(){
		oyenteTipoVenta();
	});
	//Fin oyente de obligatoriedad del campo "Cofinanciación" (solo en ventas confirmadas)

	//NUEVO HEREDADO DE INICIOS DE GRUPO
	oyentePlantillayTrabajadoresCliente('#tablaEmpresasParticipantes');
	//FIN NUEVO HEREDADO DE INICIOS DE GRUPO

	//Oyente cálculo automático precio factura
	oyentePrecioFactura();
	//Fin oyente cálculo automático  precio factura

	//Filtrado de alumnos por clientes
	oyenteAlumnosYcomplementos();
	//Fin filtrado alumnos por clientes
});

function oyenteAccionFormativa(){
	var codigoAccion=$('#codigoAccionFormativa').val();
	if(codigoAccion!='NULL'){
		//Animación de carga
		$('#accionFormativa').html('<i class="icon-spinner icon-spin"></i>');
		$('#horas').html('<i class="icon-spinner icon-spin"></i>');

		var consulta=$.post('../listadoAjax.php?include=inicios-grupos&funcion=consultaDatosAccionFormativa();',{'codigoAccion':codigoAccion});
		consulta.done(function(respuesta){
			var array=respuesta.split('&{}&');//Los valores de la respuesta AJAX están serializados con un delimitador personalizado.
			var coste=array[12];
			
			$('#precioReferencia').text(formateaNumeroWeb(coste)+' €');
		});
	}
}


function creaAccionFormativa(){
	$('#creaAccionFormativa').html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var accion=$('#accionAF').val();

	var consulta=$.post('../listadoAjax.php?funcion=creaAccionFormativaVenta();',{
		'accion':accion
	});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar la acción formativa.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			actualizaSelectAF(respuesta,accion);
		}

		$('#cajaAccionFormativa').modal('hide');
		$('#creaAccionFormativa').html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original

		//Borrado de valores del formulario (para siguientes inserciones)
		$('#accionAF').val('');
	});
}

function actualizaSelectAF(codigo,nombre){
	$('#codigoAccionFormativa option[value=NULL]').after('<option value="'+codigo+'">'+nombre+'</option>');
	$('#codigoAccionFormativa').selectpicker('refresh');
}



function oyenteAlumnos(){
	var codigoCliente=$('#codigoCliente').val();

	var consulta=$('#consultaAlumnos').text();//No escribo la consulta directamente, la estraigo de divOculto
	consulta+=" AND codigoCliente="+codigoCliente+" ORDER BY nombre";
	$('#tablaAlumnos select.selectAlumno').each(function(){
		$(this).attr('consulta',consulta);
	});
}

/*function insertaFilaAlumno(){

	insertaFila('tablaAlumnos');
	oyenteAlumno($('#tablaAlumnos > tbody > tr:last .selectAlumno'));
	consultaSelectAjax($('#tablaAlumnos > tbody > tr:last select.selectAjax'));
	actualizaBotonSelectAjax($('#tablaAlumnos > tbody > tr:last'),$('#tablaAlumnos > tbody > tr:last select.selectAjax'));
	oyenteVentanaCreacionTrabajador('#tablaAlumnos > tbody > tr:last .crea-codigoTrabajador');
}

//No dejar dos veces introducir el mismo alumno
function oyenteAlumno(selector){
	$(selector).change(function(){
		var codigoAlumno=$(this).val();

		var ocurrencias=0;

		$('select.selectAlumno').each(function(){
			if($(this).val()==codigoAlumno){
				ocurrencias++;
			}
		});

		if(ocurrencias>1){
			alert("Ha seleccionado el mismo participante más de una vez. Por favor seleccione otro participante.");
			$(this).selectpicker('val','NULL');
		}
	});
}*/


function insertaComplemento(){
	insertaFila("tablaComplementos");
	oyenteComplemento($('#tablaComplementos tr:last .selectComplemento'));
}

function oyenteComplemento(selector){
	$(selector).change(function(){
		var codigoComplemento=$(this).val();
		var fila=obtieneFilaCampo($(this));
		var id=$(this).attr('id').replace('codigo','').replace('#','').replace(/\d/g,'');

		var consulta=$.post('../listadoAjax.php?funcion=consultaPrecioComplemento();',{
			'codigoComplemento':codigoComplemento
		});

		consulta.done(function(respuesta){
			$('#precio'+id+fila).val(respuesta);
		});
	});
}

function oyenteVentanaCreacionTrabajador(){
	$('.crea-codigoTrabajador').unbind();

	$('.crea-codigoTrabajador').each(function(){
		var campo=$(this).parent().find('select.selectAjax');
		var id=campo.attr('id');

		$(this).attr('id','crea-'+id);

		$(this).click(function(){
			obtieneCuentasSSCliente($(this).attr('id'));
			$('#cajaTrabajador').modal({'show':true,'backdrop':'static','keyboard':false});
		});
	});
}

function obtieneCuentasSSCliente(idBoton){
	idBoton=idBoton.split('-');
	idBoton=idBoton[1].replace('codigoTrabajador','');

	var codigoCliente=$('#codigoCliente'+idBoton).val();

	if(codigoCliente!='NULL'){
		var consulta=$.post('../listadoAjax.php?funcion=consultaCuentaSSCliente();',{'codigoCliente':codigoCliente});
		consulta.done(function(respuesta){
			$('#codigoClienteTrabajador').val(codigoCliente);
			$('#indiceCliente').text(idBoton);
			$('#cuentaSS').html(respuesta);
			$('#cuentaSS').selectpicker('refresh');
		});
	}
}

function creaTrabajador(){
	$('#creaTrabajador').html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var nombre=$('#nombre').val();
	var apellido1=$('#apellido1').val();
	var apellido2=$('#apellido2').val();
	var nif=$('#nif').val();
	var niss=$('#niss').val();
	var cuentaSS=$('#cuentaSS').val();
	var fechaNacimiento=$('#fechaNacimiento').val();
	var sexo=$('input[name=sexo]:checked').val();
	var discapacidad=$('input[name=discapacidad]:checked').val();
	var terrorismo=$('input[name=terrorismo]:checked').val();
	var violencia=$('input[name=violencia]:checked').val();
	var email=$('#email').val();
	var telefono=$('#telefono').val();
	var movil=$('#movil').val();
	var categoriaProfesional=$('#categoriaProfesional').val();
	var grupoCotizacion=$('#grupoCotizacion').val();
	var nivelEstudios=$('#nivelEstudios').val();
	var salarioBruto=$('#salarioBruto').val();
	var horasConvenio=$('#horasConvenio').val();
	var costeHora=$('#costeHora').val();
	var usuarioPlataforma=$('#usuarioPlataforma').val();
	var clavePlataforma=$('#clavePlataforma').val();
	var observacionesTrabajador=$('#observacionesTrabajador').val();
	var codigoCliente=$('#codigoClienteTrabajador').val();
	var indiceCliente=$('#indiceCliente').text();
	var fechaAlta=$('#fechaAlta').val();

	var consulta=$.post('../listadoAjax.php?funcion=creaTrabajadorVenta();',{
		'nombre':nombre,
		'apellido1':apellido1,
		'apellido2':apellido2,
		'nif':nif,
		'niss':niss,
		'cuentaSS':cuentaSS,
		'fechaNacimiento':fechaNacimiento,
		'sexo':sexo,
		'discapacidad':discapacidad,
		'terrorismo':terrorismo,
		'violencia':violencia,
		'email':email,
		'telefono':telefono,
		'movil':movil,
		'categoriaProfesional':categoriaProfesional,
		'grupoCotizacion':grupoCotizacion,
		'nivelEstudios':nivelEstudios,
		'salarioBruto':salarioBruto,
		'horasConvenio':horasConvenio,
		'costeHora':costeHora,
		'usuarioPlataforma':usuarioPlataforma,
		'clavePlataforma':clavePlataforma,
		'observacionesTrabajador':observacionesTrabajador,
		'codigoCliente':codigoCliente,
		'fechaActualizacionTrabajador':fechaAlta,
		'activo':'SI'
	});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar el alumno.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			actualizaSelectAlumnos(indiceCliente,respuesta,nif,nombre,apellido1,apellido2);
		}

		$('#cajaTrabajador').modal('hide');
		$('#creaTrabajador').html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original

		//Borrado de valores del formulario (para siguientes inserciones)
		$('#nombre').val('');
		$('#apellido1').val('');
		$('#apellido2').val('');
		$('#nif').val('');
	});
}

function actualizaSelectAlumnos(indiceCliente,codigo,nif,nombre,apellido1,apellido2){	
	for(i=0;$('#codigoTrabajador'+indiceCliente+'-'+i).val()!=undefined;i++){
		var selector='#codigoTrabajador'+indiceCliente+'-'+i;

		$(selector+' option[value=NULL]').after('<option value="'+codigo+'">'+nif+' - '+nombre+' '+apellido1+' '+apellido2+'</option>');
		$(selector).selectpicker('refresh');
	}
}

function oyenteEstado(){
	var estado=$('#estado').val();

	if(estado=='VALIDA'){
		var codigosClientes=new Array();

		for(i=0;$('#codigoCliente'+i).val()!=undefined;i++){
			codigosClientes.push($('#codigoCliente'+i).val());
		}

		var consulta=$.post('../listadoAjax.php?include=ventas&funcion=consultaLlamadaCliente();',{'codigosClientes':codigosClientes.toString()});
		consulta.done(function(respuesta){
			if(respuesta=='NO'){
				$('#estado').selectpicker('val','PENDIENTE');
				alert('No se puede marcar la venta como válida porque no se ha marcado como realizada la llamada de comprobación de datos en la ficha del cliente.');
			}
		});
	}
}

function oyenteTipoVenta(){
	if($('input[name=privado]:checked').val()=='BONIFICADA'){
		$('#cofinanciacion').addClass('obligatorio');
	}
	else{
		$('#cofinanciacion').removeClass('obligatorio');	
	}
}

function oyenteDatosCliente(){
	$('#codigoCliente').change(function(){
		var codigoCliente=$(this).val();

		if(codigoCliente!='NULL'){
			var consulta=$.post('../listadoAjax.php?funcion=consultaDatosClienteVenta();',{'codigoCliente':codigoCliente},
			function(respuesta){
				$('#codigoComercial').val(respuesta.codigoComercial);
				$('#codigoComercial').selectpicker('refresh');
			},'json');
		}

		oyenteAlumnos();
	});
}



//PARTE NUEVA DE VENTAS, HEREDADO DE INICIOS DE GRUPO ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function insertaParticipante(){
	insertaFilaParticipante("tablaEmpresasParticipantes");
	//IMPORTANTE: actualización de divs ocultos (no se actualizan por defecto con filasTabla.js)
	$('#tablaEmpresasParticipantes > tbody > tr:last').find('div.hide').attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });
    //Fin actualización divs ocultos
	
	consultaSelectAjax($('#tablaEmpresasParticipantes > tbody > tr:last .celdaCliente select.selectAjax'));

	actualizaBotonSelectAjax($('#tablaEmpresasParticipantes > tbody > tr:last'),$('#tablaEmpresasParticipantes > tbody > tr:last select.selectAjax'));

	//Para fila alumno
	$('#tablaEmpresasParticipantes > tbody > tr:last select.selectAlumno').html("<option value='NULL'></option>").selectpicker('refresh');//Elimino el alumno que viene del cliente anterior
	consultaSelectAjax($('#tablaEmpresasParticipantes > tbody > tr:last select.selectAlumno'));
	oyentePlantillayTrabajadoresCliente('#tablaEmpresasParticipantes  > tbody > tr:last');
	actualizaBotonSelectAjax($('#tablaEmpresasParticipantes > tbody > tr:last .subtabla tbody tr:last'),$('#tablaEmpresasParticipantes > tbody > tr:last .subtabla tbody tr:last select.selectAjax'));
	oyenteVentanaCreacionTrabajador();
}


function oyentePlantillayTrabajadoresCliente(selector){
	$(selector).find('.celdaCliente select.selectAjax').each(function(){
		$(this).change(function(){
			compruebaClienteRepetido($(this).val(),$(this));
			oyenteAlumnosYcomplementos();
		});
	});
}


function compruebaClienteRepetido(codigoCliente,select){
	var ocurrencias=0;

	$('.celdaCliente select').each(function(){
		if($(this).val()==codigoCliente){
			ocurrencias++;
		}
	});

	if(ocurrencias>1){
		alert("Ha seleccionado el mismo cliente más de una vez. Por favor seleccione otro cliente.");
		select.selectpicker('val','NULL');
	}

	return ocurrencias;
}


function actualizaBotonSelectAjax(fila,campo){
	var boton=fila.find('.botonSelectAjax');
	var id=campo.attr('id');
	
	boton.attr('id','boton-'+id);

	campo.change(function(){
      oyenteBotonAjax(campo);
    });
}


function oyenteAlumnosYcomplementos(){
	var codigosClientes=new Array();

	$('#tablaEmpresasParticipantes').find('.celdaCliente select.selectAjax').each(function(){//Modificación de la consulta del select de alumnos para que filtre por cliente
		var codigoCliente=$(this).val();

		if(codigoCliente!='' && codigoCliente!='NULL'){
			codigosClientes.push(codigoCliente);
			var fila=obtieneFilaCampo($(this));

			var consulta=$('#consultaAlumnos').text();//No escribo la consulta directamente, la estraigo de divOculto
			consulta+=" AND codigoCliente="+codigoCliente+" ORDER BY nombre";
			for(i=0;$('#codigoTrabajador'+fila+'-'+i).val()!=undefined;i++){
				$('#codigoTrabajador'+fila+'-'+i).attr('consulta',consulta);	
			}
		}
	});


	if(codigosClientes.length>0){
		var consultaComplementos=$('#consultaComplementos').text();
		consultaComplementos+=" AND clientes.codigo IN("+codigosClientes.toString()+")  ORDER BY razonSocial;";

		$('#tablaComplementos').find('select.selectAjax').each(function(){//Modificación de la consulta del select de clientes para que muestre solo los elegidos anteriormente
			$(this).attr('consulta',consultaComplementos);	
		});
	}
}

function insertaAlumno(id){
	//No puede haber más alumnos que el total marcado para el grupo. Comprobación.
	var total=$('#numParticipantes').val();
	var i=$('select.selectAlumno').length;

	if(i==total || total==''){
		alert("No se pueden añadir más participantes");
	}
	else{
		insertaFilaAlumno(id);
		//oyenteComplemento($('#'+id+' tr:last .selectComplemento'));
		oyenteAlumno($('#'+id+' > tbody > tr:last select.selectAlumno'));
		consultaSelectAjax($('#'+id+' > tbody > tr:last select.selectAlumno'));
		actualizaBotonSelectAjax($('#'+id+' > tbody > tr:last'),$('#'+id+' > tbody > tr:last select.selectAjax'));
		oyenteVentanaCreacionTrabajador();
	}
}


//No deja dos veces introducir el mismo alumno
function oyenteAlumno(selector){
	$(selector).change(function(){
		var codigoAlumno=$(this).val();

		var ocurrencias=0;

		$('select.selectAlumno').each(function(){
			if($(this).val()==codigoAlumno){
				ocurrencias++;
			}
		});

		if(ocurrencias>1){
			alert("Ha seleccionado el mismo participante más de una vez. Por favor seleccione otro participante.");
			$(this).selectpicker('val','NULL');
		}
	});
}


function oyentePrecioFactura(){
	$(document).on('change','.precioFactura,input[name=privado]',function(){
		var total=0;

		$('.precioFactura').each(function(){
			total=parseFloat(total)+formateaNumeroCalculo($(this).val());
		});

		if($('input[name=privado]:checked').val()=='OBSEQUIO'){
			$('#importeVenta').val('0,00');
		}
		else{
			$('#importeVenta').val(formateaNumeroWeb(total));
		}
	});
}
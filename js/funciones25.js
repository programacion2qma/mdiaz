$(document).ready(function(){
	if($("#permisoModificar").length && $('#permisoModificar').val()=='NO'){
      $('form input,select,textarea').attr('readonly','readonly');
      $('form input,select,textarea').attr('disabled','disabled');
      $('form button[type=submit]').addClass('hide');
      $('form button.submit').addClass('hide');
    }
});
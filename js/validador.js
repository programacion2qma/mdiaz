var mensajeMostrado = 0;
//Clientes
$('.cuentaSS').attr('maxlength',12);
$('#cif').attr('maxlength',9);

//Series de facturas
$('#serie').attr('maxlength',2);

//Trabajadores
$('#niss').attr('maxlength',12);

//La siguiente instrucción pone todos los radio como obligatorios (solicitado así) y les quita el valor por defecto (excepto para un campo concreto de clientes, solicitado así también)

//Funciones generales de validación

$('input.obligatorio,select.obligatorio').each(function(){//Para poner el asterisco a los campos obligatorios
	if($(this).parent().prop('tagName')=='TD'){//Los campos en una tabla son un caso especial
		var indice=$(this).parent().index();
		var label=$(this).parents().find('th:eq('+indice+')');
	}
	else if($(this).parent().parent().prop('tagName')=='TD'){
		var indice=$(this).parent().parent().index();
		var label=$(this).parents().find('th:eq('+indice+')');
	}
	else if($(this).parent().hasClass('input-prepend') || $(this).parent().hasClass('radio')){
		var label=$(this).parent().parent().prev();//Doble salto para los campoTextoSimbolo y los radio
	}
	else{
		var label=$(this).parent().prev();	
	}
	
	var texto=label.text();
	texto=texto.replace('*','');//Para los radio, que ponen 1 asterisco por opción
	label.html('<span class="asterisco">*</span> '+texto);
});

$(':submit').click(function(e){
	e.preventDefault();
	
	validaCamposObligatorios();
});

//El parámetro selectorFormulario se utiliza para los casos en que se use el script en páginas con ventanas modales, que tienen también #formcontrols
function validaCamposObligatorios(selectorFormulario){
	var res=0;
	
	if(selectorFormulario==undefined){
		selectorFormulario='#formcontrols form';
	}

	//Le digo que no seleccione los .bootstrap-select porque corresponden al selectpicker en sí (que hereda la clase del select original), y hay que trabajar con los select directamente.
	$(selectorFormulario).find('.obligatorio:not(.bootstrap-select)').each(function(){
		res+=compruebaValor($(this));
	});

	if(res==0){
		$(selectorFormulario).submit();
	}
	else{
		alert('Por favor, rellene todos los campos marcados en rojo.');
	}

	return res;//Se devuelve res para usarlo en otras funciones personalizadas de validación (como en acciones-formativas/gestion.php)
}

function compruebaValor(campo){
	var res=0;
	
	if(campo.parent().hasClass('radio') && compruebaValorRadio(campo.attr('name'))==1){
		res=1;
	}
	else if(campo.val()=='' || campo.val()=='NULL'){
		res=1;
	}

	formateaCampoObligatorio(campo,res);

	return res;
}

function compruebaValorRadio(name){
	var res=1;//Por defecto, erróneo

	$('input[name='+name+']').each(function(){
		if($(this).prop('checked')){
			res=0;//Si alguno de los radio está marcado, se desactiva la marca errónea para todos los que componen las opciones del campo
		}
	});

	return res;
}

function formateaCampoObligatorio(campo,res){
	if(campo.hasClass('selectpicker') && res==0){
		campo.selectpicker('setStyle', 'btn-danger','remove');
	}
	else if(campo.hasClass('selectpicker') && res==1){
		campo.selectpicker('setStyle', 'btn-danger');
	}
	else if(campo.parent().hasClass('radio') && res==0){
		campo.removeClass('erroneo');
	}
	else if(campo.parent().hasClass('radio') && res==1){
		campo.addClass('erroneo');
	}
	else if(res==0){
		campo.css('border','1px solid #cccccc');
	}
	else{
		campo.css('border','1px solid red');
	}
}


$('[name*=telefono],[name*=fax],[name*=movil]').each(function(){//Validador para teléfono/fax y móvil
	asignaValidacion($(this),/^[69]\d{8}$/,'Número de teléfono no válido');
});

$('[name*=email]').each(function(){//Validador para email
	asignaValidacion($(this),/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,'Dirección de email no válida');
});

$('[class*=cuentaSS]').each(function(){//Validador para cuenta de seguridad social
	asignaValidacion($(this),/^\d{9,11}$/,'Cuenta de Seguridad Social no válida');
});

/*$('[name*=cp]').each(function(){//Validador para código postal
	asignaValidacion($(this),/^(01|02|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|AD)\d{3}$/,'Código Postal no válido');
});*/

if($('#codigo').val()==undefined){
	$('.hasDatepicker').val('');//Le quito el valor a los campos fecha, que por defecto es la fecha actual
	$('input[name*=hora]').val('');//Lo mismo para las horas
}

function asignaValidacion(campo,expresion,mensaje){//Función general de validación. Recibe el campo, la expresión regular a utilizar y el mensaje en caso de que no se cumpla.
	campo.blur(function(){
		if(campo.val().trim()!=''){
			var validador=new RegExp(expresion);
					
			if(!validador.test(campo.val())){
				 alert(mensaje);
			}
		}
	});
}



//Funciones para detección de duplicidades
$('[name*=email],[name*=telefono],[name*=nif],[name*=cif],[name*=iban],[name*=movil],[name*=fax],[name*=cuentaSS],[name=usuario],[name=niss],[name=accionFormativa]').each(function(){
	if($(this).attr('tabla')!=undefined){//Para evitar la comprobación de campos que no corresponden (como formacionBonificada, que entraría por tener nif en el nombre)
		$(this).blur(function(){
			compruebaDuplicidadCampo($(this).attr('name'),$(this).val(),$(this).attr('tabla'),$(this).attr('campoCodigo'));
		});
	}
});

function compruebaDuplicidadCampo(nombreCampo,valor,tabla,campoCodigo){
	if(valor.trim()!=''){
		nombreCampo=nombreCampo.replace(/\d/g,'');//Para quitar el índice concatenado al nombre, en caso de que el campo esté en una tabla dinámica

		var valorCodigo='NO';
		if($('#codigo').val()!=undefined){//Si se está en detalles...
			valorCodigo=$('#codigo').val();
		}

		var consulta=$.post('../listadoAjax.php?funcion=compruebaDuplicidadCampo();',{
			'tabla':tabla,
			'campo':nombreCampo,
			'valor':valor,
			'campoCodigo':campoCodigo,
			'valorCodigo':valorCodigo
		});

		consulta.done(function(respuesta){
			if(respuesta=='error'){
				alert('Atención: el dato introducido ya existe en otro registro');
			}
		});
	}
}

//La siguiente función coloca un asterisco amarillo a los campos que el sistema a determinado como modificados por un comercial
function oyenteCambios(){
	var camposModificados=$('#camposModificados').val();
	if(camposModificados!=undefined && camposModificados!=''){
		camposModificados=camposModificados.split(';');

		for(i=0;i<camposModificados.length;i++){
			var id='#'+camposModificados[i];

			if($(id).val()!=undefined && !$(id).hasClass('hide') && camposModificados[i]!='firma' && camposModificados[i]!='codigo'){//El campo firma no es compatible con esta función y el campo código no se debe detectar

				if($(id).parent().prop('tagName')=='TD'){//Los campos en una tabla son un caso especial
					var indice=$(id).parent().index();
					var label=$(id).parents().find('th:eq('+indice+')');
				}
				else if($(id).parent().parent().prop('tagName')=='TD'){
					var indice=$(id).parent().parent().index();
					var label=$(id).parents().find('th:eq('+indice+')');
				}
				else if($(id).parent().hasClass('input-prepend') || $(id).parent().hasClass('radio')){
					var label=$(id).parent().parent().prev();//Doble salto para los campoTextoSimbolo y los radio
				}
				else{
					var label=$(id).parent().prev();	
				}
				
				var texto=label.html();
				label.html('<span class="asteriscoAmarillo">*</span> '+texto);
			}
		}
	}
}


//El siguiente código elimina la obligatoriedad de los campos radio (no quito el validador.js directamente para mantener otras funciones)
function quitaObligatoriedadRadios(selector){
	if(selector==undefined){
		$('input[type=radio]').each(function(){//Para quitar el asterisco a los checks
			eliminaRadioObligatorio($(this));
		});
	}
	else{
		$(selector).find('input[type=radio]').each(function(){//Para quitar el asterisco a los checks
			eliminaRadioObligatorio($(this));
		});
	}
}

//La siguiente función está vinculada a la anterior, que como puede ser llamada de 2 formas, extraigo el código de dentro del each para modularizar
function eliminaRadioObligatorio(radio){
	radio.removeClass('obligatorio');
			
	var label=radio.parent().parent().prev();//Doble salto para los campoTextoSimbolo y los radio
	var texto=label.text();

	texto=texto.replace('*','');//Para los radio, que ponen 1 asterisco por opción
	label.html(texto);
}

function validaCif(campo,doble=false){
	res = 0;
	abc = campo.val();
	par = 0;
	non = 0;
	letras = "ABCDEFGHJKLMNPQRSUVW";
	let = abc.charAt(0);
 	mensaje = '';
	if (abc.length!=9) {
		mensaje='El CIF debe tener 9 dígitos';
		res=1;
	}
 	
	if (letras.indexOf(let.toUpperCase())==-1) {
		mensaje="El comienzo del CIF no es válido";
		res=1;
	}

 	for (zz=2;zz<8;zz+=2) {
 		par = par+parseInt(abc.charAt(zz));
 	}
 	
 	for (zz=1;zz<9;zz+=2) {
 		nn = 2*parseInt(abc.charAt(zz));
 		if (nn > 9) nn = 1+(nn-10);
 		non = non+nn;
 	}
 	
 	parcial = par + non;
 	control = (10 - ( parcial % 10));
 	if (control==10) control=0;
 	
 	if (control!=abc.charAt(8)) {
 		mensaje="El CIF no es válido";
 		res=1;
 	}

 	
 	formateaCampoObligatorio(campo,res);
 	if(!doble){
 		if(mensaje!='' && mensajeMostrado == 0){
 			alert(mensaje);
 			mensajeMostrado = 1;
 		}
 	}
 	return res;
}

function validaNif(campo,doble=false) {
  var res = 0;
  var dni = campo.val();
  var mensaje = '';	
  var numero
  var let
  var letra
  var expresion_regular_dni
 
  expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
 
  if(expresion_regular_dni.test (dni) == true){
     numero = dni.substr(0,dni.length-1);
     let = dni.substr(dni.length-1,1);
     numero = numero % 23;
     letra='TRWAGMYFPDXBNJZSQVHLCKET';
     letra=letra.substring(numero,numero+1);
     if (letra!=let.toUpperCase()) {
       	mensaje='Dni erroneo, la letra del NIF no se corresponde';
     	res = 1;
     }
  }else{
     	mensaje='Dni erroneo, formato no válido';
     	res=1;
   }

   
   	formateaCampoObligatorio(campo,res);
   	if(!doble){
 		if(mensaje!='' && mensajeMostrado == 0){
 			alert(mensaje);
 			mensajeMostrado = 1;
 		}
 	}
 	return res;
}

function validarNifoCif(campo){
	var res = 0;
	res=validaNif(campo,true);
	if(res > 0){
		res=validaCif(campo,true);
	}
	if(res > 0){
		if(mensajeMostrado == 0){
 			alert('CIF/NIF Erroneo, compruebe letras y formatos');
 			mensajeMostrado = 1;
 		}
 	}
 	return res;

}
function insertaFilaEncargados(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla+'> tbody > tr:last').clone();

    //Si existe un campo de descarga, creo un input file junto a él
    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    //Obtengo el atributo name para los inputs y selects
    $tr.find("input:not([name='filasTabla[]'],[name='filasTablaUno[]'],.input-block-level,[type='checkbox']),select,textarea").attr("name", function(){
        return obtieneName(this.name);
        
    }).attr("id", function(){//Hago lo mismo con los IDs
        return this.name;
    });

    
    $tr.find("input:[type='checkbox']").attr("name", function(){
        var name=obtieneNameCheck(this.name);
        if(!isNaN(name.split('_')[1])){
            i=name.split('_')[1];
        }
        return name;
    }).attr("id", function(){//Hago lo mismo con los IDs
        return this.name;
    });

    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker,.btn-small),table").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("button.btn-small").attr("tabla", function(){//Para los botones de cálculo
        var parts = this.getAttribute('tabla').match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find(".botonSelectAjax").attr("id", function(){//Para los botones de los desplegables con búsqueda por AJAX (LAE y similares)
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find('table tbody tr:not(:first)').remove();

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
	$tr.find("input[type=checkbox]").removeAttr("checked");//Para quitar el check a la nueva fila clon de la anterior
    $tr.find('.divTratamientos').find('input[type=checkbox]').attr('checked','checked');
    $tr.find('.divTratamientos').find('#checkComunicacion1_'+i+'').attr('checked',false);
    $tr.find('.divTratamientos').find('#checkEstructuracion_'+i+'').attr('checked',false);
    $tr.find('.divTratamientos').find('#checkInterconexion_'+i+'').attr('checked',false);
    $tr.find('.divTratamientos').find('#checkAnalisis_'+i+'').attr('checked',false);
    $tr.find("select").val("NULL");
    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker
    $tr.find('.bootstrap-filestyle').remove();//Eliminación de los filestyle
    $tr.find('.descargaFichero').remove();//Eliminación del enlace de descarga, si lo hubiere

    $tr.find('.divEncargadoUno').addClass('hide');
    $tr.find(".divEncargadoUno").attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });
    $tr.find('.encargadoUno').change(function(){
        oyenteEncargadoUno($(this));
    });

    $tr.find('.divSubcontrata').addClass('hide');
    $tr.find(".divSubcontrata").attr("id", function(){
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });
    $tr.find('.radioSubcontratacion input[type=radio][value=NO]').attr('checked','checked');
    $tr.find('.radioSubcontratacion input[type=radio]').change(function(){
        oyenteSubContratacion($(this));
    });

    //Añado la nueva fila a la tabla
    $('#'+tabla+" > tbody > tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){//Misma operación para el input-rating
        var campoRating=$tr.find('.rating');//La librería bootstrap-rating-input mete el input dentro de un div, por lo que primero rescato el input...
        $tr.find('.rating-input').replaceWith(campoRating);//... y sustituyo el div por él (si no crearía 2 filas de estrellas)
        campoRating.rating();//Inicializo la librería sobre el input
    }

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
}

function eliminaFilaEncargados(tabla){
  if($('#'+tabla+" > tbody > tr").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' > tbody > tr').eq(fila).remove();
        filasSeleccionadas++;
    });
    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' > tbody > tr').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' > tbody > tr').eq(i).find("input:not([name='filasTabla[]'],[name='filasTablaUno[]'],.input-block-level),select,textarea").attr("name", function(){
                return obtieneName(this.name,i)
            }).attr("id", function(){
                return this.name;
            });
            $('#'+tabla+' tbody > tr').eq(i).find(".divEncargadoUno").each(function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                $(this).attr('id',parts[1] + i);
            });

            $('#'+tabla+' tbody > tr').eq(i).find(".divSubcontrata").each(function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                $(this).attr('id',parts[1] + i);
            });

            $('#'+tabla+' tbody > tr').eq(i).find("button:not(.selectpicker,.btn-small),table").attr("id", function(){//Para los botones de cálculo
                var parts = this.id.match(/(\D+)(\d*)$/);
                return parts[1] + i;
            });

            $('#'+tabla+' tbody > tr').eq(i).find("button.btn-small").attr("tabla", function(){//Para los botones de cálculo
                var parts = this.getAttribute('tabla').match(/(\D+)(\d*)$/);
                return parts[1] + i;
            });

            $('#'+tabla+' tbody > tr').eq(i).find("input[name='filasTabla[]']").attr("value", function(){
                //var j=i+1
                var j=i;
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}

function obtieneName(name,elimina='false'){
    var res = name.slice(-2);
    if(res=='[]'){
        res=name.slice(0,-2);
        var parts = res.match(/(\D+)(\d*)$/);
        if(elimina=='false'){
            res=parts[1] + ++parts[2]+'[]';
        } else {
            res=parts[1] + elimina+'[]';
        }
    } else {
        if(name.indexOf('_')==-1){
            var parts = name.match(/(\D+)(\d*)$/);
            if(elimina=='false'){
                res=parts[1] + ++parts[2];
            } else {
                res=parts[1] + i;
            }
        } else {
            var parts = name.split('_');
            var parts2 = parts[0].match(/(\D+)(\d*)$/);
            if(elimina=='false'){
                res=parts2[1] + ++parts2[2]+'_'+parts[1];
            } else {
                res=parts2[1] +i+'_'+parts[1];
            }
        }
    }
    return res;
}

function obtieneNameCheck(name){
    name=name.split('_');
    return name[0]+'_'+ ++name[1];
}

function insertaFilaSubcontrata(elem){
    tabla=elem.attr('tabla');
    var $tr = $('#'+tabla).find("tbody tr:last").clone();

    
    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    
    $tr.find("input:not([name='filasTablaUno[]'],.input-block-level),select,textarea").attr("name", function(){
        if(this.name.indexOf('_')==-1){
            var parts = this.name.match(/(\D+)(\d*)$/);
            res=parts[1] + ++parts[2];
        } else {
            var parts = this.name.split('_');
            res=parts[0] +'_'+ ++parts[1];
        }
        return res;
        
    }).attr("id", function(){
        return this.name;
    });


    $tr.find("input[name='filasTablaUno[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find(".botonSelectAjax").attr("id", function(){//Para los botones de los desplegables con búsqueda por AJAX (LAE y similares)
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });


    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
    $tr.find("input[type=checkbox]").removeAttr("checked");//Para quitar el check a la nueva fila clon de la anterior
    $tr.find("select").val("NULL");
    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker
    $tr.find('.bootstrap-filestyle').remove();//Eliminación de los filestyle
    $tr.find('.descargaFichero').remove();//Eliminación del enlace de descarga, si lo hubiere

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){//Misma operación para el input-rating
        var campoRating=$tr.find('.rating');//La librería bootstrap-rating-input mete el input dentro de un div, por lo que primero rescato el input...
        $tr.find('.rating-input').replaceWith(campoRating);//... y sustituyo el div por él (si no crearía 2 filas de estrellas)
        campoRating.rating();//Inicializo la librería sobre el input
    }

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
}

function eliminaFilaSubcontrata(elem){
  tabla=elem.attr('tabla');
  if($('#'+tabla).find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTablaUno[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' tr').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' tr:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' tr:not(:first)').eq(i).find("input:not([name='filasTablaUno[]'],.input-block-level),select,textarea").attr("name", function(){
                if(this.name.indexOf('_')==-1){
                    var parts = this.name.match(/(\D+)(\d*)$/);
                    res=parts[1] + i;
                } else {
                    var parts = this.name.split('_');
                    res=parts[0] +'_'+ i;
                }
                return res;
            }).attr("id", function(){
                return this.name;
            });

            $('#'+tabla+' tr:not(:first)').eq(i).find("input[name='filasTablaUno[]']").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}
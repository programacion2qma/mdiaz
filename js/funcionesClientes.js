//Oyentes y funciones compartidas entre Clientes y Posibles Clientes

$(document).ready(function(){
	//Parte de validación del formulario de posibles clientes/clientes (además de la validación normal comprueba la duplicidad del CIF)
	$(':submit').unbind();
	$(':submit').click(function(e){
		e.preventDefault();
		
		validaDatosCliente();
	});
	//Fin parte de validación del formulario de posibles clientes/clientes 


	inicializaDatepicker();
	$('select').selectpicker();

	if($('#posibleCliente').val()=='NO'){//Solo se detectan las duplicidades para los clientes confirmados
		oyenteCambios();
	}

	oyenteMedioPago();
	$('#medioPago').change(function(){
		oyenteMedioPago();
	});

	oyenteNumeroCuenta();
	oyenteNuevaCreacion();
	$('input[name=nuevaCreacion]').change(function(){
		oyenteNuevaCreacion();
	});

    
    oyenteRLT();//Para mostrar/ocultar la caja de firma del RLT
    $('input[name=tieneRLT]').change(function(){
    	oyenteRLT();
    });
    //Fin parte de firmas

	//creaBotonesTrabajadores();

	//Oyentes de ventanas de creación
	$('#crea-codigoAsesoria').click(function(){
		$('#cajaAsesoria').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraAsesoria').click(function(){
		creaAsesoria();
	});

	$('#boton-codigoComercial').click(function(){
		$('#cajaComercial').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraComercial').click(function(){
		creaComercial();
	});

	$('#boton-codigoColaborador').click(function(){
		$('#cajaColaborador').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraColaborador').click(function(){
		creaColaborador();
	});

	$('#boton-codigoTelemarketing').click(function(){
		$('#cajaTelemarketing').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraTelemarketing').click(function(){
		creaTelemarketing();
	});

	$('#boton-codigoConsultor').click(function(){
		$('#cajaConsultor').modal({'show':true,'backdrop':'static','keyboard':false});
	});
	$('#registraConsultor').click(function(){
		creaConsultor();
	});

	//Parte de procedencia de listado
	oyenteProcedencia();
	$('input[name=procedeDeListado]').change(function(){
		oyenteProcedencia();
	});

	oyenteTextoProcedencia();
	$('#procedenciaListado').change(function(){
		oyenteTextoProcedencia();
	})
	//Fin parte de procedencia de listado

	//Oyente para actualizar los contactos de la asesoría
	$('#codigoAsesoria').change(function(){
		oyenteAsesoria($(this).val());
	});
	//Fin oyente para actualizar los contactos de la asesoría

	//Oyente cálculo créditos
	oyenteCalculoPrecio('#tablaCreditos');
	//Fin oyente cálculo créditos
});

function oyenteMedioPago(){
	if($('#medioPago').val()=='RECIBO SEPA'){
		$('#cajaMedioPago').removeClass('hide');
	}
	else{
		$('#cajaMedioPago').addClass('hide');	
	}
}

function oyenteNumeroCuenta(){
	$('#tablaCuentas').find('.numeroCuenta').each(function(){
		calculaIbanCuenta($(this));
	});
}

function calculaIbanCuenta(numeroCuenta){
	numeroCuenta.blur(function(){
		var fila=obtieneFilaCampo(numeroCuenta);
	
		if(isAccountComplete(numeroCuenta.val())){
			$("#iban"+fila).val(CalcularIBAN(numeroCuenta.val(),'ES'));
			var bic=obtieneBic(numeroCuenta.val());
			$("#bic"+fila).val(bic);
		}
		else{
			alert('Por favor, introduzca un número de cuenta correcto.');
		}
	});
}

function insertaFilaCuenta(){
	insertaFila("tablaCuentas");
	calculaIbanCuenta($('#tablaCuentas tr:last .numeroCuenta'));
}

function oyenteNuevaCreacion(){
	if($('input[name=nuevaCreacion]:checked').val()=='NO'){
		$('#cajaNueva').addClass('hide');
	}
	else{
		$('#cajaNueva').removeClass('hide');
	}
}


function creaAsesoria(){
	var asesoria=$('#cajaAsesoria #asesoria').val();//Utilizo el id de la ventana porque los campos se repiten entre ventanas
	var telefonoPrincipal=$('#cajaAsesoria #telefonoPrincipal').val();
	var emailPrincipal=$('#cajaAsesoria #emailPrincipal').val();
	var observaciones=$('#cajaAsesoria #observaciones').val();

	var campos={
		'asesoria':asesoria,
		'telefonoPrincipal':telefonoPrincipal,
		'emailPrincipal':emailPrincipal,
		'observaciones':observaciones
	}

	creaRegistro('#codigoAsesoria','#registraAsesoria','#cajaAsesoria',campos,asesoria,'creaAsesoriaCliente();');
}

function creaComercial(){
	var nombre=$('#cajaComercial #nombre').val();
	var dni=$('#cajaComercial #dni').val();
	var provincia=$('#cajaComercial #provincia').val();
	var codigoCategoria=$('#cajaComercial #codigoCategoria').val();
	var codigoComercial=$('#cajaComercial #codigoComercial').val();
	var codigoUsuario=$('#cajaComercial #codigoUsuario').val();
	var telefono=$('#cajaComercial #telefono').val();
	var movil=$('#cajaComercial #movil').val();
	var email=$('#cajaComercial #email').val();

	var campos={
		'nombre':nombre,
		'dni':dni,
		'provincia':provincia,
		'codigoCategoria':codigoCategoria,
		'codigoComercial':codigoComercial,
		'codigoUsuario':codigoUsuario,
		'telefono':telefono,
		'movil':movil,
		'email':email
	};

	creaRegistro('#codigoComercial','#registraComercial','#cajaComercial',campos,nombre,'creaComercialCliente();');
}

function creaColaborador(){
	var nombre=$('#cajaColaborador #nombre').val();
	var cif=$('#cajaColaborador #cif').val();
	var provincia=$('#cajaColaborador #provincia').val();
	var codigoUsuario=$('#cajaColaborador #codigoUsuario').val();
	var telefono=$('#cajaColaborador #telefono').val();
	var email=$('#cajaColaborador #email').val();

	var campos={
		'nombre':nombre,
		'cif':cif,
		'provincia':provincia,
		'codigoUsuario':codigoUsuario,
		'telefono':telefono,
		'email':email
	}

	creaRegistro('#codigoColaborador','#registraColaborador','#cajaColaborador',campos,nombre,'creaColaboradorCliente();');
}

function creaTelemarketing(){
	var nombre=$('#cajaTelemarketing #nombre').val();
	var dni=$('#cajaTelemarketing #dni').val();
	var provincia=$('#cajaTelemarketing #provincia').val();
	var telefono=$('#cajaTelemarketing #telefono').val();
	var email=$('#cajaTelemarketing #email').val();

	var campos={
		'nombre':nombre,
		'dni':dni,
		'provincia':provincia,
		'telefono':telefono,
		'email':email
	}

	creaRegistro('#codigoTelemarketing','#registraTelemarketing','#cajaTelemarketing',campos,nombre,'creaTelemarketingCliente();');
}

function creaConsultor(){
	var nombre=$('#cajaConsultor #nombre').val();//Utilizo el id de la ventana porque los campos se repiten entre ventanas
	var apellidos=$('#cajaConsultor #apellidos').val();
	var usuario=$('#cajaConsultor #usuario').val();
	var clave=$('#cajaConsultor #clave').val();
	var email=$('#cajaConsultor #email').val();

	var campos={
		'nombre':nombre,
		'apellidos':apellidos,
		'usuario':usuario,
		'clave':clave,
		'email':email
	}

	creaRegistro('#codigoConsultor','#registraConsultor','#cajaConsultor',campos,nombre+' '+apellidos,'creaConsultorCliente();');
}

function creaRegistro(idCampo,idBoton,idVentana,campos,campoNombre,funcion){
	$(idBoton).html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var consulta=$.post('../listadoAjax.php?include=clientes&funcion='+funcion,campos);

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar el consultor.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			actualizaSelectsTrasCreacion(idCampo,respuesta,campoNombre);
		}

		$(idVentana).modal('hide');
		$(idBoton).html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original
	});
}

function actualizaSelectsTrasCreacion(idCampo,codigo,nombre){
	$(idCampo).find('option[value=NULL]').after('<option value="'+codigo+'">'+nombre+'</option>');
	$(idCampo).selectpicker('refresh');
}

function creaBotonesTrabajadores(){
	if($('#codigo').val()!=undefined && $('#posibleCliente').val()=='NO'){
		var codigo=$('#codigo').val();
		var botones="<div class='pull-right'>";
		botones+="<a href='../trabajadores/index.php?codigoEmpresa="+codigo+"' class='btn btn-small btn-primary'><i class='icon-industry'></i> Ver trabajadores</a>";
		botones+=" <a href='../trabajadores/gestion.php?codigoEmpresa="+codigo+"' class='btn btn-small btn-primary'><i class='icon-user-plus'></i> Crear trabajador</a>";
		botones+="</div>";

		$('.widget-header h3').after(botones);
	}
}


function insertaFilaCreditos(){
	if($('#tablaCreditos tbody tr').hasClass('hide')){
		$('#tablaCreditos tbody tr').removeClass('hide');
		$('#ejercicio0').val('');//Hago esto porque si se guarda el campo vacío, le pone ceros por defecto
		$('#plantilla0').val('');
		$('#horasPIF0').val('');
	}
	else{
		insertaFila("tablaCreditos");
		oyenteCalculoPrecio('#tablaCreditos tr:last');
		inicializaDatepicker();
	}
}

function oyenteProcedencia(){
	if($('input[name=procedeDeListado]:checked').val()=='SI'){
		$('#cajaProcedencia').removeClass('hide');
	}
	else{
		$('#cajaProcedencia').addClass('hide');
	}
}

function oyenteTextoProcedencia(){
	if($('#procedenciaListado').val()=='CARTERA'){
		$('#cajaTextoProcedencia').removeClass('hide');
		$('#textoProcedencia').parent().prev().text('Nombre de comercial:');
	}
	else if($('#procedenciaListado').val()=='OTROS'){
		$('#cajaTextoProcedencia').removeClass('hide');
		$('#textoProcedencia').parent().prev().text('Otros:');
	}
	else{
		$('#cajaTextoProcedencia').addClass('hide');
	}
}

function oyenteAsesoria(codigoAsesoria){
	var consulta=$.post('../listadoAjax.php?funcion=obtieneContactosAsesoria();',{'codigoAsesoria':codigoAsesoria});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al obtener los contactos de la asesoría.\nInténtelo de nuevo en unos segundos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			$('#codigoContactoAsesoria').html(respuesta);
			$('#codigoContactoAsesoria').selectpicker('refresh');
		}
	});
}


function oyenteCalculoPrecio(selector){
	$(selector).find('.calculo').change(function(){
		var fila=obtieneFilaCampo($(this));
		var credito=formateaNumeroCalculo($('#credito'+fila).val());
		var creditoOtras=formateaNumeroCalculo($('#creditoOtras'+fila).val());
		var formacionTramitada=formateaNumeroCalculo($('#formacionTramitada'+fila).val());
		var formacionBonificada=formateaNumeroCalculo($('#formacionBonificada'+fila).val());
		
		
		credito=credito-creditoOtras-formacionTramitada-formacionBonificada;

		$('#creditoDisponible'+fila).val(formateaNumeroWeb(credito));
	});
}

function inicializaDatepicker(){
	$('#fechaFirmaDocumentos').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	
	var fechaActual=new Date();
 
	$('.campoFecha').datepicker({format:'dd/mm/yyyy',weekStart:1,
  		onRender: function(date) {
    		return date.valueOf()>fechaActual.valueOf() ? 'disabled' : '';
  		}
	}).on('changeDate',function(e){$(this).datepicker('hide');});
}


function oyenteRLT(){
	if($('input[name=tieneRLT]:checked').val()=='SI'){
		$('#contenedor-firmaRLT').removeClass('hide');
		$('#cajaRLT').removeClass('hide');
	}
	else{
		$('#contenedor-firmaRLT').addClass('hide');
		$('#cajaRLT').addClass('hide');
	}
}



function validaDatosCliente(){
	var cif=$('#cif').val();

	if(cif!=''){
		var valorCodigo='NO';
		if($('#codigo').val()!=undefined){//Si se está en detalles...
			valorCodigo=$('#codigo').val();
		}

		var consulta=$.post('../listadoAjax.php?funcion=compruebaDuplicidadCampo();',{
			'tabla':'clientes',
			'campo':'cif',
			'valor':cif,
			'campoCodigo':'codigo',
			'valorCodigo':valorCodigo
		});

		consulta.done(function(respuesta){
			if(respuesta=='error'){
				alert('Atención: no se puede registrar el cliente porque el CIF introducido ya existe en el sistema.');
			}
			else{
				validaCamposObligatorios();		
			}
		});
	}
	else{
		validaCamposObligatorios();
	}
}
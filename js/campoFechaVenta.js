$(document).ready(function(){

	$('.fechaVenta,.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1,onRender:function(date){
		var fechaActual=new Date();
		fechaActual.setDate(fechaActual.getDate()-1);

		return date.valueOf() < fechaActual.valueOf() ? 'disabled' : '';
	}}).on('changeDate',function(e){$(this).datepicker('hide');});


	//Oyente fecha vencimiento
	oyenteVencimiento();
	$('#tipoVencimiento').change(function(){
		oyenteVencimiento();
	});
	//Fin oyente fecha vencimiento
});

function oyenteVencimiento(){
	if($('#tipoVencimiento').val()=='EN LA FECHA'){
		$('#cajaFechaFacturacion').removeClass('hide');
	}
	else{
		$('#cajaFechaFacturacion').addClass('hide');
	}
}
function listadoTabla(tabla,rutaListado){
  
  //Estas variables sirven para definir un scroll interno dentro del datatable
  var scroll=false;
  var scrollY='';
  //Y esta para definir un orden por defecto en el listado de facturas
  var orden=[0,"asc"];

  if(tabla=='#tablaListadoGruposXMLInicio'){
    var scroll=true;
    var scrollY='320';
  }
  else if(tabla=='#tablaFacturas' || tabla=='#tablaGrupos' || tabla=='#tablaTutorias' || tabla=='#tablaVentasServicios' ||
          tabla=='#tablaVentas' || tabla=='#tablaPagosComisiones' || tabla=='#tablaPedidos' || tabla=='#tablaEnvios' || tabla=='#tablaEnvioClaves' ||
          tabla=='#tablaListadoVencimientos' || tabla=='#tablaRecibos' || tabla=='#tablaRemesas' || tabla=='#tablaTareas'){
    orden=[0,"desc"];
  }
  else if(tabla=='#tablaXmlAccionesFormativas' || tabla=='#tablaXmlInicio' || tabla=='#tablaXmlFin' || tabla=='#tablaAccesos'){
   orden=[[1,"desc"],[2,"desc"]]; 
  }
  else if(tabla=='#tablaInformesClientes'){
   orden=[1,"asc"]; 
  }
  else if(tabla=='#tablaInformesClientesExamen'){
   orden=[3,"asc"]; 
  }


  $(tabla).dataTable({
    'bProcessing': true, 
    'bServerSide': true,
    "stateSave":true,
    stateSaveCallback:function(settings, data){//MODIFICACIÓN 22/09/2015: uso del callback personalizado para que funcione el stateSave
        var idTabla=window.location.pathname.replace(/[- ._index.php/]*/g,'');//Uso como ID de la tabla la ruta de la página donde se encuentra (quitándole el index.php en caso de que existiera, para que no haya diferencias entre el menú y los botones de Volver)
        if(compruebaAlmacenamientoHTML5()) {
            window.localStorage.setItem(idTabla,JSON.stringify(data));//Guarda el estado de la tabla en el almacenamiento local
        }
    },
    stateLoadCallback: function(settings){//MODIFICACIÓN 22/09/2015: uso del callback personalizado para que funcione el stateSave
        var idTabla=window.location.pathname.replace(/[- ._index.php/]*/g,'');
        var res=false
        if(compruebaAlmacenamientoHTML5()) {
            res=JSON.parse(window.localStorage.getItem(idTabla));
        }
        return res;
    },
    "aLengthMenu":[ 25, 50, 100, -1 ],
    'sAjaxSource': rutaListado,
    "iDisplayLength":25,
    "bScrollCollapse": scroll,
    "sScrollY": scrollY,
    "aoColumnDefs":[{'bSortable':false, 'aTargets':[-1]}],
    "order":orden,
    "oLanguage": {
      "sLengthMenu": "_MENU_ registros por página",
      "sSearch":"Búsqueda global:",
      "oPaginate":{"sPrevious":"Atrás","sNext":"Siguiente"},
      "sInfo":"Mostrando _START_ de _END_ registros de un total de _TOTAL_",
      "sEmptyTable":"Aún no hay datos que mostrar",
      "sInfoEmpty":"",
      'sInfoFiltered':"",
      'sZeroRecords':'No se han encontrado coincidencias',
      'sProcessing':'<i class="icon-spinner icon-spin"></i> Procesando...'
    },
    "fnDrawCallback": function (oSettings) {
      $(tabla).find('.bloqueado').parent().parent().parent().addClass('filaBloqueada');
      $(tabla).find('.pendiente').parent().parent().parent().addClass('filaPendiente');
      
        $('.enlacePopOver').each(function(){
          $(this).popover({//Crea el popover
            title: 'Descripción de incidencia',
            content: $(this).next().html().replace(/[\r\n|\n]/g,'<br />'),
            placement:'right',
            thisrigger:'manual'
          });

          $('.enlacePopOver').click(function(e){
            e.preventDefault();
            $('.enlacePopOver').not($(this)).popover('hide');
            $(this).popover('show');
           
          });
        });
      
      creaSumatoriosPie(tabla);
      compruebaChecksTipoIconoTabla(tabla);
      compruebaOyentesTabla(tabla);
    }
  });
}

$('.dataTables_wrapper').find('.dataTables_paginate a').each(function(){
  $(this).addClass('noAjax');
});


//Función que detecta las columnas con la clase sumatorio, y les crea abajo una celda con el importe total de los valores en esa columna
function creaSumatoriosPie(tabla){
  //Obtención de sumatorios de columnas
  if($(tabla).find('.sumatorio').length>0){//Si hay columnas de sumatorio...
    var celdasSumatorio=new Array();//Para almacenar los índices de las columnas con sumatorio
    var totalColumna=new Array();//Para almacenar los totales de las columnas con sumatorio

    $(tabla).find('.sumatorio').each(function(){
      var indice=$(this).index();
      celdasSumatorio.push(indice);//Guardo el número de columna (empezando en 0) de cada celda con la clase sumatorio

      $(tabla).find('td:nth-child('+(indice+1)+')').each(function(){//Recorro todas las celdas en la columna especificada y almaceno sus importes. Le sumo 1 porque nth-child empieza en 1
          if($(this).parent().find('.bloqueado').html()==undefined){//Compruebo que la fila actual no tenga la clase bloqueado, en cuyo caso sus importes no deben entrar en el sumatorio
            var importe=$(this).text().replace('.','');
            importe=importe.replace(',','.');
            importe=importe.replace(' €','');
            if(importe==''){
               importe=0;
            }

            if(totalColumna[indice]==undefined){
              totalColumna[indice]=parseFloat(importe);
            }
            else{
              totalColumna[indice]+=parseFloat(importe); 
            }
          }
      });
    });



    //Creción de la nueva fila
    var nuevaFila='<tr class="filaPie">';
    
    for(i=0;i<$(tabla).find('tr:last td').length;i++){
        nuevaFila+='<td';

        if($.inArray(i,celdasSumatorio)>-1){//Si la celda actual pertenece a la columna de la clase sumatorio...
          nuevaFila+=' class="celdaSumatorio">';//..le añado la clase y el valor correspondiente
          
          var importe=totalColumna[i];
          if(importe!=undefined){//Puede darse el caso de que todas las columnas filtradas tengan la clase bloqueado, en cuyo caso el total de la columna no estará definido. Por eso este if
            importe=importe.toFixed(2).toString();
            importe=importe.replace('.',',');
            
            nuevaFila+=importe;
            nuevaFila+=' €';
          }
          else{
            nuevaFila+='0,00 €';
          }
        }
        else{
          nuevaFila+='>';
        }

        nuevaFila+='</td>';
    }

    nuevaFila+='</tr>';
    
    $(tabla+' tr:last').after(nuevaFila);
    //Fin creación nueva fila

  }
  //Fin obtención de sumatorios de columnas
}

function compruebaChecksTipoIconoTabla(tabla){
  if($(tabla).hasClass('tablaConCheckTipoIcono')){
    $(tabla).find(':checkbox').click(function(){
      oyenteChecksTipoIconoTabla($(this));//En /vencimientos/index.php y /exportar-xml-acciones/index.php (se define por separado porque hacen cosas distintas)
    });
  }
}

//Parte de consultoría
function compruebaOyentesTabla(tabla){
  if(tabla=='#tablaConsultorias'){
    oyenteTablaConsultorias();
  }
  else if(tabla=='#tablaClientes'){
    oyenteDescargaDocumentosCliente();
    oyenteTablaClientes();
  }
  else if(tabla=='#tablaListadoGruposXMLInicio'){
    oyenteChecksGruposXMLInicio();
  }
}

function oyenteTablaClientes(){
    $('.espacioTrabajo').unbind();
    $('.espacioTrabajo').click(function(e){
        e.preventDefault();
        var cliente=$(this).attr('codigoCliente');//Atributo personalizado
        $.post('../listadoAjax.php?funcion=obtenerUsuarioCliente('+cliente+');',function(respuesta){
          var camposFormulario=[];
          camposFormulario['usuario']=respuesta.usuario;
          camposFormulario['clave']=respuesta.clave;
          creaFormulario('../../mdiaz2/index.php',camposFormulario,'post','_blank');
        },'JSON');
    });
}


function oyenteTablaConsultorias(){
    colocaIndicadores();

    $('.mostrarServicios').click(function(){
        var id=$(this).attr('tabla');//Atributo personalizado
        var estado=$(this).attr('estado');//Atributo personalizado
        var textoBoton=$(this).html();

        if(estado=='oculto'){
            $(id).slideDown();
            textoBoton=textoBoton.replace('down','up');
            textoBoton=textoBoton.replace('Mostrar','Ocultar');
            $(this).html(textoBoton);
            $(this).attr('estado','mostrado');

            //Estilos
            $(this).css('-webkit-border-radius','0');
            $(this).css('-moz-border-radius','0');
            $(this).css('border-radius','0');

            $(this).parent().css('padding','0');//Celda contenedora
        }
        else{
            $(id).slideUp();
            textoBoton=textoBoton.replace('up','down');
            textoBoton=textoBoton.replace('Ocultar','Mostrar');
            $(this).html(textoBoton);
            $(this).attr('estado','oculto');

            //Estilos
            $(this).css('-webkit-border-radius','4px');
            $(this).css('-moz-border-radius','4px');
            $(this).css('border-radius','4px');

            $(this).parent().css('padding','8px');//Celda contenedora
        }
    });
}

function colocaIndicadores(){
   $('.badge-danger').each(function(){
      var alertas=$(this).parent().parent().find('.label-danger').length;//Cuento el número de fechas pasadas
      if(alertas==0){//Si son 0, oculto el indicador
        $(this).addClass('hide');
      }
      else{
        $(this).text(alertas);//Si hay al menos 1 fecha pasada, muestro el indicador
      }
    });
}

/*
Esta función desactiva los enlaces de descarga de documentos en clientes y posibles clientes, haciendo uso como selector de la clase .generacionDocumento.
En lugar de llevar a la descarga directamente, abre un modal de selección de emisor y ejercicio, para a continuación detectar con otro oyente la pulsación en el botón
"Descargar", momento en el cual se construye la URL completa del enlace de descarga y se procede a la generación del documento correspondiente.
*/
function oyenteDescargaDocumentosCliente(){
  /*$('.generacionDocumento').click(function(e){
      e.preventDefault();
      var url=$(this).attr('href');
      var codigo=url.replace(/\D/g,'');
      var textoDecargar="<i class='icon-file-text'></i><i class='icon-chevron-right'></i><i class='icon-cloud-download'></i> &nbsp; ";
      textoDecargar+=$(this).text();

      $('#cajaDocumentos').find('#url').val(url);//Guarda la URL a modificar y que se usará de destino para la descarga del documento
      $('#cajaDocumentos').find('#ejercicioDocumento').val($('#cajaDocumentos').find('#anioActual').val());//Pone como año por defecto el actual

      $('#cajaDocumentos').find('h3').html(textoDecargar);//Cambia el título del modal, para identificar qué documento se va ha descargar
      
      var consulta=$.post('../listadoAjax.php?funcion=obtieneEmisoresCliente();',{'codigoCliente':codigo,'url':url});//Le envío también la URL para que filtrar el emisor B75010686 (que solo puede emitir SEPA)
      consulta.done(function(respuesta){
        $('#cajaDocumentos').find('select').html(respuesta).selectpicker('refresh');
        $('#cajaDocumentos').modal({'show':true,'backdrop':'static','keyboard':false});
      });
  });

  $('#descargaDocumento').click(function(){
    $('#cajaDocumentos').modal('hide');

    var url=$('#cajaDocumentos').find('#url').val();
    var codigoEmisor=$('#cajaDocumentos').find('#codigoEmisor').val();
    var ejercicio=$('#cajaDocumentos').find('#ejercicioDocumento').val();

    url+='&codigoEmisor='+codigoEmisor+'&ejercicio='+ejercicio;
    window.location=url;
  });*/

  $('.importarDatos').click(function(e){
    e.preventDefault();
      var codigo=$(this).attr('codigoCliente');
      $('#codigoClienteImportar').val(codigo);
      var parametros = {
        "codigo"  : codigo
      };
      $.ajax({
       type: "POST",
       url: "../listadoAjax.php?include=clientes&funcion=recogeExcel();",
       data: parametros,
        beforeSend: function () {
          
        },
        success: function(response){
           $("#ulFicheros").html(response);
           $('.eliminaExcel').click(function(){
              codigo=$(this).attr('codigo');
              $('#ficheroExcel'+codigo).remove();
              var consulta=$.post('../listadoAjax.php?include=informes-clientes&funcion=eliminaExcel();',{'codigo':codigo});
           });
       }
      });
      $('#cajaFichero').modal({'show':true,'backdrop':'static','keyboard':false});
  });
}

//Fin parte de consultoría


//Parte de XML de Inicio

function oyenteChecksGruposXMLInicio(){
    $('#tablaListadoGruposXMLInicio').find('input[name="codigoLista[]"]').change(function(){
        validaGrupo($(this));//Definido en /exportar-xml-inicio/gestion.php
    });
}

function compruebaAlmacenamientoHTML5(){//MODIFICACIÓN 22/09/2015: nueva función
    try{
        return 'localStorage' in window && window['localStorage'] !== null;
    }
    catch (e){
        return false;
    }
}

//Fin parte de XML de Inicio
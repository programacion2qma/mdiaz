/**************************************************************/
/******** AVISO DE COOKIES para cualquier página web  *********/
/**************************************************************/
/*                                                            */
/*   El objetivo de este snippet es adaptar cualquier página  */
/*   a la ley de protección europea que obliga a mostrar un   */
/*   aviso a aquellas páginas que usan cookies (vamos, que a  */
/*   todas). Su uso es sencillo: incluye el JavaScript en un  */
/*   documento HTML y a correr. Ni jQuery ni historias.       */
/*                                                            */
/**************************************************************/

var cookie_name = 'cookies_mdiaz';
// Esperamos a que la página web cargue (no queremos incordiar)
if (typeof window.onload != 'function') {
	window.onload = showCookiesMsg;
} else {
	var onLoad = window.onload;
	window.onload = function() {
	if (onLoad) oldonload();
		showCookiesMsg();
	};
}

// Definimos el código que muestra el mensaje de aviso
function showCookiesMsg() {
	//alert(document.cookie.indexOf(cookie_name + '=1'));
	//document.cookie = cookie_name + '=1;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';
	if (document.cookie.indexOf(cookie_name + '=1') < 0) {
		document.body.innerHTML+='<div class="aviso-cookies">Este sitio web utiliza cookies propias'+
		'que permiten el funcionamiento y la '+
		'prestación de los servicios ofrecidos en el mismo. Puede aceptar su uso o rechazarlo mediante '+
		'la configuración de su navegador o consultar sus opciones cliqueando en VER MÁS. Si tienes menos '+
		'de 14 años, pide a tu padre, madre o tutor que lea este mensaje.<br />'+
		'<a class="btn btn-default" target="_blank" href="textos_legales/index.php?tipo=cookies">'+
		'<i class="fa fa-info-circle"></i> VER MÁS</a> '+
		'<a class="btn btn-success" href="javascript:void(0);" onclick="aceptaCookies();">'+
		'<i class="fa fa-check-circle"></i> ACEPTO</a></div>';
	}

}

function aceptaCookies(){
	// Guardamos (o actualizamos) una cookie para recordar que ya se ha mostrado el mensaje
	var d = new Date();
	d.setTime(d.getTime() + (30*24*60*60*1000));
	document.cookie = cookie_name + '=1;expires=' + d.toUTCString() + ';path=/';
	//alert(document.cookie.indexOf(cookie_name + '=1'));
	$('.aviso-cookies').remove();
}
function insertaFilaPlatos(tabla,fila=false){
    //Clono la última fila de la tabla
    if(fila!=false){
        var $tr = $('#'+tabla+" > tbody > tr#"+fila).clone();
    } else {
        var $tr = $('#'+tabla+" > tbody > tr:last").clone();
    }

    var indice = $('#'+tabla).find("tbody > .trPlatos").length;

    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        if(this.id.indexOf('-')!=-1){
            return obtenerNameAlergeno(this.id,true,indice);
        } else {
            var parts = this.id.match(/(\D+)(\d*)$/);
            return parts[1] + indice;
        }
        
    }).attr("id", function(){//Hago lo mismo con los IDs
        if(this.id.indexOf('-')!=-1){
            return obtenerIdAlergeno(this.id,true,indice);
        } else {
            var parts = this.id.match(/(\D+)(\d*)$/);
            return parts[1] + indice;
        }
    });

    $tr.attr("id", function(){//Para el identificador de la subtabla padre
        var id=this.id;
        var indice=id.replace(/\D/g,'');
        
        id=id.slice(0,4);
        indice++;

        return id+indice;
    });

    $tr.find(".table").attr("id", function(){//Para el identificador de la subtabla padre
        var id=this.id;
        var indice=id.replace(/\D/g,'');
        
        id=id.slice(0,13);
        indice++;

        return id+indice;
    });

    $tr.find(".btn-success").attr("onclick", function(){//Para los botones de añadir alumno
        var evento=$(this).attr('onclick');
        var indice=evento.replace(/\D/g,'');
        
        evento=evento.slice(0,30);
        indice++;

        return evento+indice+'")';
    });

    $tr.find(".btn-info").attr("onclick", function(){//Para los botones de añadir alumno
        var evento=$(this).attr('onclick');
        var indice=evento.replace(/\D/g,'');
        
        evento=evento.slice(0,37);
        indice++;

        return evento+indice+'")';
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });

    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    if(fila==false){
        $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
    }

    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker
    $tr.find('.bootstrap-filestyle').remove();//Eliminación de los filestyle
    $tr.find('.descargaFichero').remove();//Eliminación del enlace de descarga, si lo hubiere

    //Añado la nueva fila a la tabla
    $('#'+tabla+" > tbody > tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){//Misma operación para el input-rating
        var campoRating=$tr.find('.rating');//La librería bootstrap-rating-input mete el input dentro de un div, por lo que primero rescato el input...
        $tr.find('.rating-input').replaceWith(campoRating);//... y sustituyo el div por él (si no crearía 2 filas de estrellas)
        campoRating.rating();//Inicializo la librería sobre el input
    }

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $(".grupoAlergenos").change(function(){
        obtieneAlergenos($(this));
    });
}

function insertaAlergeno(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody tr:last").clone();
    
    var indice = $('#'+tabla).find("tbody > .trPlatos").length;
    indice = indice + 1;

    //Si existe un campo de descarga, creo un input file junto a él
    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    //Obtengo el atributo name para los inputs y selects
    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
        return obtenerNameAlergeno(this.id,false,indice);
        
    }).attr("id", function(){//Hago lo mismo con los IDs

        return obtenerIdAlergeno(this.id,false,indice);
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker
    $tr.find('.bootstrap-filestyle').remove();//Eliminación de los filestyle
    $tr.find('.descargaFichero').remove();//Eliminación del enlace de descarga, si lo hubiere

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){//Misma operación para el input-rating
        var campoRating=$tr.find('.rating');//La librería bootstrap-rating-input mete el input dentro de un div, por lo que primero rescato el input...
        $tr.find('.rating-input').replaceWith(campoRating);//... y sustituyo el div por él (si no crearía 2 filas de estrellas)
        campoRating.rating();//Inicializo la librería sobre el input
    }

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $(".grupoAlergenos").change(function(){
        obtieneAlergenos($(this));
    });
}

function obtenerNameAlergeno(id,losDos,indice){
    var parts=id.split('-');
    parts[1]=parts[1].split('[]');
    var esArray='';
    if(parts[0].indexOf('grupo')==-1){
        esArray='[]';
    }
    if(losDos){
        var apoyo = parts[0].match(/(\D+)(\d*)$/);
        parts[0]=apoyo[1] + indice;
        return parts[0] +'-'+ parts[1][0] + esArray;
    } else {
        return parts[0] +'-'+ ++parts[1][0] + esArray;
    }
}

function obtenerIdAlergeno(id,losDos,indice){
    var parts=id.split('-');
    parts[1]=parts[1].split('[]');
    if(losDos){
        var apoyo = parts[0].match(/(\D+)(\d*)$/);
        parts[0]=apoyo[1] + indice;
        return parts[0] +'-'+ parts[1][0];
    } else {
        return parts[0] +'-'+ ++parts[1][0];
    }
}

function eliminaFila(tabla){
  if($('#'+tabla).find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val();
        $('#'+tabla+' tr').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' tr:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' tr:not(:first)').eq(i).find("input:not([type=checkbox],.input-block-level),select,textarea").attr("name", function(){
                //Expresión regular para separar el nombre del campo y el numero de fila del name y el id
                var parts = this.id.match(/(\D+)(\d*)$/);
                //Creo un nombre nuevo incrementando el número de fila (++parts[2])
                return parts[1] + i;
                //Hago lo mismo con los IDs
            }).attr("id", function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                return parts[1] + i;
            });

            $('#'+tabla+' tr:not(:first)').eq(i).find("input[type=checkbox]").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}
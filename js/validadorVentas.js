//Para usar en conjunción con validador.js

//Oyente especial para el botón submit
$(':submit').unbind();//Le quito el validador por defecto que se asigna al importar validador.js
$(':submit').click(function(e){
	e.preventDefault();
	
	validaVenta();
});
//Fin oyente especial para el botón submit

function validaVenta(){
	if($('#confirmada').val()=='SI' && $('#codigoServicio0').val()!=undefined){//Venta confirmada/ha confirmar de servicios
		validaVentaServicios();
	}
	else{
		validaVentaFormacion('');//TODO: descomentar código de abajo y validar todos los clientes y trabajadores
	}
	/*else if($('#confirmada').val()=='SI' && $('#codigoTrabajador0').val()!=undefined && $('input[name=privado]:checked').val()!='BONIFICADA'){//Venta confirmada/ha confirmar de servicios
		validaVentaFormacion('compruebaDatosVentaFormacionNormal();');
	}
	else if($('#confirmada').val()=='SI' && $('#codigoTrabajador0').val()!=undefined && $('input[name=privado]:checked').val()=='BONIFICADA'){//Venta confirmada/ha confirmar de servicios
		validaVentaFormacion('compruebaDatosVentaFormacionBonificada();');
	}*/
}

function validaVentaServicios(){
	var codigoCliente=$('#codigoCliente').val();

	var consulta=$.post('../listadoAjax.php?include=ventas&funcion=compruebaDatosVentaServicios();',{'codigoCliente':codigoCliente});
	consulta.done(function(respuesta){
		if(respuesta=='ok'){
			validaCamposObligatorios();
		}
		else{
			alert("No se puede guardar la venta, faltan los siguientes datos: "+respuesta);
		}
	});
}

function validaVentaFormacion(funcion){
	/*var datosFormulario={};
	datosFormulario['codigoCliente']=$('#codigoCliente').val();

	for(var i=0;$('#codigoTrabajador'+i).val()!=undefined;i++){
		datosFormulario['codigoTrabajador'+i]=$('#codigoTrabajador'+i).val();
	}

	var consulta=$.post('../listadoAjax.php?include=ventas&funcion='+funcion,datosFormulario);
	consulta.done(function(respuesta){
		if(respuesta=='ok'){
			validaCamposObligatorios();
		}
		else{
			alert('No se puede guardar la venta, faltan los siguientes datos: '+respuesta);
		}
	});*/

	validaCamposObligatorios();//TODO: descomentar código de arriba y validar todos los clientes y trabajadores
}
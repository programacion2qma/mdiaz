<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de informe agenda

function listadoTareas(){
	$columnas=array('fechaInicio','horaInicio','fechaFin','horaFin','tarea','prioridad','estado','razonSocial','observaciones','fechaInicio','fechaFin');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT tareas.*, clientes.razonSocial FROM tareas LEFT JOIN clientes ON tareas.codigoCliente=clientes.codigo $having";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			formateaFechaWeb($datos['fechaInicio']),
			formateaHoraWeb($datos['horaInicio']),
			formateaFechaWeb($datos['fechaFin']),
			formateaHoraWeb($datos['horaFin']),
			$datos['tarea'],
			$datos['prioridad'],
			$datos['estado'],
			$datos['razonSocial'],
			$datos['observaciones'],
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function filtroTareas(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoFecha(0,'F. Inicio desde');
	campoFecha(9,'Hasta');
	campoFecha(1,'F. Fin desde');
	campoFecha(10,'Hasta');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(4,'Tarea');
	campoSelect(5,'Prioridad',array('','Alta','Normal','Baja'),array('','alta','normal','baja'),'','selectpicker span2 show-tick',"");
	campoSelect(6,'Estado',array('','OK visita','No interesa','Ilocalizable','Seguimiento llamada','Seguimiento visita, nueva llamada','Teléfono incorrecto'),array('','OK visita','No interesa','Ilocalizable','Seguimiento llamada','Seguimiento visita, nueva llamada','Telefono incorrecto'));
	campoSelectConsultaAjax(7,'Cliente',"SELECT razonSocial AS codigo, razonSocial AS texto FROM clientes",false,'clientes/gestion.php?codigo=','selectpicker selectAjax span3 show-tick','');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


//Fin parte informe de agenda
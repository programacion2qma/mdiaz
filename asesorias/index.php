<?php
  $seccionActiva=1;
  include_once('../cabecera.php');
  
  operacionesAsesorias();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <?php
        creaBotonesGestion();
      ?>

      <!--div class="fixed-action-btn click-to-toggle">
        <a class="btn-floating btn-large">
          <i class="icon-bars"></i> //Icono principal
        </a>
        <ul> //Opciones
          <li><a class="btn-floating btn-success" href="gestion.php" title='Nueva asesoría'><i class="icon-plus"></i></a></li>
          <li><a class="btn-floating btn-danger noAjax" id="eliminar" title='Eliminar'><i class="icon-trash"></i></a></li>
        </ul>
      </div-->

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Asesorías registradas</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroAsesorias();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaAsesorias">
                <thead>
                  <tr>
                    <th> ID </th>
                    <th> Asesoría </th>
                    <th> Tlf. principal </th>
                    <th> eMail principal </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaAsesorias','../listadoAjax.php?include=asesorias&funcion=listadoAsesorias();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaAsesorias');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de asesorías

function operacionesAsesorias(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaAsesoria();
	}
	elseif(isset($_POST['asesoria'])){
		$res=creaAsesoria();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('asesorias');
	}

	mensajeResultado('asesoria',$res,'Asesoría');
    mensajeResultado('elimina',$res,'Asesoría', true);
}

function creaAsesoria(){
	$res=insertaDatos('asesorias');

	if($res){
		$res=insertaContactosAsesoria($res);
	}

	return $res;
}

function actualizaAsesoria(){
	$res=actualizaDatos('asesorias');

	if($res){
		$res=insertaContactosAsesoria($_POST['codigo']);
	}

	return $res;
}

function insertaContactosAsesoria($codigoAsesoria){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos["codigoContacto$i"]) || isset($datos["contacto$i"]);$i++){
		if(isset($datos['codigoContacto'.$i]) && isset($datos['contacto'.$i])){//Actualización
			$res=$res && consultaBD("UPDATE contactos_asesorias SET contacto='".$datos["contacto$i"]."', telefono='".$datos["telefono$i"]."', email='".$datos["email$i"]."' WHERE codigo='".$datos['codigoContacto'.$i]."';");
		}
		elseif(!isset($datos['codigoContacto'.$i]) && isset($datos['contacto'.$i])){//Inserción
			$res=$res && consultaBD("INSERT INTO contactos_asesorias VALUES(NULL,'".$datos['contacto'.$i]."',
			'".$datos['telefono'.$i]."','".$datos['email'.$i]."',$codigoAsesoria)");
		}
		else{//Eliminación
			$res=$res && consultaBD("DELETE FROM contactos_asesorias WHERE codigo='".$datos['codigoContacto'.$i]."';");
		}
	}

	cierraBD();

	return $res;
}

function listadoAsesorias(){
	global $_CONFIG;

	$columnas=array('asesorias.codigo','asesoria','telefonoPrincipal','emailPrincipal','observaciones','activo','telefono','email');
	$where=obtieneWhereListado("WHERE 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT asesorias.*, telefono, email FROM asesorias LEFT JOIN contactos_asesorias ON asesorias.codigo=contactos_asesorias.codigoAsesoria $where GROUP BY asesorias.codigo $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT asesorias.*, telefono, email FROM asesorias LEFT JOIN contactos_asesorias ON asesorias.codigo=contactos_asesorias.codigoAsesoria $where GROUP BY asesorias.codigo;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['codigo'],
			$datos['asesoria'],
			formateaTelefono($datos['telefonoPrincipal']),
			"<a href='mailto:".$datos['emailPrincipal']."'>".$datos['emailPrincipal']."</a>",
        	botonAcciones(array('Detalles','Crear colaborador con mismos datos'),array('asesorias/gestion.php?codigo='.$datos['codigo'],'colaboradores/gestion.php?codigoAsesoria='.$datos['codigo']),array("icon-search-plus",'icon-share-alt'),array(0,2)),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionAsesoria(){
	operacionesAsesorias();

	abreVentanaGestion('Gestión de Asesorías','?','span3');
	$datos=compruebaDatos('asesorias');

	campoID($datos);
	campoTexto('asesoria','Nombre asesoría',$datos,'span3');
	areaTexto('observaciones','Observaciones',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoSimboloValidador('telefonoPrincipal','Teléfono principal','<i class="icon-phone"></i>',$datos,'input-small','asesorias');
	campoTextoSimboloValidador('emailPrincipal','eMail principal','<i class="icon-envelope"></i>',$datos,'input-large','asesorias');
	campoRadio('activo','Activa',$datos,'SI');

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');

	abreColumnaCampos();

	creaTablaContactos($datos);

	cierraVentanaGestion('index.php',true);
}


function creaTablaContactos($datos){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Contacto/s:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaContactos'>
				  	<thead>
				    	<tr>
				            <th> Contacto </th>
							<th> Teléfono </th>
							<th> eMail </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM contactos_asesorias WHERE codigoAsesoria=".$datos['codigo'],true);
				  			while($datosContactos=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaContactos($datosContactos,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaContactos(false,$i);
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaContactos\");'><i class='icon-plus'></i> Añadir contacto</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaContactos\");'><i class='icon-trash'></i> Eliminar contacto</button>
				</div>
			</div>
		</div>
	</div>";
}

function imprimeLineaTablaContactos($datos,$i){
	$j=$i+1;

	if($datos){
		campoOculto($datos['codigo'],'codigoContacto'.$i);
	}

	echo "<tr>";
		
			campoTextoTabla('contacto'.$i,$datos['contacto'],'span4');
			campoTextoTablaValidador('telefono'.$i,$datos['telefono'],'input-small pagination-right','contactos_asesorias','codigoAsesoria');
			campoTextoTablaValidador('email'.$i,$datos['email'],'span3','contactos_asesorias','codigoAsesoria');

	echo "	<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
		</tr>";
}


function filtroAsesorias(){
	abreCajaBusqueda();
	abreColumnaCampos();

	$columnas=array('codigo','asesoria','telefonoPrincipal','emailPrincipal','observaciones','activo','telefono','email');

	campoTexto(1,'Asesoría','','span3');
	campoTextoSimbolo(2,'Teléfono','<i class="icon-phone"></i>','','input-small');
	campoTextoSimbolo(3,'eMail','<i class="icon-envelope"></i>','','input-large');
	campoTexto(4,'Observaciones','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoSimbolo(6,'Teléfono contacto','<i class="icon-phone"></i>','','input-small');
	campoTextoSimbolo(7,'eMail contacto','<i class="icon-envelope"></i>','','input-large');
	campoSelectSiNoFiltro(5,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de asesorías
<?php
  $seccionActiva=36;
  include_once("../cabecera.php");
  $datos=gestionGrupo();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>

<script type="text/javascript" src="../../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript" src="../js/validador.js"></script>
<script type="text/javascript" src="../js/funcionesVentaFormacion.js"></script>
<script type="text/javascript" src="../js/validadorVentas.js"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>js/filasTablaAlumnos.js" type="text/javascript"></script>

<script type="text/javascript" src="../js/campoFechaVenta.js"></script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
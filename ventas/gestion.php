<?php
  $seccionActiva=36;
  include_once("../cabecera.php");
  gestionVentaServicios();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript" src="../../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript" src="../js/campoFechaVenta.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	if(!$('#observacionesParaFactura').hasClass('hide')){
		$('#observacionesParaFactura').wysihtml5({locale: "es-ES"});
	}
	
	oyenteCambios();

	//Oyente estado (para no dejar marcar "Válida" si no se ha llamado al cliente)
	$('#estado').change(function(){
		oyenteEstado($(this).val());
	});

	//Oyentes tabla
	$('#insertaFilaTabla').click(function(){
		insertaFila('tablaVentaServicios');
		oyenteUltimaFila();
	});


	$('#insertaFilaComplementos').click(function(){
		insertaFila('tablaComplementos');
		oyenteUltimaFilaComplemento();
	});


	$('#eliminaFilaTabla,#eliminaFilaComplementos').click(function(){
		var tabla=$(this).attr('tabla');
		eliminaFila(tabla);
		
		oyenteCalcula();
	});
	//Fin oyentes tabla
	
	//Oyente tipo de venta (para poner cálculos a 0)
	$('input[name=tipoVentaServicio]').change(function(){
		oyenteCalcula();
	});
	//Fin oyente tipo de venta

	oyentePreciosServicios();
	oyentePreciosComplementos();

	$('#codigoCliente').change(function(){
		oyenteCliente($(this).val());
	});
});

function oyentePreciosServicios(){
	$('#tablaVentaServicios').find('select').change(function(){
		obtienePrecioServicio($(this));
		oyenteCalcula();
	});

	$('#tablaVentaServicios').find('.precioUnidad,.unidades').change(function(){
		oyenteCalcula();
	});
}

function oyenteUltimaFila(){
	obtienePrecioServicio($('#tablaVentaServicios tr:last select'));
	$('#tablaVentaServicios').find('tr:last select').change(function(){
		obtienePrecioServicio($(this));
		oyenteCalcula();
	});

	$('#tablaVentaServicios').find('tr:last .precioUnidad, tr:last .unidades').change(function(){
		oyenteCalcula();
	});
}

function oyenteCalcula(){
	var total=0;
	var tipoVenta=$('input[name=tipoVentaServicio]:checked').val();

	$('#tablaVentaServicios').find('.precioUnidad').each(function(){
		var fila=obtieneFilaCampo($(this));
		var precioUnidad=formateaNumeroCalculo($(this).val());
		var unidades=formateaNumeroCalculo($('#unidades'+fila).val());
		var totalLinea=precioUnidad*unidades;

		if(tipoVenta=='NORMAL'){
			total+=totalLinea;
			$('#total'+fila).val(formateaNumeroWeb(totalLinea));
		}
		else{
			$('#total'+fila).val('0,00');
		}
	});

	$('#tablaComplementos').find('.precioComplemento').each(function(){
		var precioComplemento=formateaNumeroCalculo($(this).val());
		total+=precioComplemento;
	});

	$('#total').val(formateaNumeroWeb(total));
}

function obtienePrecioServicio(campo){
	var fila=obtieneFilaCampo(campo);
	var precio=$('#servicio'+campo.val()).text();
	$('#precioUnidad'+fila).val(precio);

	//Para poner 1 unidad por defecto:
	if($('#unidades'+fila).val()=='' || $('#unidades'+fila).val()=='0'){
		$('#unidades'+fila).val('1');
	}
}

function oyenteCliente(codigoCliente){
	if(codigoCliente!='NULL'){
		var consulta=$.post('../listadoAjax.php?funcion=consultaDatosClienteVenta();',{'codigoCliente':codigoCliente},
		function(respuesta){
			$('#codigoComercial').val(respuesta.codigoComercial);
			$('#codigoColaborador').val(respuesta.codigoColaborador);
			$('#codigoTelemarketing').val(respuesta.codigoTelemarketing);
			$('#codigoServicio0').val(respuesta.codigoServicio);

			$('#codigoComercial').selectpicker('refresh');
			$('#codigoColaborador').selectpicker('refresh');
			$('#codigoTelemarketing').selectpicker('refresh');
			$('#codigoServicio0').selectpicker('refresh');
			obtienePrecioServicio($('#codigoServicio0'));
			oyenteCalcula();
		},'json');
	}
}

//Parte de complementos
function oyentePreciosComplementos(){
	$('#tablaComplementos').find('select').change(function(){
		obtienePrecioComplemento($(this));
		oyenteCalcula();
	});

	$('#tablaComplementos').find('.precioComplemento').change(function(){
		oyenteCalcula();
	});
}

function obtienePrecioComplemento(campo){
	var fila=obtieneFilaCampo(campo);
	var precio=$('#complemento'+campo.val()).text();
	$('#precioComplemento'+fila).val(precio);
}

function insertaComplemento(){
	insertaFila("tablaComplementos");
	oyentePreciosComplementos($('#tablaComplementos tr:last .selectComplemento'));
}

function oyenteUltimaFilaComplemento(){
	obtienePrecioComplemento($('#tablaComplementos tr:last select'));
	$('#tablaComplementos').find('tr:last select').change(function(){
		obtienePrecioComplemento($(this));
		oyenteCalcula();
	});

	$('#tablaComplementos').find('tr:last .precioComplemento').change(function(){
		oyenteCalcula();
	});
}

function oyenteEstado(estado){
	if(estado=='VALIDA'){
		var codigoCliente=$('#codigoCliente').val();

		var consulta=$.post('../listadoAjax.php?include=ventas&funcion=consultaLlamadaClienteIndividual();',{'codigoCliente':codigoCliente});
		consulta.done(function(respuesta){
			if(respuesta=='NO'){
				$('#estado').selectpicker('val','PENDIENTE');
				alert('No se puede marcar la venta como válida porque no se ha marcado como realizada la llamada de comprobación de datos en la ficha del cliente.');
			}
		});
	}
}

//Fin parte de complementos
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
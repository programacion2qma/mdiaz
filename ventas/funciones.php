<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de ventas de servicios

function operacionesVentasServicios(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res = actualizaVentaServicios();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res = creaVentaServicios();
	}
	elseif(isset($_GET['confirmacion'])){
		$res = aceptaVenta();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res = eliminaDatos('ventas_servicios');
	}

	mensajeResultado('codigoCliente',$res,'Venta');
	mensajeResultado('codigoCliente0',$res,'Venta');
	mensajeResultadoAcepta('confirmacion',$res);
    mensajeResultado('elimina',$res,'Venta', true);
}

/*function creaEstadisticasVentas(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];
    $ejercicio=obtieneWhereEjercicioEstadisticas('fecha');
    $ejercicioGrupo=obtieneWhereEjercicioEstadisticas('fechaAlta');

    if($perfil=='ADMINISTRACION2'){
    	$res=consultaBD("SELECT COUNT(ventas_servicios.codigo) AS total FROM ventas_servicios LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo WHERE codigoUsuario=$codigoUsuario $ejercicio",true,true);
    	$formacion=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo WHERE esVenta='SI' AND codigoUsuario=$codigoUsuario $ejercicioGrupo",true,true);
    }
    elseif($perfil=='COMERCIAL'){
    	$res=consultaBD("SELECT COUNT(ventas_servicios.codigo) AS total FROM ventas_servicios LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo WHERE codigoUsuarioAsociado=$codigoUsuario $ejercicio",true,true);
    	$formacion=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo WHERE esVenta='SI' AND codigoUsuarioAsociado=$codigoUsuario $ejercicioGrupo",true,true);
    }
    elseif($perfil=='TELEMARKETING'){
    	$res=consultaBD("SELECT COUNT(ventas_servicios.codigo) AS total FROM ventas_servicios LEFT JOIN telemarketing ON ventas_servicios.codigoComercial=telemarketing.codigo WHERE codigoUsuarioAsociado=$codigoUsuario $ejercicio",true,true);
    	$formacion=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN telemarketing ON clientes.codigoComercial=telemarketing.codigo WHERE esVenta='SI' AND codigoUsuarioAsociado=$codigoUsuario $ejercicioGrupo",true,true);
    }
    elseif($perfil=='DELEGADO'){
    	$res=consultaBD("SELECT COUNT(ventas_servicios.codigo) AS total FROM ventas_servicios WHERE codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario $ejercicio)",true,true);
    	$formacion=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo WHERE esVenta='SI' AND codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario $ejercicioGrupo)",true,true);
    }
    else{
    	$res=estadisticasGenericas('ventas_servicios',false,"1=1 $ejercicio");
    	$formacion=estadisticasGenericas('grupos',false,"esVenta='SI' $ejercicioGrupo");
    }

    $res['total']+=$formacion['total'];

    return $res;
}*/


function aceptaVenta(){
	$res = true;
	$venta = datosRegistro('ventas_servicios',$_GET['confirmacion']);
		
	$res = consultaBD('UPDATE clientes SET posibleCliente="NO" WHERE codigo='.$venta['codigoCliente'],true);
	$res = $res && consultaBD('UPDATE ventas_servicios SET confirmada="SI" WHERE codigo='.$_GET['confirmacion'],true);
	$res = $res && crearMantenimiento($_GET['confirmacion']);
	
	return $res;
}

function crearMantenimiento($codigo){
	$res = true;
		
	$preventa        = datosRegistro('ventas_servicios',$codigo);
	$cliente         = datosRegistro('clientes',$preventa['codigoCliente']);
	$mantenimiento   = datosRegistro('clientes_mantenimiento',$preventa['codigoCliente'],'codigoCliente');
	$codigoCliente   = $preventa['codigoCliente'];
	$codigoConsultor = $cliente['codigoConsultor']=='' || $cliente['codigoConsultor']==NULL ? 'NULL' : $cliente['codigoConsultor'];	
	$fechaContrato   = date('Y-m-d');
	
	conexionBD();
	$fecha3meses = new DateTime($fechaContrato);
	$fecha3meses->add(new DateInterval('P3M'));
	$fecha3meses = $fecha3meses->format('Y-m-d');
		
	$sql = "INSERT INTO tareas VALUES (
				NULL,
				'Recordatorio para la realización o actualización anual de las Relaciones de Negocio o introducción de clientes. / Pedir Pymes.',
				'".$fecha3meses."',
				'".$fecha3meses."',
				'09:00',
				'14:00',
				'',
				'".$codigoCliente."',
				".$codigoConsultor."
			);";
	
	$res = $res && consultaBD($sql);
	
	$tarea3meses = ultimoRegistro('tareas');
	
	$fecha7meses = new DateTime($fechaContrato);
	$fecha7meses->add(new DateInterval('P7M'));
	$fecha7meses = $fecha7meses->format('Y-m-d');
	
	$sql = "INSERT INTO tareas VALUES(
				NULL,
				'Comprobar implantación de Prevención del blanqueo de capitales, recordar realización de formación e informes de riesgo. / Pedir Pymes.',
				'".$fecha7meses."',
				'".$fecha7meses."',
				'09:00',
				'14:00',
				'',
				'".$codigoCliente."',
				".$codigoConsultor.");";
	
	$res = $res && consultaBD($sql);
	$tarea7meses = ultimoRegistro('tareas');
		
	$fecha11meses = new DateTime($fechaContrato);
	$fecha11meses->add(new DateInterval('P11M'));
	$fecha11meses = $fecha11meses->format('Y-m-d');
	
	$sql = "INSERT INTO tareas VALUES(
				NULL,
				'Gestión de la renovación anual de su espacio web para la PBLC / Comprobar implantación de LOPD y reflejar posibles cambios respecto al organigrama / Auditar LOPD.',
				'".$fecha11meses."',
				'".$fecha11meses."',
				'09:00',
				'14:00',
				'',
				'".$codigoCliente."',
				".$codigoConsultor.");";
	
	$res = $res && consultaBD($sql);
	$tarea11meses = ultimoRegistro('tareas');
		
	if ($mantenimiento){
		$res = $res && consultaBD('DELETE FROM tareas WHERE codigo='.$mantenimiento['tarea3meses'].' OR codigo='.$mantenimiento['tarea7meses'].' OR codigo='.$mantenimiento['tarea11meses']);
		$res = $res && consultaBD('DELETE FROM clientes_mantenimiento WHERE codigo='.$mantenimiento['codigo']);
		$res = $res && consultaBD("INSERT INTO clientes_mantenimiento VALUES(NULL,".$codigoCliente.",'".$fechaContrato."','".$fecha3meses."',".$tarea3meses.",'".$fecha7meses."',".$tarea7meses.",'".$fecha11meses."',".$tarea11meses.")");
	} 
	else {			
		$res = $res && consultaBD("INSERT INTO clientes_mantenimiento VALUES(NULL,".$codigoCliente.",'".$fechaContrato."','".$fecha3meses."',".$tarea3meses.",'".$fecha7meses."',".$tarea7meses.",'".$fecha11meses."',".$tarea11meses.")");
	}
	
	cierraBD();

	return $res;
}

function actualizaMantenimiento($codigo){
	$res = true;
	$preventa      = datosRegistro('ventas_servicios',$codigo);
	$cliente       = datosRegistro('clientes',$preventa['codigoCliente']);
	$mantenimiento = datosRegistro('clientes_mantenimiento',$preventa['codigoCliente'],'codigoCliente');
	
	if($mantenimiento){
		$codigoCliente   = $preventa['codigoCliente'];
		$codigoConsultor = $cliente['codigoConsultor']=='' || $cliente['codigoConsultor']==NULL ? 'NULL' : $cliente['codigoConsultor'];
		$fechaContrato   = formateaFechaBD($_POST['fecha'], true);
		
		$fecha3meses = new DateTime($fechaContrato);
		$fecha3meses->add(new DateInterval('P3M'));
		$fecha3meses = $fecha3meses->format('Y-m-d');
		
		$fecha7meses = new DateTime($fechaContrato);
		$fecha7meses->add(new DateInterval('P7M'));
		$fecha7meses = $fecha7meses->format('Y-m-d');
		
		$fecha11meses = new DateTime($fechaContrato);
		$fecha11meses->add(new DateInterval('P11M'));
		$fecha11meses = $fecha11meses->format('Y-m-d');
		
		conexionBD();
		$res = $res && consultaBD('UPDATE clientes_mantenimiento SET fechaContrato="'.$fechaContrato.'",fecha3meses="'.$fecha3meses.'",fecha7meses="'.$fecha7meses.'",fecha11meses="'.$fecha11meses.'" WHERE codigo='.$mantenimiento['codigo']);
		$res = $res && consultaBD('UPDATE tareas SET fechaInicio="'.$fecha3meses.'",fechaFin="'.$fecha3meses.'" WHERE codigo='.$mantenimiento['tarea3meses']);
		$res = $res && consultaBD('UPDATE tareas SET fechaInicio="'.$fecha7meses.'",fechaFin="'.$fecha7meses.'" WHERE codigo='.$mantenimiento['tarea7meses']);
		$res = $res && consultaBD('UPDATE tareas SET fechaInicio="'.$fecha11meses.'",fechaFin="'.$fecha11meses.'" WHERE codigo='.$mantenimiento['tarea11meses']);
		cierraBD();
	}
	
	return $res;
}

function mensajeResultadoAcepta($indice,$res){
	if(isset($_GET[$indice])){
		if($res){
		  mensajeOk("Venta aceptada correctamente"); 
		}
		else{
		  mensajeError("se ha producido un error al gestionar los datos. Compruebe la información introducida."); 
		}
	}
}

function creaVentaServicios(){
	formateaPrecioBDD('total');
	$res = insertaDatos('ventas_servicios');
	if($res){
		$codigoVenta=$res;

		$datos = arrayFormulario();
		$res   = insertaConceptosVentaServicios($codigoVenta,$datos);
		$res   = $res && insertaConceptosComplementos($codigoVenta,$datos);
		$res   = $res && actualizaEstadoCliente($datos['codigoCliente']);
		crearMantenimiento($codigoVenta);
	}

	return $res;
}


function actualizaVentaServicios(){
	$datos = arrayFormulario();

	$res = true;
	//Control de cambios por parte de comerciales
	detectaCambios('ventas_servicios',$datos);//Función para detectar los cambios realizados por los perfiles COMERCIAL y derivados
	detectaCambiosSubTabla('codigoServicio','codigoVentaServicio','conceptos_venta_servicios',$datos);
	//Fin control de cambios

	formateaPrecioBDD('total');
	
	$res = $res && actualizaDatos('ventas_servicios');
	
	if($res){
		$res = $res && insertaConceptosVentaServicios($_POST['codigo'],$datos,true);
		$res = $res && insertaConceptosComplementos($_POST['codigo'],$datos,true);
		$res = $res && actualizaEstadoCliente($datos['codigoCliente']);
		if($_POST['confirmada']=='SI'){
			$mantenimiento=datosRegistro('clientes_mantenimiento',$_POST['codigoCliente'],'codigoCliente');
			if(!$mantenimiento){
				crearMantenimiento($_POST['codigo']);
			} else {
				if($datos['fecha']!=$datos['fechaAnt']){
					actualizaMantenimiento($_POST['codigo']);
				}
			}
		}
	}

	return $res;
}

//La siguiente función inserta cada línea de concepto en la venta de servicios. Si además los conceptos son nuevos para la venta (o la propia venta es nueva), inserta los trabajos asociados para la sección de Consultoría
function insertaConceptosVentaServicios($codigoVenta,$datos,$actualizacion=false){
	$res=true;

	conexionBD();

	$_SESSION['trabajos']=array();
	if($actualizacion){
		comprobarServicios($codigoVenta);
		$res=consultaBD("DELETE FROM conceptos_venta_servicios WHERE codigoVentaServicio=$codigoVenta");
	}
	for($i=0;isset($datos['codigoServicio'.$i]) || isset($datos['codigoServicioInsertado'.$i]);$i++){

		if(isset($datos['codigoServicioInsertado'.$i]) && isset($datos['codigoServicio'.$i]) && $datos['codigoServicio'.$i]==$datos['codigoServicioInsertado'.$i]){//Actualización
			$res=$res && insertaNuevoServicioVenta($codigoVenta,$datos,$i);
		}
		elseif(isset($datos['codigoServicioInsertado'.$i]) && isset($datos['codigoServicio'.$i]) && $datos['codigoServicio'.$i]!=$datos['codigoServicioInsertado'.$i]){//Borrado e inserción nueva
			$res=$res && insertaNuevoServicioVenta($codigoVenta,$datos,$i);
			$res=$res && consultaBD("UPDATE trabajos SET codigoServicio='".$datos['codigoServicio'.$i]."' WHERE codigoServicio=".$datos['codigoServicioInsertado'.$i]." AND codigoVenta=$codigoVenta;");//Modificación del trabajo
		}
		elseif(!isset($datos['codigoServicioInsertado'.$i]) && isset($datos['codigoServicio'.$i])){//Inserción nueva
			$res=$res && insertaNuevoServicioVenta($codigoVenta,$datos,$i);
		}
		else{//Eliminación
			$res=$res && consultaBD("DELETE FROM trabajos WHERE codigoServicio=".$datos['codigoServicioInsertado'.$i]." AND codigoVenta=$codigoVenta;");//Eliminación de los trabajos asociados (los conceptos ya están eliminados)
		}
	}
	$trabajos = $separado_por_comas = implode(",", $_SESSION['trabajos']);
	if($trabajos!=''){
		$res=consultaBD('DELETE FROM trabajos WHERE codigoServicio NOT IN('.$trabajos.') AND codigoVenta='.$codigoVenta);
	}

	cierraBD();

	return $res;
}

function comprobarServicios($codigoVenta){
	//CREAR ARRAY DE CONCEPTOS Y TRABAJOS Y OBTENER EL NÚMERO DE CADA UNO
	$i=0;
	$nConceptos=0;
	$conceptos=array();
	while(isset($_POST['codigoServicio'.$i])){
		if($_POST['codigoServicio'.$i]!=NULL){
			array_push($conceptos, $_POST['codigoServicio'.$i]);
			$nConceptos++;
		}
		$i++;
	}
	$nServicios=0;
	$servicios=array();
	$consulta=consultaBD('SELECT * FROM trabajos WHERE codigoVenta='.$codigoVenta);
	while($servicio=mysql_fetch_assoc($consulta)){
		array_push($servicios, $servicio['codigoServicio']);
		$nServicios++;
	}

	//COMPARAR
	while(isset($_POST['codigoServicio'.$i])){
		if($_POST['codigoServicio'.$i]!=NULL){
			if(!in_array($_POST['codigoServicio'.$i], $servicios)){
				if($nConceptos>$nServicios){

				} else if ($nConceptos==$nServicios){

				}
			}
		}
		$i++;
	}

}

function insertaNuevoServicioVenta($codigoVenta,$datos,$i){
	$res=true;

	if($datos['codigoServicio'.$i]!='NULL'){
		$codigoServicio = $datos['codigoServicio'.$i];
		$precioUnidad   = formateaNumeroWeb($datos['precioUnidad'.$i],true);
		$unidades       = formateaNumeroWeb($datos['unidades'.$i],true);
		$total          = formateaNumeroWeb($datos['total'.$i],true);

		$res = $res && consultaBD("INSERT INTO conceptos_venta_servicios VALUES(NULL,$codigoVenta,$codigoServicio,$precioUnidad,$unidades,$total);");
		$servicio = consultaBD("SELECT * FROM servicios WHERE codigo = ".$codigoServicio.";", false, true);
		
		$serviciosNO = array(
			'CAST',
			'CLAE',
			'OSLAC',
			'SFAST',
			'SFLAE',
			'TPLAG',
			'GASFLAG'
		);
		
		if(!in_array($servicio['referencia'], $serviciosNO)){
			$res = $res && insertaTrabajoDesdeVenta($datos['estado'],$datos['codigoCliente'],$codigoServicio,$codigoVenta,$datos['fecha'], false);//Inserción del trabajo solo en inserción de servicios nuevos
		}

	}

	return $res;
}


function insertaConceptosComplementos($codigoVenta,$datos,$actualizacion=false){
	$res=true;

	conexionBD();

	if($actualizacion){
		$res=consultaBD("DELETE FROM complementos_venta_servicios WHERE codigoVentaServicio=$codigoVenta");
	}

	for($i=0;isset($datos['codigoComplemento'.$i]);$i++){
		$codigoComplemento=$datos['codigoComplemento'.$i];
		$precioComplemento=formateaNumeroWeb($datos['precioComplemento'.$i],true);
		$observaciones=$datos['observaciones'.$i];

		$res=$res && consultaBD("INSERT INTO complementos_venta_servicios VALUES(NULL,$codigoComplemento,$precioComplemento,'$observaciones',$codigoVenta);");
	}

	cierraBD();

	return $res;
}

function listadoVentasServicios(){
	global $_CONFIG;

	$iconos=array(
					'VALIDA'=>'<i class="icon-check-circle iconoFactura icon-success" title="Válida"></i>',
					'INCIDENTADA'=>'<i class="icon-exclamation-circle iconoFactura icon-naranja" title="Incidentada"></i>',
					'PENDIENTE'=>'<i class="icon-minus-circle iconoFactura icon-inverse" title="Pendiente"></i>',
					'ANULADA'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Anulada"></i>'
				);

	
//Uso 2 columnas y 2 where porque las 2 consultas no tienen las columnas con el mismo nombre
	$columnas=array('fecha','razonSocial','servicios.referencia','servicio','ventas_servicios.total','ventas_servicios.estado','fecha','razonSocial','razonSocial');

	//Parte de WHERE
	$where=obtieneWhereListado("WHERE confirmada='SI'",$columnas);
	//Fin parte de WHERE

	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
		

	$query="SELECT ventas_servicios.codigo, clientes.codigo AS codigoCliente, fecha, comerciales.nombre, razonSocial, ventas_servicios.total, ventas_servicios.codigoColaborador, ventas_servicios.activo, ventas_servicios.codigoComercial, ventas_servicios.estado, servicios.referencia, servicios.servicio

	FROM ventas_servicios 
	LEFT JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio
	LEFT JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo
	LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo 
	LEFT JOIN telemarketing ON ventas_servicios.codigoTelemarketing=telemarketing.codigo 
	LEFT JOIN clientes ON ventas_servicios.codigoCliente=clientes.codigo 

	$where GROUP BY ventas_servicios.codigo";

	conexionBD();

	$consulta=consultaBD($query." $orden $limite");
	$consultaPaginacion=consultaBD($query);

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$datosServicios=obtieneServiciosVenta($datos['codigo'],$datos);
		$botonYcheck=obtieneBotonCheckVenta($datos);

		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['razonSocial'],
			$datosServicios['referencias'],
			$datosServicios['servicios'],
			"<div class='pagination-right nowrap'>".formateaNumeroWeb($datos['total']).' €</div>',
			"<div class='centro'>".$iconos[$datos['estado']]."</div>",
			$botonYcheck['boton'],
        	$botonYcheck['check'],
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	cierraBD();

	echo json_encode($res);
}



function obtieneBotonCheckVenta($datos){
	$res=array('boton'=>'','check'=>'');

		$res['boton']=botonAcciones(array('Detalles','Pasar a Preventa'),array("ventas/venta-servicios.php?codigo=".$datos['codigo'],"preventas/index.php?desconfirmar&codigo=".$datos['codigo']),array('icon-search-plus','icon-ban'),false,false,'');
		$res['check']="<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'></div>";

	return $res;
}


/*function obtieneBotonVenta($datos){
	if($datos['tipoVenta']=='SERVICIOS'){
		$res=creaBotonDetalles("ventas/venta-servicios.php?codigo=".$datos['codigo'],'');
	}
	else{
		$res=creaBotonDetalles("ventas/venta-formacion.php?codigo=".$datos['codigo'],'');
	}

	return $res;
}*/

function obtieneServiciosVenta($codigoVenta,$datosVenta){
	$res=array('referencias'=>'','servicios'=>'');


	$consulta=consultaBD("SELECT referencia, servicio FROM servicios INNER JOIN conceptos_venta_servicios ON servicios.codigo=conceptos_venta_servicios.codigoServicio WHERE codigoVentaServicio=$codigoVenta");
	while($datos=mysql_fetch_assoc($consulta)){
		$res['referencias'].=$datos['referencia'].', ';
		$res['servicios'].=$datos['servicio'].', ';
	}

	$res['referencias']=quitaUltimaComa($res['referencias']);
	$res['servicios']=quitaUltimaComa($res['servicios']);

	return $res;
}


function seleccionTipoVenta(){
	abreVentanaGestion('Gestión ventas','?','','icon-edit','',false,'noAjax');
	campoSelect('tipoVenta','Tipo de venta',array('Formación','Otros servicios'),array('FORMACION','SERVICIOS'));
	cierraVentanaGestion('index.php',false,true,'Continuar','icon-chevron-right');
}

function compruebaTipoVenta(){
	if(isset($_POST['tipoVenta']) && $_POST['tipoVenta']=='FORMACION'){
		header('Location: venta-formacion.php');
	}
	elseif(isset($_POST['tipoVenta']) && $_POST['tipoVenta']=='SERVICIOS'){
		header('Location: venta-servicios.php');
	}
}

function gestionVentaServicios(){
	operacionesVentasServicios();

	abreVentanaGestion('Gestión de venta de servicios','?','span3');
	$datos=compruebaDatos('ventas_servicios');

	campoSelectClienteFiltradoPorUsuario($datos,'Cliente','codigoCliente','obligatorio',true);
	campoFechaVenta($datos,'fecha','obligatorio');
	if($datos){
		campoOculto(formateaFechaWeb($datos['fecha']),'fechaAnt');
	}
	campoFecha('fechaFacturarServicio','Fecha para facturar',$datos);
	campoVencimientoVenta($datos);
	campoFechaVencimientoVenta($datos);

	//campoSelectConsulta('codigoComercial','Comercial',"SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre;",$datos);
	campoSelectComercial($datos,false);
	campoTipoVentaServicio($datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	//campoSelectConsulta('codigoColaborador','Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre;",$datos);
	campoSelectColaborador($datos,false);
	//campoSelectConsulta('codigoTelemarketing','Telemarketing',"SELECT codigo, nombre AS texto FROM telemarketing WHERE activo='SI' ORDER BY nombre;",$datos);
	campoTelemarketing($datos);

	campoEstado($datos);
	areaTexto('observacionesVerificacion','Observaciones verificación',$datos);

	campoOculto('NO','anulado');
	campoOculto('SI','activo');
	campoOculto($datos,'camposModificados','');
	campoOculto('SI','confirmada');

	cierraColumnaCampos();
	abreColumnaCampos('span3');

	creaTablaVentaServicios($datos);
	creaTablaComplementos($datos);
	campoTextoSimbolo('total','Importe total','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right',0,true);

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');

	campoObservacionesParaFactura($datos);

	cierraVentanaGestion('index.php');
}

function campoTipoVentaServicio($datos){
	if(isset($_GET['confirmacion'])){//Se viene de la confirmación de una preventa, por lo que el campo debe aparecer en blanco
		$valor=false;
	}
	else{
		$valor=$datos;
	}

	campoRadio('tipoVentaServicio','Tipo venta',$valor,'NORMAL',array('Con coste','Obsequio'),array('NORMAL','OBSEQUIO'));
}

function creaTablaVentaServicios($datos){
	conexionBD();

	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Servicio/s:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaVentaServicios'>
				  	<thead>
				    	<tr>
				            <th> Servicio </th>
							<th> Precio/Ud. </th>
							<th> Unidades </th>
							<th> Total </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$preciosBloqueados=false;
				  		if($_SESSION['tipoUsuario']=='COMERCIAL'){
				  			$preciosBloqueados=true;
				  		}

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM conceptos_venta_servicios WHERE codigoVentaServicio=".$datos['codigo']);
				  			while($datosVenta=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaVentaServicios($datosVenta,$i,$preciosBloqueados);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaVentaServicios(false,$i,$preciosBloqueados);
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' id='insertaFilaTabla'><i class='icon-plus'></i> Añadir servicio</button> 
					<button type='button' class='btn btn-small btn-danger' id='eliminaFilaTabla' tabla='tablaVentaServicios'><i class='icon-trash'></i> Eliminar servicio</button>
				</div>
			</div>
		</div>
	</div>";

	generaCamposPreciosServicios();
	cierraBD();
}

function imprimeLineaTablaVentaServicios($datos,$i,$preciosBloqueados){
	$j=$i+1;

	echo "
	<tr>";
	campoSelectConsulta('codigoServicio'.$i,'',"SELECT codigo, CONCAT(referencia,' - ',servicio) AS texto FROM servicios WHERE activo='SI' ORDER BY referencia;",$datos['codigoServicio'],'selectpicker span4 show-tick obligatorio','data-live-search="true"','',1,false);
	campoTextoSimbolo('precioUnidad'.$i,'','€',formateaNumeroWeb($datos['precioUnidad']),'input-mini pagination-right precioUnidad',1,$preciosBloqueados);//La clase precioUnidad no tiene CSS, es para el JS
	campoTextoTabla('unidades'.$i,$datos['unidades'],'input-mini pagination-right unidades obligatorio');
	campoTextoSimbolo('total'.$i,'','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right',1,$preciosBloqueados);
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";

	if($datos!=false && !isset($_GET['confirmacion'])){//Solo imprime el código del servicio insertado cuando se está en los detalles de una venta confirmada (al confirmar una preventa $datos será true pero existe $_GET['confirmacion'])
		campoOculto($datos['codigoServicio'],'codigoServicioInsertado'.$i);
	}
}

function generaCamposPreciosServicios(){
	$consulta=consultaBD("SELECT codigo, precio FROM servicios ORDER BY codigo;");
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto(formateaNumeroWeb($datos['precio']),'servicio'.$datos['codigo']);
	}
}


function filtroVentasServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoFecha(0,'Fecha desde');
	campoTexto(1,'Cliente','','span3');
	campoSelect(5,'Estado',array('','Válida','Incidentada','Pendiente','Anulada'),array('','VALIDA','INCIDENTADA','PENDIENTE','ANULADA'),false,'selectpicker span2 show-tick');
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(6,'Hasta');
	campoTexto(2,'Códigos');
	campoSelectConsulta(3,'Servicio',"SELECT servicio AS codigo, servicio AS texto FROM servicios WHERE activo='SI' ORDER BY servicio");

	cierraColumnaCampos();
	cierraCajaBusqueda();
}



function creaTablaComplementos($datos){
	conexionBD();

	echo "
	<br />
	<div class='control-group'>                     
		<label class='control-label'>Complemento/s:</label>
    	<div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaComplementos'>
				  	<thead>
				    	<tr>
				            <th> Complemento </th>
							<th> Precio </th>
							<th> Observaciones </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$query="SELECT * FROM complementos_venta_servicios WHERE codigoVentaServicio=".$datos['codigo'];
				  			$consulta=consultaBD($query);
				  			while($datosVenta=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaComplementos($datosVenta,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaComplementos(false,$i);
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaComplemento();'><i class='icon-plus'></i> Añadir complemento</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaComplementos\");' tabla='tablaComplementos'><i class='icon-trash'></i> Eliminar complemento</button>
				</div>
			</div>
		</div>
	</div>";

	generaCamposPreciosComplementos();
	cierraBD();
}

function imprimeLineaTablaComplementos($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	campoSelectConsulta('codigoComplemento'.$i,'',"SELECT codigo, complemento AS texto FROM complementos WHERE activo='SI' ORDER BY complemento;",$datos['codigoComplemento'],'selectpicker selectComplemento span4 show-tick','data-live-search="true"','',1,false);
	campoTextoSimbolo('precioComplemento'.$i,'','€',formateaNumeroWeb($datos['precioComplemento']),'input-mini pagination-right precioComplemento',1);//La clase precioComplemento no tiene CSS, es para el JS
	areaTextoTabla('observaciones'.$i,$datos['observaciones'],'areaTexto');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}


function generaCamposPreciosComplementos(){
	$consulta=consultaBD("SELECT codigo, precio FROM complementos ORDER BY codigo;");
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto(formateaNumeroWeb($datos['precio']),'complemento'.$datos['codigo']);
	}
}

//Fin parte de ventas de servicios

//Parte de ventas de formación

function gestionGrupo(){
	operacionesVentasServicios();

	abreVentanaGestion('Gestión de venta de formación','?','span12');
	$datos=obtieneDatosVentaFormacion();

	campoSelectAccionFormativaVenta($datos);
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoFechaVenta($datos,'fechaAlta','obligatorio');
	//campoSelectClienteFiltradoPorUsuario($datos,'Cliente','codigoCliente','obligatorio',true);
	campoSelectComercial($datos,false);	

	campoDato('Precio referencia','','precioReferencia');
	campoTextoSimbolo('importeVenta','Precio','€',formateaNumeroWeb($datos['importeVenta']),'input-mini pagination-right obligatorio');
	campoVencimientoVenta($datos);
	campoFechaVencimientoVenta($datos,'fechaFacturacionVencimiento');
	campoTipoVentaFormacion($datos);
	//campoSelect('cofinanciacion','Cofinanciación',array('','Aportación económica','Horas de formación en jornada laboral','No cofinancia'),array('','APORTACION','HORAS','NO'),$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	areaTexto('observacionesVenta','Observaciones venta',$datos);
	campoEstado($datos);
	areaTexto('observacionesVerificacion','Observaciones verificación',$datos);

	campoOculto($datos,'esVenta','SI');
	campoOculto('SI','confirmada');
	campoOculto('SI','nuevo');//Campo que no existe en la BDD y que sirve para que se limpien los datos al pulsar en "Guardar y registrar otra"
	campoOculto($datos,'yaTramitado','NO');//Necesario para la creación de tutorías

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');

	//creaTablaAlumnos($datos);
	creaTablaEmpresasVenta($datos);

	creaTablaComplementosFormacion($datos,true);

	campoObservacionesParaFactura($datos);

	//cierraVentanaGestionVentaFormacion('Guardar y registrar otra','index.php',true);
	cierraVentanaGestionVenta($datos);
	
	ventanaCreacionAccionFormativa();
	ventanaCreacionTrabajador();
}

function obtieneDatosVentaFormacion(){
	if(isset($_POST['nuevo'])){//Se ha pulsado en "Guardar y registrar otra"
		$datos=arrayFormulario();
		$datos['codigo']=false;
		$datos['codigoAccionFormativa']=false;
		$datos['observaciones']=false;
		$datos['cofinanciacion']=false;
	}
	else{
		$datos=compruebaDatos('grupos');
		if($datos){
			$consulta=consultaBD("SELECT codigoCliente, cofinanciacion FROM clientes_grupo WHERE codigoGrupo=".$datos['codigo']." ORDER BY codigo ASC",true,true);
			$datos['codigoCliente']=$consulta['codigoCliente'];
			$datos['cofinanciacion']=$consulta['cofinanciacion'];
		}
	}

	return $datos;
}

function campoTipoVentaFormacion($datos){
	if(isset($_GET['confirmacion'])){//Se viene de la confirmación de una preventa, por lo que el campo debe aparecer en blanco
		$valor=false;
	}
	else{
		$valor=$datos;
	}

	campoRadio('privado','Tipo venta',$valor,'',array('Privada','Bonificada','Obsequio'),array('PRIVADA','BONIFICADA','OBSEQUIO'));
}

function ventanaCreacionAccionFormativa(){
	abreVentanaModal('Acciones Formativas','cajaAccionFormativa');
	campoTexto('accionAF','Nombre acción',false,'span3');
	cierraVentanaModal('creaAccionFormativa');
}

/*function creaTablaAlumnos($datos){
	echo "
	<br />
	<h3 class='apartadoFormulario'>Alumnos</h3>
	<div class='table-responsive centro'>
		<table class='table table-striped tabla-simple' id='tablaAlumnos'>
		  	<thead>
		    	<tr>
		            <th> Trabajador </th>
		            <th> Observaciones </th>
					<th> </th>
		    	</tr>
		  	</thead>
		  	<tbody>";
		  	
		  	conexionBD();

		  	$i=0;
		  	if($datos!=false && $datos['codigo']!=false){
		  		$consulta=consultaBD("SELECT * FROM participantes WHERE codigoGrupo=".$datos['codigo']);
		  		while($participante=mysql_fetch_assoc($consulta)){
		  			imprimeLineaTablaAlumnos($i,$participante);
		  			$i++;
		  		}
		  	}

		  	if($i==0){
		  		imprimeLineaTablaAlumnos($i,false);
		  	}

		  	cierraBD();

		echo "
		  	</tbody>
		</table>
		<div class='centro'>
			<button type='button' class='btn btn-small btn-success' onclick='insertaFilaAlumno();'><i class='icon-plus'></i> Añadir alumno</button> 
			<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaAlumnos\");'><i class='icon-trash'></i> Eliminar alumno</button>
		</div>
	</div>";

	//El siguiente div oculto sirve para guardar la consulta de selección de alumnos, que será obtenida y modificada en JS para filtrar por empresas.
	divOculto("SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI'",'consultaAlumnos');
}

function imprimeLineaTablaAlumnos($i,$datos){
	$j=$i+1;

	echo "<tr>";
			//campoSelectConsultaAjax('codigoTrabajador'.$i,'',"SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI';",$datos['codigoTrabajador'],'trabajadores/gestion.php?codigo=','selectpicker selectAjax span6 show-tick selectAlumno','',1,false);
			campoTrabajadorVenta($i,$datos);
			areaTextoTabla('observacionesAlumno'.$i,$datos['observaciones'],'observacionesVentaFormacion');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		  </td>
		</tr>";
}*/

function creaVentaFormacion(){
	$res=true;

	$datos=arrayFormulario();
	$fechaActualizacion=fechaBD();
	$importeVenta=formateaNumeroWeb($datos['importeVenta'],true);

	conexionBD();
	//$grupo=obtieneGrupoAccionFormativa($datos['codigoAccionFormativa'],$datos['estado']);
	$grupo=0;

	$res=consultaBD("INSERT INTO grupos(codigo,codigoAccionFormativa,fechaAlta,grupo,observacionesVenta,activo,esVenta,importeVenta,confirmada,observacionesParaFactura,estado,fechaActualizacionGrupo,observacionesVerificacion,tipoVencimiento,codigoComercial,fechaFacturacionVencimiento,privado) 
						  VALUES(NULL,".$datos['codigoAccionFormativa'].",'".$datos['fechaAlta']."','$grupo','".$datos['observacionesVenta']."','PENDIENTE','".$datos['esVenta']."','$importeVenta','".$datos['confirmada']."','".$datos['observacionesParaFactura']."','".$datos['estado']."',
						  '$fechaActualizacion','".$datos['observacionesVerificacion']."','".$datos['tipoVencimiento']."',".$datos['codigoComercial'].",'".$datos['fechaFacturacionVencimiento']."','".$datos['privado']."');");
	if($res){
		$codigoGrupo=mysql_insert_id();

		$res=insertaEmpresasParticipantesVentaFormacion($codigoGrupo,$datos,true);

		$res=$res && insertaAlumnosVentaFormacion($codigoGrupo,$datos);
		//$res=$res && insertaComplementosVentaFormacion($codigoGrupo,$datos);

		//$res=$res && creaTutoriasInicioGrupo($codigoGrupo,$datos);
	}

	cierraBD();

	return $res;
}


function actualizaVentaFormacion(){
	$res=true;

	$datos=arrayFormulario();

	$importeVenta=formateaNumeroWeb($datos['importeVenta'],true);
	$fechaActualizacion=fechaBD();

	conexionBD();

	$res=consultaBD("UPDATE grupos SET codigoAccionFormativa=".$datos['codigoAccionFormativa'].", fechaAlta='".$datos['fechaAlta']."',
					 observacionesVenta='".$datos['observacionesVenta']."', estado='".$datos['estado']."', observacionesVerificacion='".$datos['observacionesVerificacion']."',
					 observacionesParaFactura='".$datos['observacionesParaFactura']."', confirmada='".$datos['confirmada']."', privado='".$datos['privado']."', 
					 tipoVencimiento='".$datos['tipoVencimiento']."', importeVenta='$importeVenta', codigoComercial=".$datos['codigoComercial'].", 
					 fechaFacturacionVencimiento='".$datos['fechaFacturacionVencimiento']."', fechaActualizacionGrupo='$fechaActualizacion', privado='".$datos['privado']."'
					 WHERE codigo=".$datos['codigo']);
	if($res){
		$res=insertaEmpresasParticipantesVentaFormacion($datos['codigo'],$datos,true,true);

		$res=$res && insertaAlumnosVentaFormacion($datos['codigo'],$datos,true);
		//$res=$res && insertaComplementosVentaFormacion($datos['codigo'],$datos,true);

		//$res=$res && creaTutoriasInicioGrupo($datos['codigo'],$datos);
	}

	cierraBD();

	return $res;
}



function actualizaCreditosClientes($datos){
	$res=true;

	if($datos['privado']=='BONIFICADA'){
		$arrayFecha=explode('-',$datos['fechaAlta']);
		$anio=$arrayFecha[0];

		for($i=0;isset($datos['codigoCliente'.$i]);$i++){
			$codigoCliente=$datos['codigoCliente'.$i];
			$nuevoPrecio=formateaNumeroWeb($datos['precioBonificado'.$i],true);
			$precioAnterior=$datos['precioBonificadoAnterior'.$i];//Ya tiene formato bdd

			if($datos['activo']=='SI' && $nuevoPrecio!=$precioAnterior){
				$diferencia=$precioAnterior-$nuevoPrecio;//Si el resultado es negativo, se restará al crédito disponible. Si es positivo (el nuevo precio es menor que el anterior), se sumará al crédito disponible.

				//A la formación bonificada se le debe incrementar la diferencia, si la hubiere. Por eso realizo operación de resta (5-(-1)=6)
				$res=$res && consultaBD("UPDATE creditos_cliente SET formacionBonificada=formacionBonificada-$diferencia, creditoDisponible=creditoDisponible+$diferencia WHERE codigoCliente=$codigoCliente AND ejercicio='$anio';");
			}
		}
	}

	return $res;
}

function obtieneGrupoAccionFormativa($codigoAccion,$estado){
	$res='';

	if($codigoAccion!=NULL && $estado=='VALIDA'){
		$grupo=consultaBD("SELECT MAX(grupos.grupo) AS grupo FROM grupos WHERE codigoAccionFormativa=$codigoAccion",false,true);

		if($grupo['grupo']==NULL){
			$res=date('y').'001';
		}
		else{
			$res=$grupo['grupo']+1;
		}
	}

	return $res;
}

function obtieneGrupoAccionFormativaActualizacion($codigoGrupo,$codigoAccion,$estado){
	$res='';

	if($estado=='VALIDA'){//Si el estado no es VALIDA, devuelvo el grupo vacío
		$grupo=consultaBD("SELECT grupo FROM grupos WHERE codigo=$codigoGrupo",false,true);
		if($grupo['grupo']==0){//Si el estado es VALIDA pero el grupo no tiene número de grupo asignado, lo genero
			$res=obtieneGrupoAccionFormativa($codigoAccion,$estado);
		}
		else{//Si el estado es VALIDA y el grupo ya tenía asignado un número de grupo, lo devuelvo para que no se cambie
			$res=$grupo['grupo'];
		}
	}

	return $res;
}


function actualizaEstadoCliente($codigoCliente){
	comprobarServiciosVenta();
	return consultaBD("UPDATE clientes SET posibleCliente='NO' WHERE codigo=$codigoCliente",true);
}


function campoEstado($datos){
	if(!$datos){
		$datos='PENDIENTE';
	}

	if($_SESSION['tipoUsuario']!='COMERCIAL'){
		campoSelectHTML('estado','Estado',array("<i class='icon-check-circle iconoFactura icon-success'></i> Válida","<i class='icon-exclamation-circle iconoFactura icon-naranja'></i> Incidentada","<i class='icon-minus-circle iconoFactura icon-inverse'></i> Pendiente","<i class='icon-times-circle iconoFactura icon-danger'></i> Anulada"),array('VALIDA','INCIDENTADA','PENDIENTE','ANULADA'),$datos,'selectpicker span3 selectEstado');
	}
	else{
		campoOculto($datos,'estado');
	}
}


function creaBotonesGestionVenta(){
    if($_SESSION['tipoUsuario']!='COMERCIAL'){
    	echo '<a class="btn-floating btn-large btn-success btn-creacion" href="gestion.php" title="Nuevo registro"><i class="icon-plus"></i></a>';
    }

    echo '<a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
}

function consultaLlamadaCliente(){
	$res='SI';

	$codigosClientes=$_POST['codigosClientes'];

	$consulta=consultaBD("SELECT codigo FROM clientes WHERE codigo IN($codigosClientes) AND llamadaVerificacion='NO';",true);
	if(mysql_num_rows($consulta)>0){
		$res='NO';
	}


	echo $res;
}


function compruebaDatosVentaServicios(){
	$res='ok';
	$codigoCliente=$_POST['codigoCliente'];

	$datos=consultaBD("SELECT razonSocial, cif, domicilio, telefono, movil, email, medioPago, firma, ccc, ejercicio, fechaFirma
					   FROM clientes 
					   LEFT JOIN cuentas_cliente ON clientes.codigo=cuentas_cliente.codigoCliente
					   LEFT JOIN creditos_cliente ON clientes.codigo=creditos_cliente.codigoCliente
					   WHERE clientes.codigo=$codigoCliente",true,true);

	$campos=compruebaCampoNecesarioValidacionAjax($datos['razonSocial'],'razón social');
	$campos.=compruebaCampoNecesarioValidacionAjax($datos['cif'],'CIF');
	$campos.=compruebaCampoNecesarioValidacionAjax($datos['domicilio'],'domicilio');
	$campos.=compruebaCamposTelefonoCliente($datos['telefono'],$datos['movil']);
	$campos.=compruebaCampoNecesarioValidacionAjax($datos['email'],'email');
	$campos.=compruebaCampoNecesarioValidacionAjax($datos['medioPago'],'medio de pago');
	$campos.=compruebaCampoNecesarioValidacionAjax($datos['firma'],'firma');
	
	if($datos['medioPago']=='RECIBO SEPA'){
		$campos.=compruebaCampoNecesarioValidacionAjax($datos['ccc'],'cuenta bancaria');
	}

	$campos.=compruebaCampoNecesarioValidacionAjax($datos['ejercicio'],'ejercicio crédito formación');
	$campos.=compruebaCampoNecesarioValidacionAjax($datos['fechaFirma'],'fecha firma crédito formación');

	if($campos!=''){
		$res=finalizaNumeracionComas($campos);
	}

	echo $res;
}

function compruebaCamposTelefonoCliente($telefono,$movil){
	$res='';

	$fijo=compruebaCampoNecesarioValidacionAjax($telefono,'teléfono');
	$movil=compruebaCampoNecesarioValidacionAjax($movil,'móvil');

	if($fijo!='' && $movil!=''){
		$res='teléfono/móvil, ';
	}

	return $res;
}


function compruebaDatosVentaFormacionNormal(){
	$res='ok';
	$datos=arrayFormulario();
	$codigoCliente=$datos['codigoCliente'];

	conexionBD();
	$cliente=consultaBD("SELECT razonSocial, cif, domicilio, telefono, movil, email, medioPago, firma, codigoEmisor, ccc, ejercicio, fechaFirma
					   FROM clientes LEFT JOIN emisores_documentos_cliente ON clientes.codigo=emisores_documentos_cliente.codigoCliente
					   LEFT JOIN cuentas_cliente ON clientes.codigo=cuentas_cliente.codigoCliente
					   LEFT JOIN creditos_cliente ON clientes.codigo=creditos_cliente.codigoCliente
					   WHERE clientes.codigo=$codigoCliente",false,true);

	$campos=compruebaCampoNecesarioValidacionAjax($cliente['razonSocial'],'razón social');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['cif'],'CIF');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['domicilio'],'domicilio');
	$campos.=compruebaCamposTelefonoCliente($cliente['telefono'],$cliente['movil']);
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['email'],'email');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['medioPago'],'medio de pago');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['firma'],'firma');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['codigoEmisor'],'emisor de documentación');
	
	if($cliente['medioPago']=='RECIBO SEPA'){
		$campos.=compruebaCampoNecesarioValidacionAjax($cliente['ccc'],'cuenta bancaria');
	}

	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['ejercicio'],'ejercicio crédito formación');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['fechaFirma'],'fecha firma crédito formación');


	for($i=0;isset($datos['codigoTrabajador'.$i]);$i++){
		$alumno=consultaBD("SELECT nombre, apellido1, nif, email, telefono, movil FROM trabajadores_cliente WHERE codigo=".$datos['codigoTrabajador'.$i],false,true);
		$camposAlumno='';
		
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['nombre'],'nombre alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['apellido1'],'primer apellido de alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['nif'],'DNI alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['email'],'email alumno');
		$camposAlumno.=compruebaCamposTelefonoCliente($alumno['telefono'],$alumno['movil']);

		if($camposAlumno!=''){
			$campos.="\nAlumno ".$alumno['nombre'].' '.$alumno['apellido1'].': '.finalizaNumeracionComas($camposAlumno);
		}
	}

	cierraBD();

	if($campos!=''){
		$res=finalizaNumeracionComas($campos);
	}

	echo $res;
}

function consultaLlamadaClienteIndividual(){

	$codigoCliente=$_POST['codigoCliente'];

	$cliente=consultaBD("SELECT llamadaVerificacion FROM clientes WHERE codigo=".$codigoCliente,true,true);
	$res=$cliente['llamadaVerificacion'];


	echo $res;
}


function compruebaDatosVentaFormacionBonificada(){
	$res='ok';
	$datos=arrayFormulario();
	$codigoCliente=$datos['codigoCliente'];

	conexionBD();
	$cliente=consultaBD("SELECT razonSocial, cif, administrador, nifAdministrador, cuentaSS, pyme, nuevaCreacion, fechaNuevaCreacion, trabajadoresCotizando, 
						 domicilio, telefono, movil, email, medioPago, firma, codigoEmisor, ccc, ejercicio, fechaFirma, actividad, tieneRLT, firmaRLT, nombreRLT, nifRLT
					     FROM clientes LEFT JOIN emisores_documentos_cliente ON clientes.codigo=emisores_documentos_cliente.codigoCliente
					     LEFT JOIN cuentas_cliente ON clientes.codigo=cuentas_cliente.codigoCliente
					     LEFT JOIN creditos_cliente ON clientes.codigo=creditos_cliente.codigoCliente
					     LEFT JOIN cuentas_ss_clientes ON clientes.codigo=cuentas_ss_clientes.codigoCliente
					     WHERE clientes.codigo=$codigoCliente",false,true);

	$campos=compruebaCampoNecesarioValidacionAjax($cliente['razonSocial'],'razón social');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['cif'],'CIF');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['domicilio'],'domicilio');
	$campos.=compruebaCamposTelefonoCliente($cliente['telefono'],$cliente['movil']);
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['email'],'email');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['medioPago'],'medio de pago');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['firma'],'firma');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['codigoEmisor'],'emisor de documentación');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['administrador'],'nombre administrador');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['nifAdministrador'],'DNI administrador');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['cuentaSS'],'cuenta S.S.');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['pyme'],'si es o no pyme');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['nuevaCreacion'],'de nueva creación');

	if($cliente['nuevaCreacion']=='SI'){
		$campos.=compruebaCampoNecesarioValidacionAjax($cliente['fechaNuevaCreacion'],'fecha de creación');	
	}
	
	if($cliente['medioPago']=='RECIBO SEPA'){
		$campos.=compruebaCampoNecesarioValidacionAjax($cliente['ccc'],'cuenta bancaria');
	}

	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['trabajadoresCotizando'],'trabajadores cotizando el año anterior');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['ejercicio'],'ejercicio crédito formación');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['fechaFirma'],'fecha firma crédito formación');
	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['tieneRLT'],'si tiene RLT');

	if($cliente['tieneRLT']=='SI'){
		$campos.=compruebaCampoNecesarioValidacionAjax($cliente['firmaRLT'],'firma RLT');
		$campos.=compruebaCampoNecesarioValidacionAjax($cliente['nombreRLT'],'nombre RLT');
		$campos.=compruebaCampoNecesarioValidacionAjax($cliente['nifRLT'],'NIF RLT');
	}

	$campos.=compruebaCampoNecesarioValidacionAjax($cliente['actividad'],'actividad de la empresa');

	for($i=0;isset($datos['codigoTrabajador'.$i]);$i++){
		$alumno=consultaBD("SELECT nombre, apellido1, nif, email, telefono, movil, niss, cuentaSS, fechaNacimiento, sexo, discapacidad, terrorismo, violencia, categoriaProfesional, grupoCotizacion, nivelEstudios
							FROM trabajadores_cliente WHERE codigo=".$datos['codigoTrabajador'.$i],false,true);

		$camposAlumno='';
		
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['nombre'],'nombre alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['apellido1'],'primer apellido de alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['nif'],'DNI alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['email'],'email alumno');
		$camposAlumno.=compruebaCamposTelefonoCliente($alumno['telefono'],$alumno['movil']);
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['niss'],'NISS alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['cuentaSS'],'cuenta S.S. alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['fechaNacimiento'],'fecha nacimiento alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['sexo'],'sexo alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['discapacidad'],'discapacidad alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['terrorismo'],'si alumno es víctima de terrorismo');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['violencia'],'si alumno es víctima de violencia de género');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['categoriaProfesional'],'categoría profesional de alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['grupoCotizacion'],'grupo cotización de alumno');
		$camposAlumno.=compruebaCampoNecesarioValidacionAjax($alumno['nivelEstudios'],'nivel estudios de alumno');

		if($camposAlumno!=''){
			$campos.="\nAlumno ".$alumno['nombre'].' '.$alumno['apellido1'].': '.finalizaNumeracionComas($camposAlumno);
		}
	}

	cierraBD();

	if($campos!=''){
		$res=finalizaNumeracionComas($campos);
	}

	echo $res;
}

function cierraVentanaGestionVenta($datos){
	if($datos && $datos['estado']=='VALIDA'){
		cierraVentanaGestionPerfil(array('ADMIN','ADMINISTRACION1','ADMINISTRACION2'),'index.php');
	}
	else{
		cierraVentanaGestion('index.php');
	}
}

//Fin parte de ventas de formación
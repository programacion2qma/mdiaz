<?php
  $seccionActiva=4;
  include_once("../cabecera.php");
  $datos=gestionGrupo();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/filasTablaAlumnos.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$(':submit').unbind();//Le quito los oyentes de envío de formulario por defecto al botón submit (para el validador de tutores)
	
	<?php defineHoraAltaInicio($datos); ?>
	
	oyenteCambios();

	oyenteAccionFormativa(true);
	$('#codigoAccionFormativa').change(function(){
		oyenteAccionFormativa();
	});

	$('#crearEmpresa').click(function(){
		$('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	$('#crea-codigoAccionFormativa').click(function(){
		$('#cajaAccionFormativa').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	$('#enviar').click(function(){
		creaEmpresaParticipante();
	});

	$('#creaAccionFormativa').click(function(){
		creaAccionFormativa();
	});

	$('#horaFinM,#horaFinOnlineM').change(function(){
		validaHorarioManiana($(this).val());
	});

	$('#horaInicioT,#horaInicioOnlineT').change(function(){
		validaHorarioTarde($(this).val());
	});

	oyenteCostesParticipante('#tablaEmpresasParticipantes',true);

	oyentePlantillayTrabajadoresCliente('#tablaEmpresasParticipantes');

	//oyenteCreditoBonificado('#tablaEmpresasParticipantes');

	/*$(':submit').click(function(e){
		e.preventDefault();
		validaDatosGrupo();		
	});*/

	oyenteAlumnosYcomplementos();

	$('#crearComplemento').click(function(){
		$('#cajaComplemento').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	$('#registraComplemento').click(function(){
		creaComplemento();
	});

	//Oyente venta privada (para limitar la fecha de inicio)
	oyenteVentaPrivada();
	$('input[name=privado]').change(function(){
		oyenteVentaPrivada();
	});
	//Fin oyente venta privada

	//Oyente cofinanciación alumno
	oyenteCofinanciacionAlumno('select.selectAlumno');
	//Fin oyente cofinanciación alumno

	//Parte de complementos
	oyenteComplemento('#tablaComplementos .selectComplemento');
	//Fin parte de complementos

	//Oyente fecha vencimiento
	oyenteVencimiento();
	$('#tipoVencimiento').change(function(){
		oyenteVencimiento();
	});
	//Fin oyente fecha vencimiento


	//Oyente de tutores (para cambio de observaciones)
	$(document).on('change','#tablaTutores select',function(){
		obtieneObservacionesTutores();
	});
	//Fin oyente de tutores
});

function oyenteVencimiento(){
	if($('#tipoVencimiento').val()=='EN LA FECHA'){
		$('#cajaFechaFacturacion').removeClass('hide');
	}
	else{
		$('#cajaFechaFacturacion').addClass('hide');
	}
}

function oyenteAccionFormativa(actualizacion=false){
	var codigoAccion=$('#codigoAccionFormativa').val();
	if(codigoAccion!='NULL'){
		//Animación de carga
		$('#accionFormativa').html('<i class="icon-spinner icon-spin"></i>');
		$('#horas').html('<i class="icon-spinner icon-spin"></i>');

		var consulta=$.post('../listadoAjax.php?include=inicios-grupos&funcion=consultaDatosAccionFormativa();',{'codigoAccion':codigoAccion});
		consulta.done(function(respuesta){
			var array=respuesta.split('&{}&');//Los valores de la respuesta AJAX están serializados con un delimitador personalizado.
			var accionFormativa=array[0];
			var horas=array[1];
			var grupo=array[2];
			var medios=array[3];
			var coste=array[4];
			var modalidad=array[5];
			var privado=array[6];
			var tutores=array[7];
			var observaciones=array[8];
			var codigoCentroPresencial=array[9];
			var horasPresencial=array[10];
			var horasTeleformacion=array[11];
			var formadores=array[13];

			$('#accionFormativa').text(accionFormativa);
			$('#horas').text(horas);
			$('#precioAccion').text(coste);
			$('#codigoCentroPresencial').selectpicker('val',codigoCentroPresencial);
			$('#horasPresencial').text(horasPresencial);
			$('#horasTeleformacion').text(horasTeleformacion);

			oyenteHorarioOnline(modalidad);
			oyenteModalidad(modalidad);
			if(!actualizacion || $('#codigoTutor0').val()=='NULL'){
				seleccionaTutoresAccionFormativa(tutores,observaciones);
			}
			if(!actualizacion || $('#nifFormador0').val()==''){
				seleccionaFormadoresAccionFormativa(formadores,modalidad);
			}

			if($('#codigo').val()==undefined || $('input[name=mediosCentro]:checked').val()==undefined){//Los campos de costes, grupo y medios solo se cambian si se está en la creación (código es undefined) o si se viene de una venta (en cuyo caso un campo obligatorio, como medios para la formación, estará sin rellenar)
				$('#grupo').val(grupo);
				$('input[name=mediosCentro][value='+medios+']').attr('checked',true);
				//$('input[name=privado][value='+privado+']').attr('checked',true);
				calculaCostesOrganizacion(coste);
				oyenteVentaPrivada();
			}
		});
	}
}

function creaEmpresaParticipante(){
	$('#enviar').html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var fechaAltaEmpresa=$('#fechaAltaEmpresa').val();
	var nombreComercialEmpresa=$('#nombreComercialEmpresa').val();
	var razonSocialEmpresa=$('#razonSocialEmpresa').val();
	var cifEmpresa=$('#cifEmpresa').val();
	var domicilioEmpresa=$('#domicilioEmpresa').val();
	var cpEmpresa=$('#cpEmpresa').val();
	var localidadEmpresa=$('#localidadEmpresa').val();
	var codigoComercialEmpresa=$('#codigoComercialEmpresa').val();

	var consulta=$.post('../listadoAjax.php?include=inicios-grupos&funcion=creaEmpresaParticipante();',{
		'fechaAlta':fechaAltaEmpresa,
		'nombreComercial':nombreComercialEmpresa,
		'razonSocial':razonSocialEmpresa,
		'cif':cifEmpresa,
		'domicilio':domicilioEmpresa,
		'cp':cpEmpresa,
		'localidad':localidadEmpresa,
		'codigoComercial':codigoComercialEmpresa
	});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar la empresa.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			actualizaSelectsEmpresas(respuesta,razonSocialEmpresa);
		}

		$('#cajaGestion').modal('hide');
		$('#enviar').html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original

		//Borrado de valores del formulario (para siguientes inserciones)
		$('#fechaAltaEmpresa').val('');
		$('#nombreComercialEmpresa').val('');
		$('#razonSocialEmpresa').val('');
		$('#cifEmpresa').val('');
		$('#domicilioEmpresa').val('');
		$('#cpEmpresa').val('');
		$('#localidadEmpresa').val('');
		$('#codigoComercialEmpresa').val('');
	});
}

function actualizaSelectsEmpresas(codigo,nombre){
	$('#tablaEmpresasParticipantes').find('.celdaCliente option[value=NULL]').each(function(){
		$(this).after('<option value="'+codigo+'">'+nombre+'</option>');
	});

	$('#tablaEmpresasParticipantes .celdaCliente select').selectpicker('refresh');
}

function insertaParticipante(){
	var total=$('#numParticipantes').val();
	var i=$('select.selectAlumno').length;

	if(i==total || total==''){
		alert("No se pueden añadir más participantes");
	}
	else{
		insertaFilaParticipante("tablaEmpresasParticipantes");
		//IMPORTANTE: actualización de divs ocultos (no se actualizan por defecto con filasTabla.js)
		$('#tablaEmpresasParticipantes > tbody > tr:last').find('div.hide').attr("id", function(){
	        var parts = this.id.match(/(\D+)(\d*)$/);
	        return parts[1] + ++parts[2];
	    });
	    //Fin actualización divs ocultos
		
		consultaSelectAjax($('#tablaEmpresasParticipantes > tbody > tr:last .celdaCliente select.selectAjax'));

		actualizaBotonSelectAjax($('#tablaEmpresasParticipantes > tbody > tr:last'),$('#tablaEmpresasParticipantes > tbody > tr:last select.selectAjax'));

		oyenteCostesParticipante('#tablaEmpresasParticipantes > tbody > tr:last',false);

		$('#tablaEmpresasParticipantes > tbody > tr:last .costeDirecto').val(formateaNumeroWeb($('#precioAccion').text()));

		oyenteCreditoBonificado('#tablaEmpresasParticipantes tr:last');

		//Para fila alumno
		$('#tablaEmpresasParticipantes > tbody > tr:last select.selectAlumno').html("<option value='NULL'></option>").selectpicker('refresh');//Elimino el alumno que viene del cliente anterior
		consultaSelectAjax($('#tablaEmpresasParticipantes > tbody > tr:last select.selectAlumno'));
		oyentePlantillayTrabajadoresCliente('#tablaEmpresasParticipantes  > tbody > tr:last');
		oyenteCofinanciacionAlumno('#tablaEmpresasParticipantes > tbody > tr:last select.selectAlumno');
	}
}

function oyenteCostesParticipante(selector,inicial){
	$(selector).find('.participantes,.costeDirecto,.costeIndirecto,.costeOrganizacion').change(function(){//Oyentes de cálculo para cuando se cambia algún dato
		var fila=obtieneFilaCampo($(this));
		var numParticipantes=formateaNumeroCalculo($('#numParticipantes'+fila).val());
		var costesIndirectos=formateaNumeroCalculo($('#costesIndirectos'+fila).val());
		var costesDirectos=formateaNumeroCalculo($('#costesDirectos'+fila).val());
		var plantilla=$('#plantilla'+fila).text();

		//Parte de cálculo de costes directos (varía en función del número de participantes)
		if($(this).hasClass('participantes')){//Esta condición sirve para que, en caso de que el campo modificado sea el propio de costes indirectos, éste no se recalcule (imposibilitando su edición manual)
			costesDirectos=$('#precioAccion').text();
			costesDirectos*=numParticipantes;
			$('#costesDirectos'+fila).val(formateaNumeroWeb(costesDirectos));
		}
		//Fin parte de cálculo de costes directos

		//Parte de costes de organización
		if($(this).hasClass('participantes') || $(this).hasClass('costeDirecto')){//El coste de organización se pone automáticamente al cambiar los participantes o los costes directos. Lo filtro así para que permita también modificarlo a mano
			calculaCosteOrganizacion(costesDirectos,plantilla,fila);
		}
		var costesOrganizacion=formateaNumeroCalculo($('#costesOrganizacion'+fila).val());
		//Fin parte de costes de organización

		var total=formateaNumeroWeb(costesDirectos+costesIndirectos+costesOrganizacion);

		$('#precioBonificado'+fila).val(total);
		$('#precioFactura'+fila).val(total);

		compruebaBonificadoDisponible(formateaNumeroCalculo(total),$('#creditoDisponible'+fila).text());
	});

	$(selector).find('.participantes').change(function(){
		compruebaParticipantesEmpresas();
	});	

	if(!inicial){//Entraría por aquí también desde calculaCostesOrganizacion() (a través del disparo del evento change()) calculando por primera vez los costes de los clientes cuando vienen de una venta
		var costeAF=$('#precioAccion').text();
		$(selector).find('.costeDirecto').each(function(){
			var participantes=$(this).parent().parent().parent().find('.participantes').val();
			var costeDirecto=costeAF*participantes;
			$(this).val(formateaNumeroWeb(costeDirecto));
		});
	}

}

function oyentePlantillayTrabajadoresCliente(selector){
	$(selector).find('.celdaCliente select.selectAjax').each(function(){
		//oyenteDatosCliente($(this));
		$(this).change(function(){
			oyenteDatosCliente($(this));
			oyenteAlumnosYcomplementos();
		});
	});
}

function oyenteDatosCliente(campo){
	if(campo.val()!='NULL' && compruebaClienteRepetido(campo.val(),campo)){
		var fila=obtieneFilaCampo(campo);
		var costeDirecto=formateaNumeroCalculo($('#costesDirectos'+fila).val());
		var fechaGrupo=$('#fechaAlta').val();
		
		var consulta=$.post('../listadoAjax.php?include=inicios-grupos&funcion=consultaDatosCliente();',{'codigoCliente':campo.val(),'fechaGrupo':fechaGrupo},
		function(respuesta){
			$('#plantilla'+fila).text(respuesta.plantilla);
			$('#creditoDisponible'+fila).text(respuesta.creditoDisponible);
		},'json');
	}
}

function compruebaClienteRepetido(codigoCliente,select){
	var ocurrencias=0;

	$('.celdaCliente select').each(function(){
		if($(this).val()==codigoCliente){
			ocurrencias++;
		}
	});

	if(ocurrencias>1){
		alert("Ha seleccionado el mismo cliente más de una vez. Por favor seleccione otro cliente.");
		select.selectpicker('val','NULL');
	}

	return ocurrencias;
}

function calculaCostesOrganizacion(costeDirecto){
	$('#tablaEmpresasParticipantes').find('.costeDirecto').each(function(){
		var fila=obtieneFilaCampo($(this));
		var plantilla=$('#plantilla'+fila).text();
		var participantes=$('#numParticipantes'+fila).val();

		$(this).val(formateaNumeroWeb(costeDirecto*participantes));

		calculaCosteOrganizacion(costeDirecto,plantilla,fila);

		$(this).change();//Dispara el evento onchange para que se ejecute las función de cálculo oyenteCostesParticipante(). Al crear una venta de formación y entrar en ella desde Grupos, si no se hace esto no se calculan los costes por defecto de los participantes
	});
}

function calculaCosteOrganizacion(costeDirecto,plantilla,fila){
	var porcentaje=0;

	if(plantilla<6){
		porcentaje=20;
	}
	else if(plantilla<10){
		porcentaje=15;
	}
	else{
		porcentaje=10;
	}

	var costeOrganizacion=costeDirecto*porcentaje/100;
	$('#costesOrganizacion'+fila).val(formateaNumeroWeb(costeOrganizacion));
}

function actualizaBotonSelectAjax(fila,campo){
	var boton=fila.find('.botonSelectAjax');
	var id=campo.attr('id');

	boton.attr('id','boton-'+id);

	campo.change(function(){
      oyenteBotonAjax(campo);
    });
}


function creaAccionFormativa(){
	$('#creaAccionFormativa').html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var codigoCentroGestor=$('#codigoCentroGestorAF').val();
	var accion=$('#accionAF').val();
	var modalidad=$('#modalidadAF').val();
	var horas=$('#horasAF').val();
	var accionFormativa=$('#accionFormativaAF').val();
	var proveedor=$('#proveedorAF').val();

	var consulta=$.post('../listadoAjax.php?include=inicios-grupos&funcion=creaAccionFormativa();',{
		'codigoCentroGestor':codigoCentroGestor,
		'accion':accion,
		'modalidad':modalidad,
		'horas':horas,
		'accionFormativa':accionFormativa,
		'proveedor':proveedor
	});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar la acción formativa.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			actualizaSelectAF(respuesta,accion,accionFormativa);
		}

		$('#cajaAccionFormativa').modal('hide');
		$('#creaAccionFormativa').html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original

		//Borrado de valores del formulario (para siguientes inserciones)
		$('#codigoCentroGestorAF').val('');
		$('#accionAF').val('');
		$('#modalidadAF').val('');
		$('#horasAF').val('');
		$('#accionFormativaAF').val('');
		$('#proveedorAF').val('');
	});
}

function actualizaSelectAF(codigo,nombre,af){
	$('#codigoAccionFormativa option[value=NULL]').after('<option value="'+codigo+'">'+af+' - '+nombre+'</option>');
	$('#codigoAccionFormativa').selectpicker('refresh');
}

function oyenteHorarioOnline(modalidad){
	if(modalidad=='MIXTA'){
		$('.cajaMixta').removeClass('hide');
		$('#cajaTutores').removeClass('hide');
	}
	else if(modalidad=='TELEFORMACIÓN'){
		$('#cajaTutores').removeClass('hide');
	}
	else{
		$('.cajaMixta').addClass('hide');
		$('#cajaTutores').addClass('hide');
	}
}

function validaHorarioManiana(horaIntroducida){
	var res=true;

	var array=horaIntroducida.split(':');
	var hora=parseInt(array[0]);
	var minutos=parseInt(array[1]);

	if(hora>15 || (hora==15 && minutos>0)){
		alert('Horario de mañana no válido.');
		res=false;
	}

	return res;
}

function validaHorarioTarde(horaIntroducida){
	var res=true;

	var array=horaIntroducida.split(':');
	var hora=parseInt(array[0]);
	var minutos=parseInt(array[1]);

	if(hora<15 || (hora==15 && minutos==0)){
		alert('Horario de tarde no válido.');
		res=false;
	}

	return res;
}


function oyenteCreditoBonificado(selector){
	$(selector).find('.costeBonificado').each(function(){
		$(this).change(function(){
			var fila=obtieneFilaCampo($(this));
			var creditoBonificado=formateaNumeroCalculo($(this).val());
			var creditoDisponible=$('#creditoDisponible'+fila).text();

			compruebaBonificadoDisponible(creditoBonificado,creditoDisponible);
		});
	});
}


function compruebaBonificadoDisponible(creditoBonificado,creditoDisponible){
	if(creditoDisponible!='' && creditoBonificado>creditoDisponible){//Cuando se cargue la página, como el crédito disponible va por AJAX, tardará más y siempre se mostraría el mensaje. Por eso la primera condición.
		alert('Atención: el crédito a bonificar es mayor que el disponible para este cliente en el ejercicio actual');
	}
}

function seleccionaTutoresAccionFormativa(codigoHTML,observaciones){
	if(codigoHTML=='NO'){
		$('#cajaTutores').addClass('hide');
	}
	else{
		$('#cajaTutores').removeClass('hide');

		if(codigoHTML!=''){
			$('#tablaTutores tbody').html(codigoHTML);
			$('#tablaTutores select').selectpicker('refresh');

			$('#observaciones').val(observaciones);
		}
	}
}

/*function validaDatosGrupo(){
	var tutores='';
	$('#tablaTutores').find('select').each(function(){
		tutores+=$(this).val()+'&{}&';
	});

	var codigo=$('#codigo').val();
	if(codigo==undefined){
		codigo='';
	}

	var consulta=$.post('../listadoAjax.php?include=inicios-grupos&funcion=compruebaNumeroGrupoYHorasTutores();',{
		'codigo':codigo,
		'codigoAccionFormativa':$('#codigoAccionFormativa').val(),
		'grupo':$('#grupo').val(),
		'numParticipantes':$('#numParticipantes').val(),
		'horaInicioM':$('#horaInicioM').val(),
		'horaFinM':$('#horaFinM').val(),
		'horaInicioT':$('#horaInicioT').val(),
		'horaFinT':$('#horaFinT').val(),
		'horaInicioOnlineM':$('#horaInicioOnlineM').val(),
		'horaFinOnlineM':$('#horaFinOnlineM').val(),
		'horaInicioOnlineT':$('#horaInicioOnlineT').val(),
		'horaFinOnlineT':$('#horaFinOnlineT').val(),
		'fechaInicio':$('#fechaInicio').val(),
		'fechaFin':$('#fechaFin').val(),
		'tutores':tutores,
	});

	consulta.done(function(respuesta){
		if(respuesta=='error'){
			alert('El número de grupo introducido ya existe para la acción formativa seleccionada.');
		}
		else if(respuesta!=''){
			var tutores=respuesta.split('&{}&');

			for(var i=0;i<tutores.length;i++){
				var tutor=$('#codigoTutor0').find('option[value='+tutores[i]+']').text();
				alert('El tutor '+tutor+' ha soprepasado los 80 alumnos tutorizados en el mismo horario.');
			}
		}
		else if(validaHorarioAntesEnvio()){
			validaCamposObligatorios('#edit-profile');
			//$('#edit-profile').submit();//No hay problemas con el número de alumnos por tutor y se envía el formulario
		}
	});
}*/


function validaHorarioAntesEnvio(){
	var res=true;

	$('#horaFinM,#horaFinOnlineM').each(function(){
		res=res && validaHorarioManiana($(this).val());
	});

	$('#horaInicioT,#horaInicioOnlineT').each(function(){
		res=res && validaHorarioTarde($(this).val());
	});

	if(res){
		var horas=$('#horaInicioM').val().trim()+$('#horaFinM').val().trim()+$('#horaInicioT').val().trim()+$('#horaFinT').val().trim();
		if(horas==''){
			res=false;
			alert('Error: debe introducir un horario de mañana o tarde para el grupo.');
		}
	}

	return res;
}


function oyenteModalidad(modalidad){
	if(modalidad=='PRESENCIAL' || modalidad=='MIXTA'){
		$('.cajaPresencial').removeClass('hide');
	}
	else{
		$('.cajaPresencial').addClass('hide');
	}
}


function oyenteAlumnosYcomplementos(){
	var codigosClientes=new Array();

	$('#tablaEmpresasParticipantes').find('.celdaCliente select.selectAjax').each(function(){//Modificación de la consulta del select de alumnos para que filtre por cliente
		var codigoCliente=$(this).val();

		if(codigoCliente!='' && codigoCliente!='NULL'){
			codigosClientes.push(codigoCliente);

			var fila=obtieneFilaCampo($(this));

			var consulta=$('#consultaAlumnos').text();//No escribo la consulta directamente, la estraigo de divOculto
			consulta+=" AND codigoCliente="+codigoCliente+" ORDER BY nombre";
			for(i=0;$('#codigoTrabajador'+fila+'-'+i).val()!=undefined;i++){
				$('#codigoTrabajador'+fila+'-'+i).attr('consulta',consulta);	
			}
		}
	});


	var consultaComplementos=$('#consultaComplementos').text();
	consultaComplementos+=" AND clientes.codigo IN("+codigosClientes.toString()+")  ORDER BY razonSocial;";

	$('#tablaComplementos').find('select.selectAjax').each(function(){//Modificación de la consulta del select de clientes para que muestre solo los elegidos anteriormente
		$(this).attr('consulta',consultaComplementos);	
	});
}

function insertaAlumno(id){
	//No puede haber más alumnos que el total marcado para el grupo. Comprobación.
	var total=$('#numParticipantes').val();
	var i=$('select.selectAlumno').length;

	if(i==total || total==''){
		alert("No se pueden añadir más participantes");
	}
	else{
		insertaFilaAlumno(id);
		//oyenteComplemento($('#'+id+' tr:last .selectComplemento'));
		oyenteAlumno($('#'+id+' > tbody > tr:last select.selectAlumno'));
		consultaSelectAjax($('#'+id+' > tbody > tr:last select.selectAlumno'));
		actualizaBotonSelectAjax($('#'+id+' > tbody > tr:last'),$('#'+id+' > tbody > tr:last select.selectAjax'));
		oyenteCofinanciacionAlumno('#'+id+' > tbody > tr:last select.selectAlumno');
	}
}


//No deja dos veces introducir el mismo alumno
function oyenteAlumno(selector){
	$(selector).change(function(){
		var codigoAlumno=$(this).val();

		var ocurrencias=0;

		$('select.selectAlumno').each(function(){
			if($(this).val()==codigoAlumno){
				ocurrencias++;
			}
		});

		if(ocurrencias>1){
			alert("Ha seleccionado el mismo participante más de una vez. Por favor seleccione otro participante.");
			$(this).selectpicker('val','NULL');
		}
	});
}


function oyenteVentaPrivada(){
	$('#fechaInicio,#fechaFin').datepicker('destroy');

	var privado=$('input[name=privado]:checked').val();
	if(privado!='PRIVADA'){//Si la venta no es privada...
		$('#fechaInicio,#fechaFin').datepicker({format:'dd/mm/yyyy',weekStart:1,onRender:function(date){
			var fechaActual=new Date();
    		fechaActual.setDate(fechaActual.getDate()+5);//.. no debe dejar poner como fecha de inicio del grupo una anterior a 7 días vista (contando el día actual y el propio día de inicio)

			return date.valueOf() < fechaActual.valueOf() ? 'disabled' : '';
		}}).on('changeDate',function(e){$(this).datepicker('hide');});
	}
	else{
		$('#fechaInicio,#fechaFin').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	}
}


function compruebaParticipantesEmpresas(){
	var numeroParticipantes=$('#numParticipantes').val();
	var participantes=0;

	$('.participantes').each(function(){
		if($(this).val()!='' && !isNaN($(this).val())){
			participantes+=formateaNumeroCalculo($(this).val());
		}
	});

	if(numeroParticipantes<participantes){
		alert('Atención: la suma de los participantes de todas las empresas es mayor al número de participantes indicado.');
		$('#numParticipantes').focus();
	}
}

function oyenteCofinanciacionAlumno(selector){
	$(selector).change(function(){
		if($(this).val()!='NULL'){
			var fila=$(this).attr('id').slice(16);
			var alumno=$(this).find('option:selected').text();

			var partesAlumno=alumno.split('(');//Formato de alumno: [nombre y apellidos] [(empresa)]
		  	partesAlumno=partesAlumno[1].split(')');
		  	var empresa=partesAlumno[0];

		  	var campoEmpresa=$('#tablaEmpresasParticipantes').find('option:contains('+empresa+'):selected').parent();
		  	var filaEmpresa=obtieneFilaCampo(campoEmpresa);
		  	var cofinanciacion=$('#cofinanciacion'+filaEmpresa).val();
		  	$('#cofinanciacionParticipante'+fila).selectpicker('val',cofinanciacion);
		}
	});
  
}


function insertaComplemento(){
	insertaFila("tablaComplementos");
	consultaSelectAjax($('#tablaComplementos tr:last select.selectAjax'));
	oyenteComplemento($('#tablaComplementos tr:last .selectComplemento'));
}

function oyenteComplemento(selector){
	$(selector).change(function(){
		var codigoComplemento=$(this).val();
		var fila=obtieneFilaCampo($(this));
		var id=$(this).attr('id').replace('codigo','').replace('#','').replace(/\d/g,'');

		var consulta=$.post('../listadoAjax.php?funcion=consultaPrecioComplemento();',{
			'codigoComplemento':codigoComplemento
		});

		consulta.done(function(respuesta){
			$('#precio'+id+fila).val(respuesta);
		});
	});
}

function creaComplemento(){
	$('#registraComplemento').html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var complemento=$('#complemento').val();
	var precio=$('#precio').val();

	var consulta=$.post('../listadoAjax.php?include=participantes&funcion=creaComplemento();',{
		'complemento':complemento,
		'precio':precio
	});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar el complemento.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			actualizaSelectsComplementos(respuesta,complemento);
		}

		$('#cajaComplemento').modal('hide');
		$('#registraComplemento').html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original

		//Borrado de valores del formulario (para siguientes inserciones)
		$('#complemento').val('');
		$('#precio').val('');
	});
}

function actualizaSelectsComplementos(codigo,nombre){
	$('#tablaComplementos select.selectComplemento').find('option[value=NULL]').each(function(){
		$(this).after('<option value="'+codigo+'">'+nombre+'</option>');
	});

	$('#tablaComplementos select.selectComplemento').selectpicker('refresh');
}


function eliminarXML(codigoGrupo,codigoXml,tipo,id){
	var eliminacion=$.post('../listadoAjax.php?include=inicios-grupos&funcion=eliminaGrupoXML();',{'codigoGrupo':codigoGrupo, 'codigoXml':codigoXml, 'tipo':tipo});
	
	eliminacion.done(function(respuesta){
		$('#'+id).text('No generado');
	});
}

function obtieneObservacionesTutores(){
	var codigosTutores=[];
	$('#tablaTutores select').each(function(){
		codigosTutores.push($(this).val());
	});

	var consulta=$.post('../listadoAjax.php?include=inicios-grupos&funcion=obtieneObservacionesTutores();',{'codigosTutores':codigosTutores});
	consulta.done(function(respuesta){
		if(respuesta=='error'){
			alert('Error: no se han podido obtener las observaciones de uno o más tutores.');
		}
		else{
			$('#observaciones').val(respuesta);
		}
	});
}

function seleccionaFormadoresAccionFormativa(codigoHTML,modalidad){
	if(modalidad=='PRESENCIAL' || modalidad=='MIXTA'){
		$('#tablaFormadores tbody').html(codigoHTML);
	}
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
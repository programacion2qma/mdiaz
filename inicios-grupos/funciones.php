<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de grupos

function operacionesGrupos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaGrupo();
	}
	elseif(isset($_POST['codigoAccionFormativa'])){
		$res=creaGrupo();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('grupos');
	}

	mensajeResultado('codigoAccionFormativa',$res,'Grupo');
    mensajeResultado('elimina',$res,'Grupo', true);
}

function creaGrupo(){
	$res=insertaDatos('grupos');
	if($res){
		$codigoGrupo=$res;
		$datos=arrayFormulario();

		conexionBD();

		$res=insertaEmpresasParticipantes($codigoGrupo,$datos);
		$res=$res && insertaTutores($codigoGrupo,$datos);
		$res=$res && insertaAlumnos($codigoGrupo,$datos);
		$res=$res && actualizaCreditosClientes($datos);
		insertaFormadoresGrupo($datos,$codigoGrupo);

		cierraBD();

	}

	return $res;
}

function actualizaGrupo(){
	$datos=arrayFormulario();

	//Control de cambios por parte de comerciales
	detectaCambios('grupos',$datos);//Función para detectar los cambios realizados por los perfiles COMERCIAL y derivados
	detectaCambiosSubTabla('codigoCliente','codigoGrupo','clientes_grupo',$datos);
	detectaCambiosSubTabla('codigoTutor','codigoGrupo','tutores_grupo',$datos);
	detectaCambiosSubTabla('codigoTrabajador','codigoGrupo','participantes',$datos);
	//Fin control de cambios

	$res=actualizaDatos('grupos');
	if($res){
		$codigoGrupo=$datos['codigo'];

		conexionBD();

		$res=insertaEmpresasParticipantes($codigoGrupo,$datos,true);
		$res=$res && insertaTutores($codigoGrupo,$datos,true);
		$res=$res && actualizaEstadoParticipantes($codigoGrupo,$datos);
		$res=$res && insertaAlumnos($codigoGrupo,$datos,true);
		$res=$res && actualizaCreditosClientes($datos);
		$res=$res && insertaFormadoresGrupo($datos,$codigoGrupo,true);

		cierraBD();

	}

	return $res;
}


function insertaEmpresasParticipantes($codigoGrupo,$datos,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM clientes_grupo WHERE codigoGrupo=$codigoGrupo");
	}

	for($i=0;isset($datos['codigoCliente'.$i]);$i++){
		$codigoCliente=$datos['codigoCliente'.$i];
		$cofinanciacion=$datos['cofinanciacion'.$i];
		$numParticipantes=$datos['numParticipantes'.$i];
		$costesDirectos=formateaNumeroWeb($datos['costesDirectos'.$i],true);
		$costesIndirectos=formateaNumeroWeb($datos['costesIndirectos'.$i],true);
		$costesOrganizacion=formateaNumeroWeb($datos['costesOrganizacion'.$i],true);
		$precioBonificado=formateaNumeroWeb($datos['precioBonificado'.$i],true);
		$precioFactura=formateaNumeroWeb($datos['precioFactura'.$i],true);

		$res=$res && consultaBD("INSERT INTO clientes_grupo VALUES(NULL,$codigoGrupo,$codigoCliente,'$cofinanciacion','$numParticipantes',$costesDirectos,$costesIndirectos,$costesOrganizacion,$precioBonificado,$precioFactura);");
	}

	return $res;
}

function insertaTutores($codigoGrupo,$datos,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM tutores_grupo WHERE codigoGrupo=$codigoGrupo");
	}

	for($i=0;isset($datos['codigoTutor'.$i]);$i++){
		$codigoTutor=$datos['codigoTutor'.$i];
		
		if($codigoTutor!='NULL'){
			$horas=$datos['horas'.$i];
			$res=$res && consultaBD("INSERT INTO tutores_grupo VALUES(NULL,$codigoGrupo,$codigoTutor,'$horas');");
		}
	}

	return $res;
}


function insertaAlumnos($codigoGrupo,$datos,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM participantes WHERE codigoGrupo=$codigoGrupo");
		$res=consultaBD("DELETE FROM complementos_participantes WHERE codigoParticipante IS NULL");//Limpieza de la tabla complementos_participantes
	}

	for($i=0;isset($datos['codigoCliente'.$i]);$i++){
		for($j=0;isset($datos['codigoTrabajador'.$i.'-'.$j]);$j++){
			$codigoTrabajador=$datos['codigoTrabajador'.$i.'-'.$j];
			$cofinanciacion=$datos['cofinanciacionParticipante'.$i.'-'.$j];
			$estado=$datos['estadoParticipante'.$i.'-'.$j];
			
			$observaciones=obtieneObservacionesParticipante($codigoTrabajador,$codigoGrupo);//Como al actualizar se hace un DELETE y un INSERT nuevo, esta función sirve rescatar las observaciones registradas (si las hay) para volverlas a insertar y que no se pierdan.

			$res=$res && consultaBD("INSERT INTO participantes VALUES(NULL,$codigoGrupo,$codigoTrabajador,'$cofinanciacion','$observaciones','$estado');");
			if($res){
				$res=$res && insertaComplementosParticipante(mysql_insert_id(),$datos['codigoCliente'.$i],$datos);//Uso el codigoCliente obtenido antes para hacer la inserción de complementos (los complementos se vinculan en el formulario con los clientes)
			}
		}
	}

	return $res;
}

//Como al actualizar se hace un DELETE y un INSERT de los participantes, esta función sirve rescatar las observaciones registradas (si las hay) para volverlas a insertar y que no se pierdan
function obtieneObservacionesParticipante($codigoTrabajador,$codigoGrupo){
	$res='';//Por defecto vacío, para los casos en que se registren nuevos participantes

	$datos=consultaBD("SELECT observaciones FROM participantes WHERE codigoTrabajador=$codigoTrabajador AND codigoGrupo=$codigoGrupo",false,true);
	if($datos){
		$res=$datos['observaciones'];
	}

	return $res;
}


function actualizaEstadoParticipantes($codigoGrupo,$datos){
	$estado='ACTIVO';

	if($datos['baja']=='SI'){
		$estado='BAJA';
	}
	if($datos['anulado']=='SI' || $datos['activo']!='SI'){
		$activo='ANULADO';
	}

	return consultaBD("UPDATE participantes SET estadoParticipante='$estado' WHERE codigoGrupo=$codigoGrupo;");
}




function insertaComplementosParticipante($codigoParticipante,$codigoCliente,$datos){
	$res=true;

	for($i=0;isset($datos['codigoClienteComplemento'.$i]);$i++){

		if($datos['codigoClienteComplemento'.$i]==$codigoCliente){
			$codigoComplemento=$datos['codigoComplemento'.$i];
			$precioComplemento=formateaNumeroWeb($datos['precioComplemento'.$i],true);
			$observaciones=$datos['observaciones'.$i];
			$complementoEnviado=compruebaComplementoEnviado($datos,$i);
			$fechaEnvio=$datos['fechaEnvio'.$i];

			$res=$res && consultaBD("INSERT INTO complementos_participantes VALUES(NULL,$codigoComplemento,$precioComplemento,'$complementoEnviado','0000-00-00','0000-00-00','0000-00-00','$fechaEnvio','$observaciones',$codigoParticipante);");
		}
	}

	return $res;
}



function compruebaComplementoEnviado($datos,$i){
	$res='NO';

	if(isset($datos['complementoEnviado'.$i])){
		$res='SI';
	}

	return $res;
}


function insertaEnvioClavesGrupo($codigoGrupo,$datos){
	$res=true;

	$af=consultaBD("SELECT modalidad FROM acciones_formativas WHERE codigo=".$datos['codigoAccionFormativa'],false,true);

	if($datos['activo']=='SI' && $datos['baja']=='NO' && $datos['anulado']=='NO' && ($af['modalidad']=='TELEFORMACIÓN' || $af['modalidad']=='MIXTA')){

		$codigosTrabajadores=array();
		$codigosParaEliminar=array();

		//Primero, obtengo todos los trabajadores vinculados al grupo:
		for($i=0;isset($datos['codigoCliente'.$i]);$i++){
			for($j=0;isset($datos['codigoTrabajador'.$i.'-'.$j]);$j++){
				array_push($codigosTrabajadores,$datos['codigoTrabajador'.$i.'-'.$j]);
			}
		}

		/*Luego, obtengo todos los envíos asociados al grupo. Si existe el trabajador en la tabla y en el array, no quita del array para no insertarlo 2 veces; 
		  si no existe en la tabla, no se hace nada para insertalo luego; y si existe en la tabla pero no en el array, se elimina*/
		$consulta=consultaBD("SELECT * FROM envio_claves_alumnos WHERE codigoGrupo=$codigoGrupo");
		while($envio=mysql_fetch_assoc($consulta)){
			$indice=array_search($envio['codigoTrabajador'],$codigosTrabajadores);//Busco el trabajador de la tabla en el array de trabajadores del grupo

			if($indice!==false){//Si se enuentra en el array de trabajadores del grupo
				unset($codigosTrabajadores[$indice]);//Lo elimino para no insertarlo por duplicado luego (no hago operaciones de actualización porque no son necesarias)
			}
			else{
				array_push($codigosParaEliminar,$envio['codigoTrabajador']);//Si no existe el trabajador extraído en el array de trabajadores del grupo, se guarda en el array de eliminación para borrarlo luego
			}
		}

		//Inserción de nuevos envíos de claves
		foreach($codigosTrabajadores as $codigoTrabajador){
			$res=$res && consultaBD("INSERT INTO envio_claves_alumnos(codigo,codigoTrabajador,codigoGrupo,estado) VALUES(NULL,$codigoTrabajador,$codigoGrupo,'PENDIENTE');");
		}

		//Eliminación de los antiguos
		foreach($codigosParaEliminar as $codigoTrabajador){
			if($codigoTrabajador!=''){
				$res=$res && consultaBD("DELETE FROM envio_claves_alumnos WHERE codigoTrabajador='$codigoTrabajador' AND codigoGrupo='$codigoGrupo'");
			}
		}
	}
	else{
		$res=$res && consultaBD("DELETE FROM envio_claves_alumnos WHERE codigoGrupo='$codigoGrupo'");
	}

	return $res;
}


function actualizaCreditosClientes($datos){
	$res=true;

	if($datos['privado']=='BONIFICADA'){
		$arrayFecha=explode('-',$datos['fechaAlta']);
		$anio=$arrayFecha[0];

		for($i=0;isset($datos['codigoCliente'.$i]);$i++){
			$codigoCliente=$datos['codigoCliente'.$i];
			$nuevoPrecio=formateaNumeroWeb($datos['precioBonificado'.$i],true);
			$precioAnterior=$datos['precioBonificadoAnterior'.$i];//Ya tiene formato bdd

			if($datos['activo']=='SI' && $nuevoPrecio!=$precioAnterior){
				$diferencia=$precioAnterior-$nuevoPrecio;//Si el resultado es negativo, se restará al crédito disponible. Si es positivo (el nuevo precio es menor que el anterior), se sumará al crédito disponible.

				//A la formación bonificada se le debe incrementar la diferencia, si la hubiere. Por eso realizo operación de resta (5-(-1)=6)
				$res=$res && consultaBD("UPDATE creditos_cliente SET formacionBonificada=formacionBonificada-$diferencia, creditoDisponible=creditoDisponible+$diferencia WHERE codigoCliente=$codigoCliente AND ejercicio='$anio';");
			}
		}
	}

	return $res;
}




function insertaFormadoresGrupo($datos,$codigoGrupo,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM formadores_grupo WHERE codigoGrupo=$codigoGrupo");
	}

	for($i=0;isset($datos['nifFormador'.$i]);$i++){
		$nifFormador=$datos['nifFormador'.$i];
		$nombreFormador=$datos['nombreFormador'.$i];
		$apellidosFormador1=$datos['primerApellidoFormador'.$i];
		$apellidosFormador2=$datos['segundoApellidoFormador'.$i];
		$telefonoFormador=$datos['telefonoFormador'.$i];
		$emailFormador=$datos['emailFormador'.$i];
		$horasFormador=$datos['horasFormador'.$i];

		$res=$res && consultaBD("INSERT INTO formadores_grupo VALUES(NULL,'$nifFormador','$nombreFormador','$apellidosFormador1','$apellidosFormador2','$telefonoFormador','$emailFormador','$horasFormador',$codigoGrupo);");
	}

	return $res;
}


/*function creaEstadisticasInicioGrupos(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];
    $ejercicio=obtieneWhereEjercicioEstadisticas('fechaAlta');

    if($perfil=='ADMINISTRACION2'){
        $consulta=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo WHERE codigoUsuario=$codigoUsuario $ejercicio GROUP BY grupos.codigo",true);
        $res['total']=mysql_num_rows($consulta);//Como uso GROUP BY (por los clientes en cada grupo), no puedo obtener directamente el número de grupos con mysql_fetch_assoc
    }
    elseif($perfil=='COMERCIAL'){
    	$consulta=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo WHERE codigoUsuarioAsociado=$codigoUsuario $ejercicio GROUP BY grupos.codigo",true);
        $res['total']=mysql_num_rows($consulta);
    }
    elseif($perfil=='TELEMARKETING'){
    	$consulta=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN telemarketing ON clientes.codigoTelemarketing=telemarketing.codigo WHERE codigoUsuarioAsociado=$codigoUsuario $ejercicio GROUP BY grupos.codigo",true);
        $res['total']=mysql_num_rows($consulta);
    }
    elseif($perfil=='DELEGADO'){
    	$consulta=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo WHERE codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario) $ejercicio GROUP BY grupos.codigo",true);
        $res['total']=mysql_num_rows($consulta);
    }
    elseif($perfil=='DIRECTOR'){
    	$consulta=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo WHERE codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario IN (SELECT usuarios.codigo FROM usuarios WHERE usuarios.codigoUsuario=$codigoUsuario)) $ejercicio GROUP BY grupos.codigo",true);
        $res['total']=mysql_num_rows($consulta);	
    }
    else{
    	$ejercicio=obtieneWhereEjercicioEstadisticas('fechaAlta');
        $res=estadisticasGenericas('grupos',false,"1=1 $ejercicio");
    }
	
	return $res;
}*/

function listadoGrupos(){
	global $_CONFIG;

	$columnas=array(
		'grupos.fechaAlta',
		'accionFormativa',
		'grupos.grupo',
		'accion',
		'modalidad',
		'razonSocial',
		'CONCAT(usuarios.nombre," ",usuarios.apellidos)',
		'comerciales.nombre',
		'grupos.numParticipantes',
		'grupos.numParticipantes-COUNT(DISTINCT participantes.codigo)',
		'fechaInicio',
		'fechaFin',
		'grupos.anulado',
		'codigoTutor',
		'grupos.privado',
		'grupos.activo',
		'grupos.fechaAlta',
		'fechaInicio',
		'fechaFin',
		'codigoUsuario'
	);

	$where=obtieneWhereListadoGrupos($columnas);//Utilizo WHERE por la agrupación de clientes y tutores (el HAVING realiza el filtrado TRAS la selección, y el WHERE ANTES)
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT grupos.codigo, grupos.fechaAlta, accion, modalidad, accionFormativa, grupos.grupo, grupos.numParticipantes, grupos.numParticipantes-COUNT(DISTINCT participantes.codigo) AS restantes, 
			fechaInicio, fechaFin, grupos.anulado, razonSocial, codigoTutor, grupos.privado AS pifAccion, grupos.activo, acciones_formativas.codigo AS codigoAccionFormativa, comerciales.nombre AS comercial, 
			CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS administrativo, codigoUsuario 
			FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo 
			LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo 
			LEFT JOIN trabajadores_cliente ON clientes_grupo.codigoCliente=trabajadores_cliente.codigoCliente 
			LEFT JOIN participantes ON participantes.codigoTrabajador=trabajadores_cliente.codigo AND clientes_grupo.codigoGrupo=participantes.codigoGrupo 
			LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo 
			LEFT JOIN tutores_grupo ON grupos.codigo=tutores_grupo.codigoGrupo 
			LEFT JOIN comerciales ON grupos.codigoComercial=comerciales.codigo
			LEFT JOIN telemarketing ON clientes.codigoTelemarketing=telemarketing.codigo 
			LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo
			$where";

	//echo $query;
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$clientes=obtieneClientesGrupo($datos['codigo']);

		$fila=array(
			formateaFechaWeb($datos['fechaAlta']),
			$datos['accionFormativa'],
			$datos['grupo'],
			"<a href='../acciones-formativas/gestion.php?codigo=".$datos['codigoAccionFormativa']."'>".$datos['accion']."</a>",
			$datos['modalidad'],
			$clientes,
			$datos['administrativo'],
			$datos['comercial'],
			$datos['numParticipantes'],
			$datos['restantes'],
			formateaFechaWeb($datos['fechaInicio']),
			formateaFechaWeb($datos['fechaFin']),
			"<div class='centro'>".$datos['anulado']."</div>",
			botonAcciones(array('Detalles','Control de asistencia'),array('inicios-grupos/gestion.php?codigo='.$datos['codigo'],'inicios-grupos/generaControlAsistencia.php?codigo='.$datos['codigo']),array('icon-search-plus','icon-cloud-download'),array(0,2),false,''),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}
	cierraBD();

	echo json_encode($res);
}

function obtieneClientesGrupo($codigoGrupo){
	$res='';

	$consulta=consultaBD("SELECT clientes.codigo, razonSocial FROM clientes INNER JOIN clientes_grupo ON clientes.codigo=clientes_grupo.codigoCliente WHERE codigoGrupo=$codigoGrupo ORDER BY razonSocial");
	while($cliente=mysql_fetch_assoc($consulta)){
		$res.="<a href='../clientes/gestion.php?codigo=".$cliente['codigo']."' class='noAjax' target='_blank'>".$cliente['razonSocial']."</a>, ";
	}

	$res=quitaUltimaComa($res);

	return $res;
}

function obtieneWhereListadoGrupos($columnas){
	$having='';

	if(isset($_GET['sSearch_9']) && $_GET['sSearch_9']!=''){
		$restantes=$_GET['sSearch_9'];

		if($restantes=='SI'){
			$having=" HAVING restantes>0";//Uso el alias "restantes" del COUNT (porque lo meto en el HAVING)
		}
		elseif($restantes=='NO'){
			$having=" HAVING restantes<1";//Uso el alias "restantes" del COUNT (porque lo meto en el HAVING)
		}

		unset($_GET['sSearch_9']);//Debe hacerse ANTES de la llamada a obtieneWhereListado(), para que no tenga el cuenta el campo restantes a la hora de construir el WHERE (porque daría fallo por uso incorrecto de función de agrupación)
	}

	$columnasWhere=$columnas;
	//unset($columnasWhere[9]);//Elimino la posición 9 del array de columnas, que se corresponde con la función de agrupación COUNT
	//$columnasWhere=array_values($columnasWhere);//Re-indexo el array para que no haya un salto entre la posición 8 y la 10
	$columnasWhere[9]='grupos.codigo';//Sustituyo el índice correspondiente a la columna de alumnos restantes por un campo neutro (si lo elimino se desvinculan los campos del filtro)

	$res=obtieneWhereListado('WHERE confirmada="SI" AND estado="VALIDA"',$columnasWhere,true);

	$res.=' GROUP BY grupos.codigo';
	$res.=$having;

	return $res;
}


function filtroGrupos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	$columnas=array(
		'grupos.fechaAlta',
		'accionFormativa',
		'grupos.grupo',
		'accion',
		'modalidad',
		'razonSocial',
		'CONCAT(usuarios.nombre," ",usuarios.apellidos)',
		'comerciales.nombre',
		'grupos.numParticipantes',
		'grupos.numParticipantes-COUNT(DISTINCT participantes.codigo)',
		'fechaInicio',
		'fechaFin',
		'grupos.anulado',
		'codigoTutor',
		'grupos.privado',
		'grupos.activo',
		'grupos.fechaAlta',
		'fechaInicio',
		'fechaFin',
		'codigoUsuario'
	);

	campoTexto(5,'Empresa');
	campoTexto(0,'Alta desde','','input-small hasDatepicker');
	campoTexto(16,'Hasta','','input-small hasDatepicker');
	campoTexto(1,'Código AF',false,'input-mini pagination-right');
    campoTexto(2,'Grupo',false,'input-mini pagination-right');
    campoTexto(3,'Acción');
	campoSelect(4,'Modalidad',array('','Presencial','Teleformación','Mixta'),array('','Presencial','Teleformación','Mixta'));
    campoTexto(10,'Inicio desde','','input-small hasDatepicker');
    campoTexto(17,'Hasta','','input-small hasDatepicker');

	cierraColumnaCampos();
	abreColumnaCampos();

    campoTexto(11,'Fin desde','','input-small hasDatepicker');
    campoTexto(18,'Hasta','','input-small hasDatepicker'); 
   	campoSelectSiNoFiltro(9,'Alumnos pendientes');    
    

	campoSelectConsulta(13,'Tutor',"SELECT codigo, CONCAT(nombre,' ',apellido1,' ',apellido2) AS texto FROM tutores WHERE activo='SI' ORDER BY nombre;");
	campoSelect(14,'Tipo venta',array('','Privada','Bonificada','Obsequio'),array('','PRIVADA','MODIFICADA','OBSEQUIO'),false,'selectpicker show-tick span2');
	campoSelectConsulta(19,'Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activo='SI' AND (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') ORDER BY nombre");
	campoSelectSiNoFiltro(12,'Anulado');
	campoSelect(15,'Activo',array('','Si','Pendiente','No'),array('','SI','PENDIENTE','NO'),false,'selectpicker span2 show-tick');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function gestionGrupo(){
	operacionesGrupos();

	abreVentanaGestion('Gestión de Grupos','?','span3');
	$datos=compruebaDatos('grupos');
	(isset($datos['codigo'])) ? $codigoEmisor=$datos['codigoEmisor'] : $codigoEmisor='1';
	campoFecha('fechaAlta','Fecha de alta',$datos);
	campoFecha('fechaFacturar','Fecha para facturar',$datos);
	campoAccionFormativa($datos);
	campoDato('Código AF','','accionFormativa');
	campoDato('Horas','','horas');
	campoTexto('grupo','Grupo',$datos,'input-mini pagination-right');
	campoRadio('privado','Tipo venta',$datos,'PRIVADA',array('Privada','Bonificada','Obsequio'),array('PRIVADA','BONIFICADA','OBSEQUIO'));
	
	campoTexto('fechaInicio','Fecha de inicio',formateaFechaWeb($datos['fechaInicio']),'input-small datepicker hasDatepicker obligatorio');
	campoTexto('fechaFin','Fecha de fin',formateaFechaWeb($datos['fechaFin']),'input-small datepicker hasDatepicker obligatorio');

	campoTexto('numParticipantes','Nº participantes',$datos,'input-mini pagination-right');

	cierraColumnaCampos();
	abreColumnaCampos('span4');
	
	campoRadio('mediosCentro','Medios para formación',$datos,'ENTIDADEXTERNA',array('De la entidad externa que tiene encomendada la organización','De la entidad de formación acreditada o inscrita'),array('ENTIDADEXTERNA','ENTIDADACREDITADA'),true);
	areaTexto('observaciones','Observaciones',$datos);
	campoDato('Observaciones venta',nl2br($datos['observacionesVenta']));
	
	campoVencimientoVenta($datos);
	campoFechaVencimientoVenta($datos,'fechaFacturacionVencimiento');

	//campoRadio('yaTramitado','Ya tramitado',$datos);
	//echo '<div class="anotacion">(si ya está tramitado, no se crean las tutorías automáticamente)</div>';
	campoOculto($datos,'yaTramitado','NO');//Pidieron este campo para luego descartarlo. Lo dejo oculto y en BDD por si acaso lo vuelven a pedir

	campoRadio('baja','Baja',$datos);
	echo '<div class="anotacion">(se mantienen los datos del inicio de grupo en la factura)</div>';

	campoRadio('anulado','Anulado',$datos);
	echo '<div class="anotacion">(los inicios de grupo desaparecen de la factura)</div>';
	
	campoActivoVenta($datos);
	campoXMLInicio($datos);
	campoXMLFin($datos);

	campoOculto($datos,'camposModificados','');
	campoOculto($datos,'esVenta','NO');
	campoOculto($datos,'confirmada','SI');
	campoOculto($datos,'observacionesParaFactura','');
	campoOculto($datos,'estado','VALIDA');
	campoOculto($datos,'observacionesVerificacion','');
	campoOculto($datos,'observacionesVenta','');
	campoOculto($datos,'codigoComercial','NULL');
	campoOculto(fecha(),'fechaActualizacionGrupo');

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');

	camposFormacionOnline($datos);
	camposFormacionPresencial($datos);

	creaTablaEmpresasParticipantes($datos);
	creaTablaComplementosFormacion($datos);

	//creaTablaAlumnos($datos);

	cierraVentanaGestion('index.php',true);

	ventanaCreacionEmpresa();
	ventanaCreacionAccionFormativa();
	ventanaCreacionComplemento();

	return $datos;
}

function campoXMLInicio($datos){
	global $_CONFIG;

	if($datos){
		$xml=consultaBD("SELECT xml_inicio.codigo FROM xml_inicio INNER JOIN grupos_xml_inicio ON xml_inicio.codigo=grupos_xml_inicio.codigoXmlInicio WHERE estado!='ELIMINADO' AND codigoGrupo=".$datos['codigo'],true,true);
		if($xml){
			$boton="<a class='btn btn-primary btn-small noAjax' href='".$_CONFIG['raiz']."exportar-xml-inicio/generaXML.php?codigo=".$xml['codigo']."'><i class='icon-cloud-download'></i> Descargar</a> <a href='javascript:void(0);' class='enlaceBorrado noAjax' onclick='eliminarXML(".$datos['codigo'].",".$xml['codigo'].",\"INICIO\",\"campoXMLInicio\");'><i class='icon-trash'></i> Eliminar</a>";
		}
		else{
			$boton="No generado";
		}

		campoDato('XML de Inicio',$boton,'campoXMLInicio');
	}
}


function campoXMLFin($datos){
	global $_CONFIG;

	if($datos){
		$xml=consultaBD("SELECT xml_fin.codigo FROM xml_fin INNER JOIN grupos_xml_fin ON xml_fin.codigo=grupos_xml_fin.codigoXmlFin WHERE estado!='ELIMINADO' AND codigoGrupo=".$datos['codigo'],true,true);
		if($xml){
			$boton="<a class='btn btn-primary btn-small noAjax' href='".$_CONFIG['raiz']."exportar-xml-fin/generaXML.php?codigo=".$xml['codigo']."'><i class='icon-cloud-download'></i> Descargar</a> <a href='javascript:void(0);' class='enlaceBorrado noAjax' onclick='eliminarXML(".$datos['codigo'].",".$xml['codigo'].",\"FIN\",\"campoXMLFin\");'><i class='icon-trash'></i> Eliminar</a>";
		}
		else{
			$boton="No generado";
		}

		campoDato('XML de Fin',$boton,'campoXMLFin');
	}
}

function camposFormacionPresencial($datos){
	echo '<h3 class="apartadoFormulario cajaMixta apartadoInicioGrupo hide">Formación Presencial</h3>';
	campoHorario('horaInicioM','horaFinM','Horario de mañana',$datos);
	campoHorario('horaInicioT','horaFinT','Horario de tarde',$datos);
	echo "<div class='hide cajaPresencial'>";
		campoTexto('centroImparticion','Lugar de impartición',$datos,'span4');
		campoTexto('domicilio','Domicilio',$datos,'span4');
		campoTexto('cp','Código postal',$datos,'input-mini pagination-right');
		campoTexto('poblacion','Población',$datos);
		creaTablaFormadores($datos);
	echo "</div>";
	creaTablaDiasImparticion($datos);
}


function camposFormacionOnline($datos){
	echo '<h3 class="apartadoFormulario cajaMixta hide apartadoInicioGrupo">Formación Online</h3>
	<div class="hide cajaMixta">';
		campoDato('Horas formación online','','horasTeleformacion');
		campoHorario('horaInicioOnlineM','horaFinOnlineM','Horario online mañana',$datos);
		campoHorario('horaInicioOnlineT','horaFinOnlineT','Horario online tarde',$datos);
		creaTablaDiasImparticion($datos,'Online');
	echo "</div>";
}


function ventanaCreacionEmpresa(){
	abreVentanaModal('Empresas');
	campoFecha('fechaAltaEmpresa','Fecha de alta');
	campoTexto('nombreComercialEmpresa','Nombre Comercial',false,'span3');
	campoTexto('razonSocialEmpresa','Razón Social',false,'span3');
	campoTextoValidador('cifEmpresa','CIF',false,'input-small','clientes');
	campoTexto('domicilioEmpresa','Domicilio',false,'span3');
	campoTexto('cpEmpresa','Código Postal',false,'input-mini pagination-right');
	campoTexto('localidadEmpresa','Localidad',false,'span3');
	//campoSelectConsulta('codigoAgrupacionEmpresa','Agrupación',"SELECT codigo, agrupacion AS texto FROM agrupaciones WHERE activo='SI' ORDER BY agrupacion;",false,'selectpicker span3 show-tick');
	//campoSelectConsulta('codigoComercialEmpresa','Comercial',"SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre;",false,'selectpicker span3 show-tick');
	campoSelectComercial(false,false,'codigoComercialEmpresa');
	cierraVentanaModal();
}

function ventanaCreacionAccionFormativa(){
	abreVentanaModal('Acciones Formativas','cajaAccionFormativa');
	campoSelectConsulta('codigoCentroGestorAF','Centro gestor pla. teleformación',"SELECT codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial;");
	campoTexto('accionAF','Nombre acción',false,'span3');
	campoSelect('modalidadAF','Modalidad',array('','Presencial','Teleformación','Mixta'),array('','PRESENCIAL','TELEFORMACIÓN','MIXTA'),false,'span3 selectpicker show-tick');
	campoTexto('horasAF','Horas',false,'input-mini pagination-right');
	campoTexto('accionFormativaAF','Código AF',false,'input-mini pagination-right');
	campoTexto('proveedorAF','Código de proveedor',false,'input-large');
	cierraVentanaModal('creaAccionFormativa');
}

function ventanaCreacionComplemento(){
	abreVentanaModal('Complementos','cajaComplemento');
	campoTexto('complemento','Complemento',false,'span3');
	campoTextoSimbolo('precio','Precio','€');
	cierraVentanaModal('registraComplemento');
}

function campoHorario($nombreCampo1,$nombreCampo2,$texto,$datos){
	$valor1='';
	$valor2='';
	if($datos){		
		$valor1=formateaHoraHorarioGrupo($datos[$nombreCampo1]);
		$valor2=formateaHoraHorarioGrupo($datos[$nombreCampo2]);
	}

	echo "
		<div class='control-group'>                     
	      <label class='control-label' for='$nombreCampo1'>$texto:</label>
	      <div class='controls'>";
	      	campoTextoSolo($nombreCampo1,$valor1,'input-mini pagination-right');
	echo "	- ";
	      	campoTextoSolo($nombreCampo2,$valor2,'input-mini pagination-right');
	echo "
	      </div> <!-- /controls -->       
	    </div> <!-- /control-group -->";
}


function formateaHoraHorarioGrupo($horaBDD){
	$res=formateaHoraWeb($horaBDD);
	
	if($res=='00:00'){
		$res='';
	}

	return $res;
}

function creaTablaEmpresasParticipantes($datos){
	echo "	
	<br />
	<h3 class='apartadoFormulario apartadoGrupos'>Participantes</h3>";

	campoImporteVentaGrupo($datos);

	echo "
	<div class='table-responsive centro'>
		<table class='table tabla-simple' id='tablaEmpresasParticipantes'>
		  	<thead>
		    	<tr>
		            <th class='cabecera1'> Empresa <button type='button' class='btn btn-primary btn-small' id='crearEmpresa'><i class='icon-plus-circle'></i> Crear empresa</button></th>
		            <th class='cabecera2'> Cofinanciación </th>
		            <th class='cabecera3'> Participantes </th>
		            <th class='cabecera4'> Costes directos </th>
		            <th class='cabecera4'> C. indirectos </th>
		            <th class='cabecera4'> C. organización </th>
					<th class='cabecera4'> Importe bonifica </th>
					<th class='cabecera4'> Importe factura </th>
					<th class='cabecera4'> </th>
		    	</tr>
		  	</thead>
		  	<tbody>";

		  		$i=0;
		  		conexionBD();
		  		if($datos){
		  			$arrayFecha=explode('-',$datos['fechaAlta']);
					$anio=$arrayFecha[0];

		  			$consulta=consultaBD("SELECT clientes_grupo.*, plantilla, creditoDisponible 
		  								  FROM clientes_grupo LEFT JOIN creditos_cliente ON clientes_grupo.codigoCliente=creditos_cliente.codigoCliente AND ejercicio='$anio'
		  								  WHERE codigoGrupo=".$datos['codigo']);

		  			while($datosParticipante=mysql_fetch_assoc($consulta)){
		  				imprimeLineaTablaEmpresasParticipantes($datos['codigo'],$datosParticipante,$i);
		  				$i++;
		  			}
		  		}

		  		if($i==0){
		  			imprimeLineaTablaEmpresasParticipantes(false,false,$i);
		  		}
		  		cierraBD();
	echo "				  		
		  	</tbody>
		</table>
		<div class='centro'>
			<button type='button' class='btn btn-small btn-success' onclick='insertaParticipante();'><i class='icon-plus'></i> Añadir empresa</button> 
			<button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaParticipante(\"tablaEmpresasParticipantes\");'><i class='icon-trash'></i> Eliminar empresa</button>
		</div>
	</div>";

	//El siguiente div oculto sirve para guardar la consulta de selección de alumnos, que será obtenida y modificada en JS para filtrar por empresas.
	divOculto("SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI'",'consultaAlumnos');
	
	divOculto('','precioAccion');//En este div irá el precio de la acción formativa seleccionada (para poner en la columna "Costes Directos")
}

function campoImporteVentaGrupo($datos){
	if($datos['esVenta']=='SI'){
		$importe=formateaNumeroWeb($datos['importeVenta']);
		campoDato('Importe Venta',$importe.' €');
	}
	else{
		campoOculto($datos,'importeVenta','0');
	}
}

function imprimeLineaTablaEmpresasParticipantes($codigoGrupo,$datos,$i){
	//$j=$i+1;//No utilizo $j para el check de eliminación porque el selector de eliminaFila contiene tbody, por lo que ya no se tiene en cuenta el tr de la cabecera

	echo "
	<tr>
		<td colspan='8' class='celdaConTabla'>
			<table class='table tabla-simple tabla-interna tablaPadre' id='tablaAlumnos$i'>
				<tbody>
					<tr>
						<td class='centro celdaCliente'>";
							campoSelectClienteListadoFiltradoPorUsuario($i,$datos['codigoCliente'],2);
	echo "					<br />
							<button type='button' class='btn btn-small btn-success' onclick='insertaAlumno(\"tablaAlumnos$i\");'><i class='icon-plus'></i> Añadir alumno</button> 
						</td>";

						campoSelect('cofinanciacion'.$i,'',array('','Aportación económica','Horas de formación en jornada laboral','No cofinancia'),array('','APORTACION','HORAS','NO'),$datos['cofinanciacion'],'selectpicker span3 show-tick','',1);
						campoTextoTabla('numParticipantes'.$i,$datos['numParticipantes'],'input-mini pagination-right participantes');
						campoTextoSimbolo('costesDirectos'.$i,'','€',formateaNumeroWeb($datos['costesDirectos']),'input-mini costeDirecto pagination-right',1);
						campoTextoSimbolo('costesIndirectos'.$i,'','€',formateaNumeroWeb($datos['costesIndirectos']),'input-mini costeIndirecto pagination-right',1);
						campoTextoSimbolo('costesOrganizacion'.$i,'','€',formateaNumeroWeb($datos['costesOrganizacion']),'input-mini costeOrganizacion pagination-right',1);
						campoTextoSimbolo('precioBonificado'.$i,'','€',formateaNumeroWeb($datos['precioBonificado']),'input-mini costeBonificado pagination-right',1);
						campoTextoSimbolo('precioFactura'.$i,'','€',formateaNumeroWeb($datos['precioFactura']),'input-mini pagination-right',1);
	echo "			</tr>";

					creaSubTablaAlumnos($codigoGrupo,$datos['codigoCliente'],$i);

	echo "

				</tbody>
			</table>
		</td>
		<td>";
			campoOculto($datos['precioBonificado'],'precioBonificadoAnterior'.$i);//Sirve para compararlo con el nuevo precio bonificado a la hora de enviar el formulario, y determinar así si hay que hacer una actualización del crédito disponible
	echo "
			<div id='plantilla$i' class='hide'>".$datos['plantilla']."</div>
			<div id='creditoDisponible$i' class='hide'>".$datos['creditoDisponible']."</div>
			<input type='checkbox' name='filasTabla[]' value='$i'>
		 </td>
	</tr>";
}


function creaSubTablaAlumnos($codigoGrupo,$codigoCliente,$i){
	$j=0;
	if($codigoCliente){
		$consulta=consultaBD("SELECT participantes.* FROM participantes INNER JOIN trabajadores_cliente ON participantes.codigoTrabajador=trabajadores_cliente.codigo WHERE codigoGrupo=$codigoGrupo AND codigoCliente=$codigoCliente");
		while($datosAlumno=mysql_fetch_assoc($consulta)){
			imprimeLineaSubtablaAlumnos($datosAlumno,$j,$i);
			$j++;
		}
	}

	if($j==0){
		imprimeLineaSubtablaAlumnos(false,$j,$i);
	}
}


function imprimeLineaSubtablaAlumnos($datos,$j,$i){
	echo "<tr id='$i-$j' class='filaSubtabla'>
			<td class='celdaInvisible'></td>
			<td colspan='7' class='celdaConTabla'>
				<table class='table tabla-simple tabla-interna subtabla'>
					<tbody>
						<tr>";
							campoSelectConsultaAjax("codigoTrabajador$i-$j",'',"SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI'",$datos['codigoTrabajador'],'trabajadores/gestion.php?codigo=','selectpicker selectAjax show-tick selectAlumno','',1,false);
							campoSelect("cofinanciacionParticipante$i-$j",'',array('','Aportación económica','Horas de formación en jornada laboral','No cofinancia'),array('','APORTACION','HORAS','NO'),$datos['cofinanciacionParticipante'],'selectpicker span3 show-tick','',1);
							campoSelect("estadoParticipante$i-$j",'',array('Activo','Baja','Anulado'),array('ACTIVO','BAJA','ANULADO'),$datos['estadoParticipante'],'selectpicker span2 show-tick','',1);
	echo "					<td class='centro'><button type='button' onclick='eliminaAlumno(\"$i-$j\")' class='btn btn-small btn-danger' title='Eliminar alumno'><i class='icon-trash'></i></button></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>";
}

function creaEmpresaParticipante(){
	$res='fallo';
	$datos=arrayFormulario();

	conexionBD();

	$consulta=consultaBD("INSERT INTO clientes(codigo,fechaAlta,nombreComercial,razonSocial,cif,domicilio,cp,localidad,codigoComercial,activo,posibleCliente) 
	VALUES(NULL,'".$datos['fechaAlta']."','".$datos['nombreComercial']."','".$datos['razonSocial']."','".$datos['cif']."','".$datos['domicilio']."','".$datos['cp']."',
	'".$datos['localidad']."','".$datos['codigoComercial']."','SI','NO');");
	
	if($consulta){
		$res=mysql_insert_id();
	}

	cierraBD();

	echo $res;
}


function creaTablaDiasImparticion($datos,$sufijoCampo=''){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Días de impartición:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table tabla-simple mitadAncho' id='tablaDiasImparticion'>
				  	<thead>
				    	<tr>
				            <th> L </th>
							<th> M </th>
							<th> X </th>
							<th> J </th>
							<th> V </th>
							<th> S </th>
							<th> D </th>
				    	</tr>
				  	</thead>
				  	<tbody>
				  		<tr>";
				  			campoCheckTabla('checkLunes'.$sufijoCampo,$datos);
				  			campoCheckTabla('checkMartes'.$sufijoCampo,$datos);
				  			campoCheckTabla('checkMiercoles'.$sufijoCampo,$datos);
				  			campoCheckTabla('checkJueves'.$sufijoCampo,$datos);
				  			campoCheckTabla('checkViernes'.$sufijoCampo,$datos);
				  			campoCheckTabla('checkSabado'.$sufijoCampo,$datos);
				  			campoCheckTabla('checkDomingo'.$sufijoCampo,$datos);
	echo "				</tr>
				  	</tbody>
				</table>
			</div>
		</div>
	</div>";
}

function creaTablaTutores($datos){
	echo "
	<div class='control-group' id='cajaTutores'>                     
		<label class='control-label'>Tutorías:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaTutores'>
				  	<thead>
				    	<tr>
				            <th> Tutor </th>
							<th> Horas tutoría</th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		conexionBD();
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM tutores_grupo WHERE codigoGrupo=".$datos['codigo']);
				  			while($datosTutoria=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaTutores($datosTutoria,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaTutores(false,$i);
				  		}
				  		cierraBD();
	echo "				  		
				  	</tbody>
				</table>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaTutores\");'><i class='icon-plus'></i> Añadir tutoría</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaTutores\");'><i class='icon-trash'></i> Eliminar tutoría</button>
			</div>
		</div>
	</div>
	<br />";
}

function imprimeLineaTablaTutores($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	campoSelectConsulta('codigoTutor'.$i,'',"SELECT codigo, CONCAT(nombre,' ',apellido1,' ',apellido2) AS texto FROM tutores WHERE activo='SI' ORDER BY nombre;",$datos['codigoTutor'],'selectpicker span5 show-tick','data-live-search="true"','',1,false);
	campoTextoTabla('horas'.$i,$datos['horas'],'input-mini pagination-right');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

function consultaDatosAccionFormativa(){
	$codigoAccion=$_POST['codigoAccion'];

	conexionBD();
	$datos=consultaBD("SELECT accionFormativa, horas, mediosFormacion, precioModulo, modalidad, privado, codigoCentroPresencial, horasPresencial, horasTeleformacion, precio FROM acciones_formativas WHERE acciones_formativas.codigo=$codigoAccion;",false,true);
	$grupo=consultaBD("SELECT MAX(grupos.grupo) AS grupo FROM grupos WHERE codigoAccionFormativa=$codigoAccion",false,true);
	$tutores=obtieneTutoresAccionFormativa($codigoAccion,$datos['modalidad']);
	$formadores=obtieneFormadoresAccionFormativa($codigoAccion,$datos['modalidad']);
	cierraBD();

	if($grupo['grupo']==NULL || $grupo['grupo']==0){
		$grupo['grupo']=date('y').'001';
	}
	else{
		$grupo['grupo']+=1;
	}

	echo $datos['accionFormativa'].'&{}&'.$datos['horas'].'&{}&'.$grupo['grupo'].'&{}&'.$datos['mediosFormacion'].'&{}&'.$datos['precioModulo'].'&{}&'.$datos['modalidad'].'&{}&'.$datos['privado'].'&{}&'.$tutores['filas'].'&{}&'.$tutores['observaciones'].'&{}&'.$datos['codigoCentroPresencial'].'&{}&'.$datos['horasPresencial'].'&{}&'.$datos['horasTeleformacion'].'&{}&'.$datos['precio'].'&{}&'.$formadores;
}

function obtieneTutoresAccionFormativa($codigoAccionFormativa,$modalidad){
	$res=array('filas'=>'','observaciones'=>'');

	if($modalidad!='PRESENCIAL'){
		$consulta=consultaBD("SELECT tutores_accion_formativa.*, observaciones FROM tutores_accion_formativa INNER JOIN tutores ON tutores_accion_formativa.codigoTutor=tutores.codigo WHERE codigoAccionFormativa=$codigoAccionFormativa");

		$i=0;
		$j=1;
		while($datos=mysql_fetch_assoc($consulta)){
			$res['filas'].="<tr>";
			$res['filas'].=campoSelectConsultaTutor('codigoTutor'.$i,$datos['codigoTutor']);
			$res['filas'].=campoTextoAjax('horas'.$i,$datos['horas']);
			$res['filas'].="<td><input type='checkbox' name='filasTabla[]' value='$j'></td></tr>";

			$res['observaciones'].=$datos['observaciones']."\n\n";

			$i++;
			$j++;
		}
	}
	else{
		$res['filas']='NO';
	}

	return $res;
}




function obtieneFormadoresAccionFormativa($codigoAccionFormativa,$modalidad){
	$res='';

	if($modalidad=='PRESENCIAL' || $modalidad=='MIXTA'){
		$consulta=consultaBD("SELECT * FROM formadores_af_presencial WHERE codigoAccionFormativa=$codigoAccionFormativa");

		$i=0;
		$j=1;
		while($datos=mysql_fetch_assoc($consulta)){
			

			$res.="<tr>";
			$res.=campoTextoAjax('nifFormador'.$i,$datos['nifFormador'],'input-small');
			$res.=campoTextoAjax('nombreFormador'.$i,$datos['nombreFormador'],'input-medium');
			$res.=campoTextoAjax('primerApellidoFormador'.$i,$datos['primerApellidoFormador'],'input-medium');
			$res.=campoTextoAjax('segundoApellidoFormador'.$i,$datos['segundoApellidoFormador'],'input-medium');	
			$res.=campoTextoAjax('telefonoFormador'.$i,$datos['telefonoFormador'],'input-small pagination-right');
			$res.=campoTextoAjax('emailFormador'.$i,$datos['emailFormador'],'input-medium');
			$res.=campoTextoAjax('horasFormador'.$i,$datos['horasFormador'],'input-mini pagination-right');
			$res.="<td><input type='checkbox' name='filasTabla[]' value='$j'></td></tr>";

			$i++;
			$j++;
		}
	}
	else{
		$res='NO';
	}

	return $res;
}


function consultaDatosCliente(){
	$codigoCliente=$_POST['codigoCliente'];
	$arrayFecha=explode('/',$_POST['fechaGrupo']);
	$anio=$arrayFecha[2];

	$datos=consultaBD("SELECT plantilla, creditoDisponible FROM creditos_cliente WHERE codigoCliente=$codigoCliente AND ejercicio='$anio';",true,true);

	echo json_encode($datos);
}

function creaAccionFormativa(){
	$res='fallo';
	$datos=arrayFormulario();

	conexionBD();

	$consulta=consultaBD("INSERT INTO acciones_formativas(codigo,codigoCentroGestor,accion,modalidad,horas,accionFormativa,proveedor) 
	VALUES(NULL,".$datos['codigoCentroGestor'].",'".$datos['accion']."','".$datos['modalidad']."','".$datos['horas']."','".$datos['accionFormativa']."',
	'".$datos['proveedor']."');");
	
	if($consulta){
		$res=mysql_insert_id();
	}

	cierraBD();

	echo $res;
}


//Función similar a campoSelectConsulta, pero simplificada para su uso únicamente con tutores y con return en lugar de echo
function campoSelectConsultaTutor($nombreCampo,$valor=false){
	$valor=compruebaValorCampo($valor,$nombreCampo);

	$res="
	<td>
		<select name='$nombreCampo' class='selectpicker span3 show-tick' id='$nombreCampo' data-live-search='true'>
			<option value='NULL'></option>";
		
		$consulta=consultaBD("SELECT codigo, CONCAT(nombre,' ',apellido1,' ',apellido2) AS texto FROM tutores WHERE activo='SI' ORDER BY nombre;");
		while($datos=mysql_fetch_assoc($consulta)){
			$res.="<option value='".$datos['codigo']."'";

			if($valor!=false && $valor==$datos['codigo']){
				$res.=" selected='selected'";
			}

			$res.=">".$datos['texto']."</option>";
		}
		
	$res.="</select>
	</td>";
	
	return $res;
}

function campoTextoAjax($nombreCampo,$valor='',$clase='input-mini pagination-right'){//MODIFICACIÓN 29/12/2014: añadido el parámetro $disabled y llamada a compruebaValorCampo()
	$valor=compruebaValorCampo($valor,$nombreCampo);

	return "<td><input type='text' class='$clase' id='$nombreCampo' name='$nombreCampo' value='$valor'></td>";
}

function compruebaNumeroGrupoYHorasTutores(){
	$res='';

	$datos=arrayFormulario();
	$tutores=quitaUltimaComa($datos['tutores'],4);
	$tutores=explode('&{}&',$tutores);
	$participantes=$datos['numParticipantes'];

	$where="WHERE (";
	$where.=generaWhereHorasTutores('grupos.fechaInicio','grupos.fechaFin',$datos['fechaInicio'],$datos['fechaFin']);
	$where=quitaUltimaComa($where,3);
	$where.=") AND (";

	$where.=generaWhereHorasTutores('horaInicioM','horaFinM',$datos['horaInicioM'],$datos['horaFinM']);
	$where.=generaWhereHorasTutores('horaInicioT','horaFinT',$datos['horaInicioT'],$datos['horaFinT']);
	$where.=generaWhereHorasTutores('horaInicioOnlineM','horaFinOnlineM',$datos['horaInicioOnlineM'],$datos['horaFinOnlineM']);
	$where.=generaWhereHorasTutores('horaInicioOnlineT','horaFinOnlineT',$datos['horaInicioOnlineT'],$datos['horaFinOnlineT']);
	$where=quitaUltimaComa($where,3);
	$where.=")";


	conexionBD();

	$res=validaNumeroGrupo($datos);

	if($res==''){//Si ha pasado la validación del grupo...
		$whereCodigo='';
		if($datos['codigo']!=''){
			$whereCodigo="AND tutores_grupo.codigoGrupo!=".$datos['codigo'];
		}

		for($i=0;$i<count($tutores);$i++){
			$codigoTutor=$tutores[$i];

			$consulta=consultaBD("SELECT COUNT(participantes.codigo) AS total FROM tutores_grupo INNER JOIN grupos ON tutores_grupo.codigoGrupo=grupos.codigo INNER JOIN participantes ON grupos.codigo=participantes.codigoGrupo $where AND estadoParticipante='ACTIVO' AND grupos.activo='SI' AND grupos.anulado='NO' AND grupos.baja='NO' AND codigoTutor='$codigoTutor' $whereCodigo",false,true);
			$totalParticipantes=$consulta['total']+$participantes;
			
			if($totalParticipantes>80){
				$res.=$codigoTutor.'&{}&';
			}
		}

		if($res!=''){
			$res=quitaUltimaComa($res,4);
		}
	}

	cierraBD();
	
	echo $res;
}

function validaNumeroGrupo($datos){
	$res='';

	if($datos['grupo']!=0){
		$whereCodigo='';
		if($datos['codigo']!=''){
			$whereCodigo="AND codigo!=".$datos['codigo'];
		}

		$consulta=consultaBD("SELECT codigo FROM grupos WHERE codigoAccionFormativa='".$datos['codigoAccionFormativa']."' AND grupo='".$datos['grupo']."' $whereCodigo");
		if(mysql_num_rows($consulta)>0){
			$res='error';
		}
	}

	return $res;
}

function generaWhereHorasTutores($campoInicio,$campoFin,$horaInicio,$horaFin){
	$res='';

	if($horaInicio!='' || $horaInicio!=$horaFin){
		$res="	($campoInicio>='$horaInicio' AND $campoInicio<'$horaFin'  AND $campoFin>='$horaFin')
				OR
				($campoInicio>='$horaInicio' AND $campoFin<='$horaFin')
				OR
				($campoInicio<='$horaInicio' AND $campoFin>='$horaFin')
				OR
				($campoInicio<='$horaInicio' AND $campoFin>'$horaInicio' AND $campoFin<='$horaFin')
				OR";
	}

	return $res;
}


function generaWhereHorasTutores2($campoInicio,$campoFin,$horaInicio,$horaFin){//EN DESARROLLO
	$arrayHoraInicio=explode(':',$horaInicio);
	$arrayHoraFin=explode(':',$horaFin);

	//Convierto la hora en un número (08:50 sería 850), y en cada iteración le sumo 5 minutos
	for($arrayHoraInicio;intval(implode($arrayHoraInicio,''))<intval(implode($arrayHoraFin,''));$arrayHoraInicio[1]+=5){
		
	}
}

/*function creaTablaAlumnos($datos){
	echo "
	<br />
	<h3 class='apartadoFormulario'>Alumnos</h3>
	<div class='table-responsive centro'>
		<table class='table table-striped tabla-simple mitadAncho' id='tablaAlumnos'>
		  	<thead>
		    	<tr>
		            <th> &nbsp; </th>
					<th> </th>
		    	</tr>
		  	</thead>
		  	<tbody>";
		  	
		  	conexionBD();

		  	$i=0;
		  	if($datos!=false){
		  		$consulta=consultaBD("SELECT * FROM participantes WHERE codigoGrupo=".$datos['codigo']);
		  		while($participante=mysql_fetch_assoc($consulta)){
		  			imprimeLineaTablaAlumnos($i,$participante);
		  			$i++;
		  		}
		  	}

		  	if($i==0){
		  		imprimeLineaTablaAlumnos($i,false);
		  	}

		  	cierraBD();

		echo "
		  	</tbody>
		</table>
		<div class='centro'>
			<button type='button' class='btn btn-small btn-success' onclick='insertaFilaAlumno();'><i class='icon-plus'></i> Añadir alumno</button> 
			<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaAlumnos\");'><i class='icon-trash'></i> Eliminar alumno</button>
		</div>
	</div>";

	//El siguiente div oculto sirve para guardar la consulta de selección de alumnos, que será obtenida y modificada en JS para filtrar por empresas.
	divOculto("SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI'",'consultaAlumnos');
}

function imprimeLineaTablaAlumnos($i,$datos){
	$j=$i+1;

	echo "	<tr>
				<td class='celdaConTabla'>
					<table class='table tabla-simple tabla-interna'>
						<tbody>
							<tr>
								<td colspan='8'>";
									campoSelectConsultaAjax('codigoTrabajador'.$i,'',"SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI';",$datos['codigoTrabajador'],'trabajadores/gestion.php?codigo=','selectpicker selectAjax ancho97 show-tick selectAlumno','',2,false);
			echo "				</td>
							</tr>
							<tr>
								<th> Cofinanciación </th>
								<th> Complemento <button type='button' class='btn btn-primary btn-small' id='crearComplemento'><i class='icon-plus-circle'></i> Crear complemento</button></th>
								<th> Observaciones </th>
								<th> Co. Enviado </th>
								<th> F. Pedido </th>
								<th> F. Envío </th>
								<th> F. Devuelto </th>
								<th> F. Complemento </th>
							</tr>
							<tr>";
								campoSelect('cofinanciacionParticipante'.$i,'',array('','Aportación económica','Horas de formación en jornada laboral','No cofinancia'),array('','APORTACION','HORAS','NO'),$datos['cofinanciacionParticipante'],'selectpicker span3 show-tick','',1);
			echo "				<td class='centro'>

									Complemento 1:<br />";
									campoSelectConsulta('codigoComplemento'.$i,'',"SELECT codigo, complemento AS texto FROM complementos WHERE activo='SI' ORDER BY complemento",$datos['codigoComplemento'],'selectpicker span3 show-tick selectComplemento','data-live-search="true"','',2,false);
									echo '<br />';
									campoTextoSimbolo('precioComplemento'.$i,'','€',formateaNumeroWeb($datos['precioComplemento']),'input-mini pagination-right',2);
									echo "<br />Complemento 2:<br />";
									campoSelectConsulta('codigoComplementoDos'.$i,'',"SELECT codigo, complemento AS texto FROM complementos WHERE activo='SI' ORDER BY complemento",$datos['codigoComplementoDos'],'selectpicker span3 show-tick selectComplemento','data-live-search="true"','',2,false);
									echo '<br />';
									campoTextoSimbolo('precioComplementoDos'.$i,'','€',formateaNumeroWeb($datos['precioComplementoDos']),'input-mini pagination-right',2);
			echo "				</td>";
									areaTextoTabla('observaciones'.$i,$datos['observaciones']);
									campoCheckTabla('complementoEnviado'.$i,$datos['complementoEnviado']);
									campoFechaTabla('fechaPedido'.$i,$datos['fechaPedido']);
									campoFechaTabla('fechaDevuelto'.$i,$datos['fechaDevuelto']);
									campoFechaTabla('fechaEnvio'.$i,$datos['fechaEnvio']);
									campoFechaTabla('fechaEntregado'.$i,$datos['fechaEntregado']);
			echo "
							</tr>
						</tbody>
					</table>
				</td>
				<td>
						<input type='checkbox' name='filasTabla[]' value='$j'>
					 </td>
			</tr>";
}*/


//La siguiente función es similar a campoSelectConsulta, solo que añade a cada option el atributo "empresa" para poder filtrar con JS los alumnos por cliente
function campoSelectAlumno($nombreCampo,$valor=false){
	$valor=compruebaValorCampo($valor,$nombreCampo);

	echo "
	<td>
		<select name='$nombreCampo' class='selectpicker span3 show-tick selectAlumno' id='$nombreCampo' data-live-search='true'>
			<option value='NULL'></option>";
		
		$consulta=consultaBD("SELECT codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2) AS texto, codigoCliente FROM trabajadores_cliente WHERE activo='SI' ORDER BY nombre;");
		
		while($datos=mysql_fetch_assoc($consulta)){
			echo "<option value='".$datos['codigo']."' empresa='".$datos['codigoCliente']."'";//Aquí esté ese atributo!

			if($valor!=false && $valor==$datos['codigo']){
				echo " selected='selected'";
			}

			echo ">".$datos['texto']."</option>";
		}
		
	echo "</select>
	</td>";
}


function consultaDatosCentro(){
	$codigoCentro=$_POST['codigoCentro'];

	$datos=consultaBD("SELECT domicilio, cp, localidad FROM centros_formacion WHERE codigo=$codigoCentro",true,true);

	echo json_encode($datos);
}




function defineHoraAltaInicio($datos){
	if(!isset($datos['codigo'])){
		echo "$('#fechaAlta').val('".date('d/m/Y')."'); $('#fechaFacturar').val('".date('d/m/Y')."');";
	}
}


function eliminaGrupoXML(){
	$codigoGrupo=$_POST['codigoGrupo'];
	$codigoXml=$_POST['codigoXml'];

	if($_POST['tipo']=='INICIO'){
		echo consultaBD("UPDATE grupos_xml_inicio SET estado='ELIMINADO' WHERE codigoGrupo=$codigoGrupo AND codigoXmlInicio=$codigoXml",true);
	}
	else{
		echo consultaBD("UPDATE grupos_xml_fin SET estado='ELIMINADO' WHERE codigoGrupo=$codigoGrupo AND codigoXmlFin=$codigoXml",true);	
	}
}



//Parte de generación de control de asistencias

function generaExcelControlAsistencia($documento,$codigoGrupo){
	$estiloBorde = array(
       'borders' => array(
            'allborders' => array(
				'style' => PHPExcel_Style_Border::BORDER_THIN,
				'color' => array('argb' => '00000000'),
			),
		)
	);
	

	$documento->setActiveSheetIndex(0);//Selección hoja
	$fila=25;

	$consulta=consultaBD("SELECT nombre, apellido1, apellido2, nif FROM trabajadores_cliente INNER JOIN participantes ON trabajadores_cliente.codigo=participantes.codigoTrabajador WHERE codigoGrupo=$codigoGrupo",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$documento->getActiveSheet()->mergeCells('B'.$fila.':C'.$fila);//Combina celdas
		$documento->getActiveSheet()->mergeCells('F'.$fila.':G'.$fila);//Combina celdas

		$documento->getActiveSheet()->SetCellValue('B'.$fila,$datos['apellido1'].' '.$datos['apellido2']);
		$documento->getActiveSheet()->SetCellValue('D'.$fila,$datos['nombre']);
		$documento->getActiveSheet()->SetCellValue('E'.$fila,$datos['nif']);

		$documento->getActiveSheet()->getStyle('B'.$fila)->applyFromArray($estiloBorde);
		$documento->getActiveSheet()->getStyle('C'.$fila)->applyFromArray($estiloBorde);
		$documento->getActiveSheet()->getStyle('D'.$fila)->applyFromArray($estiloBorde);
		$documento->getActiveSheet()->getStyle('E'.$fila)->applyFromArray($estiloBorde);
		$documento->getActiveSheet()->getStyle('F'.$fila)->applyFromArray($estiloBorde);
		$documento->getActiveSheet()->getStyle('G'.$fila)->applyFromArray($estiloBorde);
		$documento->getActiveSheet()->getStyle('H'.$fila)->applyFromArray($estiloBorde);

		$fila++;
	}

	if($fila<35){//En la fila 34 acaban los recuadros por defecto de los alumnos
		$fila=35;
	}
	
	$fila++;
	$filaFin=$fila+2;
	$documento->getActiveSheet()->mergeCells('B'.$fila.':H'.$filaFin);
	$documento->getActiveSheet()->SetCellValue('B'.$fila,'OBSERVACIONES GENERALES:');
	$documento->getActiveSheet()->duplicateStyle($documento->getActiveSheet()->getStyle('L18'), 'B'.$fila);//Duplica los estilos de una celda auxiliar para que el cuadro de observaciones se muestre con el formato correcto

	$objWriter=new PHPExcel_Writer_Excel2007($documento);
	$objWriter->save('../documentos/inicios-grupos/Control-asistencia.xlsx');
}

//Fin parte de generación de control de asistencias



function obtieneObservacionesTutores(){
	$res='';
	$codigosTutores='';
	$datos=arrayFormulario();

	foreach($datos['codigosTutores'] as $codigoTutor){
		$codigosTutores.=$codigoTutor.", ";
	}

	$codigosTutores=quitaUltimaComa($codigosTutores);

	$consulta=consultaBD("SELECT observaciones FROM tutores WHERE codigo IN($codigosTutores);",true);
	while($tutor=mysql_fetch_assoc($consulta)){
		$res.=$tutor['observaciones']."\n\n";
	}

	echo $res;
}


function creaTablaFormadores($datos){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'style='text-align:left'>Formador/es:</label>
	    <div class='controls' style='margin-left:0px'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaFormadores'>
				  	<thead>
				    	<tr>
				            <th> NIF</th>
							<th> Nombre</th>
							<th> 1º Apellido </th>
							<th> 2º Apellido </th>							
							<th> Teléfono </th>
							<th> eMail </th>
							<th> Horas imp. </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		conexionBD();
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM formadores_grupo WHERE codigoGrupo=".$datos['codigo']);
				  			while($datosFormador=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaFormadores($datosFormador,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaFormadores(false,$i);
				  		}
				  		cierraBD();
	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaFormadores\");'><i class='icon-plus'></i> Añadir formador</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaFormadores\");'><i class='icon-trash'></i> Eliminar formador</button>
				</div>
			</div>
		</div>
	</div>
	<br />";
}

function imprimeLineaTablaFormadores($datos,$i){
	$j=$i+1;

	echo "<tr>";

	campoTextoTabla('nifFormador'.$i,$datos['nifFormador'],'input-small');
	campoTextoTabla('nombreFormador'.$i,$datos['nombreFormador'],'input-medium');
	campoTextoTabla('primerApellidoFormador'.$i,$datos['primerApellidoFormador']);
	campoTextoTabla('segundoApellidoFormador'.$i,$datos['segundoApellidoFormador']);	
	campoTextoTabla('telefonoFormador'.$i,$datos['telefonoFormador'],'input-small pagination-right');
	campoTextoTabla('emailFormador'.$i,$datos['emailFormador'],'input-medium');
	campoTextoTabla('horasFormador'.$i,$datos['horasFormador'],'input-mini pagination-right');
	
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

//Fin parte de grupos
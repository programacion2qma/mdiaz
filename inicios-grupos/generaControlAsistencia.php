<?php

include_once('funciones.php');
compruebaSesion();

require_once('../../api/phpexcel/PHPExcel.php');
require_once('../../api/phpexcel/PHPExcel/Reader/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/Writer/Excel2007.php');
require_once('../../api/phpexcel/PHPExcel/IOFactory.php');

$objReader=new PHPExcel_Reader_Excel2007();
$documento=$objReader->load("../documentos/inicios-grupos/plantilla.xlsx");

generaExcelControlAsistencia($documento,$_GET['codigo']);

// Definir headers
header("Content-Type: application/ms-xlsx");
header("Content-Disposition: attachment; filename=Control-asistencia.xlsx");
header("Content-Transfer-Encoding: binary");

// Descargar archivo
readfile('../documentos/inicios-grupos/Control-asistencia.xlsx');
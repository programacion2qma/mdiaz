<?php

// Para ejecutar en la raíz
// Borra de la Base de Datos las tareas duplicadas generadas por errores al confirmar ventas.

@include_once('config.php');//Carga del archivo de configuración
@include_once('../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('funciones.php');//Carga de las funciones globales

$tareasNombres = [0 => "Recordatorio para la realización o actualización anual de las Relaciones de Negocio o introducción de clientes. / Pedir Pymes.", 
                  1 => "Comprobar implantación de Prevención del blanqueo de capitales, recordar realización de formación e informes de riesgo. / Pedir Pymes.",                 
                  2 => "Gestión de la renovación anual de su espacio web para la PBLC / Comprobar implantación de LOPD y reflejar posibles cambios respecto al organigrama / Auditar LOPD."
                 ];
              
conexionBD();
for($i = 0; $i < 3; $i++){
    consultaBD("DELETE FROM tareas WHERE tarea = '".$tareasNombres[$i]."' AND codigoCliente IS NULL;");
    $tareasRepetidas = consultaBD("SELECT t.codigoCliente, count(*) AS numero FROM tareas t WHERE t.tarea = '".$tareasNombres[$i]."' GROUP BY t.codigoCliente ;");    
    $clientes = array();
    while($item = mysql_fetch_assoc($tareasRepetidas)){
        if($item['numero'] > 1) {
            array_push($clientes, $item['codigoCliente']);            
        }   
    }
    foreach($clientes as $cliente){            
        $tareaMasReciente = consultaBD("SELECT codigo, fechaInicio, fechaFin FROM tareas WHERE tarea = '".$tareasNombres[$i]."' AND codigoCliente = ".$cliente." ORDER BY codigo DESC LIMIT 1;", false, true);
        $tareasCliente = consultaBD("SELECT codigo, fechaInicio, fechaFin FROM tareas WHERE tarea = '".$tareasNombres[$i]."' AND codigoCliente = ".$cliente.";");        
        while($tarea = mysql_fetch_assoc($tareasCliente)){
            if($tarea['codigo'] != $tareaMasReciente['codigo'] && $tarea['fechaInicio'] == $tareaMasReciente['fechaInicio'] && $tarea['fechaFin'] == $tareaMasReciente['fechaFin']){
                consultaBD("DELETE FROM tareas WHERE codigo = ".$tarea['codigo'].";");
            }
        }
    }    
}
cierraBD();

?>
<?php

include_once('../la-academia-empresas-pruebas/config.php');
include_once('../api/nucleo.php');//Carga del núcleo de funciones comunes

//include_once('../config.php');
//include_once('../../api/nucleo.php');


	//global $_CONFIG;

	conexionBD();
	$cliente=consultaBD("SELECT * FROM clientes WHERE md5(codigo)='".$_GET['idencrp']."'",false,true);
	//$cliente=consultaBD("SELECT * FROM clientes WHERE codigo='3'",false,true);
	$servicio=consultaBD("SELECT * FROM servicios WHERE referencia LIKE 'ALÉRGENOS'",false,true);
	$trabajo=consultaBD("SELECT * FROM trabajos WHERE codigoCliente=".$cliente['codigo']." AND codigoServicio=".$servicio['codigo'],false,true);
	$platos=consultaBD("SELECT * FROM platos_alergenos WHERE codigoTrabajo=".$trabajo['codigo']);
	cierraBD();

	if($cliente['ficheroLogo'] != '' && $cliente['ficheroLogo'] != 'NO'){
		$logo="<img src='../la-academia-empresas-pruebas/documentos/logos-clientes/".$cliente['ficheroLogo']."' alt='LOGO'>";
	} else {
		$logo='NO HAY LOGO SUBIDO';
	}

	$contenido = "
	<style type='text/css'>
	<!--
		*{
			color:#333;
			font-family: Arial;
			line-height:20px;
		}

		table{
			width:100%;
			border:1px solid #428bca;
			border-spacing: 0;
			border-collapse: collapse;
		}

		table th{
			font-weight:bold;
			border:1px solid #2E3D64;
			padding-top:2px;
			padding-left:5px;
			padding-right:5px;
		}

		table td{
			border:1px solid #2E3D64;
			padding-top:2px;
			padding-left:5px;
			padding-right:5px;
		}

		cabecera2 table,
		cabecera2 table td{
			border:0px;
		}

		table.leyenda,
		table.leyenda td{
			border:0px;
			font-size:10px;
			width:90%;
			padding:3px;
		}

		table.leyenda img,
		table.tablaAlergenos img
		.ficha img{
			width:15px;
		}

		table.tablaAlergenos{
			border:0px
		}

		table.tablaAlergenos td{
			border:0px solid #2E3D64;
			border-top:3px solid #FFF;
			border-bottom:3px solid #FFF;
			padding-top:2px;
			padding-left:5px;
			padding-right:5px;
		}

		table.tablaAlergenos td{
			background-color:#EEE;
		}

		table.tablaAlergenos .even td{
			background-color:#DDD;
		}

		table.leyenda .altramuz{
			color:#FADD3D;
		}

		table.leyenda .apio{
			color:#54BF36;
		}

		table.leyenda .cacahuetes{
			color:#E08F65;
		}

		table.leyenda .cereales{
			color:#F47039;
		}

		table.leyenda .crustaceos{
			color:#1CB6F1;
		}

		table.leyenda .frutossecos{
			color:#DA4752;
		}

		table.leyenda .huevos{
			color:#F69035;
		}

		table.leyenda .lacteos{
			color:#6D351E;
		}

		table.leyenda .molusco{
			color:#4DC4D5;
		}

		table.leyenda .mostaza{
			color:#C09328;
		}

		table.leyenda .pescado{
			color:#28429D;
		}

		table.leyenda .sesamo{
			color:#9A8E6C;
		}

		table.leyenda .soja{
			color:#02A75B;
		}

		table.leyenda .sulfitos{
			color:#83114F;
		}

		.cabecera img,
		.cabecera2 img{
			width:50%;
		}

		.a1{
			width:1%;
		}

		.a8{
			font-weight:bold;
			width:8%;
		}

		.a12{
			width:12%;
		}

		.a100{
			width:100%;
		}

		.a10{
			width:10%;
		}

		.a25{
			width:25%;
		}

		.a40{
			width:40%;
		}

		.a33{
			width:33%;
		}

		.a66{
			width:66%;
		}

		.a30{
			width:30%;
		}

		.a70{
			width:70%;
		}

		.centro{
			text-align:center;
		}


	-->
	</style>
		<page footer='page' backbottom='10mm'>
		<div class='cabecera2'>
			<table>
				<tbody>
					<tr>
						<td class='a66'><b>".$cliente['razonSocial']."</b></td>
						<td class='a33' style='text-align:center;'>".$logo."</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/><br/>
		<div align='center'>
			<b>PLAN DE GESTIÓN DE ALÉRGENOS <br/><br/>
			ANEXO II<br/><br/>
			Listado de productos con identificación de las sustancias que producen alergias e intolerancias alimentarias*
			recogidas en el Reglamento (UE) nº 1169/2011.</b><br/>
			<i>The list below contanis allergy and intolerance food information under the EU Food Information for Consumers Regulation N.1169/2011.</i><br/><br/>
		</div>
		<br/><br/>
		<table class='tablaAlergenos'>
			<tbody>";
		$clase='';
		$fichas=array();
		$indiceFichas=0;
		$numeroPlatos=0;
		while($plato=mysql_fetch_assoc($platos)){
			$numeroPlatos++;
			if($numeroPlatos==26){
				$numeroPlatos=0;
				$contenido.="</tbody>
		</table>
		".pieAlergenos()."
	</page>
	<page footer='page' backbottom='10mm'>
	<table class='tablaAlergenos'><tbody>";
			}
			$iconos=array('1'=>'altramuz','3'=>'apio','4'=>'cacahuetes','5'=>'cereales','6'=>'crustaceos','7'=>'sulfitos','8'=>'frutossecos','9'=>'huevos','10'=>'lacteos','11'=>'molusco','12'=>'mostaza','13'=>'pescado','14'=>'sesamo','15'=>'soja');
			$grupoAlergenos=consultaBD('SELECT * FROM platos_alergenos_grupos WHERE codigoPlato='.$plato['codigo'],true);
			$alergenos='';
			while($grupo=mysql_fetch_assoc($grupoAlergenos)){
				$alergenos.="<img width='15' src='../la-academia-empresas-pruebas/img/alergenos/".$iconos[$grupo['codigoGrupo']].".png'>";
			}
			$fichas[$indiceFichas]=creaPaginaAlergeno($cliente,$logo,$plato,$alergenos);
			$indiceFichas++;
			$contenido.= "<tr class=".$clase.">
					<td class='a30' style='text-align:right;'>".$alergenos."</td>
					<td class='a70'>".$plato['nombrePlato']."</td>
				</tr>";
			if($clase==''){
				$clase='even';
			} else{
				$clase='';
			}
		}		
$contenido.="</tbody>
		</table>
		".pieAlergenos()."
	</page>";

	foreach ($fichas as $ficha) {
		$contenido.=$ficha;
	}


    $tipo=dispositivo();
	require_once('../api/html2pdf/html2pdf.class.php');
	//require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);

	switch($tipo){
		case 0:
			$html2pdf->Output('Platos.pdf');
			break;
		case 1:
			$html2pdf->Output('Platos.pdf','f');

	header("Content-Type: application/pdf");
	header("Content-Disposition: attachment; filename=Platos.pdf");
	header("Content-Transfer-Encoding: binary");

	// Descargar archivo
	readfile('Platos.pdf');
break;
}

function creaPaginaAlergeno($cliente,$logo,$plato,$iconos){
	$alergenos=consultaBD("SELECT * FROM platos_alergenos_grupos WHERE codigoPlato=".$plato['codigo'],true);
	$listadoAlergenos='';
	while($alergeno=mysql_fetch_assoc($alergenos)){
		$alergeno=explode('&$&', $alergeno['alergenos']);
		for($i=0;$i<count($alergeno);$i++){
			$nombre=datosRegistro('alergenos',$alergeno[$i]);
			$listadoAlergenos.='<li>'.$nombre['nombre'].'</li>';
		}
	}
	$grupos=consultaBD("SELECT DISTINCT g.nombre FROM platos_alergenos_grupos p INNER JOIN grupo_alergenos g ON p.codigoGrupo=g.codigo WHERE codigoPlato=".$plato['codigo'],true);
	$listadoGrupos='';
	while($grupo=mysql_fetch_assoc($grupos)){
		$listadoGrupos.='<li>'.$grupo['nombre'].'</li>';
	}
	return "<page footer='page' backbottom='10mm'>
		<div class='cabecera2' id='platoUno'>
			<table>
				<tbody>
					<tr>
						<td class='a66'><b>".$cliente['razonSocial']."</b></td>
						<td class='a33' style='text-align:center;'>".$logo."</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/><br/>
		<div align='center'>
			<b>FICHA INFORMATIVA</b><br/><br/>
			Los ingredientes identificados en <span style='color:red;'>ROJO</span>  pueden producir alergias /intolerancias alimentarias. Información de obligada declaración por el Reglamento (UE) Nº 1169/2011.<br/><br/>

			<i>Specific allergen information. Allergens are identified in <span style='color:red;'>RED</span> color. Information offered to comply with Regulation N.1169/2011.</i><br/><br/>
		</div>
		<div class='ficha'>
		<b>".$plato['nombrePlato']."</b>  ".$iconos."<br/><br/>
		ALÉRGENOS / ALLERGENS<br/>
		<ul>
			".$listadoAlergenos."
		</ul>
		<br/>
		Este plato/producto contiene sustancias que pueden producir alergias e intolerancias clasificadas en los siguientes grupos:<br/><br/>

		<i>This food contains or uses ingredients or processing aids derived from allergen products which are listed below:</i>
		<ul>
			".$listadoGrupos."
		</ul>
		<br/>
		Nuestros procesos de elaboración de platos/productos y de selección de proveedores no contemplan medidas específicas que eviten la presencia de alguna otra sustancia alérgena, por lo que podrían estar presentes (incluso en cantidades mínimas) en este plato/producto.<br/><br/>

		<i>Our elaboration and providers selection processes don´t consider specific measures in order to avoid the presence of any other allergen in this food (even traces or small quantities).</i>

		</div>
		".pieAlergenos()."
	</page>";

}

function pieAlergenos(){
	return "<page_footer>
			<table class='leyenda'>
				<tr>
					<td class='a8'>LEYENDA:</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/altramuz.png'></td>
					<td class='a12 altramuz'>Altramuz</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/apio.png'></td>
					<td class='a12 apio'>Apio</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/cacahuetes.png'></td>
					<td class='a12 cacahuetes'>Cacahuete</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/cereales.png'></td>
					<td class='a12 cereales'>Cereales</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/crustaceos.png'></td>
					<td class='a12 crustaceos'>Crustaceos</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/frutossecos.png'></td>
					<td class='a12 frutossecos'>Frutos secos</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/huevos.png'></td>
					<td class='a12 huevos'>Huevos</td>
				</tr>
				<tr>
					<td class='a8'>KEY:</td>
					<td class='a1'></td>
					<td class='a12 altramuz'>Lupin</td>
					<td class='a1'></td>
					<td class='a12 apio'>Celery</td>
					<td class='a1'></td>
					<td class='a12 cacahuetes'>Peanuts</td>
					<td class='a1'></td>
					<td class='a12 cereales'>Cereals</td>
					<td class='a1'></td>
					<td class='a12 crustaceos'>Crustaceans</td>
					<td class='a1'></td>
					<td class='a12 frutossecos'>Nuts</td>
					<td class='a1'></td>
					<td class='a12 huevos'>Eggs</td>
				</tr>
				<tr>
					<td class='a8'></td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/lacteos.png'></td>
					<td class='a12 lacteos'>Lacteos</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/molusco.png'></td>
					<td class='a12 molusco'>Molusco</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/mostaza.png'></td>
					<td class='a12 mostaza'>Mostaza</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/pescado.png'></td>
					<td class='a12 pescado'>Pescado</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/sesamo.png'></td>
					<td class='a12 sesamo'>Sésamo</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/soja.png'></td>
					<td class='a12 soja'>Soja</td>
					<td class='a1'><img src='../la-academia-empresas-pruebas/img/alergenos/sulfitos.png'></td>
					<td class='a12 sulfitos'>Sulfito</td>
				</tr>
				<tr>
					<td class='a8'></td>
					<td class='a1'></td>
					<td class='a12 lacteos'>Milk</td>
					<td class='a1'></td>
					<td class='a12 mosluco'>Molluscs</td>
					<td class='a1'></td>
					<td class='a12 mostaza'>Mustard</td>
					<td class='a1'></td>
					<td class='a12 pescado'>Fish</td>
					<td class='a1'></td>
					<td class='a12 sesamo'>Sesame</td>
					<td class='a1'></td>
					<td class='a12 soja'>Soya</td>
					<td class='a1'></td>
					<td class='a12 sulfitos'>Sulphites</td>
				</tr>
			</table>
	    </page_footer>";
}

  function dispositivo(){
$tablet_browser = 0;
$mobile_browser = 0;
$body_class = 'desktop';
$tipo=0;

if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
$tablet_browser++;
$body_class = "tablet";
}

if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
$mobile_browser++;
$body_class = "mobile";
}

if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
$mobile_browser++;
$body_class = "mobile";
}

$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
$mobile_agents = array(
'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
'newt','noki','palm','pana','pant','phil','play','port','prox',
'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
'wapr','webc','winw','winw','xda ','xda-');

if (in_array($mobile_ua,$mobile_agents)) {
$mobile_browser++;
}

if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
$mobile_browser++;
$stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
 $tablet_browser++;
}
}
if ($tablet_browser > 0) {
  $tipo=1;
}
else if ($mobile_browser > 0) {
  $tipo=1;
}
else {
  $tipo=0;
} 
return $tipo;
}
?>
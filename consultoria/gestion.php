<?php
  $seccionActiva=10;
  include_once("../cabecera.php");
  if(isset($_GET['gratis']) || isset($_GET['codigoTrabajo']) || isset($_POST['fecha'])){
  	gestionConsultoria();
  } else {
  	gestionTrabajos();
  }
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/filasTablaAlergenos.js" type="text/javascript"></script>

<script src='../../api/js/firma/jquery.signaturepad.js'></script>
<script src='../../api/js/firma/assets/json2.min.js'></script>

<script src="../js/selectAjax.js" type="text/javascript"></script>

<script type="text/javascript">
var nombreFichero='';

$(document).on('change','.tipoDPD',function(){
	muestraEncargado($(this));
});
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');
   		//$(this).focus();
	});
	$('#fecha').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');modificarFechas($(this));});
	$('#fechaRevision').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');modificarFechaPrevista($(this));});

	$("#tablaUsuariosLOPD td:nth-child(6) .datepicker").datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');cambioFechaBaja($(this));});
	
	recorrerRadios();
	inicializaPopOvers();

	$("input[type='radio']").change(function(){
		compruebaRadio($(this),true);
	});

	mostrarIncidencia($("input[name='incidencia']:checked"));
	$("input[name='incidencia']").change(function(){
		mostrarIncidencia($(this));
	});

	if ($("#gratuita").length > 0 && $("#codigo").length == 0) {
		modificarFechas($('#fecha'));
	}
	mostrarProcedenciaAgua($("#procedenciaAgua input[type=radio]:checked").val());
	$('#procedenciaAgua input[type=radio]').change(function(){
		mostrarProcedenciaAgua($(this).val());
	})

	$(".grupoAlergenos").change(function(){
		obtieneAlergenos($(this));
	});

	$('#nuevoFichero').click(function(){
		insertaFichero();
	});

	$('#nuevoEncargado').click(function(){
		insertaEncargado();
	});

	$('.divFicheros input[type=radio]').change(function(){
		selectFicheros($(this).attr('name'),1,$(this).val());
	});
	$('.divOtroFicheros input[type=radio]').change(function(){
		selectFicheros($(this).attr('name'),2,$(this).val());
	});
	$('.nombreNuevoFichero').focus(function(){
		nombreFichero=$(this).val();
	})
	$('.nombreNuevoFichero').focusout(function(){
		$("select.multipleFicheros option[value='"+nombreFichero+"']").text($(this).val());
		$("select.multipleFicheros option[value='"+nombreFichero+"']").val($(this).val());
		$('select.multipleFicheros').selectpicker('refresh');
	})
	bloquearCampos();
	//inicializaFirma();

	/*$('.tipoDPD').change(function(){
		muestraEncargado($(this));
	});*/

	marcarPestania();
	$('.nav-tabs li a').click(function(){
		var pestana=$(this).attr('href').replace('#','');
		$('#pestanaActiva').val(pestana);
	});

	$("select.multipleFicheros.ENCARGADO").each(function( index ) {
		mostrarDivs($(this));
		$($(this)).change(function(){
			if($(this).hasClass('FIJO')){
				var n=$(this).parent().parent().parent().parent().attr('id').match(/(\D+)(\d*)$/);
				n=n[2].substring(1)
			} else {
				var n=$(this).parent().parent().parent().parent().parent().attr('id');
				if(n==undefined){
					n=0;
				} else {
					n=n.match(/(\D+)(\d*)$/);
					n=n[2];
				}
			}	
			mostrarDivs($(this),n);
		});
	});

	compruebaDelegaciones();
	$("select.selectDelegaciones").each(function( index ) {
		$(this).change(function(){
			compruebaDelegaciones();
		});
	});

	compruebaSubcontrataciones()
	$(".radioSubcontratacionFijo,.radioSubcontratacion").each(function( index ) {
		$(this).change(function(){
			compruebaSubcontrataciones();
		});
	});

	$("#tablaUsuariosLOPD td:nth-child(6) input").each(function( index ) {
  		cambioFechaBaja($(this));
	});
});

function cambioFechaBaja(elem){
	var i=obtieneFilaCampo(elem);
	var valor=elem.val();
	if(valor!=''){
		$('#accesoUsuarioLOPD'+i).val('N').selectpicker('refresh');
	}
}

function insertaFilaDPD(tabla){
	insertaFila(tabla);
	var div = $('#'+tabla).find("tbody tr:last .divEncargadoDPD");
	div.attr('id',function(){
		var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
	});
	div.addClass('hide');
}

function muestraEncargado(elem){
	var i = obtieneFilaCampo(elem);
	var valor = elem.val();
	if(valor == 2){
		$('#divEncargadoDPD'+i).removeClass('hide');
	} else {
		$('#divEncargadoDPD'+i).addClass('hide');
	}
}
function insertaFichero(){
	var clon=$( ".spanOtroFichero:last" ).clone();
	var name=clon.find('.divFicheroOculto').attr("id").split('_');
	name=name[0]+'_'+ ++name[1];
	var name=clon.find('.divFicheroOculto').attr("id",name);
	clon.find('input[type=text]').val('');
	clon.find('input[type=radio]:[value=NO]').prop('checked',true);
	clon.find("select").val("NULL");
    clon.find('.bootstrap-select').remove();//Eliminación de los selectpicker
	clon.find('input:not(.input-block-level),select').each(function() {
  		name=$(this).attr("name").split('_');
  		name=name[0]+'_'+ ++name[1];
  		$(this).attr("name",name);
  		$(this).attr("id",name);
	});

	$('.spanOtroFichero:last').after(clon);
	recorrerRadios();
	$("input[type='radio']").change(function(){
		compruebaRadio($(this));
	});
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.divOtroFicheros input[type=radio]').change(function(){
		selectFicheros($(this).attr('name'),2,$(this).val());
	});
	$('.nombreNuevoFichero').focus(function(){
		nombreFichero=$(this).val();
	})
	$('.nombreNuevoFichero').focusout(function(){
		$("select.multipleFicheros option[value='"+nombreFichero+"']").text($(this).val());
		$("select.multipleFicheros option[value='"+nombreFichero+"']").val($(this).val());
		$('select.multipleFicheros').selectpicker('refresh');
	})
	$('select.selectpicker').selectpicker();
}

function insertaEncargado(){
	var clon=$( ".spanOtroEncargado:last" ).clone();
	clon.find('input[type=text]').val('');
	clon.find('input[type=radio]:[value=NO]').prop('checked',true);
	clon.find("select").val("NULL");
    clon.find('.bootstrap-select').remove();//Eliminación de los selectpicker
	clon.find('input:not(.input-block-level,[type=checkbox]),select').each(function() {
  		var name=$(this).attr("name").split('_');
  		if(name[1].includes('[]')){
  			name[1]=name[1].replace('[]','');
  			id=name[0]+'_'+ ++name[1];
  			name=id+'[]';
  		} else {
  			id=name[0]+'_'+ ++name[1];
  			name=id;
  		}
  		$(this).attr("name",name);
  		$(this).attr("id",id);
	});
	clon.find('input:[type=checkbox]').each(function() {
  		var name=$(this).attr("name").split('_');
  		id=name[0]+'_'+name[1]+'_'+ ++name[2];
  		name=id;
  		$(this).attr("name",name);
  		$(this).attr("id",id);
	});
	clon.find('.divTratamientos').attr('id',function(){
		var id=$(this).attr('id').split('_');
		return id[0]+'_'+id[1]+'_'+ ++id[2];
	}).addClass('hide');
	clon.find("select.multipleFicheros.ENCARGADO").change(function(){
		mostrarDivs($(this));
	});

	clon.find('.radioSubcontratacion').attr('id',function(){
		var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
	});

	clon.find('.divSubcontrata').attr('id',function(){
		var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
	}).addClass('hide');

	var tabla=clon.find('.divSubcontrata').find('table');
	tabla.attr('id',function(){
		var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
	});
	tabla.find('input:not(.input-block-level,[type=checkbox]),select').attr('name',function(){
		var parts = this.name.split('_');
		var parts = parts[0].match(/(\D+)(\d*)$/);
        name=parts[1] + ++parts[2]+'_0';
        return name;
	}).attr('id',function(){
        return this.name;
	});
	tabla.find('tbody tr:not(:last)').remove();
	clon.find('.btn-success').attr('onclick',function(){
		var partes=$(this).attr('onclick').split('"');
		return partes[0]+'"'+tabla.attr('id')+'"'+partes[2];
	});
	clon.find('.btn-danger').attr('onclick',function(){
		var partes=$(this).attr('onclick').split('"');
		return partes[0]+'"'+tabla.attr('id')+'"'+partes[2];
	});
	$('.spanOtroEncargado:last').after('<br clear="all"><h3 class="apartadoFormulario borderBottom"></h3>');
	$('.spanOtroEncargado:last').next().next().after(clon);
	$('select.selectpicker').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$(".spanOtroEncargado:last .radioSubcontratacion").each(function( index ) {
		$(this).change(function(){
			compruebaSubcontrataciones();
		});
	});

}

function mostrarProcedenciaAgua(valor){
	if(valor == 2){
		$('#divAgua2').removeClass('hidden');
		$('#divAgua3').addClass('hidden');
	} else if (valor == 3){
		$('#divAgua2').addClass('hidden');
		$('#divAgua3').removeClass('hidden');
	} else if (valor == 1){
		$('#divAgua2').addClass('hidden');
		$('#divAgua3').addClass('hidden');
	}
}

function inicializaFirma(){
	$('.output').each(function(){
		var id=$(this).attr('id');
		var firma = $(this).val().trim(); 
		if(firma==''){//inicialización
			$('.'+id).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false});
		}
		else{
			$('.'+id).signaturePad({drawOnly:true,lineTop:'500',penColour:'000000',validateFields:false}).regenerate(firma.toLowerCase());
		}
	});
}

function modificarFechas(elem){
	obtenerFecha('#fechaPrevista',elem.val(),'P31D');
	obtenerFecha('#fechaTomaDatos',elem.val(),'P14D');
	obtenerFecha('#fechaRevision',elem.val(),'P21D');
	obtenerFecha('#fechaEmision',elem.val(),'P21D');
	obtenerFecha('#fechaEnvio',elem.val(),'P28D');
	obtenerFecha('#fechaEntrega',elem.val(),'P28D');
	obtenerFecha('#fechaMantenimiento',elem.val(),'P11M');
}

function modificarFechaPrevista(elem){
	var gratuita = $('#gratuita').val();
	if(gratuita == 'SI'){
		obtenerFecha('#fechaPrevista',elem.val(),'P10D');
	} else {
		obtenerFecha('#fechaPrevista',elem.val(),'P30D');
	}
}

function mostrarIncidencia(elem){
	if(elem.val() == 'SI'){
		$('#divIncidencia').removeClass('hidden');
	} else {
		$('#divIncidencia').addClass('hidden');
	}
}

function obtenerFecha(campo,fecha,suma){
	var consulta=$.post('obtenerFecha.php?',{'fecha':fecha,'suma':suma});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al obtener la fecha límite.\nInténtelo de nuevo en unos segundos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			$(campo).datepicker('setValue', respuesta)
		}
	});
}

function recorrerRadios(){
	$("input[type='radio']:checked").each(function (){
		compruebaRadio($(this));
	});
}

function compruebaRadio(elem,change=false){
	var val = $(elem).val();
  	var id = '#div'+obtieneID($(elem).attr('name'));
  	if ($(id).length) {
  		mostrarCampos(val,id,change);
  	}
}

function obtieneID(id){
	return id.replace('pregunta','');
}

function mostrarCampos(val,div,change=false){
	if($(div).attr('metodo') == 'inverso'){
		if(val == 'NO'){
			$(div).removeClass('hidden');
			$(div+'_1').removeClass('hidden');
		} else {
			$(div).addClass('hidden');
			$(div+'_1').addClass('hidden');
			
		}
	} else {
		if(val == 'SI'){
			$(div).removeClass('hidden');
			$(div+'_1').removeClass('hidden');
		} else {
			$(div).addClass('hidden');
			$(div+'_1').addClass('hidden');
		}
	}
	if(change){
		limpiarDatosCampos(div);
	}
}

function limpiarDatosCampos(div){
	$(div).find('input:not([type=radio])').val('');
	$(div).find('select').val('');
	$(div).find('select').selectpicker('refresh');
	$(div+'_').find('input:not([type=radio]').val('');
}

function obtieneAlergenos(grupo){
	var id=grupo.attr('id').split('A');
	id='#a'+id[1];
	var consulta=$.post('obtieneAlergenos.php?',{'grupo':grupo.val()});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al obtener los alérgenos de este grupo.\nInténtelo de nuevo en unos segundos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			$(id).html(respuesta);
			$(id).selectpicker('refresh');
		}
	});
}

function selectFicheros(name, tipo=1, val=''){
	if(tipo==1){
		var fichero=name.substr(9,name.length-1);
		var nombres = {
    		20: 'CLIENTES',
    		31: 'PROVEEDORES',
    		42: 'EMPLEADOS',
    		53: 'CURRÍCULUMS',
    		64: 'VIDEOVIGILANCIA',
    		75: 'USUARIOS WEB',
    		443: 'PACIENTES',
    		454: 'SOCIOS',
    		465: 'ALUMNOS',
    		476: 'VOLUNTARIOS'
		};
		name=nombres[fichero];
	} else {
		name=name.replace('otro','nombre');
		name=$('#'+name).val();
	}
	
	if(val=='SI'){
		$('select.multipleFicheros').append('<option value="'+name+'">'+name+'</option>');
	} else {
		$("select.multipleFicheros option[value='"+name+"']").remove();
	}
	$('select.multipleFicheros').selectpicker('refresh');
}

function bloquearCampos(){

	$(".seccionBloqueada").each(function( index ) {
		$(this).find("input,select,textarea").attr('disabled','disabled');
	});
	$(".seccionBloqueada").each(function( index ) {
		$(this).find(".btn-success,.btn-propio:not(.descargaFichero)").css('display','none');
	});

	$(".formVersion").each(function( index ) {
		$(this).find("input,select,textarea").attr('disabled','disabled');
		$(this).find(".btn-success,.btn-propio:not(.descargaFichero),.form-actions").css('display','none');
	});
}

function marcarPestania(){
  	var pagina=$('#pestanaActiva').val();
  	if(pagina!=undefined){
  		$('.nav-tabs li').removeClass('active');
  		$('.nav-tabs li:nth-child('+pagina+')').addClass('active');
  	}
}

function inicializaPopOvers(){
      $('.enlacePopOver').each(function(){
      	$(this).unbind();
        $(this).popover({
          title: 'Dato actual: <div class="pull-right"><button type="button" class="btn btn-small btn-default cerrar"><i class="icon-remove"></i></button>',
          content: $(this).next().html().replace(/[\r\n|\n]/g,'<br />'),
          placement:$(this).attr('placement'),
          trigger:'manual'
        });

        $(this).click(function(e){
          e.preventDefault();
          $('.enlacePopOver').not($(this)).popover('hide');
          $(this).popover('show');
          oyenteCerrar();
        });
      });
    }


    function oyenteCerrar(){
      $('.cerrar').click(function(){
        $('.enlacePopOver').popover('hide');
      });
    }

    function insertaFilaUsuariosLOPD(tabla){
    	insertaFila(tabla);
    	/*var tr = $('#'+tabla).find("tbody tr:last").find('.selectMultiple').attr('name',function(){
    		var name = this.name;
    		return name+'[]';
    	});*/
    }

function mostrarDivs(elem,n=0){
	var valor=elem.find('option:selected').val();
	var id = elem.attr('id');
	if(elem.hasClass('FIJO')){
		var tipo='FIJO';
		id=id.substring(id.length - 3, id.length);
	} else {
		var tipo='NOFIJO';
		id=id.split('_');
		id=id[1];	
	}
	id='#divTratamientos_'+tipo+'_'+id;
	if(valor==undefined){
		$(id).addClass('hide');
		if(n!=0){
			console.log(valor);
			marcarTratamientos(n,'NO',tipo);
		}
	} else {
		$(id).removeClass('hide');
		if(n!=0){
			marcarTratamientos(n,'SI',tipo);
		}
	}
}

function marcarTratamientos(n,valor,tipo){
	var listado=['checkRecogida','checkModificacion','checkConsulta','checkComunicacion1','checkRegistro','checkConservacion','checkCotejo','checkSupresion','checkComunicacion2','checkDestruccion','checkComunicacion3','checkComunicacion','checkEstructuracion','checkInterconexion','checkAnalisis'];
	if(valor=='SI'){
		for(var i=0;i<(listado.length-4);i++){
			console.log('#'+listado[i]+'_'+tipo+'_'+n);
			$('#'+listado[i]+'_'+tipo+'_'+n).attr('checked','checked');
		}
	} else {
		for(var i=0;i<listado.length;i++){
			$('#'+listado[i]+'_'+tipo+'_'+n).attr('checked',false);
		}
	}
}

function compruebaDelegaciones(){
	$("select.selectDelegaciones").each(function( index ) {
		var valor=$(this).val();
		var id='#div_'+$(this).attr('id');
		if(valor!=null && valor[valor.length-1]==22){
			$(id).removeClass('hide');
		} else {
			$(id).addClass('hide');
		}
	});
}

function compruebaSubcontrataciones(){
	$(".radioSubcontratacionFijo,.radioSubcontratacion").each(function( index ) {
		var valor=$(this).find('input[type=radio]:checked').val();
		var id='#'+$(this).attr('id').replace('radio','divSubcontratacion');
		if(valor=='SI'){
			$(id).removeClass('hide');
		} else {
			$(id).addClass('hide');
		}
	});
}

function insertaFilaSubcontrata(tabla){
    //Clono la última fila de la tabla
    var $tr = $('#'+tabla).find("tbody tr:last").clone();

    //Si existe un campo de descarga, creo un input file junto a él
    var nombreDescarga=$tr.find('.descargaFichero').attr('nombre');
    $tr.find('.descargaFichero').after("<input type='file' name='"+nombreDescarga+"' id='"+nombreDescarga+"' />");

    $tr.find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
        var parts = this.name.split('_');
        return parts[0]+'_'+ ++parts[1];
    }).attr("id", function(){//Hago lo mismo con los IDs
        return this.name;
    });


    $tr.find("input[name='filasTabla[]']").attr("value", function(){
        var valor=parseInt(this.value);
        return valor+parseInt(1);
    });
    
    $tr.find("button:not(.selectpicker)").attr("id", function(){//Para los botones de cálculo
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });

    $tr.find(".botonSelectAjax").attr("id", function(){//Para los botones de los desplegables con búsqueda por AJAX (LAE y similares)
        var parts = this.id.match(/(\D+)(\d*)$/);
        return parts[1] + ++parts[2];
    });


    $tr.find("input:not([type=checkbox],[type=radio]),textarea").attr("value","");
	$tr.find("input[type=checkbox]").removeAttr("checked");//Para quitar el check a la nueva fila clon de la anterior
    $tr.find("select").val("NULL");
    $tr.find('.bootstrap-select').remove();//Eliminación de los selectpicker
    $tr.find('.bootstrap-filestyle').remove();//Eliminación de los filestyle
    $tr.find('.descargaFichero').remove();//Eliminación del enlace de descarga, si lo hubiere

    //Añado la nueva fila a la tabla
    $('#'+tabla).find("tbody tr:last").after($tr);
    
    if(typeof jQuery.fn.selectpicker=='function'){//Compruebo que esté cargada la librería selectpicker, y en ese caso la inicializo para los select de la nueva fila
        $tr.find(".selectpicker").selectpicker('refresh');
    }
    if(typeof jQuery.fn.filestyle=='function'){//Misma operación para el filestyle
        $tr.find(':file').filestyle({input: false, iconName: "icon-folder-open", buttonText: "Seleccionar..."});
    }
    if(typeof jQuery.fn.rating=='function'){//Misma operación para el input-rating
        var campoRating=$tr.find('.rating');//La librería bootstrap-rating-input mete el input dentro de un div, por lo que primero rescato el input...
        $tr.find('.rating-input').replaceWith(campoRating);//... y sustituyo el div por él (si no crearía 2 filas de estrellas)
        campoRating.rating();//Inicializo la librería sobre el input
    }

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
}

function eliminaFilaSubcontrata(tabla){
  if($('#'+tabla).find("tbody tr").length>1){//Si la tabla tiene más de 1 fila...
    var filasSeleccionadas=0;
    $('input[name="filasTabla[]"]:checked').each(function() {
        var fila=$(this).val()-filasSeleccionadas;
        $('#'+tabla+' tr').eq(fila).remove();
        filasSeleccionadas++;
    });

    if(filasSeleccionadas>0){
        //Renumeración de los índices de la tabla
        var numFilas=$('#'+tabla+' tr:not(:first)').length;
        for(i=0;i<numFilas;i++){
           $('#'+tabla+' tr:not(:first)').eq(i).find("input:not([name='filasTabla[]'],.input-block-level),select,textarea").attr("name", function(){
               var parts = this.name.split('_');
        		return parts[0]+'_'+i;
            }).attr("id", function(){
                var parts = this.id.match(/(\D+)(\d*)$/);
                return this.name;
            });

            $('#'+tabla+' tr:not(:first)').eq(i).find("input[name='filasTabla[]']").attr("value", function(){
                var j=i+1
                return j;
            });
        }
        //Fin renumeración
    }
    else{
        alert('Para eliminar un registro, debe seleccionarlo utilizando el último check que tiene a su derecha.');
    }
  }
  else{
    alert('La tabla debe de contar con al menos 1 fila.');
  }
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
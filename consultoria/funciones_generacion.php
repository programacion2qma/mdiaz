<?php
function generaPRL($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
    	$cliente = datosRegistro("clientes",$datos['codigoCliente']);
		$consultoria=recogerFormularioServicios($datos);
		$listadoTrabajadores = consultaBD("SELECT * FROM empleados_prl WHERE codigoTrabajo=".$datos['codigo']);
		$listadoCentros = consultaBD("SELECT * FROM centros_prl WHERE codigoTrabajo=".$datos['codigo']);
	cierraBD();
	$nombreFichero="1_plan_de_prevencion.docx";
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$documento->setValue("razonSocial",utf8_decode(sanearCaracteres($consultoria['pregunta3'])));
	$documento->setValue("cif",utf8_decode($consultoria['pregunta9']));

	$documento->setValue("domicilio",utf8_decode($consultoria['pregunta4']));
	$documento->setValue("localidad",utf8_decode($consultoria['pregunta5']));
	$documento->setValue("provincia",utf8_decode(convertirMinuscula($consultoria['pregunta11'])));
	$documento->setValue("sector",utf8_decode($consultoria['pregunta67']));
	$documento->setValue("actividad",utf8_decode($consultoria['pregunta13']));
	$documento->setValue("trabajadores",utf8_decode($consultoria['pregunta7']));
	$documento->setValue("telefono",utf8_decode($consultoria['pregunta6']));
	$documento->setValue("email",utf8_decode($consultoria['pregunta12']));
	$documento->setValue("personaContacto",utf8_decode($consultoria['pregunta68']));
	$documento->setValue("responsable",utf8_decode($consultoria['pregunta69']));
	$documento->setValue("seguridad",utf8_decode($consultoria['pregunta71']));
	$documento->setValue("ergonomia",utf8_decode($consultoria['pregunta72']));
	$documento->setValue("higiene",utf8_decode($consultoria['pregunta73']));
	$documento->setValue("salud",utf8_decode($consultoria['pregunta74']));

	$p331 = ' ';
	$p332 = ' ';
	if($consultoria['pregunta33'] == 'EMPRESARIO'){
		$p331 = 'x';
	} else {
		$p332 = 'x';
	}
	$documento->setValue("p331",utf8_decode($p331));
	$documento->setValue("p332",utf8_decode($p332));

	$p701 = ' ';
	$p702 = ' ';
	$p703 = ' ';
	$p704 = ' ';
	if($consultoria['pregunta70'] == 'PROPIO'){
		$p701 = 'x';
	} else if($consultoria['pregunta70'] == 'MANCOMUNADO') {
		$p702 = 'x';
	} else if($consultoria['pregunta70'] == 'AJENO') {
		$p703 = 'x';
	} else if($consultoria['pregunta70'] == 'MIXTO') {
		$p704 = 'x';
	}
	$documento->setValue("p701",utf8_decode($p701));
	$documento->setValue("p702",utf8_decode($p702));
	$documento->setValue("p703",utf8_decode($p703));
	$documento->setValue("p704",utf8_decode($p704));

	$documento->setValue("listadoTrabajadores",utf8_decode(tablaTrabajadoresPRL($listadoTrabajadores)));

	$documento->setValue("listadoCentros",utf8_decode(tablaCentrosPRL($listadoCentros)));

	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	/*header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$nombreFichero);*/

    return '../documentos/consultorias/'.$nombreFichero;
}

function generaPRL2($codigo,$nombreFichero){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
	cierraBD();
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("empresa",utf8_decode(sanearCaracteres($consultoria['pregunta3'])));
	$documento->save('../documentos/consultorias/'.$nombreFichero);
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaPRL3($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$empleados=consultaBD("SELECT * FROM empleados_prl WHERE codigoTrabajo=".$datos['codigo']);
	cierraBD();
	$i=0;
	$documentos=array();
	while($empleado=mysql_fetch_assoc($empleados)){
		$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_12_Anexo_X_Formacion_e_informacion_en_materia_de_PRL.docx');
		$documento->setValue("empresa",utf8_decode(sanearCaracteres($consultoria['pregunta3'])));
		$documento->setValue("nombre",utf8_decode(sanearCaracteres($empleado['nombre'])));
		$documento->setValue("dni",utf8_decode(sanearCaracteres($empleado['dni'])));
		$documento->setValue("puesto",utf8_decode(sanearCaracteres($empleado['puesto'])));
		$nombreFichero='12_Anexo_X_Formacion_e_informacion_en_materia_de_PRL_'.($i+1).'.docx';
		$documento->save('../documentos/consultorias/'.$nombreFichero);
		$documentos[$i]='../documentos/consultorias/'.$nombreFichero;
		$i++;
	}
	return $documentos;
}

function generaPRL4($codigo,$nombreFichero){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$empleados=consultaBD("SELECT * FROM empleados_prl WHERE codigoTrabajo=".$datos['codigo']);
	cierraBD();
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("empresa",utf8_decode(sanearCaracteres($consultoria['pregunta3'])));
	$documento->setValue("cif",utf8_decode(sanearCaracteres($consultoria['pregunta9'])));
	$documento->setValue("fecha",utf8_decode(sanearCaracteres($consultoria['pregunta2'])));
	$documento->setValue("domicilio",utf8_decode(sanearCaracteres($consultoria['pregunta4'])));
	$documento->setValue("localidad",utf8_decode(sanearCaracteres($consultoria['pregunta5'])));
	$documento->setValue("provincia",utf8_decode(sanearCaracteres($consultoria['pregunta11'])));
	$documento->setValue("sector",utf8_decode(sanearCaracteres($consultoria['pregunta67'])));
	$documento->setValue("actividad",utf8_decode(sanearCaracteres($consultoria['pregunta13'])));
	$documento->setValue("representante",utf8_decode(sanearCaracteres($consultoria['pregunta8'])));
	$documento->setValue("listadoPuestos",utf8_decode(tablaPuestos($empleados)));
	$documento->save('../documentos/consultorias/'.$nombreFichero);
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaPRL5($codigo,$nombre){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$centros=consultaBD("SELECT * FROM centros_prl WHERE codigoTrabajo=".$datos['codigo']);
	cierraBD();
	$i=0;
	$documentos=array();
	while($centro=mysql_fetch_assoc($centros)){
		$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombre.'.docx');
		$documento->setValue("empresa",utf8_decode(sanearCaracteres($consultoria['pregunta3'])));
		$documento->setValue("nombre",utf8_decode(sanearCaracteres($centro['nombre'])));
		$nombreFichero=$nombre.'_'.($i+1).'.docx';
		$documento->save('../documentos/consultorias/'.$nombreFichero);
		$documentos[$i]='../documentos/consultorias/'.$nombreFichero;
		$i++;
	}
	return $documentos;
}

function generaLOPD($codigo,$nombreFichero){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	cierraBD();
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("empresa",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	
	$fecha =formateaFechaBD($consultoria['pregunta2']);
	$fecha = new DateTime($fecha);
	$fecha->add(new DateInterval('P11M'));
	$documento->setValue("valido",$fecha->format('m/Y'));
	$documento->save('../documentos/consultorias/'.$nombreFichero);
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaLOPD2($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	cierraBD();
	$nombreFichero='4_documento_seguridad.docx';
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("empresa",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	$documento->setValue("localidad",utf8_decode($cliente['localidad']));
	$documento->setValue("fechaAprobacion",fecha());
	
	$fecha =explode('/',$consultoria['pregunta2']);
	$meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	$mes = (int) $fecha[1];
	$documento->setValue("fecha",$meses[$mes].' de '.$fecha[2]);

	$p532_1='';
	$p532_2='';
	/*if($consultoria['pregunta522'] == '1'){
		$p532_1='x';
	} else {
		$p532_2='x';
	}*/
	$documento->setValue("p532_1",$p532_1);
	$documento->setValue("p532_2",$p532_2);
	$fechaHoy=explode('/',fecha());
	$mesHoy = (int) $fechaHoy[1];
	$documento->setValue("fecha2",$fechaHoy[0].' de '.$meses[$mesHoy].' de '.$fechaHoy[2]);
	$documento->setValue("nombreRepresentante",utf8_decode($consultoria['pregunta8']));
	$documento->setValue("dniRepresentante",utf8_decode($consultoria['pregunta14']));
	$documento->setValue("cif",utf8_decode($cliente['cif']));

	$documento->setValue("listadoUsuarios",utf8_decode(tablaUsuariosLOPD($consultoria)));

	$documento->setValue("ordenadores",utf8_decode($consultoria['pregunta321']));
	$documento->setValue("portatiles",utf8_decode($consultoria['pregunta328']));

	$documento->setValue("listadoAplicaciones",utf8_decode(tablaAplicacionesLOPD($consultoria)));
	$documento->setValue("resumenFicheros",utf8_decode(tablaResumenFicheros($consultoria)));
	$documento->setValue("tablasFicheros",utf8_decode(tablasFicheros($consultoria,$cliente['razonSocial'],$cliente['domicilio'])));
	$documento->setValue("tablaSoporte",utf8_decode(tablaSoportes($consultoria)));
	$documento->setValue("tablaEncargadosTratamiento",utf8_decode(tablaEncargadosTratamiento($consultoria)));
	$documento->setValue("tablaPortatiles",utf8_decode(tablaPortatiles($consultoria)));
	$documento->setValue("tablaSoportesDS",utf8_decode(tablaSoportesDS($datos)));
	$documento->setValue('anexoO',utf8_decode(sanearCaracteres(obtenerAnexoO($consultoria['pregunta64']))));

	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaLOPD3($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	cierraBD();
	$nombreFichero='5_documentacion_legal_lopd_05082016.docx';
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	
	$documento->setValue("empresa",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	$documento->setValue("direccion",utf8_decode($cliente['domicilio'].'. '.$cliente['localidad'].'. '.$cliente['cp'].'. '.convertirMinuscula($cliente['provincia'])));
	$documento->setValue("cif",utf8_decode($cliente['cif']));
	$documento->setValue("localidad",utf8_decode($cliente['localidad']));
	$documento->setValue("email",utf8_decode($cliente['email']));

	$documento->setValue("responsable",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	$documento->setValue("direccionResponsable",utf8_decode($cliente['domicilio'].'. '.$cliente['localidad'].'. '.$cliente['cp'].'. '.convertirMinuscula($cliente['provincia'])));
	$documento->setValue("cifResponsable",utf8_decode($cliente['cif']));

	$fecha =explode('/',$consultoria['pregunta2']);
	$meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	$mes = (int) $fecha[1];
	$documento->setValue("fecha",$fecha[0].' de '.$meses[$mes].' de '.$fecha[2]);

	//$documento->setValue('contratosLaborales',utf8_decode(sanearCaracteres(contratosLaborales())));
	$documento->setValue('contralesLaborales','');
	$documento->setValue('true','');
	
	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaLOPD4($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
	cierraBD();
	$nombreFichero='6_documentacion_del_personal.docx';
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$documento->setValue("listadoUsuarios",utf8_decode(tablaUsuariosLOPD2($consultoria)));

	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaLOPD5($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	cierraBD();
	$nombreFichero='anexo_catalan_maquetado_marcador_10082016.docx';
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$fecha =explode('/',$consultoria['pregunta2']);
	$meses = array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	$mes = (int) $fecha[1];
	$documento->setValue("anio",$fecha[2]);
	$documento->setValue("empresa",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	$documento->setValue("direccion",utf8_decode($cliente['domicilio'].'. '.$cliente['localidad'].'. '.$cliente['cp'].'. '.convertirMinuscula($cliente['provincia'])));
	$documento->setValue("cif",utf8_decode($cliente['cif']));
	$documento->setValue("localidad",utf8_decode($cliente['localidad']));
	$documento->setValue("email",utf8_decode($cliente['email']));

	$documento->setValue("responsable",utf8_decode(sanearCaracteres($consultoria['pregunta3'])));
	$documento->setValue("direccionResponsable",utf8_decode($consultoria['pregunta4'].'. '.$consultoria['pregunta5'].'. '.$consultoria['pregunta10'].'. '.$consultoria['pregunta11']));
	$documento->setValue("cifResponsable",utf8_decode($consultoria['pregunta9']));

	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaLOPD6($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
	cierraBD();
	$nombreFichero='anexo_soportes.docx';
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	
	$i=442;
	$pregunta=0;
	$campos=array('nombreSoporteLOPD','marcaSoporteLOPD','serieSoporteLOPD','fechaSoporteLOPD','ubicacionSoporteLOPD','sistemaSoporteLOPD','aplicacionesSoporteLOPD','conexionesSoporteLOPD','responsableSoporteLOPD','otrosSoporteLOPD');
	$soportes = consultaBD("SELECT * FROM soportes_lopd WHERE codigoTrabajo=".$codigo,true);
    while($soporte=mysql_fetch_assoc($soportes)){
    	foreach ($campos as $campo) {
			if($pregunta!=20){
				$documento->setValue("p".$pregunta,utf8_decode($soporte[$campo]));
			}
			$pregunta++;
		}
	}
	while($pregunta<81){
		if($pregunta!=20){
			$documento->setValue("p".$pregunta,'');
		}
		$pregunta++;
	}
	$documento->save('../documentos/consultorias/'.$nombreFichero);
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaLOPD7($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	cierraBD();
	$nombreFichero='documentacion_ingles.docx';
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$documento->setValue("empresa",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	$documento->setValue("direccion",utf8_decode($cliente['domicilio'].'. '.$cliente['localidad'].'. '.$cliente['cp'].'. '.convertirMinuscula($cliente['provincia'])));
	$documento->setValue("cif",utf8_decode($cliente['cif']));
	$documento->setValue("localidad",utf8_decode($cliente['localidad']));
	$documento->setValue("email",utf8_decode($cliente['email']));

	$documento->setValue("responsable",utf8_decode(sanearCaracteres($consultoria['pregunta3'])));
	$documento->setValue("direccionResponsable",utf8_decode($consultoria['pregunta4'].'. '.$consultoria['pregunta5'].'. '.$consultoria['pregunta10'].'. '.convertirMinuscula($consultoria['pregunta11'])));
	$documento->setValue("cifResponsable",utf8_decode($consultoria['pregunta9']));

	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaLOPD8($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("trabajos",$codigo);
		$consultoria=recogerFormularioServicios($datos);
		$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	cierraBD();
	$nombreFichero='certificado_videovigilancia.docx';
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$documento->setValue("empresa",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	$documento->setValue("direccion",utf8_decode($cliente['domicilio'].'. '.$cliente['localidad'].'. '.$cliente['cp'].'. '.convertirMinuscula($cliente['provincia'])));

	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaAlergeno($codigo,$nombreFichero){
	$datos = datosRegistro("trabajos",$codigo);
	$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$documento->setValue("empresa",utf8_decode(sanearCaracteres($cliente['razonSocial'])));
	
	$documento->save('../documentos/consultorias/'.$nombreFichero);
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaAlergeno2($codigo,$nombreFichero){
	$datos = datosRegistro("trabajos",$codigo);
	$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);

	$enlace = $_CONFIG['raiz']."cartas-alergenos/?idencrp=".md5($cliente['codigo']); 
		
	$PNG_TEMP_DIR = '../img/temp/';

	$PNG_WEB_DIR = '../img/temp/';

	if (!file_exists($PNG_TEMP_DIR))
    		mkdir($PNG_TEMP_DIR);

	$matrixPointSize = 10;
	$errorCorrectionLevel = 'L';

	$filename = $PNG_TEMP_DIR.'image3.png';

	QRcode::png($enlace, $filename, $errorCorrectionLevel, $matrixPointSize, 2);

	$documento->replaceImage('../img/temp/','image3.png');
	$documento->save('../documentos/consultorias/'.$nombreFichero);
	return '../documentos/consultorias/'.$nombreFichero;
}

function generaAlergenoPG($codigo,$nombreFichero){
	$datos = datosRegistro("trabajos",$codigo);
	$consultoria=recogerFormularioServicios($datos);
	$cliente = datosRegistro("clientes",$datos['codigoCliente']);
	if($cliente['ficheroLogo'] != '' && $cliente['ficheroLogo'] != 'NO'){
		$logo="<img src='../documentos/logos-clientes/".$cliente['ficheroLogo']."' alt='LOGO'>";
	} else {
		$logo='NO HAY LOGO SUBIDO';
	}
	$platos=consultaBD("SELECT * FROM platos_alergenos WHERE codigoTrabajo=".$datos['codigo']);
	$contenido = "
	<style type='text/css'>
	<!--
		*{
			color:#333;
			font-family: Arial;
			line-height:20px;
		}

		table{
			width:100%;
			border:1px solid #428bca;
			border-spacing: 0;
			border-collapse: collapse;
		}

		table th{
			font-weight:bold;
			border:1px solid #2E3D64;
			padding-top:2px;
			padding-left:5px;
			padding-right:5px;
		}

		table td{
			border:1px solid #2E3D64;
			padding-top:2px;
			padding-left:5px;
			padding-right:5px;
		}

		cabecera2 table,
		cabecera2 table td{
			border:0px;
		}

		table.leyenda,
		table.leyenda td{
			border:0px;
			font-size:10px;
			width:90%;
			padding:3px;
		}

		table.leyenda img,
		table.tablaAlergenos img
		.ficha img{
			width:15px;
		}

		table.tablaAlergenos{
			border:0px
		}

		table.tablaAlergenos td{
			border:0px solid #2E3D64;
			border-top:3px solid #FFF;
			border-bottom:3px solid #FFF;
			padding-top:2px;
			padding-left:5px;
			padding-right:5px;
		}

		table.tablaAlergenos td{
			background-color:#EEE;
		}

		table.tablaAlergenos .even td{
			background-color:#DDD;
		}

		table.leyenda .altramuz{
			color:#FADD3D;
		}

		table.leyenda .apio{
			color:#54BF36;
		}

		table.leyenda .cacahuetes{
			color:#E08F65;
		}

		table.leyenda .cereales{
			color:#F47039;
		}

		table.leyenda .crustaceos{
			color:#1CB6F1;
		}

		table.leyenda .frutossecos{
			color:#DA4752;
		}

		table.leyenda .huevos{
			color:#F69035;
		}

		table.leyenda .lacteos{
			color:#6D351E;
		}

		table.leyenda .molusco{
			color:#4DC4D5;
		}

		table.leyenda .mostaza{
			color:#C09328;
		}

		table.leyenda .pescado{
			color:#28429D;
		}

		table.leyenda .sesamo{
			color:#9A8E6C;
		}

		table.leyenda .soja{
			color:#02A75B;
		}

		table.leyenda .sulfitos{
			color:#83114F;
		}

		.cabecera img,
		.cabecera2 img{
			width:50%;
		}

		.a1{
			width:1%;
		}

		.a8{
			font-weight:bold;
			width:8%;
		}

		.a12{
			width:12%;
		}

		.a100{
			width:100%;
		}

		.a10{
			width:10%;
		}

		.a25{
			width:25%;
		}

		.a40{
			width:40%;
		}

		.a33{
			width:33%;
		}

		.a66{
			width:66%;
		}

		.a30{
			width:30%;
		}

		.a70{
			width:70%;
		}

		.centro{
			text-align:center;
		}



	-->
	</style>
	<page backbottom='10mm'>
		<div class='cabecera'>
			<table>
				<tbody>
					<tr>
						<td colspan='2' class='a66'><b>PLAN DE GESTIÓN DE ALÉRGENOS</b></td>
						<td class='a33' style='text-align:center;'>".$logo."</td>
					</tr>
					<tr>
						<td class='a33'><b>".$cliente['razonSocial']."</b></td>
						<td class='a33'></td>
						<td class='a33'>Última rev: ".$consultoria['pregunta2']." <br/> Pág. 1 de 2</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/><br/>
		<b>INTRODUCCIÓN</b><br/><br/>
		El objetivo de este plan es analizar tanto las materias primas como los proveedores y procesos de elaboración de los platos/productos que se llevan a cabo con el fin de identificar la presencia de sustancias que pueden producir alergias e intolerancias alimentarias en los consumidores.<br/><br/>

		<b>¿Qué se debe conocer?</b><br/><br/>

		<b>Intolerancia alimentaria.</b> Se trata de una reacción adversa del organismo frente a un alimento caracterizada por la incapacidad para digerirlo y metabolizarlo. En este caso no interviene el mecanismo inmunológico y el componente extraño no llega a pasar al torrente sanguíneo puesto que no ha podido ser absorbido.<br/><br/>

		<b>Alergia o hipersensibilidad alimentaria.</b> Es la reacción adversa que presenta un individuo tras la ingestión, contacto o inhalación de un alimento con una causa inmunológica comprobada. Es de destacar que algunas personas con intolerancia son capaces de consumir pequeñas cantidades del alimento al que presentan dicha intolerancia (excepto en el caso del gluten), mientras que en la alergia hay que eliminar totalmente el componente.<br/><br/>

		<b>Alérgeno.</b> Se entiende por alérgeno aquella sustancia que puede provocar una reacción alérgica. Son sustancias que, en algunas personas, el sistema inmunitario reconoce como “extrañas” o “peligrosas”.<br/><br/>

		<b>Contaminación cruzada.</b> Proceso por el cual los alimentos entran en contacto con sustancias ajenas o no deseadas.<br/><br/>


		<i>Un ejemplo gráfico de contaminación cruzada (indirecta) es el que se produce cuando para elaborar una salsa (libre de gluten) se usa un recipiente donde anteriormente se ha amasado harina. Si el recipiente no ha sido lavado y secado o si quedan restos de harina, la salsa contendrá harina, aún en cantidades muy pequeñas, pero no sería apta para personas alérgicas o intolerantes al gluten pese a no incorporar gluten en su fabricación. El proceso de elaboración no ha tenido en cuenta la posibilidad de contaminación cruzada.</i><br/><br/>

		<b>FINALIDAD DEL PLAN DE GESTIÓN DE ALÉRGENOS</b><br/><br/>

		Identificar la presencia de éstas sustancias (aquéllas identificadas en el Anexo II del Reglamento (UE) 1169/2011, tanto en las materias primas y otros productos empleados en la formulación de los platos /productos finales como su presencia por contaminación cruzada.
		<page_footer>
	    </page_footer>
	</page>
	<page backbottom='10mm'>
		<div class='cabecera'>
			<table>
				<tbody>
					<tr>
						<td colspan='2' class='a66'><b>PLAN DE GESTIÓN DE ALÉRGENOS</b></td>
						<td class='a33' style='text-align:center;'>".$logo."</td>
					</tr>
					<tr>
						<td class='a33'><b>".$cliente['razonSocial']."</b></td>
						<td class='a33'></td>
						<td class='a33'>Última rev: ".$consultoria['pregunta2']." <br/> Pág. 2 de 2</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/><br/>
		<b>PERSONAL RESPONSABLE DEL PLAN DE GESTIÓN DE ALÉRGENOS</b><br/><br/>
		La gestión y el control de los alérgenos es responsabilidad de todo el personal.<br/><br/>

		La gestión y el control de los alérgenos comienza en los proveedores, así la persona designada para el control del plan de trazabilidad conoce y evalúa a los proveedores con el fin de identificar posibles contaminaciones de los productos o materias primas suministradas, así como controlar la documentación que cada proveedor debe aportar sobre cada alimento aportado.<br/><br/>

		En el Anexo I se adjunta un listado de los platos/productos evaluados así como los resultados de la evaluación de los proveedores.<br/><br/>		

		Durante el proceso de elaboración, el personal de cocina será el responsable de conocer y determinar las formulaciones de los platos/productos y mantener las medidas de prevención que permiten elaborar platos libres de determinadas sustancias alérgenas así como evitar cualquier tipo de contaminación cruzada.<br/><br/>

		La evaluación de las materias primas así como la evaluación de los procesos de elaboración de los platos/productos se realiza por la persona mencionada anteriormentes.<br/><br/>

		Los resultados obtenidos de la evaluación de materias primas así como de las formulaciones empleadas en los platos/productos se documentan en el Anexo II. Este documento recoge el listado de platos/productos junto con la identificación de las sustancias presentes que pueden causar alergias o intolerancias alimentarias y está a disposición del consumidor final mediante acceso telemático al documento (código QR) como en soporte papel al consumidor que lo necesite.<br/><br/>

		<b>ACCIONES CORRECTORAS</b><br/><br/>


		Si del resultado de la evaluación  anterior se desprende el incumplimiento de alguna de las medidas básicas de prevención de contaminación cruzada en alimentos libres de alérgenos, inmediatamente se registrará la posible presencia de alérgenos en el/los platos/productos indicando su presencia en la información que se presente al consumidor para su conocimiento (anexo II).<br/><br/>

		<b>ANEXOS</b><br/><br/>

		<b>Anexo I</b> resultados de la evaluación de proveedores.<br/>
		<b>Anexo II</b> listado de platos/productos con identificación de las sustancias que causan alergias/intolerancias.<br/>
		<b>Anexo III</b> resultados de la evaluación del proceso de elaboración.

		<page_footer>
	    </page_footer>
	</page>
	<page footer='page' backbottom='10mm'>
		<div class='cabecera2'>
			<table>
				<tbody>
					<tr>
						<td class='a66'><b>".$cliente['razonSocial']."</b></td>
						<td class='a33' style='text-align:center;'>".$logo."</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/><br/>
		<div align='center'>
			<b>PLAN DE GESTIÓN DE ALÉRGENOS <br/><br/>
			ANEXO I<br/><br/>
			Evaluación de proveedores<br/><br/>
			Documento actualizado a fecha ".$consultoria['pregunta2']."</b>
		</div>
		<br/><br/>
		<b>Proveedores y sustancias evaluadas</b><br/><br/>
		<table style='text-align:center'>
			<thead>
				<tr>
					<th class='a25'>PROVEEDOR</th>
					<th class='a25'>PRODUCTO</th>
					<th class='a40'>PLATOS</th>
					<th class='a10'>IA</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class='a25'></td>
					<td class='a25'></td>
					<td class='a40'></td>
					<td class='a10'></td>
				</tr>
			</tbody>
		</table>
		<br/><br/>
		<b>Resultados de la evaluación</b><br/><br/>
		En este establecimiento se cumple:<br/>
		<br/><br/>
		Deben adoptarse las siguientes medidas:<br/>
		<page_footer>
			<b>*IA: Información aportada por el proveedor<br/>
			*RE: Requerir información al proveedor</b>
	    </page_footer>
	</page>
	<page footer='page' backbottom='10mm'>
		<div class='cabecera2'>
			<table>
				<tbody>
					<tr>
						<td class='a66'><b>".$cliente['razonSocial']."</b></td>
						<td class='a33' style='text-align:center;'>".$logo."</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/><br/>
		<div align='center'>
			<b>PLAN DE GESTIÓN DE ALÉRGENOS <br/><br/>
			ANEXO II<br/><br/>
			Listado de productos con identificación de las sustancias que producen alergias e intolerancias alimentarias*
			recogidas en el Reglamento (UE) nº 1169/2011.</b><br/>
			<i>The list below contanis allergy and intolerance food information under the EU Food Information for Consumers Regulation N.1169/2011.</i><br/><br/>
		</div>
		<br/><br/>
		<table class='tablaAlergenos'>
			<tbody>";
		$clase='';
		$fichas=array();
		$indiceFichas=0;
		$numeroPlatos=0;
		while($plato=mysql_fetch_assoc($platos)){
			$numeroPlatos++;
			if($numeroPlatos==26){
				$numeroPlatos=0;
				$contenido.="</tbody>
		</table>
		".pieAlergenos()."
	</page>
	<page footer='page' backbottom='10mm'>
	<table class='tablaAlergenos'><tbody>";
			}
			$iconos=array('1'=>'altramuz','3'=>'apio','4'=>'cacahuetes','5'=>'cereales','6'=>'crustaceos','7'=>'sulfitos','8'=>'frutossecos','9'=>'huevos','10'=>'lacteos','11'=>'molusco','12'=>'mostaza','13'=>'pescado','14'=>'sesamo','15'=>'soja');
			$grupoAlergenos=consultaBD('SELECT * FROM platos_alergenos_grupos WHERE codigoPlato='.$plato['codigo'],true);
			$alergenos='';
			while($grupo=mysql_fetch_assoc($grupoAlergenos)){
				$alergenos.="<img width='15' src='../img/alergenos/".$iconos[$grupo['codigoGrupo']].".png'>";
			}
			$fichas[$indiceFichas]=creaPaginaAlergeno($cliente,$logo,$plato,$alergenos);
			$indiceFichas++;
			$contenido.= "<tr class=".$clase.">
					<td class='a30' style='text-align:right;'>".$alergenos."</td>
					<td class='a70'>".$plato['nombrePlato']."</td>
				</tr>";
			if($clase==''){
				$clase='even';
			} else{
				$clase='';
			}
		}		
$contenido.="</tbody>
		</table>
		".pieAlergenos()."
	</page>";

	foreach ($fichas as $ficha) {
		$contenido.=$ficha;
	}
$contenido.="<page footer='page' backbottom='10mm'>
		<div class='cabecera2'>
			<table>
				<tbody>
					<tr>
						<td class='a66'><b>".$cliente['razonSocial']."</b></td>
						<td class='a33' style='text-align:center;'>".$logo."</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/><br/>
		<div align='center'>
			<b>PLAN DE GESTIÓN DE ALÉRGENOS <br/><br/>
			ANEXO III<br/><br/>
			Evaluación de proveedores<br/><br/>
			Documento actualizado a fecha ".$consultoria['pregunta2']."</b><br/><br/>
		</div>
		<b>Resultado de la evaluación</b><br/><br/>
		En este establecimiento:<br/>
		<page_footer>
	    </page_footer>
	</page>";

	require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es');
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
	$html2pdf->Output('../documentos/consultorias/'.$nombreFichero,'f');
	//$html2pdf->Output('../documentos/consultorias/'.$nombreFichero);

	return '../documentos/consultorias/'.$nombreFichero;
}

function pieAlergenos(){
	return "<page_footer>
			<table class='leyenda'>
				<tr>
					<td class='a8'>LEYENDA:</td>
					<td class='a1'><img src='../img/alergenos/altramuz.png'></td>
					<td class='a12 altramuz'>Altramuz</td>
					<td class='a1'><img src='../img/alergenos/apio.png'></td>
					<td class='a12 apio'>Apio</td>
					<td class='a1'><img src='../img/alergenos/cacahuetes.png'></td>
					<td class='a12 cacahuetes'>Cacahuete</td>
					<td class='a1'><img src='../img/alergenos/cereales.png'></td>
					<td class='a12 cereales'>Cereales</td>
					<td class='a1'><img src='../img/alergenos/crustaceos.png'></td>
					<td class='a12 crustaceos'>Crustaceos</td>
					<td class='a1'><img src='../img/alergenos/frutossecos.png'></td>
					<td class='a12 frutossecos'>Frutos secos</td>
					<td class='a1'><img src='../img/alergenos/huevos.png'></td>
					<td class='a12 huevos'>Huevos</td>
				</tr>
				<tr>
					<td class='a8'>KEY:</td>
					<td class='a1'></td>
					<td class='a12 altramuz'>Lupin</td>
					<td class='a1'></td>
					<td class='a12 apio'>Celery</td>
					<td class='a1'></td>
					<td class='a12 cacahuetes'>Peanuts</td>
					<td class='a1'></td>
					<td class='a12 cereales'>Cereals</td>
					<td class='a1'></td>
					<td class='a12 crustaceos'>Crustaceans</td>
					<td class='a1'></td>
					<td class='a12 frutossecos'>Nuts</td>
					<td class='a1'></td>
					<td class='a12 huevos'>Eggs</td>
				</tr>
				<tr>
					<td class='a8'></td>
					<td class='a1'><img src='../img/alergenos/lacteos.png'></td>
					<td class='a12 lacteos'>Lacteos</td>
					<td class='a1'><img src='../img/alergenos/molusco.png'></td>
					<td class='a12 molusco'>Molusco</td>
					<td class='a1'><img src='../img/alergenos/mostaza.png'></td>
					<td class='a12 mostaza'>Mostaza</td>
					<td class='a1'><img src='../img/alergenos/pescado.png'></td>
					<td class='a12 pescado'>Pescado</td>
					<td class='a1'><img src='../img/alergenos/sesamo.png'></td>
					<td class='a12 sesamo'>Sésamo</td>
					<td class='a1'><img src='../img/alergenos/soja.png'></td>
					<td class='a12 soja'>Soja</td>
					<td class='a1'><img src='../img/alergenos/sulfitos.png'></td>
					<td class='a12 sulfitos'>Sulfito</td>
				</tr>
				<tr>
					<td class='a8'></td>
					<td class='a1'></td>
					<td class='a12 lacteos'>Milk</td>
					<td class='a1'></td>
					<td class='a12 mosluco'>Molluscs</td>
					<td class='a1'></td>
					<td class='a12 mostaza'>Mustard</td>
					<td class='a1'></td>
					<td class='a12 pescado'>Fish</td>
					<td class='a1'></td>
					<td class='a12 sesamo'>Sesame</td>
					<td class='a1'></td>
					<td class='a12 soja'>Soya</td>
					<td class='a1'></td>
					<td class='a12 sulfitos'>Sulphites</td>
				</tr>
			</table>
	    </page_footer>";
}

function tablaTrabajadoresPRL($listado){
	$existe=false;
	$res='<w:tbl><w:tblPr><w:tblW w:w="8640" w:type="dxa"/><w:tblInd w:w="108" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="01E0" w:firstRow="1" w:lastRow="1" w:firstColumn="1" w:lastColumn="1" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="4111"/><w:gridCol w:w="1559"/><w:gridCol w:w="2970"/></w:tblGrid><w:tr w:rsidR="00B7772B" w:rsidRPr="00640E43" w14:paraId="5BD318FB" w14:textId="77777777" w:rsidTr="00855A1B"><w:tc><w:tcPr><w:tcW w:w="4111" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="608AAE72" w14:textId="77777777" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00B7772B" w:rsidP="008E5333"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="006E02A4"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>Nombre y apellidos</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="43533656" w14:textId="77777777" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00B7772B" w:rsidP="008E5333"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="006E02A4"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>DNI</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2970" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="6A5862CC" w14:textId="77777777" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00B7772B" w:rsidP="008E5333"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="006E02A4"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>Puesto de Trabajo</w:t></w:r></w:p></w:tc></w:tr>';
	while($item=mysql_fetch_assoc($listado)){
	$existe=true;
	$res.='<w:tr w:rsidR="00B7772B" w:rsidRPr="00640E43" w14:paraId="12769ED0" w14:textId="77777777" w:rsidTr="001968F0"><w:tc><w:tcPr><w:tcW w:w="4111" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4C722C1A" w14:textId="4D546C29" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00934CB4" w:rsidP="00934CB4"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item['nombre'].'</w:t></w:r><w:bookmarkStart w:id="3" w:name="_GoBack"/><w:bookmarkEnd w:id="3"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5312AFE5" w14:textId="0FD28893" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00934CB4" w:rsidP="008E5333"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item['dni'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2970" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0EF2C4DA" w14:textId="1ACE2D25" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00934CB4" w:rsidP="008E5333"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item['puesto'].'</w:t></w:r></w:p></w:tc></w:tr>';
	}
	if(!$existe){
		$res.='<w:tr w:rsidR="00B7772B" w:rsidRPr="00640E43" w14:paraId="12769ED0" w14:textId="77777777" w:rsidTr="001968F0"><w:tc><w:tcPr><w:tcW w:w="4111" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4C722C1A" w14:textId="4D546C29" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00934CB4" w:rsidP="00934CB4"><w:pPr><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="3" w:name="_GoBack"/><w:bookmarkEnd w:id="3"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5312AFE5" w14:textId="0FD28893" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00934CB4" w:rsidP="008E5333"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2970" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0EF2C4DA" w14:textId="1ACE2D25" w:rsidR="00B7772B" w:rsidRPr="006E02A4" w:rsidRDefault="00934CB4" w:rsidP="008E5333"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr>';
	}


	$res.='</w:tbl>';

	return $res;
}

function tablaCentrosPRL($listado){
	$existe = false;
	$res='<w:tbl><w:tblPr><w:tblW w:w="4957" w:type="pct"/><w:jc w:val="center"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblCellMar><w:left w:w="70" w:type="dxa"/><w:right w:w="70" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="2190"/><w:gridCol w:w="1560"/><w:gridCol w:w="1560"/><w:gridCol w:w="1527"/><w:gridCol w:w="1733"/></w:tblGrid><w:tr w:rsidR="00766305" w:rsidRPr="00A25AAD" w14:paraId="53CC0AE4" w14:textId="77777777" w:rsidTr="00A25AAD"><w:trPr><w:jc w:val="center"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1278" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="55651F70" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:caps/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A25AAD"><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:caps/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>Centro de Trabajo</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="910" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="2373E2AE" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:caps/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A25AAD"><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:caps/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>nº Delegados de Prevención</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="910" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="2FCC02A1" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A25AAD"><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>COMITÉ DE SEGURIDAD Y SALUD</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="891" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="408DB706" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A25AAD"><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>TRABAJADOR DESIGNADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1011" w:type="pct"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="17B34789" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr></w:pPr><w:r w:rsidRPr="00A25AAD"><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t>CONSULTA DIRECTA A LOS TRABAJADORES</w:t></w:r></w:p></w:tc></w:tr>';
	while($item=mysql_fetch_assoc($listado)){
	$existe = true;
	$res.='<w:tr w:rsidR="00766305" w:rsidRPr="00A25AAD" w14:paraId="66A34EE9" w14:textId="77777777" w:rsidTr="00A25AAD"><w:trPr><w:jc w:val="center"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1278" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="55CAA88A" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="16"/></w:rPr></w:pPr></w:p><w:p w14:paraId="5AB09C8F" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00100A34" w:rsidP="00AB217D"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item['nombre'].'</w:t></w:r><w:r w:rsidR="00766305" w:rsidRPr="00A25AAD"><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="910" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4472FF67" w14:textId="31585DBD" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="006640D5" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item['delegado'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="910" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0ABCEA03" w14:textId="714E3C33" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="006640D5" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item['comite'].'</w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="891" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="0C6A3588" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="61945DF9" w14:textId="7B808CB8" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="006640D5" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item['trabajador'].'</w:t></w:r></w:p><w:p w14:paraId="13E602CF" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00472DC1"><w:pPr><w:pStyle w:val="Estndar"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1011" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7A068182" w14:textId="0DA36815" w:rsidR="00766305" w:rsidRPr="007E661D" w:rsidRDefault="006640D5" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="007E661D"><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item['consulta'].'</w:t></w:r></w:p></w:tc></w:tr>';
	}
	if(!$existe){
		$res.='<w:tr w:rsidR="00766305" w:rsidRPr="00A25AAD" w14:paraId="66A34EE9" w14:textId="77777777" w:rsidTr="00A25AAD"><w:trPr><w:jc w:val="center"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1278" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="55CAA88A" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="16"/></w:rPr></w:pPr></w:p><w:p w14:paraId="5AB09C8F" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00100A34" w:rsidP="00AB217D"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00766305" w:rsidRPr="00A25AAD"><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="910" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4472FF67" w14:textId="31585DBD" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="006640D5" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="910" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0ABCEA03" w14:textId="714E3C33" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="006640D5" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="891" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders></w:tcPr><w:p w14:paraId="0C6A3588" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p><w:p w14:paraId="61945DF9" w14:textId="7B808CB8" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="006640D5" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r></w:p><w:p w14:paraId="13E602CF" w14:textId="77777777" w:rsidR="00766305" w:rsidRPr="00A25AAD" w:rsidRDefault="00766305" w:rsidP="00472DC1"><w:pPr><w:pStyle w:val="Estndar"/><w:rPr><w:rFonts w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1011" w:type="pct"/><w:tcBorders><w:left w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="12" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7A068182" w14:textId="0DA36815" w:rsidR="00766305" w:rsidRPr="007E661D" w:rsidRDefault="006640D5" w:rsidP="00785B80"><w:pPr><w:pStyle w:val="Estndar"/><w:jc w:val="center"/><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="007E661D"><w:rPr><w:rFonts w:cs="Arial"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr>';
	}


	$res.='</w:tbl>';

	return $res;
}

function tablaUsuariosLOPD($consultoria){
	$existe=false;
	$i=86;
	$listado=array();
	$usuario=array();
	$usuario['nombre']=$consultoria['pregunta8'];
	$usuario['nif']=$consultoria['pregunta14'];
	$usuario['cargo'] = 'Representante legal';
	$usuario['acceso']='Acceso total';
	$listado[0]=$usuario;
	$indice=1;
	$accesos=array('X'=>'Acceso total','W'=>'Creacción/Modificación/Borrado','R'=>'Solo lectura');
	while($i<=156){
		if($consultoria['pregunta'.$i] != ''){
			$usuario=array();
			$usuario['nombre'] = $consultoria['pregunta'.$i];
			$i++;
			$usuario['nif'] = $consultoria['pregunta'.$i];
			$i = $i+4;
			$usuario['acceso']=$accesos[$consultoria['pregunta'.$i]];
			$i++;
			$usuario['cargo']='';
			$listado[$indice]=$usuario;
			$indice++;
		} else {
			$i=$i+6;
		}
	}
	$res='<w:tbl><w:tblPr><w:tblW w:w="9355" w:type="dxa"/><w:tblInd w:w="1526" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="3368"/><w:gridCol w:w="1701"/><w:gridCol w:w="2534"/><w:gridCol w:w="1752"/></w:tblGrid><w:tr w:rsidR="006D7388" w:rsidRPr="009522F0" w14:paraId="75D7E9CB" w14:textId="77777777" w:rsidTr="00AC4739"><w:trPr><w:trHeight w:val="762"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="3368" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="1476492D" w14:textId="77777777" w:rsidR="004E24C6" w:rsidRPr="00AC4739" w:rsidRDefault="000F23C8" w:rsidP="00AC4739"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00AC4739"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>NOMBRE / APELLIDOS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="18E1FE3E" w14:textId="77777777" w:rsidR="004E24C6" w:rsidRPr="00AC4739" w:rsidRDefault="000F23C8" w:rsidP="00AC4739"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00AC4739"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>NIF</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2534" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="30E36186" w14:textId="77777777" w:rsidR="004E24C6" w:rsidRPr="00AC4739" w:rsidRDefault="000F23C8" w:rsidP="00AC4739"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00AC4739"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>CARGO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1752" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="1E2112B8" w14:textId="77777777" w:rsidR="004E24C6" w:rsidRPr="00AC4739" w:rsidRDefault="000F23C8" w:rsidP="00AC4739"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="16"/><w:szCs w:val="16"/></w:rPr></w:pPr><w:r w:rsidRPr="00AC4739"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="16"/><w:szCs w:val="16"/></w:rPr><w:t>AUTORIZACIONES</w:t></w:r></w:p></w:tc></w:tr>';
	foreach ($listado as $item) {
	$existe=true;
	$res.='<w:tr w:rsidR="006D7388" w:rsidRPr="009522F0" w14:paraId="798FEF67" w14:textId="77777777" w:rsidTr="00AC4739"><w:tc><w:tcPr><w:tcW w:w="3368" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="7730FF7D" w14:textId="7B5BEFDF" w:rsidR="004E24C6" w:rsidRPr="009522F0" w:rsidRDefault="005048B0" w:rsidP="00534B96"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['nombre'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="2DC64A3C" w14:textId="344053B2" w:rsidR="004E24C6" w:rsidRPr="009522F0" w:rsidRDefault="005048B0" w:rsidP="00534B96"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['nif'].'</w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2534" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="2B1AA1F6" w14:textId="060F4C51" w:rsidR="004E24C6" w:rsidRPr="009522F0" w:rsidRDefault="005048B0" w:rsidP="00534B96"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['cargo'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1752" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="37C41CC1" w14:textId="13EF7F6B" w:rsidR="004E24C6" w:rsidRPr="009522F0" w:rsidRDefault="005048B0" w:rsidP="00534B96"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['acceso'].'</w:t></w:r></w:p></w:tc></w:tr>';
	}
	if(!$existe){
	$res.='<w:tr w:rsidR="006D7388" w:rsidRPr="009522F0" w14:paraId="798FEF67" w14:textId="77777777" w:rsidTr="00AC4739"><w:tc><w:tcPr><w:tcW w:w="3368" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="7730FF7D" w14:textId="7B5BEFDF" w:rsidR="004E24C6" w:rsidRPr="009522F0" w:rsidRDefault="005048B0" w:rsidP="00534B96"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="2DC64A3C" w14:textId="344053B2" w:rsidR="004E24C6" w:rsidRPr="009522F0" w:rsidRDefault="005048B0" w:rsidP="00534B96"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2534" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="2B1AA1F6" w14:textId="060F4C51" w:rsidR="004E24C6" w:rsidRPr="009522F0" w:rsidRDefault="005048B0" w:rsidP="00534B96"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1752" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="37C41CC1" w14:textId="13EF7F6B" w:rsidR="004E24C6" w:rsidRPr="009522F0" w:rsidRDefault="005048B0" w:rsidP="00534B96"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr>';
	}


	$res.='</w:tbl>';

	return $res;
}

function tablaUsuariosLOPD2($consultoria){
	$existe=false;
	$i=86;
	$listado=array();
	$indice=0;
	while($i<=156){
		if($consultoria['pregunta'.$i] != ''){
			$usuario=array();
			$usuario['nombre'] = $consultoria['pregunta'.$i];
			$i = $i+3;
			$usuario['ficheros']=$consultoria['pregunta'.$i];
			$listado[$indice]=$usuario;
			$indice++;
			$i = $i+3;
		} else {
			$i=$i+6;
		}
	}
	$res='<w:tbl><w:tblPr><w:tblW w:w="9900" w:type="dxa"/><w:tblInd w:w="-698" w:type="dxa"/><w:tblLayout w:type="fixed"/><w:tblCellMar><w:left w:w="70" w:type="dxa"/><w:right w:w="70" w:type="dxa"/></w:tblCellMar><w:tblLook w:val="0000" w:firstRow="0" w:lastRow="0" w:firstColumn="0" w:lastColumn="0" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="1980"/><w:gridCol w:w="1800"/><w:gridCol w:w="1980"/><w:gridCol w:w="1800"/><w:gridCol w:w="2340"/></w:tblGrid><w:tr w:rsidR="00216438" w:rsidRPr="00B81C42" w14:paraId="481D8BD6" w14:textId="77777777" w:rsidTr="00C74D53"><w:trPr><w:trHeight w:val="242"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1980" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="FFFF00"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="33A8E755" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr><w:t>NOMBRE Y APELLIDOS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1800" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="FFFF00"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="71BF8636" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr><w:t>PUESTO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1980" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="FFFF00"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5B8CB9C1" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr><w:t>FICHEROS AUTORIZADOS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1800" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="FFFF00"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="40BF0FEF" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr><w:t>FECHA ALTA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2340" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="FFFF00"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7CD31019" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/></w:rPr><w:t>FECHA BAJA</w:t></w:r></w:p></w:tc></w:tr>';
	foreach ($listado as $item) {
	$existe=true;
	$res.='<w:tr w:rsidR="00216438" w:rsidRPr="00B81C42" w14:paraId="7861E240" w14:textId="77777777" w:rsidTr="00C74D53"><w:trPr><w:trHeight w:val="336"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1980" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="308A7B37" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4BA5CB17" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00A23E63" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t>'.$item['nombre'].'</w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1800" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="326D1097" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1980" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="02BA122E" w14:textId="3F79E5D5" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t>'.$item['ficheros'].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1800" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="37582316" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2340" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4B46DB8F" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr>';
	}
	if(!$existe){
	$res.='<w:tr w:rsidR="00216438" w:rsidRPr="00B81C42" w14:paraId="7861E240" w14:textId="77777777" w:rsidTr="00C74D53"><w:trPr><w:trHeight w:val="336"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1980" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="308A7B37" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr></w:p><w:p w14:paraId="4BA5CB17" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00A23E63" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1800" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="326D1097" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1980" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="02BA122E" w14:textId="3F79E5D5" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1800" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="37582316" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2340" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="6" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="6" w:space="0" w:color="auto"/></w:tcBorders><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4B46DB8F" w14:textId="77777777" w:rsidR="00216438" w:rsidRPr="00B81C42" w:rsidRDefault="00216438" w:rsidP="008764F6"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r w:rsidRPr="00B81C42"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr>';
	}


	$res.='</w:tbl>';

	return $res;
}

function tablaAplicacionesLOPD($consultoria){
	$existe=false;
	$i=357;
	$listado=array();
	$indice=0;
	while($i<=392){
		if($consultoria['pregunta'.$i] != ''){
			$usuario=array();
			$usuario['aplicacion'] = $consultoria['pregunta'.$i];
			$i++;
			$usuario['finalidad'] = $consultoria['pregunta'.$i];
			$i = $i+2;
			$listado[$indice]=$usuario;
			$indice++;
		} else {
			$i=$i+3;
		}
	}
	$res='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblInd w:w="1951" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="2410"/><w:gridCol w:w="6628"/></w:tblGrid><w:tr w:rsidR="000A5D3F" w:rsidRPr="009522F0" w14:paraId="66FE9820" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="9038" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="23A0B2F8" w14:textId="77777777" w:rsidR="000A5D3F" w:rsidRPr="009522F0" w:rsidRDefault="000A5D3F" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>APLICACIONES CON TRATAMIENTO DE DATOS DE CARÁCTER PERSONAL</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="000A5D3F" w:rsidRPr="009522F0" w14:paraId="21472E44" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2410" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="501F0121" w14:textId="77777777" w:rsidR="000A5D3F" w:rsidRPr="009522F0" w:rsidRDefault="000A5D3F" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>APLICACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="6628" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="507B3258" w14:textId="77777777" w:rsidR="000A5D3F" w:rsidRPr="009522F0" w:rsidRDefault="000A5D3F" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>FINALIDAD</w:t></w:r></w:p></w:tc></w:tr>';
	foreach ($listado as $item) {
	$existe=true;
	$res.='<w:tr w:rsidR="000A5D3F" w:rsidRPr="009522F0" w14:paraId="1098C7A4" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2410" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="42C6A771" w14:textId="43906954" w:rsidR="000A5D3F" w:rsidRPr="009522F0" w:rsidRDefault="00473715" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:tabs><w:tab w:val="left" w:pos="1775"/></w:tabs><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['aplicacion'].'</w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="6628" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="5A12717E" w14:textId="077D4632" w:rsidR="000A5D3F" w:rsidRPr="009522F0" w:rsidRDefault="00473715" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['finalidad'].'</w:t></w:r></w:p></w:tc></w:tr>';
	}
	if(!$existe) {
	$res.='<w:tr w:rsidR="000A5D3F" w:rsidRPr="009522F0" w14:paraId="1098C7A4" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2410" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="42C6A771" w14:textId="43906954" w:rsidR="000A5D3F" w:rsidRPr="009522F0" w:rsidRDefault="00473715" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:tabs><w:tab w:val="left" w:pos="1775"/></w:tabs><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:proofErr w:type="spellStart"/><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:proofErr w:type="spellEnd"/></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="6628" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="5A12717E" w14:textId="077D4632" w:rsidR="000A5D3F" w:rsidRPr="009522F0" w:rsidRDefault="00473715" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="240" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr>';
	}


	$res.='</w:tbl>';

	return $res;
}

function tablaResumenFicheros($consultoria){
	$existe=false;
	$preguntas=array(20,31,42,53,64,75,443,454,465,476,487);
	$ficheros=array('20'=>'CLIENTES','31'=>'PROVEEDORES','42'=>'EMPLEADOS','53'=>'CURRÍCULUMS','64'=>'VIDEOVIGILANCIA','75'=>'USUARIOS WEB','443'=>'PACIENTES','454'=>'SOCIOS','465'=>'ALUMNOS','476'=>'VOLUNTARIOS','487'=>$consultoria['pregunta488']);
	$res='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblInd w:w="2235" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="2268"/><w:gridCol w:w="1417"/><w:gridCol w:w="3119"/><w:gridCol w:w="1275"/></w:tblGrid><w:tr w:rsidR="00B43B48" w:rsidRPr="00B43B48" w14:paraId="1C47A879" w14:textId="77777777" w:rsidTr="009522F0"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="4"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="00857E37" w14:textId="77777777" w:rsidR="00B43B48" w:rsidRPr="00E26639" w:rsidRDefault="00B43B48" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>TABLA RESUMEN DE FICHEROS</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="004E24C6" w:rsidRPr="00B43B48" w14:paraId="475FFA18" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="668"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0416EF0C" w14:textId="77777777" w:rsidR="00B43B48" w:rsidRPr="00E26639" w:rsidRDefault="00B43B48" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>NOMBRE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1417" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7EDD4A1A" w14:textId="77777777" w:rsidR="00B43B48" w:rsidRPr="00E26639" w:rsidRDefault="00B43B48" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>Fecha de inscripción</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3119" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A70CC7C" w14:textId="77777777" w:rsidR="00B43B48" w:rsidRPr="00E26639" w:rsidRDefault="00B43B48" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>Código Inscripción</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1275" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="246164AD" w14:textId="77777777" w:rsidR="00B43B48" w:rsidRPr="00E26639" w:rsidRDefault="00B43B48" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>Nivel de Seguridad</w:t></w:r></w:p></w:tc></w:tr>';
	foreach ($preguntas as $pregunta){
		if($consultoria['pregunta'.$pregunta]=='SI'){
		$existe=true;
		$res.='<w:tr w:rsidR="004E24C6" w:rsidRPr="00B43B48" w14:paraId="52CA39A4" w14:textId="77777777" w:rsidTr="00995AD5"><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="480A3BDE" w14:textId="63D3EF3C" w:rsidR="00B43B48" w:rsidRPr="00B43B48" w:rsidRDefault="00755AE2" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t>'.$ficheros[$pregunta].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1417" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="4BA7829F" w14:textId="5DCBD35D" w:rsidR="00B43B48" w:rsidRPr="00B43B48" w:rsidRDefault="00755AE2" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3119" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="744AF8D5" w14:textId="7B883027" w:rsidR="00B43B48" w:rsidRPr="00B43B48" w:rsidRDefault="00755AE2" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1275" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="7B38F2DE" w14:textId="1838A10D" w:rsidR="00B43B48" w:rsidRPr="00B43B48" w:rsidRDefault="00755AE2" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="60" w:name="_GoBack"/><w:bookmarkEnd w:id="60"/><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t></w:t></w:r></w:p></w:tc></w:tr>';
		}
	}
	if(!$existe){
		$res.='<w:tr w:rsidR="004E24C6" w:rsidRPr="00B43B48" w14:paraId="52CA39A4" w14:textId="77777777" w:rsidTr="00995AD5"><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="480A3BDE" w14:textId="63D3EF3C" w:rsidR="00B43B48" w:rsidRPr="00B43B48" w:rsidRDefault="00755AE2" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1417" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="4BA7829F" w14:textId="5DCBD35D" w:rsidR="00B43B48" w:rsidRPr="00B43B48" w:rsidRDefault="00755AE2" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3119" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="744AF8D5" w14:textId="7B883027" w:rsidR="00B43B48" w:rsidRPr="00B43B48" w:rsidRDefault="00755AE2" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1275" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="7B38F2DE" w14:textId="1838A10D" w:rsidR="00B43B48" w:rsidRPr="00B43B48" w:rsidRDefault="00755AE2" w:rsidP="00534B96"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="60" w:name="_GoBack"/><w:bookmarkEnd w:id="60"/><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t></w:t></w:r></w:p></w:tc></w:tr>';
	}
	$res.='</w:tbl>';
	return $res;
}

function tablasFicheros($consultoria, $cliente, $direccion){
	$existe=false;
	$preguntas=array(20,31,42,53,64,75,443,454,465,476,487);
	$ficheros=array('20'=>'CLIENTES','31'=>'PROVEEDORES','42'=>'EMPLEADOS','53'=>'CURRÍCULUMS','64'=>'VIDEOVIGILANCIA','75'=>'USUARIOS WEB','443'=>'PACIENTES','454'=>'SOCIOS','465'=>'ALUMNOS','476'=>'VOLUNTARIOS','487'=>$consultoria['pregunta488']);
	$numerosRomanos=array('I','II','III','IV','V','VI','VII','VIII','IX','X','XI');
	$indiceNumeros=0;
	$res='';
	foreach ($preguntas as $pregunta){
		if($consultoria['pregunta'.$pregunta]=='SI'){
		$existe=true;
		$encargado = $cliente;
		if($pregunta == 20)
			$encargado = $consultoria['pregunta241'];
		if($pregunta == 64)
			$encargado = $consultoria['pregunta179'];
		$tipo=$pregunta+1;
		if($pregunta==487){
			$tipo++;
		}
		$res.='<w:tbl><w:tblPr><w:tblW w:w="8079" w:type="dxa"/><w:tblInd w:w="2235" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="3544"/><w:gridCol w:w="4535"/></w:tblGrid><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="27267A7C" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0085C6C1" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>FICHERO '.$numerosRomanos[$indiceNumeros].': '.$ficheros[$pregunta].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="38B64FFE" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A35AC83" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>DENOMINACIÓN DEL FICHERO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4827C4D1" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>ENCARGADO DE TRATAMIENTO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="4BFD7CB1" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="45279F23" w14:textId="378E4580" w:rsidR="00524AD8" w:rsidRPr="00C373E1" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$ficheros[$pregunta].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="1C10CFB8" w14:textId="70862F8C" w:rsidR="00524AD8" w:rsidRPr="00C373E1" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t>'.$encargado.'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="43E8C354" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:tcBorders><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="56A56AA4" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00956FC5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>NIVEL DE SEGURIDAD</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:tcBorders><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="22F8F557" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>RESPONSABLE DEL FICHERO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="0C664835" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="539A343F" w14:textId="66DB103C" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5DD340B5" w14:textId="7D0AA9BB" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$cliente.'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00956FC5" w:rsidRPr="00B43B48" w14:paraId="7AA7A5E5" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4AECD4AC" w14:textId="77777777" w:rsidR="00956FC5" w:rsidRPr="00E26639" w:rsidRDefault="00956FC5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>DIRECCIÓN DE ACCESO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00956FC5" w:rsidRPr="00B43B48" w14:paraId="2C7988D2" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0C94331D" w14:textId="1D1E6645" w:rsidR="00956FC5" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$direccion.'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="2BA6F01A" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4D82BD59" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>DESCRIPCIÓN DEL FICHERO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="7E291EF6" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="3E55D715" w14:textId="324617F2" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$ficheros[$pregunta].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="2CADBEAF" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A95B782" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>FINES Y USOS PREVISTOS</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="5EC88096" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="20790BDA" w14:textId="46115F34" w:rsidR="00524AD8" w:rsidRPr="00C373E1" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>GESTIÓN DE '.$ficheros[$pregunta].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="29CED5F9" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4CF9E4E4" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>SISTEMAS DE TRATAMIENTO DE LA INFORMACIÓN</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="62B2C7B7" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7EF36E72" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/><w:tab w:val="left" w:pos="1417"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:left="1843" w:right="-284" w:hanging="1951"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>TIPO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5BE10612" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>UBICACIÓN PAPEL</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="5A496BA9" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:trPr><w:trHeight w:val="526"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7FA495E3" w14:textId="237E21E3" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$consultoria['pregunta'.$tipo].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="68172092" w14:textId="38E6CCB2" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="60" w:name="_GoBack"/><w:bookmarkEnd w:id="60"/></w:p></w:tc></w:tr></w:tbl>';
		$indiceNumeros++;
		}
	}

	if(!$existe){
		$res.='<w:tbl><w:tblPr><w:tblW w:w="8079" w:type="dxa"/><w:tblInd w:w="2235" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="3544"/><w:gridCol w:w="4535"/></w:tblGrid><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="27267A7C" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0085C6C1" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>FICHERO I</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="38B64FFE" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A35AC83" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>DENOMINACIÓN DEL FICHERO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4827C4D1" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>ENCARGADO DE TRATAMIENTO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="4BFD7CB1" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="45279F23" w14:textId="378E4580" w:rsidR="00524AD8" w:rsidRPr="00C373E1" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="1C10CFB8" w14:textId="70862F8C" w:rsidR="00524AD8" w:rsidRPr="00C373E1" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="43E8C354" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:tcBorders><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="56A56AA4" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00956FC5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>NIVEL DE SEGURIDAD</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:tcBorders><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="22F8F557" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>RESPONSABLE DEL FICHERO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="0C664835" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="539A343F" w14:textId="66DB103C" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5DD340B5" w14:textId="7D0AA9BB" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00956FC5" w:rsidRPr="00B43B48" w14:paraId="7AA7A5E5" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4AECD4AC" w14:textId="77777777" w:rsidR="00956FC5" w:rsidRPr="00E26639" w:rsidRDefault="00956FC5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>DIRECCIÓN DE ACCESO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00956FC5" w:rsidRPr="00B43B48" w14:paraId="2C7988D2" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0C94331D" w14:textId="1D1E6645" w:rsidR="00956FC5" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="2BA6F01A" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4D82BD59" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>DESCRIPCIÓN DEL FICHERO</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="7E291EF6" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="3E55D715" w14:textId="324617F2" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="2CADBEAF" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A95B782" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>FINES Y USOS PREVISTOS</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="5EC88096" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="20790BDA" w14:textId="46115F34" w:rsidR="00524AD8" w:rsidRPr="00C373E1" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>GESTIÓN DE </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="29CED5F9" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="8079" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4CF9E4E4" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>SISTEMAS DE TRATAMIENTO DE LA INFORMACIÓN</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="62B2C7B7" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7EF36E72" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/><w:tab w:val="left" w:pos="1417"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:left="1843" w:right="-284" w:hanging="1951"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>TIPO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5BE10612" w14:textId="77777777" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="00524AD8" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00E26639"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>UBICACIÓN PAPEL</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00524AD8" w:rsidRPr="00B43B48" w14:paraId="5A496BA9" w14:textId="77777777" w:rsidTr="00AA4AEA"><w:trPr><w:trHeight w:val="526"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="3544" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7FA495E3" w14:textId="237E21E3" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4535" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="68172092" w14:textId="38E6CCB2" w:rsidR="00524AD8" w:rsidRPr="00E26639" w:rsidRDefault="000D37C5" w:rsidP="00AA4AEA"><w:pPr><w:tabs><w:tab w:val="left" w:pos="1193"/></w:tabs><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="60" w:name="_GoBack"/><w:bookmarkEnd w:id="60"/></w:p></w:tc></w:tr></w:tbl>';
	}
	return $res;
}

function tablaSoportes($consultoria){
	$i=86;
	$listado=array();
	$indice=0;
	$existe=false;
	while($i<=156){
		if($consultoria['pregunta'.$i] != ''){
			$usuario=array();
			$usuario['nombre'] = $consultoria['pregunta'.$i];
			$i = $i+4;
			$usuario['soporte']=$consultoria['pregunta'.$i];
			$listado[$indice]=$usuario;
			$indice++;
			$i = $i+2;
		} else {
			$i=$i+6;
		}
	}
	$res='<w:tbl><w:tblPr><w:tblW w:w="0" w:type="auto"/><w:tblInd w:w="1951" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="3969"/><w:gridCol w:w="3686"/></w:tblGrid><w:tr w:rsidR="006D7388" w:rsidRPr="009522F0" w14:paraId="14DBA3DD" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="490"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="3969" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="14DA74CB" w14:textId="77777777" w:rsidR="000F23C8" w:rsidRPr="009522F0" w:rsidRDefault="000F23C8" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120" w:line="360" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>USUARIO AUTORIZADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3686" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="1BBF4C37" w14:textId="77777777" w:rsidR="000F23C8" w:rsidRPr="009522F0" w:rsidRDefault="000F23C8" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120" w:line="360" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>SOPORTES / DOCUMENTOS</w:t></w:r></w:p></w:tc></w:tr>';
	foreach ($listado as $item) {
	$existe=true;
	$res.='<w:tr w:rsidR="000F23C8" w:rsidRPr="009522F0" w14:paraId="328B1383" w14:textId="77777777" w:rsidTr="009522F0"><w:tc><w:tcPr><w:tcW w:w="3969" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="492A0C82" w14:textId="397BA8E3" w:rsidR="000F23C8" w:rsidRPr="009522F0" w:rsidRDefault="00DB38A6" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120" w:line="360" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['nombre'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3686" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="6D2EAC0B" w14:textId="580ADEE3" w:rsidR="000F23C8" w:rsidRPr="009522F0" w:rsidRDefault="00DB38A6" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120" w:line="360" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['soporte'].'</w:t></w:r><w:bookmarkStart w:id="70" w:name="_GoBack"/><w:bookmarkEnd w:id="70"/></w:p></w:tc></w:tr>';
	}
	if(!$existe){
		$res.='<w:tr w:rsidR="000F23C8" w:rsidRPr="009522F0" w14:paraId="328B1383" w14:textId="77777777" w:rsidTr="009522F0"><w:tc><w:tcPr><w:tcW w:w="3969" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="492A0C82" w14:textId="397BA8E3" w:rsidR="000F23C8" w:rsidRPr="009522F0" w:rsidRDefault="00DB38A6" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120" w:line="360" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3686" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="6D2EAC0B" w14:textId="580ADEE3" w:rsidR="000F23C8" w:rsidRPr="009522F0" w:rsidRDefault="00DB38A6" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120" w:line="360" w:lineRule="auto"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="000000"/><w:spacing w:val="-1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="70" w:name="_GoBack"/><w:bookmarkEnd w:id="70"/></w:p></w:tc></w:tr>';
	}


	$res.='</w:tbl>';

	return $res;
}

function tablaEncargadosTratamiento($consultoria){
	$existe=false;
	$preguntas=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290);
	$servicios=array('158'=>'REALIZACIÓN DE NÓMINAS','168'=>'ASESORÍA CONTABLE/FISCAL','178'=>'VIDEOVIGILANCIA','189'=>'PRL','200'=>'MANTENIMIENTO DE EQUIPOS INFORMÁTICOS','210'=>'MANTENIMIENTO DE APLICACIONES','220'=>'MANTENIMIENTO DE LA PÁGINA WEB','230'=>'COPIAS DE SEGURIDAD','240'=>'TRANSPORTE/MENSAJERÍA PARA ENVÍOS A CLIENTES','250'=>'LIMPIEZA','260'=>'MANTENIMIENTO DE INSTALACIONES','270'=>'VIGILANCIA Y SEGURIDAD','280'=>'MANTENIMIENTO DE LA COPIADORA','290'=>'ACCESO A DATOS DE CARÁCTER PERSONAL');
	$res='';

	foreach ($preguntas as $pregunta){
		if($consultoria['pregunta'.$pregunta]=='SI'){
			$existe=true;
			$res.='<w:tbl><w:tblPr><w:tblW w:w="8080" w:type="dxa"/><w:tblInd w:w="2660" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="2977"/><w:gridCol w:w="5103"/></w:tblGrid><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="2E228759" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="357"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="50FE4502" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>ENCARGADO:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="45AA3791" w14:textId="2F6488DB" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$consultoria['pregunta'.($pregunta+1)].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="4F470313" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7DAF3AB8" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>NIF/CIF:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="466C996F" w14:textId="5E70E2C5" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$consultoria['pregunta'.($pregunta+2)].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="198C7B61" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="724DE23D" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>DIRECCIÓN:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="30ED4568" w14:textId="37DB863B" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$consultoria['pregunta'.($pregunta+3)].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="2A970883" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="3D65EE7D" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>ACCESO:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A7AF85F" w14:textId="683915AE" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="31FB3CF6" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7F2E65AC" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="600"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>SERVICIOS QUE PRESTA:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="31FF80C1" w14:textId="7101905F" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$servicios[$pregunta].'</w:t></w:r><w:bookmarkStart w:id="76" w:name="_GoBack"/><w:bookmarkEnd w:id="76"/></w:p></w:tc></w:tr></w:tbl>';
		}
	}
	if(!$existe){
			$res.='<w:tbl><w:tblPr><w:tblW w:w="8080" w:type="dxa"/><w:tblInd w:w="2660" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="2977"/><w:gridCol w:w="5103"/></w:tblGrid><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="2E228759" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="357"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="50FE4502" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>ENCARGADO:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="45AA3791" w14:textId="2F6488DB" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="4F470313" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7DAF3AB8" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>NIF/CIF:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="466C996F" w14:textId="5E70E2C5" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="198C7B61" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="724DE23D" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>DIRECCIÓN:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="30ED4568" w14:textId="37DB863B" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="2A970883" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="3D65EE7D" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="120"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>ACCESO:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A7AF85F" w14:textId="683915AE" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="003B1869" w:rsidRPr="009522F0" w14:paraId="31FB3CF6" w14:textId="77777777" w:rsidTr="009522F0"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="2977" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7F2E65AC" w14:textId="77777777" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003B1869" w:rsidP="009522F0"><w:pPr><w:spacing w:before="120" w:after="600"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>SERVICIOS QUE PRESTA:</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="5103" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="31FF80C1" w14:textId="7101905F" w:rsidR="003B1869" w:rsidRPr="009522F0" w:rsidRDefault="003A6FD6" w:rsidP="009522F0"><w:pPr><w:spacing w:after="0"/><w:ind w:right="-284"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:bCs/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="76" w:name="_GoBack"/><w:bookmarkEnd w:id="76"/></w:p></w:tc></w:tr></w:tbl>';
		}
	return $res;
}

function tablaPortatiles($consultoria){
	$existe=false;
	$sale = $consultoria['pregunta331'];
	$i=333;
	$listado=array();
	$indice=0;
	while($i<=356){
		if($consultoria['pregunta'.$i] != ''){
			$usuario=array();
			$usuario['nombre'] = $consultoria['pregunta'.$i];
			$i = $i+2;
			$usuario['accesos']=$consultoria['pregunta'.$i];
			$listado[$indice]=$usuario;
			$indice++;
			$i = $i+2;
		} else {
			$i=$i+4;
		}
	}
	$res='<w:tbl><w:tblPr><w:tblW w:w="9521" w:type="dxa"/><w:tblInd w:w="1668" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1983"/><w:gridCol w:w="2034"/><w:gridCol w:w="2040"/><w:gridCol w:w="1990"/><w:gridCol w:w="1474"/></w:tblGrid><w:tr w:rsidR="006D7388" w:rsidRPr="009522F0" w14:paraId="46C19A80" w14:textId="77777777" w:rsidTr="00534B96"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1983" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="2C653619" w14:textId="77777777" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="00F00E6D" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>TIPO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2034" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="41D925B0" w14:textId="77777777" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="00F00E6D" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>MODELO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2040" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="4287AB27" w14:textId="77777777" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="00F00E6D" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>USUARIO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1990" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="69BDC551" w14:textId="77777777" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="00F00E6D" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:ind w:hanging="212"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>SALE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1474" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4"/></w:tcPr><w:p w14:paraId="30479F93" w14:textId="77777777" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="00F00E6D" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:ind w:left="-76"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="009522F0"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>AUTORIZADO</w:t></w:r></w:p></w:tc></w:tr>';
	foreach ($listado as $item) {
	$existe=true;
	$res.='<w:tr w:rsidR="006D7388" w:rsidRPr="009522F0" w14:paraId="25DADE39" w14:textId="77777777" w:rsidTr="00534B96"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1983" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="0C510103" w14:textId="00973E6C" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2034" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="1AA081C5" w14:textId="07AA8EFD" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2040" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="1A9F2674" w14:textId="14A84E05" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t>'.$item['nombre'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1990" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="6692F958" w14:textId="0006A9C2" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t>'.$sale.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1474" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="2F749091" w14:textId="1AAB172D" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t>'.$item['accesos'].'</w:t></w:r></w:p></w:tc></w:tr>';
	}

	if(!$existe){
	$res.='<w:tr w:rsidR="006D7388" w:rsidRPr="009522F0" w14:paraId="25DADE39" w14:textId="77777777" w:rsidTr="00534B96"><w:trPr><w:trHeight w:val="20"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1983" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="0C510103" w14:textId="00973E6C" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2034" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="1AA081C5" w14:textId="07AA8EFD" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2040" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="1A9F2674" w14:textId="14A84E05" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1990" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="6692F958" w14:textId="0006A9C2" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1474" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w14:paraId="2F749091" w14:textId="1AAB172D" w:rsidR="00F00E6D" w:rsidRPr="009522F0" w:rsidRDefault="004464CE" w:rsidP="00534B96"><w:pPr><w:widowControl w:val="0"/><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:before="120" w:after="120" w:line="220" w:lineRule="exact"/><w:jc w:val="both"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr>';
	}


	$res.='</w:tbl>';

	return $res;
}

function tablaSoportesDS($datos){
	$existe=false;
	$soportes = consultaBD("SELECT * FROM soportes_lopd WHERE codigoTrabajo=".$datos['codigo'],true);
	$listado=array();
	$indice=0;
	while($item=mysql_fetch_assoc($soportes)){
			$soporte=array();
			$soporte['soporte'] = $item['nombreSoporteLOPD'];
			$soporte['marca']=$item['marcaSoporteLOPD'];
			$soporte['serie']=$item['serieSoporteLOPD'];
			$soporte['fecha']=formateaFechaWeb($item['fechaSoporteLOPD']);
			$soporte['ubicacion']=$item['ubicacionSoporteLOPD'];
			$soporte['so']=$item['sistemaSoporteLOPD'];
			$soporte['aplicaciones']=$item['aplicacionesSoporteLOPD'];
			$soporte['conexiones']=$item['conexionesSoporteLOPD'];
			$soporte['responsable']=$item['responsableSoporteLOPD'];
			$listado[$indice]=$soporte;
			$indice++;
	}
	$res='';
	foreach ($listado as $item) {
	$existe=true;
	$res.='<w:tbl><w:tblPr><w:tblW w:w="10139" w:type="dxa"/><w:tblInd w:w="851" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1809"/><w:gridCol w:w="1559"/><w:gridCol w:w="1559"/><w:gridCol w:w="1832"/><w:gridCol w:w="11"/><w:gridCol w:w="1679"/><w:gridCol w:w="1690"/></w:tblGrid><w:tr w:rsidR="00DC7EEE" w:rsidRPr="009522F0" w14:paraId="7A38BAC0" w14:textId="77777777" w:rsidTr="00DC7EEE"><w:trPr><w:trHeight w:val="649"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1809" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5DA4FA59" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3118" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="1D250F96" w14:textId="0E7E0C48" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['soporte'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1843" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="31D3AC82" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>RESPONSABLE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3369" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="208DC7DA" w14:textId="5CE8BD6C" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['responsable'].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00DC7EEE" w:rsidRPr="009522F0" w14:paraId="0F45FA35" w14:textId="77777777" w:rsidTr="00DC7EEE"><w:trPr><w:trHeight w:val="649"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1809" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="6D615FAB" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>FECHA ADQUISICIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3118" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4BA01024" w14:textId="08917506" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['fecha'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1843" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="56981A01" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>UBICACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3369" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="242A8BE8" w14:textId="1A033883" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['ubicacion'].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00DC7EEE" w:rsidRPr="009522F0" w14:paraId="21A45BC9" w14:textId="77777777" w:rsidTr="00DC7EEE"><w:trPr><w:trHeight w:val="649"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1809" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A9C7C34" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>MARCA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="30558C2A" w14:textId="7D89FD64" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['marca'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5CEF06B7" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>MODELO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1832" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="58E1840E" w14:textId="678EF553" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1690" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5E93E44D" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>Nº SERIE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1690" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="6AAA484B" w14:textId="2FFCC359" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['serie'].'</w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w14:paraId="2CA129F7" w14:textId="77777777" w:rsidTr="00DC7EEE"><w:trPr><w:trHeight w:val="649"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1809" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="3961EF22" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>S. OPERATIVO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7D1FBF45" w14:textId="72CA4087" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['so'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="45F9A4B0" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>APLICACIONES</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1832" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="152FE010" w14:textId="2E9E85B4" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['aplicaciones'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1690" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="68B37D68" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>CONEXIONES REMOTAS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1690" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="48DEF671" w14:textId="127C2548" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>'.$item['conexiones'].'</w:t></w:r><w:bookmarkStart w:id="72" w:name="_GoBack"/><w:bookmarkEnd w:id="72"/></w:p></w:tc></w:tr></w:tbl>';
	}
	if(!$existe) {
	$res.='<w:tbl><w:tblPr><w:tblW w:w="10139" w:type="dxa"/><w:tblInd w:w="851" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1809"/><w:gridCol w:w="1559"/><w:gridCol w:w="1559"/><w:gridCol w:w="1832"/><w:gridCol w:w="11"/><w:gridCol w:w="1679"/><w:gridCol w:w="1690"/></w:tblGrid><w:tr w:rsidR="00DC7EEE" w:rsidRPr="009522F0" w14:paraId="7A38BAC0" w14:textId="77777777" w:rsidTr="00DC7EEE"><w:trPr><w:trHeight w:val="649"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1809" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5DA4FA59" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3118" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="1D250F96" w14:textId="0E7E0C48" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1843" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="31D3AC82" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>RESPONSABLE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3369" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="208DC7DA" w14:textId="5CE8BD6C" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00DC7EEE" w:rsidRPr="009522F0" w14:paraId="0F45FA35" w14:textId="77777777" w:rsidTr="00DC7EEE"><w:trPr><w:trHeight w:val="649"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1809" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="6D615FAB" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>FECHA ADQUISICIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3118" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="4BA01024" w14:textId="08917506" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1843" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="56981A01" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>UBICACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="3369" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="242A8BE8" w14:textId="1A033883" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00DC7EEE" w:rsidRPr="009522F0" w14:paraId="21A45BC9" w14:textId="77777777" w:rsidTr="00DC7EEE"><w:trPr><w:trHeight w:val="649"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1809" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="0A9C7C34" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>MARCA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="30558C2A" w14:textId="7D89FD64" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5CEF06B7" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>MODELO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1832" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="58E1840E" w14:textId="678EF553" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1690" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="5E93E44D" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>Nº SERIE</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1690" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="6AAA484B" w14:textId="2FFCC359" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc></w:tr><w:tr w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w14:paraId="2CA129F7" w14:textId="77777777" w:rsidTr="00DC7EEE"><w:trPr><w:trHeight w:val="649"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="1809" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="3961EF22" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>S. OPERATIVO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="7D1FBF45" w14:textId="72CA4087" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1559" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="45F9A4B0" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>APLICACIONES</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1832" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="152FE010" w14:textId="2E9E85B4" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1690" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="B8CCE4" w:themeFill="accent1" w:themeFillTint="66"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="68B37D68" w14:textId="77777777" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="00DC7EEE" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00DC7EEE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t>CONEXIONES REMOTAS</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1690" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="48DEF671" w14:textId="127C2548" w:rsidR="00DC7EEE" w:rsidRPr="00DC7EEE" w:rsidRDefault="0026412F" w:rsidP="00DC7EEE"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="120"/><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:color w:val="A6A6A6" w:themeColor="background1" w:themeShade="A6"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="72" w:name="_GoBack"/><w:bookmarkEnd w:id="72"/></w:p></w:tc></w:tr></w:tbl>';
	}


	return $res;
}

function tablaPuestos($empleados){
	$existe=false;
	$i=0;
	$listado=array();
	while($empleado=mysql_fetch_assoc($empleados)){
		if(!in_array($empleado['puesto'], $listado)){
			$listado[$i]=$empleado['puesto'];
			$i++;
		}
	}
	$res='<w:tbl><w:tblPr><w:tblW w:w="2970" w:type="dxa"/><w:tblInd w:w="108" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="01E0" w:firstRow="1" w:lastRow="1" w:firstColumn="1" w:lastColumn="1" w:noHBand="0" w:noVBand="0"/></w:tblPr><w:tblGrid><w:gridCol w:w="2970"/></w:tblGrid><w:tr w:rsidR="002D3562" w:rsidRPr="00704CAE" w14:paraId="7FDB7480" w14:textId="77777777" w:rsidTr="00422C45"><w:tc><w:tcPr><w:tcW w:w="2970" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="BFBFBF"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="73F7AE02" w14:textId="77777777" w:rsidR="002D3562" w:rsidRPr="00704CAE" w:rsidRDefault="002D3562" w:rsidP="004C46F2"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r w:rsidRPr="00704CAE"><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:b/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>Puesto de Trabajo</w:t></w:r></w:p></w:tc></w:tr>';
	foreach ($listado as $item) {
	$existe=true;
	$res.='<w:tr w:rsidR="002D3562" w:rsidRPr="00704CAE" w14:paraId="2F066781" w14:textId="77777777" w:rsidTr="002D3562"><w:tc><w:tcPr><w:tcW w:w="2970" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="21114811" w14:textId="44816EA3" w:rsidR="002D3562" w:rsidRPr="00704CAE" w:rsidRDefault="00791672" w:rsidP="004C46F2"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t>'.$item.'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';
	}

	if(!$existe) {
	$res.='<w:tr w:rsidR="002D3562" w:rsidRPr="00704CAE" w14:paraId="2F066781" w14:textId="77777777" w:rsidTr="002D3562"><w:tc><w:tcPr><w:tcW w:w="2970" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="21114811" w14:textId="44816EA3" w:rsidR="002D3562" w:rsidRPr="00704CAE" w:rsidRDefault="00791672" w:rsidP="004C46F2"><w:pPr><w:jc w:val="center"/><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';
	}


	$res.='</w:tbl>';

	return $res;
}

function creaPaginaAlergeno($cliente,$logo,$plato,$iconos){
	$alergenos=consultaBD("SELECT * FROM platos_alergenos_grupos WHERE codigoPlato=".$plato['codigo'],true);
	$listadoAlergenos='';
	while($alergeno=mysql_fetch_assoc($alergenos)){
		$alergeno=explode('&$&', $alergeno['alergenos']);
		for($i=0;$i<count($alergeno);$i++){
			$nombre=datosRegistro('alergenos',$alergeno[$i]);
			$listadoAlergenos.='<li>'.$nombre['nombre'].'</li>';
		}
	}
	$grupos=consultaBD("SELECT DISTINCT g.nombre FROM platos_alergenos_grupos p INNER JOIN grupo_alergenos g ON p.codigoGrupo=g.codigo WHERE codigoPlato=".$plato['codigo'],true);
	$listadoGrupos='';
	while($grupo=mysql_fetch_assoc($grupos)){
		$listadoGrupos.='<li>'.$grupo['nombre'].'</li>';
	}
	return "<page footer='page' backbottom='10mm'>
		<div class='cabecera2' id='platoUno'>
			<table>
				<tbody>
					<tr>
						<td class='a66'><b>".$cliente['razonSocial']."</b></td>
						<td class='a33' style='text-align:center;'>".$logo."</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br/><br/><br/>
		<div align='center'>
			<b>FICHA INFORMATIVA</b><br/><br/>
			Los ingredientes identificados en <span style='color:red;'>ROJO</span>  pueden producir alergias /intolerancias alimentarias. Información de obligada declaración por el Reglamento (UE) Nº 1169/2011.<br/><br/>

			<i>Specific allergen information. Allergens are identified in <span style='color:red;'>RED</span> color. Information offered to comply with Regulation N.1169/2011.</i><br/><br/>
		</div>
		<div class='ficha'>
		<b>".$plato['nombrePlato']."</b>  ".$iconos."<br/><br/>
		ALÉRGENOS / ALLERGENS<br/>
		<ul>
			".$listadoAlergenos."
		</ul>
		<br/>
		Este plato/producto contiene sustancias que pueden producir alergias e intolerancias clasificadas en los siguientes grupos:<br/><br/>

		<i>This food contains or uses ingredients or processing aids derived from allergen products which are listed below:</i>
		<ul>
			".$listadoGrupos."
		</ul>
		<br/>
		Nuestros procesos de elaboración de platos/productos y de selección de proveedores no contemplan medidas específicas que eviten la presencia de alguna otra sustancia alérgena, por lo que podrían estar presentes (incluso en cantidades mínimas) en este plato/producto.<br/><br/>

		<i>Our elaboration and providers selection processes don´t consider specific measures in order to avoid the presence of any other allergen in this food (even traces or small quantities).</i>

		</div>
		".pieAlergenos()."
	</page>";

}


function sanearCaracteres($texto){
	$texto = str_replace( '&', '&#38;', $texto);
	$texto = str_replace( '–', '&#45;', $texto);
	$texto = str_replace( '"', '', $texto);
	return $texto;
}

function obtenerAnexoO($val){
	if($val == 'SI'){
		$res = '  La    </w:t></w:r><w:r><w:rPr><w:b/></w:rPr><w:t> Agencia Española de Protección de Datos</w:t></w:r><w:r><w:rPr></w:rPr><w:t>, en virtud de la competencia que la LOPD le otorga, ha dictado la Instrucción 1/ 2006, de 8 de noviembre de 2006, por la que se que regula el tratamiento de imágenes de personas físicas identificadas o identificables con fines de vigilancia, a través de sistemas de cámaras y videocámaras.<w:br />    El objeto de la Instrucción Comprende la grabación, captación, transmisión, conservación, y almacenamiento de imágenes, incluida su reproducción o emisión en tiempo real, así como el tratamiento que resulte de los datos personales relacionados con ellas.<w:br />    Por el contrario, se excluyen de la Instrucción los datos personales grabados para uso doméstica y el tratamiento de imágenes por parte de las Fuerzas y Cuerpos de Seguridad, que está regulado por la Ley Orgánica 4/97, de 4 de agosto.<w:br />           - Los responsables que cuenten con sistemas de video vigilancia deberán cumplir con el deber de información previsto en la LOPD. A tal fin deberán colocar, en las zonas videovigiladas, al menos un distintivo informativo ubicado en lugar suficientemente visible, tanto en espacios abiertos como cerrados.<w:br />           Según se establece en la Instrucción el contenido y el diseño del distintivo informativo deberá de incluir una referencia a la </w:t></w:r><w:r><w:rPr><w:b/></w:rPr><w:t> "LEY ORGANICA 15/1999, DE PROTECCIÓN DE DATOS"</w:t></w:r><w:r><w:rPr></w:rPr><w:t>, incluirá una mención a la finalidad para la que se tratan los datos (</w:t></w:r><w:r><w:rPr><w:b/></w:rPr><w:t>"ZONA VIDEOVIGILADA"</w:t></w:r><w:r><w:rPr></w:rPr><w:t>), y una mención expresa a la identificación del responsable ante quien puedan ejercitarse los derechos de las personas en materia de Protección de Datos.<w:br />               - Sólo se considerará admisible la instalación de cámaras o videocámaras cuando la finalidad de vigilancia no pueda obtenerse mediante otros medios que, sin exigir esfuerzos desproporcionados, resulten menos intrusivos para la intimidad de las personas y para su derecho a la protección de datos de carácter personal.<w:br />               - Las cámaras y videocámaras instaladas en espacios privados no podrán obtener imágenes de espacios públicos salvo que resulte imprescindible para la finalidad de vigilancia que se pretende, o resulte imposible evitarlo por razón de la ubicación de aquéllas. En todo caso deberá evitarse cualquier tratamiento de datos innecesario para la finalidad perseguida.<w:br />               - Las imágenes sólo serán tratadas cuando sean adecuadas, pertinentes y no excesivas en relación con el ámbito y las finalidades determinadas, legítimas y explícitas, que hayan justificado la instalación de las cámaras o videocámaras. <w:br />';
	} else {
		$res = 'No se ha declarado el fichero de Videovigilancia';
	}

	return $res;
}

function contratosLaborales(){
	$res = '</w:t></w:r><w:r><w:rPr><w:b/></w:rPr><w:t> ANEXO AL CONTRATO LABORAL</w:t></w:r><w:r><w:rPr></w:rPr><w:t><w:br />
Anexo al contrato de trabajo entre PNEUMATICS EBRE, S.C.P., con domicilio en PASSEIG SANT ANTONI 19. MORA D´EBRE. 43740. TARRAGONA, con NIF: J55613459, y D./Dña. __________________________________ con NIF _________ (en adelante el TRABAJADOR) en el que se pacta entre las partes arriba indicadas, el tratamiento que recibirán los datos personales suministrados por el TRABAJADOR a la SOCIEDAD, así como el deber de confidencialidad respecto a la información a la que pudiera tener acceso el TRABAJADOR durante y después de la relación laboral con la SOCIEDAD.<w:br />
1.	En cumplimiento de lo establecido en Ley Orgánica 15/1999 de Protección de Datos de Carácter Personal, la SOCIEDAD informa que los datos personales suministrados por el TRABAJADOR serán incluidos en un fichero automatizado y manual de la SOCIEDAD y se utilizarán para finalidades relacionadas con su relación laboral tales como la elaboración de las nóminas, gestión del personal, prevención de riesgos laborales y cumplimiento de las obligaciones Fiscales y de Seguridad Social. Para tales fines, la SOCIEDAD podrá recabar, a lo largo de la relación laboral, todos los datos de carácter personal que sean necesarios, incluyendo aquellos que por su naturaleza impliquen un nivel de protección medio o alto. El TRABAJADOR consiente expresamente el tratamiento de sus datos personales por la SOCIEDAD con las finalidades indicadas. <w:br />
2.	En cumplimiento de la Ley 31/1995, de 8 de noviembre, de Prevención de Riesgos Laborales, la SOCIEDAD viene obligada a ofrecer al TRABAJADOR reconocimientos médicos para garantizar la vigilancia periódica de su salud. El sometimiento voluntario a estos reconocimientos, es decir, cuando no sean obligatorios conforme a la normativa indicada, implicará el consentimiento expreso del TRABAJADOR, a la cesión de sus datos identificativos a la entidad de vigilancia de la salud contratada por la SOCIEDAD. <w:br />
3.	La SOCIEDAD en ningún caso accederá al contenido de los reconocimientos médicos pero será informada por los servicios de prevención de riesgos laborales de la aptitud o no aptitud del TRABAJADOR para continuar con la relación laboral.<w:br />
4.	En el supuesto de que el TRABAJADOR aporte datos de terceros (por ejemplo familiares) para la gestión y ejecución de las obligaciones derivadas de su relación laboral con la SOCIEDAD, deberá con carácter previo a su suministro, informar a los interesados del contenido del presente documento y obtener su consentimiento para el tratamiento de sus datos.<w:br />
5.	El TRABAJADOR consiente expresamente que sus datos personales puedan ser cedidos a compañías aseguradoras elegidas por la SOCIEDAD, con la finalidad de contratar seguros de vida, seguros médicos, de accidentes u otros seguros colectivos para sus trabajadores, por imposición de convenio colectivo o por iniciativa de la SOCIEDAD.<w:br />
6.	Asimismo consiente expresamente que puedan ser cedidos a entidades bancarias para la gestión de los cobros y pagos derivados de su relación laboral con la SOCIEDAD.<w:br />
7.	El TRABAJADOR consiente expresamente que los datos de carácter personal incluidos en el TC1 Y TC2, que acreditan la correcta situación de las cotizaciones a la Seguridad Social del TRABAJADOR, una vez disociados del resto de los datos personales, puedan ser cedidos a aquellas empresas u organizaciones que contraten servicios de la SOCIEDAD que impliquen la intervención del TRABAJADOR en sus instalaciones.<w:br />
8.	El TRABAJADOR que voluntariamente decida acudir a los cursos de formación que se le propongan, consiente expresamente la cesión de los datos personales identificativos necesarios para impartir la formación a las entidades formativas seleccionadas por la SOCIEDAD, así como la cesión de los datos personales solicitados por las Administraciones Públicas competentes necesarios para acreditar la formación realizada y poder optar a las subvenciones y bonificaciones para la formación continua. <w:br />
9.	El TRABAJADOR consiente la conservación indefinida de sus datos de contacto en los FICHEROS de la SOCIEDAD, incluso una vez finalizada la relación laboral por cualquier causa, con el objeto de hacerle participar en futuros procesos de selección de personal organizados por la SOCIEDAD o por terceros, sin perjuicio de su derecho de cancelación que podrá ejercitar en todo momento en la dirección indicada en el presente acuerdo. Si no desea que sus datos de contacto puedan ser conservados indefinidamente y/o cedidos a terceros para hacerle participar en procesos de selección marque la siguiente casilla: No □.<w:br />
10.	La SOCIEDAD en ningún caso será responsable de la licitud, veracidad y exactitud de los datos personales suministrados por el TRABAJADOR. En el supuesto de producirse modificaciones en los datos del TRABAJADOR será responsabilidad exclusiva de éste, la notificación de éstas a la SOCIEDAD.<w:br />
11.	El TRABAJADOR, durante la vigencia del presente contrato de trabajo, y con posterioridad al mismo, no difundirá o divulgará a persona alguna ningún secreto comercial y/o empresarial o información relativa al negocio o asuntos de la SOCIEDAD, ni información de carácter personal alguna relativa a los demás empleados, clientes o relativa a cualquier otra persona relacionada con la SOCIEDAD.<w:br />
12.	El TRABAJADOR reconoce y acepta sin reservas que el uso de Internet, del correo electrónico así como de las herramientas, programas, soportes o equipos informáticos suministrados por la SOCIEDAD al TRABAJADOR son para uso exclusivamente  laboral o profesional. El empleado declara conocer que estos equipos y dispositivos, por motivos de seguridad, podrán ser en todo momento intervenidos por la SOCIEDAD sin notificación previa. <w:br />
13.	Una vez extinguida la relación laboral por cualquier causa y sin necesidad de mediar requerimiento verbal o escrito alguno de la SOCIEDAD, el TRABAJADOR devolverá a la SOCIEDAD todos los expedientes, notas, informes, lista de clientes y direcciones, programas informáticos, archivos, cuentas y demás documentos de cualquier clase, secretos comerciales, materiales, así como cualquier documento propiedad de la SOCIEDAD que sea considerado como confidencial por ésta.<w:br />
14.	Asimismo el TRABAJADOR consiente expresamente que sus datos identificativos, académicos y profesionales, incluida su imagen, puedan ser publicados en la web, medios de comunicación, catálogos comerciales, publicaciones o intranet de la SOCIEDAD con finalidad corporativa o comercial y puedan ser cedidos a los clientes de la SOCIEDAD que exijan acreditación de aptitud profesional para la prestación de los servicios contratados.<w:br />
15.	El TRABAJADOR ha sido informado de la colocación de cámaras de videovigilancia en la empresa y consiente su grabación con la única finalidad de seguridad de las instalaciones de [RAZON SOCIAL]<w:br />
16.	El TRABAJADOR podrá acceder, rectificar, cancelar u oponerse al tratamiento de sus datos en todo momento, ejercitando estos derechos en la siguiente dirección:  [Calle, nº. Localidad. CP. Provincia.<w:br />

Y para que conste, se extiende este documento por duplicado ejemplar en el lugar y fecha a continuación indicados, firmando las partes interesadas.<w:br />



En MORA D´EBRE, a de  de<w:br />
LA SOCIEDAD						EL TRABAJADOR<w:br />
';

	return $res;
}
?>
<?php
  $seccionActiva=10;
  include_once('../cabecera.php');
  
  operacionesTrabajos();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

       <?php
        creaBotonesGestionConsultorias();
       ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Consultorías registradas</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroConsultorias();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaConsultorias">
                <thead>
                  <tr>
                    <th> Cliente </th>
                    <th class='centro'>Servicios</th>
                    <th class="centro"></th>
                  </tr>
                </thead>
                <tbody>
                 
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  listadoTabla('#tablaConsultorias','../listadoAjax.php?include=consultoria&funcion=listadoTrabajos();');
  $('.cajaFiltros select').selectpicker();
  oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaConsultorias');

  $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
});

    
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
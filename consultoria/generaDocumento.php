<?php

@include_once('funciones.php');
	
	$documentos=array();
	$trabajo=consultaBD("SELECT t.codigo, s.referencia, t.formulario, s.codigoFamilia FROM trabajos t INNER JOIN servicios s ON t.codigoServicio=s.codigo WHERE t.codigo=".$_GET['codigoTrabajo'],true, true);
	if($trabajo['formulario'] != ''){
		$familia=datosRegistro('servicios_familias',$trabajo['codigoFamilia']);

		switch($familia['referencia']){
			case 'PRL1':
				$documentos[0]=generaPRL($trabajo['codigo']);
				$plantilla=array('2_Anexo_I_Formacion_e_infomacion.docx','3_Anexo_II_Plan_de_Emergencias.docx','4_Anexo_III_Inspecciones_de_seguridad.docx','5_Anexo_IV_Entrega_de_epis.docx','6_Anexo_V_Investigacion_accidentes.docx','7_Anexo_VI_Nombramiento_delegados_de_prevencion.docx','8_Anexo_VII_Aceptacion_reconocimientos_medicos.docx','9_Anexo_VIII_Comunicado_interno.docx','10_Anexo_IX_Entrega_de_documentacion_subcontratas_I.docx','11_Anexo_IX_Entrega_de_documentacion_subcontratas_II.docx');
				for($i=0;$i<count($plantilla);$i++){
					$documentos[($i+1)]=generaPRL2($trabajo['codigo'],$plantilla[$i]);
				}
				$documentos[$i]=generaPRL3($trabajo['codigo']);
				$documentos[++$i]=generaPRL2($trabajo['codigo'],'13_Anexo_XI_Entrega_de_EPIs.docx');
				$documentos[++$i]=generaPRL2($trabajo['codigo'],'14_Anexo_XII_Inspecciones_de_seguridad.docx');
				$documentos[++$i]=generaPRL4($trabajo['codigo'],'15_Introduccion_evaluacion_de_riesgos.docx');
				$documentos[++$i]=generaPRL5($trabajo['codigo'],'16_Evaluacion_de_Riesgos_ADMIN');
				$documentos[++$i]=generaPRL5($trabajo['codigo'],'17_Medidas_correctoras_ADMINISTRATIVO');
				break;
			case 'LOPD1':
				$documentos[0]=generaLOPD($trabajo['codigo'],'1_carta_plantilla.docx');
				$documentos[1]=generaLOPD($trabajo['codigo'],'2_certificado_lopd.docx');
				$documentos[2]=generaLOPD($trabajo['codigo'],'3_obligaciones_plantilla.docx');
				$documentos[3]=generaLOPD2($trabajo['codigo']);
				$documentos[4]=generaLOPD3($trabajo['codigo']);
				$documentos[5]=generaLOPD4($trabajo['codigo']);
				$documentos[6]=generaLOPD5($trabajo['codigo']);
				$documentos[7]=generaLOPD6($trabajo['codigo']);
				$documentos[8]=generaLOPD($trabajo['codigo'],'anexo_medidas_nivel_alto.docx');
				$documentos[9]=generaLOPD7($trabajo['codigo']);
				$documentos[10]=generaLOPD8($trabajo['codigo']);
				break;
			case 'ALE1':
				$documentos[0]=generaAlergeno2($trabajo['codigo'],'certificado_alergenos.docx');
				$documentos[1]=generaAlergenoPG($trabajo['codigo'],'pg_alergenos.pdf');
				$documentos[2]=generaAlergeno($trabajo['codigo'],'qr_alergenos.docx');
				break;

		}
		generaZip($documentos,$trabajo['referencia']);
	}


function generaZip($documentos,$referencia){
	$zip = new ZipArchive();
	
	$nameZip = '../documentos/consultorias/documentos_'.$referencia.'.zip';
	if(file_exists($nameZip)){
		unlink($nameZip);
	}
	$fichero = $nameZip;
	
	if($zip->open($fichero,ZIPARCHIVE::CREATE)===true) {
		foreach ($documentos as $documento){
			if(is_array($documento)){
				foreach ($documento as $doc){
					$name = str_replace('../documentos/consultorias/', '', $doc);
					$zip->addFile($doc , $name);
				}
			} else {
				$name = str_replace('../documentos/consultorias/', '', $documento);
				$zip->addFile($documento , $name);
			}
		}
	}
	if(!$zip->close()){
		echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
		echo $zip->getStatusString();
	}

	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=documentos_".$referencia.".zip");
	header("Content-Transfer-Encoding: binary");

	readfile($nameZip);
}
?>
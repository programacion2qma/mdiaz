<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales
include_once('funciones_generacion.php');
@include_once("../phpqrcode/qrlib.php");

//Inicio de funciones específicas

//Parte de consultorías

function operacionesTrabajos(){
	$res=true;

	if(isset($_POST['fecha'])){
		$res=creaConsultoriaGratuita();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=actualizaServicios();
	}
	elseif(isset($_POST['sujeto'])){
		$res=actualizaServicios();
	}
	elseif(isset($_GET['codigoEliminar'])){
		$res=eliminaTrabajo($_GET['codigoEliminar']);
	}

	mensajeResultado('codigoCliente',$res,'Consultoría');
    mensajeResultado('elimina',$res,'Consultoría', true);
}

function eliminaTrabajo($codigo){
	$res=true;
	$res=consultaBD('DELETE FROM trabajos WHERE codigo='.$codigo,true);
	$_POST['elimina']='SI';
	return $res;
}

function CreaConsultoriaGratuita(){
	$res = true;
	
	if(isset($_POST['codigo'])){
		$res=actualizaDatos('trabajos');
	} else if(isset($_POST['creaGratuita'])){
		$res=insertaDatos('trabajos');
	}
	return $res;
}

function creaEstadisticasTrabajos(){
   $estadisticas=array();
   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos" ,true, true);
   $estadisticas['total'] = $res['total'];

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE gratuita='NO'" ,true, true);
   $estadisticas['ventas'] = $res['total'];
   $estadisticas['gratuitas'] = $estadisticas['total'] - $estadisticas['ventas'];

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE envio='SI'" ,true, true);
   $estadisticas['finalizadas'] = $res['total'];
   $estadisticas['enproceso'] = $estadisticas['total'] - $estadisticas['finalizadas'];

   $servicios=consultaBD("SELECT * FROM servicios ORDER BY codigo",true);
   while($servicio=mysql_fetch_assoc($servicios)){
   	$res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE codigoServicio=".$servicio['codigo'] ,true, true);
   	$estadisticas[$servicio['referencia']]=$res['total'];
   }

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE toma_datos='NO' AND fechaTomaDatos < CURDATE()" ,true, true);
   $estadisticas['toma_datos'] = $res['total'];

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE revision='NO' AND fechaRevision < CURDATE()" ,true, true);
   $estadisticas['revision'] = $res['total'];

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE emision='NO' AND fechaEmision < CURDATE()" ,true, true);
   $estadisticas['emision'] = $res['total'];

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE envio='NO' AND fechaEnvio < CURDATE()" ,true, true);
   $estadisticas['envio'] = $res['total'];

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE entrega='NO' AND fechaEntrega < CURDATE()" ,true, true);
   $estadisticas['entrega'] = $res['total'];

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE mantenimiento='NO' AND fechaMantenimiento < CURDATE()" ,true, true);
   $estadisticas['mantenimiento'] = $res['total'];

   $res=consultaBD("SELECT COUNT(codigo) as total FROM trabajos WHERE incidencia='SI'" ,true, true);
   $estadisticas['incidencias'] = $res['total'];

   return $estadisticas;
}

function actualizaServicios(){
	$res=true;
	transformaValoresPOST();
	if(isset($_POST['codigo'])){
		$res=actualizar();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=insertar();
	}
	return $res;
}

function insertar(){
	$res=true;
	$i=1;
	while(isset($_POST['tabla'.$i])){
		if(isset($_POST['toma_datos'.$i])){
			$res = consultaBD("UPDATE trabajos SET toma_datos = '".$_POST['toma_datos'.$i]."' WHERE codigo=".$_POST['trabajo'.$i],true);
		}
		if($_POST['tabla'.$i]=='manuales_pbc'){
			$res = insertaDatos($_POST['tabla'.$i],'','documentos');
			$_POST['codigo'] = $res;
		} else {
			$_POST['codigo']='';
		}
		$res=completar($_POST['tabla'.$i],$_POST['codigo'],$i);
		$i++;
	}
	return $res;
}

function actualizar(){
	$res=true;
	$i=1;
	while(isset($_POST['tabla'.$i])){
		if(isset($_POST['toma_datos'.$i])){
			$res = $res && consultaBD("UPDATE trabajos SET toma_datos = '".$_POST['toma_datos'.$i]."' WHERE codigo=".$_POST['trabajo'.$i],true);
		}
		if($_POST['tabla'.$i]=='manuales_pbc'){
			$res = $res && actualizaDatos($_POST['tabla'.$i],'','documentos');
		}
		$res = $res && completar($_POST['tabla'.$i],$_POST['codigo'],$i);
		$i++;
	}
	return $res;
}

function completar($tabla,$codigo,$i){
	$res=true;
	if($tabla=='manuales_pbc'){
		$res = $res && insertaPersonas($codigo);
	} if ($tabla=='lopd1'){
		$res = $res && insertaLOPD($i);
	}
	$res = $res && actualizaCliente($tabla,$_POST['codigoCliente'],$i);
	return $res;
}

function actualizaCliente($tabla,$codigoCliente,$i){
	$res=true;
	$_POST['pregunta'.$i.'11']=isset($_POST['pregunta'.$i.'11'])?mb_strtoupper($_POST['pregunta'.$i.'11'],'UTF-8'):'';
	if($tabla=='manuales_pbc'){
		$res = $res && consultaBD('UPDATE clientes SET actividad="'.$_POST['actividad'].'", servicios="'.$_POST['servicios'].'",empleados="'.$_POST['empleados'].'",volumen="'.$_POST['volumen'].'",tomo="'.$_POST['tomo'].'",libro="'.$_POST['libro'].'",folio="'.$_POST['folio'].'",seccion="'.$_POST['seccion'].'",hoja="'.$_POST['hoja'].'" WHERE codigo='.$codigoCliente,true);
	} if ($tabla=='lopd1'){
		$res = $res && consultaBD('UPDATE clientes SET razonSocial="'.$_POST['pregunta'.$i.'3'].'", domicilio="'.$_POST['pregunta'.$i.'4'].'",localidad="'.$_POST['pregunta'.$i.'5'].'",telefono="'.$_POST['pregunta'.$i.'6'].'",email="'.$_POST['pregunta'.$i.'7'].'",administrador="'.$_POST['pregunta'.$i.'8'].'",cif="'.$_POST['pregunta'.$i.'9'].'",cp="'.$_POST['pregunta'.$i.'10'].'",fax="'.$_POST['pregunta'.$i.'12'].'",provincia="'.$_POST['pregunta'.$i.'11'].'",nifAdministrador="'.$_POST['pregunta'.$i.'14'].'",apellido1="'.$_POST['pregunta'.$i.'627'].'",apellido2="'.$_POST['pregunta'.$i.'628'].'" WHERE codigo='.$codigoCliente,true);
	}
	return $res;
}

function insertaLOPD($i){

	$res = true;

	$datos=arrayFormulario();
	$datos=compruebaCampoCheck($datos,$i);
	$query='';
	$j=1;
	while(isset($datos['pregunta'.$i.$j])){
		//echo $i.'-'.$j.'<br/>';
		if($j>1){
			$query.="&{}&";
		}
		if(is_array($datos['pregunta'.$i.$j])){
			$select = $datos['pregunta'.$i.$j];
			$text ='';
			if(!empty($select)){
				for($k=0;$k<count($select);$k++){
					if($k > 0){
						$text .= "&$&";
					}
					$text .= $select[$k];
				}
			}
			$datos['pregunta'.$i.$j]=$text;
		}
		$query.="pregunta$j=>".$datos['pregunta'.$i.$j];
		$j++;
	}
	
	$res = $res && consultaBD("UPDATE trabajos SET formulario = '".$query."' WHERE codigo=".$datos['trabajo'.$i],true);
	$res = $res && insertaTratamientosFijos($datos['trabajo'.$i],$i);
	conexionBD();
	$res = $res && insertaSubcontratacionesFijos($datos['trabajo'.$i],$i);
	$res = $res && insertaUsuariosLOPD($datos['trabajo'.$i]);
	$res = $res && insertaProveedoresLOPD($datos['trabajo'.$i]);
	$res = $res && insertaAplicacionesLOPD($datos['trabajo'.$i]);
	$res = $res && insertaSoportesLOPD($datos['trabajo'.$i]);
	cierraBD();
	//$res = $res && insertaOtrosFicherosLOPD($datos['trabajo'.$i]);
	$res = $res && insertaOtrosEncargadosLOPD($datos['trabajo'.$i]);
	conexionBD();
	$res = $res && insertaSoportesAutomatizadosLOPD($datos['trabajo'.$i]);
	$res = $res && insertaDelegacionesLOPD($datos['trabajo'.$i]);
	$res = $res && insertaCesionesLOPD($datos['trabajo'.$i]);
	$res = $res && insertaConcurrenLOPD($datos['trabajo'.$i]);
	cierraBD();
	//$res = $res && insertaCesionesLOPD($datos['trabajo'.$i]);
	//$res = $res && insertaDPDLOPD($datos['trabajo'.$i]);
	return $res;
}

function insertaAplicacionesLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM aplicaciones_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreAplicacionLOPD'.$i])){
		if($datos['nombreAplicacionLOPD'.$i] != ''){
		$res = $res && consultaBD("INSERT INTO aplicaciones_lopd VALUES (NULL,'".$codigo."','".$datos['nombreAplicacionLOPD'.$i]."','".$datos['finalidadAplicacionLOPD'.$i]."','".$datos['empleadosAplicacionLOPD'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaSoportesLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	while(isset($datos['nombreSoporteLOPD_'.$i])){
		if($datos['nombreSoporteLOPD_'.$i] != ''){
			if(isset($datos['existeSoporte_'.$i]) && $datos['existeSoporte_'.$i]!=''){
				$res = $res && consultaBD("UPDATE soportes_lopd_nueva SET nombreSoporteLOPD='".$datos['nombreSoporteLOPD_'.$i]."', marcaSoporteLOPD='".$datos['marcaSoporteLOPD_'.$i]."', serieSoporteLOPD='".$datos['serieSoporteLOPD_'.$i]."', fechaSoporteLOPD='".$datos['fechaSoporteLOPD_'.$i]."', ubicacionSoporteLOPD='".$datos['ubicacionSoporteLOPD_'.$i]."', sistemaSoporteLOPD='".$datos['sistemaSoporteLOPD_'.$i]."', aplicacionesSoporteLOPD='".$datos['aplicacionesSoporteLOPD_'.$i]."', conexionesSoporteLOPD='".$datos['conexionesSoporteLOPD_'.$i]."', responsableSoporteLOPD='".$datos['responsableSoporteLOPD_'.$i]."', otrosSoporteLOPD='".$datos['otrosSoporteLOPD_'.$i]."', contenidoSoporteLOPD='".$datos['contenidoSoporteLOPD_'.$i]."', mecanismosAccesoSoporteLOPD='".$datos['mecanismosAccesoSoporteLOPD_'.$i]."', mecanismosAccesoRecursosSoporteLOPD='".$datos['mecanismosAccesoRecursosSoporteLOPD_'.$i]."' WHERE codigo=".$datos['existeSoporte_'.$i]);
			} else {
				$res = $res && consultaBD("INSERT INTO soportes_lopd_nueva VALUES (NULL,'".$datos['codigoCliente']."','".$datos['nombreSoporteLOPD_'.$i]."','".$datos['marcaSoporteLOPD_'.$i]."','".$datos['serieSoporteLOPD_'.$i]."','".$datos['fechaSoporteLOPD_'.$i]."','".$datos['ubicacionSoporteLOPD_'.$i]."','".$datos['sistemaSoporteLOPD_'.$i]."','".$datos['aplicacionesSoporteLOPD_'.$i]."','".$datos['conexionesSoporteLOPD_'.$i]."','".$datos['responsableSoporteLOPD_'.$i]."','".$datos['otrosSoporteLOPD_'.$i]."','".$datos['contenidoSoporteLOPD_'.$i]."','".$datos['mecanismosAccesoSoporteLOPD_'.$i]."','".$datos['mecanismosAccesoRecursosSoporteLOPD_'.$i]."');");
			}
		} else {
			if(isset($datos['existeSoporte_'.$i]) && $datos['existeSoporte_'.$i]!=''){
				$res=consultaBD('DELETE FROM soportes_lopd_nueva WHERE codigo='.$datos['existeSoporte_'.$i]);
			}
		}
		$i++;
	}

	return $res;
}

function insertaUsuariosLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();


	while(isset($datos['nombreUsuarioLOPD'.$i])){
		if($datos['nombreUsuarioLOPD'.$i] != ''){
			$select = $datos['ficherosUsuarioLOPD'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['ficherosUsuarioLOPD'.$i] = $text;

			$select = $datos['soportesUsuarioLOPD'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['soportesUsuarioLOPD'.$i] = $text;

			if(isset($datos['existeUsuarioLOPD'.$i]) && $datos['existeUsuarioLOPD'.$i]!=''){
				$res = $res && consultaBD("UPDATE usuarios_lopd SET nombreUsuarioLOPD='".$datos['nombreUsuarioLOPD'.$i]."', nifUsuarioLOPD='".$datos['nifUsuarioLOPD'.$i]."', datosUsuarioLOPD='".$datos['datosUsuarioLOPD'.$i]."', ficherosUsuarioLOPD='".$datos['ficherosUsuarioLOPD'.$i]."', soportesUsuarioLOPD='".$datos['soportesUsuarioLOPD'.$i]."', accesoUsuarioLOPD='".$datos['accesoUsuarioLOPD'.$i]."', fechaAltaUsuarioLOPD='".$datos['fechaAltaUsuarioLOPD'.$i]."', fechaBajaUsuarioLOPD='".$datos['fechaBajaUsuarioLOPD'.$i]."', puestoUsuarioLOPD='".$datos['puestoUsuarioLOPD'.$i]."' WHERE codigo=".$datos['existeUsuarioLOPD'.$i]);
			} else {
				$res = $res && consultaBD("INSERT INTO usuarios_lopd VALUES (NULL,'".$codigo."','".$datos['nombreUsuarioLOPD'.$i]."', '".$datos['nifUsuarioLOPD'.$i]."','".$datos['datosUsuarioLOPD'.$i]."','".$datos['ficherosUsuarioLOPD'.$i]."','".$datos['soportesUsuarioLOPD'.$i]."','".$datos['accesoUsuarioLOPD'.$i]."','".$datos['fechaAltaUsuarioLOPD'.$i]."','".$datos['fechaBajaUsuarioLOPD'.$i]."','".$datos['puestoUsuarioLOPD'.$i]."');");
			}
		} else {
			if(isset($datos['existeUsuarioLOPD'.$i]) && $datos['existeUsuarioLOPD'.$i]!=''){
				$res=consultaBD('DELETE FROM usuarios_lopd WHERE codigo='.$datos['existeUsuarioLOPD'.$i]);
			}
		}

		$i++;
	}

	return $res;
}

function insertaProveedoresLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();


	while(isset($datos['nombreProveedorLOPD'.$i])){
		if($datos['nombreProveedorLOPD'.$i] != ''){
			if(isset($datos['existeProveedorLOPD'.$i]) && $datos['existeProveedorLOPD'.$i]!=''){
				$res = $res && consultaBD("UPDATE proveedores_lopd SET nombre='".$datos['nombreProveedorLOPD'.$i]."', nif='".$datos['nifProveedorLOPD'.$i]."', representante='".$datos['representanteProveedorLOPD'.$i]."', servicio='".$datos['servicioProveedorLOPD'.$i]."', fechaAlta='".$datos['fechaAltaProveedorLOPD'.$i]."', fechaBaja='".$datos['fechaBajaProveedorLOPD'.$i]."' WHERE codigo=".$datos['existeProveedorLOPD'.$i]);
			} else {
				$res = $res && consultaBD("INSERT INTO proveedores_lopd VALUES (NULL,'".$codigo."','".$datos['nombreProveedorLOPD'.$i]."', '".$datos['nifProveedorLOPD'.$i]."','".$datos['representanteProveedorLOPD'.$i]."','".$datos['servicioProveedorLOPD'.$i]."','".$datos['fechaAltaProveedorLOPD'.$i]."','".$datos['fechaBajaProveedorLOPD'.$i]."');");
			}
		} else {
			if(isset($datos['existeProveedorLOPD'.$i]) && $datos['existeProveedorLOPD'.$i]!=''){
				$res=consultaBD('DELETE FROM proveedores_lopd WHERE codigo='.$datos['existeProveedorLOPD'.$i]);
			}
		}

		$i++;
	}

	return $res;
}

function insertaDPDLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM dpd_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreDPD'.$i])){
		if($datos['nombreDPD'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO dpd_lopd VALUES (NULL,'".$codigo."','".$datos['nombreDPD'.$i]."',
			'".$datos['nifDPD'.$i]."','".$datos['tipoDPD'.$i]."','".$datos['encargadoDPD'.$i]."','".$datos['direccionDPD'.$i]."','".$datos['telefonoDPD'.$i]."','".$datos['emailDPD'.$i]."','".$datos['fechaAltaDPD'.$i]."','".$datos['fechaBajaDPD'.$i]."');");
		}
		$i++;
	}

	return $res;
}

function insertaOtrosEncargadosLOPD($codigo){
	$res=true;
	$i=0;
	$datos=arrayFormulario();

	// Sino se insertan los nuevos registros
	while(isset($datos['servicioEncargadoLOPD_'.$i])){
		if($datos['servicioEncargadoLOPD_'.$i] != '' || $datos['nombreEncargadoLOPD_'.$i] != ''){
			$select = $datos['ficheroEncargadoLOPD_'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['ficheroEncargadoLOPD_'.$i] = $text;

			$select = $datos['dondeEncargadoLOPD_'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['dondeEncargadoLOPD_'.$i] = $text;
			if(isset($datos['existeEncargadoLOPD_'.$i]) && $datos['existeEncargadoLOPD_'.$i]!=''){
				$res = $res && consultaBD("UPDATE otros_encargados_lopd SET servicioEncargado='".$datos['servicioEncargadoLOPD_'.$i]."', nombreEncargado='".$datos['nombreEncargadoLOPD_'.$i]."', cifEncargado='".$datos['cifEncargadoLOPD_'.$i]."', direccionEncargado='".$datos['direccionEncargadoLOPD_'.$i]."', emailEncargado='".$datos['emailEncargadoLOPD_'.$i]."', localidadEncargado='".$datos['localidadEncargadoLOPD_'.$i]."', cpEncargado='".$datos['cpEncargadoLOPD_'.$i]."', telefonoEncargado='".$datos['telefonoEncargadoLOPD_'.$i]."', contactoEncargado='".$datos['contactoEncargadoLOPD_'.$i]."', representanteEncargado='".$datos['representanteEncargadoLOPD_'.$i]."', dondeEncargado='".$datos['dondeEncargadoLOPD_'.$i]."', subcontratacionEncargado='".$datos['subcontratacionEncargadoLOPD_'.$i]."', fechaAltaEncargado='".$datos['fechaAltaEncargadoLOPD_'.$i]."', fechaBajaEncargado='".$datos['fechaBajaEncargadoLOPD_'.$i]."',ficheroEncargado='".$datos['ficheroEncargadoLOPD_'.$i]."',accesosEncargado='".$datos['accesosEncargadoLOPD_'.$i]."',provinciaEncargado='".$datos['provinciaEncargadoLOPD_'.$i]."',exclusivoEncargado='".$datos['exclusivoEncargadoLOPD_'.$i]."' WHERE codigo=".$datos['existeEncargadoLOPD_'.$i],true);
				$codigoEncargado=$datos['existeEncargadoLOPD_'.$i];
			} else {
				$res = $res && consultaBD("INSERT INTO otros_encargados_lopd VALUES (NULL,'".$codigo."','".$datos['servicioEncargadoLOPD_'.$i]."','".$datos['nombreEncargadoLOPD_'.$i]."','".$datos['cifEncargadoLOPD_'.$i]."','".$datos['direccionEncargadoLOPD_'.$i]."','".$datos['emailEncargadoLOPD_'.$i]."','".$datos['localidadEncargadoLOPD_'.$i]."','".$datos['cpEncargadoLOPD_'.$i]."','".$datos['telefonoEncargadoLOPD_'.$i]."','".$datos['contactoEncargadoLOPD_'.$i]."','".$datos['representanteEncargadoLOPD_'.$i]."','".$datos['dondeEncargadoLOPD_'.$i]."','".$datos['subcontratacionEncargadoLOPD_'.$i]."','".$datos['fechaAltaEncargadoLOPD_'.$i]."','".$datos['fechaBajaEncargadoLOPD_'.$i]."','".$datos['ficheroEncargadoLOPD_'.$i]."','".$datos['accesosEncargadoLOPD_'.$i]."','".$datos['provinciaEncargadoLOPD_'.$i]."','".$datos['exclusivoEncargadoLOPD_'.$i]."');",true);
				if($res) {
					$codigoEncargado = consultaBD("SELECT codigo FROM otros_encargados_lopd ORDER BY codigo DESC LIMIT 1;", true, true);
					$codigoEncargado = $codigoEncargado['codigo'];
				}
				
			}
			$res=$res && insertaTratamientosNoFijos($codigo,$i,$codigoEncargado);
			$res=$res && insertaSubcontratacionesNoFijos($codigo,$i,$codigoEncargado);
		} else {
			if(isset($datos['existeEncargadoLOPD_'.$i]) && $datos['existeEncargadoLOPD_'.$i]!=''){
				$res=consultaBD('DELETE FROM otros_encargados_lopd WHERE codigo='.$datos['existeEncargadoLOPD_'.$i],true);
			}
		}

		$i++;
	}

	return $res;
}

function insertaTratamientosNoFijos($codigoTrabajo,$i,$codigoEncargado){
	$res=true;
	if($_POST['ficheroEncargadoLOPD_'.$i]!='NULL'){
			preparaTratamiento($i,'NOFIJO');
			$_POST['fijo']='NO';
			$_POST['codigoTrabajo']=$codigoTrabajo;
			$_POST['codigoEncargado']=$codigoEncargado;
			$tratamiento=consultaBD('SELECT * FROM encargados_tratamientos WHERE fijo="NO" AND codigoEncargado='.$codigoEncargado.' AND codigoTrabajo='.$codigoTrabajo,true,true);
			if($tratamiento){
				$_POST['codigo']=$tratamiento['codigo'];
				$res = $res && actualizaDatos('encargados_tratamientos');
			} else {
				$res = $res && insertaDatos('encargados_tratamientos');
			}
	} else {
		$res = $res && consultaBD('DELETE FROM encargados_tratamientos WHERE fijo="NO" AND codigoEncargado='.$codigoEncargado.' AND codigoTrabajo='.$codigoTrabajo,true);
	}
	return $res;
}

function insertaSubcontratacionesNoFijos($codigoTrabajo,$i,$codigoEncargado){
	$res=true;
	if($_POST['subcontratacionEncargadoLOPD_'.$i]=='SI'){
		$j=0;
		while(isset($_POST['empresa'.$i.'_'.$j])){
			if($_POST['empresa'.$i.'_'.$j]!=''){
				$res = $res && consultaBD('INSERT INTO declaraciones_subcontrata VALUES(NULL,'.$codigoEncargado.',"'.$_POST['empresa'.$i.'_'.$j].'","ENCARGADO",'.$codigoTrabajo.',"'.$_POST['nif'.$i.'_'.$j].'","'.$_POST['direccion'.$i.'_'.$j].'","'.$_POST['cp'.$i.'_'.$j].'","'.$_POST['localidad'.$i.'_'.$j].'","'.$_POST['provincia'.$i.'_'.$j].'","'.$_POST['representante'.$i.'_'.$j].'","'.$_POST['servicio'.$i.'_'.$j].'")',true);
			}
			$j++;
		}
	}
	return $res;
}

function insertaSoportesAutomatizadosLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM soportes_automatizados_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreAutomatizadosLOPD_'.$i])){
		if($datos['nombreAutomatizadosLOPD_'.$i] != ''){
			$select = $datos['soportesAutomatizadosLOPD_'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['soportesAutomatizadosLOPD_'.$i] = $text;
			$res = $res && consultaBD("INSERT INTO soportes_automatizados_lopd VALUES (NULL,'".$codigo."','".$datos['nombreAutomatizadosLOPD_'.$i]."','".$datos['cargoAutomatizadosLOPD_'.$i]."','".$datos['accesosAutomatizadosLOPD_'.$i]."','".$datos['nifAutomatizadosLOPD_'.$i]."','".$datos['soportesAutomatizadosLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaCesionesLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM cesiones_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['tipoCesionesLOPD_'.$i])){
		if($datos['tipoCesionesLOPD_'.$i] != ''){
			$select = $datos['ficheroCesionesLOPD_'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['ficheroCesionesLOPD_'.$i] = $text;
			$res = $res && consultaBD("INSERT INTO cesiones_lopd VALUES (NULL,'".$codigo."','".$datos['tipoCesionesLOPD_'.$i]."','".$datos['quienCesionesLOPD_'.$i]."','".$datos['objetoCesionesLOPD_'.$i]."','".$datos['finalidadCesionesLOPD_'.$i]."','".$datos['ficheroCesionesLOPD_'.$i]."','".$datos['fechaCesionesLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaDelegacionesLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$notIn='(0';

	while(isset($datos['usuario1DelegacionesLOPD_'.$i])){
		if($datos['usuario1DelegacionesLOPD_'.$i] != 'NULL'){
			$select = $datos['facultadesDelegacionesLOPD_'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['facultadesDelegacionesLOPD_'.$i] = $text;
			if(isset($datos['codigoExisteDelegacionesLOPD_'.$i]) && $datos['codigoExisteDelegacionesLOPD_'.$i]!=''){
				$res= $res && consultaBD("UPDATE delegaciones_lopd SET usuario1Delegaciones='".$datos['usuario1DelegacionesLOPD_'.$i]."',usuario2Delegaciones='".$datos['usuario2DelegacionesLOPD_'.$i]."',fechaDelegaciones='".$datos['fechaDelegacionesLOPD_'.$i]."',fechaFinDelegaciones='".$datos['fechaFinDelegacionesLOPD_'.$i]."',facultades='".$datos['facultadesDelegacionesLOPD_'.$i]."',otras='".$datos['otrasDelegacionesLOPD_'.$i]."' WHERE codigo=".$datos['codigoExisteDelegacionesLOPD_'.$i]);
				$notIn.=','.$datos['codigoExisteDelegacionesLOPD_'.$i];
			} else {
				$res = $res && consultaBD("INSERT INTO delegaciones_lopd VALUES (NULL,'".$codigo."','".$datos['usuario1DelegacionesLOPD_'.$i]."','".$datos['usuario2DelegacionesLOPD_'.$i]."','".$datos['fechaDelegacionesLOPD_'.$i]."','".$datos['fechaFinDelegacionesLOPD_'.$i]."','".$datos['facultadesDelegacionesLOPD_'.$i]."','".$datos['otrasDelegacionesLOPD_'.$i]."');");
				$notIn.=','.mysql_insert_id();
			}
		}
		$i++;
	}
	$notIn.=')';
	$res = $res && consultaBD("DELETE FROM delegaciones_lopd WHERE codigoTrabajo='".$codigo."' AND codigo NOT IN ".$notIn.";");
	return $res;
}

function insertaConcurrenLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM concurren_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreConcurrenLOPD_'.$i])){
		if($datos['nombreConcurrenLOPD_'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO concurren_lopd VALUES (NULL,'".$codigo."','".$datos['nombreConcurrenLOPD_'.$i]."','".$datos['nifConcurrenLOPD_'.$i]."','".$datos['actividadConcurrenLOPD_'.$i]."','".$datos['fechaConcurrenLOPD_'.$i]."','".$datos['fechaFinConcurrenLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}


function insertaOtrosFicherosLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM otros_ficheros_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['otroFicheroLOPD_'.$i])){
		if($datos['otroFicheroLOPD_'.$i] == 'SI'){
		$res = $res && consultaBD("INSERT INTO otros_ficheros_lopd VALUES (NULL,'".$codigo."','".$datos['nombreFicheroLOPD_'.$i]."','".$datos['finalidadFicheroLOPD_'.$i]."','".$datos['origenFicheroLOPD_'.$i]."','".$datos['colectivoFicheroLOPD_'.$i]."','".$datos['datosFicheroLOPD_'.$i]."','".$datos['sistemaFicheroLOPD_'.$i]."','".$datos['nivelFicheroLOPD_'.$i]."','".$datos['fechaAltaFicheroLOPD_'.$i]."','".$datos['fechaBajaFicheroLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function compruebaCampoCheck($datos,$i){

	for($j=1;$j<40;$j++){

		if(!isset($datos['pregunta'.$i.$j])){
			$datos['pregunta'.$i.$j]='NO';
		}
	}

	return $datos;

}

function insertaPersonas($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();

	$res=consultaBD("DELETE FROM personas_pbc WHERE codigoManual=$codigo");

	for($i=0;isset($datos['nombre'.$i]);$i++){
		if($datos['nombre'.$i]!=''){
			$res=$res && consultaBD("INSERT INTO personas_pbc VALUES(NULL,".$codigo.",'".$datos['nombre'.$i]."');");
		}
	}

	cierraBD();

	return $res;
}

function insertaSubcontratacionesFijos($codigoTrabajo,$i){
	$res=true;
	$encargados=array(158=>541,168=>527,178=>542,189=>528,200=>529,210=>530,220=>531,230=>532,240=>533,250=>534,260=>535,270=>536,280=>537,290=>538,487=>571);
	$res=consultaBD('DELETE FROM declaraciones_subcontrata WHERE codigoTrabajo='.$codigoTrabajo);
	foreach ($encargados as $key => $value) {
		if($_POST['pregunta'.$i.$key]=='SI' && $_POST['pregunta'.$i.$value]=='SI'){
			$j=0;
			while(isset($_POST['empresa'.$key.'Fijo_'.$j])){
				if($_POST['empresa'.$key.'Fijo_'.$j]!=''){
					$res=consultaBD('INSERT INTO declaraciones_subcontrata VALUES(NULL,'.$key.',"'.$_POST['empresa'.$key.'Fijo_'.$j].'","ENCARGADOFIJO",'.$codigoTrabajo.',"'.$_POST['nif'.$key.'Fijo_'.$j].'","'.$_POST['direccion'.$key.'Fijo_'.$j].'","'.$_POST['cp'.$key.'Fijo_'.$j].'","'.$_POST['localidad'.$key.'Fijo_'.$j].'","'.$_POST['provincia'.$key.'Fijo_'.$j].'","'.$_POST['representante'.$key.'Fijo_'.$j].'","'.$_POST['servicio'.$key.'Fijo_'.$j].'")');
				}
				$j++;
			}
		}
	}
	return $res;
}

function insertaTratamientosFijos($codigoTrabajo,$i){
	$res=true;
	$encargados=array(158=>574,168=>575,178=>576,189=>577,200=>578,210=>579,220=>609,230=>580,240=>581,250=>582,260=>583,270=>584,280=>585,290=>586,487=>587);
	foreach ($encargados as $key => $value) {
		if($_POST['pregunta'.$i.$key]=='SI' && $_POST['pregunta'.$i.$value]!='NULL'){
			preparaTratamiento($key,'FIJO');
			$_POST['fijo']='SI';
			$_POST['codigoTrabajo']=$codigoTrabajo;
			$_POST['codigoEncargado']=$key;
			$tratamiento=consultaBD('SELECT * FROM encargados_tratamientos WHERE fijo="SI" AND codigoEncargado='.$key.' AND codigoTrabajo='.$codigoTrabajo,true,true);
			if($tratamiento){
				$_POST['codigo']=$tratamiento['codigo'];
				$res=actualizaDatos('encargados_tratamientos');
			} else {
				$res=insertaDatos('encargados_tratamientos');
			}
		} else {
			$res=consultaBD('DELETE FROM encargados_tratamientos WHERE fijo="SI" AND codigoEncargado='.$key.' AND codigoTrabajo='.$codigoTrabajo,true);
		}
	}
	return $res;
}

function preparaTratamiento($n,$tipo){
	$listado=array('checkRecogida','checkModificacion','checkConsulta','checkComunicacion','checkComunicacion1','checkRegistro','checkConservacion','checkCotejo','checkSupresion','checkComunicacion2','checkEstructuracion','checkInterconexion','checkAnalisis','checkDestruccion','checkComunicacion3');
	foreach ($listado as $value) {
		if(isset($_POST[$value.'_'.$tipo.'_'.$n])){
			$_POST[$value]='SI';
		} else {
			$_POST[$value]='NO';
		}
	}
}


function imprimeTrabajos(){
	global $_CONFIG;
	$consulta=consultaBD("SELECT c.codigo as cliente, t.codigo as trabajo, c.razonSocial, t.codigoServicio FROM trabajos t INNER JOIN clientes c ON t.codigoCliente=c.codigo GROUP BY c.codigo" ,true);
	while($datos=mysql_fetch_assoc($consulta)){
		echo "
		<tr>
			<td> ".$datos['razonSocial']." </td>";
		echo"
			<td>
				<table class='tablaInternaServicios'>
					<thead>
						<tr>
							<th>Servicio</th>
							<th>Gratuita</th>
							<th>Fecha Prevista Finalización</th>
							<th>Toma de datos</th>
							<th>Revisión y Emisión de Documentación</th>
							<th>Envío - Entrega al Cliente</th>
							<th>Mantenimiento</th>
							<th></th>
						<tr>
					</thead>
					<tbody>";
		$servicios = consultaBD("SELECT * FROM trabajos WHERE codigoCliente=".$datos['cliente']." ORDER BY codigoServicio",true);
		while($servicio=mysql_fetch_assoc($servicios)){
			$servi=datosRegistro('servicios',$servicio['codigoServicio']);
			$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger"></i>');
			echo "<tr>
					<td>".$servi['referencia']."</td>
					<td>".$servicio['gratuita']."</td>
					<td class='centro'>Columna 2</td>
					<td class='centro'>".$iconoValidado[$servicio['toma_datos']]."<br/>".obtieneFechaLimite($servicio['fechaTomaDatos'])."</td>
					<td class='centro'>".$iconoValidado[$servicio['revision']]."<br/>".obtieneFechaLimite($servicio['fechaRevision'])."</td>
					<td class='centro'>".$iconoValidado[$servicio['envio']]."<br/>".obtieneFechaLimite($servicio['fechaEnvio'])."</td>
					<td class='centro'>".$iconoValidado[$servicio['mantenimiento']]."<br/>".obtieneFechaLimite($servicio['fechaMantenimiento'])."</td>
					<td class='centro'>".creaBotonDetalles('consultoria/gestion.php?codigoTrabajo='.$servicio['codigo'],'')."</td>
				</tr>";
		}
					
		echo"		</tbody>
				</table>
			</td>
			";	
        echo"	
        	<td class='centro'>
				<div class='btn-group'>
					<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i><span class='caret'></span></button>
					 	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."clientes/gestion.php?codigo=".$datos['cliente']."&consultoria'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."consultoria/gestion.php?codigoCliente=".$datos['cliente']."'><i class='icon-edit'></i> Toma de datos</i></a></li>
						    <li class='divider'></li>
						    <li><a href='".$_CONFIG['raiz']."consultoria/gestion.php?codigoCliente=".$datos['cliente']."'><i class='icon-file-text-o'></i> Generación de documentos</i></a></li>
						</ul>
				</div>
			</td>
        	<td class='centro'>
                <input type='checkbox' name='codigoLista[]' value='".$datos['trabajo']."'>
            </td>
    	</tr>";
	}
}

function listadoTrabajos(){
	global $_CONFIG;

	$columnas=array('clientes.codigo','clientes.razonSocial','trabajos.codigo','trabajos.codigoServicio','trabajos.gratuita','trabajos.toma_datos','trabajos.revision','trabajos.envio','trabajos.mantenimiento','trabajos.fechaTomaDatos','trabajos.fechaRevision','trabajos.fechaEnvio','trabajos.fechaMantenimiento','trabajos.fechaTomaDatos','trabajos.fechaRevision','trabajos.fechaEnvio','trabajos.fechaMantenimiento','trabajos.fechaPrevista','trabajos.fechaPrevista','trabajos.fechaEmision','trabajos.emision','trabajos.fechaEmision','trabajos.fechaEntrega','trabajos.entrega','trabajos.fechaEntrega','trabajos.incidencia','codigoUsuario');
	$where=obtieneWhereListado("WHERE 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();

	if($_SESSION['tipoUsuario'] == 'CLIENTE'){
		$cliente=datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');
		$where .= ' AND clientes.codigo = '.$cliente['codigoCliente'];
	}
		

	$query="SELECT clientes.codigo as cliente, trabajos.codigo as trabajo, clientes.razonSocial, trabajos.codigoServicio, comerciales.codigoUsuario, codigoUsuarioAsociado, trabajos.codigoVenta, GROUP_CONCAT(CONCAT('- ',servicios.servicio,' (',YEAR(ventas_servicios.fecha),')') SEPARATOR '<br/>') AS servicios
			FROM (trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo) 
			LEFT JOIN ventas_servicios ON trabajos.codigoVenta=ventas_servicios.codigo
			LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo
			LEFT JOIN servicios ON trabajos.codigoServicio=servicios.codigo
			$where AND trabajos.codigoServicio <> '408'
			GROUP BY clientes.codigo";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite");
	$consultaPaginacion=consultaBD($query);

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
        $textos = array('Detalles','Toma de datos');
        $enlaces = array("clientes/gestion.php?codigo=".$datos['cliente'].'&consultoria',"consultoria/gestion.php?codigoCliente=".$datos['cliente']);
        $iconos = array('icon-search-plus','icon-edit');
       	$codigosServicios='1,3,4,6,8,11,12,15';
        $lopd = consultaBD("SELECT COUNT(codigo) AS codigo FROM trabajos WHERE codigoCliente = ".$datos['cliente']." AND codigoServicio IN (".$codigosServicios.");",false, true);
        if($lopd['codigo'] > 0){
            array_push($textos, 'Declaraciones LOPD');
            array_push($enlaces, 'gestion-lopd/index.php?codigoCliente='.$datos['cliente']);
            array_push($iconos, 'icon-flag');
        }
        $razonSocial=$datos['razonSocial'];
        if($datos['codigoVenta']==''){
        	$razonSocial="<span><span class='hide bloqueado'></span></span>".$datos['razonSocial'];
        	array_push($textos, 'Eliminar');
            array_push($enlaces, 'consultoria/index.php?codigoEliminar='.$datos['trabajo']);
            array_push($iconos, 'icon-trash');
        }
        $fila=array(
            $razonSocial,
            $datos['servicios'],
            //creaTablaServicios($datos['cliente'],$where),
            botonAcciones($textos,$enlaces,$iconos,false,true,''),
            "DT_RowId"=>$datos['trabajo']
        );

        $res['aaData'][]=$fila;
    }

	cierraBD();

	echo json_encode($res);
}

function creaTablaServicios($cliente,$where){
	$tabla="
	<button class='btn btn-propio mostrarServicios' estado='oculto' tabla='#tabla$cliente'> <i class='icon-caret-down'></i> Mostrar servicios <span class='badge badge-danger'></span> </button>
    <div id='tabla$cliente' class='hide'>
        <table class='tabla-simple ancho100'>
		<thead>
			<tr>
				<th>Servicio</th>
				<th>Gratuita</th>
				<th>Fecha Prevista Finalización</th>
				<th>Toma de datos</th>
				<th>Revisión de Documentación</th>
				<th>Emisión de Documentación</th>
				<th>Envío al Cliente</th>
				<th>Entrega al Cliente</th>
				<th>Mantenimiento</th>
				<th></th>
			<tr>
		</thead>
		<tbody>";

		$servicios = consultaBD("SELECT trabajos.* FROM (trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo) LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo ".$where." AND trabajos.codigoCliente=".$cliente." GROUP BY trabajos.codigo ORDER BY codigoServicio");
		while($servicio=mysql_fetch_assoc($servicios)){
			$incidencia = $servicio['incidencia']=='SI'?'<a href="#" class="enlacePopOver"><i class="icon-exclamation-circle iconoFactura icon-danger" title="Con incidencia"></i></a><div class="hide">'.$servicio['incidenciaTexto'].'</div>':'';
			$servi=datosRegistro('servicios',$servicio['codigoServicio']);
			$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Acción realizada"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Acción pendiente de realizar"></i>');
			$tabla.= "<tr>
                    <td>".$servi['servicio']." ".$incidencia."</td>
                    <td>".$servicio['gratuita']."</td>
                    <td class='centro'>".formateaFechaWeb($servicio['fechaPrevista'])."</td>
                    <td class='centro'>".$iconoValidado[$servicio['toma_datos']];
                    if($servicio['toma_datos'] == 'NO'){
                        $tabla.="<br/>".obtieneFechaLimite($servicio['fechaTomaDatos']);
                    }
                    $tabla.="</td>
                    <td class='centro'>".$iconoValidado[$servicio['revision']];
                    if($servicio['revision'] == 'NO'){
                        $tabla.="<br/>".obtieneFechaLimite($servicio['fechaRevision']);
                    }
                    $tabla.="</td>
                    <td class='centro'>".$iconoValidado[$servicio['emision']];
                    if($servicio['emision'] == 'NO'){
                        $tabla.="<br/>".obtieneFechaLimite($servicio['fechaEmision']);
                    }
                    $tabla.="</td>
                    <td class='centro'>".$iconoValidado[$servicio['envio']];
                    if($servicio['envio'] == 'NO'){
                        $tabla.="<br/>".obtieneFechaLimite($servicio['fechaEnvio']);
                    }
                    $tabla.="</td>
                    <td class='centro'>".$iconoValidado[$servicio['entrega']];
                    if($servicio['entrega'] == 'NO'){
                        $tabla.="<br/>".obtieneFechaLimite($servicio['fechaEntrega']);
                    }
                    $tabla.="</td>
                    <td class='centro'>".$iconoValidado[$servicio['mantenimiento']];
                    if($servicio['mantenimiento'] == 'NO'){
                        $tabla.="<br/>".obtieneFechaLimite($servicio['fechaMantenimiento']);
                    }
                    /*if($_SESSION['tipoUsuario']=='ADMIN' || $_SESSION['tipoUsuario']=='ADMINISTRACION1' || $_SESSION['tipoUsuario']=='ADMINISTRACION2' || $_SESSION['tipoUsuario']=='TECNICO' || $_SESSION['tipoUsuario']=='CONSULTOR'){
                    	$tabla.="</td>
                    	<td class='centro'>".botonAcciones(array('Detalles','Documentación'),array('consultoria/gestion.php?codigoTrabajo='.$servicio['codigo'],'consultoria/generaDocumento.php?codigoTrabajo='.$servicio['codigo']),array('icon-search-plus','icon-download'),false,false,'')."</td>
                			</tr>";
            		} else {*/
            			$tabla.="</td>
                    	<td class='centro'>".botonAcciones(array('Detalles','Eliminar'),array('consultoria/gestion.php?codigoTrabajo='.$servicio['codigo'],'consultoria/index.php?codigoEliminar='.$servicio['codigo']),array('icon-search-plus','icon-trash'),false,false,'')."</td>
                			</tr>";
            		//}

        }
                    
         $tabla.="        
            </tbody>
        </table>
    </div>";
    
    return $tabla;
}

function obtieneFechaLimite($fecha){
	$fechaFinal = formateaFechaWeb($fecha);
	$fecha = new DateTime($fecha);
	$fecha->sub(new DateInterval('P7D'));
	$fechaWarning = $fecha->format('d/m/Y');
	$label = 'success';
	if(compruebaFechaRevision($fechaFinal)){
		$label = 'danger';
	} else if(compruebaFechaRevision($fechaWarning)){
		$label = 'warning';
	}
	return "<label class='label label-".$label."'>".$fechaFinal."</label>";
}

function obtieneFechaLimiteAJAX($fecha,$suma){
	$fecha = formateaFechaBD($fecha);
	$fecha = new DateTime($fecha);
	$fecha->add(new DateInterval($suma));
	return $fecha->format('d/m/Y');
}

function gestionConsultoria(){
	operacionesTrabajos();
	abreVentanaGestion('Gestión de Consultoría','?');
	if(isset($_GET['codigoTrabajo']) && $_GET['codigoTrabajo']!=''){
		$_REQUEST['codigo'] = $_GET['codigoTrabajo'];
	} else {
		campoOculto('SI','creaGratuita');
	}
	$datos=compruebaDatos('trabajos');

	campoOculto($datos,'codigoVenta','NULL');
	campoOculto($datos,'formulario');
	campoOculto($datos,'gratuita','SI');

	campoFecha('fecha','Fecha',$datos);
	campoSelectClienteFiltradoPorUsuario($datos);
	campoSelectConsulta('codigoServicio','Servicio','SELECT codigo, servicio AS texto FROM servicios ORDER BY servicio',$datos);

	campoFecha('fechaPrevista','Fecha prevista finalización',$datos);
	campoRadio('toma_datos','Toma de datos',$datos);
	campoFecha('fechaTomaDatos','Fecha límite',$datos);

	campoRadio('revision','Revisión de documentos',$datos);
	campoFecha('fechaRevision','Fecha límite',$datos);

	campoRadio('emision','Emisión de documentos',$datos);
	campoFecha('fechaEmision','Fecha límite',$datos);

	campoRadio('envio','Envío al cliente',$datos);
	campoFecha('fechaEnvio','Fecha límite',$datos);

	campoRadio('entrega','Entrega al cliente',$datos);
	campoFecha('fechaEntrega','Fecha límite',$datos);

	campoRadio('mantenimiento','Mantenimiento',$datos);
	campoFecha('fechaMantenimiento','Fecha límite',$datos);

	campoRadio('incidencia','Incidencia',$datos);
	echo "<div id='divIncidencia' class='hidden'>";
		areaTexto('incidenciaTexto','Descripción',$datos,'areaInforme');
	echo "</div>";

	cierraVentanaGestion('index.php',true);
}

function gestionTrabajos(){

	operacionesTrabajos();

	$cliente = isset($_GET['codigoCliente']) ? $_GET['codigoCliente'] : $_POST['codigoCliente'];
	abreVentanaGestion('Toma de datos','?','','icon-edit','',true,'noAjax');	

	$servicios = consultaBD("SELECT * FROM servicios ORDER BY codigo", true);
	$pestanias = array();
	$i = 0;
	$conLOPD = array(5, 7, 10, 11, 13);

	while($servicio = mysql_fetch_assoc($servicios)){

		$sql = "SELECT 
					t.*,
					s.referencia 
				FROM trabajos t 
				INNER JOIN servicios s ON t.codigoServicio = s.codigo 
				WHERE t.codigoCliente = ".$cliente." 
				AND t.codigoServicio = ".$servicio['codigo']."
				ORDER BY codigo DESC LIMIT 1;";

		$trabajo = consultaBD($sql, true, true);

        if($trabajo['codigo'] != ''){
			$pestanias[$i] = $servicio['servicio'];
			$i++;
			if(in_array($servicio['referencia'], $conLOPD)){
				$pestanias[$i] = 'LOPD';
				$i++;
			}
		}
	}

	$first = true;

	campoOculto($cliente, 'codigoCliente');

	creaPestaniasAPI($pestanias);

	$pestana = isset($_POST['pestanaActiva']) ? $pestana = $_POST['pestanaActiva'] : 1;
	
	campoOculto($pestana,'pestanaActiva');
	$i=1;

	$servicios = consultaBD("SELECT * FROM servicios ORDER BY codigo;",true);

	while($servicio = mysql_fetch_assoc($servicios)){

		$trabajo = consultaBD("SELECT 
									t.*,
									s.referencia 
								FROM trabajos t INNER JOIN servicios s ON t.codigoServicio = s.codigo 
								WHERE t. codigoCliente=".$cliente." AND t.codigoServicio = ".$servicio['codigo']."
								ORDER BY codigo DESC LIMIT 1;", true, true);

		$familia = datosRegistro('servicios_familias', $servicio['codigoFamilia']);

		if($trabajo['codigo'] != ''){
			abrePestaniaAPI($i, $pestana == $i ? true : false);
			creaFormulario($i, $familia['referencia'], $servicio['codigo'], $cliente, $trabajo);
			cierraPestaniaAPI();
		
			$i++;
			$first=false;
		
			if(in_array($trabajo['referencia'], $conLOPD)){
				abrePestaniaAPI($i, $pestana == $i ? true : false);
					creaFormulario($i,'LOPD1',$servicio['codigo'],$cliente,$trabajo,true);
				cierraPestaniaAPI();
				$i++;
			}
		}
	}

	cierraPestaniasAPI();

	cierraVentanaGestion('index.php',true);
}

function creaFormulario($indice,$familia,$servicio,$cliente,$trabajo,$adicional=false){
	if($familia=='LOPD1' || $familia=='LOPD2'){
		formularioLOPD($indice,$cliente,$servicio,$trabajo,$adicional);
	} else if($familia=='PRL1'){
		//formularioPRL($indice,$cliente,$servicio);
	} else if($familia=='LOPDCP'){
		//formularioLOPDComunidad($indice,$cliente,$servicio);
	} else if($familia=='PBC1'){
		formularioPBLAC($indice,$cliente,$servicio,$trabajo);
	} else if($familia=='APPCC'){
		//formularioAPPCC($indice,$cliente,$servicio);
	} else if($familia=='ALERG'){
		//formularioAlargenos($indice,$cliente,$servicio);
	} else if($familia=='LSSI'){
		//formularioLSSI($indice,$cliente,$servicio);
	} else if($familia=='COMPL'){
		//formularioCompliance($indice,$cliente,$servicio);
	}
}

function formularioLOPD($i, $cliente, $servicio, $trabajo, $adicional = false){
	campoOculto($servicio, 'servicio'.$i);	
	$formulario = recogerFormularioServicios($trabajo);
	$formulario = completarDatosLOPD($formulario, $cliente);
	
	abreColumnaCampos();
		campoOculto('lopd1', 'tabla'.$i);
		campoOculto($trabajo['codigo'],'trabajo'.$i);
		campoTextoFormulario(1,$i,'Consultor',$formulario);
		if($adicional){
			campoRadioFormulario(610,$i,'Toma de datos finalizada',$formulario);
		} else {
			campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
			campoOculto($formulario['pregunta610'],'pregunta'.$i.'610');
		}
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RESPONSABLE DEL FICHERO</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,$i,'Razón Social',$formulario);
		campoTextoFormulario(4,$i,'Dirección Social',$formulario);
		campoTextoFormulario(5,$i,'Población',$formulario);
		campoTextoFormulario(6,$i,'Teléfono',$formulario);
		campoTextoFormulario(7,$i,'Email de la empresa',$formulario);
		campoDato('Representante legal','');
		campoTextoFormulario(8,$i,'Nombre',$formulario);
		campoTextoFormulario(627,$i,'Primer apellido',$formulario);
		campoTextoFormulario(628,$i,'Segundo apellido',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,$i,'NIF',$formulario);
		campoTextoFormulario(10,$i,'Código postal',$formulario);
		campoTextoFormulario(11,$i,'Provincia',$formulario);
		campoTextoFormulario(12,$i,'Fax',$formulario);
		campoTextoFormulario(13,$i,'Actividad de la empresa',$formulario);
		campoTextoFormulario(14,$i,'NIF',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		/*campoRadioFormulario(522,$i,'No será necesario el registro de accesos cuando',$formulario,'1',array('El responsable del fichero es una persona física','El responsable del fichero garantice que solo tiene el acceso y trata los datos personales, y se haga constar en el documento de seguridad'),array('1','2'),true);*/
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RESPONSABLE DE SEGURIDAD</h3>";
	abreColumnaCampos();
		campoTextoFormulario(15,$i,'Nombre y apellidos',$formulario);
		campoTextoFormulario(16,$i,'Teléfono',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(17,$i,'NIF',$formulario);
		campoTextoFormulario(19,$i,'E-mail',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DELEGADO DE PROTECCIÓN DE DATOS</h3>";
	abreColumnaCampos();
		campoTextoFormulario(604,$i,'Nombre y apellidos',$formulario);
		campoTextoFormulario(605,$i,'Datos de Contacto',$formulario);
		campoTextoFormulario(608,$i,'Cargo',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(606,$i,'NIF',$formulario);
		campoTextoFormulario(607,$i,'E-mail',$formulario);
		campoTextoFormulario(626,$i,'Dirección',$formulario);
	cierraColumnaCampos();	

	echo "<br clear='all'><h3 class='apartadoFormulario'> PÁGINA WEB</h3>";
		abreColumnaCampos();
			campoRadioFormulario(644, $i, '¿Dispone de página web?', $formulario);
		cierraColumnaCampos(true);

	/*echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre y apellidos' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaDPD'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
							<th> Nombre y apellidos </th>
							<th> NIF </th>
							<th> Tipo </th>
							<th> Dirección </th>
							<th> Tfno </th>
							<th> Email </th>
							<th> Fecha designación </th>
							<th> Fecha baja </th>
						</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$usuarios = consultaBD("SELECT * FROM dpd_lopd WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($usuario= mysql_fetch_assoc($usuarios)){
        		echo "<tr>";
    				campoTextoTabla('nombreDPD'.$indice,$usuario['nombreDPD'],'input-small');
    				campoTextoTabla('nifDPD'.$indice,$usuario['nifDPD'],'input-small');
    				echo '<td>';
    					campoSelectSolo('tipoDPD'.$indice,'',array('Plantilla del responsable','Plantilla del encargado de tratamiento','Desempeña sus funciones en el marco de un contrato de servicios'),array(1,2,3),$usuario['tipoDPD'],'selectpicker span3 tipoDPD','');
    					$clase=$usuario['tipoDPD']==2?'':'hide';
    					echo '<div id="divEncargadoDPD'.$indice.'" class="divEncargadoDPD '.$clase.'">';
    						echo 'Encargado:';
    						campoTextoSolo('encargadoDPD'.$indice,$usuario['encargadoDPD'],'span2');
    					echo '</div>';
    				echo '</td>';
    				campoTextoTabla('direccionDPD'.$indice,$usuario['direccionDPD']);
    				campoTextoTabla('telefonoDPD'.$indice,$usuario['telefonoDPD'],'input-mini');
    				campoTextoTabla('emailDPD'.$indice,$usuario['emailDPD'],'input-small');
    				campoFechaTabla('fechaAltaDPD'.$indice,$usuario['fechaAltaDPD']);
    				campoFechaTabla('fechaBajaDPD'.$indice,$usuario['fechaBajaDPD']);
				echo"
					</tr>";
        		$indice++;
        	}
        }
        $maximo=$indice+2;
    	for($indice;$indice<$maximo;$indice++){
    		echo "<tr>";
    			campoTextoTabla('nombreDPD'.$indice,'','input-small');
    			campoTextoTabla('nifDPD'.$indice,'','input-small');
    			echo '<td>';
    			campoSelectSolo('tipoDPD'.$indice,'',array('Plantilla del responsable','Plantilla del encargado de tratamiento','Desempeña sus funciones en el marco de un contrato de servicios'),array(1,2,3),'','selectpicker span3 tipoDPD','');
    			echo '<div id="divEncargadoDPD'.$indice.'" class="divEncargadoDPD hide">';
    			echo 'Encargado:';
    			campoTextoSolo('encargadoDPD'.$indice,'','span2');
    			echo '</div>';
    			echo '</td>';
    			campoTextoTabla('direccionDPD'.$indice);
    			campoTextoTabla('telefonoDPD'.$indice,'','input-mini');
    			campoTextoTabla('emailDPD'.$indice,'','input-small');
    			campoFechaTabla('fechaAltaDPD'.$indice);
    			campoFechaTabla('fechaBajaDPD'.$indice);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFilaDPD(\"tablaDPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";*/

	echo '<div class="hide">';
	echo "<br clear='all'><h3 class='apartadoFormulario'> FICHEROS A DECLARAR</h3>";
	$arrayFichero=array();
	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(20,$i,'CLIENTES',$formulario);
		echo '</div>';
		if($formulario['pregunta20']=='SI'){
			array_push($arrayFichero, 'CLIENTES');
		}
		echo "<div id='div".$i."20' class='hidden'>";
			campoRadioFormulario(21,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(22,$i,'Nombre',$formulario);
			campoRadioFormulario(23,$i,'DNI',$formulario);
			campoRadioFormulario(24,$i,'Domicilio',$formulario);
			campoRadioFormulario(25,$i,'Tlf',$formulario);
			campoRadioFormulario(26,$i,'Email',$formulario);
			campoRadioFormulario(27,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(28,$i,'Currículum',$formulario);
			campoRadioFormulario(29,$i,'Datos de salud',$formulario);
			campoRadioFormulario(30,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(31,$i,'PROVEEDORES',$formulario);
		echo '</div>';
		if($formulario['pregunta31']=='SI'){
			array_push($arrayFichero, 'PROVEEDORES');
		}
		echo "<div id='div".$i."31' class='hidden'>";
			campoRadioFormulario(32,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(33,$i,'Nombre',$formulario);
			campoRadioFormulario(34,$i,'DNI',$formulario);
			campoRadioFormulario(35,$i,'Domicilio',$formulario);
			campoRadioFormulario(36,$i,'Tlf',$formulario);
			campoRadioFormulario(37,$i,'Email',$formulario);
			campoRadioFormulario(38,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(39,$i,'Currículum',$formulario);
			campoRadioFormulario(40,$i,'Datos de salud',$formulario);
			campoRadioFormulario(41,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(42,$i,'EMPLEADOS',$formulario);
		echo '</div>';
		if($formulario['pregunta42']=='SI'){
			array_push($arrayFichero, 'EMPLEADOS');
		}
		echo "<div id='div".$i."42' class='hidden'>";
			campoRadioFormulario(43,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(44,$i,'Nombre',$formulario);
			campoRadioFormulario(45,$i,'DNI',$formulario);
			campoRadioFormulario(46,$i,'Domicilio',$formulario);
			campoRadioFormulario(47,$i,'Tlf',$formulario);
			campoRadioFormulario(48,$i,'Email',$formulario);
			campoRadioFormulario(49,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(50,$i,'Currículum',$formulario);
			campoRadioFormulario(51,$i,'Datos de salud',$formulario);
			campoRadioFormulario(52,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";

	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(53,$i,'CURRÍCULUMS',$formulario);
		echo '</div>';
		if($formulario['pregunta53']=='SI'){
			array_push($arrayFichero, 'CURRÍCULUMS');
		}
		echo "<div id='div".$i."53' class='hidden'>";
			campoRadioFormulario(54,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(55,$i,'Nombre',$formulario);
			campoRadioFormulario(56,$i,'DNI',$formulario);
			campoRadioFormulario(57,$i,'Domicilio',$formulario);
			campoRadioFormulario(58,$i,'Tlf',$formulario);
			campoRadioFormulario(59,$i,'Email',$formulario);
			campoRadioFormulario(60,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(61,$i,'Currículum',$formulario);
			campoRadioFormulario(62,$i,'Datos de salud',$formulario);
			campoRadioFormulario(63,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(64,$i,'VIDEOVIGILANCIA',$formulario);
		echo '</div>';
		if($formulario['pregunta64']=='SI'){
			array_push($arrayFichero, 'VIDEOVIGILANCIA');
		}
		echo "<div id='div".$i."64' class='hidden'>";
			campoRadioFormulario(65,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(66,$i,'Nombre',$formulario);
			campoRadioFormulario(67,$i,'DNI',$formulario);
			campoRadioFormulario(68,$i,'Domicilio',$formulario);
			campoRadioFormulario(69,$i,'Tlf',$formulario);
			campoRadioFormulario(70,$i,'Email',$formulario);
			campoRadioFormulario(71,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(72,$i,'Currículum',$formulario);
			campoRadioFormulario(73,$i,'Datos de salud',$formulario);
			campoRadioFormulario(74,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(75,$i,'USUARIOS WEB',$formulario);
		echo '</div>';
		if($formulario['pregunta75']=='SI'){
			array_push($arrayFichero, 'USUARIOS WEB');
		}
		echo "<div id='div".$i."75' class='hidden'>";
			campoRadioFormulario(76,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(77,$i,'Nombre',$formulario);
			campoRadioFormulario(78,$i,'DNI',$formulario);
			campoRadioFormulario(79,$i,'Domicilio',$formulario);
			campoRadioFormulario(80,$i,'Tlf',$formulario);
			campoRadioFormulario(81,$i,'Email',$formulario);
			campoRadioFormulario(82,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(83,$i,'Currículum',$formulario);
			campoRadioFormulario(84,$i,'Datos de salud',$formulario);
			campoRadioFormulario(85,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";

	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(443,$i,'PACIENTES',$formulario);
		echo '</div>';
		if($formulario['pregunta443']=='SI'){
			array_push($arrayFichero, 'PACIENTES');
		}
		echo "<div id='div".$i."443' class='hidden'>";
			campoRadioFormulario(444,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(445,$i,'Nombre',$formulario);
			campoRadioFormulario(446,$i,'DNI',$formulario);
			campoRadioFormulario(447,$i,'Domicilio',$formulario);
			campoRadioFormulario(448,$i,'Tlf',$formulario);
			campoRadioFormulario(449,$i,'Email',$formulario);
			campoRadioFormulario(450,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(451,$i,'Currículum',$formulario);
			campoRadioFormulario(452,$i,'Datos de salud',$formulario);
			campoRadioFormulario(453,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(454,$i,'SOCIOS',$formulario);
		echo '</div>';
		if($formulario['pregunta454']=='SI'){
			array_push($arrayFichero, 'SOCIOS');
		}
		echo "<div id='div".$i."454' class='hidden'>";
			campoRadioFormulario(455,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(456,$i,'Nombre',$formulario);
			campoRadioFormulario(457,$i,'DNI',$formulario);
			campoRadioFormulario(458,$i,'Domicilio',$formulario);
			campoRadioFormulario(459,$i,'Tlf',$formulario);
			campoRadioFormulario(460,$i,'Email',$formulario);
			campoRadioFormulario(461,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(462,$i,'Currículum',$formulario);
			campoRadioFormulario(463,$i,'Datos de salud',$formulario);
			campoRadioFormulario(464,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		echo '<div class="divFicheros">';
		campoRadioFormulario(465,$i,'ALUMNOS',$formulario);
		echo '</div>';
		if($formulario['pregunta465']=='SI'){
			array_push($arrayFichero, 'ALUMNOS');
		}
		echo "<div id='div".$i."465' class='hidden'>";
			campoRadioFormulario(466,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(467,$i,'Nombre',$formulario);
			campoRadioFormulario(468,$i,'DNI',$formulario);
			campoRadioFormulario(469,$i,'Domicilio',$formulario);
			campoRadioFormulario(470,$i,'Tlf',$formulario);
			campoRadioFormulario(471,$i,'Email',$formulario);
			campoRadioFormulario(472,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(473,$i,'Currículum',$formulario);
			campoRadioFormulario(474,$i,'Datos de salud',$formulario);
			campoRadioFormulario(475,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";

	abreColumnaCampos();
		echo '<div class="divFicheros">';	
		campoRadioFormulario(476,$i,'VOLUNTARIOS',$formulario);
		echo '</div>';
		if($formulario['pregunta476']=='SI'){
			array_push($arrayFichero, 'VOLUNTARIOS');
		}
		echo "<div id='div".$i."476' class='hidden'>";
			campoRadioFormulario(477,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(478,$i,'Nombre',$formulario);
			campoRadioFormulario(479,$i,'DNI',$formulario);
			campoRadioFormulario(480,$i,'Domicilio',$formulario);
			campoRadioFormulario(481,$i,'Tlf',$formulario);
			campoRadioFormulario(482,$i,'Email',$formulario);
			campoRadioFormulario(483,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(484,$i,'Currículum',$formulario);
			campoRadioFormulario(485,$i,'Datos de salud',$formulario);
			campoRadioFormulario(486,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	/*abreColumnaCampos();
		campoRadioFormulario(487,$i,'OTROS',$formulario);
		echo "<div id='div".$i."487' class='hidden'>";
			campoTextoFormulario(488,$i,'Indicar',$formulario);
			campoRadioFormulario(489,$i,'¿Dónde se guardan los datos contenidos en el fichero?',$formulario,'PAPEL',array('Papel','PC','Ambos'),array('PAPEL','PC','AMBOS'),true);
			campoDato('Tipos de datos','');
			campoRadioFormulario(490,$i,'Nombre',$formulario);
			campoRadioFormulario(491,$i,'DNI',$formulario);
			campoRadioFormulario(492,$i,'Domicilio',$formulario);
			campoRadioFormulario(493,$i,'Tlf',$formulario);
			campoRadioFormulario(494,$i,'Email',$formulario);
			campoRadioFormulario(495,$i,'Cta. Bancaria',$formulario);
			campoRadioFormulario(495,$i,'Currículum',$formulario);
			campoRadioFormulario(496,$i,'Datos de salud',$formulario);
			campoRadioFormulario(497,$i,'Afiliación',$formulario);
		echo "</div>";
	cierraColumnaCampos();*/

	$j=0;
	if($trabajo){
		$ficheros=consultaBD('SELECT * FROM otros_ficheros_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
		while($fichero=mysql_fetch_assoc($ficheros)){
			array_push($arrayFichero, $fichero['nombreFichero']);
			abreColumnaCampos('span3 spanOtroFichero');
				echo '<div class="divOtroFicheros">';
				campoRadio('otroFicheroLOPD_'.$j,'OTRO','SI');
				echo '</div>';
				echo "<div id='divotroFicheroLOPD_".$j."' class='divFicheroOculto hidden'>";
					campoTexto('nombreFicheroLOPD_'.$j,'Nombre del fichero',$fichero['nombreFichero'],'input-large nombreNuevoFichero');
					campoTexto('finalidadFicheroLOPD_'.$j,'Finalidad',$fichero['finalidadFichero']);
					campoTexto('origenFicheroLOPD_'.$j,'Origen de los datos',$fichero['origenFichero']);
					campoTexto('colectivoFicheroLOPD_'.$j,'Colectivos interesados',$fichero['colectivoFichero']);
					campoSelect('datosFicheroLOPD_'.$j,'Datos tratados',array('Ideología','Afiliación sindical','Religión', 'Creencias', 'Origen racial o étnico', 'Salud', 'Vida sexual', 'Violencia de Género', 'Datos recabados para fines policiales sin consentimiento de las personas afectadas', 'Comisión de infracciones administrativas o penales', 'Evaluación aspectos de la personalidad', 'Otros datos'),array('IDEOLOGIA','AFILICIACION','RELIGION','CREENCIAS','ORIGEN','SALUD','VIDA','VIOLENCIA','DATOS','COMISION','EVALUACION','OTROS'),$fichero['datosFichero'],'selectpicker span2 show-tick');
					campoRadio('sistemaFicheroLOPD_'.$j,'Sistema de tratamiento',$fichero['sistemaFichero'],'AUTO',array('Automatizado','Manual', 'Mixto'),array('AUTO','MANUAL','MIXTO'),true);
					campoRadio('nivelFicheroLOPD_'.$j,'Nivel de seguridad',$fichero['nivelFichero'],'ALTO',array('Alto','Básico', 'Bajo'),array('ALTO','BASICO','BAJO'),true);
					campoFecha('fechaAltaFicheroLOPD_'.$j,'Fecha solicitud alta del fichero',$fichero['fechaAltaFichero']);
					campoFecha('fechaBajaFicheroLOPD_'.$j,'Fecha baja del fichero',$fichero['fechaBajaFichero']);
				echo "</div>";
			cierraColumnaCampos();
			$j++;
		}
	}
	abreColumnaCampos('span3 spanOtroFichero');
		echo '<div class="divOtroFicheros">';
		campoRadio('otroFicheroLOPD_'.$j,'OTRO');
		echo '</div>';
		echo "<div id='divotroFicheroLOPD_".$j."' class='divFicheroOculto hidden'>";
			campoTexto('nombreFicheroLOPD_'.$j,'Nombre del fichero','','input-large nombreNuevoFichero');
			campoTexto('finalidadFicheroLOPD_'.$j,'Finalidad');
			campoTexto('origenFicheroLOPD_'.$j,'Origen de los datos');
			campoTexto('colectivoFicheroLOPD_'.$j,'Colectivos interesados');
			campoSelect('datosFicheroLOPD_'.$j,'Datos tratados',array('Ideología','Afiliación sindical','Religión', 'Creencias', 'Origen racial o étnico', 'Salud', 'Vida sexual', 'Violencia de Género', 'Datos recabados para fines policiales sin consentimiento de las personas afectadas', 'Comisión de infracciones administrativas o penales', 'Evaluación aspectos de la personalidad', 'Otros datos'),array('IDEOLOGIA','AFILICIACION','RELIGION','CREENCIAS','ORIGEN','SALUD','VIDA','VIOLENCIA','DATOS','COMISION','EVALUACION','OTROS'),false,'selectpicker span2 show-tick');
			campoRadio('sistemaFicheroLOPD_'.$j,'Sistema de tratamiento',false,'AUTO',array('Automatizado','Manual', 'Mixto'),array('AUTO','MANUAL','MIXTO'),true);
			campoRadio('nivelFicheroLOPD_'.$j,'Nivel de seguridad',false,'ALTO',array('Alto','Básico', 'Bajo'),array('ALTO','BASICO','BAJO'),true);
			campoFecha('fechaAltaFicheroLOPD_'.$j,'Fecha solicitud alta del fichero');
			campoFecha('fechaBajaFicheroLOPD_'.$j,'Fecha baja del fichero');
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span3 spanBoton');
		echo '<button id="nuevoFichero" type="button" class="btn btn-propio">Añadir fichero</button>';
	cierraColumnaCampos();
	echo '</div>';

	echo "<br clear='all'><h3 class='apartadoFormulario'> USUARIOS QUE ACCEDEN AL FICHERO</h3>";

	/*echo"	
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCentros'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre y apellidos </th>
							<th> NIF </th>
							<th> ¿Pueden sacar datos? </th>
							<th> ¿A que ficheros acceden? </th>
							<th> ¿En qué tipo de soportes? </th>
							<th> Accesos </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    	$numero=86;
    	for($indice=0;$indice<12;$indice++){
    		echo "<tr>";
				campoTextoTablaFormulario($numero++,$i,$formulario);
				campoTextoTablaFormulario($numero++,$i,$formulario,'input-small');
				campoRadioTablaFormulario($numero++,$i,$formulario,'NO',array('Si','No'),array('SI','NO'),true);
				areaTextoTablaFormulario($numero++,$i,$formulario);
				areaTextoTablaFormulario($numero++,$i,$formulario);
				campoRadioTablaFormulario($numero++,$i,$formulario,'X',array('Acceso total','Creacción/Modificación/Borrado','Sólo lectura'),array('X','W','R'),true);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
		 	";*/
	for($indice=86;$indice<158;$indice++){
		campoOculto('','pregunta'.$i.$indice);
	}
	$nombresAsignados=array('');
	$codigosAsignados=array('');
	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre y apellidos' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaUsuariosLOPD'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
							<th> Nombre y apellidos </th>
							<th> NIF </th>
							<th> Puesto </th>
							<th> Fecha alta </th>
							<th> Fecha baja </th>
							<th> ¿Pueden sacar datos? </th>
							<th> ¿A que ficheros acceden? </th>
							<th> ¿En qué tipo de soportes? </th>
							<th> Accesos </th>
						</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$usuarios = consultaBD("SELECT * FROM usuarios_lopd WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($usuario= mysql_fetch_assoc($usuarios)){
        		echo "<tr>";
        			campoOculto($usuario['codigo'],'existeUsuarioLOPD'.$indice);
        			array_push($codigosAsignados, 'usuarios_'.$usuario['codigo']);
        			array_push($nombresAsignados, $usuario['nombreUsuarioLOPD']);
    				campoTextoTabla('nombreUsuarioLOPD'.$indice,$usuario['nombreUsuarioLOPD'],'input-small');
    				campoTextoTabla('nifUsuarioLOPD'.$indice,$usuario['nifUsuarioLOPD'],'input-small');
    				campoTextoTabla('puestoUsuarioLOPD'.$indice,$usuario['puestoUsuarioLOPD'],'input-small');
    				campoFechaTabla('fechaAltaUsuarioLOPD'.$indice,$usuario['fechaAltaUsuarioLOPD']);
    				campoFechaTabla('fechaBajaUsuarioLOPD'.$indice,$usuario['fechaBajaUsuarioLOPD']);
    				campoSelect('datosUsuarioLOPD'.$indice,'',array('No','Si'),array('NO','SI'),$usuario['datosUsuarioLOPD'],'selectpicker input-mini show-tick','data-live-search="true"',1);
    				campoSelectConsultaMultipleFicheros('ficherosUsuarioLOPD'.$indice,'',$arrayFichero,$arrayFichero,$usuario['ficherosUsuarioLOPD'],'selectpicker span3 show-tick multipleFicheros',"data-live-search='true'",1,$cliente);
    				campoSelectMultiple('soportesUsuarioLOPD'.$indice,'',array('Automatizado','Manual'),array('AUTO','MANUAL'),$usuario['soportesUsuarioLOPD'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoSelect('accesoUsuarioLOPD'.$indice,'',array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),$usuario['accesoUsuarioLOPD'],'selectpicker span2 show-tick','data-live-search="true"',1);
				echo"
					</tr>";
        		$indice++;
        	}
        }
        $maximo=$indice+2;
    	for($indice;$indice<$maximo;$indice++){
    		echo "<tr>";
    			campoOculto('','existeUsuarioLOPD'.$indice);
    			campoTextoTabla('nombreUsuarioLOPD'.$indice,'','input-small');
    			campoTextoTabla('nifUsuarioLOPD'.$indice,'','input-small');
    			campoTextoTabla('puestoUsuarioLOPD'.$indice,'','input-small');
    			campoFechaTabla('fechaAltaUsuarioLOPD'.$indice);
    			campoFechaTabla('fechaBajaUsuarioLOPD'.$indice);
    			campoSelect('datosUsuarioLOPD'.$indice,'',array('No','Si'),array('NO','SI'),'','selectpicker input-mini show-tick','data-live-search="true"',1);
    			campoSelectConsultaMultipleFicheros('ficherosUsuarioLOPD'.$indice,'',$arrayFichero,$arrayFichero,false,'selectpicker span3 show-tick multipleFicheros',"data-live-search='true'",1,$cliente);
    			campoSelectMultiple('soportesUsuarioLOPD'.$indice,'',array('Automatizado','Manual'),array('AUTO','MANUAL'),false,'selectpicker span2 show-tick','data-live-search="true"',1);
    			campoSelect('accesoUsuarioLOPD'.$indice,'',array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'','selectpicker span2 show-tick','data-live-search="true"',1);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFilaUsuariosLOPD(\"tablaUsuariosLOPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> PROVEEDORES SIN ACCESO A DATOS Y CON ACCESO A LAS DEPENDENCIAS</h3>";
	echo"	<br clear='all'>
			Para eliminar deja el campo 'Nombre o denominación' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaProveedoresLOPD'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
							<th> Nombre o denominación </th>
							<th> NIF </th>
							<th> Representante legal </th>
							<th> Servicio </th>
							<th> Fecha alta </th>
							<th> Fecha baja </th>
						</tr>
                  	</thead>
                  	<tbody>";
            $indice=0;
        	if($trabajo){
        		$proveedores = consultaBD("SELECT * FROM proveedores_lopd WHERE codigoTrabajo=".$trabajo['codigo'],true);
        		while($proveedor= mysql_fetch_assoc($proveedores)){
        			echo "<tr>";
    				campoOculto($proveedor['codigo'],'existeProveedorLOPD'.$indice);
    				campoTextoTabla('nombreProveedorLOPD'.$indice,$proveedor['nombre']);
    				campoTextoTabla('nifProveedorLOPD'.$indice,$proveedor['nif'],'input-small');
    				campoTextoTabla('representanteProveedorLOPD'.$indice,$proveedor['representante']);
    				campoTextoTabla('servicioProveedorLOPD'.$indice,$proveedor['servicio']);
    				campoFechaTabla('fechaAltaProveedorLOPD'.$indice,$proveedor['fechaAlta']);
    				campoFechaTabla('fechaBajaProveedorLOPD'.$indice,$proveedor['fechaBaja']);
					echo"</tr>";
					$indice++;
        		}
        	}
        	$maximo=$indice+2;
    		for($indice;$indice<$maximo;$indice++){
    			echo "<tr>";
    				campoOculto('','existeProveedorLOPD'.$indice);
    				campoTextoTabla('nombreProveedorLOPD'.$indice);
    				campoTextoTabla('nifProveedorLOPD'.$indice,'','input-small');
    				campoTextoTabla('representanteProveedorLOPD'.$indice);
    				campoTextoTabla('servicioProveedorLOPD'.$indice);
    				campoFechaTabla('fechaAltaProveedorLOPD'.$indice);
    				campoFechaTabla('fechaBajaProveedorLOPD'.$indice);
				echo"</tr>";
    		}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaProveedoresLOPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";
	echo "<br clear='all'><h3 class='apartadoFormulario'> SUBCONTRATACIÓN Y ENCARGADOS DE TRATAMIENTO</h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta158']=='SI' && $formulario['pregunta159']!=''){
			array_push($codigosAsignados, 'encargadofijo_158');
        	array_push($nombresAsignados, $formulario['pregunta159']);
		}
		campoRadioEncargado(158,$i,'¿Cuentan con alguna asesoría laboral para la realización de nóminas?',$formulario);
		echo "<div id='div".$i."158' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(159,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(160,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(161,$i,'Dirección Social',$formulario);
				campoTextoFormulario(162,$i,'E-mail',$formulario);
				campoTextoFormulario(163,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(611,$i,$formulario);
				campoTextoFormulario(164,$i,'Código postal',$formulario);
				campoTextoFormulario(165,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(166,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(167,$i,'Rpt. Legal',$formulario);;
				campoSelectMultipleFormulario(511,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(629,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio158Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(541,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(543,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(544,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(574,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(589,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,158,574,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',158);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta168']=='SI' && $formulario['pregunta169']!=''){
			array_push($codigosAsignados, 'encargadofijo_168');
        	array_push($nombresAsignados, $formulario['pregunta169']);
		}
		campoRadioEncargado(168,$i,'¿Cuentan con alguna asesoría contable/fiscal?',$formulario);
		echo "<div id='div".$i."168' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(169,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(170,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(171,$i,'Dirección Social',$formulario);
				campoTextoFormulario(172,$i,'E-mail',$formulario);
				campoTextoFormulario(173,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(612,$i,$formulario);
				campoTextoFormulario(174,$i,'Código postal',$formulario);
				campoTextoFormulario(175,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(176,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(177,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(512,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(630,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio168Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(527,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(545,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(546,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(575,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(590,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,168,575,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',168);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta178']=='SI' && $formulario['pregunta179']!=''){
			array_push($codigosAsignados, 'encargadofijo_178');
        	array_push($nombresAsignados, $formulario['pregunta179']);
		}
		campoRadioEncargado(178,$i,'¿Cuentan con alguna empresa de video vigilancia?',$formulario);
		echo "<div id='div".$i."178' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(179,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(180,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(181,$i,'Dirección Social',$formulario);
				campoTextoFormulario(182,$i,'E-mail',$formulario);
				campoTextoFormulario(183,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(613,$i,$formulario);
				campoTextoFormulario(184,$i,'Código postal',$formulario);
				campoTextoFormulario(185,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(186,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(187,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(513,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(631,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio178Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(542,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(547,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(548,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(576,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(591,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
				campoRadioFormulario(188,$i,'¿Llegan a tener acceso a las imágenes?',$formulario);
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,178,576,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',178);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta189']=='SI' && $formulario['pregunta190']!=''){
			array_push($codigosAsignados, 'encargadofijo_189');
        	array_push($nombresAsignados, $formulario['pregunta190']);
		}
		campoRadioEncargado(189,$i,'¿Cuentan con alguna empresa de PRL?',$formulario);
		echo "<div id='div".$i."189' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(190,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(191,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(192,$i,'Dirección Social',$formulario);
				campoTextoFormulario(193,$i,'E-mail',$formulario);
				campoTextoFormulario(194,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(614,$i,$formulario);
				campoTextoFormulario(195,$i,'Código postal',$formulario);
				campoTextoFormulario(196,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(197,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(198,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(514,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(632,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio189Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(528,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(549,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(550,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(577,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(592,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
				campoRadioFormulario(199,$i,'Especialidad: Vigilancia de la Salud',$formulario);
				campoRadioFormulario(497,$i,'Especialidad: Ergonomía',$formulario);
				campoRadioFormulario(498,$i,'Especialidad: Seguridad',$formulario);
				campoRadioFormulario(499,$i,'Especialidad: Higiene',$formulario);
				campoRadioFormulario(500,$i,'Especialidad: Otros',$formulario);
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,189,577,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',189);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta200']=='SI' && $formulario['pregunta201']!=''){
			array_push($codigosAsignados, 'encargadofijo_200');
        	array_push($nombresAsignados, $formulario['pregunta201']);
		}
		campoRadioEncargado(200,$i,'¿Cuentan con alguna empresa que realice el mantenimiento de equipos informáticos?',$formulario);
		echo "<div id='div".$i."200' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(201,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(202,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(203,$i,'Dirección Social',$formulario);
				campoTextoFormulario(204,$i,'E-mail',$formulario);
				campoTextoFormulario(205,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(615,$i,$formulario);
				campoTextoFormulario(206,$i,'Código postal',$formulario);
				campoTextoFormulario(207,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(208,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(209,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(515,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(633,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio200Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(529,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(551,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(552,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(578,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(593,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,200,578,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',200);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta210']=='SI' && $formulario['pregunta211']!=''){
			array_push($codigosAsignados, 'encargadofijo_210');
        	array_push($nombresAsignados, $formulario['pregunta211']);
		}
		campoRadioEncargado(210,$i,'¿Cuentan con alguna empresa que realice el mantenimiento de aplicaciones?',$formulario);
		echo "<div id='div".$i."210' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(211,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(212,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(213,$i,'Dirección Social',$formulario);
				campoTextoFormulario(214,$i,'E-mail',$formulario);
				campoTextoFormulario(215,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(616,$i,$formulario);
				campoTextoFormulario(216,$i,'Código postal',$formulario);
				campoTextoFormulario(217,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(218,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(219,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(516,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(634,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio210Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(530,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(553,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(554,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(579,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(594,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,210,579,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',210);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta220']=='SI' && $formulario['pregunta221']!=''){
			array_push($codigosAsignados, 'encargadofijo_220');
        	array_push($nombresAsignados, $formulario['pregunta221']);
		}	
		campoRadioEncargado(220,$i,'¿Cuentan con alguna empresa que realice el mantenimiento de la página web?',$formulario);
		echo "<div id='div".$i."220' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(221,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(222,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(223,$i,'Dirección Social',$formulario);
				campoTextoFormulario(224,$i,'E-mail',$formulario);
				campoTextoFormulario(225,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(617,$i,$formulario);
				campoTextoFormulario(226,$i,'Código postal',$formulario);
				campoTextoFormulario(227,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(228,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(229,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(517,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(635,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio220Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(531,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(555,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(556,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(609,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(595,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,220,609,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',220);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta230']=='SI' && $formulario['pregunta231']!=''){
			array_push($codigosAsignados, 'encargadofijo_230');
        	array_push($nombresAsignados, $formulario['pregunta231']);
		}	
		campoRadioEncargado(230,$i,'¿Cuentan con alguna empresa que realice copias de seguridad "Backup"? ',$formulario);
		echo "<div id='div".$i."230' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(231,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(232,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(233,$i,'Dirección Social',$formulario);
				campoTextoFormulario(234,$i,'E-mail',$formulario);
				campoTextoFormulario(235,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(618,$i,$formulario);
				campoTextoFormulario(236,$i,'Código postal',$formulario);
				campoTextoFormulario(237,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(238,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(239,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(518,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(636,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio230Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(532,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(557,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(558,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(580,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(596,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,230,580,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',230);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta240']=='SI' && $formulario['pregunta241']!=''){
			array_push($codigosAsignados, 'encargadofijo_240');
        	array_push($nombresAsignados, $formulario['pregunta241']);
		}
		campoRadioEncargado(240,$i,'¿Cuentan con alguna empresa de transporte/mensajería para envíos a clientes? ',$formulario);
		echo "<div id='div".$i."240' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(241,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(242,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(243,$i,'Dirección Social',$formulario);
				campoTextoFormulario(244,$i,'E-mail',$formulario);
				campoTextoFormulario(245,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(619,$i,$formulario);
				campoTextoFormulario(246,$i,'Código postal',$formulario);
				campoTextoFormulario(247,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(248,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(249,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(519,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(637,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio240Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(533,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(559,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(560,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(581,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(597,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,240,581,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',240);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta250']=='SI' && $formulario['pregunta251']!=''){
			array_push($codigosAsignados, 'encargadofijo_250');
        	array_push($nombresAsignados, $formulario['pregunta251']);
		}
		campoRadioEncargado(250,$i,'¿Cuentan con alguna empresa de limpieza?',$formulario);
		echo "<div id='div".$i."250' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(251,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(252,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(253,$i,'Dirección Social',$formulario);
				campoTextoFormulario(254,$i,'E-mail',$formulario);
				campoTextoFormulario(255,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(620,$i,$formulario);
				campoTextoFormulario(256,$i,'Código postal',$formulario);
				campoTextoFormulario(257,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(258,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(259,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(520,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(638,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio250Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(534,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(561,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(562,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(582,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(598,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,250,582,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',250);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta260']=='SI' && $formulario['pregunta261']!=''){
			array_push($codigosAsignados, 'encargadofijo_260');
        	array_push($nombresAsignados, $formulario['pregunta261']);
		}
		campoRadioEncargado(260,$i,'¿Cuentan con alguna empresa de mantenimiento de instalaciones?',$formulario);
		echo "<div id='div".$i."260' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(261,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(262,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(263,$i,'Dirección Social',$formulario);
				campoTextoFormulario(264,$i,'E-mail',$formulario);
				campoTextoFormulario(265,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(621,$i,$formulario);
				campoTextoFormulario(266,$i,'Código postal',$formulario);
				campoTextoFormulario(267,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(268,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(269,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(521,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(639,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio260Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(535,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(563,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(564,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(583,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(599,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,260,583,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',260);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta270']=='SI' && $formulario['pregunta271']!=''){
			array_push($codigosAsignados, 'encargadofijo_270');
        	array_push($nombresAsignados, $formulario['pregunta271']);
		}
		campoRadioEncargado(270,$i,'¿Cuentan con alguna empresa de vigilancia/seguridad?',$formulario);
		echo "<div id='div".$i."270' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(271,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(272,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(273,$i,'Dirección Social',$formulario);
				campoTextoFormulario(274,$i,'E-mail',$formulario);
				campoTextoFormulario(275,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(622,$i,$formulario);
				campoTextoFormulario(276,$i,'Código postal',$formulario);
				campoTextoFormulario(277,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(278,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(279,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(522,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(640,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio270Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(536,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(565,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(566,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(584,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(600,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,270,584,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',270);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta280']=='SI' && $formulario['pregunta281']!=''){
			array_push($codigosAsignados, 'encargadofijo_280');
        	array_push($nombresAsignados, $formulario['pregunta281']);
		}
		campoRadioEncargado(280,$i,'¿Cuentan con alguna empresa de mantenimiento de la copiadora? ',$formulario);
		echo "<div id='div".$i."280' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(281,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(282,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(283,$i,'Dirección Social',$formulario);
				campoTextoFormulario(284,$i,'E-mail',$formulario);
				campoTextoFormulario(285,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(623,$i,$formulario);
				campoTextoFormulario(286,$i,'Código postal',$formulario);
				campoTextoFormulario(287,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(288,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(289,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(523,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(641,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio280Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(537,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(567,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(568,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(585,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(601,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,280,585,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',280);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta290']=='SI' && $formulario['pregunta291']!=''){
			array_push($codigosAsignados, 'encargadofijo_290');
        	array_push($nombresAsignados, $formulario['pregunta291']);
		}
		campoRadioEncargado(290,$i,'¿Cuentan con algún agente o colaboración mercantil, que tenga acceso a datos de carácter personal?(Comercial, Calidad, etc) ',$formulario);
		echo "<div id='div".$i."290' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(291,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(292,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(293,$i,'Dirección Social',$formulario);
				campoTextoFormulario(294,$i,'E-mail',$formulario);
				campoTextoFormulario(295,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(624,$i,$formulario);
				campoTextoFormulario(296,$i,'Código postal',$formulario);
				campoTextoFormulario(297,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(298,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(299,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(524,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(642,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio290Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(538,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(569,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(570,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(586,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(602,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
				campoTextoFormulario(18,$i,'Especificar los servicios que presta',$formulario);
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,290,586,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',290);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";

	abreColumnaCampos('span3 spanEncargado');
		if($formulario['pregunta487']=='SI' && $formulario['pregunta488']!=''){
			array_push($codigosAsignados, 'encargadofijo_487');
        	array_push($nombresAsignados, $formulario['pregunta488']);
		}
		campoRadioEncargado(487,$i,'¿Cuentan con alguna empresa que realiza la selección de personal? ',$formulario);
		echo "<div id='div".$i."487' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(488,$i,'Nombre o Razón Social',$formulario);
				campoTextoFormulario(489,$i,'NIF/CIF',$formulario);
				campoTextoFormulario(490,$i,'Dirección Social',$formulario);
				campoTextoFormulario(491,$i,'E-mail',$formulario);
				campoTextoFormulario(492,$i,'Localidad',$formulario);
				campoSelectProvinciaFormulario(625,$i,$formulario);
				campoTextoFormulario(493,$i,'Código postal',$formulario);
				campoTextoFormulario(494,$i,'Teléfono',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(495,$i,'Persona de contacto/Tlf',$formulario);
				campoTextoFormulario(496,$i,'Rpt. Legal',$formulario);
				campoSelectMultipleFormulario(525,$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$formulario,'selectpicker span3 show-tick','',0);
				campoRadioFormulario(643,$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$formulario);
				echo '<div id="radio487Fijo" class="radioSubcontratacionFijo">';
					campoRadioFormulario(571,$i,'¿Está permitida la subcontratación?',$formulario);
				echo '</div>';
				campoFechaFormulario(572,$i,'Fecha del contrato de prestación de servicio',$formulario);
				campoFechaFormulario(544,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
				campoSelectMultipleFormularioFicheros(587,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros ENCARGADO FIJO',"data-live-search='true'",0,$cliente);
				campoSelectFormulario(603,$i,'Accesos',$formulario,0,array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			seccionTratamientos($trabajo,487,587,'FIJO');
			creaTablaSubContratacion($trabajo,'Fijo',487);
		echo "</div>";
	cierraColumnaCampos();
	echo "<br clear='all'><h3 class='apartadoFormulario borderBottom'></h3>";
	$j=0;
	$consulta=consultaBD("SELECT codigo, nombreFichero FROM declaraciones WHERE codigoCliente='$cliente' AND (fechaBaja='0000-00-00' OR fechaBaja>'".date('Y-m-d')."');",true);
	$opciones=array();
	$valores=array();
    while($datosFicheros=mysql_fetch_assoc($consulta)){
        array_push($opciones, $datosFicheros['nombreFichero']);
        array_push($valores, $datosFicheros['codigo']);        
    }
	if($trabajo){
		$encargados=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
		while($encargado=mysql_fetch_assoc($encargados)){
			$hide='';
			$hide = trim($encargado['cifEncargado']) == trim($formulario['pregunta9']) ? 'hide' : '';
			echo "<div id='divotroEncargadoLOPD_".$j."' class='".$hide."'>";
			abreColumnaCampos('span3 spanOtroEncargado');
				abreColumnaCampos();
					campoOculto($encargado['codigo'],'existeEncargadoLOPD_'.$j);
					array_push($codigosAsignados, 'encargado_'.$encargado['codigo']);
        			array_push($nombresAsignados, $encargado['nombreEncargado']);
						campoTexto('servicioEncargadoLOPD_'.$j,'Servicio',$encargado['servicioEncargado']);
						campoTexto('nombreEncargadoLOPD_'.$j,'Nombre o Razón Social',$encargado['nombreEncargado']);
						campoTexto('cifEncargadoLOPD_'.$j,'NIF/CIF',$encargado['cifEncargado']);
						campoTexto('direccionEncargadoLOPD_'.$j,'Dirección Social',$encargado['direccionEncargado']);
						campoTexto('emailEncargadoLOPD_'.$j,'E-mail',$encargado['emailEncargado']);
						campoTexto('localidadEncargadoLOPD_'.$j,'Localidad',$encargado['localidadEncargado']);
						campoSelectProvinciaAEPD('provinciaEncargadoLOPD_'.$j,$encargado['provinciaEncargado']);
						campoTexto('cpEncargadoLOPD_'.$j,'Código postal',$encargado['cpEncargado']);
						campoTexto('telefonoEncargadoLOPD_'.$j,'Teléfono',$encargado['telefonoEncargado']);
				cierraColumnaCampos();
				abreColumnaCampos();
						campoTexto('contactoEncargadoLOPD_'.$j,'Persona de contacto/Tlf',$encargado['contactoEncargado']);
						campoTexto('representanteEncargadoLOPD_'.$j,'Rpt. Legal',$encargado['representanteEncargado']);
						campoSelectMultiple('dondeEncargadoLOPD_'.$j,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$encargado['dondeEncargado'],'selectpicker span3 show-tick','',0);
						campoRadio('exclusivoEncargadoLOPD_'.$j,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$encargado['exclusivoEncargado']);
						echo '<div id="radio'.$j.'" class="radioSubcontratacion">';
						campoRadio('subcontratacionEncargadoLOPD_'.$j,'¿Está permitida la subcontratación?',$encargado['subcontratacionEncargado']);
						echo '</div>';
						campoFecha('fechaAltaEncargadoLOPD_'.$j,'Fecha del contrato de prestación de servicio',$encargado['fechaAltaEncargado']);
						campoFecha('fechaBajaEncargadoLOPD_'.$j,'Fecha de vencimiento del contrato de prestación de servicio',$encargado['fechaBajaEncargado']);
						camposelectMultiple('ficheroEncargadoLOPD_'.$j,'Ficheros a los que acceden',$opciones,$valores,$encargado['ficheroEncargado'],'selectpicker span3 show-tick multipleFicheros ENCARGADO');
						campoSelect('accesosEncargadoLOPD_'.$j,'Accesos',array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),$encargado['accesosEncargado'],'selectpicker span2 show-tick','');
				cierraColumnaCampos(true);
				seccionTratamientos($trabajo,$j,$encargado['codigo'],'NOFIJO');
				creaTablaSubContratacion($trabajo,'',$encargado['codigo'],$j);
			cierraColumnaCampos();
			echo "</div>";
			echo "<br clear='all'><h3 class='apartadoFormulario borderBottom ".$hide."'></h3>";
			$j++;
		}
	}
	abreColumnaCampos('span3 spanOtroEncargado');
		echo "<div id='divotroEncargadoLOPD_".$j."'>";
		abreColumnaCampos();
			campoOculto('','existeEncargadoLOPD_'.$j);
				campoTexto('servicioEncargadoLOPD_'.$j,'Servicio');
				campoTexto('nombreEncargadoLOPD_'.$j,'Nombre o Razón Social');
				campoTexto('cifEncargadoLOPD_'.$j,'NIF/CIF');
				campoTexto('direccionEncargadoLOPD_'.$j,'Dirección Social');
				campoTexto('emailEncargadoLOPD_'.$j,'E-mail');
				campoTexto('localidadEncargadoLOPD_'.$j,'Localidad');
				campoSelectProvinciaAEPD('provinciaEncargadoLOPD_'.$j,false);
				campoTexto('cpEncargadoLOPD_'.$j,'Código postal');
				campoTexto('telefonoEncargadoLOPD_'.$j,'Teléfono');
		cierraColumnaCampos();
		abreColumnaCampos();
				campoTexto('contactoEncargadoLOPD_'.$j,'Persona de contacto/Tlf');
				campoTexto('representanteEncargadoLOPD_'.$j,'Rpt. Legal');
				campoSelectMultiple('dondeEncargadoLOPD_'.$j,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),false,'selectpicker span3 show-tick','',0);
				campoRadio('exclusivoEncargadoLOPD_'.$j,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?');
				echo '<div id="radio'.$j.'" class="radioSubcontratacion">';
					campoRadio('subcontratacionEncargadoLOPD_'.$j,'¿Está permitida la subcontratación?');
				echo '</div>';
				campoFecha('fechaAltaEncargadoLOPD_'.$j,'Fecha del contrato de prestación de servicio');
				campoFecha('fechaBajaEncargadoLOPD_'.$j,'Fecha de vencimiento del contrato de prestación de servicio');
				camposelectMultiple('ficheroEncargadoLOPD_'.$j,'Ficheros a los que acceden',$opciones,$valores,false,'selectpicker span3 show-tick multipleFicheros ENCARGADO');
				campoSelect('accesosEncargadoLOPD_'.$j,'Accesos',array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),$encargado['accesosEncargado'],'selectpicker span2 show-tick','');
		cierraColumnaCampos(true);
		seccionTratamientos($trabajo,$j,0,'NOFIJO');
		creaTablaSubContratacion(false,'',0,$j);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span3 spanBoton');
		echo '<button id="nuevoEncargado" type="button" class="btn btn-propio">Añadir encargado</button>';
	cierraColumnaCampos();

	echo '<div class="hide">';
	echo "<br clear='all'><h3 class='apartadoFormulario'> PUBLICIDAD</h3>";

	abreColumnaCampos();
		campoRadioFormulario(300,$i,'¿Envían publicidad o comunicados a clientes/no clientes?',$formulario);
		echo "<div id='div".$i."300' class='hidden'>";
			campoRadioFormulario(301,$i,'Clientes',$formulario);
			campoRadioFormulario(302,$i,'No clientes',$formulario);
			campoRadioFormulario(303,$i,'¿De qué modo?',$formulario,'POSTAL',array('Vía Postal','Vía electrónica','Ambas'),array('POSTAL','ELECTRONICA','AMBAS'));
			campoRadioFormulario(304,$i,'Destinararios',$formulario,'FISICAS',array('Personas físicas (destinatarios finales)','Personas jurídicas (empresas)','Ambos'),array('FISICAS','JURIDICAS','AMBOS'));
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(305,$i,'¿Cuentan con alguna empresa que lleve los servicios de publicidad? ',$formulario);
		echo "<div id='div".$i."305' class='hidden'>";
			campoTextoFormulario(306,$i,'Nombre o Razón Social',$formulario);
			campoTextoFormulario(307,$i,'NIF/CIF',$formulario);
			campoTextoFormulario(308,$i,'Dirección Social',$formulario);
			campoTextoFormulario(309,$i,'E-mail',$formulario);
			campoTextoFormulario(310,$i,'Localidad',$formulario);
			campoTextoFormulario(311,$i,'Código postal',$formulario);
			campoTextoFormulario(312,$i,'Teléfono',$formulario);
			campoTextoFormulario(313,$i,'Persona de contacto/Tlf',$formulario);
			campoTextoFormulario(314,$i,'Rpt. Legal',$formulario);
			campoRadioFormulario(526,$i,'¿Donde presta los servicios?',$formulario,'ENCARGADO',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),true);
			campoRadioFormulario(540,$i,'¿Está permitida la subcontratación?',$formulario);
			campoFechaFormulario(573,$i,'Fecha del contrato de prestación de servicio',$formulario);
			campoFechaFormulario(539,$i,'Fecha de vencimiento del contrato de prestación de servicio',$formulario);
			campoSelectMultipleFormularioFicheros(588,$i,'Ficheros a los que acceden',$arrayFichero,$arrayFichero,$formulario,'selectpicker span3 show-tick multipleFicheros',"data-live-search='true'",0,$cliente);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> PÁGINA WEB</h3>";

	abreColumnaCampos();
		campoRadioFormulario(315,$i,'¿Disponen de página web? ',$formulario);
		echo "<div id='div".$i."315' class='hidden'>";
			campoRadioFormulario(316,$i,'¿Está adaptada a la LSSI? (Aviso legal, política de cookies, CGV y USO…)',$formulario);
			campoRadioFormulario(317,$i,'¿Puede almacenar datos de particulares mediante formularios?',$formulario);
			campoRadioFormulario(318,$i,'¿Tienen cookies?',$formulario);
			campoRadioFormulario(319,$i,'¿Pueden almacenar datos de particulares?',$formulario);
		echo "</div>";
	cierraColumnaCampos();
	echo '</div>';

	echo "<br clear='all'><h3 class='apartadoFormulario'> EQUIPOS INFORMÁTICOS</h3>";

	abreColumnaCampos();
		campoRadioFormulario(320,$i,'¿Disponen de ordenadores?',$formulario);
		echo "<div id='div".$i."320' class='hidden'>";
			campoTextoFormulario(321,$i,'¿Cuantos?',$formulario);
			campoTextoFormulario(322,$i,'¿Qué sistema operativo utilizan?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(323,$i,'¿Disponen de servidores?',$formulario);
		echo "<div id='div".$i."323' class='hidden'>";
			campoTextoFormulario(324,$i,'¿Cuantos?',$formulario);
			campoTextoFormulario(325,$i,'¿Qué sistema operativo utilizan?',$formulario);
			campoTextoFormulario(326,$i,'¿Donde están alojados',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";

	abreColumnaCampos();
		campoRadioFormulario(501,$i,'¿Disponen de tablets?',$formulario);
		echo "<div id='div".$i."501' class='hidden'>";
			campoTextoFormulario(502,$i,'¿Cuantas?',$formulario);
			campoTextoFormulario(503,$i,'¿Qué sistema operativo utilizan?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(504,$i,'¿Disponen de móviles?',$formulario);
		echo "<div id='div".$i."504' class='hidden'>";
			campoTextoFormulario(505,$i,'¿Cuantos?',$formulario);
			campoTextoFormulario(506,$i,'¿Qué sistema operativo utilizan?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";

	abreColumnaCampos();
		campoRadioFormulario(507,$i,'¿Disponen de acessos remotos?',$formulario);
		echo "<div id='div".$i."507' class='hidden'>";
			campoTextoFormulario(508,$i,'¿Cuantos?',$formulario);
			campoTextoFormulario(509,$i,'¿Qué sistema operativo utilizan?',$formulario);
			campoTextoFormulario(510,$i,'¿Desde donde',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(327,$i,'¿Disponen de ordenadores portatiles?',$formulario);
		echo "<div id='div".$i."327' class='hidden'>";
			campoTextoFormulario(328,$i,'¿Cuantos?',$formulario);
			campoTextoFormulario(329,$i,'¿Qué sistema operativo utilizan?',$formulario);
			campoRadioFormulario(330,$i,'¿Están asignados?',$formulario);
			campoRadioFormulario(331,$i,'¿Salen de la empresa?',$formulario);
			campoRadioFormulario(332,$i,'¿Tienen datos de caracter personal?',$formulario);
	echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ANEXO SOPORTES</h3>";


    echo"	Para eliminar deja el campo 'Soporte' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaSoportesLOPD'>
                  	<thead>
                  	</thead>
                  	<tbody>";
        $indice=0;

        $soportes = consultaBD("SELECT * FROM soportes_lopd_nueva WHERE codigoCliente=".$cliente,true);
        while($soporte= mysql_fetch_assoc($soportes)){
     		echo "<tr><td>";
       			abreColumnaCampos();
       				campoOculto($soporte['codigo'],'existeSoporte_'.$indice);
   					campoTexto('nombreSoporteLOPD_'.$indice,'Soporte',$soporte['nombreSoporteLOPD']);
					campoTexto('marcaSoporteLOPD_'.$indice,'Marca/Modelo',$soporte['marcaSoporteLOPD']);
					campoTexto('serieSoporteLOPD_'.$indice,'Nº Serie',$soporte['serieSoporteLOPD']);
					campoFecha('fechaSoporteLOPD_'.$indice,'Fecha de Adquisición',$soporte['fechaSoporteLOPD']);
					campoTexto('ubicacionSoporteLOPD_'.$indice,'Ubicación',$soporte['ubicacionSoporteLOPD']);
					campoTexto('sistemaSoporteLOPD_'.$indice,'Sistema operativo',$soporte['sistemaSoporteLOPD']);
					campoTexto('aplicacionesSoporteLOPD_'.$indice,'Aplicaciones instaladas',$soporte['aplicacionesSoporteLOPD']);
					campoTexto('conexionesSoporteLOPD_'.$indice,'Conexiones remotas',$soporte['conexionesSoporteLOPD']);
					campoTexto('responsableSoporteLOPD_'.$indice,'Responsable',$soporte['responsableSoporteLOPD']);
					campoTexto('otrosSoporteLOPD_'.$indice,'Otros usuarios',$soporte['otrosSoporteLOPD']);
				cierraColumnaCampos();
				abreColumnaCampos();
					campoTexto('contenidoSoporteLOPD_'.$indice,'Contenido',$soporte['contenidoSoporteLOPD']);	
					areaTexto('mecanismosAccesoSoporteLOPD_'.$indice,'Mecanismos de Acceso al puesto',$soporte['mecanismosAccesoSoporteLOPD']);		
					areaTexto('mecanismosAccesoRecursosSoporteLOPD_'.$indice,'Mecanismos de Acceso a los recursos',$soporte['mecanismosAccesoRecursosSoporteLOPD']);	
				cierraColumnaCampos();
    		echo "</td></tr>";
    		$indice++;
        }

        $maximo=$indice+2;
    	for($indice;$indice<$maximo;$indice++){
    		echo "<tr><td>";
       			abreColumnaCampos();
       				campoOculto('','existeSoporte_'.$indice);
   					campoTexto('nombreSoporteLOPD_'.$indice,'Soporte');
					campoTexto('marcaSoporteLOPD_'.$indice,'Marca/Modelo');
					campoTexto('serieSoporteLOPD_'.$indice,'Nº Serie');
					campoFecha('fechaSoporteLOPD_'.$indice,'Fecha de Adquisición');
					campoTexto('ubicacionSoporteLOPD_'.$indice,'Ubicación');
					campoTexto('sistemaSoporteLOPD_'.$indice,'Sistema operativo');
					campoTexto('aplicacionesSoporteLOPD_'.$indice,'Aplicaciones instaladas');
					campoTexto('conexionesSoporteLOPD_'.$indice,'Conexiones remotas');
					campoTexto('responsableSoporteLOPD_'.$indice,'Responsable');
					campoTexto('otrosSoporteLOPD_'.$indice,'Otros usuarios');
				cierraColumnaCampos();
				abreColumnaCampos();
					campoTexto('contenidoSoporteLOPD_'.$indice,'Contenido');	
					areaTexto('mecanismosAccesoSoporteLOPD_'.$indice,'Mecanismos de Acceso al puesto');		
					areaTexto('mecanismosAccesoRecursosSoporteLOPD_'.$indice,'Mecanismos de Acceso a los recursos');	
				cierraColumnaCampos();
    		echo "</td></tr>";
    	}
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaSoportesLOPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo"	<br/>
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaAutomatizadosLOPD'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th colspan='5' class='centro'> Usuarios que tienen asignados soportes </th>
                    	</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre y apellidos </th>
		                    <th> Cargo </th>
							<th> Accesos autorizados </th>
							<th> Ref. soporte </th>
							<th> Soportes </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    	$j=0;
		if($trabajo){
			$soportes=consultaBD('SELECT * FROM soportes_automatizados_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
			while($soporte=mysql_fetch_assoc($soportes)){
    			echo "<tr>";
					campoSelect('nombreAutomatizadosLOPD_'.$j,'',$nombresAsignados,$codigosAsignados,strtolower($soporte['nombreAutomatizados']),'selectpicker span3 show-tick',"data-live-search='true'",1);
					campoTextoTabla('cargoAutomatizadosLOPD_'.$j,$soporte['cargoAutomatizados']);
					campoTextoTabla('accesosAutomatizadosLOPD_'.$j,$soporte['accesosAutomatizados']);
					campoTextoTabla('nifAutomatizadosLOPD_'.$j,$soporte['nifAutomatizados'],'input-small');
					campoSelectConsultaMultiple('soportesAutomatizadosLOPD_'.$j,'',array('Ordenador sobremesa','Ordenador portatil','Móvil','Disco duro externo','Pendrive'),array('SOBREMESA','PORTATIL','MOVIL','DISCO','PEN'),$soporte['soportesAutomatizados'],'selectpicker span3 show-tick',"data-live-search='true'",1,$cliente);
				echo"
					</tr>";
				$j++;	
    		}
    	}
    	$maximo=$j+2;
    	for($j;$j<$maximo;$j++){
    		echo "<tr>";
				campoSelect('nombreAutomatizadosLOPD_'.$j,'',$nombresAsignados,$codigosAsignados,'','selectpicker span3 show-tick',"data-live-search='true'",1);
				campoTextoTabla('cargoAutomatizadosLOPD_'.$j);
				campoTextoTabla('accesosAutomatizadosLOPD_'.$j);
				campoTextoTabla('nifAutomatizadosLOPD_'.$j,'','input-small');
				campoSelectConsultaMultiple('soportesAutomatizadosLOPD_'.$j,'',array('Ordenador sobremesa','Ordenador portatil','Móvil','Disco duro externo','Pendrive'),array('SOBREMESA','PORTATIL','MOVIL','DISCO','PEN'),'','selectpicker span3 show-tick',"data-live-search='true'",1,$cliente);
			echo"
				</tr>";
    	}	
    	echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFilaUsuariosLOPD(\"tablaAutomatizadosLOPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 ";
	

	for($indice=333;$indice<393;$indice++){
		campoOculto('','pregunta'.$i.$indice);
	}
	echo '<div class="hide">';
	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Aplicación' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaAplicacionesLOPD'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th colspan='3' class='centro'> Especificar las aplicaciones que traten datos de carácter personal. </th>
                    	</tr>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
							<th> Aplicación </th>
							<th> Finalidad </th>
							<th> Empleados </th>
						</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$aplicaciones = consultaBD("SELECT * FROM aplicaciones_lopd WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($aplicacion= mysql_fetch_assoc($aplicaciones)){
        		echo "<tr>";
    				campoTextoTabla('nombreAplicacionLOPD'.$indice,$aplicacion['nombreAplicacionLOPD']);
    				campoTextoTabla('finalidadAplicacionLOPD'.$indice,$aplicacion['finalidadAplicacionLOPD']);
    				campoTextoTabla('empleadosAplicacionLOPD'.$indice,$aplicacion['empleadosAplicacionLOPD']);
				echo"
					</tr>";
        		$indice++;
        	}
        }
        $maximo=$indice+2;
    	for($indice;$indice<$maximo;$indice++){
    		echo "<tr>";
    			campoTextoTabla('nombreAplicacionLOPD'.$indice);
    			campoTextoTabla('finalidadAplicacionLOPD'.$indice);
    			campoTextoTabla('empleadosAplicacionLOPD'.$indice);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaAplicacionesLOPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	abreColumnaCampos();
		campoRadioFormulario(393,$i,'¿Disponen de un servidor?',$formulario);
		echo "<div id='div".$i."393' class='hidden'>";
			campoTextoFormulario(394,$i,'Ubicación',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(395,$i,'¿Se realizan copias de seguridad?',$formulario);
		echo "<div id='div".$i."395' class='hidden'>";
			campoRadioFormulario(396,$i,'¿Con que frecuencia?',$formulario,'DIARIA',array('Diaria','Semanal','Mensual'),array('DIARIA','SEMANAL','MENSUAL'));
			campoRadioFormulario(397,$i,'¿Dónde se guardan las copias de seguridad?',$formulario,'CD',array('CD/DVD','Disco duro','Disco duro externo','Otros'),array('CD','HD','HDE','OTROS'));
			campoTextoFormulario(398,$i,'Estos dispositivos ¿dónde se almacenan?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> CESIONES DE DATOS</h3>";

		campoRadioFormulario(399,$i,'¿La empresa cede datos de carácter personal (ya sea de clientes, proveedores, empleados) a  otras personas físicas, jurídicas sin recibir a cambio ninguna prestación de servicios? Por ejemplo: que venda una base de datos, que envíe un CV a otra empresa',$formulario);
	echo "<div id='div".$i."399' class='hidden'>";
	echo"	
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCesionesLOPD'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Tipo de datos que cede </th>
		                    <th> A quién se lo cede </th>
							<th> Objeto social </th>
							<th> Finalidad </th>
							<th> Fichero en el que se incluye los datos cedidos </th>
							<th> Fecha de la cesión </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    	$j=0;
    	if($trabajo){
    			$cesiones=consultaBD('SELECT * FROM cesiones_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
    			while($cesion=mysql_fetch_assoc($cesiones)){
    				echo "<tr>";
						campoTextoTabla('tipoCesionesLOPD_'.$j,$cesion['tipoCesiones']);
						campoTextoTabla('quienCesionesLOPD_'.$j,$cesion['quienCesiones'],'input-small');
						campoTextoTabla('objetoCesionesLOPD_'.$j,$cesion['objetoCesiones'],'input-small');
						campoTextoTabla('finalidadCesionesLOPD_'.$j,$cesion['finalidadCesiones'],'input-small');
						campoSelectConsultaMultipleFicheros('ficheroCesionesLOPD_'.$j,'',$arrayFichero,$arrayFichero,$cesion['ficheroCesiones'],'selectpicker span3 show-tick multipleFicheros',"data-live-search='true'",1,$cliente);
						campoFechaTabla('fechaCesionesLOPD_'.$j,$cesion['fechaCesiones']);
					echo"
					</tr>";
					$j++;
				}
    	}
    	$maximo=$j+2;
    	for($j;$j<$maximo;$j++){
    		echo "<tr>";
				campoTextoTabla('tipoCesionesLOPD_'.$j);
				campoTextoTabla('quienCesionesLOPD_'.$j,'','input-small');
				campoTextoTabla('objetoCesionesLOPD_'.$j,'','input-small');
				campoTextoTabla('finalidadCesionesLOPD_'.$j,'','input-small');
				campoSelectConsultaMultipleFicheros('ficheroCesionesLOPD_'.$j,'',$arrayFichero,$arrayFichero,false,'selectpicker span3 show-tick multipleFicheros',"data-live-search='true'",1,$cliente);
				campoFechaTabla('fechaCesionesLOPD_'.$j);
			echo"</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCesionesLOPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";
	for($indice=400;$indice<424;$indice++){
		campoOculto('','pregunta'.$i.$indice);
	}
	areaTextoFormulario(424,$i,'Observaciones',$formulario,'areaInforme');
	echo "</div>";
	echo '</div>';

	echo "<br clear='all'><h3 class='apartadoFormulario'> DELEGACIONES DE FUNCIONES</h3>";

	campoRadioFormulario(425,$i,'¿Tiene delegaciones?',$formulario);
	echo "<div id='div".$i."425' class='hidden'>";
	echo"	
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaDelegacionesLOPD'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
                    		<th> Facultades delegadas </th>
		                    <th> Usuario que delega </th>
		                    <th> Usuario en quien delega </th>
							<th> Fecha inicio delegación </th>
							<th> Fecha fin delegación </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    	$j=0;
    	$facultades=array('','Actualizar y difundir los registros y documentos preceptivos para el cumplimiento de la normativa en vigor en materia de Protección de Datos de las Personas Físicas.','Ejercer el deber de información','Solicitar peticiones de consentimiento','Difundir entre el personal las normas referentes a Protección de Datos de Carácter Personal que les afectan, así como las consecuencias por su incumplimiento.','Concesión de permisos de acceso a datos  al personal propio','Asignar las claves de acceso a cada usuario','Revisar que se cambien las claves de usuario y contraseñas anualmente','Contratar los servicios de los encargados del tratamiento y firmar el correspondiente contrato de encargado del tratamiento','Autorizar la ejecución de trabajos fuera de los locales del responsable o del encargado del tratamiento','Realizar una auditoría cada dos años o ante modificaciones sustanciales en los sistemas de información con repercusiones en seguridad o encargarla a un tercero, propio o ajeno','Autorizar  las entradas y salidas de soportes y datos, incluidas las que se realizan a través de redes de telecomunicaciones','Registrar las entradas y salidas de soportes y datos','Registrar las entradas y salidas de datos por red','Revisar semestralmente los procedimientos de generación de copias de respaldo y recuperación de datos','Analizar trimestralmente las incidencias y poner medidas correctoras','Registrar las incidencias que se produzcan (tipo, momento de su detección, persona que la notifica, efectos y medidas correctoras adoptadas) y notificarlas a quién corresponda','Autorizar la ejecución del procedimiento de recuperación de datos','Registrar los procedimientos de recuperación, persona que lo ejecuta, datos restaurados y, en su caso, datos grabados manualmente','Designar  Responsables de Seguridad','Designar al Delegado de Protección de Datos','Delegar responsabilidades propias de su cargo así como cualquiera de las funciones y obligaciones que le correspondan por  delegación en otros miembros de la plantilla','Otras');
    	$valores=array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22);
    	if($trabajo){
    			$delegaciones=consultaBD('SELECT * FROM delegaciones_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
    			while($delegacion=mysql_fetch_assoc($delegaciones)){
    				echo "<tr>";
    					campoOculto($delegacion['codigo'],'codigoExisteDelegacionesLOPD_'.$j);
    					campoSelectDelegaciones('facultadesDelegacionesLOPD_'.$j,'',$facultades,$valores,$delegacion['facultades'],$delegacion['otras'],'selectpicker span3 show-tick','data-live-search="true"',1);
    					campoSelectConsulta('usuario1DelegacionesLOPD_'.$j,'','SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$trabajo['codigo'],$delegacion['usuario1Delegaciones'],'selectpicker span3 show-tick',"data-live-search='true'",'',1);
    					campoSelectConsulta('usuario2DelegacionesLOPD_'.$j,'','SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$trabajo['codigo'],$delegacion['usuario2Delegaciones'],'selectpicker span3 show-tick',"data-live-search='true'",'',1);
						campoFechaTabla('fechaDelegacionesLOPD_'.$j,$delegacion['fechaDelegaciones']);
						campoFechaTabla('fechaFinDelegacionesLOPD_'.$j,$delegacion['fechaFinDelegaciones']);
					echo"
					</tr>";
					$j++;
				}
    	}
    	$maximo=$j+2;
    	for($j;$j<$maximo;$j++){
    		echo "<tr>";
    			campoSelectDelegaciones('facultadesDelegacionesLOPD_'.$j,'',$facultades,$valores,false,false,'selectpicker span3 show-tick','data-live-search="true"',1);
				campoSelectConsulta('usuario1DelegacionesLOPD_'.$j,'','SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$trabajo['codigo'],false,'selectpicker span3 show-tick',"data-live-search='true'",'',1);
    			campoSelectConsulta('usuario2DelegacionesLOPD_'.$j,'','SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$trabajo['codigo'],false,'selectpicker span3 show-tick',"data-live-search='true'",'',1);
				campoFechaTabla('fechaDelegacionesLOPD_'.$j);
				campoFechaTabla('fechaFinDelegacionesLOPD_'.$j);
			echo"</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaDelegacionesLOPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br></div>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> GRUPO DE EMPRESAS</h3>";

	campoRadioFormulario(426,$i,'¿La empresa pertenece a un grupo de empresas? ',$formulario);

	echo "<br clear='all'><h3 class='apartadoFormulario'> CONCURREN CON OTRAS EMPRESAS</h3>";

	campoRadioFormulario(427,$i,'¿En las propias instalaciones de la empresa concurren con otras empresas? ',$formulario);
	echo "<div id='div".$i."427' class='hidden'>";
	echo"	
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaConcurrenLOPD'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Empresa </th>
		                    <th> CIF/NIF </th>
		                    <th> Actividad </th>
							<th> Fecha inicio </th>
							<th> Fecha fin </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
    	$j=0;
    	if($trabajo){
    			$concurrencias=consultaBD('SELECT * FROM concurren_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
    			while($concurrencia=mysql_fetch_assoc($concurrencias)){
    				echo "<tr>";
						campoTextoTabla('nombreConcurrenLOPD_'.$j,$concurrencia['nombreConcurren']);
						campoTextoTabla('nifConcurrenLOPD_'.$j,$concurrencia['nifConcurren'],'input-small');
						campoTextoTabla('actividadConcurrenLOPD_'.$j,$concurrencia['actividadConcurren']);
						campoFechaTabla('fechaConcurrenLOPD_'.$j,$concurrencia['fechaConcurren']);
						campoFechaTabla('fechaFinConcurrenLOPD_'.$j,$concurrencia['fechaFinConcurren']);
					echo"
					</tr>";
					$j++;
				}
    	}
    	$maximo=$j+2;
    	for($j;$j<$maximo;$j++){
    		echo "<tr>";
				campoTextoTabla('nombreConcurrenLOPD_'.$j);
				campoTextoTabla('nifConcurrenLOPD_'.$j,'','input-small');
				campoTextoTabla('actividadConcurrenLOPD_'.$j);
				campoFechaTabla('fechaConcurrenLOPD_'.$j);
				campoFechaTabla('fechaFinConcurrenLOPD_'.$j);
			echo"</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaConcurrenLOPD\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br></div>
		 	";

	echo '<div class="hide">';
	echo "<br clear='all'><h3 class='apartadoFormulario'> SISTEMAS DE VIDEOVIGILANCIA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(428,$i,'¿Disponen de camaras de videovigilancia?',$formulario);
		echo "<div id='div".$i."428' class='hidden'>";
			campoTextoFormulario(429,$i,'Cuantas',$formulario);
			campoRadioFormulario(430,$i,'¿Graban imágenes? ',$formulario);
			campoRadioFormulario(431,$i,'¿Emiten / reproducen únicamente en tiempo real?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(432,$i,'¿Se realizan copias de seguridad? ',$formulario);
		echo "<div id='div".$i."432' class='hidden'>";
			campoRadioFormulario(433,$i,'¿Con que frecuencia?',$formulario,'DIARIA',array('Diaria','Semanal','Mensual'),array('DIARIA','SEMANAL','MENSUAL'));
			campoTextoFormulario(434,$i,'¿Qué usuarios tienen acceso a las imágenes?',$formulario);
			campoTextoFormulario(435,$i,'Detallar la finalidad de las cámaras',$formulario);
			areaTextoFormulario(436,$i,'Observaciones',$formulario,'areaInforme');
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> TIPOLOGÍA DE CLIENTES</h3>";

	abreColumnaCampos();
		campoRadioFormulario(437,$i,'¿Tiene clientes particulares (destinatarios finales)? ',$formulario);
	cierraColumnaCampos();

	echo "<div id='div".$i."437' class='hidden'>";
	abreColumnaCampos();
		campoRadioFormulario(438,$i,'¿Tiene clientes empresas? ',$formulario);
		campoRadioFormulario(439,$i,'¿Les pueden dar datos de particulares (destinatarios finales)?',$formulario);
		campoTextoFormulario(440,$i,'Especificar qué datos y finalidad',$formulario);
		areaTextoFormulario(441,$i,'Observaciones',$formulario,'areaInforme');
	cierraColumnaCampos();
	echo "</div>";
	echo "</div>";
	

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(442,$i,'Comentarios de comercial',$formulario,'areaInforme');


	
}

function completarDatosGeneralesPRL($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	$formulario['pregunta3'] = $formulario['pregunta3'] == '' ? $cliente['razonSocial'] : $formulario['pregunta3']; 
	$formulario['pregunta4'] = $formulario['pregunta4'] == '' ? $cliente['domicilio'] : $formulario['pregunta4']; 
	$formulario['pregunta5'] = $formulario['pregunta5'] == '' ? $cliente['localidad'] : $formulario['pregunta5']; 
	$formulario['pregunta6'] = $formulario['pregunta6'] == '' ? $cliente['telefono'] : $formulario['pregunta6']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['email'] : $formulario['pregunta12']; 
	$formulario['pregunta8'] = $formulario['pregunta8'] == '' ? $cliente['administrador'] : $formulario['pregunta8']; 
	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['cif'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['cp'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? convertirMinuscula($cliente['provincia']) : convertirMinuscula($formulario['pregunta11']); 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['fax'] : $formulario['pregunta12']; 
	$formulario['pregunta13'] = $formulario['pregunta13'] == '' ? $cliente['actividad'] : $formulario['pregunta13']; 
	$formulario['pregunta14'] = $formulario['pregunta14'] == '' ? $cliente['nifAdministrador'] : $formulario['pregunta14']; 

	return $formulario;
}

function formularioPRL($i,$cliente,$servicio){
	campoOculto($servicio,'servicio'.$i);
	$trabajo = recogerTrabajo($cliente,$servicio);
	$formulario = recogerFormularioServicios($trabajo);
	$formulario = completarDatosGeneralesPRL($formulario,$cliente);

	abreColumnaCampos();
		campoTextoFormulario(1,$i,'Consultor',$formulario);
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS GENERALES</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,$i,'Razón Social',$formulario);
		campoTextoFormulario(4,$i,'Dirección Social',$formulario);
		campoTextoFormulario(10,$i,'Código postal',$formulario);
		campoTextoFormulario(8,$i,'Representate legal',$formulario);
		campoTextoFormulario(6,$i,'Teléfono',$formulario);
		campoTextoFormulario(7,$i,'Nº de trabajadores',$formulario);
		campoRadioFormulario(16,$i,'¿Hay trabajadores en centros ajenos?',$formulario);
		campoTextoFormulario(67,$i,'Sector',$formulario);
		campoTextoFormulario(68,$i,'Persona de contacto',$formulario);
		
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,$i,'CIF',$formulario);
		campoTextoFormulario(5,$i,'Localidad',$formulario);
		campoTextoFormulario(11,$i,'Provincia',$formulario);
		campoTextoFormulario(14,$i,'NIF',$formulario);
		campoTextoFormulario(12,$i,'Email',$formulario);
		campoTextoFormulario(15,$i,'Nº de centros',$formulario);
		campoTextoFormulario(21,$i,'Nombres de los centros',$formulario);
		echo "<br/>";
		campoTextoFormulario(13,$i,'Actividad empresarial',$formulario);
		campoTextoFormulario(69,$i,'Responsable del Plan de Prevención',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DESCRIPCIÓN DEL CENTRO</h3>";

	abreColumnaCampos();
		campoTextoFormulario(17,$i,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(19,$i,'Nº de plantas',$formulario);
		
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(18,$i,'Superficie m2',$formulario);
		echo "<br/>";
		campoTextoFormulario(20,$i,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(22,$i,'¿Mismo nivel?',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre y apellidos' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaEmpleadosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='6'>Trabajadores</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre y apellidos </th>
							<th> DNI </th>
							<th> Puesto de trabajo </th>
							<th> Descripción funciones </th>
							<th> ¿Salen fuera del centro? </th>
							<th> En caso afirmativo ¿Tipo de vehículo? </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$empleados = consultaBD("SELECT * FROM empleados_prl WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($empleado = mysql_fetch_assoc($empleados)){
        		echo "<tr>";
    				campoTextoTabla('nombreEmpleadoPRL'.$indice,$empleado['nombre']);
    				campoTextoTabla('dniEmpleadoPRL'.$indice,$empleado['dni'],'input-small');
    				campoTextoTabla('puestoEmpleadoPRL'.$indice,$empleado['puesto'],'input-small');
    				areaTextoTabla('funcionesEmpleadoPRL'.$indice,$empleado['funciones']);
    				campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),$empleado['salen'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('vehiculoEmpleadoPRL'.$indice,$empleado['vehiculo'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
        $maximo=$indice+2;
    	for($indice;$indice<$maximo;$indice++){
    		echo "<tr>";
    			campoTextoTabla('nombreEmpleadoPRL'.$indice);
    			campoTextoTabla('dniEmpleadoPRL'.$indice,'','input-small');
    			campoTextoTabla('puestoEmpleadoPRL'.$indice,'','input-small');
    			areaTextoTabla('funcionesEmpleadoPRL'.$indice);
    			campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('vehiculoEmpleadoPRL'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmpleadosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Centro de trabajo' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCentrosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='5'>Centros de trabajo</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Centro de trabajo </th>
							<th> Nº delegados<br/>de prevención </th>
							<th> Comite de<br/>seguridad y salud </th>
							<th> Trabajador designado </th>
							<th> Consulta directa <br/>a los trabajadores </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$centros = consultaBD("SELECT * FROM centros_prl WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($centro = mysql_fetch_assoc($centros)){
        		echo "<tr>";
    				campoTextoTabla('nombreCentroPRL'.$indice,$centro['nombre']);
    				campoTextoTabla('delegadoCentroPRL'.$indice,$centro['delegado'],'input-small');
    				campoTextoTabla('comiteCentroPRL'.$indice,$centro['comite']);
    				campoTextoTabla('trabajadorCentroPRL'.$indice,$centro['trabajador']);
    				campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),$centro['consulta'],'selectpicker span2 show-tick','data-live-search="true"',1);
				echo"
					</tr>";
        		$indice++;
        	}
        }

    		echo "<tr>";
    			campoTextoTabla('nombreCentroPRL'.$indice);
    			campoTextoTabla('delegadoCentroPRL'.$indice,'','input-small');
    			campoTextoTabla('comiteCentroPRL'.$indice);
    			campoTextoTabla('trabajadorCentroPRL'.$indice);
    			campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
			echo"
				</tr>";
    
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCentrosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> INSTALACIONES</h3>";
	abreColumnaCampos();
		campoRadioFormulario(23,$i,'Dispone de instalación de acondicionamiento de aire (calefacción, aire)',$formulario);
		campoRadioFormulario(32,$i,'¿Dispone de cuadro electríco',$formulario);
		echo "<div id='div".$i."32' class='hidden'>";
			campoRadioFormulario(33,$i,'Está señalizado',$formulario);
			campoTextoFormulario(34,$i,'Ubicación',$formulario);
		echo "</div>";
		campoRadioFormulario(25,$i,'Dispone de instalación de extracción de humos (campana extractora)',$formulario);


	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<div id='div".$i."23' class='hidden'>";
		campoRadioFormulario(24,$i,'¿Cuales?',$formulario,'CALEFACCION',array('Calefaccion','Aire'),array('CALEFACCION','AIRE'));
		echo "</div>";
		echo "<br/><br/>";
		campoRadioFormulario(26,$i,'Dispone de Montacargas/ascensores',$formulario);
		echo "<div id='div".$i."26' class='hidden'>";
			campoTextoFormulario(27,$i,'¿Cuantos?',$formulario);
			campoTextoFormulario(28,$i,'Empresa de mantenimiento',$formulario);
		echo "</div>";
		echo "Dispones de otras:";
		campoRadioFormulario(29,$i,'Aire',$formulario);
		campoRadioFormulario(30,$i,'Gas',$formulario);
		campoRadioFormulario(31,$i,'Frigos',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'><h3> Maquinaria en la empresa</h3><br/>";
	abreColumnaCampos('span4');
		campoRadioFormulario(75,$i,'Transpaleta manual',$formulario);
		campoRadioFormulario(76,$i,'Carretilla elevadora',$formulario);
		campoRadioFormulario(77,$i,'Grúa puente',$formulario);
		campoRadioFormulario(78,$i,'Escalera de mano',$formulario);
		campoRadioFormulario(79,$i,'Hostelería (cortadora fiambres, parrillas, hornos, etc.)',$formulario);
		campoRadioFormulario(80,$i,'Productos químicos',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(81,$i,'Gatos eléctricos',$formulario);
		campoRadioFormulario(82,$i,'Elevadores',$formulario);
		campoRadioFormulario(83,$i,'Taladros',$formulario);
		campoRadioFormulario(84,$i,'Herramiento de mano',$formulario);
		campoRadioFormulario(85,$i,'Otros',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ORGANIZACIÓN EMPRESA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(32,$i,'Dirección',$formulario,'INTERMEDIO',array('Mando intermedio','Trabajadores'),array('INTERMEDIO','TRABAJADORES'),true);
		campoRadioFormulario(35,$i,'Qué medio de comunicación se utiliza para la prevención',$formulario,'ORAL',array('Oral','Correo electrónico','Carta'),array('ORAL','CORREO','CARTA'),true);
		campoRadioFormulario(36,$i,'Qué recursos dispone para esta actividad:',$formulario,'OFICINA',array('Oficina','Equipos informáticos','Impresora','Otros'),array('OFICINA','EQUIPOS','IMPRESORA','OTROS'),true);
		campoTextoFormulario(71,$i,'Trabajador responsable de la seguridad',$formulario);
		campoTextoFormulario(72,$i,'Trabajador responsable de la ergonomía y psicología',$formulario);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(33,$i,'Tipo de prevención',$formulario,'EMPRESARIO',array('Empresario','Trabajador designado'),array('EMPRESARIO','TRABAJADOR'),true);
		campoRadioFormulario(66,$i,'Formación del responsable de prevención',$formulario,'BASICO',array('Básico','Medio','Superior'),array('BASICO','MEDIO','SUPERIOR'),true);
		campoRadioFormulario(70,$i,'Servicio de prevencion:',$formulario,'PROPIO',array('Propio','Mancomunado','Ajeno','Sistema Mixto'),array('PROPIO','MANCOMUNADO','AJENO','MIXTO'),true);
		campoTextoFormulario(73,$i,'Trabajador responsable de la higiene',$formulario);
		campoTextoFormulario(74,$i,'Trabajador responsable de la vigilancia de la salud',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> MEDIDAS DE EMERGENCIA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(37,$i,'¿Alguien en la empresa dispone de formación en primeros auxilios?',$formulario);
		echo "<div id='div".$i."37' class='hidden'>";
			campoTextoFormulario(86,$i,'Nombre',$formulario);
			campoTextoFormulario(87,$i,'Apellidos',$formulario);
			campoTextoFormulario(88,$i,'DNI',$formulario);
		echo "</div>";
		echo "<br/>";
		campoTextoFormulario(38,$i,'Instalación de seguridad contraincendios perteneciente al edificio donde se ubica el local o la oficina',$formulario);
		campoTextoFormulario(41,$i,'Nº de Bies',$formulario);
		echo "<br/>";
		campoRadioFormulario(43,$i,'Sistema de detección automática de incendios',$formulario);
		campoRadioFormulario(45,$i,'Extintores',$formulario);
		echo "<div id='div".$i."45' class='hidden'>";
			echo "Tipos y carácteristicas:";
			campoRadioFormulario(47,$i,'ABC',$formulario);
			campoRadioFormulario(48,$i,'Espuma',$formulario);
			campoRadioFormularioConLogo(51,$i,'Señal de seguridad de cada extintor',$formulario,'iconExtintor.jpg');
		echo "</div>";
		campoRadioFormulario(53,$i,'En todo caso existirá el alumbrado de emergencia en las puertas de salida',$formulario);
		campoRadioFormulario(55,$i,'Sistema de alarmas',$formulario,'AUTOMATICA',array('Alarma automática de incendios','Megafonía/telefonía','Alarma manual de incendios (con pulsador de alarma)','No dispone de sistema de alarmas'),array('AUTOMATICA','MEGAFONIA','MANUAL','NO'),true);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(39,$i,'¿Alguien en la empresa dispone de formación en extinción de incendios?',$formulario);
		echo "<div id='div".$i."39' class='hidden'>";
			campoTextoFormulario(89,$i,'Nombre',$formulario);
			campoTextoFormulario(90,$i,'Apellidos',$formulario);
			campoTextoFormulario(91,$i,'DNI',$formulario);
		echo "</div>";
		campoTextoFormulario(40,$i,'Instalación de seguridad contraincendios formada por red de agua y Bies (Bocas de incendio equipadas)',$formulario);
		campoRadioFormularioConLogo(42,$i,'Señal de Seguridad de cada BIE',$formulario,'iconBie.png');
		campoRadioFormulario(44,$i,'Sistema de actuación automática de incendios (sprinkkles)',$formulario);
		echo "<div id='div".$i."45_1' class='hidden'>";
			campoTextoFormulario(46,$i,'Nº de Extintores',$formulario);
			echo "<br/>";
			campoRadioFormulario(49,$i,'CO2',$formulario);
			campoRadioFormulario(50,$i,'Otros',$formulario);
		echo "</div>";
		echo "<br/>";
		campoRadioFormulario(52,$i,'Alumbrado de emergencia',$formulario);
		campoRadioFormulario(54,$i,'Nº de lámparas alumbrado de emergencia',$formulario);
		echo "<div id='div".$i."26_1' class='hidden'>";
			campoRadioFormularioConLogo(56,$i,'Si dispone de ascensores/montacargas deberá contar con la preceptiva señal de seguridad',$formulario,'iconAscensor.png');
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE EVACUACIÓN</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(57,$i,'Señal de salvamento. Recorrido de evacuación horizontal',$formulario,'iconHorizontal.jpg');
		campoRadioFormularioConLogo(58,$i,'Señal de salvamento. Recorrido de evacuación vertica',$formulario,'iconVertical.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(59,$i,'Señal de salvamento. Recorrido de evacuación',$formulario,'iconDireccion.png');
		campoRadioFormularioConLogo(60,$i,'Señal de salvamento. Teléfono de salvamento',$formulario,'iconTelefono.png');
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE PRIMEROS AUXILIOS</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(61,$i,'Botiquín. En todo caso existirá en el centro de trabajo un botiquín de primeros auxilios junto con la señalización informativa correspondiente',$formulario,'iconBotiquin.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(62,$i,'Sistema lavaojos. Señal informativa',$formulario,'iconLavaojos.png');
		campoRadioFormulario(63,$i,'Otros',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> VIGILANCIA DE SALUD</h3>";
	abreColumnaCampos();
		campoRadioFormulario(64,$i,'¿Dispone usted de un servicio de Vigilancia de la Salud (reconocimientos médicos)?',$formulario);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormulario(65,$i,'¿En caso negativo, querría que le pusiéramos en contacto con un colaborador nuestro?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> PUNTO DE ENCUENTRO</h3>";

	abreColumnaCampos();
		areaTextoFormulario(92,$i,'En caso de evacuación, indicar punto de encuentro/reunión para los trabajadores',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(93,$i,'Comentarios de comercial',$formulario,'areaInforme');
}

function formularioLOPDComunidad($i,$cliente,$servicio){
	campoOculto($servicio,'servicio'.$i);
	$trabajo = recogerTrabajo($cliente,$servicio);
	$formulario = recogerFormularioServicios($trabajo);
	$formulario = completarDatosGeneralesLOPDComunidad($formulario,$cliente);

	abreColumnaCampos();
		campoTextoFormulario(1,$i,'Consultor',$formulario);
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS DEL ADMINISTRADOR DE FINCAS</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,$i,'Razón Social del administrador',$formulario);
		campoTextoFormulario(4,$i,'Dirección Social',$formulario);
		campoTextoFormulario(5,$i,'Población',$formulario);
		campoTextoFormulario(6,$i,'Teléfono',$formulario);
		campoTextoFormulario(12,$i,'Email',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,$i,'NIF / CIF del administrador',$formulario);
		campoTextoFormulario(10,$i,'Código postal',$formulario);
		campoTextoFormulario(11,$i,'Provincia',$formulario);
		campoTextoFormulario(18,$i,'Fax',$formulario);
		campoTextoFormulario(14,$i,'Total ficheros declarados',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS DE LA COMUNIDAD DE PROPIETARIOS</h3>";

	abreColumnaCampos('span10');
		campoTextoFormulario(7,$i,'Razón Social de la comunidad',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(8,$i,'Dirección Social',$formulario);
		campoTextoFormulario(15,$i,'Población',$formulario);
		campoTextoFormulario(17,$i,'Nombre y apellidos del presidente',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(13,$i,'Código postal',$formulario);
		campoTextoFormulario(16,$i,'Provincia',$formulario);
		campoTextoFormulario(19,$i,'NIF',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		campoTextoFormulario(20,$i,'Teléfono',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS DE LOS DE EMPLEADOS</h3>";

	abreColumnaCampos();
		campoTextoFormulario(21,$i,'Nº de empleados',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(22,$i,'¿Tienen los empleados acceso a datos sensibles como dni, datos bancarios, etc.?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "En caso afirmativo indicar los datos de cada uno de ellos";
	cierraColumnaCampos();
	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaEmpleadosLOPDComunidad'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='4'>Empleados</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre </th>
							<th> Apellidos </th>
							<th> NIF </th>
							<th> Cargo </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$empleados = consultaBD("SELECT * FROM empleados_lopd_comunidad WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($empleado = mysql_fetch_assoc($empleados)){
        		echo "<tr>";
    				campoTextoTabla('nombreEmpleadoLOPDComunidad'.$indice,$empleado['nombre']);
    				campoTextoTabla('apellidosEmpleadoLOPDComunidad'.$indice,$empleado['apellidos']);
    				campoTextoTabla('nifEmpleadoLOPDComunidad'.$indice,$empleado['nif'],'input-small');
    				campoTextoTabla('cargoEmpleadoLOPDComunidad'.$indice,$empleado['cargo']);
				echo"
					</tr>";
        		$indice++;
        	}
        }
        $maximo=$indice+2;
    	for($indice;$indice<$maximo;$indice++){
    		echo "<tr>";
    			campoTextoTabla('nombreEmpleadoLOPDComunidad'.$indice);
    			campoTextoTabla('apellidosEmpleadoLOPDComunidad'.$indice);
    			campoTextoTabla('nifEmpleadoLOPDComunidad'.$indice,'','input-small');
    			campoTextoTabla('cargoEmpleadoLOPDComunidad'.$indice);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmpleadosLOPDComunidad\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> SISTEMAS DE VIDEOVIGILANCIA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(23,$i,'¿Disponen de cámaras de videovigilancia?',$formulario);
		echo "<div id='div".$i."23' class='hidden'>";
			campoRadioFormulario(24,$i,'¿Graban imágenes?',$formulario);
			echo "<div id='div".$i."26' class='hidden'>";
				campoTextoFormulario(27,$i,'Denominación social',$formulario);
				campoTextoFormulario(28,$i,'Dirección social',$formulario);
				campoTextoFormulario(29,$i,'NIF',$formulario);
			echo "</div>";
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<div id='div".$i."23_1' class='hidden'>";
			campoRadioFormulario(25,$i,'Emiten / reproducen únicamente en tiempo real',$formulario);
			echo "<div id='div".$i."24' class='hidden'>";
				campoRadioFormulario(26,$i,'En caso de grabar, ¿tienen contratada alguna empresa de videovigilancia qe pueda acceder a las imágenes?',$formulario);
			echo "</div>";
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> EQUIPAMIENTO INFORMÁTICO</h3>";

	abreColumnaCampos();
		campoRadioFormulario(30,$i,'¿Disponen de algún equipo informático?',$formulario);
		echo "<div id='div".$i."30' class='hidden'>";
			campoTextoFormulario(31,$i,'Nº de ordenadores de sobremesa',$formulario,'input-small');
			campoTextoFormulario(32,$i,'Nº ordenadores portátiles',$formulario,'input-small');
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ACCESO A DATOS</h3>";

	abreColumnaCampos();
		campoRadioFormulario(33,$i,'Además del presidente, ¿hay algún otro comunero que pueda acceder a datos?',$formulario);
		echo "<div id='div".$i."33' class='hidden'>";
			campoTextoFormulario(31,$i,'Nombre y apellidos',$formulario);
			campoTextoFormulario(32,$i,'NIF',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>PRESTADORES DE SERVICIOS CON ACCESO A DATOS DE LA COMUNIDAD</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(33,$i,'Denominación Social',$formulario);
		campoTextoFormulario(34,$i,'NIF',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(35,$i,'Dirección Social',$formulario);
		campoFechaFormulario(36,$i,'Fecha de inicio de contrato',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>TERCEROS QUE PRESTAN SERVICIOS SIN ACCESO A DATOS:</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(37,$i,'Denominación Social',$formulario);
		campoTextoFormulario(38,$i,'NIF',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(39,$i,'Dirección Social',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ACTAS ANUALES</h3>";

	abreColumnaCampos();
		campoRadioFormulario(40,$i,'¿Publican las actas anuales en un lugar visible para todos los vecinos?',$formulario);
		echo "<div id='div".$i."40' class='hidden'>";
			campoRadioFormulario(41,$i,'En caso afirmativo, ¿incluyen datos personales de los vecinos en las mismas?',$formulario);
			campoRadioFormulario(42,$i,'Y, ¿hacen referencia a cuotas pendientes de pago por parte de algún vecino?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(43,$i,'Comentarios de comercial',$formulario,'areaInforme');
}

function recogerDatosPBLAC($datos, $codigoTrabajo){

	$sql = "SELECT 
				c.* 
			FROM 
				clientes c
			INNER JOIN trabajos t ON 
				c.codigo = t.codigoCliente 
			WHERE t.codigo = ".$codigoTrabajo.";";

	$cliente = consultaBD($sql, true, true);
	
	if($datos) {		
		$datos['tomo']    = isset($datos['tomo'])    && $datos['tomo']    == '' ? $cliente['tomo']    : $datos['tomo'];
		$datos['libro']   = isset($datos['libro'])   && $datos['libro']   == '' ? $cliente['libro']   : $datos['libro'];
		$datos['folio']   = isset($datos['folio'])   && $datos['folio']   == '' ? $cliente['folio']   : $datos['folio'];
		$datos['hoja']    = isset($datos['hoja'])    && $datos['hoja']    == '' ? $cliente['hoja']    : $datos['hoja'];
		$datos['seccion'] = isset($datos['seccion']) && $datos['seccion'] == '' ? $cliente['seccion'] : $datos['seccion'];
	}
	else {
		$datos['tomo']    = $cliente['tomo'];
		$datos['libro']   = $cliente['libro'];
		$datos['folio']   = $cliente['folio'];
		$datos['hoja']    = $cliente['hoja'];
		$datos['seccion'] = $cliente['seccion'];
	}

	return $datos;
}

function formularioPBLAC($i,$cliente,$servicio,$trabajo){
	global $_CONFIG;
	campoOculto($servicio,'servicio'.$i);
	//$trabajo = recogerTrabajo($cliente,$servicio);
	$datos = compruebaItem('manuales_pbc',$trabajo['codigo']);
	$datos = recogerDatosPBLAC($datos, $trabajo['codigo']);

	abreColumnaCampos('span10');
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
	campoOculto('manuales_pbc','tabla'.$i);
	campoOculto($trabajo['codigo'],'trabajo'.$i);
	campoFecha('fechaInicio', 'Fecha inicio', $datos);
	campoSelect('sujeto','Sujeto',array('Persona física','Persona jurídica'),array('FISICA','JURIDICA'),$datos);
	campoSelect('actividad','Actividad',array('Abogacía','Asesoramiento fiscal','Asesoramiento fiscal y contable','Contabilidad externa','Auditoría de cuentas','Promoción inmobiliaria','Otra'),array('ABOGACIA','FISCAL','CONTABLE','CONTABILIDAD','AUDITORIA','PROMOCION','OTRA'),$datos);
	campoTexto('otra','Otra actividad',$datos);
	areaTexto('servicios','Servicios habituales',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha('fechaFin','Fecha fin',$datos);
	campoSelect('empleados','Número de empleados',array('Menos de 10 personas','Entre 10 y 49 personas','50 personas o más'),array(10,49,50),$datos);
	campoSelect('volumen','Volumen de negocios anual',array('No supera los 2 millones de euros','Supera los 2 millones de euros sin superar los 10 millones de euros','Supera los 10 millones de euros'),array(1,2,3),$datos);
	campoFichero('ficheroCatalogo','Catálogo',0,$datos,$_CONFIG['raiz'].'consultoria/documentos/','Descargar');
	cierraColumnaCampos();

	echo "<br clear='all'><h1 class='apartadoFormulario'>Registro mercantil</h1>";
	abreColumnaCampos();
	campoTexto('tomo','Tomo',$datos);
	campoTexto('folio','Folio',$datos);
	campoTexto('hoja','Hoja',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
	campoTexto('libro','Libro',$datos);
	campoTexto('seccion','Sección',$datos);
	cierraColumnaCampos();

	echo "<br clear='all'><h1 class='apartadoFormulario'>Representante ante el SEPBLAC</h1>";
	abreColumnaCampos();
		campoTexto('representante','Representante',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('nifRepresentante','NIF',$datos);
	cierraColumnaCampos();

	echo "<br clear='all'><h1 class='apartadoFormulario'>Organo de control interno</h1>";
	creaTablaPersonas($datos);
}

function creaTablaPersonas($datos){
	conexionBD();

	echo "	                 
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaPersonas'>
				  	<thead>
				    	<tr>
				            <th> Personas </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if(isset($datos['codigo'])){
				  			$consulta=consultaBD("SELECT * FROM personas_pbc WHERE codigoManual=".$datos['codigo']);
				  			while($persona=mysql_fetch_assoc($consulta)){
				  				echo '<tr>';
				  				campoTextoTabla('nombre'.$i,$persona['nombre'],'span11');
				  				echo '</tr>';
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr>';
				  			campoTextoTabla('nombre'.$i,'','span11');
				  			echo '</tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaPersonas\");'><i class='icon-plus'></i> Añadir persona</button> 
				</div>
			</div>";

	cierraBD();
}

function compruebaItem($tabla,$codigoTrabajo=false){

	if($codigoTrabajo!=false){//OJO: con $_REQUEST porque puede venir desde el listado ($_GET) o desde el alta
	
		$datos=datosRegistro($tabla,$codigoTrabajo,'codigoTrabajo');
	
		if($datos){
			campoOculto($datos);
			campoOculto($datos,'codigoTrabajo');
		} else {
			campoOculto($codigoTrabajo,'codigoTrabajo');
		}

	}
	else{
		$datos=false;
	}
	return $datos;
}

function formularioAPPCC($i,$cliente,$servicio){
	campoOculto($servicio,'servicio'.$i);
	$trabajo = recogerTrabajo($cliente,$servicio);
	$formulario = recogerFormularioServicios($trabajo);
	$formulario = completarDatosGeneralesAlergenos($formulario,$cliente);

	abreColumnaCampos();
		campoTextoFormulario(1,$i,'Consultor',$formulario);
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS GENERALES</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,$i,'Razón Social',$formulario);
		campoTextoFormulario(4,$i,'Dirección Social',$formulario);
		campoTextoFormulario(10,$i,'Código postal',$formulario);
		campoTextoFormulario(8,$i,'Representate legal',$formulario);
		campoTextoFormulario(6,$i,'Teléfono',$formulario);
		campoTextoFormulario(7,$i,'Nº de trabajadores',$formulario);
		campoTextoFormulario(15,$i,'Nº de centros',$formulario);
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,$i,'CIF',$formulario);
		campoTextoFormulario(5,$i,'Localidad',$formulario);
		campoTextoFormulario(11,$i,'Provincia',$formulario);
		campoTextoFormulario(14,$i,'NIF',$formulario);
		campoTextoFormulario(12,$i,'Email',$formulario);
		campoTextoFormulario(13,$i,'Nombre comercial',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ACTIVIDAD Y ÁMBITO DE COMERCIALIZACIÓN DEL ESTABLECIMIENTO</h3>";

	abreColumnaCampos('span10 centro');
		echo "Restauración";
		echo "<br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos('span5');
		campoRadioFormulario(16,$i,'Bar',$formulario);
		campoRadioFormulario(17,$i,'Cafetería',$formulario);
		campoRadioFormulario(18,$i,'Restaurante',$formulario);
		campoRadioFormulario(19,$i,'Establecimiento de temporada',$formulario);
		campoRadioFormulario(20,$i,'Establecimiento no permanente',$formulario);
		campoRadioFormulario(26,$i,'Hotel o establecimiento de servicios de comidas que tenga capacidad o sirva menos de 200 comidas/día (incluido salón de celebraciones sin cocina propia)',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span5');
		campoRadioFormulario(21,$i,'Asador de pollo',$formulario);
		campoRadioFormulario(22,$i,'Pizzería',$formulario);
		campoRadioFormulario(23,$i,'Hamburguesería',$formulario);
		campoRadioFormulario(24,$i,'Venta de carretera',$formulario);
		campoRadioFormulario(25,$i,'Catering',$formulario);
		campoRadioFormulario(27,$i,'Take away. Tipo de vehículo',$formulario);
		echo "<div id='div".$i."27' class='hidden'>";
			campoTextoFormulario(28,$i,'Especificar',$formulario);
		echo "</div>";
		campoRadioFormulario(29,$i,'Otros',$formulario);
		echo "<div id='div".$i."29' class='hidden'>";
			campoTextoFormulario(30,$i,'Especificar',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'>";
	abreColumnaCampos('span5');
		campoRadioFormulario(31,$i,'En función del producto',$formulario,'ELABORACION',array('Elaboración del producto','Venta de productos','Ambos','Otros'),array('ELABORACION','VENTA','AMBOS','OTROS'),true);
		campoTextoFormulario(32,$i,'Especificar otros',$formulario,'input-small');
	cierraColumnaCampos();

	abreColumnaCampos('span5');
		campoRadioFormulario(33,$i,'En función del ámbito geográfico',$formulario,'LOCAL',array('Local','Provincial','Autonómico','Nacional'),array('LOCAL','PROVINCIAL','AUTONOMICO','NACIONAL'),true);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DESCRIPCIÓN DEL CENTRO (cada uno de los centros)</h3>";

	abreColumnaCampos();
		campoTextoFormulario(34,$i,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(35,$i,'Nº plantas',$formulario);
		campoTextoFormulario(36,$i,'Aforo máximo',$formulario);
		campoRadioFormulario(37,$i,'Tipo de cliente',$formulario,'FINAL',array('Ciente final','A domicilio','Otro establecimiento'),array('FINAL','DOMICILIO','OTRO'));
	cierraColumnaCampos();
	

	abreColumnaCampos();
		campoTextoFormulario(38,$i,'Superficie m²',$formulario);
		campoTextoFormulario(39,$i,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(40,$i,'Mismo nivel',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'><br/>";
	abreColumnaCampos();
		campoTextoFormulario(41,$i,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(42,$i,'Nº plantas',$formulario);
		campoTextoFormulario(43,$i,'Aforo máximo',$formulario);
		campoRadioFormulario(44,$i,'Tipo de cliente',$formulario,'FINAL',array('Ciente final','A domicilio','Otro establecimiento'),array('FINAL','DOMICILIO','OTRO'));
	cierraColumnaCampos();
	

	abreColumnaCampos();
		campoTextoFormulario(45,$i,'Superficie m²',$formulario);
		campoTextoFormulario(46,$i,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(47,$i,'Mismo nivel',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'><br/>";

	abreColumnaCampos();
		campoTextoFormulario(48,$i,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(49,$i,'Nº plantas',$formulario);
		campoTextoFormulario(50,$i,'Aforo máximo',$formulario);
		campoRadioFormulario(51,$i,'Tipo de cliente',$formulario,'FINAL',array('Ciente final','A domicilio','Otro establecimiento'),array('FINAL','DOMICILIO','OTRO'));
	cierraColumnaCampos();
	

	abreColumnaCampos();
		campoTextoFormulario(52,$i,'Superficie m²',$formulario);
		campoTextoFormulario(53,$i,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(54,$i,'Mismo nivel',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'><br/>";

	abreColumnaCampos();
		campoRadioFormulario(55,$i,'¿Dispone de platos o croquis del establecimiento indicando la ubicación de los elementos de frío, equipos, instalaciones, dependencias, zonas de trabajo, elaboración, preparación, servicio, lavado, los movimientos de las materias primas, alimentos preparados, comidas y desperdicios?  ',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "Tomar fotografías de las distintas zonas de trabajo (Zona de almacenamiento, zona de elaboración, zona de servicio, etc.)";
	cierraColumnaCampos();
	echo "<br clear='all'><br/>";

	echo "<br clear='all'><h3 class='apartadoFormulario'> DIAGRAMA DE FLUJO</h3>";

	abreColumnaCampos('span10');
		echo "A continuación se presenta un diagrama de flujo “tipo” de los procesos desarrollados por un restaurante y por una cocina central (servicios de catering, comedores escolares, residencias, etc.). Indicar cuál de los dos se aproxima más al desarrollado por la empresa y marcar sí/no en cada uno de los procesos según proceda.";
		campoRadioFormulario(56,$i,'Tipo de diagrama',$formulario,'RESTAURANTE',array('Restaurante','Cocina central'),array('RESTAURANTE','COCINA CENTRAL'));
	cierraColumnaCampos();

	abreColumnaCampos('span5');
		echo "Diagrama tipo 'Restaurante'<br/>";
		echo "<img src='../img/tipoRestaurante.png' alt='Tipo Restaurante'>";
	cierraColumnaCampos();

	abreColumnaCampos('span5');
		echo "Diagrama tipo 'Cocina central'<br/>";
		echo "<img src='../img/tipoCocina.png' alt='Tipo Cocina central'>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE CONTROL DE TEMPERATURAS</h3>";

	abreColumnaCampos('span10');
		echo "Identificación de los equipos de conservación de temperatura regulada  <br/>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		campoRadioFormulario(183,$i,'Vitrinas de frío',$formulario);
	cierraColumnaCampos();
	echo "<div id='div".$i."183' class='hidden'>";
	abreColumnaCampos('span10');
		campoTextoFormulario(184,$i,'Cuantas',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Marca' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaConservacionFrio'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Marca </th>
							<th> Modelo </th>
							<th> Capacidad </th>
							<th> Rango de temperaturas<br/>admisibles<br/>Tº min / Tº max </th>
							<th> ¿Termometro incorporado? </th>
							<th> Alimentos contenidos </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$conservaciones = consultaBD("SELECT * FROM conservacion_appcc WHERE codigoTrabajo=".$trabajo['codigo']." AND tipo='frio'",true);
        	while($conservacion = mysql_fetch_assoc($conservaciones)){
        		echo "<tr>";
    				campoTextoTabla('marcaConservacionFrio'.$indice,$conservacion['marca'],'input-small');
    				campoTextoTabla('modeloConservacionFrio'.$indice,$conservacion['modelo'],'input-small');
    				campoTextoTabla('capacidadConservacionFrio'.$indice,$conservacion['capacidad'],'input-small');
    				campoTextoTabla('rangoConservacionFrio'.$indice,$conservacion['rango'],'input-small');
    				campoSelect('termometroConservacionFrio'.$indice,'',array('Si','No'),array('SI','NO'),$conservacion['termometro'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('alimentosConservacionFrio'.$indice,$conservacion['alimentos'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<2;$indice++){
    		echo "<tr>";
    			campoTextoTabla('marcaConservacionFrio'.$indice,'','input-small');
    			campoTextoTabla('modeloConservacionFrio'.$indice,'','input-small');
    			campoTextoTabla('capacidadConservacionFrio'.$indice,'','input-small');
    			campoTextoTabla('rangoConservacionFrio'.$indice,'','input-small');
    			campoSelect('termometroConservacionFrio'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('alimentosConservacionFrio'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaConservacionFrio\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";
	echo "</div>";

	abreColumnaCampos('span10');
		campoRadioFormulario(185,$i,'Vitrinas de congelado',$formulario);
	cierraColumnaCampos();
	echo "<div id='div".$i."185' class='hidden'>";
	abreColumnaCampos('span10');
		campoTextoFormulario(186,$i,'Cuantas',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Marca' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaConservacionCongelado'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Marca </th>
							<th> Modelo </th>
							<th> Capacidad </th>
							<th> Rango de temperaturas<br/>admisibles<br/>Tº min / Tº max </th>
							<th> ¿Termometro incorporado? </th>
							<th> Alimentos contenidos </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$conservaciones = consultaBD("SELECT * FROM conservacion_appcc WHERE codigoTrabajo=".$trabajo['codigo']." AND tipo='congelado'",true);
        	while($conservacion = mysql_fetch_assoc($conservaciones)){
        		echo "<tr>";
    				campoTextoTabla('marcaConservacionCongelado'.$indice,$conservacion['marca'],'input-small');
    				campoTextoTabla('modeloConservacionCongelado'.$indice,$conservacion['modelo'],'input-small');
    				campoTextoTabla('capacidadConservacionCongelado'.$indice,$conservacion['capacidad'],'input-small');
    				campoTextoTabla('rangoConservacionCongelado'.$indice,$conservacion['rango'],'input-small');
    				campoSelect('termometroConservacionCongelado'.$indice,'',array('Si','No'),array('SI','NO'),$conservacion['termometro'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('alimentosConservacionCongelado'.$indice,$conservacion['alimentos'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<2;$indice++){
    		echo "<tr>";
    			campoTextoTabla('marcaConservacionCongelado'.$indice,'','input-small');
    			campoTextoTabla('modeloConservacionCongelado'.$indice,'','input-small');
    			campoTextoTabla('capacidadConservacionCongelado'.$indice,'','input-small');
    			campoTextoTabla('rangoConservacionCongelado'.$indice,'','input-small');
    			campoSelect('termometroConservacionCongelado'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('alimentosConservacionCongelado'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaConservacionCongelado\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";
	echo "</div>";

	abreColumnaCampos('span10');
		campoRadioFormulario(187,$i,'Cámara frigorífica',$formulario);
	cierraColumnaCampos();
	echo "<div id='div".$i."187' class='hidden'>";
	abreColumnaCampos('span10');
		campoTextoFormulario(188,$i,'Cuantas',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Marca' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaConservacionFrigorificas'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Marca </th>
							<th> Modelo </th>
							<th> Capacidad </th>
							<th> Rango de temperaturas<br/>admisibles<br/>Tº min / Tº max </th>
							<th> ¿Termometro incorporado? </th>
							<th> Alimentos contenidos </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$conservaciones = consultaBD("SELECT * FROM conservacion_appcc WHERE codigoTrabajo=".$trabajo['codigo']." AND tipo='frigorificas'",true);
        	while($conservacion = mysql_fetch_assoc($conservaciones)){
        		echo "<tr>";
    				campoTextoTabla('marcaConservacionFrigorificas'.$indice,$conservacion['marca'],'input-small');
    				campoTextoTabla('modeloConservacionFrigorificas'.$indice,$conservacion['modelo'],'input-small');
    				campoTextoTabla('capacidadConservacionFrigorificas'.$indice,$conservacion['capacidad'],'input-small');
    				campoTextoTabla('rangoConservacionFrigorificas'.$indice,$conservacion['rango'],'input-small');
    				campoSelect('termometroConservacionFrigorificas'.$indice,'',array('Si','No'),array('SI','NO'),$conservacion['termometro'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('alimentosConservacionFrigorificas'.$indice,$conservacion['alimentos'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<2;$indice++){
    		echo "<tr>";
    			campoTextoTabla('marcaConservacionFrigorificas'.$indice,'','input-small');
    			campoTextoTabla('modeloConservacionFrigorificas'.$indice,'','input-small');
    			campoTextoTabla('capacidadConservacionFrigorificas'.$indice,'','input-small');
    			campoTextoTabla('rangoConservacionFrigorificas'.$indice,'','input-small');
    			campoSelect('termometroConservacionFrigorificas'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('alimentosConservacionFrigorificas'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaConservacionFrigorificas\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";
	echo "</div>";

	abreColumnaCampos('span10');
		campoRadioFormulario(189,$i,'Cámaras de congelado',$formulario);
	cierraColumnaCampos();
	echo "<div id='div".$i."189' class='hidden'>";
	abreColumnaCampos('span10');
		campoTextoFormulario(190,$i,'Cuantas',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Marca' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaConservacionCongelado2'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Marca </th>
							<th> Modelo </th>
							<th> Capacidad </th>
							<th> Rango de temperaturas<br/>admisibles<br/>Tº min / Tº max </th>
							<th> ¿Termometro incorporado? </th>
							<th> Alimentos contenidos </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$conservaciones = consultaBD("SELECT * FROM conservacion_appcc WHERE codigoTrabajo=".$trabajo['codigo']." AND tipo='congelado2'",true);
        	while($conservacion = mysql_fetch_assoc($conservaciones)){
        		echo "<tr>";
    				campoTextoTabla('marcaConservacionCongelado2'.$indice,$conservacion['marca'],'input-small');
    				campoTextoTabla('modeloConservacionCongelado2'.$indice,$conservacion['modelo'],'input-small');
    				campoTextoTabla('capacidadConservacionCongelado2'.$indice,$conservacion['capacidad'],'input-small');
    				campoTextoTabla('rangoConservacionCongelado2'.$indice,$conservacion['rango'],'input-small');
    				campoSelect('termometroConservacionCongelado2'.$indice,'',array('Si','No'),array('SI','NO'),$conservacion['termometro'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('alimentosConservacionCongelado2'.$indice,$conservacion['alimentos'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<2;$indice++){
    		echo "<tr>";
    			campoTextoTabla('marcaConservacionCongelado2'.$indice,'','input-small');
    			campoTextoTabla('modeloConservacionCongelado2'.$indice,'','input-small');
    			campoTextoTabla('capacidadConservacionCongelado2'.$indice,'','input-small');
    			campoTextoTabla('rangoConservacionCongelado2'.$indice,'','input-small');
    			campoSelect('termometroConservacionCongelado2'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('alimentosConservacionCongelado2'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaConservacionCongelado2\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";
	echo "</div>";

	abreColumnaCampos('span10');
		campoRadioFormulario(191,$i,'Mesas calientes',$formulario);
	cierraColumnaCampos();
	echo "<div id='div".$i."191' class='hidden'>";
	abreColumnaCampos('span10');
		campoTextoFormulario(192,$i,'Cuantas',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Marca' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaConservacionCalientes'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Marca </th>
							<th> Modelo </th>
							<th> Capacidad </th>
							<th> Rango de temperaturas<br/>admisibles<br/>Tº min / Tº max </th>
							<th> ¿Termometro incorporado? </th>
							<th> Alimentos contenidos </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$conservaciones = consultaBD("SELECT * FROM conservacion_appcc WHERE codigoTrabajo=".$trabajo['codigo']." AND tipo='calientes'",true);
        	while($conservacion = mysql_fetch_assoc($conservaciones)){
        		echo "<tr>";
    				campoTextoTabla('marcaConservacionCalientes'.$indice,$conservacion['marca'],'input-small');
    				campoTextoTabla('modeloConservacionCalientes'.$indice,$conservacion['modelo'],'input-small');
    				campoTextoTabla('capacidadConservacionCalientes'.$indice,$conservacion['capacidad'],'input-small');
    				campoTextoTabla('rangoConservacionCalientes'.$indice,$conservacion['rango'],'input-small');
    				campoSelect('termometroConservacionCalientes'.$indice,'',array('Si','No'),array('SI','NO'),$conservacion['termometro'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('alimentosConservacionCalientes'.$indice,$conservacion['alimentos'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<2;$indice++){
    		echo "<tr>";
    			campoTextoTabla('marcaConservacionCalientes'.$indice,'','input-small');
    			campoTextoTabla('modeloConservacionCalientes'.$indice,'','input-small');
    			campoTextoTabla('capacidadConservacionCalientes'.$indice,'','input-small');
    			campoTextoTabla('rangoConservacionCalientes'.$indice,'','input-small');
    			campoSelect('termometroConservacionCalientes'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('alimentosConservacionCalientes'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaConservacionCalientes\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";
	echo "</div>";

	abreColumnaCampos('span10');
		echo "Control de temperatura";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(57,$i,'¿Cocinan alimentos en el establecimiento?  ',$formulario);
		campoTextoFormulario(58,$i,'¿Calientan alimentos que estaban previamente enfriados?',$formulario);
		campoTextoFormulario(59,$i,'¿Presenta platos preparados a base de pescados que van a ser consumidos crudos?',$formulario);
	cierraColumnaCampos();
	

	abreColumnaCampos();
		campoTextoFormulario(61,$i,'¿Elaboran alimentos que van a ser servidos en frío con algún tratamiento térmico?',$formulario);
		campoTextoFormulario(62,$i,'¿Tienen sala de elaboración de fríos?',$formulario);
		campoRadioFormulario(63,$i,'¿Toman y registran las temperaturas de cada uno de los equipos?',$formulario);
		echo "<div id='div".$i."63' class='hidden'>";
			campoTextoFormulario(64,$i,'Periocidad',$formulario);
			campoTextoFormulario(65,$i,'Responsable',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE LIMPIEZA Y DESINFECCIÓN</h3>";

	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Elemento / superficie </th>
							<th> Método </th>
							<th> Producto </th>
							<th> ¿Dispone de ficha de<br/>datos de seguridad? </th>
							<th> Frecuencia </th>
							<th> Responsable </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $elementos = array('Platos, cubiertos, vasos, etc.','Máquinas (picadoras,<br/>batidoras, etc.)','Útiles (cuchillos, cacerolas,<br/>sartenes, etc.)','Superficies de trabajo<br/>(encimeras, tablas de corte,<br/>mesas de trabajo, fogones, etc.)','Despensas','Cámaras de conservación','Instalaciones (suelos, paredes,</br>techos, etc.)','Otros');
        $campo=66;
    	for($j=0;$j<count($elementos);$j++){
    		echo "<tr>";
    			echo "<td>".$elementos[$j]."</td>";
    			campoTextoTablaFormulario($campo++,$i,$formulario,'input-small');
    			campoTextoTablaFormulario($campo++,$i,$formulario,'input-small');
    			campoSelectFormulario($campo++,$i,'',$formulario,1);
    			campoTextoTablaFormulario($campo++,$i,$formulario,'input-small');
    			campoTextoTablaFormulario($campo++,$i,$formulario,'input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
		 	";

	campoTextoFormulario(106,$i,'Especificar lugar/es donde se guardan los productos y útiles de limpieza:',$formulario);

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE CONTROL DE PLAGAS</h3>";

	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th colspan='2'> Actuación en caso de presencia de plagas </th>
                    	</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Zonas críticas </th>
							<th> Medidas preventivas / correctivas aplicadas<br/>(revisión periódica, colocación de cebos, uso de repelentes,  lámparas electrocutoras, pegamentos, etc.)</th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $elementos = array('Motores de cámaras','Bajo los fregaderos','Zona de almacén de productos','Zona de basuras','Otros');
        $campo=107;
    	for($j=0;$j<count($elementos);$j++){
    		echo "<tr>";
    			echo "<td>".$elementos[$j]."</td>";
    			areaTextoTablaFormulario($campo++,$i,$formulario,'areaInforme');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
		 	";

	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th colspan='4'> Registro de tratamientos (Realizados sólo por empresas autorizadas) </th>
                    	</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Tratamiento realizado </th>
							<th> Empresa aplicadora acreditada </th>
							<th> Fecha </th>
							<th> Documentos conservados (registro de<br/>diagnosis, registro de tratamiento, etc.) </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $elementos = array('Desinsectación','Desratización');
    	for($j=0;$j<count($elementos);$j++){
    		echo "<tr>";
    			echo "<td>".$elementos[$j]."</td>";
    			campoTextoTablaFormulario($campo++,$i,$formulario);
    			campoFechaTablaFormulario($campo++,$i,$formulario);
    			campoTextoTablaFormulario($campo++,$i,$formulario);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE FORMACIÓN DE LOS MANIPULADORES</h3>";
	abreColumnaCampos('span10');
		campoTextoFormulario(193,$i,'Responsable de la formación de los manipuladores en la empresa:',$formulario);
	cierraColumnaCampos();
	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre del empleado' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaManipuladores'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Fecha de la<br/>formacion </th>
							<th> Nombre del<br/>empleado </th>
							<th> Puesto de<br/>trabajo </th>
							<th> Tipo de actividad<br/>formativa </th>
							<th> Contenido de<br/>acción formativa </th>
							<th> Responsable de<br/>la formación </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$manipuladores = consultaBD("SELECT * FROM manipuladores_appcc WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($manipulador = mysql_fetch_assoc($manipuladores)){
        		echo "<tr>";
    				campoFechaTabla('fechaManipuladoresAPPCC'.$indice,$manipulador['fecha']);
    				campoTextoTabla('nombreManipuladoresAPPCC'.$indice,$manipulador['nombre'],'input-small');
    				campoTextoTabla('puestoManipuladoresAPPCC'.$indice,$manipulador['puesto'],'input-small');
    				campoTextoTabla('tipoManipuladoresAPPCC'.$indice,$manipulador['tipo'],'input-small');
    				campoTextoTabla('contenidoManipuladoresAPPCC'.$indice,$manipulador['contenido'],'input-small');
    				campoTextoTabla('responsableManipuladoresAPPCC'.$indice,$manipulador['responsable'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<5;$indice++){
    		echo "<tr>";
    			campoFechaTabla('fechaManipuladoresAPPCC'.$indice);
    			campoTextoTabla('nombreManipuladoresAPPCC'.$indice,'','input-small');
    			campoTextoTabla('puestoManipuladoresAPPCC'.$indice,'','input-small');
    			campoTextoTabla('tipoManipuladoresAPPCC'.$indice,'','input-small');
    			campoTextoTabla('contenidoManipuladoresAPPCC'.$indice,'','input-small');
    			campoTextoTabla('responsableManipuladoresAPPCC'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaManipuladores\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE TRAZABILIDAD</h3>";
	abreColumnaCampos('span10');
		campoTextoFormulario(118,$i,'Responsable de la trazabilidad',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "Destino de los productos<br/><br/>";
		campoTextoFormulario(119,$i,'Consumidor final',$formulario);
		campoTextoFormulario(120,$i,'Otro establecimiento de venta',$formulario);
		campoTextoFormulario(121,$i,'¿Registra los destinatarios y productos distribuidos?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "Registro de proveedores<br/><br/>";
		campoTextoFormulario(122,$i,'¿Mantiene un registro de proveedores?',$formulario);
		campoTextoFormulario(123,$i,'¿Conserva los albaranes de entrada de los productos?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE CONTROL DE AGUA</h3>";
	abreColumnaCampos();
		echo "Origen del agua<br/><br/>";
		campoTextoFormulario(124,$i,'Responsable del control del agua',$formulario);
		echo "<div id='procedenciaAgua'>";
			campoRadioFormulario(125,$i,'Procedencia del agua',$formulario,'1',array('Directamente de la red de abastecimiento público','Red de abastecimiento público con deposito intermedio','De un lugar de captación propia (pozo)'),array('1','2','3'),true);
		echo "</div>";
	cierraColumnaCampos();

	echo "<div id='divAgua2' class='hidden'>";
		abreColumnaCampos();
			echo "Si procede de abastecimiento público con deposito intermedio<br/><br/>";
			campoTextoFormulario(126,$i,'Capacidad del deposito (L o m3)',$formulario);
			campoRadioFormulario(127,$i,'El depósito se encuentra ubicado por encima del nivel del alcantarillado',$formulario);
			campoRadioFormulario(128,$i,'El depósito está tapado',$formulario);
			campoRadioFormulario(129,$i,'Dispone de desagüe para garantizar vaciado total y facilitar su desinfección y limpieza',$formulario);
			campoRadioFormulario(130,$i,'Se limpia periódicamente',$formulario);
			campoRadioFormulario(131,$i,'Incluye dosificador automático de cloro como tratamiento de agua',$formulario);
		cierraColumnaCampos();
	echo "</div>";

	echo "<div id='divAgua3' class='hidden'>";
		abreColumnaCampos();
			echo "Si procede de un lugar de captación propia<br/><br/>";
			campoTextoFormulario(132,$i,'Origen del agua',$formulario);
			campoTextoFormulario(133,$i,'Tratamiendo que se aplica al agua para el consumo',$formulario);
			campoRadioFormulario(134,$i,'Dispone de documentación de legalización del pozo y autorización del uso del agua',$formulario);
			campoRadioFormulario(135,$i,'Realiza control analítico del agua',$formulario);
			campoTextoFormulario(136,$i,'Laboratorio',$formulario);
			campoTextoFormulario(137,$i,'Periocidad',$formulario);
			campoFechaFormulario(138,$i,'Fecha del último analisis',$formulario);
			campoRadioFormulario(139,$i,'Mide el cloro residual diariamente',$formulario);
			echo "<div id='div".$i."139' class='hidden'>";
				campoTextoFormulario(140,$i,'Responsable de realizar las mediciones',$formulario);
			echo "</div>";
		cierraColumnaCampos();
	echo "</div>";

	echo "<br clear='all'>";
	abreColumnaCampos();
		echo "Mediciones del cloro residual (sólo si realiza desinfección por cloro)<br/><br/>";
		campoTextoFormulario(141,$i,'Equipo empleado',$formulario);
		campoTextoFormulario(142,$i,'Dónde se toma la muestra',$formulario);
		campoRadioFormulario(143,$i,'¿Se guardan los registros?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE MANTENIMIENTO DE LAS INSTALACIONES Y EQUIPOS</h3>";

	abreColumnaCampos('span10');
		campoTextoFormulario(141,$i,'Responsable del mantenimiento correctivo/preventivo',$formulario);
	cierraColumnaCampos();

	echo"
	 		<center>
	 		<div class='table-responsive'>
				<table class='table'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Equipos e instalaciones sujetos a mantenimiento </th>
							<th> Mantenimiento preventivo</th>
							<th> Mantenimiento correctivo</th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $elementos = array('Vitrinas','Cámaras frigoríficas','Cámaras de congelado','Mesas calientes','Red pública y depósito','Instalación de gas','Instalación de aire comprimido','Instalación electríca','Instalación de aire acondicionado','Otras');
        $campo=142;
    	for($j=0;$j<count($elementos);$j++){
    		echo "<tr>";
    			echo "<td>".$elementos[$j]."</td>";
    			campoSelectFormulario($campo++,$i,'',$formulario,1);
    			campoSelectFormulario($campo++,$i,'',$formulario,1);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE ELIMINACIÓN DE RESIDUOS</h3>";

	abreColumnaCampos('span10');
		campoTextoFormulario(162,$i,'Responsable de la gestión de residuos',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "Identificación de residudos<br/><br/>";
		campoRadioFormulario(163,$i,'Residuos orgánicos (restos de cocina, etc.)',$formulario);
		campoRadioFormulario(164,$i,'Residuos de origen animal (carnicería, pescadería)',$formulario);
		campoRadioFormulario(165,$i,'Residuos inorgánicos (envases, papel, cajas, etc.)',$formulario);
		campoRadioFormulario(166,$i,'Aceite usado',$formulario);
		campoRadioFormulario(167,$i,'Otros residudos',$formulario);
		echo "<div id='div".$i."167' class='hidden'>";
			campoTextoFormulario(168,$i,'Especificar',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "Almacenamiento de los residuos<br/><br/>";
		campoRadioFormulario(169,$i,'Cubo para basura (tapado y con pedal)',$formulario);
		campoRadioFormulario(170,$i,'Cubo para restos de carne y pescado',$formulario);
		campoRadioFormulario(171,$i,'Recipiente para aceite frito',$formulario);
		campoRadioFormulario(172,$i,'Cubo para inorgánicos',$formulario);
		campoRadioFormulario(173,$i,'Cubo para vidrios',$formulario);
		campoRadioFormulario(174,$i,'Otros',$formulario);
		echo "<div id='div".$i."174' class='hidden'>";
			campoTextoFormulario(175,$i,'Especificar',$formulario);
		echo "</div>";
		campoTextoFormulario(176,$i,'Descripción del lugar donde se almacenan los residuos o adjuntar fotografía',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'>";
	abreColumnaCampos();
		echo "Eliminación de residuos<br/><br/>";
		campoRadioFormulario(177,$i,'Se depositan en contenedores de la vía pública',$formulario);
		campoRadioFormulario(178,$i,'Los aceites usados se depositan en un contenedor de aceites usados',$formulario);
		campoRadioFormulario(179,$i,'Los residuos de origen animal se eliminan por medio de gestor autorizado. ',$formulario);
		echo "<div id='div".$i."179' class='hidden'>";
			campoTextoFormulario(180,$i,'Razón social',$formulario);
		echo "</div>";
		campoRadioFormulario(181,$i,'Los aceites usados se eliminan por medio de gestor autorizado. Indicar razón social:',$formulario);
		echo "<div id='div".$i."181' class='hidden'>";
			campoTextoFormulario(182,$i,'Razón social',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> PLAN DE TRANSPORTE</h3>";

	abreColumnaCampos();
		campoRadioFormulario(60,$i,'¿Dispone de vehículos para el transporte de alimentos?',$formulario);
	cierraColumnaCampos();

	echo "<div id='div".$i."60' class='hidden'>";

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Modelo/Marca' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaVehiculosAPPCC'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Modelo/Marca</th>
							<th> Transporte a temperaturas<br/>controladas </th>
							<th> Uso (transporte materias primas, comida<br/>elaborada, etc.)</th>
							<th> Documento ATP </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$vehiculos = consultaBD("SELECT * FROM vehiculos_appcc WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($vehiculo= mysql_fetch_assoc($vehiculos)){
        		echo "<tr>";
    				campoTextoTabla('modeloVehiculoAPPCC'.$indice,$vehiculo['modelo']);
    				campoSelect('temperaturasVehiculoAPPCC'.$indice,'',array('Si','No'),array('SI','NO'),$vehiculo['temperaturas'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('usoVehiculoAPPCC'.$indice,$vehiculo['uso']);
    				campoSelect('documentoVehiculoAPPCC'.$indice,'',array('Si','No'),array('SI','NO'),$vehiculo['documento'],'selectpicker span2 show-tick','data-live-search="true"',1);
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<2;$indice++){
    		echo "<tr>";
    			campoTextoTabla('modeloVehiculoAPPCC'.$indice);
    				campoSelect('temperaturasVehiculoAPPCC'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('usoVehiculoAPPCC'.$indice);
    				campoSelect('documentoVehiculoAPPCC'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaVehiculosAPPCC\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";
	echo "</div>";

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(194,$i,'Comentarios de comercial',$formulario,'areaInforme');

}

function formularioAuditoriaLOPD($i,$cliente,$servicio){
	campoOculto($servicio,'servicio'.$i);
	$trabajo = recogerTrabajo($cliente,$servicio);
	$formulario = recogerFormularioServicios($trabajo);
	//$formulario = completarDatosGeneralesAlergenos($formulario,$cliente);

	abreColumnaCampos();
		campoTextoFormulario(1,$i,'Consultor',$formulario);
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS DEL CLIENTE</h3>";

	campoRadioFormulario(3,$i,'¿Ha cambiado algún dato de la empresa?',$formulario);
	echo "<div id='div".$i."3' class='hidden'>";
		echo 'En caso afirmativo, indicar qué datos de la empresa se han modificado<br clear="all"><br/>';
		abreColumnaCampos();
			campoTextoFormulario(4,$i,'Razón Social',$formulario);
			campoTextoFormulario(5,$i,'Dirección Social',$formulario);
			campoTextoFormulario(6,$i,'Código postal',$formulario);
			campoTextoFormulario(7,$i,'Teléfono',$formulario);
		cierraColumnaCampos();

		abreColumnaCampos();
			campoTextoFormulario(8,$i,'CIF',$formulario);
			campoTextoFormulario(9,$i,'Poblacion',$formulario);
			campoTextoFormulario(10,$i,'Provincia',$formulario);
			campoTextoFormulario(11,$i,'Representate legal',$formulario);
		cierraColumnaCampos();
	echo "</div>";

	echo "<br clear='all'><h3 class='apartadoFormulario'> PROCEDIMIENTOS GENERALRES</h3>";
	echo "<br clear='all'><span class='subtitulo'>Tratamiento físico de los datos</span><br/><br/>";

	abreColumnaCampos();
		campoRadioFormulario(12,$i,'¿Guarda la empresa los datos de carácter personal (facturas, nóminas, etc.) en amrarios con llaves?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(13,$i,'¿Cuena la empresa con una destructora de papel?',$formulario);
		echo "<div id='div".$i."13' class='hidden' metodo='inverso'>";
		campoTextoFormulario(14,$i,'¿Cómo procede la empresa a la destrucción de la documentación confidencial',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>Tratamiento digital de los datos</span><br/><br/>";

	abreColumnaCampos();
		campoRadioFormulario(15,$i,'¿Los ordenadores tienen acceso por usuario y contraseña?',$formulario);
		campoRadioFormulario(16,$i,'Cuando un trabajador ya no está en la empresa¿Se procede a dar de baja su usuario y contraseña?',$formulario);
		campoRadioFormulario(17,$i,'¿El servidor de la empresa está filtrado por usuario y contraseña?',$formulario);
		campoRadioFormulario(18,$i,'¿Las copias de seguridad se almacenan fuera de las instalaciones de la empresa?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(19,$i,'¿Cada cuánto renueva la empresa las contraseñas?',$formulario);
		campoRadioFormulario(20,$i,'¿Los programas de gestión están limitadas por perfiles y privilegios?',$formulario);
		campoRadioFormulario(21,$i,'¿Cada cuánto realiza la empresa las copias de seguridad?',$formulario,'SEMANAL',array('Semanal','Quincenal','Mensual'),array('SEMANAL','QUINCENAL','MENSUAL'));
		campoRadioFormulario(22,$i,'¿Qué medio utiliza la empresa para la realización de las copias?',$formulario,'USB',array('USB','Disco duro externo','CD-DVD','Otros'),array('USB','DISCO','CD','OTROS'));
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> EN CASO DE FICHERO DE:</h3>";
	echo "<br clear='all'><span class='subtitulo'>Nóminas y RRHH</span><br/><br/>";

	campoRadioFormulario(23,$i,'¿Ha quedado registrado el fichero en la AEPD?',$formulario);
	echo "<div id='div".$i."23' class='hidden'>";
		abreColumnaCampos();
			campoTextoFormulario(24,$i,'Razón Social',$formulario);
			campoTextoFormulario(25,$i,'Dirección Social',$formulario);
		cierraColumnaCampos();

		abreColumnaCampos();
			campoTextoFormulario(26,$i,'CIF',$formulario);
		cierraColumnaCampos();
	echo "</div>";
	echo "<br clear='all'>";
	abreColumnaCampos();
		campoRadioFormulario(27,$i,'¿Se ha procedido a la firma del contrato de encargado del tratamiento con la asesoría laboral?',$formulario);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormulario(28,$i,'¿Los asesores envían la información confidencial (nóminas, seguros sociales, cartas de despido, etc.) de forma encriptada (en envíos por mail)?',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'>";
	campoRadioFormulario(29,$i,'¿Ha cambiado la empresa de servicio de prevención ajeno?',$formulario);
	echo "<div id='div".$i."29' class='hidden'>";
		abreColumnaCampos();
			campoTextoFormulario(30,$i,'Razón Social',$formulario);
			campoTextoFormulario(31,$i,'Dirección Social',$formulario);
		cierraColumnaCampos();

		abreColumnaCampos();
			campoTextoFormulario(32,$i,'CIF',$formulario);
		cierraColumnaCampos();
	echo "</div>";
	echo "<br clear='all'>";
	abreColumnaCampos();
		campoRadioFormulario(33,$i,'¿Se ha procedido a la firma del contrato de encargado del tratamiento con el servicio de prevención?',$formulario);
		campoRadioFormulario(34,$i,'¿Se han contratado nuevos empleados en el último año?',$formulario);
		campoRadioFormulario(35,$i,'¿Se han incorporado nuevos trabajadores con acceso a los datos de empleados en la empresa?',$formulario);
		echo "<div id='div".$i."35' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(36,$i,'Razón Social',$formulario);
				campoTextoFormulario(37,$i,'Dirección Social',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(38,$i,'CIF',$formulario);
				campoTextoFormulario(39,$i,'Cargo',$formulario);
			cierraColumnaCampos();
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(40,$i,'¿El servicio de prevención ajeno envía la información confidencial (evaluación de riesgos, planificación preventiva e informes de aptitud) de  forma encriptada (en envíos por mail)?',$formulario);
		campoRadioFormulario(41,$i,'¿Se ha firmado con todos los trabajadores el anexo al contrato de trabajo?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>Clientes y proveedores</span><br/><br/>";

	abreColumnaCampos();
		campoRadioFormulario(42,$i,'¿Ha quedado registrado el fichero en la AEPD?',$formulario);
		campoRadioFormulario(43,$i,'¿Se ha incluido el aviso legal en los correos electrónicos?',$formulario);
		campoRadioFormulario(44,$i,'¿Se han incorporado nuevos trabajadores con acceso a los datos de empleados en la empresa?',$formulario);
		echo "<div id='div".$i."44' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(45,$i,'Razón Social',$formulario);
				campoTextoFormulario(46,$i,'Dirección Social',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(47,$i,'CIF',$formulario);
				campoTextoFormulario(48,$i,'Cargo',$formulario);
			cierraColumnaCampos();
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(49,$i,'¿Se ha incluido el aviso legal en las facturas?',$formulario);
		campoRadioFormulario(50,$i,'¿Se ha procedido a la firma del contrato de encargado del tratamiento con el proveedor informático?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>Currículums</span><br/><br/>";

	abreColumnaCampos();
		campoRadioFormulario(51,$i,'¿Ha quedado registrado el fichero en la AEPD?',$formulario);
		campoRadioFormulario(52,$i,'¿Han cambiado los trabajadores con acceso a perfiles de candidatos a formar parte de la empresa?',$formulario);
	
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(53,$i,'¿Se envía por parte de la empresa el aviso legal sobre el tratamiento de los datos que aparecen en el CV?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>Videovigilancia</span><br/><br/>";

	abreColumnaCampos();
		campoRadioFormulario(54,$i,'¿Ha quedado registrado el fichero en la AEPD?',$formulario);
		campoRadioFormulario(55,$i,'¿Ha cambiado la finalidad de las grabaciones? Control empresarial o seguridad de las instalaciones',$formulario);
		campoRadioFormulario(56,$i,'¿Se incluye en el anexo al contrato de trabajo la finalidad de las grabaciones?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(57,$i,'¿Ha instalado la empresa nuevas cámaras de videovigilancia?',$formulario);
		campoRadioFormulario(58,$i,'Se ha notificado al comité de empresa los cambios en la finalidad?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>Historial clínico /pacientes</span><br/><br/>";

	abreColumnaCampos();
		campoRadioFormulario(59,$i,'¿Ha quedado registrado el fichero en la AEPD?',$formulario);
		campoRadioFormulario(60,$i,'¿Trabaja la empresa con algún colaborador/empresa externa que pueda tener acceso a los datos?',$formulario);
		echo "<div id='div".$i."38' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(61,$i,'Razón Social',$formulario);
				campoTextoFormulario(62,$i,'Dirección Social',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(63,$i,'CIF',$formulario);
			cierraColumnaCampos();
		echo "</div>";
		campoRadioFormulario(64,$i,'¿Se encriptan los datos de los pacientes al enviarse por medios digitales?',$formulario);
		campoRadioFormulario(71,$i,'¿Los datos llegan a entidades aseguradoras? En ese caso, ¿Se firma el contrato por cuenta de terceros, en calidad de encargado del tratamiento?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(65,$i,'¿Se ha firmado el consetimiento informado de los pacientes del centro?',$formulario);
		campoRadioFormulario(66,$i,'¿Ha cambiado los trabajadores con acceso a datos de pacientes?',$formulario);
		echo "<div id='div".$i."66' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(67,$i,'Razón Social',$formulario);
				campoTextoFormulario(68,$i,'Dirección Social',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(69,$i,'CIF',$formulario);
				campoTextoFormulario(70,$i,'Cargo',$formulario);
			cierraColumnaCampos();
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>Alumnos</span><br/><br/>";

	abreColumnaCampos();
		campoRadioFormulario(72,$i,'¿Ha quedado registrado el fichero en la AEPD?',$formulario);
		campoRadioFormulario(73,$i,'¿Se ha llegado a algún acuerdo nuevo con entidades externa al centro y que requieran el acceso a datos confidenciales (actividades deportivos, servicios sociales, escuelas de música, etc.)?',$formulario);
		echo "<div id='div".$i."73' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(74,$i,'Razón Social',$formulario);
				campoTextoFormulario(75,$i,'Dirección Social',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(76,$i,'CIF',$formulario);
			cierraColumnaCampos();
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(77,$i,'¿Ha cambiado los trabajadores con acceso a datos de alumnos?',$formulario);
		echo "<div id='div".$i."77' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(78,$i,'Razón Social',$formulario);
				campoTextoFormulario(79,$i,'Dirección Social',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(80,$i,'CIF',$formulario);
				campoTextoFormulario(81,$i,'Cargo',$formulario);
			cierraColumnaCampos();
		echo "</div>";
	cierraColumnaCampos();
	echo "<br clear='all'>";
	abreColumnaCampos();
		campoRadioFormulario(82,$i,'Cuando hay alguna salida (excursiones, convivencias, etc.) ¿los datos de los alumnos y/o sus representates legales son cedidos a empresas externas?',$formulario);
		echo "<div id='div".$i."82' class='hidden'>";
			abreColumnaCampos();
				campoTextoFormulario(83,$i,'Razón Social',$formulario);
				campoTextoFormulario(84,$i,'Dirección Social',$formulario);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoTextoFormulario(85,$i,'CIF',$formulario);
			cierraColumnaCampos();
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(86,$i,'Comentarios de comercial',$formulario,'areaInforme');
}

/*function formularioAuditoriaLOPD($i,$cliente,$servicio){
	campoOculto($servicio,'servicio'.$i);
	$trabajo = recogerTrabajo($cliente,$servicio);
	$formulario = recogerFormularioServicios($trabajo);
	//$formulario = completarDatosGeneralesAlergenos($formulario,$cliente);

	abreColumnaCampos();
		campoTextoFormulario(1,$i,'Consultor',$formulario);
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> FICHA TÉCNICA</h3>";

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>IDENTIFICACIÓN DEL ENTREVISTADO</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(3,$i,'Nombre y apellidos',$formulario);
		campoTextoFormulario(4,$i,'Cargo',$formulario);
		campoTextoFormulario(5,$i,'Teléfono',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(6,$i,'Email',$formulario);
		campoTextoFormulario(7,$i,'Departamento',$formulario);
		campoTextoFormulario(8,$i,'Tipo de entrevista',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>DOCUMENTACIÓN RECIBIDA POR EL AUDITOR</span><br/><br/>";
	cierraColumnaCampos();

	echo"	<br clear='all'>
			Para eliminar deja el campo 'Título del documento' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaDocumentosAuditoria'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Título del documento</th>
							<th> Fecha de entrega </th>
							<th> Breve referencia del contenido </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$documentos = consultaBD("SELECT * FROM documentos_auditoria WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($documento = mysql_fetch_assoc($documentos)){
        		echo "<tr>";
    				campoTextoTabla('tituloDocumentoAuditoria'.$indice,$documento['titulo']);
    				campoFechaTabla('fechaDocumentoAuditoria'.$indice,$documento['fecha']);
    				areaTextoTabla('referenciaDocumentoAuditoria'.$indice,$documento['referencia']);
				echo"
					</tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<3;$indice++){
    		echo "<tr>";
    			campoTextoTabla('tituloDocumentoAuditoria'.$indice);
    			campoFechaTabla('fechaDocumentoAuditoria'.$indice);
    			areaTextoTabla('referenciaDocumentoAuditoria'.$indice);
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaDocumentosAuditoria\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";


	echo "<br clear='all'><h3 class='apartadoFormulario'> CUESTIONES - REGLAMENTO DE SEGURIDAD</h3>";

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>CAMBIOS EN LA ORGANIZACIÓN</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		areaTextoFormulario(9,$i,'¿Ha habido cambios en la organización de carácter identificativo? (denominación social, NIF…)',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		areaTextoFormulario(10,$i,'¿Ha variado el organigrama de la organización? (nuevos empleados, bajas…)',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>FICHEROS DECLARADOS</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		campoRadioFormulario(11,$i,'¿Se han creado, modificado o suprimido ficheros con datos de carácter personal desde la última auditoría?',$formulario);
		echo "<div id='div".$i."11' class='hidden'>";
			areaTextoFormulario(12,$i,'Ficheros',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "Coger las inscripciones de los ficheros de la Agencia Española de Protección de Datos y preguntar al cliente si:";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(13,$i,'Los datos de carácter personal están englobados en los ficheros declarados o no. Si es necesario añadir alguno (por ejemplo: que en la actualidad hayan puesto cámaras, por lo que sería necesario declarar el fichero video vigilancia), o suprimir alguno (por ejemplo: que en la actualidad ya no utilicen currículums)',$formulario);
		echo "<div id='div".$i."13' class='hidden'>";
			areaTextoFormulario(14,$i,'Datos',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(15,$i,'El soporte indicado de cada fichero está actualizado o no. (Por ejemplo: fichero nóminas: soporte manual, - preguntar si sigue siendo manual o ahora lo conservan tanto en papel como ordenador)',$formulario);
		echo "<div id='div".$i."15' class='hidden'>";
			areaTextoFormulario(16,$i,'Soportes',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>DOCUMENTOS DE SEGURIDAD</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		campoRadioFormulario(17,$i,'¿El documento se mantiene en todo momento actualizado y es revisado siempre que se produzcan cambios relevantes en los sistemas de información o en la organización del mismo?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "¿El documento contiene los siguientes aspectos?<br/>";
		campoRadioFormulario(18,$i,'Ámbito de aplicación del documento con especificación detallada de los recursos protegidos',$formulario);
		campoRadioFormulario(19,$i,'Medidas, normas, procedimientos de actuación, reglas y estándares encaminados a garantizar el nivel de seguridad exigido en este reglamento.',$formulario);
		campoRadioFormulario(20,$i,'Funciones y obligaciones del personal en relación con el tratamiento de los datos de carácter personal incluidos en los ficheros.',$formulario);
		campoRadioFormulario(21,$i,'Estructura de los ficheros con datos de carácter personal y descripción de los sistemas de información que los tratan.',$formulario);
		campoRadioFormulario(22,$i,'Procedimiento de notificación, gestión y respuesta ante incidencias.',$formulario);
		campoRadioFormulario(23,$i,'Los procedimientos de realización de copias de respaldo y de recuperación de los datos en los ficheros o tratamientos automatizados.',$formulario);
		campoRadioFormulario(24,$i,'Las medidas que sea necesario adoptar para el transporte de soportes y documentos, así como la destrucción de los documentos y soportes, o en su caso, la reutilización de estos últimos',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "¿El documento de seguridad contiene lo siguiente? <br/>";
		campoRadioFormulario(25,$i,'La identificación del responsable o responsables de seguridad',$formulario);
		campoRadioFormulario(26,$i,'Los controles periódicos que se deban realizar para verificar el cumplimiento de lo dispuesto en el propio documento',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>ENCARGADOS DE TRATAMIENTO</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(27,$i,'¿Hay empresas contratadas por el cliente que accedan a soportes o recursos que contengan datos de carácter personal con el fin de prestarle un servicio al cliente? ',$formulario);
		echo "<div id='div".$i."27' class='hidden'>";
			areaTextoFormulario(28,$i,'Empresas',$formulario);
		echo "</div>";
		campoRadioFormulario(29,$i,'¿Tiene el cliente los contratos de acceso a datos con dichas empresas? ',$formulario);
		campoRadioFormulario(35,$i,'Cuando el responsable del fichero o tratamiento facilite el acceso a los datos, a los soportes que los contengan o a los recursos del sistema de información que los trate, a un encargado de tratamiento que preste sus servicios en los locales del primero, ¿se hace constar esta circunstancia en el Documento de Seguridad? ',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(30,$i,'En el caso de existir un tratamiento de datos por cuenta de terceros, el documento incluye la identificación de los ficheros o tratamientos en concepto de encargado de tratamiento, o bien un documento que regule las condiciones de prestador de servicios. Especificar cuál de las opciones contempla ',$formulario);
		echo "<div id='div".$i."30' class='hidden'>";
			areaTextoFormulario(31,$i,'Opciones ',$formulario);
		echo "</div>";
		campoRadioFormulario(33,$i,'¿Los datos de carácter personal de un fichero o tratamiento se incorporan y se tratan de forma exclusiva en los sistemas del encargado de tratamiento?',$formulario);
		echo "<div id='div".$i."33' class='hidden'>";
			campoRadioFormulario(34,$i,'¿El responsable lo hace constar en el documento de seguridad?',$formulario);
		echo "</div>";
		campoRadioFormulario(36,$i,'Cuando  dicho acceso sea remoto habiéndose prohibido al encargado incorporar tales datos a sistemas o soportes distintos de los del responsable, este último ¿hace constar esta circunstancia en el documento de seguridad del responsable?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>PRESTADORES DE SERVICIOS SIN ACCESO A DATOS PERSONALES</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(37,$i,'¿Tiene el cliente prestadores de servicios sin acceso a datos personales?',$formulario);
		echo "<div id='div".$i."37' class='hidden'>";
			campoRadioFormulario(38,$i,'¿Firman con esos prestadores acuerdos de prestación de servicios sin acceso a datos?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>DELEGACIÓN DE AUTORIDADES</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(39,$i,'¿Las autoridades que en el art. 84 RD se atribuyen al responsable del fichero o tratamiento son delegadas en alguien?',$formulario);
		echo "<div id='div".$i."39' class='hidden'>";
			campoTextoFormulario(40,$i,'¿En quién?',$formulario);
			campoTextoFormulario(41,$i,'¿Se hace constar esa delegación en el documento de seguridad?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>ACCESO A TRAVÉS DE REDES DE TELECOMUNICACIONES</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(42,$i,'¿Existe algún tipo de acceso a los datos de carácter personal a través de redes de comunicaciones? ',$formulario);
		campoRadioFormulario(43,$i,'¿Se garantiza un nivel de seguridad equivalente al correspondiente a los accesos en modo local?  ',$formulario);
		echo "<div id='div".$i."43' class='hidden'>";
			areaTextoFormulario(44,$i,'Describa las medidas de seguridad establecidas para dichos accesos',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>RÉGIMEN DE TRABAJO FUERA DE LOS LOCALES DE LA UBICACIÓN DEL FICHERO</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(45,$i,'¿Se ejecutan tratamientos de datos de carácter personal fuera de los locales en los que se encuentran ubicados los ficheros? ',$formulario);
		echo "<div id='div".$i."45' class='hidden'>";
			campoRadioFormulario(46,$i,'Autoriza expresamente el responsable del fichero a que los tratamientos de datos de carácter personal se ejecuten fuera de los locales de la ubicación de los mismos?',$formulario);
				echo "<div id='div".$i."46' class='hidden'>";
					campoRadioFormulario(47,$i,'¿La autorización consta en el documento de seguridad?',$formulario);
				echo "</div>";
			campoRadioFormulario(48,$i,'¿Se garantiza el nivel de seguridad correspondiente al tipo de fichero tratado fuera de los locales de ubicación de los ficheros?',$formulario);
		echo "</div>";
		areaTextoFormulario(49,$i,'Observaciones',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>FICHEROS TEMPORALES O COPIAS DE TRABAJO DE DOCUMENTOS</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(50,$i,'¿Se generan ficheros temporales o copias de documentos que contengan datos de carácter personal?',$formulario);
		echo "<div id='div".$i."50' class='hidden'>";
			campoRadioFormulario(51,$i,'¿Cumplen las medidas de seguridad que les corresponde con arreglo a los criterios establecidos por el RLOPD (nivel de seguridad)?',$formulario);
			campoRadioFormulario(52,$i,'¿Son eliminados o destruidos una vez que han dejado de ser necesarios para los fines que motivaron su creación?',$formulario);
		echo "</div>";
		areaTextoFormulario(53,$i,'Observaciones',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>FUNCIONES Y OBLIGACIONES DEL PERSONAL</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(54,$i,'¿Ha informado al personal sobre las medidas de seguridad que deben adoptar en el desarrollo de sus funciones respecto al tratamiento de datos personales? ',$formulario);
		echo "<div id='div".$i."54' class='hidden'>";
			areaTextoFormulario(55,$i,'Especifique',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(56,$i,'¿Las funciones y obligaciones de cada uno de los usuarios o perfiles de usuarios con acceso a los datos de carácter personal y a los sistemas de información están claramente definidas y documentadas en el documento de seguridad? ',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>REGISTRO DE INCIDENCIAS</span><br/><br/>";
	cierraColumnaCampos();
	
	abreColumnaCampos();
		campoRadioFormulario(57,$i,'¿Tiene establecido algún procedimiento de notificación de incidencias?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<div id='div".$i."57' class='hidden'>";
			campoRadioFormulario(58,$i,'¿Cuenta dicho procedimiento con un registro de incidencias?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<div id='div".$i."58' class='hidden'>";
		abreColumnaCampos('span10');
			echo "Señale qué apartados de los siguientes constan en el registro de incidencias:<br/>";
		cierraColumnaCampos();
	
		abreColumnaCampos();
			campoRadioFormulario(59,$i,'Tipo de incidencia',$formulario);
			echo "<br/>";
			campoRadioFormulario(60,$i,'Persona que notificia la incidencia',$formulario);
			campoRadioFormulario(61,$i,'Efectos que se hubieren derivado de la incidencia',$formulario);
			campoRadioFormulario(62,$i,'Persona que ejecutó el proceso de recuperación',$formulario);
			campoRadioFormulario(63,$i,'Datos grabados manualmente',$formulario);
		cierraColumnaCampos();

		abreColumnaCampos();
			campoRadioFormulario(64,$i,'Fecha y hora en la que se produce',$formulario);
			campoRadioFormulario(65,$i,'Persona a quien se notifica',$formulario);
			campoRadioFormulario(66,$i,'Medidas correctoras aplicadas',$formulario);
			echo "<br/>";
			campoRadioFormulario(67,$i,'Datos restaurados',$formulario);
		cierraColumnaCampos();
	echo "</div>";

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>CONTROL DE ACCESO</span><br/><br/>";
	cierraColumnaCampos();
	
	abreColumnaCampos();
		campoRadioFormulario(68,$i,'¿Los usuarios tendrán acceso autorizado a aquellos datos y recursos que se necesiten para el desarrollo de sus funciones?',$formulario);
		campoRadioFormulario(69,$i,'¿El responsable del fichero establece mecanismos para evitar que un usuario pueda acceder a datos o recursos con derechos diferentes a los autorizados? ',$formulario);
		echo "<div id='div".$i."69' class='hidden'>";
			areaTextoFormulario(70,$i,'Detalles',$formulario);
		echo "</div>";
		campoRadioFormulario(71,$i,'¿Exclusivamente el personal autorizado en el documento de seguridad tiene acceso a los lugares donde se hallen instalados los equipos físicos que den soporte a los sistemas de información?',$formulario);
		campoRadioFormulario(72,$i,'Si, atendidas las características de los locales de los que dispusiera el responsable el fichero o tratamiento, no fuera posible cumplir lo establecido en el apartado anterior, el responsable adopta medidas alternativas que, debidamente motivas, se incluirán en el documento de seguridad.',$formulario);
		campoRadioFormulario(73,$i,'Siempre que se proceda al traslado físico de la documentación contenida en un fichero, se adoptan medidas dirigidas a impedir el acceso o manipulación de la información objeto de traslado.',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(74,$i,'¿El responsable del fichero o tratamiento se encarga de existencia de una relación de usuarios y perfil de usuarios con sus respectivos accesos autorizados?',$formulario);
		campoRadioFormulario(75,$i,'¿Exclusivamente el personal autorizado en el documento de seguridad podrá conceder, modificar o anular el acceso autorizado sobre datos y recursos, de acuerdo con los criterios establecidos con el responsable del fichero? ',$formulario);
		campoRadioFormulario(76,$i,'¿En el supuesto de que existan personas ajenas al responsable del fichero que tengan acceso a los recursos, cumplen con las mismas obligaciones y condiciones de seguridad que el personal propio?',$formulario);
		campoRadioFormulario(77,$i,'¿Los armarios, archivadores u otros elementos en los que se almacenen los ficheros no automatizados con datos de carácter personal se  encuentran en áreas en las que el acceso esté protegido con puertas de acceso dotas de sistemas de apertura mediante llave u otro dispositivo equivalente? ¿Dichas áreas permanecen cerradas cuando no sea preciso el acceso a los documentos incluidos en el fichero? ',$formulario);
		echo "<div id='div".$i."77' class='hidden'>";
			areaTextoFormulario(78,$i,'Detallar',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>GESTIÓN DE SOPORTES Y DOCUMENTOS</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(79,$i,'¿Están los soportes que contienen datos de carácter personal inequívocamente identificados, de tal manera que pueda conocerse el tipo de información que contienen?',$formulario);
		campoRadioFormulario(80,$i,'¿Los soportes y documentos que contienen datos personales identifican el tipo de información que contienen ',$formulario);
		echo "<div id='div".$i."80' class='hidden'>";
			areaTextoFormulario(81,$i,'Describa el sistema de etiquetado que se utiliza',$formulario);
		echo "</div>";
		campoRadioFormulario(82,$i,'¿Siempre que vaya a desecharse cualquier documento o soporte que contenga datos de carácter personal se procede a su destrucción o borrado, mediante la adopción de medidas dirigidas a evitar el acceso a la información contenida en el mismo o su recuperación posterior?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "¿Existe un inventario de los siguientes tipos de soportes o documentos que contienen datos de carácter personal?<br/>";
		campoRadioFormulario(83,$i,'Soporte no infórmaticos',$formulario);
		campoRadioFormulario(84,$i,'Soporte informático',$formulario);
		campoRadioFormulario(85,$i,'Documentos',$formulario);
		campoRadioFormulario(86,$i,'¿Se encuentra actualizado dicho inventario?',$formulario);
		campoRadioFormulario(87,$i,'¿Existe un registro de entrada de los soportes que contienen datos de carácter personal?',$formulario);
	cierraColumnaCampos();

	echo "<div id='div".$i."87' class='hidden'>";
		abreColumnaCampos('span10');
			echo "Señale qué apartados de los siguientes constan en el registro:<br/>";
		cierraColumnaCampos();
	
		abreColumnaCampos();
			campoRadioFormulario(88,$i,'Tipo de soporte recibido',$formulario);
			campoRadioFormulario(89,$i,'Emisor',$formulario);
			campoRadioFormulario(90,$i,'Tipo de información que contienen',$formulario);
			campoRadioFormulario(91,$i,'Personas responsable y debidamente autorizada para la recepción',$formulario);
		cierraColumnaCampos();

		abreColumnaCampos();
			campoRadioFormulario(92,$i,'Fecha y hora de la recepción',$formulario);
			campoRadioFormulario(93,$i,'Número de soportes recibidos',$formulario);
			campoRadioFormulario(94,$i,'Forma de envío',$formulario);
		cierraColumnaCampos();
	echo "</div>";

	abreColumnaCampos('span10');
		campoRadioFormulario(95,$i,'¿Existe un registro de salida de los soportes que contienen datos de carácter personal?',$formulario);
	cierraColumnaCampos();

	echo "<div id='div".$i."95' class='hidden'>";
		abreColumnaCampos('span10');
			echo "Señale qué apartados de los siguientes constan en el registro:<br/>";
		cierraColumnaCampos();
	
		abreColumnaCampos();
			campoRadioFormulario(96,$i,'Tipo de soporte enviado',$formulario);
			campoRadioFormulario(97,$i,'Destinatario',$formulario);
			campoRadioFormulario(98,$i,'Tipo de información que contienen',$formulario);
			campoRadioFormulario(91,$i,'Autorización del responsable del fichero para la salida de soportes',$formulario);
		cierraColumnaCampos();

		abreColumnaCampos();
			campoRadioFormulario(99,$i,'Fecha y hora de la envío',$formulario);
			campoRadioFormulario(100,$i,'Número de soportes enviados',$formulario);
			campoRadioFormulario(101,$i,'Forma de envío',$formulario);
		cierraColumnaCampos();
	echo "</div><br clear='all'>";

	abreColumnaCampos();
		campoRadioFormulario(102,$i,'La distribución de soportes físicos informáticos (por ejemplo portátiles) que contienen datos de carácter personal (nivel alto), es decir, el transporte, reparto, entrega o envío de los mismos, ¿se lleva a cabo cifrando los datos o utilizando cualquier otro mecanismo que garantice que la información no sea inteligible ni manipulada durante su transporte? ',$formulario);
		echo "<div id='div".$i."102' class='hidden'>";
			areaTextoFormulario(103,$i,'Describa el método empleado',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(104,$i,'¿Se utiliza un sistema de etiquetado críptico en relación a soportes que contienen datos especialmente sensibles para la organización? ',$formulario);
		echo "<div id='div".$i."104' class='hidden'>";
			areaTextoFormulario(105,$i,'Describa el sistema de etiquetado que se utiliza',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>IDENTIFICACIÓN Y AUTENTIFICACIÓN</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(106,$i,'¿Existe una relación actualizada de los usuarios autorizados para acceder al sistema de información? Si es así, muestre al auditor dicha relación',$formulario);
		campoRadioFormulario(107,$i,'Se trata de un mecanismo basado en el uso de contraseñas? En caso afirmativo periodicidad de cambio. En caso negativo indique el procedimiento de autenticación implantado',$formulario);
		echo "<div id='div".$i."107' class='hidden' metodo='inverso'>";
			areaTextoFormulario(108,$i,'Procedimiento',$formulario);
		echo "</div>";
		campoRadioFormulario(109,$i,'¿El responsable del fichero o tratamiento establece un mecanismo que limite la posibilidad de intentar reiteradamente el acceso no autorizado al sistema de información?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(110,$i,'¿Está establecido un procedimiento de identificación y autenticación para dicho acceso? Si es así, muestre al auditor dicho procedimiento',$formulario);
		campoRadioFormulario(111,$i,'Cuando el mecanismo de autentificación se base en la existencia de contraseñas ¿existe algún procedimiento de asignación, distribución y almacenamiento que garantice su confidencialidad e integridad? ',$formulario);
		echo "<div id='div".$i."111' class='hidden'>";
			areaTextoFormulario(112,$i,'Detalles',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>COPIAS DE RESPALDO Y RECUPERACIÓN</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(113,$i,'¿Con qué periodicidad se realizan copias de seguridad?',$formulario);
		campoRadioFormulario(114,$i,'¿El responsable del fichero se encarga de verificar semestralmente la correcta definición, funcionamiento y aplicación de los procedimientos de realización de copias de respaldo? ',$formulario);
		echo "<div id='div".$i."114' class='hidden'>";
			areaTextoFormulario(115,$i,'Detallar',$formulario);
		echo "</div>";
		campoRadioFormulario(116,$i,'¿Se conserva una copia de seguridad y de los procedimientos de recuperación de los datos en un lugar diferente de donde se encuentren los equipos? ',$formulario);
		echo "<div id='div".$i."116' class='hidden'>";
			areaTextoFormulario(117,$i,'Detallar',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(118,$i,'¿Existen procedimientos establecidos para la realización de copias de seguridad y para la recuperación de los datos que garanticen su reconstrucción en el estado en que se encontraban en el momento de producirse la pérdida o destrucción?  ',$formulario);
		echo "<div id='div".$i."118' class='hidden'>";
			areaTextoFormulario(119,$i,'Detallar',$formulario);
		echo "</div>";
		campoRadioFormulario(120,$i,'¿Las pruebas anteriores a la implantación o modificación de los sistemas de información que traten datos de carácter personal se realizan con datos reales?',$formulario);
		echo "<div id='div".$i."120' class='hidden'>";
			campoRadioFormulario(121,$i,'¿Se asegura el nivel de seguridad por el tratamiento realizado y se anota su realización en el documento de seguridad?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>RESPONSABLE DE SEGURIDAD</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(122,$i,'¿En el documento de seguridad se designa uno o varios responsables de seguridad encargados de coordinar y controlar las medidas definidas en el mismo?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>AUDITORÍA</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(123,$i,'¿A partir del nivel medio ¿los sistemas de información e instalaciones de tratamiento y almacenamiento de datos se someten, al menos cada dos años, a una auditoría interna o externa que verifique el cumplimiento del presente título?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(124,$i,'¿Con carácter extraordinario se ha realizado dicha auditoría siempre que se realicen modificaciones sustanciales en el sistema de información que puedan repercutir en el cumplimiento de las medidas de seguridad implantadas con el objeto de verificar la adaptación, adecuación y eficacia de las mismas?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "¿El informe de auditoría incluye estos aspectos?<br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(125,$i,'La adecuación de las medidas y controles a la ley y desarrollo reglamentario',$formulario);
		campoRadioFormulario(126,$i,'Identifica deficiencias y propone las medidas correctoras o complementarias necesarias',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(127,$i,'Incluye los datos, hechos y observaciones en que se basen los dictámenes alcanzados y las recomendaciones propuestas.',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>REGISTRO DE ACCESOS (NIVEL ALTO)</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(128,$i,'¿De cada intento de acceso se guardan, como mínimo, la identificación del usuario, la fecha y hora en que se realizó, el fichero accedido, el tipo de acceso y si ha sido autorizado o denegado? ',$formulario);
		echo "<div id='div".$i."128' class='hidden'>";
			areaTextoFormulario(129,$i,'Detallar',$formulario);
		echo "</div>";
		campoRadioFormulario(130,$i,'¿El período mínimo de conservación de los datos registrados es de dos años? ',$formulario);
		echo "<div id='div".$i."130' class='hidden'>";
			areaTextoFormulario(131,$i,'Detallar',$formulario);
		echo "</div>";
		campoRadioFormulario(132,$i,'¿El acceso a la documentación se limita exclusivamente al personal autorizado?',$formulario);
		campoRadioFormulario(137,$i,'Respecto a los datos de salud que se conservan en papel, ¿S establecen mecanismos que permitan identificar los accesos realizados en el caso de documentos que puedan ser utilizados por múltiples usuarios? Como por ejemplo, llevar un registro de cada acceso',$formulario);
		echo "<div id='div".$i."137' class='hidden'>";
			areaTextoFormulario(138,$i,'Detallar',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(133,$i,'En el caso de que el acceso haya sido autorizado, ¿se guarda la información que permita identificar el registro accedido?',$formulario);
		campoRadioFormulario(134,$i,'¿Los mecanismos que permiten el registro de accesos estarán bajo el control  directo del responsable de seguridad competente sin que deban permitir la desactivación ni la manipulación de los mismos?',$formulario);
		campoRadioFormulario(135,$i,'¿El responsable de seguridad se encarga de revisar al menos una vez al mes la información de control registrada y elabora un informe de las revisiones realizadas y los problemas detectados? ',$formulario);
		echo "<div id='div".$i."135' class='hidden'>";
			areaTextoFormulario(136,$i,'Detallar',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>ALMACENAMIENTO DE INFORMACIÓN</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(139,$i,'¿Los dispositivos de almacenamiento de los documentos que contienen datos de carácter personal disponen de mecanismos que obstaculicen su apertura? ',$formulario);
		echo "<div id='div".$i."139' class='hidden'>";
			areaTextoFormulario(140,$i,'Detallar',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(141,$i,'Cuando las características físicas de aquéllos no permitan adoptar esta medida, ¿el responsable del fichero o tratamiento adoptará medidas que impidan el acceso de persona no autorizadas? ',$formulario);
		echo "<div id='div".$i."141' class='hidden'>";
			areaTextoFormulario(142,$i,'Detallar',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>CUSTODIA DE SOPORTES</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(143,$i,'¿Mientras la documentación con datos de carácter personal no se encuentre archivada en los dispositivos de almacenamiento establecidos en el artículo anterior, por estar en proceso de revisión o tramitación, ya sea previo o posterior a su archivo, la persona que se encuentre al cargo de la misma custodia e impide en todo momento que pueda ser accedida por persona no autorizada? ',$formulario);
		echo "<div id='div".$i."143' class='hidden'>";
			areaTextoFormulario(144,$i,'Detallar',$formulario);
		echo "</div>";
	cierraColumnaCampos();


	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>CRITERIOS DE ARCHIVOS</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(145,$i,'¿El archivo de los soportes o documentos se realiza de acuerdo con los criterios previstos en su respectiva legislación garantizando la correcta conservación de los documentos, la localización y consulta de la información y posibilitando el ejercicio de los derechos de oposición al tratamiento, acceso, rectificación y cancelación? ',$formulario);
		echo "<div id='div".$i."145' class='hidden'>";
			areaTextoFormulario(146,$i,'Especificar los criterios de archivos que se sigue',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(147,$i,'¿En aquellos casos en los que no exista norma aplicable, el responsable del fichero establece los criterios y procedimientos de actuación que deban seguirse para el archivo? ',$formulario);
		echo "<div id='div".$i."147' class='hidden'>";
			areaTextoFormulario(148,$i,'Especificar los criterios de archivos que se sigue',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> CUESTIONES - LSSI</h3>";

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>AUDITORÍA LOPD</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(149,$i,'¿Tienen página web?',$formulario);
		echo "<div id='div".$i."149' class='hidden'>";
			campoRadioFormulario(150,$i,'¿Está adaptada a la LSSI? (cuentan con el aviso legal, política de cookies…)',$formulario);
		echo "</div>";
		campoRadioFormulario(151,$i,'¿Realizan publicidad?',$formulario);
		echo "<div id='div".$i."151' class='hidden'>";
			campoRadioFormulario(152,$i,'Usuarios',$formulario,'CLIENTES',array('Clientes','No clientes'),array('CLIENTES','NOCLIENTES'));
			campoRadioFormulario(153,$i,'¿De qué modo?',$formulario,'POSTAL',array('Vía Postal','Vía electrónica','SMS'),array('POSTAL','ELECTRONICA','SMS'));
			campoRadioFormulario(154,$i,'¿Guardan la relación de los destinatarios de esa publicidad?',$formulario);
			echo "<div id='div".$i."154' class='hidden'>";
				campoTextoFormulario(155,$i,'¿En qué soporte?',$formulario);
			echo "</div>";
			campoRadioFormulario(156,$i,'¿Solicita consentimiento para el envío de la publicidad?',$formulario);
			echo "<div id='div".$i."156' class='hidden'>";
				campoRadioFormulario(157,$i,'¿Con cada comunicación les informan del derecho a revocación del consentimiento y modo de realizarla?',$formulario);
			echo "</div>";
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> CUESTIONES - LOPD</h3>";

	abreColumnaCampos('span10');
		echo "<span class='subtitulo'>AUDITORÍA LOPD</span><br/><br/>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(158,$i,'¿Tienen firmados los correspondientes contratos de acceso a datos con todos sus proveedores con acceso a datos de carácter persona?',$formulario);
		campoRadioFormulario(159,$i,'En el caso de que necesite acceder a datos de carácter personal de sus empresas clientas, tiene firmados los correspondientes contratos de acceso a datos como prestador de servicios?',$formulario);
		campoRadioFormulario(160,$i,'¿Informan a los clientes (destinatarios finales) de que sus datos forman parte del fichero clientes, con qué finalidad y dónde poder ejercitar los derechos arco? (acceso, rectificación, cancelación y oposición de sus datos)',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(161,$i,'¿Tienen firmados los acuerdos de prestación de servicios sin acceso a datos con sus proveedores de servicios? (empresa de limpieza, de mantenimiento extintores…)',$formulario);
		campoRadioFormulario(162,$i,'¿Tienen firmados con sus empleados los correspondientes acuerdos de confidencialidad?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> CONCLUSION</h3>";

	abreColumnaCampos('span10');
		campoFechaFormulario(163,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(164,$i,'Acción formativa',$formulario);
		campoTextoFormulario(165,$i,'Nº Alumnos',$formulario,'input-small');
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(166,$i,'Importe',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'>";
	abreColumnaCampos();
		campoTextoFormulario(167,$i,'Mantenimiento',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(168,$i,'Importe',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span10');
		echo "El Cliente se compromete a abonar a LAE CONSULTING, contra presentación de la correspondiente factura,<br/>los honorarios meritados a favor de LAE CONSULTING por los servicios contratados, tanto si son de formación o de gestión de la misma.<br/><br/>";
		campoTextoFormulario(169,$i,'Forma de pago',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<span class='subtitulo'>IDENTIFICACIÓN DEL AUDITOR</span><br/><br/>";
		campoTextoFormulario(170,$i,'Nombre y apellidos',$formulario);
		campoTextoFormulario(171,$i,'DNI',$formulario);
		campoFirmaFormulario(173,$i,'Firma',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<span class='subtitulo'>IDENTIFICACIÓN DEL ENTREVISTADO</span><br/><br/>";
		campoTextoFormulario(172,$i,'Nombre y apellidos',$formulario);
		campoTextoFormulario(32,$i,'DNI',$formulario);
		campoFirmaFormulario(174,$i,'Firma',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(175,$i,'Comentarios de comercial',$formulario,'areaInforme');

}*/

function completarDatosGeneralesAlergenos($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	$formulario['pregunta3'] = $formulario['pregunta3'] == '' ? $cliente['razonSocial'] : $formulario['pregunta3']; 
	$formulario['pregunta4'] = $formulario['pregunta4'] == '' ? $cliente['domicilio'] : $formulario['pregunta4']; 
	$formulario['pregunta5'] = $formulario['pregunta5'] == '' ? $cliente['localidad'] : $formulario['pregunta5']; 
	$formulario['pregunta6'] = $formulario['pregunta6'] == '' ? $cliente['telefono'] : $formulario['pregunta6']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['email'] : $formulario['pregunta12']; 
	$formulario['pregunta8'] = $formulario['pregunta8'] == '' ? $cliente['administrador'] : $formulario['pregunta8']; 
	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['cif'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['cp'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? convertirMinuscula($cliente['provincia']) : convertirMinuscula($formulario['pregunta11']); 
	$formulario['pregunta18'] = $formulario['pregunta18'] == '' ? $cliente['fax'] : $formulario['pregunta18']; 
	$formulario['pregunta13'] = $formulario['pregunta13'] == '' ? $cliente['nombreComercial'] : $formulario['pregunta13']; 
	$formulario['pregunta14'] = $formulario['pregunta14'] == '' ? $cliente['nifAdministrador'] : $formulario['pregunta14']; 

	return $formulario;
}

function completarDatosGeneralesLOPDComunidad($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	$formulario['pregunta3'] = $formulario['pregunta3'] == '' ? $cliente['razonSocial'] : $formulario['pregunta3']; 
	$formulario['pregunta4'] = $formulario['pregunta4'] == '' ? $cliente['domicilio'] : $formulario['pregunta4']; 
	$formulario['pregunta5'] = $formulario['pregunta5'] == '' ? $cliente['localidad'] : $formulario['pregunta5']; 
	$formulario['pregunta6'] = $formulario['pregunta6'] == '' ? $cliente['telefono'] : $formulario['pregunta6']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['email'] : $formulario['pregunta12']; 
	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['cif'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['cp'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? convertirMinuscula($cliente['provincia']) : convertirMinuscula($formulario['pregunta11']); 
	$formulario['pregunta18'] = $formulario['pregunta18'] == '' ? $cliente['fax'] : $formulario['pregunta18']; 

	return $formulario;
}

function formularioAlargenos($i,$cliente,$servicio){
	campoOculto($servicio,'servicio'.$i);
	$trabajo = recogerTrabajo($cliente,$servicio);
	$formulario = recogerFormularioServicios($trabajo);
	$formulario = completarDatosGeneralesAlergenos($formulario,$cliente);

	abreColumnaCampos();
		/*echo "<a href='https://crmparapymes.com.es/cartas-alergenos/?idencrp=".md5($cliente)."' target='_blank' class='noAjax'>Enlace</a><br/>";
		
		$enlace = "https://crmparapymes.com.es/cartas-alergenos/?idencrp=".md5($cliente); 
		
		$PNG_TEMP_DIR = '../img/temp/';

		$PNG_WEB_DIR = '../img/temp/';

		if (!file_exists($PNG_TEMP_DIR))
    		mkdir($PNG_TEMP_DIR);

		$matrixPointSize = 10;
		$errorCorrectionLevel = 'L';

		$filename = $PNG_TEMP_DIR.'code'.md5($enlace.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';

		QRcode::png($enlace, $filename, $errorCorrectionLevel, $matrixPointSize, 2); 

		echo '<img src="'.$PNG_WEB_DIR.basename($filename).'" /><hr/>';  */

		campoTextoFormulario(1,$i,'Consultor',$formulario);
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS GENERALES</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,$i,'Razón Social',$formulario);
		campoTextoFormulario(4,$i,'Dirección Social',$formulario);
		campoTextoFormulario(10,$i,'Código postal',$formulario);
		campoTextoFormulario(8,$i,'Representate legal',$formulario);
		campoTextoFormulario(6,$i,'Teléfono',$formulario);
		campoTextoFormulario(7,$i,'Nº de trabajadores',$formulario);
		campoTextoFormulario(15,$i,'Nº de centros',$formulario);
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,$i,'CIF',$formulario);
		campoTextoFormulario(5,$i,'Localidad',$formulario);
		campoTextoFormulario(11,$i,'Provincia',$formulario);
		campoTextoFormulario(14,$i,'NIF',$formulario);
		campoTextoFormulario(12,$i,'Email',$formulario);
		campoTextoFormulario(13,$i,'Nombre comercial',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DESCRIPCIÓN DEL CENTRO (cada uno de los centros)</h3>";

	abreColumnaCampos();
		campoTextoFormulario(16,$i,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(17,$i,'Nº plantas',$formulario);
		campoTextoFormulario(18,$i,'Aforo máximo',$formulario);
		campoRadioFormulario(19,$i,'Tipo de cliente',$formulario,'FINAL',array('Ciente final','A domicilio','Otro establecimiento'),array('FINAL','DOMICILIO','OTRO'));
	cierraColumnaCampos();
	

	abreColumnaCampos();
		campoTextoFormulario(20,$i,'Superficie m²',$formulario);
		campoTextoFormulario(21,$i,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(22,$i,'Mismo nivel',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'><br/>";
	abreColumnaCampos();
		campoTextoFormulario(23,$i,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(24,$i,'Nº plantas',$formulario);
		campoTextoFormulario(25,$i,'Aforo máximo',$formulario);
		campoRadioFormulario(26,$i,'Tipo de cliente',$formulario,'FINAL',array('Ciente final','A domicilio','Otro establecimiento'),array('FINAL','DOMICILIO','OTRO'));
	cierraColumnaCampos();
	

	abreColumnaCampos();
		campoTextoFormulario(27,$i,'Superficie m²',$formulario);
		campoTextoFormulario(28,$i,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(29,$i,'Mismo nivel',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'><br/>";

	abreColumnaCampos();
		campoTextoFormulario(30,$i,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(31,$i,'Nº plantas',$formulario);
		campoTextoFormulario(32,$i,'Aforo máximo',$formulario);
		campoRadioFormulario(33,$i,'Tipo de cliente',$formulario,'FINAL',array('Ciente final','A domicilio','Otro establecimiento'),array('FINAL','DOMICILIO','OTRO'));
	cierraColumnaCampos();
	

	abreColumnaCampos();
		campoTextoFormulario(34,$i,'Superficie m²',$formulario);
		campoTextoFormulario(35,$i,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(36,$i,'Mismo nivel',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ACTIVIDAD Y ÁMBITO DE COMERCIALIZACIÓN DEL ESTABLECIMIENTO</h3>";

	abreColumnaCampos('span5 centro');
		echo "Minoristas";
	cierraColumnaCampos();

	abreColumnaCampos('span5 centro');
		echo "Restauración";
	cierraColumnaCampos();
	echo "<br/><br/>";
	abreColumnaCampos('span2');
		campoRadioFormulario(37,$i,'Carnicería',$formulario);
		campoRadioFormulario(38,$i,'Sucursal de carne',$formulario);
		campoRadioFormulario(39,$i,'Volatería/Recova',$formulario);
		campoRadioFormulario(40,$i,'Pescadería',$formulario);
		campoRadioFormulario(41,$i,'Freiduría de pescado',$formulario);
		campoRadioFormulario(42,$i,'Freiduría de verduras/patatas',$formulario);
		campoRadioFormulario(43,$i,'Frutería',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span2');
		campoRadioFormulario(44,$i,'Panadería (sin obrador)',$formulario);
		campoRadioFormulario(45,$i,'Pastelería (sin obrador)',$formulario);
		campoRadioFormulario(46,$i,'Horneado de pan',$formulario);
		campoRadioFormulario(47,$i,'Pastelería',$formulario);
		campoRadioFormulario(48,$i,'Heladería',$formulario);
		campoRadioFormulario(49,$i,'Minorista polivalente (supermercado)',$formulario);
		campoRadioFormulario(50,$i,'Otros',$formulario);
		echo "<div id='div".$i."50' class='hidden'>";
			campoTextoFormulario(51,$i,'Especificar',$formulario,'input-small');
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos('span2');
		campoRadioFormulario(52,$i,'Bar',$formulario);
		campoRadioFormulario(53,$i,'Cafetería',$formulario);
		campoRadioFormulario(54,$i,'Restaurante',$formulario);
		campoRadioFormulario(55,$i,'Establecimiento de temporada',$formulario);
		campoRadioFormulario(56,$i,'Establecimiento no permanente',$formulario);
		campoRadioFormulario(57,$i,'Hotel o establecimiento de servicios de comidas que tenga capacidad o sirva menos de 200 comidas/día (incluido salón de celebraciones sin cocina propia)',$formulario);
		campoRadioFormulario(58,$i,'Take away. Tipo de vehículo',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos('span2');
		campoRadioFormulario(59,$i,'Asador de pollo',$formulario);
		campoRadioFormulario(60,$i,'Pizzería',$formulario);
		campoRadioFormulario(61,$i,'Hamburguesería',$formulario);
		campoRadioFormulario(62,$i,'Venta de carretera',$formulario);
		campoRadioFormulario(63,$i,'Catering',$formulario);
		campoRadioFormulario(64,$i,'Otros',$formulario);
		echo "<div id='div".$i."64' class='hidden'>";
			campoTextoFormulario(65,$i,'Especificar',$formulario,'input-small');
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ACTIVIDAD Y ÁMBITO DE ACTUACIÓN DEL ESTABLECIMIENTO</h3>";


	abreColumnaCampos('span5');
		campoRadioFormulario(66,$i,'En función del producto',$formulario,'ELABORACION',array('Elaboración del producto','Venta de productos','Ambos','Otros'),array('ELABORACION','VENTA','AMBOS','OTROS'),true);
		campoTextoFormulario(67,$i,'Especificar otros',$formulario,'input-small');
	cierraColumnaCampos();

	abreColumnaCampos('span5');
		campoRadioFormulario(68,$i,'En función del ámbito geográfico',$formulario,'LOCAL',array('Local','Provincial','Autonómico','Nacional'),array('LOCAL','PROVINCIAL','AUTONOMICO','NACIONAL'),true);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre del plato' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaPlatos'>
                  	<thead>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Platos </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$platos = consultaBD("SELECT * FROM platos_alergenos WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($plato = mysql_fetch_assoc($platos)){
        		echo "<tr class='trPlatos' id='fila".$indice."'><td>";
    				campoTexto('nombrePlato'.$indice,'Nombre del plato',$plato['nombrePlato']);
    				areaTexto('ingredientesPlato'.$indice,'Ingredientes',$plato['ingredientesPlato'],'areaInforme');
    				creaTablaAlergenos($indice,$plato['codigo']);
    				
    				//creaListadoCheck($indice,$plato);
				echo "<br/><br/><button type='button' id='duplicaPlato' class='btn btn-small btn-info' onclick='insertaFilaPlatos(\"tablaPlatos\",\"fila".$indice."\");'><i class='icon-files-o'></i> Duplicar Plato</button> 
				</td></tr>";
        		$indice++;
        	}
        }
    	for($indice;$indice<10;$indice++){
    		echo "<tr class='trPlatos' id='fila".$indice."'><td>";
    			campoTexto('nombrePlato'.$indice,'Nombre del plato');
    			areaTexto('ingredientesPlato'.$indice,'Ingredientes','','areaInforme');
    			creaTablaAlergenos($indice);
    			//creaListadoCheck($indice);
    		echo "<br/><br/><button type='button' id='duplicaPlato' class='btn btn-small btn-info' onclick='insertaFilaPlatos(\"tablaPlatos\",\"fila".$indice."\");'><i class='icon-files-o'></i> Duplicar Plato</button> 
				</td></tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFilaPlatos(\"tablaPlatos\");'><i class='icon-plus'></i> Añadir Plato</button> 
	            </center>
				<br>
		 	";	

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(69,$i,'Comentarios de comercial',$formulario,'areaInforme');	

}

function completarDatosLSSI($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['razonSocial'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['domicilio'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? $cliente['localidad'] : $formulario['pregunta11']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['actividad'] : $formulario['pregunta12'];
	$formulario['pregunta14'] = $formulario['pregunta14'] == '' ? $cliente['cif'] : $formulario['pregunta14']; 
	$formulario['pregunta15'] = $formulario['pregunta15'] == '' ? $cliente['email'] : $formulario['pregunta15']; 
	$formulario['pregunta16'] = $formulario['pregunta16'] == '' ? $cliente['cp'] : $formulario['pregunta16'];  
	$formulario['pregunta17'] = $formulario['pregunta17'] == '' ? $cliente['telefono'] : $formulario['pregunta17']; 

	return $formulario;
}

function formularioLSSI($i,$cliente,$servicio){
	campoOculto($servicio,'servicio'.$i);
	$trabajo = recogerTrabajo($cliente,$servicio);
	$formulario = recogerFormularioServicios($trabajo);
	$formulario = completarDatosLSSI($formulario,$cliente);

	abreColumnaCampos();
		campoTextoFormulario(1,$i,'Consultor',$formulario);
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>¿Tienen inscrito el nombre de dominio, marca y/o nombre comercial?</span><br/><br/>";
	campoRadioFormulario(3,$i,'Dominio',$formulario);
	echo "<div id='div".$i."3' class='hidden'>";
		campoTextoFormulario(4,$i,'Dominio',$formulario);
	echo "</div>";
	campoRadioFormulario(5,$i,'Marca',$formulario);
	echo "<div id='div".$i."5' class='hidden'>";
		campoTextoFormulario(6,$i,'Marca',$formulario);
	echo "</div>";
	campoRadioFormulario(7,$i,'Nombre comercial',$formulario);
	echo "<div id='div".$i."7' class='hidden'>";
		campoTextoFormulario(8,$i,'Nombre comercial',$formulario);
	echo "</div>";

	echo "<br clear='all'><span class='subtitulo'>Datos</span><br/><br/>";
	abreColumnaCampos();
		campoTextoFormulario(9,$i,'Denominación Social',$formulario);
		campoTextoFormulario(10,$i,'Dirección Social',$formulario);
		campoTextoFormulario(11,$i,'Localidad',$formulario);
		campoTextoFormulario(12,$i,'Actividad empresarial',$formulario);
		campoTextoFormulario(13,$i,'Página web',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(14,$i,'NIF',$formulario);
		campoTextoFormulario(15,$i,'E-mail',$formulario);
		campoTextoFormulario(16,$i,'Código postal',$formulario);
		campoTextoFormulario(17,$i,'Teléfono',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3></h3><br/>";
	abreColumnaCampos();
		areaTextoFormulario(18,$i,'¿Tiene una S.L., S.A. u otro tipo de sociedad?, en tal caso datos de su inscripción en el Registro Mercantil. (Tomo, Libro, Folio, Sección, Hoja)',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		areaTextoFormulario(19,$i,'En el caso de que su actividad estuviese sujeta a un régimen de autorización administrativa previa, los datos relativos a dicha autorización y los identificativos del órgano competente encargado de su supervisión',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>***Si ejerce una profesión regulada deberá indicar:</span><br/><br/>";
	abreColumnaCampos();
		campoTextoFormulario(20,$i,'Datos del Colegio profesional al que, en su caso, pertenezca y número de colegiado',$formulario);
		
		campoTextoFormulario(24,$i,'Normas profesionales aplicables al ejercicio de su profesión y los medios a través de los cuales se pueden conocer, incluidos los electrónicos',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(23,$i,'Título académico oficial o profesional que cuente',$formulario);
		campoTextoFormulario(21,$i,'Estado de la Unión Europea o del Espacio Económico Europeo en el que se expidió dicho título y, en su caso, la correspondiente homologación o reconocimiento',$formulario);
		
	cierraColumnaCampos();

	echo "<br clear='all'><h3></h3><br/><br/><br/>";
	abreColumnaCampos();
		campoTextoFormulario(22,$i,'Número de identificación fiscal que le corresponda',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(25,$i,'Códigos de conducta a los que, en su caso, esté adherido y la manera de consultarlos electrónicamente',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'>PUBLICIDAD</h3>";
	abreColumnaCampos();
		campoRadioFormulario(26,$i,'¿Envía publicidad a través de correo electrónico?',$formulario);
		campoRadioFormulario(27,$i,'¿La página web dispone de cookies?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(28,$i,'¿Recaba datos a través de formularios de contacto?',$formulario);
		campoRadioFormulario(29,$i,'¿Los datos recabados se utilizan para enviar publicidad por email?',$formulario);
		echo "<div id='div".$i."29' class='hidden'>";
			campoTextoFormulario(30,$i,'Indica el correo electrónico disponible para darse de baja y para el ejercicio de los derechos Arco',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'>REDES SOCIALES</h3>";
	abreColumnaCampos();
		campoRadioFormulario(31,$i,'¿La empresa dispone de perfiles de redes sociales?',$formulario);
		campoTextoFormulario(32,$i,'En caso afirmativo, especificar cuáles',$formulario);
		campoTextoFormulario(33,$i,'En caso negativo, ¿Le gustaría recibir formación de redes sociales?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(34,$i,'¿Dispone de newsletter/boletín informativo?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'>PRESTADOR DE SERVICIOS WEB</h3>";
	abreColumnaCampos();
		campoRadioFormulario(35,$i,'¿Dispone de empresa que realice el mantenimiento de la página web y trate datos?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(36,$i,'¿Ha formalizado el correspondiente contrato de prestación de servicios?',$formulario);
		echo "<div id='div".$i."36' class='hidden'>";
			campoTextoFormulario(37,$i,'Denominación',$formulario);
			campoTextoFormulario(38,$i,'Dirección social',$formulario);
			campoTextoFormulario(39,$i,'NIF',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'>CHECK LIST PRODUCTOS/BIENES - CGC</h3>";
	echo "*A rellenar sólo por empresas que vendan productos";
	echo "<br clear='all'><span class='subtitulo'>I. Precio de compra</span><br/><br/>";
	abreColumnaCampos();
		campoRadioFormulario(40,$i,'¿En la página web aparece un listado con los precios de los productos?',$formulario);
		campoRadioFormulario(41,$i,'¿El precio se especifica sólo en euros?',$formulario);
		echo "<div id='div".$i."41' class='hidden' metodo='inverso'>";
			campoTextoFormulario(42,$i,'¿En qué otras monedas?',$formulario);
		echo "</div>";
		campoRadioFormulario(43,$i,'¿El precio final se puede ver incrementado por otros costes tales como gastos envío? (En caso de marcar sí, pasar al bloque II. En caso de marcar No, pasar al bloque III)',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(44,$i,'¿El precio tiene el IVA incluido (debe mostrarse en la web con IVA)?',$formulario);
		campoRadioFormulario(45,$i,'¿Hay ofertas promocionales?',$formulario);
		echo "<div id='div".$i."45' class='hidden'>";
			campoTextoFormulario(46,$i,'¿De qué servicios?',$formulario);
			campoTextoFormulario(47,$i,'¿Cuánto tiempo dura la promoción?',$formulario);
			campoRadioFormulario(48,$i,'¿Está limitada a determinadas unidades totales de venta?',$formulario);
			echo "<div id='div".$i."48' class='hidden'>";
				campoTextoFormulario(49,$i,'¿A cuántas?',$formulario);
			echo "</div>";
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>II. Otros gastos</span><br/><br/>";
	abreColumnaCampos();
		campoTextoFormulario(50,$i,'¿Cuáles son los costes de envío?',$formulario);
		campoRadioFormulario(51,$i,'¿Se hará cargo el comprador de los gastos de envío? Y ¿de los gastos adicionales?',$formulario);
		campoRadioFormulario(52,$i,'¿Los gastos de envío van en función del peso del producto?',$formulario);
		campoRadioFormulario(53,$i,'¿Puede el cliente recoger el producto directamente en la tienda?',$formulario);
		echo "<div id='div".$i."53' class='hidden'>";
			campoRadioFormulario(54,$i,'¿Supone esto alguna rebaja en el precio?',$formulario);
		echo "</div>";
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(55,$i,'¿Cuáles son los costes adicionales del servicio?',$formulario);
		campoRadioFormulario(56,$i,'¿Los gastos de envío son fijos?',$formulario);
		campoRadioFormulario(57,$i,'¿Se usan medios de entrega urgente?',$formulario);
		echo "<div id='div".$i."57' class='hidden'>";
			campoTextoFormulario(58,$i,'¿Qué costes tiene la entrega?',$formulario);
			campoTextoFormulario(59,$i,'¿Por qué medio o compañía se va a realizar?',$formulario);
			campoTextoFormulario(60,$i,'¿Quién se hará responsable en caso de retraso injustificado?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>III. Forma de pago</span><br/><br/>";
	abreColumnaCampos();
		echo '¿Se aceptan las siguientes modalidades de pago?';
		campoRadioFormulario(61,$i,'Transferencia bancaria',$formulario);
		campoRadioFormulario(62,$i,'Ingreso en cuenta',$formulario);
		campoRadioFormulario(63,$i,'TPV Virtual (pasarela de pago para tarjetas de crédito)',$formulario);
		campoRadioFormulario(64,$i,'Paypal',$formulario);
		campoRadioFormulario(65,$i,'Bitcoins',$formulario);
		campoRadioFormulario(66,$i,'Otros medios',$formulario);
		echo "<div id='div".$i."66' class='hidden'>";
			campoTextoFormulario(67,$i,'Especificar cuales',$formulario);
		echo "</div>";
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(68,$i,'¿Se puede fraccionar el pago?',$formulario);
		echo "<div id='div".$i."68' class='hidden'>";
			campoTextoFormulario(69,$i,'¿A partir de qué cantidad se podrá fraccionar?',$formulario);
			campoTextoFormulario(70,$i,'¿En cuantos plazos?',$formulario);
			campoRadioFormulario(71,$i,'¿Se incrementará el precio del servicio?',$formulario);
			echo "<div id='div".$i."71' class='hidden'>";
				campoTextoFormulario(72,$i,'¿En cuánto?',$formulario);
			echo "</div>";
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>IV. Forma y plazos de entrega</span><br/><br/>";
	abreColumnaCampos();
		campoRadioFormulario(73,$i,'¿Se envía al domicilio del cliente el bien o servicio?',$formulario);
		echo "<div id='div".$i."73' class='hidden'>";
			campoTextoFormulario(74,$i,'¿Cuál es el plazo normal de entrega?',$formulario);
		echo "</div>";
		campoRadioFormulario(75,$i,'¿Se otorga el número de seguimiento del producto?',$formulario);
		campoRadioFormulario(76,$i,'¿Se entrega a toda España?',$formulario);
		campoRadioFormulario(77,$i,'¿Se envía a cualquier país de la Unión Europea?',$formulario);
		campoTextoFormulario(78,$i,'¿Qué costos tienen los envíos internacionales?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(79,$i,'¿Se envía de manera urgente?',$formulario);
		echo "<div id='div".$i."79' class='hidden'>";
			campoTextoFormulario(80,$i,'¿Cuál es el plazo de envío urgente?',$formulario);
		echo "</div>";
		campoTextoFormulario(81,$i,'¿Quién es el responsable en caso de retraso injustificado?',$formulario);
		campoRadioFormulario(82,$i,'¿Tienen costes adicionales entregarlo en Canarias o Baleares?',$formulario);
		campoRadioFormulario(83,$i,'¿Se envía a los paises no miembros de la Unión Europea?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>V. Características básicas del bien o servicio</span><br/><br/>";
	abreColumnaCampos();
		campoRadioFormulario(84,$i,'¿Se garantiza la integridad del bien o servicio enviado?',$formulario);
		campoTextoFormulario(85,$i,'¿Qué productos/servicios pueden ofrecer a través de la web?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(86,$i,'¿Hay una definición previa, concreta y pormenorizada del bien o servicio?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>VI. Derecho de desestimiento y devolución</span><br/><br/>";
	abreColumnaCampos();
		campoRadioFormulario(87,$i,'¿Se concede un plazo de desistimiento o devolución superior al legalmente establecido (14 días)?',$formulario);
		echo "<div id='div".$i."87' class='hidden'>";
			campoTextoFormulario(88,$i,'¿Qué plazo?',$formulario);
		echo "</div>";
		campoRadioFormulario(89,$i,'¿Se le envía al cliente el modelo para ejercer su derecho de desistimiento (es obligación legal)?',$formulario);
		echo "<div id='div".$i."89' class='hidden'>";
			campoRadioFormulario(90,$i,'¿Se le envía al momento de contratación?',$formulario);
			campoRadioFormulario(91,$i,'¿Se le envía al momento de recepción del bien o servicio?',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(94,$i,'¿Aparte de las causas o motivos legales de devolución o desistimiento se establecen otras?',$formulario);
		echo "<div id='div".$i."94' class='hidden'>";
			campoTextoFormulario(95,$i,'¿Cuáles?',$formulario);
		echo "</div>";
		campoRadioFormulario(96,$i,'En caso de devolución, ¿El producto lo debe recoger la empresa?',$formulario);
		campoRadioFormulario(92,$i,'En caso de devolución, ¿El producto lo debe devolver el cliente?',$formulario);
		campoRadioFormulario(97,$i,'¿El coste de la devolución lo asume la empresa?',$formulario);
		campoRadioFormulario(93,$i,'¿El coste de la devolución lo asume el cliente?',$formulario);
		campoRadioFormulario(98,$i,'*La devolución o desistimiento no pueden tener costes añadidos para el cliente',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>VII. Subsanación errores y contratación</span><br/><br/>";
	abreColumnaCampos();
		campoRadioFormulario(99,$i,'¿El prestador va a archivar el documento electrónico en que se formalice el contrato y éste va a ser accesible?',$formulario);
		campoTextoFormulario(100,$i,'¿Qué medios técnicos pone a disposición del cliente para identificar y corregir errores en la introducción de los datos?',$formulario);
		campoRadioFormulario(101,$i,'La contratación será telemática. El cliente deberá facilitar los datos que se le requieran en el formulario habilitado al efecto. ¿Previamente a la contratación del servicio, el cliente habrá prestado su consentimiento a través de estas condiciones generales de contratación?',$formulario);
		campoRadioFormulario(102,$i,'¿Es necesario registrarse como usuario para poder adquirir los productos y/o servicios o para realizar la reserva online?',$formulario);
		campoRadioFormulario(103,$i,'Zonas o países de destinos de envío: Baleares',$formulario);
		campoRadioFormulario(104,$i,'Zonas o países de destinos de envío: Europa',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(105,$i,'¿Dispone de factura electrónica?',$formulario);
		campoTextoFormulario(106,$i,'¿En qué idiomas o lenguas podrá formalizarse el contrato?',$formulario);
		campoRadioFormulario(107,$i,'¿La compra o reserva del producto y/o servicio ofrecido al usuario, se realiza a través de la propia página web y no es redirigido a la web de un tercero?',$formulario);
		echo "<div id='div".$i."107' class='hidden'>";
			campoTextoFormulario(108,$i,'¿A qué dominio es redirigido el usuario?',$formulario);
		echo "</div>";
		campoRadioFormulario(109,$i,'Zonas o países de destinos de envío: Península',$formulario);
		campoRadioFormulario(110,$i,'Zonas o países de destinos de envío: Canarias',$formulario);
		campoRadioFormulario(111,$i,'Zonas o países de destinos de envío: Mundial',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'>CHECK LIST SERVICIOS</h3>";
	echo "*A rellenar sólo por empresas que vendan servicios";
	echo "<br clear='all'><span class='subtitulo'>I. Precio de compra</span><br/><br/>";

	abreColumnaCampos();
		campoTextoFormulario(112,$i,'Determinar el objeto del servicio',$formulario);
		campoRadioFormulario(113,$i,'¿En la página web aparece un listado con los precios de los productos?',$formulario);
		campoRadioFormulario(114,$i,'¿El precio se especifica sólo en euros?',$formulario);
		echo "<div id='div".$i."114' class='hidden' metodo='inverso'>";
			campoTextoFormulario(115,$i,'¿En qué otras monedas?',$formulario);
		echo "</div>";
		campoRadioFormulario(116,$i,'¿Se incluyen en los mismos los impuestos indirectos aplicables?',$formulario);
		campoRadioFormulario(117,$i,'¿Los precios ofertados son sólo aplicables a la contratación desde la propia página?',$formulario);
		campoTextoFormulario(118,$i,'¿La compra o reserva del producto y/o servicio ofrecido al usuario, se realiza a través de la propia página web o es redirigido a la web de un tercero?',$formulario);
		campoRadioFormulario(119,$i,'¿Es necesario registrarse como usuario para poder adquirir los productos y/o servicios o para realizar una reserva online?',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(120,$i,'¿El precio tiene el IVA incluido (debe mostrarse en la web con IVA)?',$formulario);
		campoRadioFormulario(121,$i,'¿Hay ofertas promocionales?',$formulario);
		echo "<div id='div".$i."121' class='hidden'>";
			campoTextoFormulario(122,$i,'¿De qué servicios?',$formulario);
			campoTextoFormulario(123,$i,'¿Cuánto tiempo dura la promoción?',$formulario);
		echo "</div>";
		campoRadioFormulario(124,$i,'¿La empresa se reserva el derecho de modificar en cada momento, y de forma unilateral, el precio de los servicios ofertados?',$formulario);
		campoTextoFormulario(125,$i,'¿A qué dominio es redirigido el usuario?',$formulario);
		campoRadioFormulario(126,$i,'**En cualquier caso el precio del servicio no incluye precios, tasas, gravámenes o cualquier otro tipo de coste adicional que haya que liquidar ante entes, órganos, organismos o instancias administrativas, jurisdiccionales o aquellas otras que pudieran corresponder. Todos los gastos y suplidos ajenos a la actividad propia de empresa jurídica son de cuenta exclusiva del CLIENTE y deberán ser abonados directamente por el CLIENTE, y previamente al pago de los mismos',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><span class='subtitulo'>II. Forma de pago</span><br/><br/>";
	abreColumnaCampos();
		echo '¿Se aceptan las siguientes modalidades de pago?';
		campoRadioFormulario(127,$i,'Transferencia bancaria',$formulario);
		campoRadioFormulario(128,$i,'Ingreso en cuenta',$formulario);
		campoRadioFormulario(129,$i,'TPV Virtual (pasarela de pago para tarjetas de crédito)',$formulario);
		campoRadioFormulario(130,$i,'Paypal',$formulario);
		campoRadioFormulario(131,$i,'Bitcoins',$formulario);
		campoRadioFormulario(132,$i,'Otros medios',$formulario);
		echo "<div id='div".$i."132' class='hidden'>";
			campoTextoFormulario(133,$i,'Especificar cuales',$formulario);
		echo "</div>";
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(134,$i,'¿Se puede fraccionar el pago?',$formulario);
		echo "<div id='div".$i."134' class='hidden'>";
			campoTextoFormulario(135,$i,'¿A partir de qué cantidad se podrá fraccionar?',$formulario);
			campoTextoFormulario(136,$i,'¿En cuantos plazos?',$formulario);
			campoRadioFormulario(137,$i,'¿Se incrementará el precio del servicio?',$formulario);
			echo "<div id='div".$i."137' class='hidden'>";
				campoTextoFormulario(138,$i,'¿En cuánto?',$formulario);
			echo "</div>";
		echo "</div>";
		campoRadioFormulario(139,$i,'Factura',$formulario);
		echo "<div id='div".$i."139' class='hidden'>";
			campoRadioFormulario(140,$i,'Electrónica/telemática',$formulario);
			campoRadioFormulario(141,$i,'En papel',$formulario);
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(142,$i,'Comentarios de comercial',$formulario,'areaInforme');
}

function completarDatosGeneralesCompliance($formulario,$cliente){
	$cliente = datosRegistro('clientes',$cliente);

	/*$formulario['pregunta3'] = $formulario['pregunta3'] == '' ? $cliente['razonSocial'] : $formulario['pregunta3']; 
	$formulario['pregunta4'] = $formulario['pregunta4'] == '' ? $cliente['domicilio'] : $formulario['pregunta4']; 
	$formulario['pregunta5'] = $formulario['pregunta5'] == '' ? $cliente['localidad'] : $formulario['pregunta5']; 
	$formulario['pregunta6'] = $formulario['pregunta6'] == '' ? $cliente['telefono'] : $formulario['pregunta6']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['email'] : $formulario['pregunta12']; 
	$formulario['pregunta8'] = $formulario['pregunta8'] == '' ? $cliente['administrador'] : $formulario['pregunta8']; 
	$formulario['pregunta9'] = $formulario['pregunta9'] == '' ? $cliente['cif'] : $formulario['pregunta9']; 
	$formulario['pregunta10'] = $formulario['pregunta10'] == '' ? $cliente['cp'] : $formulario['pregunta10']; 
	$formulario['pregunta11'] = $formulario['pregunta11'] == '' ? $cliente['provincia'] : $formulario['pregunta11']; 
	$formulario['pregunta12'] = $formulario['pregunta12'] == '' ? $cliente['fax'] : $formulario['pregunta12']; 
	$formulario['pregunta13'] = $formulario['pregunta13'] == '' ? $cliente['actividad'] : $formulario['pregunta13']; 
	$formulario['pregunta14'] = $formulario['pregunta14'] == '' ? $cliente['nifAdministrador'] : $formulario['pregunta14']; */

	return $formulario;
}

function formularioCompliance($i,$cliente,$servicio){
	campoOculto($servicio,'servicio'.$i);
	$trabajo = recogerTrabajo($cliente,$servicio);
	$formulario = recogerFormularioServicios($trabajo);
	$formulario = completarDatosGeneralesCompliance($formulario,$cliente);

	abreColumnaCampos();
		campoTextoFormulario(1,$i,'Consultor',$formulario);
		campoRadio('toma_datos'.$i,'Toma de datos finalizada',$trabajo['toma_datos']);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFechaFormulario(2,$i,'Fecha',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DATOS GENERALES</h3>";

	abreColumnaCampos();
		campoTextoFormulario(3,$i,'Razón Social',$formulario);
		campoTextoFormulario(4,$i,'Dirección Social',$formulario);
		campoTextoFormulario(10,$i,'Código postal',$formulario);
		campoTextoFormulario(8,$i,'Representate legal',$formulario);
		campoTextoFormulario(6,$i,'Teléfono',$formulario);
		campoTextoFormulario(7,$i,'Nº de trabajadores',$formulario);
		campoRadioFormulario(16,$i,'¿Hay trabajadores en centros ajenos?',$formulario);
		campoTextoFormulario(67,$i,'Sector',$formulario);
		campoTextoFormulario(68,$i,'Persona de contacto',$formulario);
		
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(9,$i,'CIF',$formulario);
		campoTextoFormulario(5,$i,'Localidad',$formulario);
		campoTextoFormulario(11,$i,'Provincia',$formulario);
		campoTextoFormulario(14,$i,'NIF',$formulario);
		campoTextoFormulario(12,$i,'Email',$formulario);
		campoTextoFormulario(15,$i,'Nº de centros',$formulario);
		campoTextoFormulario(21,$i,'Nombres de los centros',$formulario);
		echo "<br/>";
		campoTextoFormulario(13,$i,'Actividad empresarial',$formulario);
		campoTextoFormulario(69,$i,'Responsable del Plan de Prevención',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> DESCRIPCIÓN DEL CENTRO</h3>";

	abreColumnaCampos();
		campoTextoFormulario(17,$i,'Localización (zona industrial, comercial, residencial)',$formulario);
		campoTextoFormulario(19,$i,'Nº de plantas',$formulario);
		
		
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTextoFormulario(18,$i,'Superficie m2',$formulario);
		echo "<br/>";
		campoTextoFormulario(20,$i,'Tipo de plantas (bajos, primera planta, ático)',$formulario);
		campoRadioFormulario(22,$i,'¿Mismo nivel?',$formulario);
	cierraColumnaCampos();

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Nombre y apellidos' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaEmpleadosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='6'>Trabajadores</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Nombre y apellidos </th>
							<th> DNI </th>
							<th> Puesto de trabajo </th>
							<th> Descripción funciones </th>
							<th> ¿Salen fuera del centro? </th>
							<th> En caso afirmativo ¿Tipo de vehículo? </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$empleados = consultaBD("SELECT * FROM empleados_prl WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($empleado = mysql_fetch_assoc($empleados)){
        		echo "<tr>";
    				campoTextoTabla('nombreEmpleadoPRL'.$indice,$empleado['nombre']);
    				campoTextoTabla('dniEmpleadoPRL'.$indice,$empleado['dni'],'input-small');
    				campoTextoTabla('puestoEmpleadoPRL'.$indice,$empleado['puesto'],'input-small');
    				areaTextoTabla('funcionesEmpleadoPRL'.$indice,$empleado['funciones']);
    				campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),$empleado['salen'],'selectpicker span2 show-tick','data-live-search="true"',1);
    				campoTextoTabla('vehiculoEmpleadoPRL'.$indice,$empleado['vehiculo'],'input-small');
				echo"
					</tr>";
        		$indice++;
        	}
        }
        $maximo=$indice+2;
    	for($indice;$indice<$maximo;$indice++){
    		echo "<tr>";
    			campoTextoTabla('nombreEmpleadoPRL'.$indice);
    			campoTextoTabla('dniEmpleadoPRL'.$indice,'','input-small');
    			campoTextoTabla('puestoEmpleadoPRL'.$indice,'','input-small');
    			areaTextoTabla('funcionesEmpleadoPRL'.$indice);
    			campoSelect('salenEmpleadoPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
    			campoTextoTabla('vehiculoEmpleadoPRL'.$indice,'','input-small');
			echo"
				</tr>";
    	}	
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaEmpleadosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo"	<br clear='all'><br/>
			Para eliminar deja el campo 'Centro de trabajo' vacío
	 		<center>
	 		<div class='table-responsive'>
				<table class='table' id='tablaCentrosPRL'>
                  	<thead>
                  		<tr class='apartadoTablaFormularioEvaluacion'>
                  			<th colspan='5'>Centros de trabajo</th>
                  		</tr>
                    	<tr class='apartadoTablaFormularioEvaluacion'>
		                    <th> Centro de trabajo </th>
							<th> Nº delegados<br/>de prevención </th>
							<th> Comite de<br/>seguridad y salud </th>
							<th> Trabajador designado </th>
							<th> Consulta directa <br/>a los trabajadores </th>
                    	</tr>
                  	</thead>
                  	<tbody>";
        $indice=0;
        if($trabajo){
        	$centros = consultaBD("SELECT * FROM centros_prl WHERE codigoTrabajo=".$trabajo['codigo'],true);
        	while($centro = mysql_fetch_assoc($centros)){
        		echo "<tr>";
    				campoTextoTabla('nombreCentroPRL'.$indice,$centro['nombre']);
    				campoTextoTabla('delegadoCentroPRL'.$indice,$centro['delegado'],'input-small');
    				campoTextoTabla('comiteCentroPRL'.$indice,$centro['comite']);
    				campoTextoTabla('trabajadorCentroPRL'.$indice,$centro['trabajador']);
    				campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),$centro['consulta'],'selectpicker span2 show-tick','data-live-search="true"',1);
				echo"
					</tr>";
        		$indice++;
        	}
        }

    		echo "<tr>";
    			campoTextoTabla('nombreCentroPRL'.$indice);
    			campoTextoTabla('delegadoCentroPRL'.$indice,'','input-small');
    			campoTextoTabla('comiteCentroPRL'.$indice);
    			campoTextoTabla('trabajadorCentroPRL'.$indice);
    			campoSelect('consultaCentroPRL'.$indice,'',array('Si','No'),array('SI','NO'),'','selectpicker span2 show-tick','data-live-search="true"',1);
			echo"
				</tr>";
    
    echo "
					</tbody>
	                </table>
	            </div>
				</center>
				<center>
	              <button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCentrosPRL\");'><i class='icon-plus'></i> Añadir</button> 
	            </center>
				<br>
		 	";

	echo "<br clear='all'><h3 class='apartadoFormulario'> INSTALACIONES</h3>";
	abreColumnaCampos();
		campoRadioFormulario(23,$i,'Dispone de instalación de acondicionamiento de aire (calefacción, aire)',$formulario);
		campoRadioFormulario(32,$i,'¿Dispone de cuadro electríco',$formulario);
		echo "<div id='div".$i."32' class='hidden'>";
			campoRadioFormulario(33,$i,'Está señalizado',$formulario);
			campoTextoFormulario(34,$i,'Ubicación',$formulario);
		echo "</div>";
		campoRadioFormulario(25,$i,'Dispone de instalación de extracción de humos (campana extractora)',$formulario);


	cierraColumnaCampos();

	abreColumnaCampos();
		echo "<div id='div".$i."23' class='hidden'>";
		campoRadioFormulario(24,$i,'¿Cuales?',$formulario,'CALEFACCION',array('Calefaccion','Aire'),array('CALEFACCION','AIRE'));
		echo "</div>";
		echo "<br/><br/>";
		campoRadioFormulario(26,$i,'Dispone de Montacargas/ascensores',$formulario);
		echo "<div id='div".$i."26' class='hidden'>";
			campoTextoFormulario(27,$i,'¿Cuantos?',$formulario);
			campoTextoFormulario(28,$i,'Empresa de mantenimiento',$formulario);
		echo "</div>";
		echo "Dispones de otras:";
		campoRadioFormulario(29,$i,'Aire',$formulario);
		campoRadioFormulario(30,$i,'Gas',$formulario);
		campoRadioFormulario(31,$i,'Frigos',$formulario);
	cierraColumnaCampos();
	echo "<br clear='all'><h3> Maquinaria en la empresa</h3><br/>";
	abreColumnaCampos('span4');
		campoRadioFormulario(75,$i,'Transpaleta manual',$formulario);
		campoRadioFormulario(76,$i,'Carretilla elevadora',$formulario);
		campoRadioFormulario(77,$i,'Grúa puente',$formulario);
		campoRadioFormulario(78,$i,'Escalera de mano',$formulario);
		campoRadioFormulario(79,$i,'Hostelería (cortadora fiambres, parrillas, hornos, etc.)',$formulario);
		campoRadioFormulario(80,$i,'Productos químicos',$formulario);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(81,$i,'Gatos eléctricos',$formulario);
		campoRadioFormulario(82,$i,'Elevadores',$formulario);
		campoRadioFormulario(83,$i,'Taladros',$formulario);
		campoRadioFormulario(84,$i,'Herramiento de mano',$formulario);
		campoRadioFormulario(85,$i,'Otros',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> ORGANIZACIÓN EMPRESA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(32,$i,'Dirección',$formulario,'INTERMEDIO',array('Mando intermedio','Trabajadores'),array('INTERMEDIO','TRABAJADORES'),true);
		campoRadioFormulario(35,$i,'Qué medio de comunicación se utiliza para la prevención',$formulario,'ORAL',array('Oral','Correo electrónico','Carta'),array('ORAL','CORREO','CARTA'),true);
		campoRadioFormulario(36,$i,'Qué recursos dispone para esta actividad:',$formulario,'OFICINA',array('Oficina','Equipos informáticos','Impresora','Otros'),array('OFICINA','EQUIPOS','IMPRESORA','OTROS'),true);
		campoTextoFormulario(71,$i,'Trabajador responsable de la seguridad',$formulario);
		campoTextoFormulario(72,$i,'Trabajador responsable de la ergonomía y psicología',$formulario);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(33,$i,'Tipo de prevención',$formulario,'EMPRESARIO',array('Empresario','Trabajador designado'),array('EMPRESARIO','TRABAJADOR'),true);
		campoRadioFormulario(66,$i,'Formación del responsable de prevención',$formulario,'BASICO',array('Básico','Medio','Superior'),array('BASICO','MEDIO','SUPERIOR'),true);
		campoRadioFormulario(70,$i,'Servicio de prevencion:',$formulario,'PROPIO',array('Propio','Mancomunado','Ajeno','Sistema Mixto'),array('PROPIO','MANCOMUNADO','AJENO','MIXTO'),true);
		campoTextoFormulario(73,$i,'Trabajador responsable de la higiene',$formulario);
		campoTextoFormulario(74,$i,'Trabajador responsable de la vigilancia de la salud',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> MEDIDAS DE EMERGENCIA</h3>";

	abreColumnaCampos();
		campoRadioFormulario(37,$i,'¿Alguien en la empresa dispone de formación en primeros auxilios?',$formulario);
		echo "<div id='div".$i."37' class='hidden'>";
			campoTextoFormulario(86,$i,'Nombre',$formulario);
			campoTextoFormulario(87,$i,'Apellidos',$formulario);
			campoTextoFormulario(88,$i,'DNI',$formulario);
		echo "</div>";
		echo "<br/>";
		campoTextoFormulario(38,$i,'Instalación de seguridad contraincendios perteneciente al edificio donde se ubica el local o la oficina',$formulario);
		campoTextoFormulario(41,$i,'Nº de Bies',$formulario);
		echo "<br/>";
		campoRadioFormulario(43,$i,'Sistema de detección automática de incendios',$formulario);
		campoRadioFormulario(45,$i,'Extintores',$formulario);
		echo "<div id='div".$i."45' class='hidden'>";
			echo "Tipos y carácteristicas:";
			campoRadioFormulario(47,$i,'ABC',$formulario);
			campoRadioFormulario(48,$i,'Espuma',$formulario);
			campoRadioFormularioConLogo(51,$i,'Señal de seguridad de cada extintor',$formulario,'iconExtintor.jpg');
		echo "</div>";
		campoRadioFormulario(53,$i,'En todo caso existirá el alumbrado de emergencia en las puertas de salida',$formulario);
		campoRadioFormulario(55,$i,'Sistema de alarmas',$formulario,'AUTOMATICA',array('Alarma automática de incendios','Megafonía/telefonía','Alarma manual de incendios (con pulsador de alarma)','No dispone de sistema de alarmas'),array('AUTOMATICA','MEGAFONIA','MANUAL','NO'),true);

	cierraColumnaCampos();

	abreColumnaCampos();
		campoRadioFormulario(39,$i,'¿Alguien en la empresa dispone de formación en extinción de incendios?',$formulario);
		echo "<div id='div".$i."39' class='hidden'>";
			campoTextoFormulario(89,$i,'Nombre',$formulario);
			campoTextoFormulario(90,$i,'Apellidos',$formulario);
			campoTextoFormulario(91,$i,'DNI',$formulario);
		echo "</div>";
		campoTextoFormulario(40,$i,'Instalación de seguridad contraincendios formada por red de agua y Bies (Bocas de incendio equipadas)',$formulario);
		campoRadioFormularioConLogo(42,$i,'Señal de Seguridad de cada BIE',$formulario,'iconBie.png');
		campoRadioFormulario(44,$i,'Sistema de actuación automática de incendios (sprinkkles)',$formulario);
		echo "<div id='div".$i."45_1' class='hidden'>";
			campoTextoFormulario(46,$i,'Nº de Extintores',$formulario);
			echo "<br/>";
			campoRadioFormulario(49,$i,'CO2',$formulario);
			campoRadioFormulario(50,$i,'Otros',$formulario);
		echo "</div>";
		echo "<br/>";
		campoRadioFormulario(52,$i,'Alumbrado de emergencia',$formulario);
		campoRadioFormulario(54,$i,'Nº de lámparas alumbrado de emergencia',$formulario);
		echo "<div id='div".$i."26_1' class='hidden'>";
			campoRadioFormularioConLogo(56,$i,'Si dispone de ascensores/montacargas deberá contar con la preceptiva señal de seguridad',$formulario,'iconAscensor.png');
		echo "</div>";
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE EVACUACIÓN</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(57,$i,'Señal de salvamento. Recorrido de evacuación horizontal',$formulario,'iconHorizontal.jpg');
		campoRadioFormularioConLogo(58,$i,'Señal de salvamento. Recorrido de evacuación vertica',$formulario,'iconVertical.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(59,$i,'Señal de salvamento. Recorrido de evacuación',$formulario,'iconDireccion.png');
		campoRadioFormularioConLogo(60,$i,'Señal de salvamento. Teléfono de salvamento',$formulario,'iconTelefono.png');
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> RECURSOS TÉCNICOS EN CASO DE PRIMEROS AUXILIOS</h3>";
	abreColumnaCampos();
		campoRadioFormularioConLogo(61,$i,'Botiquín. En todo caso existirá en el centro de trabajo un botiquín de primeros auxilios junto con la señalización informativa correspondiente',$formulario,'iconBotiquin.png');
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormularioConLogo(62,$i,'Sistema lavaojos. Señal informativa',$formulario,'iconLavaojos.png');
		campoRadioFormulario(63,$i,'Otros',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> VIGILANCIA DE SALUD</h3>";
	abreColumnaCampos();
		campoRadioFormulario(64,$i,'¿Dispone usted de un servicio de Vigilancia de la Salud (reconocimientos médicos)?',$formulario);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoRadioFormulario(65,$i,'¿En caso negativo, querría que le pusiéramos en contacto con un colaborador nuestro?',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'> PUNTO DE ENCUENTRO</h3>";

	abreColumnaCampos();
		areaTextoFormulario(92,$i,'En caso de evacuación, indicar punto de encuentro/reunión para los trabajadores',$formulario);
	cierraColumnaCampos();

	echo "<br clear='all'><h3 class='apartadoFormulario'></h3>";
	areaTextoFormulario(93,$i,'Comentarios de comercial',$formulario,'areaInforme');
}

function compruebaFechaRevision($fechaRevision){
	$res=false;
	if($fechaRevision != ''){
		$fecha=explode('/',$fechaRevision);

		if($fecha[2]<date('Y')){
			$res=true;
		}
		elseif($fecha[2]==date('Y') && $fecha[1]<date('m')){
			$res=true;
		}
		elseif($fecha[2]==date('Y') && $fecha[1]==date('m') && $fecha[0]<date('d')){
			$res=true;
		}
	}

	return $res;
}

function creaTablaAlergenos($indice,$plato=false){
	if($plato){
		$listadoAlergenos=consultaBD("SELECT * FROM platos_alergenos_grupos WHERE codigoPlato=".$plato,true);
	}
	$i=0;
	echo "<div class='table-responsive'>
				<table class='table' id='tablaAlergenos".$indice."'>";
				if(isset($listadoAlergenos)){
				while($alergenos=mysql_fetch_assoc($listadoAlergenos)){
					echo "<tr>";
						campoSelectConsulta('grupoAlergeno'.$indice.'-'.$i,'Grupo','SELECT codigo, nombre AS texto FROM grupo_alergenos',$alergenos['codigoGrupo'],'selectpicker span3 show-tick grupoAlergenos','data-live-search="true"','',1);
						$consulta=consultaBD("SELECT * FROM alergenos",true);
						$nombres=array();
						$valores=array();
						$j=0;
						while($item=mysql_fetch_assoc($consulta)){
							$nombres[$j]=$item['nombre'];
							$valores[$j]=$item['codigo'];
							$j++;
						}
						campoSelectMultiple('alergeno'.$indice.'-'.$i,'',$nombres,$valores,$alergenos['alergenos'],'selectpicker span3 show-tick',"data-live-search='true'",$tipo=1);
					echo "</tr>";
					$i++;
				}
				}
				if($i==0){
					echo "<tr>";
						campoSelectConsulta('grupoAlergeno'.$indice.'-'.$i,'Grupo','SELECT codigo, nombre AS texto FROM grupo_alergenos','','selectpicker span3 show-tick grupoAlergenos','data-live-search="true"','',1);
						$consulta=consultaBD("SELECT * FROM alergenos",true);
						$nombres=array();
						$valores=array();
						$j=0;
						while($item=mysql_fetch_assoc($consulta)){
							$nombres[$j]=$item['nombre'];
							$valores[$j]=$item['codigo'];
							$j++;
						}
						campoSelectMultiple('alergeno'.$indice.'-'.$i,'',$nombres,$valores,'','selectpicker span3 show-tick',"data-live-search='true'",$tipo=1);
					echo "</tr>";
				}
	echo "		</table>
		</div>
		<button type='button' id='anadirAlergeno' class='btn btn-small btn-success' onclick='insertaAlergeno(\"tablaAlergenos".$indice."\");'><i class='icon-plus'></i> Añadir Alérgeno</button> ";

}

function creaListadoCheck($fila,$plato=false){
	$alergenos=array('Crustáceos','Cereales con gluten','Huevos','Pescado','Cacahuetes','Soja','Altramuz','Sulfitos','Sésamos','Lácteos','Moluscos','Mostaza','Frutos secos','Apio');
	$campos=array('crustaceos','cereales','huevos','pescado','cacahuetes','soja','altramuz','sulfitos','sesamos','lacteos','moluscos','mostaza','frutos','apio');
	echo "<div style='float:left;margin-right:30px;'>";
	for($i=0;$i<count($alergenos);$i++){
		if($i>0 && ($i%2==0)){
			echo "</div>";
			echo "<div style='float:left;margin-right:30px;'>";
		}
		campoCheckIndividual($campos[$i].$fila,$alergenos[$i],$plato[$campos[$i]]);
	}
	echo "</div>";
}

function filtroConsultorias(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(1,'Cliente');
	campoSelectConsulta(26,'Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activo='SI' AND (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') ORDER BY nombre");
	campoFecha(17,'Fecha prevista desde');
	campoSelectSiNoFiltro(5,'Toma de datos');
	campoFecha(9,'Fecha limite toma de datos desde');

	campoSelectSiNoFiltro(6,'Revisión de documentación');
	campoFecha(10,'Fecha limite revisión de documentación desde');

	campoSelectSiNoFiltro(19,'Emisión de documentación');
	campoFecha(19,'Fecha limite emisión de documentación desde');

	campoSelectSiNoFiltro(7,'Envío al cliente');
	campoFecha(11,'Fecha limite envío al cliente desde');

	campoSelectSiNoFiltro(22,'Entrega al cliente');
	campoFecha(21,'Fecha limite rntrega al cliente desde');

	campoSelectSiNoFiltro(8,'Mantenimiento');
	campoFecha(12,'Fecha limite mantenimiento desde');
	
	campoSelectSiNoFiltro(4,'Gratuita');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta(3,'Servicio','SELECT codigo, CONCAT(referencia," ",servicio) AS texto FROM servicios ORDER BY referencia');
	campoFecha(18,'Fecha prevista hasta');
	echo "<div style='opacity:0;'>";
		campoSelect(100,'Toma de datos',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(13,'Fecha limite toma de datos hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Revisión de documentación',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(14,'Fecha limite revisión de documentación hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Emisión de documentación',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(20,'Fecha limite emisión de documentación hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Envío al cliente',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(15,'Fecha limite envío al cliente hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Entrega al cliente',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(23,'Fecha limite entrega al cliente hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Mantenimiento',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(16,'Fecha limite mantenimiento hasta');
	
	campoSelectSiNoFiltro(24,'Con incidencia');
	

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function campoFicheroFormulario($numero,$indice,$texto,$tipo=0,$valor=false,$ruta=false,$nombreDescarga=''){
    campoFichero('pregunta'.$indice.$numero,$texto,$tipo,$valor,$ruta,$nombreDescarga);
}

function obtieneAlergenos(){
   $grupo=$_POST['grupo'];
   $res="<option value='NULL'> </option>";

   $consulta=consultaBD("SELECT codigo, nombre AS texto FROM alergenos WHERE grupo=".$grupo." ORDER BY nombre",true);
   if($consulta){
       while($datos=mysql_fetch_assoc($consulta)){
           $res.="<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
       }
   }
   else{
       $res='fallo';
   }

   echo $res;
} 


function creaBotonesGestionConsultorias(){
    echo '
    <a class="btn-floating btn-large btn-success btn-eliminacion" href="gestion.php?gratis" title="Nuevo registro"><i class="icon-plus"></i></a>';
}

function campoSelectProvinciaAEPD($nombreCampo,$datos){
	$valores=array();
	$nombres=array();
	$provinciasAgencia=array(''=>'','01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	
	foreach ($provinciasAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,convertirMinuscula($nombre));
	}

	campoSelect($nombreCampo,'Provincia',$nombres,$valores,$datos,'selectpicker span3 show-tick');
}

function seccionTratamientos($datos,$n,$f,$tipo='FIJO'){
	
	$listado=array('checkRecogida'=>'Recogida','checkModificacion'=>'Modificación','checkConsulta'=>'Consulta','checkComunicacion'=>'Comunicación','checkComunicacion1'=>'Comunicación por transmisión al responsable del fichero','checkRegistro'=>'Registro','checkConservacion'=>'Conservación','checkCotejo'=>'Cotejo','checkSupresion'=>'Supresión','checkComunicacion2'=>'Comunicación a la Administración Pública competente','checkEstructuracion'=>'Estructuración','checkInterconexion'=>'Interconexión','checkAnalisis'=>'Análisis de conducta','checkDestruccion'=>'Destrucción','checkComunicacion3'=>'Comunicación permitida por ley');
	$i=0;
	if($tipo=='FIJO'){
		$fijo='SI';
		$tratamientos=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$n.' AND codigoTrabajo='.$datos['codigo'].' AND fijo="'.$fijo.'";',true,true);
		echo '<div id="divTratamientos_'.$tipo.'_'.$f.'" class="divTratamientos"><h1>Tratamientos</h1><hr/>';
	} else {
		$fijo='NO';
		$tratamientos=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$f.' AND codigoTrabajo='.$datos['codigo'].' AND fijo="'.$fijo.'";',true,true);
		echo '<div id="divTratamientos_'.$tipo.'_'.$n.'" class="divTratamientos"><h1>Tratamientos</h1><hr/>';
	}
	$columna=0;
		foreach ($listado as $key => $value) {
			if($i%5==0){
				if($columna<2){
					abreColumnaCampos('span3 spanTratamientos');
				} else {
					abreColumnaCampos();
				}
			}
			if($tratamientos){
				$valor=$tratamientos[$key];
			} else {
				if(in_array($key,array('checkRecogida','checkModificacion','checkConsulta','checkComunicacion1','checkRegistro','checkConservacion','checkCotejo','checkSupresion','checkComunicacion2','checkComunicacion3','checkDestruccion'))){
					$valor='SI';
				} else {
					$valor='NO';
				}
			}
			campoCheckIndividual($key.'_'.$tipo.'_'.$n,$value,$valor);
			if(($i+1)%5==0){
				cierraColumnaCampos();
				$columna++;
			}
			$i++;
		}
	echo '</div>';
}

function creaTablaSubContratacion($datos,$tipo,$n,$j=0){
	$filasTabla='filasTabla[]';
	$div=$tipo=='Fijo'?'divSubcontratacion'.$n.$tipo:'divSubcontratacion'.$j.$tipo;
	$tabla=$tipo=='Fijo'?'tablaSubcontratacion'.$n.$tipo:'tablaSubcontratacion'.$j.$tipo;
	$campos=$tipo=='Fijo'?array('empresa'.$n.$tipo,'nif'.$n.$tipo,'direccion'.$n.$tipo,'cp'.$n.$tipo,'localidad'.$n.$tipo,'provincia'.$n.$tipo,'representante'.$n.$tipo,'servicio'.$n.$tipo):array('empresa'.$j.$tipo,'nif'.$j.$tipo,'direccion'.$j.$tipo,'cp'.$j.$tipo,'localidad'.$j.$tipo,'provincia'.$j.$tipo,'representante'.$j.$tipo,'servicio'.$j.$tipo);
	$tipo=$tipo=='Fijo'?'ENCARGADOFIJO':'ENCARGADO';
	echo "<br clear='all'>
	<div class='centro divSubcontrata' id='".$div."'>
		<br/><h1 style='text-align:left;'>Subcontratación</h1>
		<div class='table-responsive'>
			<table class='table table-striped tabla-simple tablaConSpan3' id='".$tabla."'>
			  	<thead>
			    	<tr>
			            <th> Empresa </th>
			            <th></th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;
			  		conexionBD();
			  		if($datos){
		  				$consulta=consultaBD("SELECT * FROM declaraciones_subcontrata WHERE codigoResponsable=".$n." AND tipo='".$tipo."' AND codigoTrabajo=".$datos['codigo'].";");
		  				while($item=mysql_fetch_assoc($consulta)){
		  					$j=$i+1;
		  					echo '<tr><td>';
		  					abreColumnaCampos();
		  						campoTexto($campos[0].'_'.$i,'Empresa',$item['empresa']);
		  						campoTexto($campos[1].'_'.$i,'CIF/NIF',$item['nif'],'input-small');
		  						campoTexto($campos[6].'_'.$i,'Representante legal',$item['representante']);
		  						campoTexto($campos[7].'_'.$i,'Servicio',$item['servicio']);
		  					cierraColumnaCampos();
		  					abreColumnaCampos();
		  						campoTexto($campos[2].'_'.$i,'Dirección',$item['direccion']);
		  						campoTexto($campos[3].'_'.$i,'CP',$item['cp'],'input-mini');
		  						campoTexto($campos[4].'_'.$i,'Localidad',$item['localidad']);
		  						campoTexto($campos[5].'_'.$i,'Provincia',$item['provincia']);
		  					cierraColumnaCampos(true);
		  					echo "</td><td class='centro'><input type='checkbox' name='".$filasTabla."' value='$j'></td></tr>";
		  					$i++;
		  				}
		  			}
			  		

			  		if($i==0){
			  			$j=$i+1;
			  			echo '<tr><td>';
		  					abreColumnaCampos();
		  						campoTexto($campos[0].'_'.$i,'Empresa');
		  						campoTexto($campos[1].'_'.$i,'CIF/NIF',false,'input-small');
		  						campoTexto($campos[6].'_'.$i,'Representante legal');
		  						campoTexto($campos[7].'_'.$i,'Servicio');
		  					cierraColumnaCampos();
		  					abreColumnaCampos();
		  						campoTexto($campos[2].'_'.$i,'Dirección');
		  						campoTexto($campos[3].'_'.$i,'CP',false,'input-mini');
		  						campoTexto($campos[4].'_'.$i,'Localidad');
		  						campoTexto($campos[5].'_'.$i,'Provincia');
		  					cierraColumnaCampos(true);
			  			echo "</td><td class='centro'><input type='checkbox' name='".$filasTabla."' value='$j'></td></tr>";
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFilaSubcontrata(\"".$tabla."\");'><i class='icon-plus'></i> Añadir empresa</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaSubcontrata(\"".$tabla."\");'><i class='icon-trash'></i> Eliminar empresa</button>
			</div>
		</div>
	</div><br /><br />";
}
//Fin parte de consultorías
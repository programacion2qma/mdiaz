<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesConcurrencias(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaConcurrencia();
	}
	elseif(isset($_POST['nombreConcurren'])){
		$res=insertaConcurrencia();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('concurren_lopd');
	}

	mensajeResultado('nombreConcurren',$res,'Concurrencia');
    mensajeResultado('elimina',$res,'Concurrencia', true);
}

function insertaConcurrencia(){
	$res=true;
	$res=insertaDatos('concurren_lopd');
	return $res;
}

function actualizaConcurrencia(){
	$res=true;
	$res=actualizaDatos('concurren_lopd');
	return $res;
}


function listadoConcurrencias(){
	global $_CONFIG;

	$columnas=array('nombreConcurren','activo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT concurren_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM concurren_lopd INNER JOIN trabajos ON concurren_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT concurren_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM concurren_lopd INNER JOIN trabajos ON concurren_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['nombreConcurren'],
			$datos['actividadConcurren'],
			formateaFechaWeb($datos['fechaConcurren']),
			formateaFechaWeb($datos['fechaFinConcurren']),
			formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'],
			creaBotonDetalles("zona-cliente-concurrencias/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionConcurrencias(){
	operacionesConcurrencias();

	abreVentanaGestion('Gestión de Concurrencias','?','','icon-edit','margenAb');
	$datos=compruebaDatos('concurren_lopd');

    if(!$datos){
    	abreColumnaCampos();
    		campoSelectConsulta('codigoTrabajo','Consultoría','SELECT trabajos.codigo, servicios.servicio AS texto FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE codigoCliente='.obtenerCodigoCliente(true),false,'selectpicker span5');
    	cierraColumnaCampos(true);
    }

	abreColumnaCampos();
		campoTexto('nombreConcurren','Empresa',$datos,'span5');
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoTexto('nifConcurren','CIF/NIF',$datos);
		campoFecha('fechaConcurren','Fecha inicio',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('actividadConcurren','Actividad',$datos);
		campoFecha('fechaFinConcurren','Fecha fin',$datos);
	cierraColumnaCampos();


	cierraVentanaGestion('index.php',true);
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
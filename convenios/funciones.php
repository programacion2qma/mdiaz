<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesConvenios(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('convenios');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('convenios');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('convenios');
	}

	mensajeResultado('nombre',$res,'Convenio');
    mensajeResultado('elimina',$res,'Convenio', true);
}


function listadoConvenios(){
	$columnas=array('nombre');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);//Uso de función personalizada
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre FROM convenios $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, nombre FROM convenios $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['nombre'],
			botonAcciones(array('Detalles'),array('convenios/gestion.php?codigo='.$datos['codigo']),array('icon-edit'),false,true,''),
			obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionConvenios(){
	operacionesConvenios();

	abreVentanaGestion('Gestión de Convenios','?','span3','icon-edit','margenAb');
	$datos=compruebaDatos('convenios');

	campoTexto('nombre','Convenio',$datos,'span8');

	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}

function filtroServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nombre','','span3');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de servicios
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de telemarketing

function operacionesTelemarketing(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('telemarketing');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('telemarketing');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('telemarketing');
	}

	mensajeResultado('nombre',$res,'Telemarketing');
    mensajeResultado('elimina',$res,'Telemarketing', true);
}


function listadoTelemarketing(){
	global $_CONFIG;

	$columnas=array('codigo','nombre','dni','provincia','telefono','movil','email','activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombre, dni, provincia, telefono, movil, email, activo FROM telemarketing $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, nombre, dni, provincia, telefono, movil, email, activo FROM telemarketing $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['codigo'],
			$datos['nombre'],
			$datos['dni'],
			convertirMinuscula($datos['provincia']),
			"<div class='nowrap'>".formateaTelefono($datos['telefono'])."</div>",
			"<div class='nowrap'>".formateaTelefono($datos['movil'])."</div>",
			"<a href='mailto:".$datos['email']."'>".$datos['email']."</a>",
			creaBotonDetalles("telemarketing/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionTelemarketing(){
	operacionesTelemarketing();

	abreVentanaGestion('Gestión de Telemarketing','?','span3');
	$datos=compruebaDatos('telemarketing');

	campoID($datos);
	campoTexto('nombre','Nombre',$datos,'span3');
	campoTextoValidador('dni','DNI',$datos,'input-small validaDNI','telemarketing');
	campoSelectProvincia($datos);
	campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small','telemarketing');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',$datos,'input-small','telemarketing');
	campoTextoSimboloValidador('fax','Fax','<i class="icon-fax"></i>',$datos,'input-small','telemarketing');
	campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large','telemarketing');
	campoTextoSimboloValidador('email2','eMail secundario','<i class="icon-envelope"></i>',$datos,'input-large','telemarketing');
	campoRadio('activo','Activo',$datos,'SI');
	campoOculto($datos,'codigoUsuarioAsociado','NULL');

	cierraVentanaGestion('index.php',true);
}


function filtroTelemarketing(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(1,'Nombre');
	campoSelectProvincia(false,3);

	cierraColumnaCampos();
	abreColumnaCampos();

	
	campoSelectSiNoFiltro(7,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de telemarketing
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agenda de agenda de eventos

function campoFechaCalendario(){
	echo "<div id='fechaCalendario'>
			<span>Ir a: </span>";
			campoTextoSolo('campoFechaCalendario',fecha(),'input-small datepicker hasDatepicker');
	echo "</div>";
}

function cargaEventosCalendario(){
	$inicio=$_GET['start'];
	$fin=$_GET['end'];

	$tareas="";

	$inicioVista=formateaFechaVistaCalendario($inicio,true);
	$finVista=formateaFechaVistaCalendario($fin,true,true);
	$whereFiltro=generaWhereFiltroUsuariosAgenda();

	//La subconsulta en el último LEFT JOIN de esta consulta sirve para que, en caso de que se haya mandado más de un email de notificación para la misma cita, no aparezcan N registros de la misma cita
	$consulta=consultaBD("SELECT tareas.codigo, fechaInicio, fechaFin, horaInicio, horaFin, tarea, tareas.observaciones, colorTareas
						  FROM tareas LEFT JOIN usuarios ON tareas.codigoUsuario=usuarios.codigo 
						  WHERE (fechaInicio>='$inicioVista' OR fechaInicio<='$finVista' OR fechaFin>='$inicioVista' OR fechaFin<='$finVista')
						  $whereFiltro 
						  GROUP BY tareas.codigo",true);
	
	while($datos=mysql_fetch_assoc($consulta)){
		$fechaInicio=explode('-',$datos['fechaInicio']);
		$fechaFin=explode('-',$datos['fechaFin']);
		
		$horaInicio=explode(':',$datos['horaInicio']);
		$horaFin=explode(':',$datos['horaFin']);

		$tareas.='
			{
				"id": '.$datos['codigo'].',
	    		"title": "'.$datos['tarea'].'",
			    "start": "'.$fechaInicio[0]."-".$fechaInicio[1]."-".$fechaInicio[2]." ".$horaInicio[0].":".$horaInicio[1].':00",
		    	"end": "'.$fechaFin[0]."-".$fechaFin[1]."-".$fechaFin[2]." ".$horaFin[0].":".$horaFin[1].':00",
		    	"allDay": false,
		    	"tarea": "'.$datos['tarea'].'",
		    	"descripcion": "",
		    	"backgroundColor": "'.$datos['colorTareas'].'",
		    	"borderColor": "'.$datos['colorTareas'].'"
	    	},';
  	}

  	if($tareas!=''){
  		$tareas=substr_replace($tareas, '', strlen($tareas)-1, strlen($tareas));//Para quitar última coma
  	}

  	echo '['.$tareas.']';
}

function formateaFechaVistaCalendario($timestamp,$bdd=false,$fechaFin=false){
	$fecha=new DateTime();
	$fecha=date_timestamp_set($fecha,$timestamp);
	
	if($bdd){
		$fecha=date_format($fecha,'Y-m-d');
		if($fechaFin){//Porque la fecha de fin transmitida por el calendario es 1 día más que el final
			$array=explode('-',$fecha);
			$array[2]--;
			$fecha=$array[0].'-'.$array[1].'-'.$array[2];
		}
	}

	return $fecha;
}

function generaWhereFiltroUsuariosAgenda(){
	$res='';
    $perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    /*if($perfil=='COMERCIAL'){
        $res=" AND (tareas.codigoUsuario=$codigoUsuario OR comerciales.codigoUsuarioAsociado=$codigoUsuario)";
    }
    elseif($perfil=='ADMINISTRACION2'){
        $res=" AND (tareas.codigoUsuario=$codigoUsuario OR comerciales.codigoUsuario=$codigoUsuario)";
    }
    elseif($perfil=='DELEGADO'){
        $res=" AND (tareas.codigoUsuario=$codigoUsuario OR (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u2 ON c2.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario))))";
    }
    elseif($perfil=='DIRECTOR'){
    	$res=" AND (tareas.codigoUsuario=$codigousuario OR (comerciales.codigoUsuarioAsociado=$codigoUsuario OR comerciales.codigo IN(SELECT c1.codigo FROM comerciales AS c1 WHERE c1.codigoComercial IN(SELECT c2.codigo FROM comerciales AS c2 INNER JOIN usuarios AS u1 ON c2.codigoUsuarioAsociado=u1.codigo WHERE u1.codigo=$codigoUsuario)) OR comerciales.codigo IN(SELECT c3.codigo FROM comerciales AS c3 WHERE c3.codigoComercial IN(SELECT c4.codigo FROM comerciales AS c4 WHERE c4.codigoComercial IN(SELECT c5.codigo FROM comerciales AS c5 INNER JOIN usuarios AS u2 ON c5.codigoUsuarioAsociado=u2.codigo WHERE u2.codigo=$codigoUsuario)))))";
    }
    elseif($perfil=='TELEMARKETING'){
        $res=" AND (tareas.codigoUsuario=$codigoUsuario OR telemarketing.codigoUsuarioAsociado=$codigoUsuario)";
    }*/

    if($_SESSION['tipoUsuario']!='ADMIN'){
    	$res=" AND tareas.codigoUsuario=$codigoUsuario";
	}


    return $res;
}

function actualizaEventoCalendario(){
	echo actualizaDatos('tareas');
}

function creaEventoCalendario(){
	echo insertaDatos('tareas');
}

function gestionTarea(){
	campoFecha('fechaInicio','Fecha inicio');
	campoTexto('horaInicio','Hora inicio',false,'input-mini pagination-right');
	campoFecha('fechaFin','Fecha fin');
	campoTexto('horaFin','Hora fin',false,'input-mini pagination-right');
	campoTexto('tarea','Tarea','','span4');
	//campoSelectConsultaAjax('codigoCliente','Cliente',"SELECT codigo, razonSocial AS texto FROM clientes",false,'clientes/gestion.php?codigo=','selectpicker selectAjax span3 show-tick','',0,false);
	campoSelectClienteFiltradoPorUsuario(false,'Cliente/posible cliente','codigoCliente','',true);
	areaTexto('observaciones','Observaciones');
	campoOculto($_SESSION['codigoU'],'codigoUsuario');
}

function campoColorTarea(){
	//Colores de Google Calendar
	$nombres=array(
		'<span class="label" style="background-color:#79d15f"></span>',
		'<span class="label" style="background-color:#5685e6"></span>',
		'<span class="label" style="background-color:#a5bef7"></span>',
		'<span class="label" style="background-color:#45d6da"></span>',
		'<span class="label" style="background-color:#79e7c2"></span>',
		'<span class="label" style="background-color:#4fb759"></span>',
		'<span class="label" style="background-color:#fbd670"></span>',
		'<span class="label" style="background-color:#ffb781"></span>',
		'<span class="label" style="background-color:#ff877f"></span>',
		'<span class="label" style="background-color:#dc1d2e"></span>',
		'<span class="label" style="background-color:#dcaef9"></span>',
		'<span class="label" style="background-color:#e1e1e1"></span>'
	);
	$valores=array(
		"#79d15f",
		"#5685e6",
		"#a5bef7",
		"#45d6da",
		"#79e7c2",
		"#4fb759",
		"#fbd670",
		"#ffb781",
		"#ff877f",
		"#dc1d2e",
		"#dcaef9",
		"#e1e1e1"
	);

	echo "
	<div class='control-group'>                     
		<label class='control-label' for='color'>Color:</label>
		<div class='controls'>
			<select name='color' id='color' class='selectpicker span1'>";
		
			for($i=0;$i<count($nombres);$i++){
				echo "<option value='".$valores[$i]."' data-content='".$nombres[$i]."'></option>";
			}
		
	echo "	</select>
		</div> <!-- /controls -->       
	</div> <!-- /control-group -->";
}

function consultaDatosTarea(){
	$codigoTarea=$_POST['codigo'];
	$datos=datosRegistro('tareas',$codigoTarea);
	
	$datos['fechaInicio']=formateaFechaWeb($datos['fechaInicio']);
	$datos['fechaFin']=formateaFechaWeb($datos['fechaFin']);
	$datos['horaInicio']=formateaHoraWeb($datos['horaInicio']);
	$datos['horaFin']=formateaHoraWeb($datos['horaFin']);
	$datos['cliente']=obtieneOptionClienteTarea($datos['codigoCliente']);

	echo json_encode($datos);
}

function obtieneOptionClienteTarea($codigoCliente){
	$res="<option value='NULL'></option>";

	if($codigoCliente!=NULL){
		$consulta=consultaBD("SELECT codigo, razonSocial AS texto FROM clientes WHERE codigo=$codigoCliente",true,true);
		$res.="<option value='".$consulta['codigo']."'>".$consulta['texto']."</option>";
	}

	return $res;
}

function eliminarTarea(){
	$codigoTarea=$_POST['codigo'];

	echo consultaBD("DELETE FROM tareas WHERE codigo=$codigoTarea",true);
}

//Fin parte de agenda de eventos
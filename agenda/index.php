<?php
  $seccionActiva=24;
  include_once('../cabecera.php');
?> 

<div class="main sinMargenAb" id="contenido">
  <div class="main-inner">
    <div class="container">
      
      <div class="widget widget-nopad widget-agenda">
        <div class="widget-header"> <i class="icon-calendar"></i>
          <h3>Agenda de Tareas</h3>
          <?php campoFechaCalendario(); ?>
        </div>

        <div class="widget-content">
          <div id='calendario'></div>
        </div>

      </div>
    </div>
  </div>


  <!-- Caja para nueva tarea -->
  <div id='cajaCreacion' class='modal hide fade'> 
    <div class='modal-header'> 
      <h3> <i class="icon-calendar"></i><i class="icon-chevron-right"></i><i class="icon-list-alt"></i> &nbsp; Gestión de tarea</h3> 
    </div>
    <div class='modal-body'>
      <form id="edit-profile" class="form-horizontal" method="post">
          <?php
            gestionTarea();
          ?>
      </form>
    </div> 
    <div class='modal-footer'> 
      <button type="button" class="btn btn-propio" id="creaTarea"><i class="icon-check"></i> Registrar tarea</button>  
      <button type="button" class="btn btn-propio hide" id="actualizaTarea"><i class="icon-refresh"></i> Actualizar tarea</button>  
      <button type="button" class="btn btn-danger hide" id="eliminarTarea"><i class="icon-trash"></i> Eliminar</button>  
      <button type="button" class="btn btn-default" id="cerrar"><i class="icon-remove"></i> Cancelar</button> 
    </div> 
  </div>
  <!-- Fin caja para nueva tarea -->


<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/full-calendar/jquery-ui.custom.min.js"></script><!-- Habilita el drag y el resize -->
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/full-calendar/fullcalendar.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.cookie.js"></script>

<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>

<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">

$(document).ready(function() {
  $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');
      var d=new Date(e.date.valueOf());
      $('#calendario').fullCalendar('gotoDate', d);//MODIFICACIÓN 09/06/2015: para que el calendario se vaya a la fecha seleccionada por el datepicker.
  });

  $('#cajaCreacion .hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});


  $('.selectpicker').selectpicker();

  $('input[name=todoDia]').change(function(){
    $('#cajaHora').toggleClass('hide');
  });

  $('#cerrar').click(function(){
    cierraVentana();
  });
  
  if($.cookie('diaElegidoCalendario')!=undefined){
    var ultimaFecha=$.cookie('diaElegidoCalendario');
    ultimaFecha=ultimaFecha.split('/');
    ultimaFecha[1]--;//El formato de mes que acepta fullcalendar empieza en 0
  }
  else{
    var f=new Date();
    var ultimaFecha=[f.getDay()+1,f.getMonth(),f.getFullYear()];
  }
  

  var calendario = $('#calendario').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    defaultView: 'agendaDay',
    year:ultimaFecha[2],
    month:ultimaFecha[1],
    date:ultimaFecha[0],
    viewRender: function(view, element){
      fechaActual=$.fullCalendar.formatDate(view.start, 'dd/MM/yyyy');
      $.cookie('diaElegidoCalendario', fechaActual);
    },
    selectable:true,
    selectHelper: true,
    editable:true,
    select: function(start, end, allDay, event, resourceId) {
      var fechaInicio = $.fullCalendar.formatDate(start, 'dd/MM/yyyy');
      var fechaFin = $.fullCalendar.formatDate(end, 'dd/MM/yyyy');
      var horaInicio = $.fullCalendar.formatDate(start, 'HH:mm');
      var horaFin = $.fullCalendar.formatDate(end, 'HH:mm');

      abreVentana(fechaInicio,fechaFin,horaInicio,horaFin);

      $('#creaTarea').click(function(){
          var codigoCliente=$('#codigoCliente').val();
          var fechaInicio=$('#fechaInicio').val();
          var horaInicio=$('#horaInicio').val();
          var fechaFin=$('#fechaFin').val();
          var horaFin=$('#horaFin').val();
          var tarea=$('#tarea').val();
          var observaciones=$('#observaciones').val();
          var codigoUsuario=$('#codigoUsuario').val();

          //Creación-renderización del evento
          var creacion=$.post("../listadoAjax.php?include=agenda&funcion=creaEventoCalendario();",{'fechaInicio':fechaInicio,'horaInicio':horaInicio,'fechaFin':fechaFin,'horaFin':horaFin,'tarea':tarea,'codigoCliente':codigoCliente,'observaciones':observaciones,'codigoUsuario':codigoUsuario});

          creacion.done(function(datos){
            calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
            cierraVentana();
          });
          //Fin creación-renderización
      });
      calendario.fullCalendar('unselect');
    },
    eventResize: function( event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view ) {//Actualización del evento cuando se modifica tu tamaño (duración)
      var fechaInicio = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var fechaFin = $.fullCalendar.formatDate(event.end, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');

      var creacion=$.post("../listadoAjax.php?include=agenda&funcion=actualizaEventoCalendario();", {codigo: event._id, fechaInicio:fechaInicio, fechaFin:fechaFin, horaInicio: horaI, horaFin: horaF});
      creacion.done(function(datos){
        calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
      });
      calendario.fullCalendar('unselect');
    },
    eventDrop: function (event, dayDelta, minuteDelta) {//Actualización del evento cuando se mueve de hora
      var fechaInicio = $.fullCalendar.formatDate(event.start, 'dd/MM/yyyy');
      var fechaFin = $.fullCalendar.formatDate(event.end, 'dd/MM/yyyy');
      var horaI = $.fullCalendar.formatDate(event.start, 'HH:mm');
      var horaF = $.fullCalendar.formatDate(event.end, 'HH:mm');
      
      var creacion=$.post("../listadoAjax.php?include=agenda&funcion=actualizaEventoCalendario();", {codigo: event._id, fechaInicio:fechaInicio, fechaFin:fechaFin, horaInicio: horaI, horaFin: horaF});
      creacion.done(function(datos){
        calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
      });
      calendario.fullCalendar('unselect');
    },
    eventClick:function(evento){
      abreVentanaOpciones(evento._id);
      $('#actualizaTarea').click(function(){
        var fechaInicio=$('#fechaInicio').val();
        var horaInicio=$('#horaInicio').val();
        var fechaFin=$('#fechaFin').val();
        var horaFin=$('#horaFin').val();
        var tarea=$('#tarea').val();
        var codigoCliente=$('#codigoCliente').val();
        var observaciones=$('#observaciones').val();
        var codigoUsuario=$('#codigoUsuario').val();

        //Actualización-renderización del evento
        var actualizacion=$.post("../listadoAjax.php?include=agenda&funcion=actualizaEventoCalendario();",{codigo:evento._id, 'fechaInicio':fechaInicio,'horaInicio':horaInicio,'fechaFin':fechaFin,'horaFin':horaFin,'tarea':tarea,'codigoCliente':codigoCliente,'observaciones':observaciones,'codigoUsuario':codigoUsuario});

        actualizacion.done(function(datos){
          calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
          cierraVentana();
        });
        calendario.fullCalendar('unselect');
        //Fin actualización-renderización
      });
      $('#eliminarTarea').click(function(){
        //Eliminación-renderización del evento
        var eliminacion=$.post("../listadoAjax.php?include=agenda&funcion=eliminarTarea();",{codigo:evento._id});

        eliminacion.done(function(datos){
          calendario.fullCalendar('refetchEvents');//Recarga los eventos del calendario, para obtener las actualizaciones
          cierraVentana();
        });
        calendario.fullCalendar('unselect');
        //Fin eliminación-renderización
      });
    },
    eventRender: function(event, element){//Para generar popover
        var posicion=obtienePosicionPopOver();

        $('.popover.in').remove();//Elimina los popovers creados anteriormente, evitando un bug que hace que al mover un evento el popover se quede fijo en la posición anterior, sin ocultarse.
        element.popover({//Crea el popover
            title: event.tarea,
            content: event.descripcion,
            placement:posicion
        });
        event.popOver=true;
    },
    firstDay:1,
    titleFormat: {
      month: 'MMMM yyyy',
      week: "d MMM [ yyyy]{ '&#8212;' d [ MMM] yyyy}",
      day: 'dddd, d MMM, yyyy'
    },
    columnFormat: {
      month: 'ddd',
      week: 'ddd d/M',
      day: 'dddd d/M'
    },
    axisFormat: 'H:mm',
    weekends:true,
    slotMinutes:30,
    allDaySlot:false,
    minTime:'7:00:00',
    maxTime:'22:00',
    timeFormat: 'H:mm { - H:mm}',
    events: "../listadoAjax.php?include=agenda&funcion=cargaEventosCalendario();"
  });
});

function abreVentana(fechaInicio,fechaFin,horaInicio,horaFin){
  $('#fechaInicio').val(fechaInicio);
  $('#fechaFin').val(fechaFin);
  $('#horaInicio').val(horaInicio);
  $('#horaFin').val(horaFin);

  $('#creaTarea').removeClass('hide');
  $('#actualizaTarea').addClass('hide');
  $('#eliminarTarea').addClass('hide');

  $('#cajaCreacion').modal({'show':true,'backdrop':'static','keyboard':false});
}

function abreVentanaOpciones(codigoTarea){
  $('#creaTarea').addClass('hide');
  $('#actualizaTarea').removeClass('hide');
  $('#eliminarTarea').removeClass('hide');

  $.post('../listadoAjax.php?include=agenda&funcion=consultaDatosTarea();',{'codigo':codigoTarea},
  function(respuesta){
    $('#fechaInicio').val(respuesta.fechaInicio);
    $('#fechaFin').val(respuesta.fechaFin);
    $('#horaInicio').val(respuesta.horaInicio);
    $('#horaFin').val(respuesta.horaFin);
    $('#tarea').val(respuesta.tarea);
    $('#prioridad').val(respuesta.prioridad);
    $('#estado').val(respuesta.estado);
    $('#observaciones').val(respuesta.observaciones);

    $('#codigoCliente').html(respuesta.cliente);
    $('#codigoCliente').val(respuesta.codigoCliente);

    $('.selectpicker').selectpicker('refresh');
  },'json');

  $('#cajaCreacion').modal({'show':true,'backdrop':'static','keyboard':false});
}


function cierraVentana(){
  $('#tarea').val('');
  $('#cajaCreacion').modal('hide');
  $('#creaTarea').unbind();
  $('#actualizaTarea').unbind();
  $('#eliminarTarea').unbind();
}

//La siguiente función define dónde debe mostrarse por defecto el popover de las citas, ya que los de la primera y última columna no se mostrarían correctamente si la posición es siempre 'top'
function obtienePosicionPopOver(){
  var res='bottom';
          
  return res;
}

</script>

<!-- contenido --></div>
<?php include_once('../pie.php'); ?>
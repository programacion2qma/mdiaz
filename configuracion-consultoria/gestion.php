<?php
  $seccionActiva=12;
  include_once("../cabecera.php");
  gestionTablas();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
});
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
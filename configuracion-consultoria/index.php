<?php
  $seccionActiva=12;
  include_once('../cabecera.php');
  
  operacionesTablas();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Tablas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
				        <a href="<?php echo $_CONFIG['raiz']; ?>configuracion-consultoria/gestion.php?tabla=grupo_alergenos" class="shortcut"><i class="shortcut-icon icon-list"></i><span class="shortcut-label">Grupos alérgenos</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>configuracion-consultoria/gestion.php?tabla=alergenos" class="shortcut"><i class="shortcut-icon icon-list"></i><span class="shortcut-label">Alérgenos</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){

  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesTablas(){
	$res=true;

	if(isset($_POST['tabla'])){
		$res=insertaDatosTabla();
	}

	mensajeResultado('tabla',$res,'Datos');
    mensajeResultado('elimina',$res,'Servicio', true);
}

function insertaDatosTabla(){
	$res=true;

	$i=0;

	$datos=arrayFormulario();
	$tabla=strtolower($datos['tabla']);

	conexionBD();
		if($tabla=='grupo_alergenos'){
			$res = insertaGrupoAlergenos($datos);
		} else if($tabla=='alergenos'){
			$res = insertaAlergenos($datos);
		}
	cierraBD();

	return $res;
}

function insertaGrupoAlergenos($datos){
	$res = true;
	$i=0;

	while(isset($datos['nombreExiste'.$i])){
		if($datos['nombreExiste'.$i] != ''){
			$res = $res && consultaBD("UPDATE grupo_alergenos SET nombre='".$datos['nombreExiste'.$i]."' WHERE codigo=".$datos['codigoExiste'.$i]);
		} else {
			$res = $res && consultaBD("DELETE FROM grupo_alergenos WHERE codigo=".$datos['codigoExiste'.$i]);
		}
		$i++;
	}
	$i=0;
	while(isset($datos['nombre'.$i])){
		if($datos['nombre'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO grupo_alergenos VALUES (NULL,'".$datos['nombre'.$i]."');");
		}
		$i++;
	}

	return $res;
}

function insertaAlergenos($datos){
	$res = true;

	$i=0;
	$res = $res && consultaBD("DELETE FROM alergenos");
	while(isset($datos['nombre'.$i])){
		if($datos['nombre'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO alergenos VALUES (NULL,'".$datos['grupo'.$i]."','".$datos['nombre'.$i]."');");
		}
		$i++;
	}

	return $res;
}

function gestionTablas(){
	$titulos=array('grupo_alergenos'=>'Grupos de Alérgenos','alergenos'=>'Alérgenos');

	abreVentanaGestion('Gestión de '.$titulos[$_GET['tabla']],'?');

	if($_GET['tabla']=='grupo_alergenos'){
		$datos=consultaBD('SELECT * FROM '.$_GET['tabla'].' ORDER BY NOMBRE',true);
		formularioGrupoAlergenos($datos);
	} else if($_GET['tabla']=='alergenos'){
		$datos=consultaBD('SELECT alergenos.nombre, grupo_alergenos.codigo AS grupo FROM '.$_GET['tabla'].' INNER JOIN grupo_alergenos ON alergenos.grupo=grupo_alergenos.codigo ORDER BY grupo_alergenos.nombre',true);
		formularioAlergenos($datos);
	}

	cierraVentanaGestion('index.php',true);
}


function formularioGrupoAlergenos($datos){
	campoOculto('grupo_alergenos','tabla');
	echo"	
	 	<center>
	 	Para eliminar dejar el campo 'nombre' vacío
	 	<div class='table-responsive'>
			<table style='width:50%;' class='table' id='tablaCentros'>
                <thead>
                  	<tr class='apartadoTablaFormularioEvaluacion'>
		            	<th> Nombre grupo </th>
                   	</tr>
               	</thead>
                <tbody>";
    $i=0;
	if($datos){	  
		while($item=mysql_fetch_assoc($datos)){
		 	echo "<tr>";
				campoTextoTabla('nombreExiste'.$i,$item['nombre'],'span6');
				campoOculto($item['codigo'],'codigoExiste'.$i);
			echo"</tr>";
			$i++;
		}
	}
	echo "</div>";
	$i=0;
	echo "<tr>";
		campoTextoTabla('nombre'.$i,$item['nombre'],'span6');
	echo"</tr>";

	echo "
				</tbody>
	        </table>
	    </div>
		</center>
		<center>
			<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCentros\");'><i class='icon-plus'></i> Añadir grupo</button> 
		</center>";
}


function formularioAlergenos($datos){
	campoOculto('alergenos','tabla');
	echo"	
	 	<center>
	 	Para eliminar dejar el campo 'nombre' vacío
	 	<div class='table-responsive'>
			<table style='width:50%;' class='table' id='tablaCentros'>
                <thead>
                  	<tr class='apartadoTablaFormularioEvaluacion'>
                  		<th> Grupo </th>
		            	<th> Nombre </th>
                   	</tr>
               	</thead>
                <tbody>";
    $i=0;
	if($datos){	  
		while($item=mysql_fetch_assoc($datos)){
		 	echo "<tr>";
		 		campoSelectConsulta('grupo'.$i,'Grupo','SELECT codigo, nombre AS texto FROM grupo_alergenos',$item['grupo'],'selectpicker span3 show-tick','data-live-search="true"','',1);
				campoTextoTabla('nombre'.$i,$item['nombre'],'span6');
			echo"</tr>";
			$i++;
		}
	}
	echo "<tr>";
		campoSelectConsulta('grupo'.$i,'Grupo','SELECT codigo, nombre AS texto FROM grupo_alergenos','','selectpicker span3 show-tick','data-live-search="true"','',1);
		campoTextoTabla('nombre'.$i,'','span6');
	echo"</tr>";

	echo "
				</tbody>
	        </table>
	    </div>
		</center>
		<center>
			<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCentros\");'><i class='icon-plus'></i> Añadir alérgeno</button> 
		</center>";
}


//Fin parte de servicios
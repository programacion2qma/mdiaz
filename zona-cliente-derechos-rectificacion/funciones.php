<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesDerechos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDerecho();
	}
	elseif(isset($_POST['interesado'])){
		$res=insertaDerecho();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('derechos');
	}

	mensajeResultado('interesado',$res,'Derecho');
    mensajeResultado('elimina',$res,'Derecho', true);
}

function insertaDerecho(){
	$res=true;
	responsable();
	$res=insertaDatos('derechos');
	return $res;
}

function actualizaDerecho(){
	$res=true;
	responsable();
	$res=actualizaDatos('derechos');
	return $res;
}

function responsable(){
	$_POST['responsable']='';
	$_POST['responsableDireccion']='';
	if($_POST['respuesta']==3 && $_POST['selectResponsable']!='NULL'){
		$i=$_POST['selectResponsable'];
		$_POST['responsable']=$_POST['responsable'.$i];
		$_POST['responsableDireccion']=$_POST['direccion'.$i];
	}
}

function listadoDerechos(){
	global $_CONFIG;

	$columnas=array('interesado','fechaRecepcion','fechaRespuesta','respuesta','tipo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente." AND tipo='Rectificación'",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having;");
	cierraBD();
	$respuestas=array('',
	'La solicitud no reúne los requisitos ',
	'No figuran datos personales del interesado',
	'Comunicación al Responsable de Tratamientos',
	'Datos personales bloqueados',
	'Denegación de rectificación',
	'Otorgamiento de rectificación');
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$otrosDatos="";
		if($datos['respuesta']==7 && $datos['otrosDatos']!=''){
			$otrosDatos=' - '.$datos['otrosDatos'];
		}
		$opciones=array('Detalles','Descargar respuesta');
		$enlaces=array("zona-cliente-derechos-rectificacion/gestion.php?codigo=".$datos['codigo'],"zona-cliente-derechos-rectificacion/generaRespuesta.php?codigo=".$datos['codigo']);
		$iconos=array('icon-search-plus','icon-cloud-download');
		if($datos['visualizacion']=='SI'){
			array_push($opciones,'Descargar comunicación a cesionario');
			array_push($enlaces,"zona-cliente-derechos-rectificacion/generaRespuesta.php?cesionario&codigo=".$datos['codigo']);
			array_push($iconos,'icon-cloud-download');
		}
		$fila=array(
			$datos['referencia'],
			$datos['interesado'],
			formateaFechaWeb($datos['fechaRecepcion']),
			formateaFechaWeb($datos['fechaRespuesta']),
			$respuestas[$datos['respuesta']].$otrosDatos,
			botonAcciones($opciones,$enlaces,$iconos),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionDerechos(){
	operacionesDerechos();

	abreVentanaGestion('Gestión de interesados que ejercen el derecho de rectificación','?','','icon-edit','margenAb');
	$datos=compruebaDatos('derechos');

    if(!$datos){
    	$codigoCliente=obtenerCodigoCliente(true);
    	$referencia=obtieneReferenciaDerecho('Rectificación',$codigoCliente);
    	campoOculto($codigoCliente,'codigoCliente');
    }  else {
    	$codigoCliente=$datos['codigoCliente'];
    	$referencia=$datos['referencia'];
    }

    campoOculto('','horaVisualizacion');
    campoOculto('','otrosDatos');

	abreColumnaCampos();
		$valores=array('','Acceso','Rectificación','Supresión o cancelación','Oposición y decisiones individuales automatizadas','Limitación del tratamiento','Portabilidad de los datos');
		campoOculto('Rectificación','tipo');
		campoTexto('referencia','Referencia',$referencia,'input-mini',true);
   		campoTexto('interesado','Nombre y apellido del interesado',$datos,'span7');
   		campoTexto('interesadoDni','NIF',$datos,'input-small');
   		campoTexto('representante','Representante legal',$datos,'span7');
   		campoTexto('representanteDni','NIF',$datos,'input-small');
   		campoTexto('direccion','Dirección a efecto de notificaciones',$datos,'span7');
   		campoTexto('email','Correo electrónico',$datos,'span7');
   	cierraColumnaCampos(true);
   	abreColumnaCampos();
		campoFecha('fechaRecepcion','Fecha de Recepción',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoFecha('fechaRespuesta','Fecha de Respuesta',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		campoRadio('mediosElectronicos','La solicitud se ha presentado por medios electrónicos',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		echo '<div id="divMediosDiferente" class="hide">';
			campoRadio('mediosDiferente','El interesado solicita que se le responda por otros medios diferentes al correo electrónico',$datos);
		echo '</div>';
	cierraColumnaCampos();
	abreColumnaCampos();
		echo '<div id="divOtroMedio" class="hide">';
			campoTexto('otroMedio','Indicar',$datos);
		echo '</div>';
	cierraColumnaCampos(true);
	abreColumnaCampos();
		$select=rellenaSelect();
		campoSelect('respuesta','Respuesta',$select['nombres'],$select['valores'],$datos,'selectpicker span6 show-tick');
	cierraColumnaCampos(true);
	$responsables=array();
	$direcciones=array();
	conexionBD();
	$declaraciones=consultaBD('SELECT * FROM declaraciones WHERE codigoCliente='.$codigoCliente);
	while($d=mysql_fetch_assoc($declaraciones)){
		if(!in_array($d['n_razon'],$responsables)){
			array_push($responsables,$d['n_razon']);
			array_push($direcciones,$d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$d['provincia_responsableFichero']);
		}
		$otros=consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion='.$d['codigo']);
		while($o=mysql_fetch_assoc($otros)){
			if($o['razonSocial']!='' && !in_array($o['razonSocial'],$responsables)){
				array_push($responsables,$o['razonSocial']);
				array_push($direcciones,$o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$o['provincia']);
			}
		}
	}
	cierraBD();
	abreColumnaCampos('span3 divResponsable hide');
		$nombres=array('');
		$valores=array('NULL');
		foreach ($responsables as $key => $value) {
			campoOculto($value,'responsable'.$key);
			array_push($nombres,$value);
			array_push($valores,$key);
		}
		foreach ($direcciones as $key => $value) {
			campoOculto($value,'direccion'.$key);
		}
		if($datos && $datos['responsable']!=''){
			campoDato('Responsable de tratamiento',$datos['responsable']);
		}
		campoSelect('selectResponsable','Selecciona responsable',$nombres,$valores);
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divOtrosDatos hide');
		echo '<a id="art14" style="margin-left:85px" class="btnModal noAjax">artículo 14 LOPD-GD</a>';
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divTratamiento hide');
		campoSelectConsulta('codigoTratamiento','Fichero/Tratamiento','SELECT codigo, nombreFichero AS texto FROM declaraciones WHERE codigoCliente='.$codigoCliente,$datos);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoRadio('visualizacion','Previamente a la solicitud de rectificación, ¿se cedieron los datos objeto de la rectificación? <a id="art19" class="btnModal noAjax">-artículo 19 RGPD-</a>',$datos,'NO',array('No','Sí'),array('NO','SI'));
		echo '<div id="divProcede" class="hide">';
			campoTexto('nombreCesionario','Nombre del cesionario',$datos);
			campoFecha('fechaVisualizacion','Fecha comunicación al cesionario',$datos);
		echo '</div>';
	cierraColumnaCampos(true);

	abreColumnaCampos();
		areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 
	cierraColumnaCampos(true);

	cierraVentanaGestion('index.php',true);

	abreVentanaModal('información','cajaGestion14');
	echo '<i><b>Artículo 14 LOPD-GDD. Derecho de rectificación.</b> Al ejercer el derecho de rectificación
reconocido en el artículo 16 del Reglamento (UE) 2016/679, el afectado deberá indicar en su
solicitud a qué datos se refiere y la corrección que haya de realizarse. Deberá acompañar,
cuando sea preciso, la documentación justificativa de la inexactitud o carácter incompleto de
los datos objeto de tratamiento</i>';
	cierraVentanaModal('','','',false,'Cerrar');

	abreVentanaModal('información','cajaGestion19');
	echo '<i><b>Artículo 19 RGPD. Obligación de notificación relativa a la rectificación o supresión de
datos personales o la limitación del tratamiento.</b> El responsable del tratamiento
comunicará cualquier rectificación o supresión de datos personales o limitación del
tratamiento efectuada con arreglo al artículo 16, al artículo 17, apartado 1, y al artículo 18
a cada uno de los destinatarios a los que se hayan comunicado los datos personales, salvo
que sea imposible o exija un esfuerzo desproporcionado. El responsable informará al
interesado acerca de dichos destinatarios, si este así lo solicita.</i>';
	cierraVentanaModal('','','',false,'Cerrar');
}

function rellenaSelect(){
	$res=array('valores'=>array(0,1,2,3,4,5,6),'nombres'=>array('',
	'La solicitud no reúne los requisitos</a>',
	'No figuran datos personales del interesado',
	'Comunicación al Responsable de Tratamientos',
	'Datos personales bloqueados',
	'Denegación de rectificación',
	'Otorgamiento de rectificación'));
	return $res;
}

function wordInteresadoDerechos($PHPWord,$codigoDerecho){
    global $_CONFIG;
    $datos=consultaBD('SELECT derechos.*, clientes.razonSocial, CONCAT(clientes.domicilio,", CP ",clientes.cp,", ",clientes.localidad,", ",clientes.provincia) AS direccionCliente, ficheroLogo, administrador FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo WHERE derechos.codigo='.$codigoDerecho,true,true);
    $tipo=$datos['tipo'];
    $ref=$datos['referencia']<10?'0'.$datos['referencia']:$datos['referencia'];
    $fichero='RESPUESTA_DERECHO_'.mb_strtoupper($tipo).'_INTERESADO_REF_'.$ref.'.docx';
    $documento=$PHPWord->loadTemplate('../documentos/derechos/plantillaDerechos.docx');
    $documento->setValue("true","");
    $documento->setValue("cliente",utf8_decode(sanearCaracteresDerechos($datos['razonSocial'])));  
    $documento->setValue("titulo",utf8_decode("RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO ".$datos['interesado']." REF. ".$ref));

    reemplazarLogoDerechos($documento,$datos,'image2.png'); 
    $respuestas=array('','LA SOLICITUD NO REÚNE LOS REQUISITOS NECESARIOS','NO FIGURAN DATOS PERSONALES DEL INTERESADO EN LOS FICHEROS DEL RESPONSABLE','COMUNICACIÓN AL RESPONSABLE DE TRATAMIENTOS ','DATOS PERSONALES BLOQUEADOS','DENEGACIÓN DE RECTIFICACIÓN','OTORGAMIENTO DE ACCESO');
    if($datos['respuesta']==3){
        $texto="RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": ".$respuestas[$datos['respuesta']]."<w:br/><w:br/>".$datos['razonSocial'].", en concepto de encargado del tratamiento de datos personales, de los cuales es responsable del tratamiento ".$datos['responsable'].", mediante el presente escrito es para dar respuesta a la petición de rectificación recibida el día ".formateaFechaWeb($datos['fechaRecepcion'])." realizada por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['responsableDireccion']."<w:br/><w:br/>";
    } else {
        $texto="RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": ".$respuestas[$datos['respuesta']]."<w:br/><w:br/>El presente escrito es para dar respuesta a la petición de rectificación de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['direccion']."<w:br/><w:br/>";
        
    }
    if($datos['representante']!=''){
        $texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    } else {
        $texto.=$datos['interesado'];
    }
    $texto.=$direccion;
    switch($datos['respuesta']){
        case 1:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que su solicitud no reúne los requisitos necesarios para el cumplimiento de lo establecido por el artículo 14 de la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos Personales y Garantía de los Derechos Digitales:<w:br/><w:br/>1. Nombre y apellidos del interesado; fotocopia de su documento nacional de identidad, o de su pasaporte u otro documento válido que lo identifique y, en su caso, de la persona que lo represente, o instrumentos electrónicos equivalentes; así como el documento o instrumento electrónico acreditativo de tal representación. La utilización de firma electrónica identificativa del afectado eximirá de la presentación de las fotocopias del DNI o documento equivalente<w:br/>2. Petición en que se concreta la solicitud: a qué datos se refiere y la corrección que haya de realizarse.<w:br/>3. Dirección a efectos de notificaciones, fecha y firma del solicitante.<w:br/>4. Documentación justificativa de la inexactitud o carácter incompleto de los datos objeto de tratamiento";
        break;

        case 2:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>No figuran datos personales del interesado/a en nuestros sistemas de tratamiento de datos.";
        break;

        case 3:
        $texto.="Se adjunta copia de la solicitud de rectificación recibida, a fin de que el/la responsable del tratamiento resuelva sobre la misma.";
        break;

        case 4:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>Los datos personales del interesado se encuentran bloqueados, por lo que no pueden ser tratados excepto para su puesta a disposición de las Administraciones Públicas competentes, Jueces y Tribunales y Ministerio Fiscal. Una vez transcurrido el plazo de prescripción que corresponda, los datos personales bloqueados son eliminados de nuestros sistema de tratamiento/archivos -\"Artículo 5.1.e) RGPD (limitación del plazo de conservación) y artículo 32 LOPD-GDD (\"Bloqueo de los datos. 1. El responsable del tratamiento estará obligado a bloquear los datos cuando proceda a su rectificación o supresión. 2. El bloqueo de los datos consiste en la identificación y reserva de los mismos, adoptando medidas técnicas y organizativas, para impedir su tratamiento, incluyendo su visualización, excepto para la puesta a disposición de los datos a los jueces y tribunales, el Ministerio Fiscal o las Administraciones Públicas competentes, en particular de las autoridades de protección de datos, para la exigencia de posibles responsabilidades derivadas del tratamiento y solo por el plazo de prescripción de las mismas. Transcurrido ese plazo deberá procederse a la destrucción de los datos. 3. Los datos bloqueados no podrán ser tratados para ninguna finalidad distinta de la señalada en el apartado anterior.\")-.";
        break;

        case 5:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/><w:br/>Artículo 12.5 Reglamento (UE) 2016/679. Transparencia de la información, comunicación y modalidades de ejercicio de los derechos del interesado: \"5. La información facilitada en virtud de los artículos 13 y 14 así como toda comunicación y cualquier actuación realizada en virtud de los artículos 15 a 22 y 34 serán a título gratuito. Cuando las solicitudes sean manifiestamente infundadas o excesivas, especialmente debido a su carácter repetitivo, el responsable del tratamiento podrá:<w:br/><w:br/>a) cobrar un canon razonable en función de los costes administrativos afrontados para facilitar la información o la comunicación o realizar la actuación solicitada, o<w:br/><w:br/>b) negarse a actuar respecto de la solicitud.<w:br/><w:br/>El responsable del tratamiento soportará la carga de demostrar el carácter manifiestamente infundado o excesivo de la solicitud.\"";
        break;

        case 6:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que se ha procedido a la rectificación de datos solicitada.";
        break;
    }
    $texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
   	if($datos['respuesta']==1){
    	$texto.="<w:br/><w:br/>Deberá subsanar este defecto ante ".$datos['razonSocial'].", con domicilio a efectos de notificaciones en ".$datos['direccion']." para que podamos atender su petición";
    }
    $texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];

    if($datos['respuesta']==3){
    	$texto.="<w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/>RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": COMUNICACIÓN AL INTERESADO EN CONCEPTO DE ENCARGADO/ DEL TRATAMIENTO<w:br/><w:br/>El presente escrito es para dar respuesta a la petición de rectificación de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['direccion']."<w:br/><w:br/>";
        if($datos['representante']!=''){
        	$texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    	} else {
        	$texto.=$datos['interesado'];
    	}
    	$texto.=$direccion;
    	$texto.=$datos['razonSocial']." le informa que:<w:br/><w:br/>1º. El tratamiento que realiza respecto los datos personales que solicita rectifiquemos, lo realizamos en concepto de encargado del tratamiento, por lo que deberá solicitar su rectificación ante el Responsable del Tratamiento ".$datos['direccion']." con dirección en ".$datos['responsableDireccion'].".<w:br/><w:br/>2º. Se ha procedido a trasladar al Responsable del Tratamiento referenciado la solicitud de rectificación referenciada en el presente escrito.";
    	$texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
    	$texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];
    }
    
    $documento->setValue("texto",utf8_decode($texto));

    $documento->save('../documentos/derechos/respuesta.docx');
    return $fichero;
}

function wordCesionario($PHPWord,$codigoDerecho){
    global $_CONFIG;
    $datos=consultaBD('SELECT derechos.*, clientes.razonSocial, CONCAT(clientes.domicilio,", CP ",clientes.cp,", ",clientes.localidad,", ",clientes.provincia) AS direccionCliente, ficheroLogo, administrador FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo WHERE derechos.codigo='.$codigoDerecho,true,true);
    $tipo=$datos['tipo'];
    $ref=$datos['referencia']<10?'0'.$datos['referencia']:$datos['referencia'];
    $fichero='COMUNICACIÓN_A_CESIONARIO_DE_'.mb_strtoupper($tipo).'_SOBRE_DATOS_INTERESADO_REF_'.$ref.'.docx';
    $documento=$PHPWord->loadTemplate('../documentos/derechos/plantillaDerechos.docx');
    $documento->setValue("true","");
    $documento->setValue("cliente",utf8_decode(sanearCaracteresDerechos($datos['razonSocial'])));  
    $documento->setValue("titulo",utf8_decode("COMUNICACIÓN A CESIONARIO DE ".mb_strtoupper($tipo)." SOBRE DATOS INTERESADO ".$datos['interesado']." REF. ".$ref));

    reemplazarLogoDerechos($documento,$datos,'image2.png'); 
    
    $texto="COMUNICACIÓN A CESIONARIO DE DATOS PERSONALES DE RECTIFICACIÓN EFECTUADA SOBRE DICHOS DATOS.<w:br/><w:br/><w:br/><w:br/>".$datos['razonSocial'].", en concepto de responsable del tratamiento de datos de carácter personal cedidos a ".$datos['nombreCesionario'].", mediante el presente escrito informamos al cesionario referenciado que hemos efectuado sobre los datos objeto de dicha cesión la rectificación solicitada el día ".formateaFechaWeb($datos['fechaRecepcion'])." realizada por D./ª";
    $direccion=" con domicilio a efectos de notificaciones en ".$datos['responsableDireccion']."<w:br/><w:br/>";
    
    if($datos['representante']!=''){
        $texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    } else {
        $texto.=$datos['interesado'];
    }
    $texto.=$direccion;
    $texto.="<w:br/><w:br/>Adjuntamos copia de la solicitud de rectificación recibida y efectuada, objeto del presente comunicado.";
    $texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
    $texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];

    $documento->setValue("texto",utf8_decode($texto));

    $documento->save('../documentos/derechos/respuesta.docx');
    return $fichero;
}
//Fin parte de agrupaciones
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesCesiones(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaCesion();
	}
	elseif(isset($_POST['tipoCesiones'])){
		$res=insertaCesion();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('cesiones_lopd');
	}

	mensajeResultado('tipoCesiones',$res,'Cesión');
    mensajeResultado('elimina',$res,'Cesión', true);
}

function insertaCesion(){
	$res=true;
	prepararCampos();
	$res=insertaDatos('cesiones_lopd');
	return $res;
}

function actualizaCesion(){
	$res=true;
	prepararCampos();
	$res=actualizaDatos('cesiones_lopd');
	return $res;
}

function prepararCampos(){
	$datos=arrayFormulario();
	$select = $datos['ficheroCesiones'];
	$text ='';
	if(!empty($select)){
		for($j=0;$j<count($select);$j++){
			if($j > 0){
				$text .= "&$&";
			}
			$text .= $select[$j];
		}
	}
	$_POST['ficheroCesiones'] = $text;

}


function listadoCesiones(){
	global $_CONFIG;

	$columnas=array('tipoCesiones','activo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT cesiones_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM cesiones_lopd INNER JOIN trabajos ON cesiones_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT cesiones_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM cesiones_lopd INNER JOIN trabajos ON cesiones_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$ficheros=obtieneFicheros($datos['ficheroCesiones']);
		$fila=array(
			$datos['tipoCesiones'],
			$datos['quienCesiones'],
			$ficheros,
			formateaFechaWeb($datos['fechaCesiones']),
			formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'],
			creaBotonDetalles("zona-cliente-cesiones/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function obtieneFicheros($ficheros){
	$res='';
	$ficheros=explode('&$&', $ficheros);
	foreach ($ficheros as $key => $value) {
		$item=datosRegistro('declaraciones',$value);
		if($res!=''){
			$res.=',<br/>';
		}
		$res.=$item['nombreFichero'];
	}

	return $res;
}



function gestionCesiones(){
	operacionesCesiones();

	abreVentanaGestion('Gestión de Cesiones de datos','?','','icon-edit','margenAb');
	$datos=compruebaDatos('cesiones_lopd');

	$consulta=consultaBD("SELECT codigo, nombreFichero FROM declaraciones WHERE codigoCliente='".obtenerCodigoCliente(true)."';",true);
	$opciones=array();
	$valores=array();
    while($datosFicheros=mysql_fetch_assoc($consulta)){
        array_push($opciones, $datosFicheros['nombreFichero']);
        array_push($valores, $datosFicheros['codigo']);        
    }

    if(!$datos){
    	abreColumnaCampos();
    		campoSelectConsulta('codigoTrabajo','Consultoría','SELECT trabajos.codigo, servicios.servicio AS texto FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE codigoCliente='.obtenerCodigoCliente(true),false,'selectpicker span5');
    	cierraColumnaCampos(true);
    }

	abreColumnaCampos();
		campoTexto('tipoCesiones','Tipo de datos que cede',$datos,'span5');
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoTexto('quienCesiones','A quién se lo cede',$datos);
		campoTexto('finalidadCesiones','Finalidad',$datos);
		campoFecha('fechaCesiones','Fecha de la cesión',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto('objetoCesiones','Objeto social',$datos);
		camposelectMultiple('ficheroCesiones','Ficheros a los que acceden',$opciones,$valores,$datos,'selectpicker span3 show-tick multipleFicheros');
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
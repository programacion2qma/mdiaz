<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de gestión LOPD


function operacionesLOPD(){
	$res=true;

	if(isset($_GET['revision'])){
		$res=cambiaValorCampo('revision','SI',$_GET['revision'],'declaraciones');
		$_POST['razon_s']=true;
	}
	elseif(isset($_GET['nuevaAuditoria'])){
		$res=cambiaValorCampo('revision','SI',$_GET['revision'],'declaraciones');
		$_POST['razon_s']=true;
	}
	elseif(isset($_POST['codigo'])){
		prepararMultiple();
		$res=actualizaDatos('declaraciones');
		$codigo=$_POST['codigo'];
		$res=$res && insertaTransferencias($_POST['codigo']);
		$res=$res && insertaResponsables($_POST['codigo']);
		$res=$res && insertaEncargados($_POST['codigo']);
		$res=$res && finalizarOperacionesLOPD($_POST['codigo'],$_POST['codigoCliente'],$_POST['accion']);
		$_REQUEST['codigo']=$codigo;
	}
	elseif(isset($_POST['razon_s'])){
		prepararMultiple();
		$res=insertaDatos('declaraciones');
		$codigo=$res;
		$res=$res && insertaTransferencias($codigo);
		$res=$res && insertaResponsables($codigo);
		$res=$res && insertaEncargados($codigo);
		$res=$res && finalizarOperacionesLOPD($res,$_POST['codigoCliente'],$_POST['accion']);
		$_REQUEST['codigo']=$codigo;
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('declaraciones');
	}

   mensajeResultadoAdicional('razon_s',$res,'Declaración');
   mensajeResultado('elimina',$res,'Declaración', true);
}

function prepararMultiple(){
	$datos=arrayFormulario();
	$text='';
	if(is_array($datos['donde_encargado'])){
		$select = $datos['donde_encargado'];
		if(!empty($select)){
			for($k=0;$k<count($select);$k++){
				if($k > 0){
					$text .= "&$&";
				}
				$text .= $select[$k];
			}
		} 
	}
	$_POST['donde_encargado']=$text;
	if(is_array($datos['serviciosResponsable'])){
		$select = $datos['serviciosResponsable'];
		if(!empty($select)){
			for($k=0;$k<count($select);$k++){
				if($k > 0){
					$text .= "&$&";
				}
				$text .= $select[$k];
			}
		} 
	}
	$_POST['serviciosResponsable']=$text;
}

function finalizarOperacionesLOPD($fichero,$cliente,$accion){
	$res=true;//Siempre que vayamos a usarlo con un AND, inicializarlo a true
	if ($accion == 'Finalizar'){
		$res = presentaDeclaracion($fichero);
	}
	return $res;
}

function insertaTransferencias($codigo){
	$res=true;
	conexionBD();
	$res=consultaBD('DELETE FROM transferencias_internacionales WHERE codigoDeclaracion='.$codigo);
	$i=0;
	while(isset($_POST['paisTrans'.$i])){
		$res=consultaBD('INSERT INTO transferencias_internacionales VALUES(NULL,'.$codigo.',"'.$_POST['paisTrans'.$i].'","'.$_POST['categoriaTrans'.$i].'","'.$_POST['otrosTrans'.$i].'","'.$_POST['destNSadecuado'.$i].'","'.$_POST['destNSinadecuado'.$i].'","'.$_POST['otrosDestTI'.$i].'","'.$_POST['tiautorizacionAEPD'.$i].'")');
		$i++;
	}
	cierraBD();
	return $res;
}

function insertaResponsables($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
		$res=consultaBD('DELETE FROM responsables_fichero WHERE codigoDeclaracion='.$codigo);
		$i=0;
		while(isset($datos['cif_responsableFichero'.$i])){
				$text='';
				if(is_array($datos['donde_responsableFichero'.$i])){
					$select = $datos['donde_responsableFichero'.$i];
					if(!empty($select)){
						for($k=0;$k<count($select);$k++){
							if($k > 0){
								$text .= "&$&";
							}
							$text .= $select[$k];
						}
					}
				} 
				$datos['donde_responsableFichero'.$i]=$text;
				$res=consultaBD('INSERT INTO responsables_fichero VALUES (NULL,'.$codigo.',"'.$datos['cif_responsableFichero'.$i].'","'.$datos['direccion_responsableFichero'.$i].'","'.$datos['provincia_responsableFichero'.$i].'","'.$datos['pais_responsableFichero'.$i].'","'.$datos['localidad_responsableFichero'.$i].'","'.$datos['cp_responsableFichero'.$i].'","'.$datos['telefono_responsableFichero'.$i].'","'.$datos['fax_responsableFichero'.$i].'","'.$datos['email_responsableFichero'.$i].'","'.$datos['razonSocial'.$i].'","'.$datos['actividad'.$i].'","'.$datos['representante_responsableFichero'.$i].'","'.$datos['nifRepresentante_responsableFichero'.$i].'","'.$datos['fechaInicio_responsableFichero'.$i].'","'.$datos['fechaFin_responsableFichero'.$i].'","'.$datos['donde_responsableFichero'.$i].'");');
			$i++;
		}
	cierraBD();
	return $res;
}

function insertaSubcontratacion($codigo,$tipo,$codigoTrabajo='NULL',$j=0){
	$res=true;
	$campos=$tipo=='TRATAMIENTO'?array('empresa','nif','direccion','cp','localidad','provincia','representante','servicio'):array('empresa'.$j.'_','nif'.$j.'_','direccion'.$j.'_','cp'.$j.'_','localidad'.$j.'_','provincia'.$j.'_','representante'.$j.'_','servicio'.$j.'_');
	conexionBD();
	$res=consultaBD('DELETE FROM declaraciones_subcontrata WHERE codigoResponsable='.$codigo);
	$i=0;
	while(isset($_POST[$campos[0].$i])){
		$res=consultaBD('INSERT INTO declaraciones_subcontrata VALUES(NULL,'.$codigo.',"'.$_POST[$campos[0].$i].'","'.$tipo.'",'.$codigoTrabajo.',"'.$_POST[$campos[1].$i].'","'.$_POST[$campos[2].$i].'","'.$_POST[$campos[3].$i].'","'.$_POST[$campos[4].$i].'","'.$_POST[$campos[5].$i].'","'.$_POST[$campos[6].$i].'","'.$_POST[$campos[7].$i].'")');
		$i++;
	}
	cierraBD();
	return $res;
}

function insertaEncargados($codigo){
	$i=0;
	$datos=arrayFormulario();
	$hayEncargado=false;
	$encargadosFijos=array();
	$encargadosFijosExiste=array();
	$encargadosDinamicosExiste=array();
	$trabajo=consultaBD('SELECT trabajos.* FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE codigoCliente='.$datos['codigoCliente'],true,true);
	while(isset($datos['encargadoUno'.$i])){
		if($datos['encargadoUno'.$i]!=0){
			$hayEncargado=true;
			if($datos['encargadoUno'.$i]==1000){
				$codigoEncargado=insertaEncargadoDinamico($i,$codigo,$trabajo['codigo']);
				array_push($encargadosDinamicosExiste, $codigoEncargado);
				insertaTratamientos($trabajo['codigo'],$i,$codigoEncargado,'NO');
				insertaSubcontratacion($codigoEncargado,'ENCARGADO',$trabajo['codigo'],$i,'NO');
			} else {
				$encargadosFijos=preparaEncargadoFijo($i,$datos,$codigo,$encargadosFijos);
				array_push($encargadosFijosExiste, $datos['encargadoUno'.$i]);
				insertaTratamientos($trabajo['codigo'],$i,$datos['encargadoUno'.$i],'SI');
				insertaSubcontratacion($datos['encargadoUno'.$i],'ENCARGADOFIJO',$trabajo['codigo'],$i);
			}
		}
		$i++;
	}
	if(!$hayEncargado && trim($datos['cif_nif']) != trim($datos['cif_nif_responsableFichero'])) {
		$i=0;
		$_POST['encargadoDiecisiete'.$i]=$datos['nombreFichero'];
		$_POST['encargadoDos'.$i]=$datos['razon_s'];
		$_POST['encargadoTres'.$i]=$datos['cif_nif'];
		$_POST['encargadoNueve'.$i]=$datos['dir_postal'];
		$_POST['encargadoCuatro'.$i]=$datos['email'];
		$_POST['encargadoDiez'.$i]=$datos['localidad'];
		$_POST['encargadoOnce'.$i]=$datos['postal'];
		$_POST['encargadoCinco'.$i]=$datos['telefono'];
		$_POST['encargadoSeis'.$i]='';
		$_POST['encargadoSiete'.$i]=$datos['nombre'];
		$_POST['encargadoTrece'.$i]=array('ENCARGADO','REMOTO');
		$_POST['encargadoCatorce'.$i]='NO';
		$_POST['encargadoOcho'.$i]='00/00/0000';
		$_POST['encargadoDieciseis'.$i]='00/00/0000';
		$_POST['encargadoQuince'.$i]='X';
		$_POST['encargadoDoce'.$i]=$datos['provincia'];
		$_POST['encargadoDieciocho'.$i]='NO';
		$codigoEncargado=insertaEncargadoDinamico($i,$codigo,$trabajo['codigo']);
		array_push($encargadosDinamicosExiste, $codigoEncargado);
		insertaTratamientos($trabajo['codigo'],$i,$codigoEncargado,'NO');
	}
	$formulario=recogerFormularioServicios($trabajo);
	$preguntas='';
	$ficheros=array(574,575,576,577,578,579,580,581,582,582,583,584,585,586,587,609);
	foreach ($formulario as $key => $value) {
		$i=str_replace('pregunta','',$key);
		if($i>1){
			$preguntas.="&{}&";
		}
		if(isset($encargadosFijos[$i])){
			if(is_array($encargadosFijos[$i])){
				$select = $encargadosFijos[$i];
				$text ='';
				if(!empty($select)){
					for($k=0;$k<count($select);$k++){
						if($k > 0){
							$text .= "&$&";
						}
						$text .= $select[$k];
					}
				}
				$encargadosFijos[$i]=$text;
			}
			if(in_array($i, $ficheros)){
				$dato=explode('&$&', $value);
				if(!in_array($encargadosFijos[$i], $dato)){
					if($value!=''){
						$encargadosFijos[$i]=$value.'&$&'.$encargadosFijos[$i];
					} 
				}
			}
			$preguntas.=$key.'=>'.$encargadosFijos[$i];
		} else {
			$preguntas.=$key.'=>'.$value;
		}
	}
	$res=consultaBD('UPDATE trabajos SET formulario="'.$preguntas.'" WHERE codigo='.$trabajo['codigo'],true);
	$res=eliminaEncargadoFijo($encargadosFijosExiste,$datos['codigoCliente'],$codigo);
	$notIn=implode(',',$encargadosDinamicosExiste);
	$notIn=$notIn==''?'0':$notIn;
	$res=consultaBD('DELETE FROM otros_encargados_lopd WHERE codigoTrabajo='.$trabajo['codigo'].' AND codigo NOT IN('.$notIn.') AND ficheroEncargado LIKE "%'.$codigo.'%";',true);
	return $res;
}

function insertaTratamientos($codigoTrabajo,$i,$codigoEncargado,$fijo){
	$res=true;
	preparaTratamiento($i);
	$_POST['fijo']=$fijo;
	$_POST['codigoTrabajo']=$codigoTrabajo;
	$_POST['codigoEncargado']=$codigoEncargado;
	$tratamiento=consultaBD('SELECT * FROM encargados_tratamientos WHERE fijo="'.$fijo.'" AND codigoEncargado='.$codigoEncargado.' AND codigoTrabajo='.$codigoTrabajo,true,true);
	if($tratamiento){
		$_POST['codigo']=$tratamiento['codigo'];
		$res=actualizaDatos('encargados_tratamientos');
	} else {
		$res=insertaDatos('encargados_tratamientos');
	}
	return $res;
}

function preparaTratamiento($n){
	$listado=array('checkRecogida','checkModificacion','checkConsulta','checkComunicacion','checkComunicacion1','checkRegistro','checkConservacion','checkCotejo','checkSupresion','checkComunicacion2','checkEstructuracion','checkInterconexion','checkAnalisis','checkDestruccion','checkComunicacion3');
	foreach ($listado as $value) {
		if(isset($_POST[$value.'_'.$n])){
			$_POST[$value]='SI';
		} else {
			$_POST[$value]='NO';
		}
	}
}

function preparaEncargadoFijo($i,$datos,$codigoFichero,$encargado){
	$campos=array(
			158=>array('','','',159,160,161,162,163,164,165,166,167,511,541,543,544,574,589,611,629),
			168=>array('','','',169,170,171,172,173,174,175,176,177,512,527,545,546,575,590,612,630),
			178=>array('','','',179,180,181,182,183,184,185,186,187,513,542,547,548,576,591,613,631),
			189=>array('','','',190,191,192,193,194,195,196,197,198,514,528,549,550,577,592,614,632),
			200=>array('','','',201,202,203,204,205,206,207,208,209,515,529,551,552,578,593,615,633),
			210=>array('','','',211,212,213,214,215,216,219,218,217,516,530,553,554,579,594,616,634),
			220=>array('','','',221,222,223,224,225,226,229,228,227,517,531,555,556,609,595,617,635),
			230=>array('','','',231,232,233,234,235,236,239,238,236,518,532,557,558,580,596,618,636),
			240=>array('','','',241,242,243,244,245,246,249,248,247,519,533,559,560,581,597,619,637),
			250=>array('','','',251,252,253,254,255,256,259,258,257,520,534,561,562,582,598,620,638),
			260=>array('','','',261,262,263,264,265,266,269,268,267,521,535,563,564,583,599,621,639),
			270=>array('','','',271,272,273,274,275,276,279,278,277,522,536,565,566,584,600,622,640),
			280=>array('','','',281,282,283,284,285,286,289,288,287,523,537,567,568,585,601,623,641),
			290=>array('','','',291,292,293,294,295,296,299,298,297,524,538,569,570,586,602,624,642),
			487=>array('','','',488,489,490,491,492,493,496,495,494,525,571,572,573,587,603,625,643));
	$campos=$campos[$datos['encargadoUno'.$i]];
	$encargado[$datos['encargadoUno'.$i]]='SI';
	$encargado[$campos[3]]=$datos['encargadoDos'.$i];
	$encargado[$campos[4]]=$datos['encargadoTres'.$i];
	$encargado[$campos[5]]=$datos['encargadoNueve'.$i];
	$encargado[$campos[6]]=$datos['encargadoCuatro'.$i];
	$encargado[$campos[7]]=$datos['encargadoDiez'.$i];
	$encargado[$campos[8]]=$datos['encargadoOnce'.$i];
	$encargado[$campos[9]]=$datos['encargadoCinco'.$i];
	$encargado[$campos[10]]=$datos['encargadoSeis'.$i];
	$encargado[$campos[11]]=$datos['encargadoSiete'.$i];
	$encargado[$campos[12]]=$datos['encargadoTrece'.$i];
	$encargado[$campos[13]]=$datos['encargadoCatorce'.$i];
	$encargado[$campos[14]]=$datos['encargadoOcho'.$i];
	$encargado[$campos[15]]=$datos['encargadoDieciseis'.$i];
	$encargado[$campos[16]]=$codigoFichero;
	$encargado[$campos[17]]=$datos['encargadoQuince'.$i];
	$encargado[$campos[18]]=$datos['encargadoDoce'.$i];
	$encargado[$campos[19]]=$datos['encargadoDieciocho'.$i];
	return $encargado;
}

function eliminaEncargadoFijo($encargadosFijosExiste,$codigoCliente,$codigo){
	$res=true;
	$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
	$campos=array(
			158=>array('','','',159,160,161,162,163,164,165,166,167,511,541,543,544,574,589,611,629),
			168=>array('','','',169,170,171,172,173,174,175,176,177,512,527,545,546,575,590,612,630),
			178=>array('','','',179,180,181,182,183,184,185,186,187,513,542,547,548,576,591,613,631),
			189=>array('','','',190,191,192,193,194,195,196,197,198,514,528,549,550,577,592,614,632),
			200=>array('','','',201,202,203,204,205,206,207,208,209,515,529,551,552,578,593,615,633),
			210=>array('','','',211,212,213,214,215,216,219,218,217,516,530,553,554,579,594,616,634),
			220=>array('','','',221,222,223,224,225,226,229,228,227,517,531,555,556,609,595,617,635),
			230=>array('','','',231,232,233,234,235,236,239,238,236,518,532,557,558,580,596,618,636),
			240=>array('','','',241,242,243,244,245,246,249,248,247,519,533,559,560,581,597,619,637),
			250=>array('','','',251,252,253,254,255,256,259,258,257,520,534,561,562,582,598,620,638),
			260=>array('','','',261,262,263,264,265,266,269,268,267,521,535,563,564,583,599,621,639),
			270=>array('','','',271,272,273,274,275,276,279,278,277,522,536,565,566,584,600,622,640),
			280=>array('','','',281,282,283,284,285,286,289,288,287,523,537,567,568,585,601,623,641),
			290=>array('','','',291,292,293,294,295,296,299,298,297,524,538,569,570,586,602,624,642),
			487=>array('','','',488,489,490,491,492,493,496,495,494,525,571,572,573,587,603,625,643));
	$trabajo=consultaBD('SELECT trabajos.* FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE codigoCliente='.$codigoCliente,true,true);
	$formulario=recogerFormularioServicios($trabajo);
	foreach ($encargados as $encargado) {
		if(!in_array($encargado, $encargadosFijosExiste)){
			if($formulario['pregunta'.$encargado]=='SI'){
				$ficheros=$formulario['pregunta'.$campos[$encargado][16]];
				$ficheros=explode('&$&', $ficheros);
				if(in_array($codigo, $ficheros)){
					if(count($ficheros)==1){
						$formulario['pregunta'.$encargado]='NO';
						for($i=3;$i<=18;$i++){
							$formulario['pregunta'.$campos[$encargado][$i]]='';
						}
					} else {
						$texto='';
						foreach ($ficheros as $f) {
							if($f!=$codigo){
								if($texto!=''){
									$texto.='&$&';
								}
								$texto.=$f;
							}
						}
						$formulario['pregunta'.$campos[$encargado][16]]=$texto;
					}
				}
			}
		}
	}
	$preguntas='';
	foreach ($formulario as $key => $value) {
		if($preguntas!=''){
			$preguntas.="&{}&";
		}
		$preguntas.=$key.'=>'.$value;
	}
	$res=consultaBD('UPDATE trabajos SET formulario="'.$preguntas.'" WHERE codigo='.$trabajo['codigo'],true);
	return $res;
}

function insertaEncargadoDinamico($i,$codigo,$codigoTrabajo){
	$res=0;
	$datos=arrayFormulario();

	$select = $datos['encargadoTrece'.$i];
	$text ='';
	if(!empty($select)){
		for($j=0;$j<count($select);$j++){
			if($j > 0){
				$text .= "&$&";
			}
			$text .= $select[$j];
		}
	}
	$datos['encargadoTrece'.$i] = $text;
	conexionBD();
	if(isset($datos['codigoEncargadoExiste'.$i]) && $datos['codigoEncargadoExiste'.$i]!=''){
		$res = consultaBD("UPDATE otros_encargados_lopd SET servicioEncargado='".$datos['encargadoDiecisiete'.$i]."', nombreEncargado='".$datos['encargadoDos'.$i]."', cifEncargado='".$datos['encargadoTres'.$i]."', direccionEncargado='".$datos['encargadoNueve'.$i]."', emailEncargado='".$datos['encargadoCuatro'.$i]."', localidadEncargado='".$datos['encargadoDiez'.$i]."', cpEncargado='".$datos['encargadoOnce'.$i]."', telefonoEncargado='".$datos['encargadoCinco'.$i]."', contactoEncargado='".$datos['encargadoSeis'.$i]."', representanteEncargado='".$datos['encargadoSiete'.$i]."', dondeEncargado='".$datos['encargadoTrece'.$i]."', subcontratacionEncargado='".$datos['encargadoCatorce'.$i]."', fechaAltaEncargado='".formateaFechaBD($datos['encargadoOcho'.$i])."', fechaBajaEncargado='".formateaFechaBD($datos['encargadoDieciseis'.$i])."',accesosEncargado='".$datos['encargadoQuince'.$i]."',provinciaEncargado='".$datos['encargadoDoce'.$i]."',exclusivoEncargado='".$datos['encargadoDieciocho'.$i]."' WHERE codigo=".$datos['codigoEncargadoExiste'.$i]);
		
		$res=$datos['codigoEncargadoExiste'.$i];
	} else {
		$res = consultaBD("INSERT INTO otros_encargados_lopd VALUES (NULL,'".$codigoTrabajo."','".$datos['encargadoDiecisiete'.$i]."','".$datos['encargadoDos'.$i]."','".$datos['encargadoTres'.$i]."','".$datos['encargadoNueve'.$i]."','".$datos['encargadoCuatro'.$i]."','".$datos['encargadoDiez'.$i]."','".$datos['encargadoOnce'.$i]."','".$datos['encargadoCinco'.$i]."','".$datos['encargadoSeis'.$i]."','".$datos['encargadoSiete'.$i]."','".$datos['encargadoTrece'.$i]."','".$datos['encargadoCatorce'.$i]."','".formateaFechaBD($datos['encargadoOcho'.$i])."','".formateaFechaBD($datos['encargadoDieciseis'.$i])."','".$codigo."','".$datos['encargadoQuince'.$i]."','".$datos['encargadoDoce'.$i]."','".$datos['encargadoDieciocho'.$i]."');");
		$res=mysql_insert_id();
	}
	cierraBD();

	return $res;
}

function gestionPredefinida(){
	abreVentanaGestion('Gestión de LOPD','?','','icon-edit','',false,'formLOPD');
		$listado=consultaBD('SELECT codigo, nombreFichero AS texto FROM declaraciones_predefinidos ORDER BY nombreFichero',true);
		$nombres=array();
		$valores=array();
		while($item=mysql_fetch_assoc($listado)){
			array_push($nombres,$item['texto']);
			array_push($valores,$item['codigo']);
		}
		campoSelect('declaracionPredefinida','Declaraciones predefinidas',$nombres,$valores);
		/*campoSelectConsulta('declaracionPredefinida','Declaraciones predefinidas','SELECT codigo, nombreFichero AS texto FROM declaraciones_predefinidos ORDER BY nombreFichero');*/
		campoOculto($_GET['codigoCliente'],'codigoCliente');
	cierraVentanaGestion('index.php?codigoCliente='.$_GET['codigoCliente'],false,true,'Crear','icon-plus-circle');
}

function gestionLOPD(){
	operacionesLOPD();

	abreVentanaGestion('Gestión de LOPD','?','','icon-edit','',false,'formLOPD');

	$datos=compruebaDatos('declaraciones');
	$predefindo=false;

	$paginas=array(
		'Datos Declarante',
		'Responsable del Fichero',
		'Datos para ejercer derechos',
		'Encargado del Tratamiento',
		'Finalidad del Fichero',
		'Origen y Procedencia de los Datos',
		'Tipos de Datos',
		'Cesiones',
		'Tratamiento',
		'Transferencias internacionales',
		'Otros responsables del fichero'
	);

	creaPestaniasAPI($paginas);		
		$pestana = isset($_POST['pestanaActiva']) ? $_POST['pestanaActiva'] : 1;
		campoOculto($pestana,'pestanaActiva');

		// Datos declarante
		abrePestaniaAPI(1, $pestana == 1 ? true : false);  
			abreColumnaCampos();
			
				if (isset($_GET['codigoCliente'])) {
					$codigoCliente = $_GET['codigoCliente'];
				} 
				else if (isset($_POST['declaracionPredefinida'])){
					$datos = recogerDeclaracionPredefinida($_POST['declaracionPredefinida']);
					$codigoCliente = $_POST['codigoCliente'];
					$predefindo = true;
				} 
				else if ($datos) {
					$codigoCliente = $datos['codigoCliente'];
				}

				$cliente = datosRegistro('clientes',$codigoCliente);
			
				campoOculto($codigoCliente,'codigoCliente');
				campoDato('Cliente',$cliente['razonSocial']);			
				campoOculto(formateaFechaWeb($datos['fecha']),'fecha',fecha());
				campoTexto('razon_s','Razón social',$datos,'input-large maximoCaracteres');
				campoTexto('cif_nif','CIF/NIF',$datos,'input-small maximoCaracteres');
				campoTexto('nombre','Nombre',$datos,'input-large maximoCaracteres');
				campoTexto('apellido1','Primer apellido',$datos,'input-large maximoCaracteres');
				campoTexto('apellido2','Segundo apellido',$datos,'input-large maximoCaracteres');
				campoTexto('nif','NIF',$datos,'input-small maximoCaracteres');
				campoTexto('cargo','Cargo',$datos,'input-large maximoCaracteres');

			cierraColumnaCampos();

			abreColumnaCampos();

				campoTexto('dir_postal','Dirección postal',$datos,'input-large maximoCaracteres');
				campoSelectProvinciaAEPD('provincia',$datos);
				campoSelectPaisesAEPD('pais',$datos);
				campoTexto('localidad','Localidad',$datos,'input-large maximoCaracteres');
				campoTexto('postal','Código Postal',$datos,'input-mini pagination-right maximoCaracteres');
				campoTextoSimbolo('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right maximoCaracteres');
				campoTextoSimbolo('fax','Fax','<i class="icon-fax"></i>',$datos,'input-small pagination-right maximoCaracteres');
				campoTextoSimbolo('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large maximoCaracteres');

			cierraColumnaCampos();

		cierraPestaniaAPI();

		// Responsable del fichero
		abrePestaniaAPI(2, $pestana == 2 ? true : false);

			abreColumnaCampos();
		
				echo "<div id='distintoDeclarante'>";
					campoRadio('datosResponsable','Distinto al declarante',$datos,'NO',array('No','Si'),array('NO','SI'));
				echo "</div>";
			
				campoTexto('cif_nif_responsableFichero','CIF/NIF',$datos,'input-small maximoCaracteres');
				campoTexto('dir_postal_responsableFichero','Dirección postal',$datos,'input-large maximoCaracteres');
				campoSelectProvinciaAEPD('provincia_responsableFichero',$datos);
				campoSelectPaisesAEPD('pais_responsableFichero',$datos);
				campoTexto('localidad_responsableFichero','Localidad',$datos,'input-large maximoCaracteres');
				campoTexto('postal_responsableFichero','Código Postal',$datos,'input-mini pagination-right maximoCaracteres');
				campoTextoSimbolo('telefono_responsableFichero','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right maximoCaracteres');
				campoTextoSimbolo('fax_responsableFichero','Fax','<i class="icon-fax"></i>',$datos,'input-small pagination-right maximoCaracteres');
				campoTextoSimbolo('email_responsableFichero','eMail','<i class="icon-envelope"></i>',$datos,'input-large maximoCaracteres');
				campoTexto('n_razon','Razón social',$datos,'input-large maximoCaracteres');
				campoSelectActividad('cap',$datos);
				campoOculto($datos['cap'],'actividadOculta');
				campoTexto('representante_responsableFichero','Representante legal',$datos,'input-large');
				campoTexto('nifRepresentante_responsableFichero','NIF',$datos,'input-large');

			cierraColumnaCampos();
			
			abreColumnaCampos();
			
				campoFecha('fechaInicioResponsable','Inicio del servicio',$datos);
				campoFecha('fechaFinResponsable','Fin del servicio',$datos);
				campoSelectMultiple('serviciosResponsable','¿Donde recibe los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$datos,'selectpicker span3 show-tick','',0);
			
			cierraColumnaCampos();
		
		cierraPestaniaAPI();

		// Datos para ejercer derechos
		abrePestaniaAPI(3, $pestana == 3 ? true : false);

			echo "<div id='distinto'>";
				campoRadio('datosDerecho','Copiar datos desde el',$datos,'NO',array('Declarante','Responsable','Ninguno'),array('SI','NO','0'));
			echo "</div>";
			campoTexto('oficina','Oficina/Dependencia',$datos,'input-large maximoCaracteres');
			campoTexto('nif_cif','CIF/NIF',$datos,'input-small maximoCaracteres');
			campoTexto('dir_postal_derecho','Dirección postal',$datos,'input-large maximoCaracteres');
			campoSelectProvinciaAEPD('provincia_derecho',$datos);
			campoSelectPaisesAEPD('pais_derecho',$datos);
			campoTexto('localidad_derecho','Localidad',$datos,'input-large maximoCaracteres');
			campoTexto('postal_derecho','Código Postal',$datos,'input-mini pagination-right maximoCaracteres');
			campoTextoSimbolo('telefono_derecho','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right maximoCaracteres');
			campoTextoSimbolo('fax_derecho','Fax','<i class="icon-fax"></i>',$datos,'input-small pagination-right maximoCaracteres');
			campoTextoSimbolo('email_derecho','eMail','<i class="icon-envelope"></i>',$datos,'input-large maximoCaracteres ');

		cierraPestaniaAPI();

		// Encargado del tratamiento
		abrePestaniaAPI(4, $pestana == 4 ? true : false);
			
			echo '<div class="hide">';
				echo "<div id='distintoEncargado'>";
					campoRadio('datosEncargado','Distintos al del Declarante','SI','SI');
					campoSelectEncargado($codigoCliente,$datos?$datos['datosEncargado']:'SI',$datos?$datos['n_razon']:'');
				echo "</div>";
				campoTexto('n_razon_encargado','Razón social',$datos,'input-large maximoCaracteres');
				campoTexto('cif_nif_encargado','CIF/NIF',$datos,'input-small maximoCaracteres');
				campoTexto('dir_postal_encargado','Dirección postal',$datos,'input-large maximoCaracteres');
				campoSelectProvinciaAEPD('provincia_encargado',$datos);
				campoSelectPaisesAEPD('pais_encargado',$datos);
				campoTexto('localidad_encargado','Localidad',$datos,'input-large maximoCaracteres');
				campoTexto('postal_encargado','Código Postal',$datos,'input-mini pagination-right maximoCaracteres');
				campoTextoSimbolo('telefono_encargado','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right maximoCaracteres');
				campoTextoSimbolo('fax_encargado','Fax','<i class="icon-fax"></i>',$datos,'input-small pagination-right maximoCaracteres');
				campoTextoSimbolo('email_encargado','eMail','<i class="icon-envelope"></i>',$datos,'input-large maximoCaracteres ');
				echo '<div class="radioSubcontratacion">';
					campoRadio('subcontratacion_encargado','¿Permite la subcontratación?',$datos['subcontratacion_encargado']);
				echo '</div>';
				campoSelectMultiple('donde_encargado','¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$datos,'selectpicker span3 show-tick','',0);
				creaTablaSubContratacion($datos,'fichero');
			echo '</div>';

			creaTablaEncargados($codigoCliente,$datos['codigo']);

		cierraPestaniaAPI();

		// Finalidad del fichero
		abrePestaniaAPI(5, $pestana == 5 ? true : false);
			conexionBD();

			campoTexto('nombreFichero','Nombre fichero/tratamiento',$datos,'input-large maximoCaracteres');
			areaTexto('desc_fin_usos','Descripción de finalidad y usos',$datos,'areaInforme maximoCaracteres');
			campoCheckFinalidades($datos);

			cierraBD();
		cierraPestaniaAPI();

		// Origen y procedencia de los datos
		abrePestaniaAPI(6, $pestana == 6 ? true : false);
			campoCheckOrigenes($datos);
			campoCheckColectivos($datos);
		cierraPestaniaAPI();

		// Tipo de datos
		abrePestaniaAPI(7, $pestana == 7 ? true : false);
			
			campoSelect('nivel','Nivel de medidas de seguridad',array('Básico','Medio','Alto'),array(1,2,3),$datos,'span2 selectpicker show-tick','');
			campoCheckDatosProtegidos($datos);
			campoCheckDatosEspeciales($datos);
			campoCheckDatosIdentificativos($datos);
			campoCheckOtrosDatos($datos);

		cierraPestaniaAPI();

		// Cesiones
		abrePestaniaAPI(8, $pestana == 8 ? true : false);
			campoCheckCesiones($datos);
			campoOculto('','accion');
			campoOculto($datos,'identificador','');
			campoOculto($datos,'numeroRegistro','');
			campoOculto($datos,'revision','NO');
			campoOculto($datos,'nuevaAuditoria','NO');
		cierraPestaniaAPI();

		// Tratamiento
		abrePestaniaAPI(9, $pestana == 9 ? true : false);

			abreColumnaCampos();
				areaTexto('tipoTratamiento','Tipo Tratamiento',$datos);
				areaTexto('baseJuridica','Base Jurídica del tratamiento',$datos);
				campoFechaBlanco('fechaInicio','Fecha de inicio del tratamiento',$datos);
				campoFechaBlanco('fechaBaja','Fecha de baja del tratamiento',$datos);
			cierraColumnaCampos();

			abreColumnaCampos();			
				areaTexto('numInteresados','Nº de interesados afectados',$datos);
				areaTexto('plazo','Plazo o Criterio de conservación de la información',$datos);

				$tratamiento = $datos ? $datos['tratamiento'] : 3;
				campoSelect('tratamiento','Sistema de tratamiento',array('Automatizado','Manual','Mixto'),array('1','2','3'),$tratamiento,'selectpicker span2 show-tick','');

			cierraColumnaCampos(true);
			
			echo '<div>';
				echo '<br/><h3 class="apartadoFormulario">Tipo Tratamiento</h3>';
				abreColumnaCampos();
					campoCheckIndividual('checkTipoTratamiento1','Recogida',$datos);
					campoCheckIndividual('checkTipoTratamiento4','Modificación',$datos);
					campoCheckIndividual('checkTipoTratamiento7','Consulta',$datos);
					campoCheckIndividual('checkTipoTratamiento10','Comunicación',$datos);
					campoCheckIndividual('checkTipoTratamiento13','Comunicación por transmisión al responsable del fichero',$datos);
				cierraColumnaCampos();

				abreColumnaCampos();
					campoCheckIndividual('checkTipoTratamiento2','Registro',$datos);
					campoCheckIndividual('checkTipoTratamiento5','Conservación',$datos);
					campoCheckIndividual('checkTipoTratamiento8','Cotejo',$datos);
					campoCheckIndividual('checkTipoTratamiento11','Supresión',$datos);
					campoCheckIndividual('checkTipoTratamiento14','Comunicación a la Administración Pública competente',$datos);
				cierraColumnaCampos();

				abreColumnaCampos();
					campoCheckIndividual('checkTipoTratamiento3','Estructuración',$datos);
					campoCheckIndividual('checkTipoTratamiento6','Interconexión',$datos);
					campoCheckIndividual('checkTipoTratamiento9','Análisis de conducta',$datos);
					campoCheckIndividual('checkTipoTratamiento12','Destrucción',$datos);
					campoCheckIndividual('checkTipoTratamiento15','Comunicación permitida por ley',$datos);
				cierraColumnaCampos();
			echo '</div>';

		cierraPestaniaAPI();

		// Transferencias internacionales
		abrePestaniaAPI(10, $pestana == 10 ? true : false);

			creaTablaDestinatarios($datos,$predefindo);

		cierraPestaniaAPI();

		// Otros responsables del fichero
		abrePestaniaAPI(11, $pestana == 11 ? true : false);

			creaTablaResponsables($datos);

		cierraPestaniaAPI();

	cierraPestaniasAPI();;
		
	if ($_SESSION['tipoUsuario'] == 'ADMIN'){	
		cierraVentanaGestionAdicional('Guardar y declarar', 'index.php?codigoCliente='.$codigoCliente, true);
	} 
	else {
		cierraVentanaGestion('index.php');
	}
}

function recogerDeclaracionPredefinida($codigo){
	$datos=datosRegistro('declaraciones_predefinidos',$codigo);
	conexionBD();
		$campos=camposTabla('declaraciones');
		foreach ($campos as $campo) {
			if(!isset($datos[$campo])){
				$datos[$campo]=false;
			}
		}
	cierraBD();
	return $datos;
}

//Parte de campos personalizados

function campoSelectProvinciaAEPD($nombreCampo,$datos){
	$valores=array();
	$nombres=array();
	$provinciasAgencia=array(''=>'','01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	
	foreach ($provinciasAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,convertirMinuscula($nombre));
	}

	campoSelect($nombreCampo,'Provincia',$nombres,$valores,$datos,'selectpicker span3 show-tick');
}

function campoSelectActividad($nombreCampo,$datos){
	$valores=array();
	$nombres=array();
	$actividadesAgencia=array('N01'=>'Comercio','N02'=>'Sanidad','N03'=>'Contabilidad, Auditoría y Asesoría Fiscal','N04'=>'Asociaciones y Clubes','N05'=>'Actividades Inmobiliarias','N06'=>'Industria Química y Farmacéutica','N07'=>'Construcción','N08'=>'Turismo y Hostelería','N09'=>'Maquinaria y Medios de Transporte','N10'=>'Educación','N11'=>'Transporte','N12'=>'Seguros Privados','N13'=>'Servicios Informáticos','N14'=>'Actividades relacionadas con los Productos Alimenticios, Bebidas y Tabacos','N15'=>'Agricultura, Ganadería, Explotación Forestal, Caza, Pesca','N16'=>'Entidades Bancarias y Financieras','N17'=>'Producción de Bienes de Consumo','N18'=>'Sector Energético','N19'=>'Actividades Jurídicas, Notarios y Registradores','N20'=>'Actividades Diversas de Servicios Personales','N21'=>'Actividades de Organizaciones Empresariales, Profesionales Y Patronales','N22'=>'Actividades de Servicios Sociales','N23'=>'Publicidad Directa','N24'=>'Servicios de Telecomunicaciones','N25'=>'Actividades relacionadas con los Juegos de Azar y Apuestas','N26'=>'Seguridad','N27'=>'Selección de Personal','N28'=>'Actividades Postales y de Correo (Operadores Postales, Empresas Prestadoras de Servicios Postales, Transportistas, ...)','N29'=>'Investigación y Desarrollo (I+D)','N30'=>'Actividades Políticas, Sindicales y Religiosas','N31'=>'Mutualidades Colaboradoras de los Organismos de la Seguridad Social','N32'=>'Organizacion de Ferias, Exhibiciones, Congresos y otras actividades relacionadas','N33'=>'Solvencia Patrimonial Y Crédito','N34'=>'Inspección Técnica de Vehículos y otros Análisis Técnicos','N35'=>'Comercio y Servicios Electrónicos','N36'=>'Comunidades de Propietarios','N99'=>'Otras Actividades');

	foreach ($actividadesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoSelect($nombreCampo,'Actividad',$nombres,$valores,$datos);
}

function campoCheckFinalidades($datos){
	$finalidadesAgencia=array('400'=>'Gestión de Clientes, Contable, Fiscal y Administrativa','401'=>'Recursos Humanos','402'=>'Gestión de Nóminas','403'=>'Prevención de Riesgos Laborales','404'=>'Prestación de Servicios de Solvencia Patrimonial y Crédito','405'=>'Cumplimiento/Incumplimiento de Obligaciones Dinerarias','406'=>'Servicios Económicos-Financieros y Seguros','407'=>'Análisis de Perfiles','408'=>'Publicidad y Prospección Comercial','409'=>'Prestación de Servicios de Comunicaciones Electrónicas','410'=>'Guías/Repertorios de Servicios de Comunicaciones Electrónicas','411'=>'Comercio Electrónico','412'=>'Prestación de Servicios de Certificación Electrónica','413'=>'Gestión de Asociados o Miembros de Partidos Políticos, Sindicatos, Iglesias, Confesiones o Comunidades Religiosas y Asociaciones, Fundaciones y tras Entidades sin Ánimo de Lucro, cuya finalidad sea Política, Filosófica, Religiosa o Sindical','414'=>'Gestión de Actividades Asociativas, Culturales, Recreativas, Deportivas y Sociales','415'=>'Gestión de Asistencia Social','416'=>'Educación','417'=>'Investigación Epidemiológica y Actividades Análogas','418'=>'Gestión y Control Sanitario','419'=>'Historial Clínico','420'=>'Seguridad Privada','421'=>'Seguridad y Control de acceso a edificios','422'=>'Videovigilancia','423'=>'Fines Estadísticos, Históricos o Científicos','499'=>'Otras Finalidades');
	$valores=array();
	$nombres=array();

	foreach ($finalidadesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	echo "<div id='cajaFinalidades'>";
	campoCheck('checkFinalidad','Finalidades (máximo 6)',$datos,$nombres,$valores,true);
	echo "</div>";
}

function campoCheckOrigenes($datos){
	$origenesAgencia=array('1'=>'El propio interesado o su representante legal','2'=>'Otras personas físicas','3'=>'Fuentes accesibles al público','4'=>'Registros públicos','5'=>'Entidad privada','6'=>'Administraciones Públicas');
	$valores=array();
	$nombres=array();

	foreach ($origenesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	campoCheck('checkOrigen','Orígenes',$datos,$nombres,$valores,true);
}

function campoCheckColectivos($datos){
	$colectivosAgencia=array('01'=>'Empleados','02'=>'Clientes y Usuarios','03'=>'Proveedores','04'=>'Asociados o Miembros','05'=>'Propietarios o Arrendatarios','06'=>'Pacientes','07'=>'Estudiantes','08'=>'Personas de Contacto','09'=>'Padres o Tutores','10'=>'Representante Legal','11'=>'Solicitantes','12'=>'Beneficiarios','13'=>'Cargos Públicos','OTRO'=>'Otro colectivo');
	$valores=array();
	$nombres=array();

	foreach ($colectivosAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	echo "<div id='cajaColectivos'>";
	campoCheck('checkColectivo','Colectivos (máximo 6)',$datos,$nombres,$valores,true);
	echo "</div>";

	echo "<div id='cajaOtroColectivo' class='hide'>";
	areaTexto('otro_col','Otro colectivo',$datos);
	echo "</div>";
}

function campoCheckDatosProtegidos($datos){
	$datosProtegidos=array('1'=>'Ideología','2'=>'Afiliación sindical','3'=>'Religión','4'=>'Creencias');
	$valores=array();
	$nombres=array();

	foreach ($datosProtegidos as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	campoCheck('checkDatosProtegidos','Datos especialmente protegidos',$datos,$nombres,$valores,true);
}

function campoCheckDatosEspeciales($datos){
	$datosEspeciales=array('1'=>'Origen racial o Étnico','2'=>'Salud','3'=>'Vida sexual');
	$valores=array();
	$nombres=array();

	foreach ($datosEspeciales as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	campoCheck('checkDatosEspeciales','Otros datos especialmente protegidos',$datos,$nombres,$valores,true);
}


function campoCheckDatosIdentificativos($datos){
	$datosIdentificativosAgencia=array('1'=>'NIF / DNI','2'=>'Nº SS / Mutualidad','3'=>'Nombre y apellidos','4'=>'Tarjeta sanitaria','5'=>'Dirección','6'=>'Teléfono','7'=>'Firma','8'=>'Huella','9'=>'Imagen / voz','10'=>'Marcas físicas','11'=>'Firma electrónica','13'=>'Correo electrónico','12'=>'Otros datos biométricos','otroIden'=>'Otros datos de carácter identificativo');
	$valores=array();
	$nombres=array();

	foreach ($datosIdentificativosAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoCheck('checkDatosIdentificativos','Datos de carácter identificativo',$datos,$nombres,$valores,true);

	echo "<div id='cajaOtroIdentificativo' class='hide'>";
	areaTexto('ODCI','Otros datos',$datos,'areaTexto maximoCaracteres');
	echo "</div>";
}


function campoCheckOtrosDatos($datos){
	$datosTipificadosAgencia=array('1'=>'Características Personales','2'=>'Académicos y profesionales','3'=>'Detalles del empleo','4'=>'Económicos, financieros y de seguros');
	$valores=array();
	$nombres=array();

	foreach ($datosTipificadosAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoRadio('otrosTiposDatos','Otros tipos de datos',$datos);

	echo "<div id='cajaOtrosDatosTipificados' class='hide'>";
	campoCheck('checkOtrosTiposDatos','Tipos',$datos,$nombres,$valores,true);
	areaTexto('desc_otros_tipos','Otros datos',$datos);
	echo "</div>";
}

function campoCheckCesiones($datos){
	$destinatariosAgencia=array('01'=>'Organizaciones o Personas Directamente Relacionadas con el Responsable','02'=>'Organismos de la Seguridad Social','03'=>'Registros Públicos','04'=>'Colegios Profesionales','05'=>'Administración Tributaria','06'=>'Otros Órganos de la Administración Pública','07'=>'Comisión Nacional del Mercado de Valores','08'=>'Comisión Nacional del Juego','09'=>'Notarios y Procuradores','10'=>'Fuerzas y Cuerpos de Seguridad','11'=>'Organismos de la Unión Europea','12'=>'Entidades Dedicadas al Cumplimiento o Incumplimiento de Obligaciones Dinerarias','13'=>'Bancos, Cajas de Ahorros y Cajas Rurales','14'=>'Entidades Aseguradoras','15'=>'Otras Entidades Financieras','16'=>'Entidades Sanitarias','17'=>'Prestaciones de Servicios de Telecomunicaciones','18'=>'Empresas Dedicadas a Publicidad o Marketing Directo','19'=>'Asociaciones y Organizaciones sin Ánimo de Lucro','20'=>'Sindicatos y Juntas de Personal','21'=>'Administración Pública con Competencia en la Materia','21'=>'Otros destinatarios de cesiones');
	$valores=array();
	$nombres=array();

	foreach ($destinatariosAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoCheck('checkDestinatariosCesiones','Categorías de destinatarios de cesiones',$datos,$nombres,$valores,true);

	echo "<div id='cajaOtrosDestintarios' class='hide'>";
	areaTexto('desc_otros','Otros destinatarios de cesiones',$datos);
	echo "</div>";
}

//Fin parte de campos personalizados


function imprimeDeclaracionesLOPD($cliente,$pendientes='SI'){
	global $_CONFIG;
	$where=obtieneWhereListadoDeclaraciones($pendientes);

	conexionBD();

	if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT codigo, codigoCliente, revision, nuevaAuditoria, fecha, razon_s, telefono, email, numeroRegistro, identificador, nombreFichero FROM declaraciones $where AND codigoCliente=".$cliente." ORDER BY razon_s;");
	} 
	else{
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU'],false,true);
		if($director['director'] == 'SI'){
			$consulta=consultaBD("SELECT declaraciones.codigo, codigoCliente, revision, nuevaAuditoria, declaraciones.fecha, declaraciones.razon_s, declaraciones.telefono, declaraciones.email, numeroRegistro, identificador, nombreFichero FROM declaraciones LEFT JOIN clientes c ON declaraciones.codigoCliente=c.codigo  $where AND c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') AND codigoCliente=".$cliente."  ORDER BY razon_s;");
		} 
		else {
			$consulta=consultaBD("SELECT declaraciones.codigo, codigoCliente, revision, nuevaAuditoria, declaraciones.fecha, declaraciones.razon_s, declaraciones.telefono, declaraciones.email, numeroRegistro, identificador, nombreFichero FROM declaraciones LEFT JOIN clientes c ON declaraciones.codigoCliente=c.codigo $where AND c.empleado LIKE ".$_SESSION['codigoU']." AND codigoCliente=".$cliente."  ORDER BY razon_s;");
		}
	}

	
	while($datos=mysql_fetch_assoc($consulta)){
		$celdasExtra=obtieneCeldasExtraListadoDeclaraciones($pendientes,$datos);
		$fecha=compruebaFechaEnvioDeclaracion($datos['fecha'],$datos['revision'],$datos['nuevaAuditoria'],$pendientes);
		$opciones=obtieneOpcionesDeclaracion($datos,$pendientes);

		echo "<tr>
				<td>".$datos['razon_s']."</td>
				<td>".$datos['nombreFichero']."</td>
				<td class='centro'>".$fecha."</td>
				<td class='nowrap'><a href='tel:".$datos['telefono']."'>".formateaTelefono($datos['telefono'])."</a></td>
				<td><a href='mailto:".$datos['email']."'>".$datos['email']."</a></td>
				
				$celdasExtra

				<td class='centro'>
					<div class='btn-group'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i>  <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='".$_CONFIG['raiz']."gestion-lopd/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li>
						    $opciones
						</ul>
					</div>
				 </td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
	        	</td>
			</tr>";
	}
	cierraBD();
}

function obtieneWhereListadoDeclaraciones($pendientes){
	$res="WHERE numeroRegistro=''";
	if($pendientes=='NO'){
		$res="WHERE numeroRegistro!=''";
	}

	return $res;
}

function obtieneCeldasExtraListadoDeclaraciones($pendientes,$datos){
	$res="";
	if($pendientes=='NO'){
		$res="<td>".$datos['identificador']."</td>";
		$res.="<td>".$datos['numeroRegistro']."</td>";
	}

	return $res;
}


function compruebaFechaEnvioDeclaracion($fechaDeclaracion,$revision,$nuevaAuditoria,$pendientes){
	$res=formateaFechaWeb($fechaDeclaracion);

	$tiempo1=date_create_from_format('Y-m-d',$fechaDeclaracion);
	$tiempo2=date_create_from_format('Y-m-d',fechaBD());

	$diferencia=date_diff($tiempo1,$tiempo2);
	$meses=date_interval_format($diferencia,'%m');

	if($meses>=12 && $nuevaAuditoria=='NO' && $pendientes=='NO'){
		$res=formateaFechaWeb($fechaDeclaracion);
		$res.="<br /><span class='label label-danger'><i class='icon-exclamation-circle'></i> Auditoría pendiente</span>";
	}
	elseif($meses>=6 && $revision=='NO' && $pendientes=='NO'){
		$res=formateaFechaWeb($fechaDeclaracion);
		$res.="<br /><span class='label label-danger'><i class='icon-exclamation-circle'></i> Revisión simple</span>";
	}

	return $res;
}

function obtieneOpcionesDeclaracion($datos,$pendientes){
	global $_CONFIG;
	$res='';

	if($_SESSION['tipoUsuario']=='ADMIN'){
		$res="<li class='divider'></li>
			  <li><a href='".$_CONFIG['raiz']."gestion-lopd/presentaSolicitud.php?codigo=".$datos['codigo']."&cliente=".$datos['codigoCliente']."'><i class='icon-send'></i> Enviar declaración a la AEPD</i></a></li>";
		/*$res="<li class='divider'></li>
			  <li><a onClick=\"alert('El fichero no se podrá declarar hasta la recepción del certificado');\" class='noAjax'><i class='icon-send'></i> Enviar declaración a la AEPD</i></a></li>";*/
	}
	
	if($pendientes=='NO' && $datos['revision']=='NO'){
		$res.="<li class='divider'></li>
			  <li><a href='".$_CONFIG['raiz']."gestion-lopd/index.php?revision=".$datos['codigo']."' class='noAjax'><i class='icon-list-ol'></i> Marcar revisión como realizada</i></a></li>";
	}

	if($pendientes=='NO' && $datos['nuevaAuditoria']=='NO'){
		$res.="<li class='divider'></li>
			  <li><a href='".$_CONFIG['raiz']."gestion-lopd/index.php?nuevaAuditoria=".$datos['codigo']."' class='noAjax'><i class='icon-certificate'></i> Marcar auditoría anual como realizada</i></a></li>";
	}

	return $res;
}

//Parte generación de XML

function presentaDeclaracion($codigo){
	$res=false;
	$datos=consultaBD("SELECT declaraciones.*, razonSocial FROM declaraciones INNER JOIN clientes ON declaraciones.codigoCliente=clientes.codigo WHERE declaraciones.codigo='$codigo';",true,true);

	$fichero=file_get_contents('../documentos/declaraciones/plantilla.xml');
	
	$identificador=generaIdentificadorSolicitud($datos);
	$fichero=rellenaCamposFichero($identificador,$datos,$fichero);
	
	
	if(firmaXML($fichero,$identificador)){
		$res=enviaXML($identificador,$codigo,$datos['codigoCliente']);
	}

	return $res;
}

function enviaXML($identificador,$codigoDeclaracion,$codigoCliente){
	$res=false;

	$soap=new SoapClient('../firma-xml/wsdl.xml');

	$xml=base64_encode(file_get_contents('../documentos/declaraciones/'.$identificador.'.xml'));
	$respuesta=$soap->registrarXml($xml);
	//$respuesta=$soap->probarXml($xml);

	$ficheroAgencia=base64_decode($respuesta);
	if(file_put_contents('../documentos/declaraciones/'.$identificador.'_Agencia.xml', $ficheroAgencia)){
		$res=compruebaResultadoDeclaracion($ficheroAgencia,$identificador,$codigoDeclaracion,$codigoCliente);
	}
	else{
		mensajeError("ha habido un problema al presentar su solicitud. Compruebe la información introducida.");
	}

	return $res;
}

function compruebaResultadoDeclaracion($ficheroAgencia,$identificador,$codigoDeclaracion,$codigoCliente){
	$res=false;

	$ficheroAgencia=simplexml_load_string($ficheroAgencia);		
	$codigoError=$ficheroAgencia->reg_uno->declarante->control->est_err;

	if($codigoError=='00'){
		$numReg=$ficheroAgencia->reg_uno->Control->num_reg;

		conexionBD();
		$res=consultaBD("UPDATE declaraciones SET fecha='".fechaBD()."' ,identificador='$identificador', numeroRegistro='$numReg' WHERE codigo='$codigoDeclaracion'");
		$res=$res && consultaBD("UPDATE actuaciones_cliente SET declaracion_ficheros='1' WHERE codigoCliente='$codigoCliente';");
		cierraBD();

		echo 'Declaración presentada correctamente.<br />Para cualquier trámite con la agencia, utilice la siguiente información:<br /><br /><div class="resaltado">Identificador de solicitud: '.$identificador.'<br />Número de registro: '.$numReg.'</div>';
	}
	elseif($codigoError=='02'){
		echo 'Ha habido un problema al presentar su solicitud.<br />Contacte con el responsable indicando el siguiente código de error: '.$codigoError.' (datos inválidos)';
	}
	else{
		echo 'Ha habido un problema al presentar su solicitud.<br />Contacte con el responsable indicando el siguiente código de error: '.$codigoError;
	}

	echo '<br /><a href="index.php?codigoCliente='.$codigoCliente.'" class="btn btn-propio btn-small"><i class="icon-chevron-left"></i> Volver</a>';

	return $res;
}

function firmaXML($fichero,$identificador){
	$res=true;
	$certificado=array();

	if(openssl_pkcs12_read(file_get_contents('../firma-xml/certificado.pfx'), $certificado,'1')){
		$documentoXML = new DOMDocument();
		$documentoXML->loadXML($fichero);

		$xmlTool = new FR3D\XmlDSig\Adapter\XmlseclibsAdapter();
		$xmlTool->setPrivateKey($certificado['pkey']);
		$xmlTool->setPublicKey($certificado['cert']);
		$xmlTool->addTransform(FR3D\XmlDSig\Adapter\XmlseclibsAdapter::ENVELOPED);

		$xmlTool->sign($documentoXML);
		$documentoXML->save('../documentos/declaraciones/'.$identificador.'.xml');
		if(!$xmlTool->verify($documentoXML)){
			$res=false;
			mensajeError('no se ha podido realizar la firma digital de su declaración.');
		}
	}
	else{
		$res=false;
		mensajeError('no se ha podido leer el certificado digital.');
	}

	return $res;
}


function rellenaCamposFichero($identificador,$datos,$fichero){
	$fichero=str_replace('${fecha}',date('dmY'),$fichero);
	$fichero=str_replace('${hora}',date('His'),$fichero);
	$fichero=str_replace('${identificador}',$identificador,$fichero);

	$fichero=str_replace('${razon_s}',trim($datos['razon_s']),$fichero);
	$fichero=str_replace('${cif_nif}',trim($datos['cif_nif']),$fichero);
	$fichero=str_replace('${nombre}',trim($datos['nombre']),$fichero);
	$fichero=str_replace('${apellido1}',trim($datos['apellido1']),$fichero);
	$fichero=str_replace('${apellido2}',trim($datos['apellido2']),$fichero);
	$fichero=str_replace('${nif}',trim($datos['nif']),$fichero);
	$fichero=str_replace('${cargo}',trim($datos['cargo']),$fichero);
	$fichero=str_replace('${denomina_p}',trim($datos['razonSocial']),$fichero);
	$fichero=str_replace('${dir_postal}',trim($datos['dir_postal']),$fichero);
	$fichero=str_replace('${provincia}',trim($datos['provincia']),$fichero);
	$fichero=str_replace('${localidad}',trim($datos['localidad']),$fichero);
	$fichero=str_replace('${postal}',trim($datos['postal']),$fichero);
	$fichero=str_replace('${telefono}',trim($datos['telefono']),$fichero);
	$fichero=str_replace('${fax}',trim($datos['fax']),$fichero);
	$fichero=str_replace('${email}',trim($datos['email']),$fichero);

	$fichero=str_replace('${cif_responsableFichero}',trim($datos['cif_nif_responsableFichero']),$fichero);
	$fichero=str_replace('${dir_postal_responsableFichero}',trim($datos['dir_postal_responsableFichero']),$fichero);
	$fichero=str_replace('${postal_responsableFichero}',trim($datos['postal_responsableFichero']),$fichero);
	$fichero=str_replace('${localidad_responsableFichero}',trim($datos['localidad_responsableFichero']),$fichero);
	$fichero=str_replace('${provincia_responsableFichero}',trim($datos['provincia_responsableFichero']),$fichero);
	$fichero=str_replace('${telefono_responsableFichero}',trim($datos['telefono_responsableFichero']),$fichero);
	$fichero=str_replace('${fax_responsableFichero}',trim($datos['fax_responsableFichero']),$fichero);
	$fichero=str_replace('${email_responsableFichero}',trim($datos['email_responsableFichero']),$fichero);
	$fichero=str_replace('${n_razon}',trim($datos['n_razon']),$fichero);
	$fichero=str_replace('${cap}',trim($datos['cap']),$fichero);

	$fichero=str_replace('${oficina}',trim($datos['oficina']),$fichero);
	$fichero=str_replace('${nif_cif}',trim($datos['nif_cif']),$fichero);
	$fichero=str_replace('${dir_postal_derecho}',trim($datos['dir_postal_derecho']),$fichero);
	$fichero=str_replace('${localidad_derecho}',trim($datos['localidad_derecho']),$fichero);
	$fichero=str_replace('${postal_derecho}',trim($datos['postal_derecho']),$fichero);
	$fichero=str_replace('${provincia_derecho}',trim($datos['provincia_derecho']),$fichero);
	$fichero=str_replace('${telefono_derecho}',trim($datos['telefono_derecho']),$fichero);
	$fichero=str_replace('${fax_derecho}',trim($datos['fax_derecho']),$fichero);
	$fichero=str_replace('${email_derecho}',trim($datos['email_derecho']),$fichero);

	$fichero=str_replace('${n_razon_encargado}',trim($datos['n_razon_encargado']),$fichero);
	$fichero=str_replace('${cif_nif_encargado}',trim($datos['cif_nif_encargado']),$fichero);
	$fichero=str_replace('${dir_postal_encargado}',trim($datos['dir_postal_encargado']),$fichero);
	$fichero=str_replace('${localidad_encargado}',trim($datos['localidad_encargado']),$fichero);
	$fichero=str_replace('${postal_encargado}',trim($datos['postal_encargado']),$fichero);
	$fichero=str_replace('${provincia_encargado}',trim($datos['provincia_encargado']),$fichero);
	$fichero=str_replace('${telefono_encargado}',trim($datos['telefono_encargado']),$fichero);
	$fichero=str_replace('${fax_encargado}',trim($datos['fax_encargado']),$fichero);
	$fichero=str_replace('${email_encargado}',trim($datos['email_encargado']),$fichero);

	$fichero=str_replace('${fichero}',trim($datos['nombreFichero']),$fichero);
	$fichero=str_replace('${desc_fin_usos}',trim($datos['desc_fin_usos']),$fichero);

	$finalidades=obtieneValorCampoAgrupado($datos,'checkFinalidad',25);
	$fichero=str_replace('${finalidades}',$finalidades,$fichero);

	$fichero=sustituyeCheck('checkOrigen0',trim($datos['checkOrigen0']),$fichero);
	$fichero=sustituyeCheck('checkOrigen1',trim($datos['checkOrigen1']),$fichero);
	$fichero=sustituyeCheck('checkOrigen2',trim($datos['checkOrigen2']),$fichero);
	$fichero=sustituyeCheck('checkOrigen3',trim($datos['checkOrigen3']),$fichero);
	$fichero=sustituyeCheck('checkOrigen4',trim($datos['checkOrigen4']),$fichero);
	$fichero=sustituyeCheck('checkOrigen5',trim($datos['checkOrigen5']),$fichero);

	$colectivos=obtieneValorCampoAgrupado($datos,'checkColectivo',13);
	$fichero=str_replace('${colectivos}',$colectivos,$fichero);
	$fichero=str_replace('${otro_col}',trim($datos['otro_col']),$fichero);
	$fichero=str_replace('${nivel}',trim($datos['nivel']),$fichero);

	$fichero=sustituyeCheck('checkProtegido0',trim($datos['checkDatosProtegidos0']),$fichero);
	$fichero=sustituyeCheck('checkProtegido1',trim($datos['checkDatosProtegidos1']),$fichero);
	$fichero=sustituyeCheck('checkProtegido2',trim($datos['checkDatosProtegidos2']),$fichero);
	$fichero=sustituyeCheck('checkProtegido3',trim($datos['checkDatosProtegidos3']),$fichero);

	$fichero=sustituyeCheck('checkEspecial0',trim($datos['checkDatosEspeciales0']),$fichero);
	$fichero=sustituyeCheck('checkEspecial1',trim($datos['checkDatosEspeciales1']),$fichero);
	$fichero=sustituyeCheck('checkEspecial2',trim($datos['checkDatosEspeciales2']),$fichero);

	$fichero=sustituyeCheck('checkIdenti0',trim($datos['checkDatosIdentificativos0']),$fichero);
	$fichero=sustituyeCheck('checkIdenti1',trim($datos['checkDatosIdentificativos1']),$fichero);
	$fichero=sustituyeCheck('checkIdenti2',trim($datos['checkDatosIdentificativos2']),$fichero);
	$fichero=sustituyeCheck('checkIdenti3',trim($datos['checkDatosIdentificativos3']),$fichero);
	$fichero=sustituyeCheck('checkIdenti4',trim($datos['checkDatosIdentificativos4']),$fichero);
	$fichero=sustituyeCheck('checkIdenti5',trim($datos['checkDatosIdentificativos5']),$fichero);
	$fichero=sustituyeCheck('checkIdenti6',trim($datos['checkDatosIdentificativos6']),$fichero);
	$fichero=sustituyeCheck('checkIdenti7',trim($datos['checkDatosIdentificativos7']),$fichero);
	$fichero=sustituyeCheck('checkIdenti8',trim($datos['checkDatosIdentificativos8']),$fichero);
	$fichero=sustituyeCheck('checkIdenti9',trim($datos['checkDatosIdentificativos9']),$fichero);
	$fichero=sustituyeCheck('checkIdenti10',trim($datos['checkDatosIdentificativos10']),$fichero);
	$fichero=sustituyeCheck('checkIdenti11',trim($datos['checkDatosIdentificativos11']),$fichero);
	$fichero=sustituyeCheck('checkIdenti12',trim($datos['checkDatosIdentificativos12']),$fichero);
	$fichero=str_replace('${ODCI}',trim($datos['ODCI']),$fichero);

	$tiposDatos=obtieneValorCampoAgrupado($datos,'checkOtrosDatos',7);
	$fichero=str_replace('${otrosIden}',$tiposDatos,$fichero);
	$fichero=str_replace('${desc_otros_tipos}',trim($datos['desc_otros_tipos']),$fichero);

	$fichero=str_replace('${tratamiento}',trim($datos['tratamiento']),$fichero);

	$cesiones=obtieneValorCampoAgrupado($datos,'checkDestinatariosCesiones',21);
	$fichero=str_replace('${cesiones}',$cesiones,$fichero);

	$fichero=str_replace('${desc_otros}',trim($datos['desc_otros']),$fichero);

	$fichero=str_replace('&','',$fichero);//Sustitución del & por su equivalente en HTML, ya que si no da error en el loadXML().

	return $fichero;
}

function generaIdentificadorSolicitud($datos){
	$mes=strtoupper(dechex(date('n')));
	return $datos['cif_nif_responsableFichero'].date('d').$mes.date('YHis');
}

function obtieneValorCampoAgrupado($datos,$indice,$tam){
	$res='';
	for($i=0;$i<$tam;$i++){
		if($datos[$indice.$i]!='NO'){
			$res.=$datos[$indice.$i].';';
		}
	}
	
	$res=quitaUltimaComa($res,1);
	return $res;
}

function sustituyeCheck($etiqueta,$valor,$fichero){
	if($valor=='NO'){
		$valor=0;
	}
	else{
		$valor=1;
	}

	return str_replace('${'.$etiqueta.'}',$valor,$fichero);
}

//Fin parte generación de XML

function obtieneCliente(){
	$datos=arrayFormulario();//Recibirá el codigo como si vienera de un campo del formulario
	/*$cliente=consultaBD("SELECT c.*,f.actividad_responsableFichero AS actividad, fc.* FROM clientes c LEFT JOIN formularioLOPD f ON c.codigo=f.codigoCliente LEFT JOIN ficheros_cliente fc ON c.codigo=fc.codigoCliente WHERE c.codigo=".$datos['codigo'],true,true);*/
	$paises=array(''=>'','NULL'=>'',3=>'AF',4=>'AL',5=>'DE',6=>'AD',7=>'AO',9=>'AG',10=>'SA',11=>'DZ',12=>'AR',13=>'AM',15=>'AU',16=>'AT',17=>'AZ',18=>'BS',19=>'BH',20=>'BD',21=>'BB',22=>'BE',23=>'BZ',24=>'BJ',26=>'BY',28=>'BO',30=>'BA',31=>'BW',32=>'BR',33=>'BN',34=>'BG',35=>'BF',36=>'BI',37=>'BT',38=>'CV',40=>'KH',41=>'CM',42=>'CA',45=>'TD',46=>'CZ',47=>'CL',48=>'CN',49=>'CY',52=>'CO',53=>'KM',54=>'CG',55=>'CD',57=>'KP',58=>'KR',59=>'CI',60=>'CR',61=>'HR',62=>'CU',64=>'DK',65=>'DM',66=>'DO',67=>'EC',68=>'EG',69=>'SV',70=>'AE',71=>'ER',72=>'SK',73=>'SI',74=>'ES',75=>'US',76=>'EP',77=>'ET',78=>'FO',79=>'PH',80=>'FI',81=>'FJ',82=>'FR',83=>'GA',84=>'GM',85=>'GE',86=>'GH',88=>'GD',89=>'GR',90=>'GL',92=>'GT',93=>'GG',94=>'GN',95=>'GW',96=>'GQ',97=>'GY',98=>'HT',99=>'HM',100=>'HK',101=>'HU',102=>'IN',103=>'ID',104=>'IQ',105=>'IR',106=>'IE',107=>'IS',108=>'IL',109=>'IT',110=>'JM',111=>'JP',113=>'JO',114=>'KZ',115=>'KE',116=>'KG',117=>'KI',119=>'KW',121=>'LS',122=>'LV',123=>'LB',124=>'LR',125=>'LY',126=>'LI',127=>'LT',128=>'LU',130=>'MK',131=>'MG',132=>'MY',133=>'MW',134=>'MV',135=>'ML',136=>'MT',140=>'MA',141=>'MH',142=>'MU',143=>'MR',145=>'MX',146=>'FM',148=>'MC',149=>'MN',150=>'ME',152=>'MZ',154=>'NA',155=>'NR',157=>'NP',158=>'NI',159=>'NE',160=>'NG',163=>'NO',166=>'NZ',167=>'OM',169=>'NL',170=>'PK',171=>'PW',173=>'PA',174=>'PG',175=>'PY',176=>'PE',179=>'PL',180=>'PT',181=>'PR',182=>'GB',43=>'QA',184=>'RO',185=>'RU',189=>'WS',193=>'SM',196=>'VC',198=>'LC',199=>'ST',200=>'SN',201=>'RS',202=>'SC',203=>'SL',204=>'SG',207=>'SY',208=>'SO',209=>'LK',210=>'S>',211=>'ZA',212=>'SD',214=>'SE',215=>'CH',216=>'SR',218=>'TH',219=>'TW',220=>'TZ',221=>'TJ',225=>'TO',227=>'TT',228=>'TN',230=>'TM',231=>'TU',232=>'TV',233=>'UA',234=>'UG',235=>'UY',236=>'UZ',237=>'VU',239=>'VE',240=>'VN',244=>'YE',246=>'ZM',247=>'ZW');
	$cliente=consultaBD("SELECT c.* FROM clientes c WHERE c.codigo=".$datos['codigo'],true,true);
	$cliente['pais']=$paises[$cliente['pais']];
	echo json_encode($cliente);//IMPORTANTE: un echo, no un return, y convierte el array en formato JSON.
}

function estadisticasDeclaracionesRestrict($cliente){
	$res=array();

	conexionBD();

	if($_SESSION['tipoUsuario'] == 'ADMIN'){
		$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM declaraciones WHERE codigoCliente=".$cliente." ORDER BY razon_s;",false, true);
	} else {
		$director = consultaBD("SELECT * FROM usuarios WHERE codigo=".$_SESSION['codigoU']." AND codigoCliente=".$cliente,false, true);
		if($director['director'] == 'SI'){
			$consulta=consultaBD("SELECT COUNT(declaraciones.codigo) AS total FROM declaraciones LEFT JOIN clientes c ON declaraciones.codigoCliente=c.codigo WHERE c.empleado IN (SELECT codigo FROM usuarios WHERE tipo LIKE '".$director['tipo']."') AND codigoCliente=".$cliente." ORDER BY declaraciones.razon_s;",false, true);
		} else {
			$consulta=consultaBD("SELECT COUNT(declaraciones.codigo) AS total FROM declaraciones LEFT JOIN clientes c ON declaraciones.codigoCliente=c.codigo WHERE c.empleado LIKE ".$_SESSION['codigoU']." AND codigoCliente=".$cliente." ORDER BY declaraciones.razon_s;", false, true);
		}
	}
	$res['total'] = $consulta['total'];

	cierraBD();

	return $res;

}

function recogeDatosEncargado(){
	$cliente=$_POST['codigoCliente'];
	
	$trabajo=consultaBD('SELECT * FROM trabajos WHERE codigoCliente='.$cliente.' AND codigoServicio=6',true,true);
	$consultoria=recogerFormularioServicios($trabajo);

	if($trabajo){
		$res=json_encode($consultoria);
	} 
	else {//Valores por defecto
		$res=array(
			'pregunta3'=>'',
			'pregunta9'=>'',
			'pregunta4'=>'',
			'pregunta11'=>'',
			'pregunta5'=>'',
			'pregunta10'=>'',
			'pregunta6'=>'',
			'pregunta12'=>'',
			'pregunta7'=>''
		);

		$res=json_encode($res);
	}

	echo $res;
}

function campoSelectPaisesAEPD($nombreCampo,$datos=false){
	$valores=array();
	$nombres=array();
	$paisesAgencia=array(''=>'','VP'=>'INTERNACIONAL','AF'=>'AFGANISTAN','AL'=>'ALBANIA','DE'=>'ALEMANIA','AD'=>'ANDORRA','AO'=>'ANGOLA','AG'=>'ANTIGUA Y BARBUDA','SA'=>'ARABIA SAUDI','DZ'=>'ARGELIA','AR'=>'ARGENTINA','AM'=>'ARMENIA','AU'=>'AUSTRALIA','AT'=>'AUSTRIA','AZ'=>'AZERBAIYAN','BS'=>'BAHAMAS','BH'=>'BAHREIN','BD'=>'BANGLADESH','BB'=>'BARBADOS','BY'=>'BIELORRUSIA','BE'=>'BELGICA','BZ'=>'BELICE','BJ'=>'BENIN','BT'=>'BHUTÁN','BO'=>'BOLIVIA','BA'=>' BOSNIA HERZEGOVINA','BW'=>'BOTSWANA','BR'=>'BRASIL','BN'=>'BRUNEI DARUSSALAM','BG'=>'BULGARIA','BF'=>'BURKINA FASO','BI'=>'BURUNDI','CV'=>'CABO VERDE','KH'=>'CAMBOYA','CM'=>'CAMERUN','CA'=>'CANADA','TD'=>'CHAD','CL'=>'CHILE','CN'=>'CHINA','CY'=>'CHIPRE','CO'=>'COLOMBIA','KM'=>'COMORAS','CG'=>'CONGO','CI'=>'COSTA DE MARFIL','CR'=>'COSTA RICA','HR'=>'CROACIA','CU'=>'CUBA','DK'=>'DINAMARCA DJ DJIBOUTI','DM'=>'DOMINICA','EC'=>'ECUADOR','EG'=>'EGIPTO','SV'=>'EL SALVADOR','AE'=>'EMIRATOS ARABES','ER'=>'ERITREA','SI'=>'ESLOVENIA','ES'=>'ESPAÑA','US'=>'ESTADOS UNIDOS','EP'=>'EEUU - ESCUDO PRIVACIDAD EE ESTONIA','ET'=>'ETIOPIA','RU'=>'FEDERACIÓN DE RUSIA','FJ'=>'FIJI','PH'=>'FILIPINAS','FI'=>'FINLANDIA','FR'=>'FRANCIA','GA'=>'GABON','GM'=>'GAMBIA','GE'=>'GEORGIA','GH'=>'GHANA','GD'=>'GRANADA','GR'=>'GRECIA','GL'=>'GROENLANDIA','GT'=>'GUATEMALA','GG'=>'GUERNSEY','GN'=>'GUINEA','GW'=>'GUINEA BISSAU','GQ'=>'GUINEA ECUATORIAL','GY'=>'GUYANA','HT'=>'HAITI','HN'=>'HONDURAS','HK'=>'HONG KONG','HU'=>'HUNGRIA','IN'=>'INDIA','ID'=>'INDONESIA','IR'=>'IRAN','IQ'=>'IRAQ','IE'=>'IRLANDA','IM'=>'ISLA DE MAN','IS'=>'ISLANDIA','FO'=>'ISLAS FEROE','MH'=>'ISLAS MARSHALL','SB'=>'ISLAS SALOMÓN','IL'=>'ISRAEL','IT'=>'ITALIA','JM'=>'JAMAICA','JP'=>'JAPON','JO'=>'JORDANIA','KZ'=>'KAZAJSTÁN','KE'=>'KENIA','KG'=>'KIRGUISTÁN','KI'=>'KIRIBATI','KW'=>'KUWAIT','LS'=>'LESOTHO','LV'=>'LETONIA','LB'=>'LIBANO','LR'=>'LIBERIA','LY'=>'LIBIA','LI'=>'LIECHTENSTEIN','LT'=>'LITUANIA','LU'=>'LUXEMBURGO','MK'=>'MACEDONIA','MG'=>'MADAGASCAR','MY'=>'MALASIA','MW'=>'MALAWI','MV'=>'MALDIVAS','ML'=>'MALÍ','MT'=>'MALTA','MA'=>'MARRUECOS','MU'=>'MAURICIO','MR'=>'MAURITANIA','MX'=>'MEJICO','FM'=>'MICRONESIA','MC'=>'MONACO','MN'=>'MONGOLIA','ME'=>'MONTENEGRO','MZ'=>'MOZAMBIQUE','MM'=>'MYANMAR','NA'=>'NAMIBIA','NR'=>'NAURU','NP'=>'NEPAL','NI'=>'NICARAGUA','NE'=>'NÍGER','NG'=>'NIGERIA','NO'=>'NORUEGA','NZ'=>'NUEVA ZELANDA','OM'=>'OMÁN','NL'=>'PAISES BAJOS','PW'=>'PALAU','PA'=>'PANAMA','PG'=>'PAPUA NUEVA GUINEA','PK'=>'PAQUISTAN','PY'=>'PARAGUAY','PE'=>'PERU','PL'=>'POLONIA','PT'=>'PORTUGAL','PR'=>'PUERTO RICO','QA'=>'QATAR','GB'=>'REINO UNIDO','CF'=>'REP. CENTROAFRICANA','CZ'=>'REPÚBLICA CHECA','KR'=>'REPÚBLICA DE COREA','DO'=>'REPÚBLICA DOMINICANA','MD'=>'REPÚBLICA DE MOLDOVA','CD'=>'REP. DEM. DEL CONGO','KP'=>'REP. DEM. POP. DE COREA','LA'=>'REP. DEM. POP. DE LAOS','TL'=>'REP. DEM. DE TIMOR-LESTE','SK'=>'REPÚBLICA ESLOVACA','RO'=>'RUMANIA','RW'=>'RWANDA','KN'=>'SAINT KITTS Y NEVIS','WS'=>'SAMOA','SM'=>'SAN MARINO','VC'=>'S.VICENTE Y GRANADINAS','LC'=>'SANTA LUCÍA','ST'=>'SANTO TOMÉ Y PRÍNCIPE','SN'=>'SENEGAL','RS'=>'SERBIA','SC'=>'SEYCHELLES','SL'=>'SIERRA LEONA','SG'=>'SINGAPUR','SY'=>'SIRIA','SO'=>'SOMALIA','LK'=>'SRI LANKA','ZA'=>'SUDAFRICA','SD'=>'SUDAN','SE'=>'SUECIA','CH'=>'SUIZA','SR'=>'SURINAM','SZ'=>'SWAZILANDIA','TH'=>'TAILANDIA','TW'=>'TAIWAN','TZ'=>'TANZANIA','TJ'=>'TAYIKISTÁN','TG'=>'TOGO','TO'=>'TONGA','TT'=>'TRINIDAD Y TOBAGO','TN'=>'TUNEZ','TM'=>'TURKMENISTÁN','TR'=>'TURQUIA','TV'=>'TUVALU','UA'=>'UCRANIA','UG'=>'UGANDA','UY'=>'URUGUAY','UZ'=>'UZBEKISTÁN','VU'=>'VANUATU','VE'=>'VENEZUELA','VN'=>'VIETNAM','YE'=>'YEMEN','YU'=>'YUGOSLAVIA','ZR'=>'ZAIRE','ZM'=>'ZAMBIA','ZW'=>'ZIMBABWE');

	foreach ($paisesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	if($datos==false){
		$datos[$nombreCampo]='ES';
	}
	campoSelect($nombreCampo,'País',$nombres,$valores,$datos,'selectpicker span3 show-tick');
}

function campoSelectPaisesAEPDTabla($nombreCampo,$datos=false){
	$valores=array();
	$nombres=array();
	$paisesAgencia=array('VP'=>'INTERNACIONAL','AF'=>'AFGANISTAN','AL'=>'ALBANIA','DE'=>'ALEMANIA','AD'=>'ANDORRA','AO'=>'ANGOLA','AG'=>'ANTIGUA Y BARBUDA','SA'=>'ARABIA SAUDI','DZ'=>'ARGELIA','AR'=>'ARGENTINA','AM'=>'ARMENIA','AU'=>'AUSTRALIA','AT'=>'AUSTRIA','AZ'=>'AZERBAIYAN','BS'=>'BAHAMAS','BH'=>'BAHREIN','BD'=>'BANGLADESH','BB'=>'BARBADOS','BY'=>'BIELORRUSIA','BE'=>'BELGICA','BZ'=>'BELICE','BJ'=>'BENIN','BT'=>'BHUTÁN','BO'=>'BOLIVIA','BA'=>' BOSNIA HERZEGOVINA','BW'=>'BOTSWANA','BR'=>'BRASIL','BN'=>'BRUNEI DARUSSALAM','BG'=>'BULGARIA','BF'=>'BURKINA FASO','BI'=>'BURUNDI','CV'=>'CABO VERDE','KH'=>'CAMBOYA','CM'=>'CAMERUN','CA'=>'CANADA','TD'=>'CHAD','CL'=>'CHILE','CN'=>'CHINA','CY'=>'CHIPRE','CO'=>'COLOMBIA','KM'=>'COMORAS','CG'=>'CONGO','CI'=>'COSTA DE MARFIL','CR'=>'COSTA RICA','HR'=>'CROACIA','CU'=>'CUBA','DK'=>'DINAMARCA DJ DJIBOUTI','DM'=>'DOMINICA','EC'=>'ECUADOR','EG'=>'EGIPTO','SV'=>'EL SALVADOR','AE'=>'EMIRATOS ARABES','ER'=>'ERITREA','SI'=>'ESLOVENIA','ES'=>'ESPAÑA','US'=>'ESTADOS UNIDOS','EP'=>'EEUU - ESCUDO PRIVACIDAD EE ESTONIA','ET'=>'ETIOPIA','RU'=>'FEDERACIÓN DE RUSIA','FJ'=>'FIJI','PH'=>'FILIPINAS','FI'=>'FINLANDIA','FR'=>'FRANCIA','GA'=>'GABON','GM'=>'GAMBIA','GE'=>'GEORGIA','GH'=>'GHANA','GD'=>'GRANADA','GR'=>'GRECIA','GL'=>'GROENLANDIA','GT'=>'GUATEMALA','GG'=>'GUERNSEY','GN'=>'GUINEA','GW'=>'GUINEA BISSAU','GQ'=>'GUINEA ECUATORIAL','GY'=>'GUYANA','HT'=>'HAITI','HN'=>'HONDURAS','HK'=>'HONG KONG','HU'=>'HUNGRIA','IN'=>'INDIA','ID'=>'INDONESIA','IR'=>'IRAN','IQ'=>'IRAQ','IE'=>'IRLANDA','IM'=>'ISLA DE MAN','IS'=>'ISLANDIA','FO'=>'ISLAS FEROE','MH'=>'ISLAS MARSHALL','SB'=>'ISLAS SALOMÓN','IL'=>'ISRAEL','IT'=>'ITALIA','JM'=>'JAMAICA','JP'=>'JAPON','JO'=>'JORDANIA','KZ'=>'KAZAJSTÁN','KE'=>'KENIA','KG'=>'KIRGUISTÁN','KI'=>'KIRIBATI','KW'=>'KUWAIT','LS'=>'LESOTHO','LV'=>'LETONIA','LB'=>'LIBANO','LR'=>'LIBERIA','LY'=>'LIBIA','LI'=>'LIECHTENSTEIN','LT'=>'LITUANIA','LU'=>'LUXEMBURGO','MK'=>'MACEDONIA','MG'=>'MADAGASCAR','MY'=>'MALASIA','MW'=>'MALAWI','MV'=>'MALDIVAS','ML'=>'MALÍ','MT'=>'MALTA','MA'=>'MARRUECOS','MU'=>'MAURICIO','MR'=>'MAURITANIA','MX'=>'MEJICO','FM'=>'MICRONESIA','MC'=>'MONACO','MN'=>'MONGOLIA','ME'=>'MONTENEGRO','MZ'=>'MOZAMBIQUE','MM'=>'MYANMAR','NA'=>'NAMIBIA','NR'=>'NAURU','NP'=>'NEPAL','NI'=>'NICARAGUA','NE'=>'NÍGER','NG'=>'NIGERIA','NO'=>'NORUEGA','NZ'=>'NUEVA ZELANDA','OM'=>'OMÁN','NL'=>'PAISES BAJOS','PW'=>'PALAU','PA'=>'PANAMA','PG'=>'PAPUA NUEVA GUINEA','PK'=>'PAQUISTAN','PY'=>'PARAGUAY','PE'=>'PERU','PL'=>'POLONIA','PT'=>'PORTUGAL','PR'=>'PUERTO RICO','QA'=>'QATAR','GB'=>'REINO UNIDO','CF'=>'REP. CENTROAFRICANA','CZ'=>'REPÚBLICA CHECA','KR'=>'REPÚBLICA DE COREA','DO'=>'REPÚBLICA DOMINICANA','MD'=>'REPÚBLICA DE MOLDOVA','CD'=>'REP. DEM. DEL CONGO','KP'=>'REP. DEM. POP. DE COREA','LA'=>'REP. DEM. POP. DE LAOS','TL'=>'REP. DEM. DE TIMOR-LESTE','SK'=>'REPÚBLICA ESLOVACA','RO'=>'RUMANIA','RW'=>'RWANDA','KN'=>'SAINT KITTS Y NEVIS','WS'=>'SAMOA','SM'=>'SAN MARINO','VC'=>'S.VICENTE Y GRANADINAS','LC'=>'SANTA LUCÍA','ST'=>'SANTO TOMÉ Y PRÍNCIPE','SN'=>'SENEGAL','RS'=>'SERBIA','SC'=>'SEYCHELLES','SL'=>'SIERRA LEONA','SG'=>'SINGAPUR','SY'=>'SIRIA','SO'=>'SOMALIA','LK'=>'SRI LANKA','ZA'=>'SUDAFRICA','SD'=>'SUDAN','SE'=>'SUECIA','CH'=>'SUIZA','SR'=>'SURINAM','SZ'=>'SWAZILANDIA','TH'=>'TAILANDIA','TW'=>'TAIWAN','TZ'=>'TANZANIA','TJ'=>'TAYIKISTÁN','TG'=>'TOGO','TO'=>'TONGA','TT'=>'TRINIDAD Y TOBAGO','TN'=>'TUNEZ','TM'=>'TURKMENISTÁN','TR'=>'TURQUIA','TV'=>'TUVALU','UA'=>'UCRANIA','UG'=>'UGANDA','UY'=>'URUGUAY','UZ'=>'UZBEKISTÁN','VU'=>'VANUATU','VE'=>'VENEZUELA','VN'=>'VIETNAM','YE'=>'YEMEN','YU'=>'YUGOSLAVIA','ZR'=>'ZAIRE','ZM'=>'ZAMBIA','ZW'=>'ZIMBABWE');

	
	foreach ($paisesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	if($datos==false){
		$datos[$nombreCampo]='ES';
	}
	campoSelect($nombreCampo,'País',$nombres,$valores,$datos,'selectpicker span3 show-tick',"data-live-search='true'",1);
}

function campoDestinatarios($nombreCampo,$datos=false){
	$valores=array();
	$nombres=array();
	$paisesAgencia=array('01'=>'COMPAÑIAS FILIALES','02'=>'COMPAÑIA MATRIZ','03'=>'COMPAÑIAS DEL GRUPO','04'=>'PRESTADORES DE SERVICIO','05'=>'UNIVERSIDADES Y CENTROS EDUCATIVOS','06'=>'ÓRGANOS PÚBLICOS DE OTROS ESTADOS','07'=>'ORGANISMOS INTERNACIONALES','08'=>'ENTIDADES SANITARIAS','09'=>'ÓRGANOS JUDICIALES','10'=>'ENTIDADES FINANCIERAS','11'=>'DATOS DE PASAJEROS CON DESTINO A LOS ORGANISMOS DE FRONTERAS DE EEUU Y CANADÁ');
	foreach ($paisesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoSelect($nombreCampo,'Destinatarios',$nombres,$valores,$datos,'selectpicker span3 show-tick',"data-live-search='true'",1);
}

function creaTablaDestinatarios($datos,$predefindo){
	conexionBD();

	echo "	                 
			<div class='table-responsive centroDestinatarios'>
				<table class='table table-striped tabla-simple' id='tablaDestinatarios'>
				  	<thead>
				    	<tr>
				            <th> Destinatarios en países con nivel de protección adecuado </th>
				            <th> Destinatarios en países sin nivel de protección adecuado </th>
				            <th> Otros destinatarios de Transferencias Internacionales </th>
				            <th> Transferencias internacionales con autorización del Director de la AEPD </th>				            
				            <th> País </th>
				            <th> Categoría </th>
				            <th> Otros </th>
				            <th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		$j=1;
				  		if($datos){
				  			if($predefindo){
				  				$consulta=consultaBD("SELECT * FROM transferencias_internacionales_predefinidos WHERE codigoDeclaracion=".$datos['codigo']);
				  			} else {
				  				$consulta=consultaBD("SELECT * FROM transferencias_internacionales WHERE codigoDeclaracion=".$datos['codigo']);
				  			}
				  			while($item=mysql_fetch_assoc($consulta)){
				  				echo '<tr>';
				  					campoTextoTabla('destNSadecuado'.$i,$item['destNSadecuado'],'input-small');
				  					campoTextoTabla('destNSinadecuado'.$i,$item['destNSinadecuado'],'input-small');
				  					campoTextoTabla('otrosDestTI'.$i,$item['otrosDestTI'],'input-small');
				  					campoTextoTabla('tiautorizacionAEPD'.$i,$item['tiautorizacionAEPD'],'input-small');
				  					campoSelectPaisesAEPDTabla('paisTrans'.$i,$item['pais']);
				  					campoDestinatarios('categoriaTrans'.$i,$item['categoria']);
				  					areaTextoTabla('otrosTrans'.$i,$item['otros']); 
				  				echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
				  				</tr>";
				  				$i++;
				  				$j++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr>';
				  					campoTextoTabla('destNSadecuado'.$i,'','input-small');
				  					campoTextoTabla('destNSinadecuado'.$i,'','input-small');
				  					campoTextoTabla('otrosDestTI'.$i,'','input-small');
				  					campoTextoTabla('tiautorizacionAEPD'.$i,'','input-small');
				  					campoSelectPaisesAEPDTabla('paisTrans'.$i);
				  					campoDestinatarios('categoriaTrans'.$i);
				  					areaTextoTabla('otrosTrans'.$i);
				  			echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
							</tr>";
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaDestinatarios\");'><i class='icon-plus'></i> Añadir destinatario</button>
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaDestinatarios\");'><i class='icon-trash'></i> Eliminar destinatario</button> 
				</div>
			</div>";

	cierraBD();
}

function campoSelectEncargado($codigoCliente,$valor='NO',$responsable=''){
	$clase=$valor=='NO'?'hide':'';
	$nombres=array('');
	$valores=array('NULL');
	$trabajo=consultaBD('SELECT trabajos.* FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE codigoCliente='.$codigoCliente,true,true);
	$formulario=recogerFormularioServicios($trabajo);
	$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
	foreach ($encargados as $key => $value){
		if($formulario['pregunta'.$value]=='SI'){
			$i=$value+1;
			array_push($nombres, $formulario['pregunta'.$i]);
			array_push($valores, 'ENCARGADOFIJO_'.$value);
		}
	}
	$encargados=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
	while($item=mysql_fetch_assoc($encargados)){
		array_push($nombres, $item['nombreEncargado']);
		array_push($valores, 'ENCARGADO_'.$item['codigo']);
	}
	if($responsable!=''){
		array_push($nombres, $responsable);
		array_push($valores, 'RESP');
	}
	echo '<div id="selectEncargado" class="'.$clase.'">';
	campoSelect('recogerDatosEncargado','Recoger datos del Encargado',$nombres,$valores);
	campoOculto($trabajo['codigo'],'codigoTrabajo');
	echo '</div>';
}

function recogeDatosEncargadoSelect(){
	$codigoTrabajo=$_POST['codigoTrabajo'];
	$codigo=$_POST['codigo'];
	$res=array();
	$codigo=explode('_', $codigo);
	if($codigo[0]=='ENCARGADO'){
		$encargado=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigo='.$codigo[1],true,true);
		$res['n_razon']=$encargado['nombreEncargado'];
  		$res['cif_nif']=$encargado['cifEncargado'];
  		$res['dir_postal']=$encargado['direccionEncargado'];
  		$res['provincia']=$encargado['provinciaEncargado'];
  		$res['localidad']=$encargado['localidadEncargado'];
  		$res['postal']=$encargado['cpEncargado'];
  		$res['telefono']=$encargado['telefonoEncargado'];
  		$res['fax']='';
  		$res['email']=$encargado['emailEncargado'];
  		$res['subcontratacion']=$encargado['subcontratacionEncargado'];
  		$res['donde']=$encargado['dondeEncargado'];
	} else if($codigo[0]=='ENCARGADOFIJO'){
		$trabajo=consultaBD('SELECT * FROM trabajos WHERE codigo='.$codigoTrabajo,true,true);
		$consultoria=recogerFormularioServicios($trabajo);
		$campos=array(
		158=>array(511,159,160,161,164,163,165,167,543,574,544,541,162,611),
		168=>array(512,169,170,171,174,173,175,177,545,575,546,527,172,612),
		178=>array(513,179,180,181,184,183,185,187,547,576,548,542,182,613),
		189=>array(514,190,191,191,195,194,196,198,549,577,550,528,193,614),
		200=>array(515,201,202,203,206,205,207,209,551,578,552,529,204,615),
		210=>array(516,211,212,213,216,215,217,219,553,579,554,530,214,616),
		220=>array(517,221,222,223,226,225,229,227,555,609,556,531,224,617),
		230=>array(518,231,232,233,236,235,239,236,557,580,558,532,234,618),
		240=>array(519,241,242,243,246,245,249,247,559,581,560,533,244,619),
		250=>array(520,251,252,253,256,255,259,257,561,582,562,534,254,620),
		260=>array(521,261,262,263,266,265,269,267,563,583,564,535,264,621),
		270=>array(522,271,272,273,276,275,279,277,565,584,566,536,274,622),
		280=>array(523,281,282,283,286,285,289,287,567,585,568,537,284,623),
		290=>array(524,291,292,293,296,295,299,297,569,586,570,538,294,624),
		487=>array(525,488,489,490,493,492,496,494,572,587,544,571,491,625));
		$encargado=$campos[$codigo[1]];
		$res['n_razon']=$consultoria['pregunta'.$encargado[1]];
  		$res['cif_nif']=$consultoria['pregunta'.$encargado[2]];
  		$res['dir_postal']=$consultoria['pregunta'.$encargado[3]];
  		$res['provincia']=$consultoria['pregunta'.$encargado[13]];
  		$res['localidad']=$consultoria['pregunta'.$encargado[5]];
  		$res['postal']=$consultoria['pregunta'.$encargado[4]];
  		$res['telefono']=$consultoria['pregunta'.$encargado[6]];
  		$res['fax']='';
  		$res['email']=$consultoria['pregunta'.$encargado[12]];
  		$res['subcontratacion']=$consultoria['pregunta'.$encargado[11]];
  		$res['donde']=$consultoria['pregunta'.$encargado[0]];
	}
	echo json_encode($res);
}

function creaTablaResponsables($datos){
	echo "<br />
	<div class='centro'>
		<div class='table-responsive'>
			<table class='table table-striped tabla-simple' id='tablaResponsables'>
			  	<thead>
			    	<tr>
			            <th> Responsables del fichero </th>
			            <th></th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;
			  		conexionBD();
			  		if($datos){
		  				$consulta=consultaBD("SELECT * FROM responsables_fichero WHERE codigoDeclaracion=".$datos['codigo']);
		  				while($item=mysql_fetch_assoc($consulta)){
		  					imprimeLineaTablaResponsables($item,$i);
		  					$i++;
		  				}
		  			}
			  		

			  		if($i==0){
			  			imprimeLineaTablaResponsables(false,'0');
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaResponsables\");'><i class='icon-plus'></i> Añadir responsable</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaResponsables\");'><i class='icon-trash'></i> Eliminar responsable</button>
			</div>
		</div>
	</div><br /><br />";
}

function imprimeLineaTablaResponsables($datos,$i){
	$j=$i+1;
	echo "<tr><td>";
	abreColumnaCampos('span3 spanFloat');
		campoTexto('cif_responsableFichero'.$i,'CIF/NIF',$datos['cif'],'input-small');
		campoTexto('razonSocial'.$i,'Razón social',$datos['razonSocial'],'input-large maximoCaracteres');
		campoTexto('representante_responsableFichero'.$i,'Representante legal',$datos['representante'],'input-large');
		campoTexto('nifRepresentante_responsableFichero'.$i,'NIF',$datos['nifRepresentante'],'input-large');
		campoSelectActividad('actividad'.$i,$datos['actividad']);
		campoTextoSimbolo('telefono_responsableFichero'.$i,'Teléfono','<i class="icon-phone"></i>',$datos['telefono'],'input-small pagination-right maximoCaracteres');
		campoTextoSimbolo('fax_responsableFichero'.$i,'Fax','<i class="icon-fax"></i>',$datos['fax'],'input-small pagination-right maximoCaracteres');
		campoTextoSimbolo('email_responsableFichero'.$i,'eMail','<i class="icon-envelope"></i>',$datos['email'],'input-large maximoCaracteres');
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto('direccion_responsableFichero'.$i,'Dirección postal',$datos['direccion'],'input-large maximoCaracteres');
		campoSelectProvinciaAEPD('provincia_responsableFichero'.$i,$datos['provincia']);
		campoSelectPaisesAEPD('pais_responsableFichero'.$i,$datos['pais']);
		campoTexto('localidad_responsableFichero'.$i,'Localidad',$datos['localidad'],'input-large maximoCaracteres');
		campoTexto('cp_responsableFichero'.$i,'Código Postal',$datos['cp'],'input-mini pagination-right maximoCaracteres');
		campoFecha('fechaInicio_responsableFichero'.$i,'Inicio del servicio',$datos['fechaInicio']);
		campoFecha('fechaFin_responsableFichero'.$i,'Fin del servicio',$datos['fechaFin']);
		campoSelectMultiple('donde_responsableFichero'.$i,'¿Donde recibe los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$datos['donde'],'selectpicker span3 show-tick','',0);
		/*echo '<div class="radioSubcontratacion">';
		campoRadio('subcontratacion_responsableFichero'.$i,'¿Permite la<br/>subcontratación?',$datos['subcontratacion'],'NO',array('No','Si'),array('NO','SI'));
		echo '</div>';*/
	cierraColumnaCampos(true);

	//creaTablaSubContratacion($datos,$i);

	echo "</td><td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td></tr>";
}

function creaTablaEncargados($datos,$codigo){
	$datos=consultaBD('SELECT trabajos.codigo, formulario FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE codigoCliente='.$datos,true,true);
	$z=0;
	if($datos){
		$formulario=recogerFormularioServicios($datos);
		echo "<br />
		<div class='centro'>
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaEncargados'>
			  		<thead>
			    		<tr>
			            	<th> Encargados del tratamiento </th>
			            	<th> </th>
			    		</tr>
			  		</thead>
			  		<tbody>";

			  		$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
			  		$campos=array(
						158=>array('','','Asesoría laboral para la realización de nóminas',159,160,161,162,163,164,165, 166,167,511,541,543,544,574,589,611,629),
						168=>array('','','Asesoría contable/fiscal',169,170,171,172,173,174,175,176,177,512,527,545,546,575,590,612,630),
						178=>array('','','Video vigilancia',179,180,181,182,183,184,185,186,187,513,542,547,548,576,591,613,631),
						189=>array('','','PRL',190,191,192,193,194,195,196,197,198,514,528,549,550,577,592,614,632),
						200=>array('','','Mantenimiento de equipos informáticos',201,202,203,204,205,206,207,208,209,515,529,551,552,578,593,615,633),
						210=>array('','','Mantenimiento de aplicaciones',211,212,213,214,215,216,219,218,217,516,530,553,554,579,594,616,634),
						220=>array('','','Mantenimiento de la pagina web',221,222,223,224,225,226,229,228,227,517,531,555,556,609,595,617,635),
						230=>array('','','Copias de seguridad "Backup"',231,232,233,234,235,236,239,238,236,518,532,557,558,580,596,618,636),
						240=>array('','','Transporte/mensajería para envíos a clientes',241,242,243,244,245,246,249,248,247,519,533,559,560,581,597,619,637),
						250=>array('','','Limpieza',251,252,253,254,255,256,259,258,257,520,534,561,562,582,598,620,638),
						260=>array('','','Mantenimiento de instalaciones',261,262,263,264,265,266,269,268,267,521,535,563,564,583,599,621,639),
						270=>array('','','Vigilancia/seguridad',271,272,273,274,275,276,279,278,277,522,536,565,566,584,600,622,640),
						280=>array('','','Mantenimiento de la copiadora',281,282,283,284,285,286,289,288,287,523,537,567,568,585,601,623,641),
						290=>array('','','Colaboración mercantil',291,292,293,294,295,296,299,298,297,524,538,569,570,586,602,624,642),
						487=>array('','','Selección de personal',488,489,490,491,492,493,496,495,494,525,571,572,573,587,603,625,643));
			  		foreach ($encargados as $key => $value) {
			  			if($formulario['pregunta'.$value]=='SI'){
			  				$item=array();
			  				$encargado=$campos[$value];
			  				$item[2]=$encargado[2];
			  				$i=0;
			  				foreach ($encargado as $key => $value2) {
			  					if($i==2){
			  						$item[2]=$encargado[2];
			  					} elseif($i>2){
			  						$item[$i]=$formulario['pregunta'.$value2];
			  					}
			  					$i++;
			  				}
			  				$item[2]='';
			  				$item[0]=$value;
			  				$z=imprimeLineaTablaEncargados($item,$codigo,$z,$value,$datos['codigo']);
			  			}
			  		}
			  		$encargados=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$datos['codigo'],true);
			  		while($encargado=mysql_fetch_assoc($encargados)){
			  			$i=0;
			  			$item=array();
			  			foreach ($encargado as $key => $value) {
			  				$item[$i]=$value;
			  				$i++;
			  			}
			  			$item[14]=formateaFechaWeb($item[14]);
			  			$item[15]=formateaFechaWeb($item[15]);
			  			$z=imprimeLineaTablaEncargados($item,$codigo,$z,1000,$datos['codigo'],'NOFIJO');
			  		}

		if($z==0){
			$item=creaItemVacio($codigo);
			$z=imprimeLineaTablaEncargados($item,$codigo,$z,0,$datos['codigo']);
		}
		echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFilaEncargados(\"tablaEncargados\");'><i class='icon-plus'></i> Añadir encargado</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFilaEncargados(\"tablaEncargados\");'><i class='icon-trash'></i> Eliminar encargado</button>
			</div>
		</div>
	</div><br /><br />";
	}
}

function creaItemVacio($codigo){
	$res=array();
	for($i=0;$i<=19;$i++){
		$res[$i]='';
	}
	$res[16]=$codigo;
	return $res;
}

function imprimeLineaTablaEncargados($item,$codigo,$i,$servicio,$codigoTrabajo,$tipo='FIJO'){
	$provinciasAgencia=array('01'=>'ÁLAVA','02'=>'ALBACETE','03'=>'ALICANTE','04'=>'ALMERÍA','05'=>'ÁVILA','06'=>'BADAJOZ','07'=>'ILLES BALEARS','08'=>'BARCELONA','09'=>'BURGOS','10'=>'CÁCERES','11'=>'CÁDIZ','12'=>'CASTELLÓN DE LA PLANA','13'=>'CIUDAD REAL','14'=>'CÓRDOBA','15'=>'A CORUÑA','16'=>'CUENCA','17'=>'GIRONA','18'=>'GRANADA','19'=>'GUADALAJARA','20'=>'GUIPÚZCOA','21'=>'HUELVA','22'=>'HUESCA','23'=>'JAÉN','24'=>'LEÓN','25'=>'LLEIDA','26'=>'LA RIOJA','27'=>'LUGO','28'=>'MADRID','29'=>'MÁLAGA','30'=>'MURCIA','31'=>'NAVARRA','32'=>'OURENSE','33'=>'ASTURIAS','34'=>'PALENCIA','35'=>'LAS PALMAS','36'=>'PONTEVEDRA','37'=>'SALAMANCA','38'=>'SANTA CRUZ DE TENERIFE','39'=>'CANTABRIA','40'=>'SEGOVIA','41'=>'SEVILLA','42'=>'SORIA','43'=>'TARRAGONA','44'=>'TERUEL','45'=>'TOLEDO','46'=>'VALENCIA','47'=>'VALLADOLID','48'=>'VIZCAYA','49'=>'ZAMORA','50'=>'ZARAGOZA','51'=>'CEUTA','52'=>'MELILLA');
	$nombres=array('','Asesoría laboral para la realización de nóminas','Asesoría contable/fiscal','Video vigilancia','PRL','Mantenimiento de equipos informáticos','Mantenimiento de aplicaciones','Mantenimiento de la pagina web','Copias de seguridad "Backup"','Transporte/mensajería para envíos a clientes','Limpieza','Mantenimiento de instalaciones','Vigilancia/seguridad','Mantenimiento de la copiadora','Colaboración mercantil','Selección de personal','Otro servicio');
	$valores=array(0,158,168,178,189,200,210,220,230,240,250,260,270,280,290,487,1000);
	$listadoPrestar=array('ENCARGADO'=>'Locales del encargdo','RESPONSABLE'=>'Locales del responsable','REMOTO'=>'Mediante acceso remoto');
	$prestar=explode('&$&', $item[12]);

	$accesos=array('X'=>'Acceso total','W'=>'Creación/Modificación/Borrado','R'=>'Solo lectura','S'=>'Solo a las dependencias','B'=>'Solo a los datos básicos','N'=>'Ninguno');
	$ficheros=explode('&$&', $item['16']);
	if(in_array($codigo, $ficheros)){
		$j=$i+1;
		echo "<tr><td>";
		abreColumnaCampos('span50 spanFloat');
			campoOculto($item[0],'codigoEncargadoExiste'.$i);
			$item['codigo']=$item[0];
			campoSelect('encargadoUno'.$i,'Servicio',$nombres,$valores,$servicio,'selectpicker span3 show-tick encargadoUno');
			$clase=$servicio!=1000?'hide':'';
			echo '<div id="divencargadoUno'.$i.'" class="divEncargadoUno '.$clase.'">';
				campoTexto('encargadoDiecisiete'.$i,'Otro servicio',$item[2]);
			echo '</div>';
			campoTexto('encargadoDos'.$i,'Nombre o Razón Social',$item[3]);
			campoTexto('encargadoTres'.$i,'NIF/CIF',$item[4]);
			campoTexto('encargadoCuatro'.$i,'Email',$item[6]);
			campoTexto('encargadoCinco'.$i,'Teléfono',$item[9]);
			campoTexto('encargadoSeis'.$i,'Persona de contacto / Tlf',$item[10]);
			campoTexto('encargadoSiete'.$i,'Representante legal',$item[11]);
			campoFechaBlanco('encargadoOcho'.$i,'Fecha del contrato de prestación de servicio',formateaFechaBD($item[14]));
		cierraColumnaCampos();
		abreColumnaCampos('span50');
			campoTexto('encargadoNueve'.$i,'Dirección Social',$item[5]);
			campoTexto('encargadoDiez'.$i,'Localidad',$item[7]);
			campoTexto('encargadoOnce'.$i,'Código Postal',$item[8]);
			campoSelectProvinciaAEPD('encargadoDoce'.$i,$item[18]);
			campoSelectMultiple('encargadoTrece'.$i,'¿Donde presta los servicios?',array('Locales del encargado','Locales del responsable','Mediante acceso remoto'),array('ENCARGADO','RESPONSABLE','REMOTO'),$item[12],'selectpicker span3 show-tick','',0);
			campoRadio('encargadoDieciocho'.$i,'¿Los datos personales serán tratados exclusivamente en los sistemas del encargado?',$item[19]);
			echo '<div class="radioSubcontratacion">';
				campoRadio('encargadoCatorce'.$i,'¿Está permitida la subcontratación?',$item[13]);
			echo '</div>';
			campoSelect('encargadoQuince'.$i,'Accesos',array('Acceso total','Creación/Modificación/Borrado','Sólo lectura','Solo a datos básicos','Sólo a las dependencias','Ninguno'),array('X','W','R','B','S','N'),$item[17],'selectpicker span3 show-tick','');
			campoFechaBlanco('encargadoDieciseis'.$i,'Fecha de vencimiento del contrato de prestación de servicio',formateaFechaBD($item[15]));
		cierraColumnaCampos(true);
		seccionTratamientos($codigoTrabajo,$i,$item[0],$tipo);
		creaTablaSubContratacion($item,'-'.$i);
		echo "</td><td class='centro'><input type='checkbox' name='filasTabla[]' value='$i'></td></tr>";
		$i++;
	}
	return $i;

}

function creaTablaSubContratacion($datos,$tipo){
	if($tipo=='fichero'){
		$div='div_subcontratacion_encargado';
		$tabla='tabla_subcontratacion_encargado';
		$campos=array('empresa','nif','direccion','cp','localidad','provincia','representante','servicio');
		$filasTabla='filasTabla[]';
	} else{
		$tipo=str_replace('-','',$tipo);
		$div='div_encargadoCatorce'.$tipo;
		$tabla='tabla_encargadoCatorce'.$tipo;
		$campos=array('empresa'.$tipo.'_','nif'.$tipo.'_','direccion'.$tipo.'_','cp'.$tipo.'_','localidad'.$tipo.'_','provincia'.$tipo.'_','representante'.$tipo.'_','servicio'.$tipo.'_');
		$filasTabla='filasTablaUno[]';
	}
	echo "<br clear='all'>
	<div class='centro divSubcontrata' id='".$div."'>
		<br/><h1 style='text-align:left;'>Subcontratación</h1>
		<div class='table-responsive'>
			<table class='table table-striped tabla-simple tablaConSpan3' id='".$tabla."'>
			  	<thead>
			    	<tr>
			            <th> Empresa </th>
			            <th></th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;
			  		conexionBD();
			  		if($datos){
		  				$consulta=consultaBD("SELECT * FROM declaraciones_subcontrata WHERE codigoResponsable=".$datos['codigo']);
		  				while($item=mysql_fetch_assoc($consulta)){
		  					$j=$i+1;
		  					echo '<tr><td>';
		  					abreColumnaCampos();
		  						campoTexto($campos[0].$i,'Empresa',$item['empresa']);
		  						campoTexto($campos[1].$i,'CIF/NIF',$item['nif'],'input-small');
		  						campoTexto($campos[6].$i,'Representante legal',$item['representante']);
		  						campoTexto($campos[7].$i,'Servicio',$item['servicio']);
		  					cierraColumnaCampos();
		  					abreColumnaCampos();
		  						campoTexto($campos[2].$i,'Dirección',$item['direccion']);
		  						campoTexto($campos[3].$i,'CP',$item['cp'],'input-mini');
		  						campoTexto($campos[4].$i,'Localidad',$item['localidad']);
		  						campoSelectProvinciaAEPD($campos[5].$i,$item['provincia']);
		  					cierraColumnaCampos(true);
		  					echo "</td><td class='centro'><input type='checkbox' name='".$filasTabla."' value='$j'></td></tr>";
		  					$i++;
		  				}
		  			}
			  		

			  		if($i==0){
			  			$j=$i+1;
			  			echo '<tr><td>';
		  					abreColumnaCampos();
		  						campoTexto($campos[0].$i,'Empresa');
		  						campoTexto($campos[1].$i,'CIF/NIF',false,'input-small');
		  						campoTexto($campos[6].$i,'Representante legal');
		  						campoTexto($campos[7].$i,'Servicio');
		  					cierraColumnaCampos();
		  					abreColumnaCampos();
		  						campoTexto($campos[2].$i,'Dirección');
		  						campoTexto($campos[3].$i,'CP',false,'input-mini');
		  						campoTexto($campos[4].$i,'Localidad');
		  						campoSelectProvinciaAEPD($campos[5].$i,false);
		  					cierraColumnaCampos(true);
			  			echo "</td><td class='centro'><input type='checkbox' name='".$filasTabla."' value='$j'></td></tr>";
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' tabla='".$tabla."' onclick='insertaFilaSubcontrata($(this));'><i class='icon-plus'></i> Añadir empresa</button> 
				<button type='button' class='btn btn-small btn-danger' tabla='".$tabla."' onclick='eliminaFilaSubcontrata($(this));'><i class='icon-trash'></i> Eliminar empresa</button>
			</div>
		</div>
	</div><br /><br />";
}

function seccionTratamientos($codigoTrabajo,$n,$f,$tipo='FIJO'){
	
	$listado=array('checkRecogida'=>'Recogida','checkModificacion'=>'Modificación','checkConsulta'=>'Consulta','checkComunicacion'=>'Comunicación','checkComunicacion1'=>'Comunicación por transmisión al responsable del fichero','checkRegistro'=>'Registro','checkConservacion'=>'Conservación','checkCotejo'=>'Cotejo','checkSupresion'=>'Supresión','checkComunicacion2'=>'Comunicación a la Administración Pública competente','checkEstructuracion'=>'Estructuración','checkInterconexion'=>'Interconexión','checkAnalisis'=>'Análisis de conducta','checkDestruccion'=>'Destrucción','checkComunicacion3'=>'Comunicación permitida por ley');
	$i=0;
	$f=$f==''?0:$f;
	if($tipo=='FIJO'){
		$fijo='SI';
		$tratamientos=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$f.' AND codigoTrabajo='.$codigoTrabajo.' AND fijo="'.$fijo.'";',true,true);
	} else {
		$fijo='NO';
		$tratamientos=consultaBD('SELECT * FROM encargados_tratamientos WHERE codigoEncargado='.$f.' AND codigoTrabajo='.$codigoTrabajo.' AND fijo="'.$fijo.'";',true,true);
	}
	echo '<div class="divTratamientos"><h1>Tratamientos</h1><br/>';
		foreach ($listado as $key => $value) {
			if($i%5==0){
				abreColumnaCampos();
			}
			if($tratamientos){
				$valor=$tratamientos[$key];
			} else {
				if(in_array($key,array('checkRecogida','checkModificacion','checkConsulta','checkComunicacion1','checkRegistro','checkConservacion','checkCotejo','checkSupresion','checkComunicacion2','checkComunicacion3','checkDestruccion'))){
					$valor='SI';
				} else {
					$valor='NO';
				}
			}
			campoCheckIndividual($key.'_'.$n,$value,$valor);
			if(($i+1)%5==0){
				cierraColumnaCampos();
			}
			$i++;
		}
	echo '</div>';
}


			
//Fin parte de gestión LOPD
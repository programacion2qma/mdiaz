<?php
  $seccionActiva=12;
  include_once("../cabecera.php");
  if(isset($_GET['predefinida'])){
  	gestionPredefinida();
  } else {
  	gestionLOPD();
  }
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/filasTablaEncargados.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../js/validadorAEPD.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.selectpicker').selectpicker();
		/*$(".submit").click(function() {
  			$('#accion').attr('value',$(this).attr('value'));
  			if($(this).attr('value')=='Finalizar'){
  				alert('El fichero no se podrá declarar hasta la recepción del certificado');
  			}
  			$('#edit-profile').submit();
		});*/ 

		//Función para los detalles de un registro (en la creación nunca se cambiará a priori un color)
		$(':checkbox:not(.divTratamientos input[type=checkbox])').each(function(){
			cambiaColorCheck($(this));
		});

		//Oyentes checks
		$('#cajaFinalidades input:checkbox').change(function(){
			if($('#cajaFinalidades :checkbox:checked').length<7){
				cambiaColorCheck($(this));
			}
			else{
				$(this).attr('checked',false);
				alert('Solo puede seleccionar un máximo de 6 elementos.');
			}
		});

		$('#cajaColectivos input:checkbox').change(function(){
			if($('#cajaColectivos :checkbox:checked').length<7){
				cambiaColorCheck($(this));
			}
			else{
				$(this).attr('checked',false);
				alert('Solo puede seleccionar un máximo de 6 elementos.');
			}
		});

		//$('.nav-tabs li a[href=#'+pestanaActiva+']').trigger('click');
		marcarPestania();
		$('.nav-tabs li a').click(function(){
			var pestana=$(this).attr('href').replace('#','');
			$('#pestanaActiva').val(pestana);
		});

		oyenteOtroColectivo();
		$('input[type=checkbox][value=OTRO]').change(function(){
			oyenteOtroColectivo();
		});

		oyenteOtroIdentificativo();
		$('input[type=checkbox][value=otroIden]').change(function(){
			oyenteOtroIdentificativo();
		});

		oyenteOtroTipificado();
		$('input[type=radio][name=otrosTiposDatos]').change(function(){
			oyenteOtroTipificado();
		});

		oyenteOtrosDestinatarios();
		$('input[type=checkbox][value=21]').change(function(){
			oyenteOtrosDestinatarios();
		});
		
		$(".cif").focusout(function(){
    		isValidCif($(this).val());
		});
		
		$(".nif").focusout(function(){
    		isValidNif($(this).val());
		}); 
	
		$(".cifnif").focusout(function(){
			if(isValidCif($(this).val())){
				alert("Es un CIF valido");
			} else {
				alert("No es un CIF valido, comprobando NIF");
				if(isValidNif($(this).val())){
					alert("Es un NIF valido");
				}
			}
		});
		
		if ( $("#codigo").length == 0 ) {
			recogeDatos($('#codigoCliente').val());
			recogeDatosEncargado($('#codigoCliente').val());
		}

		$("#cif_nif").change(function(){
			insertaCargo();
		});

		$("#codigoCliente").change(function(){
			recogeDatos($(this).val());
		});

		$("#distinto input[type=radio]").change(function(){
			distintoDerechos($(this));
		});
		
		$("#distintoEncargado input[type=radio]").change(function(){
			distintoEncargado($(this),$('#codigoCliente').val());			
		});
		$("#distintoDeclarante input[type=radio]").change(function(){
			distintoDeclarante($(this),$('#codigoCliente').val());			
		});

		$('#n_razon').change(function(){
			compruebaSelectEncargado($(this).val());
		});

		$('#recogerDatosEncargado').change(function(){
			recogeDatosEncargadoSelect($(this).val());
		});

		$('#tablaEncargados').on('change','.encargadoUno',function(){
			oyenteEncargadoUno($(this));
			recogeDatosEncargadoSelect($(this));
		})

		$(".radioSubcontratacion input[type=radio]").each(function( index ) {
			oyenteSubContratacion($(this));
			$(this).change(function(){
				oyenteSubContratacion($(this));
			});
		});

	});

	function cambiaColorCheck(campo){
		if(campo.is(':checked')){
			campo.parent().addClass('marcado');
		}
		else{
			campo.parent().removeClass('marcado');
		}
	}
	
	function recogeDatos(codigo){
		provinciasAgencia=new Array();

		provinciasAgencia={'ÁLAVA':'01','ALBACETE':'02','ALICANTE':'03','ALMERÍA':'04','ÁVILA':'05','BADAJOZ':'06','ILLES BALEARS':'07','BARCELONA':'08','BURGOS':'09','CÁCERES':'10','CÁDIZ':'11','CASTELLÓN DE LA PLANA':'12','CIUDAD REAL':'13','CÓRDOBA':'14','A CORUÑA':'15','CUENCA':'16','GIRONA':'17','GRANADA':'18','GUADALAJARA':'19','GUIPÚZCOA':'20','HUELVA':'21','HUESCA':'22','JAÉN':'23','LEÓN':'24','LLEIDA':'25','LA RIOJA':'26','LUGO':'27','MADRID':'28','MÁLAGA':'29','MURCIA':'30','NAVARRA':'31','OURENSE':'32','ASTURIAS':'33','PALENCIA':'34','LAS PALMAS':'35','PONTEVEDRA':'36','SALAMANCA':'37','SANTA CRUZ DE TENERIFE':'38','CANTABRIA':'39','SEGOVIA':'40','SEVILLA':'41','SORIA':'42','TARRAGONA':'43','TERUEL':'44','TOLEDO':'45','VALENCIA':'46','VALLADOLID':'47','VIZCAYA':'48','ZAMORA':'49','ZARAGOZA':'50','CEUTA':'51','MELILLA':'52'};
		$.ajax({
    		data: {"codigo":codigo},//Matriz $_POST: se recogera de la forma $_POST['codigo'] (y tendrá el valor 1)
    		type: "POST",//Siempre asi
    		dataType: "json",//Siempre asi
    		url: "recogeDatos.php",//URL destino (que solo debe hacer echo del resultado)
  		}).done(function(datos,textStatus,jqXHR){
  			var provincia = provinciasAgencia[datos.provincia];
  			$('#cif_nif').val(datos.cif);
  			$('#dir_postal').val(datos.domicilio);
  			$('#provincia').val(provinciasAgencia[datos.provincia]);
  			$('#localidad').val(datos.localidad);
  			$('#postal').val(datos.cp);
  			$('#telefono').val(datos.telefono);
  			$('#fax').val(datos.fax);
  			$('#email').val(datos.email);
  			$('#pais').val(datos.pais);
    		$("#razon_s").val(datos.razonSocial);//datos es un objeto JSON que tiene como atributos los índices del array PHP (ver código PHP)
  			$("#nombre").val(datos.administrador);
  			$("#apellido1").val(datos.apellido1);
  			$("#apellido2").val(datos.apellido2);
  			$("#nif").val(datos.nifAdministrador);
  			$("#cargo").val('');
  			
  			if($('input[name=datosResponsable]:checked').val()=='NO'){
  				$('#cif_nif_responsableFichero').val(datos.cif);
  				$('#dir_postal_responsableFichero').val(datos.domicilio);
  				$('#provincia_responsableFichero').val(provinciasAgencia[datos.provincia]);
  				$('#localidad_responsableFichero').val(datos.localidad);
  				$('#postal_responsableFichero').val(datos.cp);
  				$('#telefono_responsableFichero').val(datos.telefono);
  				$('#fax_responsableFichero').val(datos.fax);
  				$('#email_responsableFichero').val(datos.email);
  				$("#n_razon").val(datos.razonSocial);
  				$("#representante_responsableFichero").val(datos.administrador+' '+datos.apellido1+' '+datos.apellido2);
  				$("#nifRepresentante_responsableFichero").val(datos.nifAdministrador);
  				$('#pais_responsableFichero').val(datos.pais);
  				compruebaSelectEncargado(datos.razonSocial);
  			} else {
  				$('#encargadoUno0').val(1000);
  				$('#encargadoDiecisiete0').val($('#nombreFichero').val());
				$('#encargadoDos0').val($('#razon_s').val());
				$('#encargadoTres0').val($('#cif_nif').val());
				$('#encargadoNueve0').val($('#dir_postal').val());
				$('#encargadoCuatro0').val($('#email').val());
				$('#encargadoDiez0').val($('#localidad').val());
				$('#encargadoOnce0').val($('#postal').val());
				$('#encargadoCinco0').val($('#telefono').val());
				$('#encargadoSeis0').val('');
				$('#encargadoSiete0').val($('#nombre').val()+' '+$('#apellido1').val()+' '+$('#apellido2').val());
				$('#encargadoTrece0 option[value=ENCARGADO]').prop('selected',true);
				$('#encargadoTrece0 option[value=REMOTO]').prop('selected',true);
				$('#encargadoCatorce0').val('NO');
				$('#encargadoOcho0').val('');
				$('#encargadoDieciseis0').val('');
				$('#encargadoQuince0').val('X');
				$('#encargadoDoce0').val($('#provincia').val());
				$('.selectpicker').selectpicker('refresh');
  			}
  			//$("#cap").val(datos.actividad);

  			if($('input[name=datosDerecho]:checked').val()=='NO'){
  				$('#nif_cif').val($('#cif_nif_responsableFichero').val());
  				$('#dir_postal_derecho').val($('#dir_postal_responsableFichero').val());
  				$('#provincia_derecho').val($('#provincia_responsableFichero').val());
  				$('#localidad_derecho').val($('#localidad_responsableFichero').val());
  				$('#postal_derecho').val($('#postal_responsableFichero').val());
  				$('#telefono_derecho').val($('#telefono_responsableFichero').val());
  				$('#fax_derecho').val($('#fax_responsableFichero').val());
  				$('#email_derecho').val($('#email_responsableFichero').val());
  				$('#pais_derecho').val(datos.pais);
  				$('.selectpicker').selectpicker('refresh');
  			} else if($('input[name=datosDerecho]:checked').val()=='SI'){
  				$('#nif_cif').val(datos.cif);
  				$('#dir_postal_derecho').val(datos.domicilio);
  				$('#provincia_derecho').val(provinciasAgencia[datos.provincia]);
  				$('#localidad_derecho').val(datos.localidad);
  				$('#postal_derecho').val(datos.cp);
  				$('#telefono_derecho').val(datos.telefono);
  				$('#fax_derecho').val(datos.fax);
  				$('#email_derecho').val(datos.email);
  				$('#pais_derecho').val(datos.pais);
  				$('.selectpicker').selectpicker('refresh');
  			}

  			if(datos.clientes_y_proveedores == 'SI'){
  				$("input[name=checkFinalidad0]").attr('checked','checked');
  			}
  			if(datos.nominas_personal == 'SI'){
  				$("input[name=checkFinalidad2]").attr('checked','checked');
  			}
  			if(datos.videovigilancia == 'SI'){
  				$("input[name=checkFinalidad22]").attr('checked','checked');
  			}
  			if(datos.historial_clinico == 'SI'){
  				$("input[name=checkFinalidad19]").attr('checked','checked');
  			}
  			if(datos.comunidades == 'SI'){
  				$("input[name=checkFinalidad13]").attr('checked','checked');
  			}
  			if(datos.correduria_seguros == 'SI'){
  				$("input[name=checkFinalidad6]").attr('checked','checked');
  			}
  			insertaCargo();
  			$('.selectpicker').selectpicker('refresh');
  		});
  	}

  	function insertaCargo(){
  		if($("#cif_nif").length > 0){
  			var valor=$('#cif_nif').val().substr(0,1);
  			if (isNaN(valor)){
  				$('#cargo').val('Representante legal');
  			} else {
  				$('#cargo').val('Profesional autónomo');
  			}
  		}
  	}

  	function recogeDatosEncargado(codigoCliente){
  		provinciasAgencia={'ÁLAVA':'01','ALBACETE':'02','ALICANTE':'03','ALMERÍA':'04','ÁVILA':'05','BADAJOZ':'06','ILLES BALEARS':'07','BARCELONA':'08','BURGOS':'09','CÁCERES':'10','CÁDIZ':'11','CASTELLÓN DE LA PLANA':'12','CIUDAD REAL':'13','CÓRDOBA':'14','A CORUÑA':'15','CUENCA':'16','GIRONA':'17','GRANADA':'18','GUADALAJARA':'19','GUIPÚZCOA':'20','HUELVA':'21','HUESCA':'22','JAÉN':'23','LEÓN':'24','LLEIDA':'25','LA RIOJA':'26','LUGO':'27','MADRID':'28','MÁLAGA':'29','MURCIA':'30','NAVARRA':'31','OURENSE':'32','ASTURIAS':'33','PALENCIA':'34','LAS PALMAS':'35','PONTEVEDRA':'36','SALAMANCA':'37','SANTA CRUZ DE TENERIFE':'38','CANTABRIA':'39','SEGOVIA':'40','SEVILLA':'41','SORIA':'42','TARRAGONA':'43','TERUEL':'44','TOLEDO':'45','VALENCIA':'46','VALLADOLID':'47','VIZCAYA':'48','ZAMORA':'49','ZARAGOZA':'50','CEUTA':'51','MELILLA':'52'};
		var consulta=$.post('../listadoAjax.php?include=gestion-lopd&funcion=recogeDatosEncargado();',{'codigoCliente':codigoCliente},
		function(respuesta){
			if(respuesta=='fallo'){
				alert('Ha habido un problema al obtener los datos del encargado del fichero.\nInténtelo de nuevo en unos segundos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
			}
			else{
				$('#n_razon_encargado').val(respuesta.pregunta3);
  				$('#cif_nif_encargado').val(respuesta.pregunta9);
  				$('#dir_postal_encargado').val(respuesta.pregunta4);
  				$('#provincia_encargado').val(provinciasAgencia[respuesta.pregunta11]);
  				$('#localidad_encargado').val(respuesta.pregunta5);
  				$('#postal_encargado').val(respuesta.pregunta10);
  				$('#telefono_encargado').val(respuesta.pregunta6);
  				$('#fax_encargado').val(respuesta.pregunta12);
  				$('#email_encargado').val(respuesta.pregunta7);
  				$('.selectpicker').selectpicker('refresh');
			}
		},'json');
	}
	
	function oyenteOtroColectivo(){
		var check=$('input[type=checkbox][value=OTRO]');
		if(check.is(':checked')){
			$('#cajaOtroColectivo').removeClass('hide');
		}
		else{
			$('#cajaOtroColectivo').addClass('hide');
		}
	}
	
	function oyenteOtroIdentificativo(){
		var check=$('input[type=checkbox][value=otroIden]');
		if(check.is(':checked')){
			$('#cajaOtroIdentificativo').removeClass('hide');
		}
		else{
			$('#cajaOtroIdentificativo').addClass('hide');
		}
	}

	function oyenteOtroTipificado(){
		var check=$('input[type=radio][name=otrosTiposDatos]:checked').val();
		if(check=='SI'){
			$('#cajaOtrosDatosTipificados').removeClass('hide');
		}
		else{
			$('#cajaOtrosDatosTipificados').addClass('hide');
		}
	}

	function oyenteOtrosDestinatarios(){
		var check=$('input[type=checkbox][value=21]');
		if(check.is(':checked')){
			$('#cajaOtrosDestintarios').removeClass('hide');
		}
		else{
			$('#cajaOtrosDestintarios').addClass('hide');
		}
	}

	function distintoDerechos(elem){
		if(elem.val() == '0'){
			$('#nif_cif').val('');
  			$('#dir_postal_derecho').val('');
  			$('#provincia_derecho').val('01');
  			$('#localidad_derecho').val('');
  			$('#postal_derecho').val('');
  			$('#telefono_derecho').val('');
  			$('#fax_derecho').val('');
  			$('#email_derecho').val('');
  			$('.selectpicker').selectpicker('refresh');
		} else if(elem.val() == 'NO') {
			$('#nif_cif').val($('#cif_nif_responsableFichero').val());
  			$('#dir_postal_derecho').val($('#dir_postal_responsableFichero').val());
  			$('#provincia_derecho').val($('#provincia_responsableFichero').val());
  			$('#localidad_derecho').val($('#localidad_responsableFichero').val());
  			$('#postal_derecho').val($('#postal_responsableFichero').val());
  			$('#telefono_derecho').val($('#telefono_responsableFichero').val());
  			$('#fax_derecho').val($('#fax_responsableFichero').val());
  			$('#email_derecho').val($('#email_responsableFichero').val());
  			$('.selectpicker').selectpicker('refresh');
		} else if(elem.val() == 'SI') {
			$('#nif_cif').val($('#cif_nif').val());
  			$('#dir_postal_derecho').val($('#dir_postal').val());
  			$('#provincia_derecho').val($('#provincia').val());
  			$('#localidad_derecho').val($('#localidad').val());
  			$('#postal_derecho').val($('#postal').val());
  			$('#telefono_derecho').val($('#telefono').val());
  			$('#fax_derecho').val($('#fax').val());
  			$('#email_derecho').val($('#email').val());
  			$('.selectpicker').selectpicker('refresh');
		}			
	}

	function distintoEncargado(elem,codigoCliente){
		if(elem.val() == 'SI'){
			$('#selectEncargado').removeClass('hide');
			recogeDatosEncargado(codigoCliente);
		} else {
			$('#selectEncargado').addClass('hide');
			$('#n_razon_encargado').val($('#razon_s').val());
			$('#cif_nif_encargado').val($('#cif_nif').val());
  			$('#dir_postal_encargado').val($('#dir_postal').val());
  			$('#provincia_encargado').val($('#provincia').val());
  			$('#localidad_encargado').val($('#localidad').val());
  			$('#postal_encargado').val($('#postal').val());
  			$('#telefono_encargado').val($('#telefono').val());
  			$('#fax_encargado').val($('#fax').val());
  			$('#email_encargado').val($('#email').val());
  			$('.selectpicker').selectpicker('refresh');
		}					
	}

	function distintoDeclarante(elem){
		if(elem.val() == 'NO') {
			$('#cif_nif_responsableFichero').val($('#cif_nif').val());
  			$('#dir_postal_responsableFichero').val($('#dir_postal').val());
  			$('#provincia_responsableFichero').val($('#provincia').val());
  			$('#localidad_responsableFichero').val($('#localidad').val());
  			$('#postal_responsableFichero').val($('#postal').val());
  			$('#telefono_responsableFichero').val($('#telefono').val());
  			$('#fax_responsableFichero').val($('#fax').val());
  			$('#email_responsableFichero').val($('#email').val());
  			$('#representante_responsableFichero').val($('#nombre').val()+' '+$('#apellido1').val()+' '+$('#apellido2').val());
  			$('#nifRepresentante_responsableFichero').val($('#nif').val());
  			$('#n_razon').val($('#razon_s').val());
  			
  			$('#encargadoUno0').val(0);
  			$('#encargadoDiecisiete0').val('');
			$('#encargadoDos0').val('');
			$('#encargadoTres0').val('');
			$('#encargadoNueve0').val('');
			$('#encargadoCuatro0').val('');
			$('#encargadoDiez0').val('');
			$('#encargadoOnce0').val('');
			$('#encargadoCinco0').val('');
			$('#encargadoSeis0').val('');
			$('#encargadoSiete0').val('');
			$('#encargadoTrece0 option[value=ENCARGADO]').prop('selected',false);
			$('#encargadoTrece0 option[value=REMOTO]').prop('selected',false);
			$('#encargadoTrece0 option[value=RESPONSABLE]').prop('selected',false);
			$('#encargadoCatorce0').val('NO');
			$('#encargadoOcho0').val('');
			$('#encargadoDieciseis0').val('');
			$('#encargadoQuince0').val('X');
			$('#encargadoDoce0').val('');

			$('.selectpicker').selectpicker('refresh');

			oyenteEncargadoUno($('#encargadoUno0'));
		} else if(elem.val() == 'SI') {
			$('#cif_nif_responsableFichero').val('');
  			$('#dir_postal_responsableFichero').val('');
  			$('#provincia_responsableFichero').val('');
  			$('#localidad_responsableFichero').val('');
  			$('#postal_responsableFichero').val('');
  			$('#telefono_responsableFichero').val('');
  			$('#fax_responsableFichero').val('');
  			$('#email_responsableFichero').val('');
  			$('#representante_responsableFichero').val('');
  			$('#nifRepresentante_responsableFichero').val('');
  			$('#n_razon').val('');
  			
  			$('#encargadoUno0').val(1000);
  			$('#encargadoDiecisiete0').val($('#nombreFichero').val());
			$('#encargadoDos0').val($('#razon_s').val());
			$('#encargadoTres0').val($('#cif_nif').val());
			$('#encargadoNueve0').val($('#dir_postal').val());
			$('#encargadoCuatro0').val($('#email').val());
			$('#encargadoDiez0').val($('#localidad').val());
			$('#encargadoOnce0').val($('#postal').val());
			$('#encargadoCinco0').val($('#telefono').val());
			$('#encargadoSeis0').val('');
			$('#encargadoSiete0').val($('#nombre').val()+' '+$('#apellido1').val()+' '+$('#apellido2').val());
			$('#encargadoTrece0 option[value=ENCARGADO]').prop('selected',true);
			$('#encargadoTrece0 option[value=REMOTO]').prop('selected',true);
			$('#encargadoCatorce0').val('NO');
			$('#encargadoOcho0').val('');
			$('#encargadoDieciseis0').val('');
			$('#encargadoQuince0').val('X');
			$('#encargadoDoce0').val($('#provincia').val());

			$('.selectpicker').selectpicker('refresh');

			oyenteEncargadoUno($('#encargadoUno0'));
		}			
	}

	function marcarPestania(){
		if($('#pestanaActiva').val()!=undefined){
  			var pagina=$('#pestanaActiva').val();
  			$('.nav-tabs li').removeClass('active');
  			$('.nav-tabs li:nth-child('+pagina+')').addClass('active');
  		}
	}

	function compruebaSelectEncargado(valor){
		if($('#recogerDatosEncargado option[value=RESP]').length==0){
			$('#recogerDatosEncargado').append('<option value="RESP">'+valor+'</option>');
		} else {
			$('#recogerDatosEncargado option[value=RESP]').html(valor);
		}
		$('#recogerDatosEncargado').selectpicker('refresh');
	}

	/*function recogeDatosEncargadoSelect(valor){
		if(valor=='RESP'){
			$('#n_razon_encargado').val($('#n_razon').val());
  			$('#cif_nif_encargado').val($('#cif_nif_responsableFichero').val());
  			$('#dir_postal_encargado').val($('#dir_postal_responsableFichero').val());
  			$('#provincia_encargado').val($('#provincia_responsableFichero').val());
  			$('#localidad_encargado').val($('#localidad_responsableFichero').val());
  			$('#postal_encargado').val($('#postal_responsableFichero').val());
  			$('#telefono_encargado').val($('#telefono_responsableFichero').val());
  			$('#fax_encargado').val($('#fax_responsableFichero').val());
  			$('#email_encargado').val($('#email_responsableFichero').val());
  			$('.selectpicker').selectpicker('refresh');
		} else {
  			provinciasAgencia={'ÁLAVA':'01','ALBACETE':'02','ALICANTE':'03','ALMERÍA':'04','ÁVILA':'05','BADAJOZ':'06','ILLES BALEARS':'07','BARCELONA':'08','BURGOS':'09','CÁCERES':'10','CÁDIZ':'11','CASTELLÓN DE LA PLANA':'12','CIUDAD REAL':'13','CÓRDOBA':'14','A CORUÑA':'15','CUENCA':'16','GIRONA':'17','GRANADA':'18','GUADALAJARA':'19','GUIPÚZCOA':'20','HUELVA':'21','HUESCA':'22','JAÉN':'23','LEÓN':'24','LLEIDA':'25','LA RIOJA':'26','LUGO':'27','MADRID':'28','MÁLAGA':'29','MURCIA':'30','NAVARRA':'31','OURENSE':'32','ASTURIAS':'33','PALENCIA':'34','LAS PALMAS':'35','PONTEVEDRA':'36','SALAMANCA':'37','SANTA CRUZ DE TENERIFE':'38','CANTABRIA':'39','SEGOVIA':'40','SEVILLA':'41','SORIA':'42','TARRAGONA':'43','TERUEL':'44','TOLEDO':'45','VALENCIA':'46','VALLADOLID':'47','VIZCAYA':'48','ZAMORA':'49','ZARAGOZA':'50','CEUTA':'51','MELILLA':'52'};
  			var codigoTrabajo=$('#codigoTrabajo').val();
			var consulta=$.post('../listadoAjax.php?include=gestion-lopd&funcion=recogeDatosEncargadoSelect();',{'codigoTrabajo':codigoTrabajo,'codigo':valor},
			function(respuesta){
				if(respuesta=='fallo'){
					alert('Ha habido un problema al obtener los datos del encargado del fichero.\nInténtelo de nuevo en unos segundos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
				}
				else{
					$('#n_razon_encargado').val(respuesta.n_razon);
  					$('#cif_nif_encargado').val(respuesta.cif_nif);
  					$('#dir_postal_encargado').val(respuesta.dir_postal);
  					$('#provincia_encargado').val(respuesta.provincia);
  					$('#localidad_encargado').val(respuesta.localidad);
  					$('#postal_encargado').val(respuesta.postal);
  					$('#telefono_encargado').val(respuesta.telefono);
  					$('#fax_encargado').val('');
  					$('#email_encargado').val(respuesta.email);
  					$('input[name=subcontratacion_encargado][value='+respuesta.subcontratacion+']').attr('checked','checked');
  					var donde=respuesta.donde.split('&$&');
  					$('#donde_encargado option').each(function( index ) {
  						if(donde.indexOf($(this).val())>-1){
  							$('#donde_encargado option[value='+$(this).val()+']').attr('selected','selected');
  						} else {
  							$('#donde_encargado option[value='+$(this).val()+']').removeAttr("selected");
  						}
  					});
  					$('.selectpicker').selectpicker('refresh');
				}
			},'json');
		}
	}*/

	function oyenteSubContratacion(elem){
		var valor=elem.val();
		var div='#div_'+elem.attr('name');
		if(valor=='SI' && elem.attr('checked')=='checked'){
			$(div).removeClass('hide');
		}
		else if(valor=='NO' && elem.attr('checked')=='checked'){
			$(div).addClass('hide');
		}
	}

	function oyenteEncargadoUno(elem){
		var id='#div'+elem.attr('id');
		if(elem.val()==1000){
			$(id).removeClass('hide');
		} else {
			$(id).addClass('hide');
		}
	}

	function recogeDatosEncargadoSelect(elem){
		var i=obtieneFilaCampo(elem);
		//alert(i);
		provinciasAgencia={'ÁLAVA':'01','ALBACETE':'02','ALICANTE':'03','ALMERÍA':'04','ÁVILA':'05','BADAJOZ':'06','ILLES BALEARS':'07','BARCELONA':'08','BURGOS':'09','CÁCERES':'10','CÁDIZ':'11','CASTELLÓN DE LA PLANA':'12','CIUDAD REAL':'13','CÓRDOBA':'14','A CORUÑA':'15','CUENCA':'16','GIRONA':'17','GRANADA':'18','GUADALAJARA':'19','GUIPÚZCOA':'20','HUELVA':'21','HUESCA':'22','JAÉN':'23','LEÓN':'24','LLEIDA':'25','LA RIOJA':'26','LUGO':'27','MADRID':'28','MÁLAGA':'29','MURCIA':'30','NAVARRA':'31','OURENSE':'32','ASTURIAS':'33','PALENCIA':'34','LAS PALMAS':'35','PONTEVEDRA':'36','SALAMANCA':'37','SANTA CRUZ DE TENERIFE':'38','CANTABRIA':'39','SEGOVIA':'40','SEVILLA':'41','SORIA':'42','TARRAGONA':'43','TERUEL':'44','TOLEDO':'45','VALENCIA':'46','VALLADOLID':'47','VIZCAYA':'48','ZAMORA':'49','ZARAGOZA':'50','CEUTA':'51','MELILLA':'52'};
  		var codigoTrabajo=$('#codigoTrabajo').val();
		/*var consulta=$.post('../listadoAjax.php?include=gestion-lopd&funcion=recogeDatosEncargadoSelect();',{'codigoTrabajo':codigoTrabajo,'codigo':valor},
		function(respuesta){
			if(respuesta=='fallo'){
				alert('Ha habido un problema al obtener los datos del encargado del fichero.\nInténtelo de nuevo en unos segundos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
			}
			else{
				$('#n_razon_encargado').val(respuesta.n_razon);
  				$('#cif_nif_encargado').val(respuesta.cif_nif);
  				$('#dir_postal_encargado').val(respuesta.dir_postal);
  				$('#provincia_encargado').val(respuesta.provincia);
  				$('#localidad_encargado').val(respuesta.localidad);
  				$('#postal_encargado').val(respuesta.postal);
  				$('#telefono_encargado').val(respuesta.telefono);
  				$('#fax_encargado').val('');
  				$('#email_encargado').val(respuesta.email);
  				$('input[name=subcontratacion_encargado][value='+respuesta.subcontratacion+']').attr('checked','checked');
  				var donde=respuesta.donde.split('&$&');
  				$('#donde_encargado option').each(function( index ) {
  					if(donde.indexOf($(this).val())>-1){
  						$('#donde_encargado option[value='+$(this).val()+']').attr('selected','selected');
  					} else {
  						$('#donde_encargado option[value='+$(this).val()+']').removeAttr("selected");
  					}
  				});
  				$('.selectpicker').selectpicker('refresh');
			}
		},'json');*/
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
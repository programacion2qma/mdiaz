<?php
  $seccionActiva=12;
  include_once('../cabecera.php');
  if(isset($_GET['codigoCliente'])){
    $cliente = datosRegistro('clientes',$_GET['codigoCliente']);
  } else if(isset($_POST['codigoCliente'])){
    $cliente = datosRegistro('clientes',$_POST['codigoCliente']);
  } else if(isset($_SESSION['coddigoCliente'])){
    $cliente = datosRegistro('clientes',$_SESSION['codigoCliente']);
  }
  $_SESSION['codigoCliente']=$cliente;
  $res=operacionesLOPD();
  $estadisticas=estadisticasDeclaracionesRestrict($cliente['codigo']);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema para el área de gestión LOPD de <?php echo $cliente['razonSocial']; ?>:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-flag"></i> <span class="value"><?php echo $estadisticas['total']?></span><br />Declaraciones registradas</div>
                     <!--div class="stat"> <i class="icon-send"></i> <span class="value"><?php echo $estadisticas['total']?></span> <br>Enviados a la agencia</div-->
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Declaraciones</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <a href="<?php echo $_CONFIG['raiz']; ?>consultoria/index.php" class="shortcut"><i class="shortcut-icon icon-arrow-left"></i><span class="shortcut-label">Volver</span> </a>
				        <a href="<?php echo $_CONFIG['raiz']; ?>gestion-lopd/gestion.php?codigoCliente=<?php echo $cliente['codigo']; ?>" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva declaración</span> </a>
                <a href="<?php echo $_CONFIG['raiz']; ?>gestion-lopd/gestion.php?predefinida&codigoCliente=<?php echo $cliente['codigo']; ?>" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva declaración predefinida</span> </a>
                <a href="javascript:void" id='eliminar' class="shortcut noAjax"><i class="shortcut-icon icon-trash"></i><span class="shortcut-label">Eliminar</span> </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Declaraciones de Ficheros pendientes de enviar de <?php echo $cliente['razonSocial']; ?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable tablaNo">
              <thead>
                <tr>
                  <th> Empresa </th>
                  <th> Nombre fichero/tratamiento </th>
                  <th> Fecha registro/baja </th>
                  <th> Teléfono </th>
                  <th> eMail </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todoNo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeDeclaracionesLOPD($cliente['codigo']);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
      </div>

      <div class="span12">        
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Declaraciones de Ficheros enviados de <?php echo $cliente['razonSocial']; ?></h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable tablaSi">
              <thead>
                <tr>
                  <th> Empresa </th>
                  <th> Nombre fichero/tratamiento </th>
                  <th> Fecha envío </th>
                  <th> Teléfono </th>
                  <th> eMail </th>
                  <th> Identificador </th>
                  <th> Nº Registro </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todoSi"></th>
                </tr>
              </thead>
              <tbody>

                <?php
                  imprimeDeclaracionesLOPD($cliente['codigo'],'NO');
                ?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
      


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#todoNo').click(function(e){ 
        $('.tablaNo td input:checkbox').prop('checked',this.checked);
      });

      $('#todoSi').click(function(e){    
        $('.tablaSi td input:checkbox').prop('checked',this.checked);
      });
    });
  </script>
<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
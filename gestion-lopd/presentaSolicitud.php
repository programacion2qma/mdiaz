<?php
  $seccionActiva=12;
  include_once('../cabecera.php');

  //Carga de librerías SOAP y firma XML
  require_once("../firma-xml/xmlseclibs/xmlseclibs.php");
  require_once("../firma-xml/xmlsoap/src/Soap/SoapClient.php");
  require_once("../firma-xml/xmlsoap/src/Adapter/AdapterInterface.php");
  require_once("../firma-xml/xmlsoap/src/Adapter/XmlseclibsAdapter.php");
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12 margenAb">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-send"></i>
              <h3>Envío de solicitud a la Agencia Española de Protección de Datos</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Resultado de presentación de la declaración</h6>
                   <div id="big_stats" class="cf centro">
                     
                      <?php
                        finalizarOperacionesLOPD($_GET['codigo'],$_GET['cliente'],'Finalizar');
                      ?>
                     
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
         
        </div>
        <!-- /span12 -->	  

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesDelegaciones(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDelegacion();
	}
	elseif(isset($_POST['usuario1Delegaciones'])){
		$res=insertaDelegacion();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('delegaciones_lopd');
	}

	tieneDelegaciones();

	mensajeResultado('usuario1Delegaciones',$res,'Delegación');
    mensajeResultado('elimina',$res,'Delegación', true);
}

function insertaDelegacion(){
	$res=true;
	preparaFacultades();
	$res=insertaDatos('delegaciones_lopd');
	return $res;
}

function actualizaDelegacion(){
	$res=true;
	preparaFacultades();
	$res=actualizaDatos('delegaciones_lopd');
	return $res;
}

function tieneDelegaciones(){
	$codigoCliente=obtenerCodigoCliente(true);
	$trabajos=consultaBD('SELECT trabajos.* FROM trabajos 
	INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo
	LEFT JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo
	WHERE codigoCliente='.$codigoCliente.'
	AND (servicios_familias.referencia="LOPD1" OR servicios.servicio LIKE "%LOPD%")',true);
	while($trabajo=mysql_fetch_assoc($trabajos)){
		$delegaciones=consultaBD('SELECT COUNT(codigo) AS total FROM delegaciones_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true,true);
		if($delegaciones['total']==0){
			actualizaFormulario($trabajo['codigo'],'NO');
		} else {
			actualizaFormulario($trabajo['codigo'],'SI');
		}
	}
}

function preparaFacultades(){
	$res='';
	for($i=1;$i<=22;$i++){
		if(isset($_POST['facultades'.$i]) && $_POST['facultades'.$i]=='SI'){
			if($res!=''){
				$res.='&$&';
			}
			$res.=$i;
		}
	}
	$_POST['facultades']=$res;
}

function actualizaFormulario($codigoTrabajo,$valor='SI'){
	$res=true;
	$trabajo=datosRegistro('trabajos',$codigoTrabajo);
	$formulario=recogerFormularioServicios($trabajo);
	$query='';
	$i=1;
	foreach ($formulario as $key => $value) {
		if($i>1){
			$query.='&{}&';
		}
		if($key=='pregunta425'){
			$query.=$key.'=>'.$valor;
		} else {
			$query.=$key.'=>'.$value;
		}
		$i++;
	}
	$res = consultaBD("UPDATE trabajos SET formulario = '".$query."' WHERE codigo=".$trabajo['codigo'],true);
	return $res;
}


function listadoDelegaciones(){
	global $_CONFIG;

	$columnas=array('usuario1Delegaciones','activo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT delegaciones_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio, u1.nombreUsuarioLOPD AS usuario1, u2.nombreUsuarioLOPD AS usuario2 FROM delegaciones_lopd INNER JOIN trabajos ON delegaciones_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo LEFT JOIN usuarios_lopd u1 ON delegaciones_lopd.usuario1Delegaciones=u1.codigo LEFT JOIN usuarios_lopd u2 ON delegaciones_lopd.usuario2Delegaciones=u2.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT delegaciones_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio, u1.nombreUsuarioLOPD AS usuario1, u2.nombreUsuarioLOPD AS usuario2 FROM delegaciones_lopd INNER JOIN trabajos ON delegaciones_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo LEFT JOIN usuarios_lopd u1 ON delegaciones_lopd.usuario1Delegaciones=u1.codigo LEFT JOIN usuarios_lopd u2 ON delegaciones_lopd.usuario2Delegaciones=u2.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	$i=1;
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$i,
			$datos['usuario1'],
			$datos['usuario2'],
			formateaFechaWeb($datos['fechaDelegaciones']),
			formateaFechaWeb($datos['fechaFinDelegaciones']),
			formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'],
			creaBotonDetalles("zona-cliente-delegaciones/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);
		$i++;
		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionDelegaciones(){
	operacionesDelegaciones();

	abreVentanaGestion('Gestión de Delegaciones','?','','icon-edit','margenAb');
	$datos=compruebaDatos('delegaciones_lopd');

    if(!$datos){
    	abreColumnaCampos();
    		campoSelectConsulta('codigoTrabajo','Consultoría','SELECT trabajos.codigo, servicios.servicio AS texto FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE codigoCliente='.obtenerCodigoCliente(true),false,'selectpicker span5');
    	cierraColumnaCampos(true);
    }

	abreColumnaCampos();
		campoSelectConsulta('usuario1Delegaciones','Usuario que delega','SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$datos['codigoTrabajo'],$datos);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoSelectConsulta('usuario2Delegaciones','Usuario en quien delega','SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$datos['codigoTrabajo'],$datos);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoFecha('fechaDelegaciones','Fecha inicio delegación',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoFecha('fechaFinDelegaciones','Fecha fin delegación',$datos);
	cierraColumnaCampos();

	echo '<br clear="all"><h3 class="apartadoFormulario">Facultades delegadas</h3>';

	$facultades=explode('&$&',$datos['facultades']);
	$listado=array('Actualizar y difundir los registros y documentos preceptivos para el cumplimiento de la normativa en vigor en materia de Protección de Datos de las Personas Físicas.','Ejercer el deber de información','Solicitar peticiones de consentimiento','Difundir entre el personal las normas referentes a Protección de Datos de Carácter Personal que les afectan, así como las consecuencias por su incumplimiento.','Concesión de permisos de acceso a datos  al personal propio','Asignar las claves de acceso a cada usuario','Revisar que se cambien las claves de usuario y contraseñas anualmente','Contratar los servicios de los encargados del tratamiento y firmar el correspondiente contrato de encargado del tratamiento','Autorizar la ejecución de trabajos fuera de los locales del responsable o del encargado del tratamiento','Realizar una auditoría cada dos años o ante modificaciones sustanciales en los sistemas de información con repercusiones en seguridad o encargarla a un tercero, propio o ajeno','Autorizar  las entradas y salidas de soportes y datos, incluidas las que se realizan a través de redes de telecomunicaciones','Registrar las entradas y salidas de soportes y datos','Registrar las entradas y salidas de datos por red','Revisar semestralmente los procedimientos de generación de copias de respaldo y recuperación de datos','Analizar trimestralmente las incidencias y poner medidas correctoras','Registrar las incidencias que se produzcan (tipo, momento de su detección, persona que la notifica, efectos y medidas correctoras adoptadas) y notificarlas a quién corresponda','Autorizar la ejecución del procedimiento de recuperación de datos','Registrar los procedimientos de recuperación, persona que lo ejecuta, datos restaurados y, en su caso, datos grabados manualmente','Designar  Responsables de Seguridad','Designar al Delegado de Protección de Datos','Delegar responsabilidades propias de su cargo así como cualquiera de las funciones y obligaciones que le correspondan por  delegación en otros miembros de la plantilla','Otras');
	$i=1;
	foreach ($listado as $value) {
		$check='NO';
		if(in_array($i, $facultades)){
			$check='SI';
		}
		campoCheckIndividual('facultades'.$i,$value,$check);
		$i++;
	}
	echo '<div id="divOtrasDelegaciones" class="hide">';
		campoTexto('otras','Indicar',$datos);
	echo '</div>';
	cierraVentanaGestion('index.php',true);
}

function recogeUsuarios(){
	$res='<option value="NULL">&nbsp;</option>';
	$datos=arrayFormulario();
	$listado=consultaBD('SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$datos['codigoTrabajo'],true);
	while($item=mysql_fetch_assoc($listado)){
		$res.='<option value="'.$item['codigo'].'">'.$item['texto'].'</value>';
	}

	echo $res;
}

/*$facultades=array('','Actualizar y difundir los registros y documentos preceptivos para el cumplimiento de la normativa en vigor en materia de Protección de Datos de las Personas Físicas.','Ejercer el deber de información','Solicitar peticiones de consentimiento','Difundir entre el personal las normas referentes a Protección de Datos de Carácter Personal que les afectan, así como las consecuencias por su incumplimiento.','Concesión de permisos de acceso a datos  al personal propio','Asignar las claves de acceso a cada usuario','Revisar que se cambien las claves de usuario y contraseñas anualmente','Contratar los servicios de los encargados del tratamiento y firmar el correspondiente contrato de encargado del tratamiento','Autorizar la ejecución de trabajos fuera de los locales del responsable o del encargado del tratamiento','Realizar una auditoría cada dos años o ante modificaciones sustanciales en los sistemas de información con repercusiones en seguridad o encargarla a un tercero, propio o ajeno','Autorizar  las entradas y salidas de soportes y datos, incluidas las que se realizan a través de redes de telecomunicaciones','Registrar las entradas y salidas de soportes y datos','Registrar las entradas y salidas de datos por red','Revisar semestralmente los procedimientos de generación de copias de respaldo y recuperación de datos','Analizar trimestralmente las incidencias y poner medidas correctoras','Registrar las incidencias que se produzcan (tipo, momento de su detección, persona que la notifica, efectos y medidas correctoras adoptadas) y notificarlas a quién corresponda','Autorizar la ejecución del procedimiento de recuperación de datos','Registrar los procedimientos de recuperación, persona que lo ejecuta, datos restaurados y, en su caso, datos grabados manualmente','Designar  Responsables de Seguridad','Designar al Delegado de Protección de Datos','Delegar responsabilidades propias de su cargo así como cualquiera de las funciones y obligaciones que le correspondan por  delegación en otros miembros de la plantilla','Otras (espacio de campo libre para dar la posibilidad al usuario a incluir un texto con otras facultades a delegar)');
    	$valores=array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22);
    	if($trabajo){
    			$delegaciones=consultaBD('SELECT * FROM delegaciones_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
    			while($delegacion=mysql_fetch_assoc($delegaciones)){
    				echo "<tr>";
    					campoOculto($delegacion['codigo'],'codigoExisteDelegacionesLOPD_'.$j);
    					campoSelectMultiple('facultadesDelegacionesLOPD_'.$j,'',$facultades,$valores,$delegacion['facultades'],'selectpicker span3 show-tick','data-live-search="true"',1);
    					campoSelectConsulta('usuario1DelegacionesLOPD_'.$j,'','SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$trabajo['codigo'],$delegacion['usuario1Delegaciones'],'selectpicker span3 show-tick',"data-live-search='true'",'',1);
    					campoSelectConsulta('usuario2DelegacionesLOPD_'.$j,'','SELECT codigo, nombreUsuarioLOPD AS texto FROM usuarios_lopd WHERE codigoTrabajo='.$trabajo['codigo'],$delegacion['usuario2Delegaciones'],'selectpicker span3 show-tick',"data-live-search='true'",'',1);
						campoFechaTabla('fechaDelegacionesLOPD_'.$j,$delegacion['fechaDelegaciones']);
						campoFechaTabla('fechaFinDelegacionesLOPD_'.$j,$delegacion['fechaFinDelegaciones']);
					echo"
					</tr>";
					$j++;
				}
    	}
*/

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function datosPersonales($datos,$formulario){
	if($datos['familia']=='LOPD1'){
		$datos['razonSocial']=$formulario['pregunta3'];
		$datos['domicilio']=$formulario['pregunta4'];
		$datos['cp']=$formulario['pregunta10'];
		$datos['localidad']=$formulario['pregunta5'];
		$datos['provincia']=convertirMinuscula($formulario['pregunta11']);
		$datos['cif']=$formulario['pregunta9'];
	}

	return $datos;
}

function sanearCaracteres($texto){
	$texto = str_replace( '&', '&#38;', $texto);
	$texto = str_replace( '–', '&#45;', $texto);
	$texto = str_replace( '<br />', '<w:br/>', $texto);
	$texto = str_replace( '"', '', $texto);
	return $texto;
}

function reemplazarLogo($documento,$datos,$imagen='image1.png',$campo='ficheroLogo'){
	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos[$campo]);
    if($hayLogo!='NO' && $hayLogo!=''){
    	$logo = '../documentos/logos-clientes/'.$datos[$campo];
    	$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
		if (!copy($logo, $nuevo_logo)) {
    		echo "Error al copiar $fichero...\n";
		}	
		$documento->replaceImage('../documentos/logos-clientes/',$imagen);	
	}
}

function fechaTabla($fecha,$texto){
	return '<w:p w14:paraId="1060A2A2" w14:textId="7A44C6D1" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="00397BF1" w:rsidP="0016753F"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">REGISTRO DE '.$texto.' A </w:t></w:r><w:r w:rsidR="00D303CF"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$fecha.'</w:t></w:r></w:p>';
}

function pieTabla($total){
	return '<w:p w14:paraId="2CF98860" w14:textId="77777777" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="001F0F57" w:rsidP="001F0F57"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="right"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>Nº total de re</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">gistros </w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$total.'</w:t></w:r></w:p>';
}

function generaDocumento($codigoCliente){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
	
	conexionBD();

	zipAnexoIXLOPD($codigoCliente, $PHPWord);

	cierraBD();	

}

function zipAnexoIXLOPD($codigoCliente, $PHPWord){
	$documentos=array();
	$i=1;
	$listado=consultaBD('SELECT trabajos.codigo FROM trabajos INNER JOIN delegaciones_lopd ON trabajos.codigo=delegaciones_lopd.codigoTrabajo WHERE trabajos.codigoCliente='.$codigoCliente.' GROUP BY trabajos.codigo',true);
	while($trabajo=mysql_fetch_assoc($listado)){
		array_push($documentos,anexoIXLOPD($trabajo['codigo'],$PHPWord,$i,'AnexoIX.docx'));
		$i++;
		$listado2=consultaBD('SELECT * FROM delegaciones_lopd WHERE codigoTrabajo='.$trabajo['codigo'],true);
		while($item=mysql_fetch_assoc($listado2)){
			array_push($documentos,anexoIXLOPDIND($item['codigo'],$PHPWord,$i,'AnexoIX_ind.docx'));
			$i++;
		}
	}
	$zip = new ZipArchive();
    $nameZip = '../documentos/consultorias/anexos_IX.zip';
    if(file_exists($nameZip)){
        unlink($nameZip);
    }
    $ficheroZip = $nameZip;
    if($zip->open($ficheroZip,ZIPARCHIVE::CREATE)===true) {
        foreach ($documentos as $doc){
            $zip->addFile('../documentos/consultorias/'.$doc , $doc);
        }
    }
    if(!$zip->close()){
        echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
        echo $zip->getStatusString();
    }
	
	header("Content-Type: application/zip");
    header("Content-Disposition: attachment; filename=anexos_IX.zip");
    header("Content-Transfer-Encoding: binary");

    readfile($nameZip);
}

function anexoIXLOPD($codigo, $PHPWord, $i, $nombreFichero=''){
	$fichero='ANEXO_IX_AUTORIZACIONES_DELEGADAS_'.$i.'.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario, ficheroLogo FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE trabajos.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",fecha());
	reemplazarLogo($documento,$datos,'image3.png');

	$tabla=fechaTabla(date("d/m/Y"),'AUTORIZACIONES DELEGADAS');
	$tabla.='<w:tbl><w:tblPr><w:tblW w:w="8613" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="702"/><w:gridCol w:w="1958"/><w:gridCol w:w="2212"/><w:gridCol w:w="1298"/><w:gridCol w:w="2443"/></w:tblGrid><w:tr w:rsidR="0088254E" w:rsidRPr="008F5EA5" w14:paraId="2C498856" w14:textId="77777777" w:rsidTr="00E16BE0"><w:tc><w:tcPr><w:tcW w:w="702" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="75431AAF" w14:textId="1875C639" w:rsidR="0088254E" w:rsidRPr="008F5EA5" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF</w:t></w:r><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1958" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2A63A81B" w14:textId="01100CE1" w:rsidR="0088254E" w:rsidRPr="008F5EA5" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>USUARIO QUE DELEGA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2212" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="4C34E974" w14:textId="7D1A7013" w:rsidR="0088254E" w:rsidRPr="008F5EA5" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>USUARIO EN QUIEN DELEGA</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1298" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="36C57E99" w14:textId="444AE695" w:rsidR="0088254E" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA INICIO DELEGACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2443" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="24ECD239" w14:textId="4F9F5356" w:rsidR="0088254E" w:rsidRPr="008F5EA5" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA FINALIZACIÓN DELEGACIÓN</w:t></w:r></w:p></w:tc></w:tr>';
	$i=1;
	$delegaciones=consultaBD("SELECT u1.nombreUsuarioLOPD AS usuario1, u2.nombreUsuarioLOPD AS usuario2, fechaDelegaciones, fechaFinDelegaciones, facultades, otras FROM delegaciones_lopd d LEFT JOIN usuarios_lopd u1 ON d.usuario1Delegaciones=u1.codigo LEFT JOIN usuarios_lopd u2 ON d.usuario2Delegaciones=u2.codigo WHERE d.codigoTrabajo='$codigo';",true);
	$valores=array('','Actualizar y difundir los registros y documentos preceptivos para el cumplimiento de la normativa en vigor en materia de Protección de Datos de las Personas Físicas.','Ejercer el deber de información','Solicitar peticiones de consentimiento','Difundir entre el personal las normas referentes a Protección de Datos de Carácter Personal que les afectan, así como las consecuencias por su incumplimiento.','Concesión de permisos de acceso a datos  al personal propio','Asignar las claves de acceso a cada usuario','Revisar que se cambien las claves de usuario y contraseñas anualmente','Contratar los servicios de los encargados del tratamiento y firmar el correspondiente contrato de encargado del tratamiento','Autorizar la ejecución de trabajos fuera de los locales del responsable o del encargado del tratamiento','Realizar una auditoría cada dos años o ante modificaciones sustanciales en los sistemas de información con repercusiones en seguridad o encargarla a un tercero, propio o ajeno','Autorizar  las entradas y salidas de soportes y datos, incluidas las que se realizan a través de redes de telecomunicaciones','Registrar las entradas y salidas de soportes y datos','Registrar las entradas y salidas de datos por red','Revisar semestralmente los procedimientos de generación de copias de respaldo y recuperación de datos','Analizar trimestralmente las incidencias y poner medidas correctoras','Registrar las incidencias que se produzcan (tipo, momento de su detección, persona que la notifica, efectos y medidas correctoras adoptadas) y notificarlas a quién corresponda','Autorizar la ejecución del procedimiento de recuperación de datos','Registrar los procedimientos de recuperación, persona que lo ejecuta, datos restaurados y, en su caso, datos grabados manualmente','Designar  Responsables de Seguridad','Designar al Delegado de Protección de Datos','Delegar responsabilidades propias de su cargo así como cualquiera de las funciones y obligaciones que le correspondan por  delegación en otros miembros de la plantilla','Otras');
	while($delegacion=mysql_fetch_assoc($delegaciones)){
		$ref=str_pad($i,2,'0',STR_PAD_LEFT);
		$facultades='';
		$listado=explode('&$&', $delegacion['facultades']);
		foreach ($listado as $key => $value) {
			if($value==22){
				$facultades.='- '.$delegacion['otras'].'<w:br/>';
			} else {
				$facultades.='- '.$valores[$value].'<w:br/>';
			}
		}
		$tabla.='<w:tr w:rsidR="0088254E" w14:paraId="6D301254" w14:textId="77777777" w:rsidTr="00E16BE0"><w:tc><w:tcPr><w:tcW w:w="702" w:type="dxa"/></w:tcPr><w:p w14:paraId="1C00047D" w14:textId="77777777" w:rsidR="0088254E" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1958" w:type="dxa"/></w:tcPr><w:p w14:paraId="47B9008F" w14:textId="0126DC36" w:rsidR="0088254E" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$delegacion['usuario1'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2212" w:type="dxa"/></w:tcPr><w:p w14:paraId="1F573CED" w14:textId="60695767" w:rsidR="0088254E" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$delegacion['usuario2'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1298" w:type="dxa"/></w:tcPr><w:p w14:paraId="1818413C" w14:textId="32ED7830" w:rsidR="0088254E" w:rsidRDefault="00E16BE0" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWord($delegacion['fechaDelegaciones']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2443" w:type="dxa"/></w:tcPr><w:p w14:paraId="70C914CE" w14:textId="51968FD2" w:rsidR="0088254E" w:rsidRPr="002B0C55" w:rsidRDefault="0088254E" w:rsidP="0088254E"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWord($delegacion['fechaFinDelegaciones']).'</w:t></w:r></w:p></w:tc></w:tr>';
		$i++;
	}

	$total=$i-1;
	$tabla.='</w:tbl>'.pieTabla($total);
	$documento->setValue("tabla",utf8_decode($tabla));

	
	$documento->save('../documentos/consultorias/'.$fichero);
	

    return $fichero;
}

function anexoIXLOPDIND($codigo, $PHPWord,$i,$nombreFichero=''){
	$fichero='ANEXO_IX_AUTORIZACIONES_DELEGADAS_'.$i.'.docx';
	$datos=consultaBD("SELECT clientes.razonSocial, clientes.domicilio, clientes.cp, clientes.localidad, clientes.cif, clientes.provincia, servicios_familias.referencia AS familia, formulario, ficheroLogo, u1.nombreUsuarioLOPD AS usuario1, u1.nifUsuarioLOPD AS nif1, u2.nombreUsuarioLOPD AS usuario2, u2.nifUsuarioLOPD AS nif2, fechaDelegaciones, fechaFinDelegaciones, facultades, otras 
		FROM delegaciones_lopd
		INNER JOIN trabajos ON delegaciones_lopd.codigoTrabajo=trabajos.codigo 
		INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo 
		INNER JOIN usuarios_lopd u1 ON delegaciones_lopd.usuario1Delegaciones=u1.codigo 
		INNER JOIN usuarios_lopd u2 ON delegaciones_lopd.usuario2Delegaciones=u2.codigo 
		INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo
		WHERE delegaciones_lopd.codigo = ".$codigo,true,true);
	$formulario = recogerFormularioServicios($datos);
	$datos=datosPersonales($datos,$formulario);
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("localidad",utf8_decode(sanearCaracteres($datos['localidad'])));
	$documento->setValue("usuario1",utf8_decode(sanearCaracteres($datos['usuario1'])));
	$documento->setValue("usuario2",utf8_decode(sanearCaracteres($datos['usuario2'])));
	$documento->setValue("nif1",utf8_decode(sanearCaracteres($datos['nif1'])));
	$documento->setValue("nif2",utf8_decode(sanearCaracteres($datos['nif2'])));
	$documento->setValue("fechaInicio",utf8_decode(formateaFechaWeb($datos['fechaDelegaciones'])));
	if($datos['fechaFinDelegaciones']!='' && $datos['fechaFinDelegaciones']!='0000-00-00'){
		$fechaFin=' hasta el '.formateaFechaWeb($datos['fechaFinDelegaciones']);
	} else {
		$fechaFin='';
	}
	$documento->setValue("fechaFin",utf8_decode($fechaFin));
	reemplazarLogo($documento,$datos,'image3.png');

	$facultades=array('','Actualizar y difundir los registros y documentos preceptivos para el cumplimiento de la normativa en vigor en materia de Protección de Datos de las Personas Físicas.','Ejercer el deber de información','Solicitar peticiones de consentimiento','Difundir entre el personal las normas referentes a Protección de Datos de Carácter Personal que les afectan, así como las consecuencias por su incumplimiento.','Concesión de permisos de acceso a datos  al personal propio','Asignar las claves de acceso a cada usuario','Revisar que se cambien las claves de usuario y contraseñas anualmente','Contratar los servicios de los encargados del tratamiento y firmar el correspondiente contrato de encargado del tratamiento','Autorizar la ejecución de trabajos fuera de los locales del responsable o del encargado del tratamiento','Realizar una auditoría cada dos años o ante modificaciones sustanciales en los sistemas de información con repercusiones en seguridad o encargarla a un tercero, propio o ajeno','Autorizar  las entradas y salidas de soportes y datos, incluidas las que se realizan a través de redes de telecomunicaciones','Registrar las entradas y salidas de soportes y datos','Registrar las entradas y salidas de datos por red','Revisar semestralmente los procedimientos de generación de copias de respaldo y recuperación de datos','Analizar trimestralmente las incidencias y poner medidas correctoras','Registrar las incidencias que se produzcan (tipo, momento de su detección, persona que la notifica, efectos y medidas correctoras adoptadas) y notificarlas a quién corresponda','Autorizar la ejecución del procedimiento de recuperación de datos','Registrar los procedimientos de recuperación, persona que lo ejecuta, datos restaurados y, en su caso, datos grabados manualmente','Designar  Responsables de Seguridad','Designar al Delegado de Protección de Datos','Delegar responsabilidades propias de su cargo así como cualquiera de las funciones y obligaciones que le correspondan por  delegación en otros miembros de la plantilla','Otras');
	$texto='';
	$listado=explode('&$&',$datos['facultades']);
	$i=1;
	foreach ($listado as $value) {
		if($texto!=''){
			$texto.='<w:br/><w:br/>';
		}
		if($value==22){
			$texto.=$i.'ª. '.$datos['otras'];
		} else {
			$texto.=$i.'ª. '.$facultades[$value];
		}
		$i++;
	}
	$documento->setValue("funciones",utf8_decode($texto));

	$documento->save('../documentos/consultorias/'.$fichero);
	

    return $fichero;
}

function formateaFechaWord($fecha){
	if($fecha!='' && strpos($fecha, '/')==false){
		$fecha=formateaFechaWeb($fecha);
	}
	return $fecha;
}
//Fin parte de agrupaciones
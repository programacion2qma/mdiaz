<?php
  $seccionActiva=67;
  include_once("../cabecera.php");
  gestionDelegaciones();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	if($('#codigoTrabajo').length){
		$('#codigoTrabajo').change(function(){
			recogerUsuarios($(this).val());
		});
	}

	mostrarDivs();
	$('#facultades22').change(function(){
		mostrarDivs();
	});
});

function recogerUsuarios(valor){
	var consulta=$.post('../listadoAjax.php?include=zona-cliente-delegaciones&funcion=recogeUsuarios();',{'codigoTrabajo':valor});
	consulta.done(function(respuesta){
		$('#usuario1Delegaciones').html(respuesta).selectpicker('refresh');
		$('#usuario2Delegaciones').html(respuesta).selectpicker('refresh');
	});
}

function mostrarDivs(){
	if($('#facultades22').attr('checked')=='checked'){
		$('#divOtrasDelegaciones').removeClass('hide');
	} else {
		$('#divOtrasDelegaciones').addClass('hide');
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
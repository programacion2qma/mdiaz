<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de ventas de servicios

function operacionesTrabajos(){
	$res=true;


	if(isset($_POST['gratuita'])){
		$res=creaConsultoriaGratuita();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=actualizaServicios();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('trabajos');
	}

	mensajeResultado('codigoCliente',$res,'Consultoría');
    mensajeResultado('elimina',$res,'Consultoría', true);
}

function creaEstadisticasTrabajos(){

   	$indice=array();
   	$indice[0]='TOTAL';
   	$codigos=array();
   	$i=1;
   	$servicios=consultaBD("SELECT * FROM servicios_familias ORDER BY referencia",true);
   	while($servicio=mysql_fetch_assoc($servicios)){
   		$referencia = str_replace(' ', '_', $servicio['referencia']);
   		$indice[$i] = $referencia;
   		$codigos[$i] = $servicio['codigo'];
   		$i++;
   	}	
   	$estadisticas=array();
   	for($i=0;$i<count($indice);$i++){
   		$where='';
   		$where2='WHERE';

   		if($_SESSION['tipoUsuario'] == 'CONSULTOR'){
			/*$where = ' INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo WHERE clientes.codigoConsultor = '.$_SESSION['codigoU'];
			$where2 = ' INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo WHERE clientes.codigoConsultor = '.$_SESSION['codigoU'].' AND';*/
		}
   		if($i>0){
   			if($_SESSION['tipoUsuario'] == 'CONSULTOR'){
   				$where .= ' AND codigoFamilia='.$codigos[$i];
   				$where2 .= ' codigoFamilia='.$codigos[$i].' AND';
   			} else {
   				$where = 'WHERE codigoFamilia='.$codigos[$i];
   				$where2 = 'WHERE codigoFamilia='.$codigos[$i].' AND';
   			}
   		}
   		
   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where ,true, true);
   		$estadisticas['total_'.$indice[$i]] = $res['total'];

   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." gratuita='NO'" ,true, true);
   		$estadisticas['ventas_'.$indice[$i]] = $res['total'];
   		$estadisticas['gratuitas_'.$indice[$i]] = $estadisticas['total_'.$indice[$i]] - $estadisticas['ventas_'.$indice[$i]];

   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." envio='SI'" ,true, true);
   		$estadisticas['finalizadas_'.$indice[$i]] = $res['total'];
   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." envio='SI' AND gratuita='NO'" ,true, true);
   		$estadisticas['finalizadasVentas_'.$indice[$i]] = $res['total'];
   		$estadisticas['finalizadasGratuitas_'.$indice[$i]] = $estadisticas['finalizadas_'.$indice[$i]] - $estadisticas['finalizadasVentas_'.$indice[$i]];

   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." toma_datos='SI'" ,true, true);
   		$estadisticas['tomaDatos_'.$indice[$i]] = $res['total'];
   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." toma_datos='SI' AND gratuita='NO'" ,true, true);
   		$estadisticas['tomaDatosVentas_'.$indice[$i]] = $res['total'];
   		$estadisticas['tomaDatosGratuitas_'.$indice[$i]] = $estadisticas['tomaDatos_'.$indice[$i]] - $estadisticas['tomaDatosVentas_'.$indice[$i]];

   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." envio='NO' AND fechaPrevista < CURDATE()" ,true, true);
   		$estadisticas['vencidas_'.$indice[$i]] = $res['total'];
   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." envio='NO' AND fechaPrevista < CURDATE() AND gratuita='NO'" ,true, true);
   		$estadisticas['vencidasVentas_'.$indice[$i]] = $res['total'];
   		$estadisticas['vencidasGratuitas_'.$indice[$i]] = $estadisticas['vencidas_'.$indice[$i]] - $estadisticas['vencidasVentas_'.$indice[$i]];

   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." envio='NO' AND fechaPrevista > CURDATE() AND fechaPrevista < DATE_ADD(CURDATE(), INTERVAL 6 DAY)" ,true, true);
   		$estadisticas['vencidasSiete_'.$indice[$i]] = $res['total'];
   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." envio='NO' AND fechaPrevista > CURDATE() AND fechaPrevista < DATE_ADD(CURDATE(), INTERVAL 6 DAY) AND gratuita='NO'" ,true, true);
   		$estadisticas['vencidasSieteVentas_'.$indice[$i]] = $res['total'];
   		$estadisticas['vencidasSieteGratuitas_'.$indice[$i]] = $estadisticas['vencidasSiete_'.$indice[$i]] - $estadisticas['vencidasSieteVentas_'.$indice[$i]];

   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." incidencia='SI'" ,true, true);
   		$estadisticas['incidencias_'.$indice[$i]] = $res['total'];
   		$res=consultaBD("SELECT COUNT(trabajos.codigo) as total FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo ".$where2." incidencia='SI' AND gratuita='NO'" ,true, true);
   		$estadisticas['incidenciasVentas_'.$indice[$i]] = $res['total'];
   		$estadisticas['incidenciasGratuitas_'.$indice[$i]] = $estadisticas['incidencias_'.$indice[$i]] - $estadisticas['incidenciasVentas_'.$indice[$i]];
   	}

   return $estadisticas;
}



function listadoTrabajos(){
	global $_CONFIG;

	$columnas=array('cliente','clientes.razonSocial','trabajo','trabajos.codigoServicio','trabajos.gratuita','trabajos.toma_datos','trabajos.revision','trabajos.envio','trabajos.mantenimiento','trabajos.fechaTomaDatos','trabajos.fechaRevision','trabajos.fechaEnvio','trabajos.fechaMantenimiento','trabajos.fechaTomaDatos','trabajos.fechaRevision','trabajos.fechaEnvio','trabajos.fechaMantenimiento','trabajos.fechaPrevista','trabajos.fechaPrevista','trabajos.fechaEmision','trabajos.emision','trabajos.fechaEmision','trabajos.fechaEntrega','trabajos.entrega','trabajos.fechaEntrega','trabajos.incidencia');
	$where=obtieneWhereListado("WHERE 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
		
	if($_SESSION['tipoUsuario'] == 'CONSULTOR'){
		$where .= ' AND clientes.codigoConsultor = '.$_SESSION['codigoU'];
	}
	
	conexionBD();
	$consulta=consultaBD("SELECT clientes.codigo as cliente, trabajos.codigo as trabajo, clientes.razonSocial, trabajos.codigoServicio, codigoUsuario, codigoUsuarioAsociado FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo $where GROUP BY clientes.codigo $orden $limite");
	$consultaPaginacion=consultaBD("SELECT clientes.codigo as cliente, trabajos.codigo as trabajo, clientes.razonSocial, trabajos.codigoServicio, codigoUsuario, codigoUsuarioAsociado FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo $where GROUP BY clientes.codigo");

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
        $textos = array('Detalles','Toma de datos','Generación de documentos');
        $enlaces = array("clientes/gestion.php?codigo=".$datos['cliente'],"consultoria/gestion.php?codigoCliente=".$datos['cliente'],"consultoria/gestion.php?codigoCliente=".$datos['cliente']);
        $iconos = array('icon-search-plus','icon-edit','icon-file-text-o');
        $lopd = consultaBD("SELECT COUNT(codigo) AS codigo FROM trabajos WHERE codigoCliente = ".$datos['cliente']." AND codigoServicio = (SELECT codigo FROM servicios WHERE referencia = 'LOPD')",true, true);
        if($lopd['codigo'] > 0){
            $textos[3] = 'Declaraciones LOPD';
            $enlaces[3] = 'gestion-lopd/index.php?codigoCliente='.$datos['cliente'];
            $iconos[3] = 'icon-flag';
        }
        $fila=array(
            $datos['razonSocial'],
            creaTablaServicios($datos['cliente'],$where),
            "DT_RowId"=>$datos['trabajo']
        );

        $res['aaData'][]=$fila;
    }

	cierraBD();

	echo json_encode($res);
}

function creaTablaServicios($cliente,$where){
	$tabla="
	<button class='btn btn-propio mostrarServicios' estado='oculto' tabla='#tabla$cliente'> <i class='icon-caret-down'></i> Mostrar servicios <span class='badge badge-danger'></span> </button>
    <div id='tabla$cliente' class='hide'>
        <table class='tabla-simple ancho100'>
		<thead>
			<tr>
				<th>Servicio</th>
				<th>Fecha registro</th>
				<th>Días desde registro</th>
				<th></th>
			<tr>
		</thead>
		<tbody>";

		$servicios = consultaBD("SELECT trabajos.* FROM trabajos INNER JOIN clientes ON trabajos.codigoCliente=clientes.codigo LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo ".$where." AND trabajos.codigoCliente=".$cliente." ORDER BY codigoServicio",true);
		while($servicio=mysql_fetch_assoc($servicios)){
			$servi=datosRegistro('servicios',$servicio['codigoServicio']);
			$fecha = new DateTime($servicio['fecha']);
			$fechaHoy = new DateTime(fechaBD());
			$interval = $fecha->diff($fechaHoy);
			$tabla.= "<tr>
                    <td>".$servi['referencia']."</td>
                    <td>".formateaFechaWeb($servicio['fecha'])."</td>
                    <td>".$interval->format('%a días')."</td>
                    <td class='centro'>".botonAcciones(array('Detalles','Documentación'),array('consultoria/gestion.php?codigoTrabajo='.$servicio['codigo'],'consultoria/generaDocumento.php?codigoTrabajo='.$servicio['codigo']),array('icon-search-plus','icon-download'),false,false,'')."</td>
                </tr>";

        }
                    
         $tabla.="        
            </tbody>
        </table>
    </div>";
    
    return $tabla;
}

function obtieneFechaLimite($fecha){
	$fechaFinal = formateaFechaWeb($fecha);
	$fecha = new DateTime($fecha);
	$fecha->sub(new DateInterval('P7D'));
	$fechaWarning = $fecha->format('d/m/Y');
	$label = 'success';
	if(compruebaFechaRevision($fechaFinal)){
		$label = 'danger';
	} else if(compruebaFechaRevision($fechaWarning)){
		$label = 'warning';
	}
	return "<label class='label label-".$label."'>".$fechaFinal."</label>";
}

function obtieneFechaLimiteAJAX($fecha,$suma){
	$fecha = formateaFechaBD($fecha);
	$fecha = new DateTime($fecha);
	$fecha->add(new DateInterval($suma));
	return $fecha->format('d/m/Y');
}


function compruebaFechaRevision($fechaRevision){
	$res=false;
	if($fechaRevision != ''){
		$fecha=explode('/',$fechaRevision);

		if($fecha[2]<date('Y')){
			$res=true;
		}
		elseif($fecha[2]==date('Y') && $fecha[1]<date('m')){
			$res=true;
		}
		elseif($fecha[2]==date('Y') && $fecha[1]==date('m') && $fecha[0]<date('d')){
			$res=true;
		}
	}

	return $res;
}

function gestionConsultoria(){
	operacionesTrabajos();
	if(isset($_GET['codigoTrabajo']) && $_GET['codigoTrabajo']!=''){
		$_REQUEST['codigo'] = $_GET['codigoTrabajo'];
	}
	abreVentanaGestion('Gestión de Consultoría','?');
	$datos=compruebaDatos('trabajos');

	campoOculto($datos,'codigoVenta','NULL');
	campoOculto($datos,'formulario');
	campoOculto($datos,'gratuita','SI');

	campoFecha('fecha','Fecha',$datos);
	campoSelectClienteFiltradoPorUsuario($datos);
	campoSelectConsulta('codigoServicio','Servicio','SELECT codigo, servicio AS texto FROM servicios ORDER BY servicio',$datos);

	campoFecha('fechaPrevista','Fecha prevista finalización',$datos);
	campoRadio('toma_datos','Toma de datos',$datos);
	campoFecha('fechaTomaDatos','Fecha límite',$datos);

	campoRadio('revision','Revisión de documentos',$datos);
	campoFecha('fechaRevision','Fecha límite',$datos);

	campoRadio('emision','Emisión de documentos',$datos);
	campoFecha('fechaEmision','Fecha límite',$datos);

	campoRadio('envio','Envío al cliente',$datos);
	campoFecha('fechaEnvio','Fecha límite',$datos);

	campoRadio('entrega','Entrega al cliente',$datos);
	campoFecha('fechaEntrega','Fecha límite',$datos);

	campoRadio('mantenimiento','Mantenimiento',$datos);
	campoFecha('fechaMantenimiento','Fecha límite',$datos);

	campoRadio('incidencia','Incidencia',$datos);
	echo "<div id='divIncidencia' class='hidden'>";
		areaTexto('incidenciaTexto','Descripción',$datos,'areaInforme');
	echo "</div>";
	cierraVentanaGestionPerfil('ADMIN','index.php',true);
}

function filtroConsultorias(){
	abreCajaBusqueda();
	abreColumnaCampos();

	$columnas=array('cliente','clientes.razonSocial','trabajo','trabajos.codigoServicio','trabajos.gratuita','trabajos.toma_datos','trabajos.revision','trabajos.envio','trabajos.mantenimiento','trabajos.fechaTomaDatos','trabajos.fechaRevision','trabajos.fechaEnvio','trabajos.fechaMantenimiento','trabajos.fechaTomaDatos','trabajos.fechaRevision','trabajos.fechaEnvio','trabajos.fechaMantenimiento','trabajos.fechaPrevista','trabajos.fechaPrevista','trabajos.fechaEmision','trabajos.emision','trabajos.fechaEmision','trabajos.fechaEntrega','trabajos.entrega','trabajos.fechaEntrega','trabajos.incidencia');

	campoTexto(1,'Cliente');
	campoFecha(17,'Fecha prevista desde');
	campoSelectSiNoFiltro(5,'Toma de datos');
	campoFecha(9,'Fecha limite toma de datos desde');

	campoSelectSiNoFiltro(6,'Revisión de documentación');
	campoFecha(10,'Fecha limite revisión de documentación desde');

	campoSelectSiNoFiltro(19,'Emisión de documentación');
	campoFecha(19,'Fecha limite emisión de documentación desde');

	campoSelectSiNoFiltro(7,'Envío al cliente');
	campoFecha(11,'Fecha limite envío al cliente desde');

	campoSelectSiNoFiltro(22,'Entrega al cliente');
	campoFecha(21,'Fecha limite rntrega al cliente desde');

	campoSelectSiNoFiltro(8,'Mantenimiento');
	campoFecha(12,'Fecha limite mantenimiento desde');
	
	campoSelectSiNoFiltro(4,'Gratuita');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta(3,'SERVICIO','SELECT codigo, referencia AS texto FROM servicios ORDER BY referencia');
	campoFecha(18,'Fecha prevista hasta');
	echo "<div style='opacity:0;'>";
		campoSelect(100,'Toma de datos',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(13,'Fecha limite toma de datos hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Revisión de documentación',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(14,'Fecha limite revisión de documentación hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Emisión de documentación',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(20,'Fecha limite emisión de documentación hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Envío al cliente',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(15,'Fecha limite envío al cliente hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Entrega al cliente',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(23,'Fecha limite entrega al cliente hasta');

	echo "<div style='opacity:0;'>";
		campoSelect(100,'Mantenimiento',array('Si','No'),array('SI','NO'),'','selectpicker span1 show-tick');
	echo "</div>";
	campoFecha(16,'Fecha limite mantenimiento hasta');
	
	campoSelectSiNoFiltro(24,'Con incidencia');
	

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function campoFicheroFormulario($numero,$indice,$texto,$tipo=0,$valor=false,$ruta=false,$nombreDescarga=''){
    campoFichero('pregunta'.$indice.$numero,$texto,$tipo,$valor,$ruta,$nombreDescarga);
}


//Fin parte de ventas de servicios
?>
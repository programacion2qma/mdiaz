<?php
  $seccionActiva=11;
  include_once("../cabecera.php");
  if(isset($_GET['gratis']) || isset($_GET['codigoTrabajo'])){
  	gestionConsultoria();
  } else {
  	gestionTrabajos();
  }
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('#fecha').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');modificarFechas($(this));});
	$('#fechaRevision').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');modificarFechaPrevista($(this));});

	recorrerRadios();

	$("input[type='radio']").change(function(){
		compruebaRadio($(this));
	});

	mostrarIncidencia($("input[name='incidencia']:checked"));
	$("input[name='incidencia']").change(function(){
		mostrarIncidencia($(this));
	});

	if ( $("#codigo").length == 0 ) {
		modificarFechas($('#fecha'));
	}

});

function modificarFechas(elem){
	obtenerFecha('#fechaPrevista',elem.val(),'P31D');
	obtenerFecha('#fechaTomaDatos',elem.val(),'P14D');
	obtenerFecha('#fechaRevision',elem.val(),'P21D');
	obtenerFecha('#fechaEmision',elem.val(),'P21D');
	obtenerFecha('#fechaEnvio',elem.val(),'P28D');
	obtenerFecha('#fechaEntrega',elem.val(),'P28D');
	obtenerFecha('#fechaMantenimiento',elem.val(),'P11M');
}

function modificarFechaPrevista(elem){
	var gratuita = $('#gratuita').val();
	if(gratuita == 'SI'){
		obtenerFecha('#fechaPrevista',elem.val(),'P10D');
	} else {
		obtenerFecha('#fechaPrevista',elem.val(),'P30D');
	}
}

function mostrarIncidencia(elem){
	if(elem.val() == 'SI'){
		$('#divIncidencia').removeClass('hidden');
	} else {
		$('#divIncidencia').addClass('hidden');
	}
}

function obtenerFecha(campo,fecha,suma){
	var consulta=$.post('obtenerFecha.php?',{'fecha':fecha,'suma':suma});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al obtener la fecha límite.\nInténtelo de nuevo en unos segundos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			$(campo).val(respuesta);
		}
	});
}

function recorrerRadios(){
	$("input[type='radio']:checked").each(function (){
		compruebaRadio($(this));
	});
}

function compruebaRadio(elem){
	var val = $(elem).val();
  	var id = '#div'+obtieneID($(elem).attr('name'));
  	if ($(id).length) {
  		mostrarCampos(val,id);
  	}
}

function obtieneID(id){
	return id.replace('pregunta','');
}

function mostrarCampos(val,div){
	if(val == 'SI'){
		$(div).removeClass('hidden');
		$(div+'_1').removeClass('hidden');
	} else {
		$(div).addClass('hidden');
		$(div+'_1').addClass('hidden');
	}
}


</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
  $seccionActiva=11;
  include_once('../cabecera.php');
  
  operacionesTrabajos();
  $estadisticas=creaEstadisticasTrabajos();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span12">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas sobre consultorías registradas:</h6>
                  
                  <div id="big_stats" class="cf">
                    <?php
                      $indice=array();
                      $indice[0]='TOTAL';
                      $i=1;
                      $servicios=consultaBD("SELECT * FROM servicios_familias ORDER BY referencia",true);
                      while($servicio=mysql_fetch_assoc($servicios)){
                        $referencia = str_replace(' ', '_', $servicio['nombre']);
                        $indice[$i] = $referencia;
                        $i++;
                      }
                      for($i=0;$i<count($indice);$i++){ 
                      if($i>0){
                        echo "<button class='btn bigstats tituloSeguimientoConsultoria' estado='oculto' tabla='#".$indice[$i]."'>".str_replace('_', ' ', $indice[$i])."</button>";
                      } else {
                        echo "<button class='btn bigstats tituloSeguimientoConsultoria' estado='oculto' tabla='#".$indice[$i]."'>TOTALES</button>";
                      }
                    ?>
                    <div id="<?php echo $indice[$i] ?>" class='tablaStats hide'>

                    <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-commenting"></i> <span class="value"><?php echo $estadisticas['total_'.$indice[$i]]?></span> <br>Registrada/s</div>
                     <div class="stat"> <i class="icon-money"></i> <span class="value"><?php echo $estadisticas['ventas_'.$indice[$i]]?></span> <br>De ventas</div>
                     <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $estadisticas['gratuitas_'.$indice[$i]]?></span> <br>De colaboradores</div>
                    </div>

                    <h6 class="bigstats"></h6>
                    
                    <div id="big_stats" class="cf">
                      <div class="stat"> <i class="icon-check"></i> <span class="value"><?php echo $estadisticas['finalizadas_'.$indice[$i]]?></span> <br>Finalizada/s</div>
                      <div class="stat"> <i class="icon-money"></i> <span class="value"><?php echo $estadisticas['finalizadasVentas_'.$indice[$i]]?></span> <br>De ventas</div>
                      <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $estadisticas['finalizadasGratuitas_'.$indice[$i]]?></span> <br>De colaboradores</div>
                    </div>

                    <h6 class="bigstats"></h6>
                    
                    <div id="big_stats" class="cf">
                      <div class="stat"> <i class="icon-list"></i> <span class="value"><?php echo $estadisticas['tomaDatos_'.$indice[$i]]?></span> <br>Total CheckList recibidos</div>
                      <div class="stat"> <i class="icon-money"></i> <span class="value"><?php echo $estadisticas['tomaDatosVentas_'.$indice[$i]]?></span> <br>De ventas</div>
                      <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $estadisticas['tomaDatosGratuitas_'.$indice[$i]]?></span> <br>De colaboradores</div>
                    </div>

                    <h6 class="bigstats"></h6>
                    
                    <div id="big_stats" class="cf">
                      <div class="stat"> <i class="icon-close"></i> <span class="value"><?php echo $estadisticas['vencidas_'.$indice[$i]]?></span> <br>Total vencidas</div>
                      <div class="stat"> <i class="icon-money"></i> <span class="value"><?php echo $estadisticas['vencidasVentas_'.$indice[$i]]?></span> <br>De ventas</div>
                      <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $estadisticas['vencidasGratuitas_'.$indice[$i]]?></span> <br>De colaboradores</div>
                    </div>

                    <h6 class="bigstats"></h6>
                    
                    <div id="big_stats" class="cf">
                      <div class="stat"> <i class="icon-clock-o"></i> <span class="value"><?php echo $estadisticas['vencidasSiete_'.$indice[$i]]?></span> <br>Total vencen<br/>en 7 días</div>
                      <div class="stat"> <i class="icon-money"></i> <span class="value"><?php echo $estadisticas['vencidasSieteVentas_'.$indice[$i]]?></span> <br>De ventas</div>
                      <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $estadisticas['vencidasSieteGratuitas_'.$indice[$i]]?></span> <br>De colaboradores</div>
                    </div>

                    <h6 class="bigstats"></h6>

                    <div id="big_stats" class="cf">
                      <div class="stat"> <i class="icon-exclamation-circle"></i> <span class="value"><?php echo $estadisticas['incidencias_'.$indice[$i]]?></span> <br>Total incidencias</div>
                      <div class="stat"> <i class="icon-money"></i> <span class="value"><?php echo $estadisticas['incidenciasVentas_'.$indice[$i]]?></span> <br>De ventas</div>
                      <div class="stat"> <i class="icon-user"></i> <span class="value"><?php echo $estadisticas['incidenciasGratuitas_'.$indice[$i]]?></span> <br>De colaboradores</div>
                    </div>

                    </div>
                    <?php } ?>

                     
                    <!-- <h6 class="bigstats"></h6>
                    <div id="big_stats" class="cf">
                     <div class="stat"> 
                          <canvas id="graficoCircularFinalizadas" class="chart-holder" height="150" width="438"></canvas>
                          <div class="leyenda" id="leyendaFinalizadas"></div>
                     </div>
                     <div class="stat"> 
                          <canvas id="graficoCircularServicios" class="chart-holder" height="150" width="438"></canvas>
                          <div class="leyenda" id="leyendaServicios"></div>
                     </div>
                     <div class="stat"> 
                          <canvas id="graficoCircularTipoVenta" class="chart-holder" height="150" width="438"></canvas>
                          <div class="leyenda" id="leyendaTipoVenta"></div>
                     </div>
                    </div>-->
                     
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
                
              </div>
            </div>
          </div>
         
        </div>

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Consultorías registradas</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroConsultorias();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaConsultorias">
                <thead>
                  <tr>
                    <th> Cliente </th>
                    <th class='centro'>Servicios</th>
                  </tr>
                </thead>
                <tbody>
                 
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/chart.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaConsultorias','../listadoAjax.php?include=seguimiento-consultoria&funcion=listadoTrabajos();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaConsultorias');

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    $('.tituloSeguimientoConsultoria').click(function(){
        var id=$(this).attr('tabla');//Atributo personalizado
        var estado=$(this).attr('estado');//Atributo personalizado
        if(estado=='oculto'){
          $('.tablaStats').slideUp();
          $('.tablaStats').attr('estado','oculto');
          $(id).slideDown();
          $(this).attr('estado','mostrado');
        }
        else{
          $(id).slideUp();
          $(this).attr('estado','oculto');
        }
    });
  });


</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
<?php
  $seccionActiva=23;
  include_once("../cabecera.php");
  gestionPagoComision();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	$('input[name=tipo]').change(function(){
		oyenteTipoAgente($(this).val(),$(this).parent().text().trim());
	});
});

function oyenteTipoAgente(tipo,texto){
	var consulta=$.post('../listadoAjax.php?include=pagos-comisiones&funcion=obtieneOptionTipoAgente();',{'tipo':tipo});

	consulta.done(function(respuesta){
		$('#codigoAgente').parent().prev().text(texto+':');
		$('#codigoAgente').html(respuesta);
		$('#codigoAgente').selectpicker('refresh');
	});
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
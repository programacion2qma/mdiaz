<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de pago de comisiones

function operacionesPagosComisiones(){
	$res=true;

	if(isset($_POST['codigo'])){
		formateaPrecioBDD('importePago');
		$res=actualizaDatos('pagos_comisiones');
	}
	elseif(isset($_POST['fechaPago'])){
		formateaPrecioBDD('importePago');
		$res=insertaDatos('pagos_comisiones');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('pagos_comisiones');
	}

	mensajeResultado('fechaPago',$res,'Pago de comisión');
    mensajeResultado('elimina',$res,'Pago de comisión', true);
}


function listadoPagosComisiones(){
	global $_CONFIG;

	$columnas=array('fechaPago','nombre','importePago','concepto','emisor','banco','fechaPago','nombre','codigoUsuario');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	

	$query="SELECT pagos_comisiones.codigo, fechaPago, CONCAT(comerciales.nombre,' (Comercial)') AS nombre, importePago, concepto,
	 pagos_comisiones.activo, codigoUsuario

	FROM pagos_comisiones LEFT JOIN comerciales ON pagos_comisiones.codigoAgente=comerciales.codigo

	WHERE tipo='COMERCIAL'
	$having

	UNION ALL

	SELECT pagos_comisiones.codigo, fechaPago, CONCAT(telemarketing.nombre,' (Telemarketing)') AS nombre, importePago, concepto,
	pagos_comisiones.activo, NULL AS codigoUsuario

	FROM pagos_comisiones LEFT JOIN telemarketing ON pagos_comisiones.codigoAgente=telemarketing.codigo

	WHERE tipo='TELEMARKETING'
	$having

	UNION ALL

	SELECT pagos_comisiones.codigo, fechaPago, CONCAT(colaboradores.nombre,' (Colaborador)') AS nombre, importePago, concepto,
	pagos_comisiones.activo, NULL AS codigoUsuario

	FROM pagos_comisiones LEFT JOIN colaboradores ON pagos_comisiones.codigoAgente=colaboradores.codigo

	WHERE tipo='COLABORADOR'

	$having";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			formateaFechaWeb($datos['fechaPago']),
			$datos['nombre'],
			"<div class='pagination-right'>".formateaNumeroWeb($datos['importePago'])." €</div>",
			$datos['concepto'],
			creaBotonDetalles("pagos-comisiones/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionPagoComision(){
	operacionesPagosComisiones();

	abreVentanaGestion('Gestión de pago de comisiones','?');
	$datos=compruebaDatos('pagos_comisiones');

	campoFecha('fechaPago','Fecha pago',$datos);
	campoRadio('tipo','Asignar a',$datos,'COMERCIAL',array('Comercial','Telemarketing','Colaborador'),array('COMERCIAL','TELEMARKETING','COLABORADOR'));
	campoSelectAgenteAsignado($datos);
	campoTextoSimbolo('importePago','Importe','€',formateaNumeroWeb($datos['importePago']));
	campoTexto('concepto','Concepto',$datos,'span6');
	

	campoOculto('SI','activo');

	cierraVentanaGestion('index.php');
}

function campoSelectAgenteAsignado($datos){
	if(!$datos ||  ($datos && $datos['tipo']=='COMERCIAL')){//Porque por defecto el tipo es "COMERCIAL"
		campoSelectConsulta('codigoAgente','Comercial',"SELECT codigo, nombre AS texto FROM comerciales ORDER BY nombre",$datos);
	}
	elseif($datos && $datos['tipo']=='TELEMARKETING'){
		campoSelectConsulta('codigoAgente','Telemarketing',"SELECT codigo, nombre AS texto FROM telemarketing ORDER BY nombre",$datos);
	}
	elseif($datos && $datos['tipo']=='COLABORADOR'){
		campoSelectConsulta('codigoAgente','Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores ORDER BY nombre",$datos);
	}
}

function filtroPagosComisiones(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoFecha(0,'Fecha pago desde');
	campoFecha(6,'Hasta');
	campoSelect(1,'Tipo destinatario',array('','Comercial','Telemarketing','Colaborador'),array('','Comercial','Telemarketing','Colaborador'));
	campoTexto(7,'Nombre destinatario');	
	campoSelectConsulta(8,'Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE activo='SI' AND (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') ORDER BY nombre");

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoSimbolo(2,'Importe','€');
	campoTexto(3,'Concepto');
	campoSelectConsulta(4,'Emisor',"SELECT razonSocial AS codigo, razonSocial AS texto FROM emisores ORDER BY razonSocial");
	campoSelectConsulta(5,'Banco',"SELECT nombre AS codigo, nombre AS texto FROM cuentas_propias ORDER BY nombre");

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function obtieneOptionTipoAgente(){
	$tipo=$_POST['tipo'];

	$res='';

	if($tipo=='COMERCIAL'){//Porque por defecto el tipo es "COMERCIAL"
		$res=generaOptionTipoAgente("SELECT codigo, nombre AS texto FROM comerciales ORDER BY nombre");
	}
	elseif($tipo=='TELEMARKETING'){
		$res=generaOptionTipoAgente("SELECT codigo, nombre AS texto FROM telemarketing ORDER BY nombre");
	}
	elseif($tipo=='COLABORADOR'){
		$res=generaOptionTipoAgente("SELECT codigo, nombre AS texto FROM colaboradores ORDER BY nombre");
	}


	echo $res;
}

function generaOptionTipoAgente($query){
	$res="<option value='NULL'></option>";

	$consulta=consultaBD($query,true);
	while($datos=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
	}

	return $res;
}

//Fin parte de pagos de comisiones
-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Mar 07, 2025 at 05:28 PM
-- Server version: 5.7.41-log
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mdiaz`
--

-- --------------------------------------------------------

--
-- Table structure for table `paises`
--

CREATE TABLE `paises` (
  `codigo` int(11) NOT NULL,
  `codigoInterno` int(10) NOT NULL,
  `nombre` varchar(255) COLLATE latin1_spanish_ci NOT NULL,
  `medidas` varchar(255) COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Dumping data for table `paises`
--

INSERT INTO `paises` (`codigo`, `codigoInterno`, `nombre`, `medidas`) VALUES
(1, 1, 'Abjasia', 'REFORZADAS'),
(2, 2, 'Acrotiri y Dhekelia', 'REFORZADAS'),
(3, 3, 'AfganistÃ¡n---PARAISO FISCAL---', 'EXAMEN'),
(4, 4, 'Albania', 'REFORZADAS'),
(5, 5, 'Alemania', 'NORMALES'),
(6, 6, 'Andorra', 'REFORZADAS'),
(7, 7, 'Angola', 'REFORZADAS'),
(8, 8, 'Anguila---No cooperador con UE---', 'EXAMEN'),
(9, 9, 'Antigua y Barbuda', 'REFORZADAS'),
(10, 10, 'Arabia Saudita', 'REFORZADAS'),
(11, 11, 'Argelia', 'REFORZADAS'),
(12, 12, 'Argentina', 'REFORZADAS'),
(13, 13, 'Armenia', 'REFORZADAS'),
(14, 14, 'Aruba', 'NORMALES'),
(15, 15, 'Australia', 'REFORZADAS'),
(16, 16, 'Austria', 'NORMALES'),
(17, 17, 'AzerbaiyÃ¡n', 'REFORZADAS'),
(18, 18, 'Bahamas', 'REFORZADAS'),
(19, 19, 'BarÃ©in---PARAISO FISCAL--', 'EXAMEN'),
(20, 20, 'BangladÃ©s', 'REFORZADAS'),
(21, 21, 'Barbados', 'REFORZADAS'),
(22, 22, 'BÃ©lgica', 'NORMALES'),
(23, 23, 'Belice', 'REFORZADAS'),
(24, 24, 'BenÃ­n', 'REFORZADAS'),
(25, 25, 'Bermudas---PARAISO FISCAL---', 'EXAMEN'),
(26, 26, 'Bielorrusia', 'REFORZADAS'),
(27, 27, 'Birmania', 'REFORZADAS'),
(28, 28, 'Bolivia', 'REFORZADAS'),
(29, 29, 'Bonaire', 'NORMALES'),
(30, 30, 'Bosnia y Herzegovina---PARAISO FISCAL---', 'EXAMEN'),
(31, 31, 'Botsuana', 'REFORZADAS'),
(32, 32, 'Brasil', 'NORMALES'),
(33, 33, 'BrunÃ©i---PARAISO FISCAL---', 'EXAMEN'),
(34, 34, 'Bulgaria---PARAISO FISCAL---', 'EXAMEN'),
(35, 35, 'Burkina Faso', 'REFORZADAS'),
(36, 36, 'Burundi', 'REFORZADAS'),
(37, 37, 'ButÃ¡n', 'REFORZADAS'),
(38, 38, 'Cabo Verde', 'REFORZADAS'),
(39, 39, 'CaimÃ¡n, Islas---PARAISO FISCAL---', 'EXAMEN'),
(40, 40, 'Camboya', 'REFORZADAS'),
(41, 41, 'CamerÃºn---PARAISO FISCAL---', 'EXAMEN'),
(42, 42, 'CanadÃ¡', 'NORMALES'),
(43, 43, 'Catar', 'REFORZADAS'),
(44, 44, 'Centroafricana, RepÃºblica', 'REFORZADAS'),
(45, 45, 'Chad', 'REFORZADAS'),
(46, 46, 'Checa, RepÃºblica', 'NORMALES'),
(47, 47, 'Chile', 'REFORZADAS'),
(48, 48, 'China', 'REFORZADAS'),
(49, 49, 'Chipre', 'NORMALES'),
(50, 50, 'Chipre del Norte', 'REFORZADAS'),
(51, 51, 'Cocos, Islas', 'REFORZADAS'),
(52, 52, 'Colombia', 'REFORZADAS'),
(53, 53, 'Comoras', 'REFORZADAS'),
(54, 54, 'Congo, RepÃºblica del', 'REFORZADAS'),
(55, 55, 'Congo, RepÃºblica DemocrÃ¡tica del', 'REFORZADAS'),
(56, 56, 'Cook, Islas---PARAISO FISCAL---', 'EXAMEN'),
(57, 57, 'Corea del Norte---PARAISO FISCAL---', 'EXAMEN'),
(58, 58, 'Corea del Sur', 'NORMALES'),
(59, 59, 'Costa de Marfil', 'REFORZADAS'),
(60, 60, 'Costa Rica', 'REFORZADAS'),
(61, 61, 'Croacia---PARAISO FISCAL---', 'EXAMEN'),
(62, 62, 'Cuba', 'REFORZADAS'),
(63, 63, 'Curazao', 'REFORZADAS'),
(64, 64, 'Dinamarca', 'NORMALES'),
(65, 65, 'Dominica---PARAISO FISCAL---', 'EXAMEN'),
(66, 66, 'Dominicana, RepÃºblica', 'REFORZADAS'),
(67, 67, 'Ecuador', 'REFORZADAS'),
(68, 68, 'Egipto', 'REFORZADAS'),
(69, 69, 'El Salvador', 'REFORZADAS'),
(70, 70, 'Emiratos Ãrabes Unidos', 'REFORZADAS'),
(71, 71, 'Eritrea', 'REFORZADAS'),
(72, 72, 'Eslovaquia', 'NORMALES'),
(73, 73, 'Eslovenia', 'NORMALES'),
(74, 74, 'EspaÃ±a', 'NORMALES'),
(75, 75, 'Estados Unidos', 'NORMALES'),
(76, 76, 'Estonia', 'NORMALES'),
(77, 77, 'EtiopÃ­a', 'REFORZADAS'),
(78, 210, 'Eswatini', 'REFORZADAS'),
(79, 78, 'Feroe, Islas', 'REFORZADAS'),
(80, 79, 'Filipinas', 'REFORZADAS'),
(81, 80, 'Finlandia', 'NORMALES'),
(82, 81, 'Fiyi---No cooperador con UE---', 'EXAMEN'),
(83, 82, 'Francia', 'NORMALES'),
(84, 83, 'GabÃ³n', 'REFORZADAS'),
(85, 84, 'Gambia', 'NORMALES'),
(86, 85, 'Georgia', 'NORMALES'),
(87, 86, 'Ghana', 'NORMALES'),
(88, 87, 'Gibraltar', 'REFORZADAS'),
(89, 88, 'Granada---PARAISO FISCAL---', 'EXAMEN'),
(90, 89, 'Grecia', 'NORMALES'),
(91, 90, 'Groenlandia', 'REFORZADAS'),
(92, 91, 'Guam---No cooperador con UE---', 'EXAMEN'),
(93, 92, 'Guatemala', 'REFORZADAS'),
(94, 93, 'Guernsey---PARAISO FISCAL---', 'EXAMEN'),
(95, 94, 'Guinea', 'REFORZADAS'),
(96, 95, 'Guinea-BisÃ¡u', 'REFORZADAS'),
(97, 96, 'Guinea Ecuatorial', 'REFORZADAS'),
(98, 97, 'Guyana---PARAISO FISCAL---', 'EXAMEN'),
(99, 98, 'HaitÃ­', 'REFORZADAS'),
(100, 99, 'Honduras', 'REFORZADAS'),
(101, 100, 'Hong Kong---PARAISO FISCAL---', 'EXAMEN'),
(102, 101, 'HungrÃ­a', 'NORMALES'),
(103, 102, 'India', 'NORMALES'),
(104, 103, 'Indonesia', 'REFORZADAS'),
(105, 104, 'Irak---PARAISO FISCAL---', 'EXAMEN'),
(106, 105, 'IrÃ¡n---PARAISO FISCAL---', 'EXAMEN'),
(107, 106, 'Irlanda', 'NORMALES'),
(108, 107, 'Islandia', 'REFORZADAS'),
(109, 108, 'Israel', 'REFORZADAS'),
(110, 109, 'Italia', 'NORMALES'),
(111, 110, 'Jamaica', 'REFORZADAS'),
(112, 111, 'JapÃ³n', 'NORMALES'),
(113, 112, 'Jersey---PARAISO FISCAL---', 'EXAMEN'),
(114, 113, 'Jordania---PARAISO FISCAL---', 'EXAMEN'),
(115, 114, 'KazajistÃ¡n', 'REFORZADAS'),
(116, 115, 'Kenia---PARAISO FISCAL---', 'EXAMEN'),
(117, 116, 'KirguistÃ¡n', 'REFORZADAS'),
(118, 117, 'Kiribati', 'REFORZADAS'),
(119, 118, 'Kosovo', 'REFORZADAS'),
(120, 119, 'Kuwait', 'REFORZADAS'),
(121, 120, 'Laos---PARAISO FISCAL---', 'EXAMEN'),
(122, 121, 'Lesoto', 'REFORZADAS'),
(123, 122, 'Letonia', 'NORMALES'),
(124, 123, 'LÃ­bano---PARAISO FISCAL---', 'EXAMEN'),
(125, 124, 'Liberia---PARAISO FISCAL---', 'EXAMEN'),
(126, 125, 'Libia', 'REFORZADAS'),
(127, 126, 'Liechtenstein---PARAISO FISCAL---', 'EXAMEN'),
(128, 127, 'Lituania', 'NORMALES'),
(129, 128, 'Luxemburgo', 'NORMALES'),
(130, 129, 'Macao---PARAISO FISCAL---', 'EXAMEN'),
(131, 130, 'Macedonia del Norte', 'REFORZADAS'),
(132, 131, 'Madagascar', 'REFORZADAS'),
(133, 132, 'Malasia', 'REFORZADAS'),
(134, 133, 'Malaui', 'REFORZADAS'),
(135, 134, 'Maldivas', 'REFORZADAS'),
(136, 135, 'MalÃ­', 'REFORZADAS'),
(137, 136, 'Malta', 'NORMALES'),
(138, 137, 'Malvinas, Islas---PARAISO FISCAL---', 'EXAMEN'),
(139, 138, 'Man, Isla de---PARAISO FISCAL---', 'EXAMEN'),
(140, 139, 'Marianas del Norte, Islas---PARAISO FISCAL---', 'EXAMEN'),
(141, 140, 'Marruecos', 'REFORZADAS'),
(142, 141, 'Marshall, Islas', 'REFORZADAS'),
(143, 142, 'Mauricio---PARAISO FISCAL---', 'EXAMEN'),
(144, 143, 'Mauritania', 'REFORZADAS'),
(145, 144, 'Mayotte', 'NORMALES'),
(146, 145, 'MÃ©xico', 'NORMALES'),
(147, 146, 'Micronesia', 'REFORZADAS'),
(148, 147, 'Moldavia', 'REFORZADAS'),
(149, 148, 'MÃ³naco---PARAISO FISCAL---', 'EXAMEN'),
(150, 149, 'Mongolia', 'REFORZADAS'),
(151, 150, 'Montenegro', 'REFORZADAS'),
(152, 151, 'Montserrat---PARAISO FISCAL---', 'EXAMEN'),
(153, 152, 'Mozambique', 'REFORZADAS'),
(154, 153, 'Nagorno Karabaj', 'REFORZADAS'),
(155, 154, 'Namibia---PARAISO FISCAL---', 'EXAMEN'),
(156, 155, 'Nauru---PARAISO FISCAL---', 'EXAMEN'),
(157, 156, 'Navidad, Isla de', 'REFORZADAS'),
(158, 157, 'Nepal', 'REFORZADAS'),
(159, 158, 'Nicaragua', 'REFORZADAS'),
(160, 159, 'NÃ­ger', 'REFORZADAS'),
(161, 160, 'Nigeria---PARAISO FISCAL---', 'EXAMEN'),
(162, 161, 'Niue', 'REFORZADAS'),
(163, 162, 'Norfolk, Isla', 'REFORZADAS'),
(164, 163, 'Noruega', 'REFORZADAS'),
(165, 164, 'Nueva Caledonia', 'NORMALES'),
(166, 165, 'Nueva Rusia', 'REFORZADAS'),
(167, 166, 'Nueva Zelanda', 'REFORZADAS'),
(168, 167, 'OmÃ¡n---PARAISO FISCAL---', 'EXAMEN'),
(169, 168, 'Osetia del Sur', 'REFORZADAS'),
(170, 169, 'PaÃ­ses Bajos', 'NORMALES'),
(171, 170, 'PakistÃ¡n', 'REFORZADAS'),
(172, 171, 'Palaos---No cooperador con UE---', 'EXAMEN'),
(173, 172, 'Palestina', 'REFORZADAS'),
(174, 173, 'PanamÃ¡---No cooperador con UE---', 'EXAMEN'),
(175, 174, 'PapÃºa Nueva Guinea', 'REFORZADAS'),
(176, 175, 'Paraguay', 'REFORZADAS'),
(177, 176, 'PerÃº', 'REFORZADAS'),
(178, 177, 'Pitcairn, Islas', 'REFORZADAS'),
(179, 178, 'Polinesia Francesa', 'NORMALES'),
(180, 179, 'Polonia', 'NORMALES'),
(181, 180, 'Portugal', 'NORMALES'),
(182, 181, 'Puerto Rico', 'REFORZADAS'),
(183, 182, 'Reino Unido', 'NORMALES'),
(184, 183, 'Ruanda', 'REFORZADAS'),
(185, 184, 'Rumania', 'NORMALES'),
(186, 185, 'Rusia---No cooperador con UE---', 'EXAMEN'),
(187, 186, 'Saba', 'NORMALES'),
(188, 187, 'Sahara Occidental', 'REFORZADAS'),
(189, 188, 'SalomÃ³n, Islas---PARAISO FISCAL---', 'EXAMEN'),
(190, 189, 'Samoa---No cooperador con UE---', 'EXAMEN'),
(191, 190, 'Samoa Americana---No cooperador con UE---', 'EXAMEN'),
(192, 191, 'San BartolomÃ©', 'REFORZADAS'),
(193, 192, 'San CristÃ³bal y Nieves', 'REFORZADAS'),
(194, 193, 'San Marino', 'REFORZADAS'),
(195, 194, 'San MartÃ­n', 'REFORZADAS'),
(196, 195, 'San Pedro y MiquelÃ³n', 'NORMALES'),
(197, 196, 'San Vicente y las Granadinas---PARAISO FISCAL---', 'EXAMEN'),
(198, 197, 'Santa Elena, AscensiÃ³n y TristÃ¡n de AcuÃ±a', 'REFORZADAS'),
(199, 198, 'Santa LucÃ­a---PARAISO FISCAL---', 'EXAMEN'),
(200, 199, 'Santo TomÃ© y PrÃ­ncipe', 'REFORZADAS'),
(201, 200, 'Senegal', 'REFORZADAS'),
(202, 201, 'Serbia', 'REFORZADAS'),
(203, 202, 'Seychelles', 'REFORZADAS'),
(204, 203, 'Sierra Leona', 'REFORZADAS'),
(205, 204, 'Singapur', 'NORMALES'),
(206, 205, 'Sint Eustatius', 'NORMALES'),
(207, 206, 'Sint Maarten', 'NORMALES'),
(208, 207, 'Siria---PARAISO FISCAL---', 'EXAMEN'),
(209, 208, 'Somalilandia', 'REFORZADAS'),
(210, 209, 'Sri Lanka', 'REFORZADAS'),
(211, 211, 'SudÃ¡frica---PARAISO FISCAL---', 'EXAMEN'),
(212, 212, 'SudÃ¡n', 'REFORZADAS'),
(213, 213, 'SudÃ¡n del Sur', 'REFORZADAS'),
(214, 214, 'Suecia', 'NORMALES'),
(215, 215, 'Suiza', 'NORMALES'),
(216, 216, 'Surinam', 'REFORZADAS'),
(217, 217, 'Svalbard', 'REFORZADAS'),
(218, 218, 'Tailandia', 'REFORZADAS'),
(219, 219, 'TaiwÃ¡n', 'REFORZADAS'),
(220, 220, 'Tanzania', 'REFORZADAS'),
(221, 221, 'TayikistÃ¡n', 'REFORZADAS'),
(222, 222, 'Timor Oriental', 'REFORZADAS'),
(223, 223, 'Togo', 'REFORZADAS'),
(224, 224, 'Tokelau', 'REFORZADAS'),
(225, 225, 'Tonga', 'REFORZADAS'),
(226, 226, 'Transnistria', 'REFORZADAS'),
(227, 227, 'Trinidad y Tobago---No cooperador con UE---', 'EXAMEN'),
(228, 228, 'TÃºnez', 'REFORZADAS'),
(229, 229, 'Turcas y Caicos, Islas---PARAISO FISCAL---', 'EXAMEN'),
(230, 230, 'TurkmenistÃ¡n', 'REFORZADAS'),
(231, 231, 'TurquÃ­a', 'REFORZADAS'),
(232, 232, 'Tuvalu', 'REFORZADAS'),
(233, 233, 'Ucrania', 'REFORZADAS'),
(234, 234, 'Uganda', 'REFORZADAS'),
(235, 235, 'Uruguay', 'REFORZADAS'),
(236, 236, 'UzbekistÃ¡n', 'REFORZADAS'),
(237, 237, 'Vanuatu---No cooperador con UE---', 'EXAMEN'),
(238, 238, 'Vaticano, Ciudad del', 'REFORZADAS'),
(239, 239, 'Venezuela---PARAISO FISCAL---', 'EXAMEN'),
(240, 240, 'Vietnam', 'REFORZADAS'),
(241, 241, 'VÃ­rgenes BritÃ¡nicas, Islas---PARAISO FISCAL---', 'EXAMEN'),
(242, 242, 'VÃ­rgenes de los Estados Unidos, Islas---No cooperador con UE---', 'EXAMEN'),
(243, 243, 'Wallis y Futuna', 'NORMALES'),
(244, 244, 'Yemen---PARAISO FISCAL---', 'EXAMEN'),
(245, 245, 'Yibuti', 'REFORZADAS'),
(246, 246, 'Zambia', 'REFORZADAS'),
(247, 247, 'Zimbabue', 'REFORZADAS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`codigo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `paises`
--
ALTER TABLE `paises`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=248;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas


//Parte de generación de informes


function operacionesInformes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('informes');
	}
	elseif(isset($_POST['codigoEvaluacion'])){
		$res=insertaDatos('informes');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('informes');
	}

	mensajeResultado('codigoEvaluacion',$res,'Informe');
    mensajeResultado('elimina',$res,'Informe', true);
}

function estadisticasInformes($codigoCliente=false){
	if($codigoCliente==false){
		$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM informes WHERE codigoEvaluacion IS NOT NULL GROUP BY codigoEvaluacion;",true);
	}else{
		$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM informes WHERE codigoCliente='$codigoCliente' GROUP BY codigoEvaluacion;",true);		
	}
	return mysql_num_rows($consulta);
}



function gestionInforme(){
	operacionesInformes();
	
	abreVentanaGestion('Gestión de Informes ','?','','icon-edit','margenAb');

	$datos=compruebaDatos('informes');
	/*if($datos){
		campoOculto(recogerPermiso(8,'checkModificar'),'permisoModificar');
	}*/
	campoSelectConsulta('codigoEvaluacion','Evaluación',"SELECT codigo, DATE_FORMAT(fechaEvaluacion,'%d/%m/%Y') AS texto FROM evaluacion_general ORDER BY fechaEvaluacion DESC",$datos,'selectpicker span6 show-tick');

	campoFecha('fecha','Fecha de creación',$datos);
	areaTexto('historico','Histórico',$datos);

	
	cierraVentanaGestion('index.php');
}

function imprimeInformes($codigoCliente=false){
	global $_CONFIG;

	$consultaClientes=consultaBD("SELECT * FROM clientes WHERE codigo='$codigoCliente';",true);
	if($codigoCliente==false){
		$consulta=consultaBD("SELECT MAX(informes.codigo) AS codigo, informes.fecha AS fechaInforme, evaluacion_general.fechaEvaluacion AS fechaEvaluacion, razonSocial FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo INNER JOIN clientes ON evaluacion_general.codigoCliente=clientes.codigo GROUP BY codigoEvaluacion;",true);
		while($datos=mysql_fetch_assoc($consulta)){
			echo "
				<tr>
					<td>".$datos['razonSocial']."</td>
					<td>".formateaFechaWeb($datos['fechaInforme'])."</td>
					<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
					<td class='centro'>
						<div class='btn-group centro'>
							<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
						  	<ul class='dropdown-menu' role='menu'>
							    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
							    <li class='divider'></li>
							    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/generaPDF.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descarga de informe</i></a></li>
							</ul>
						</div>
					</td>
					<td>
						<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
		        	</td>
				</tr>";
		}	
	}else{
		while($datosCliente=mysql_fetch_assoc($consultaClientes)){
			$consultaAux=consultaBD("SELECT MAX(informes.codigo) AS codigo, informes.fecha AS fechaInforme, evaluacion_general.fechaEvaluacion AS fechaEvaluacion FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo WHERE informes.codigoCliente='".$datosCliente['codigo']."' GROUP BY codigoEvaluacion;",true);
			while($datos=mysql_fetch_assoc($consultaAux)){
				echo "
				<tr>
					<td>".formateaFechaWeb($datos['fechaInforme'])."</td>
					<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
					<td class='centro'>
						<div class='btn-group centro'>
							<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
						  	<ul class='dropdown-menu' role='menu'>
							    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
							    <li class='divider'></li>
							    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/generaPDF.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descarga de informe</i></a></li>
							</ul>
						</div>
					</td>
					<td>
						<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
		        	</td>
				</tr>";
			}			

		}		
		//$consulta=consultaBD("SELECT MAX(informes.codigo) AS codigo, informes.fecha AS fechaInforme, evaluacion_general.fechaEvaluacion AS fechaEvaluacion FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo WHERE informes.codigoCliente='".$datosCliente['codigo']."' GROUP BY codigoEvaluacion;",true);		
	}
	

}

/*function imprimeInformes($codigoCliente=false){
	global $_CONFIG;

	$consultaClientes=consultaBD("SELECT * FROM clientes WHERE empleado='$codigoCliente';",true);
	if($codigoCliente==false){
		$consulta=consultaBD("SELECT MAX(informes.codigo) AS codigo, informes.fecha AS fechaInforme, evaluacion_general.fechaEvaluacion AS fechaEvaluacion FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo GROUP BY codigoEvaluacion;",true);
		
		while($datos=mysql_fetch_assoc($consulta)){
			echo "
				<tr>
					<td>".$datosCliente['razon_s']."</td>
					<td>".formateaFechaWeb($datos['fechaInforme'])."</td>
					<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
					<td class='centro'>
						<div class='btn-group centro'>
							<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
						  	<ul class='dropdown-menu' role='menu'>
							    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
							    <li class='divider'></li>
							    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/generaPDF.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descarga de informe</i></a></li>
							</ul>
						</div>
					</td>
					<td>
						<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
		        	</td>
				</tr>";
		}	
	}else{
		while($datosCliente=mysql_fetch_assoc($consultaClientes)){
			$consultaAux=consultaBD("SELECT MAX(informes.codigo) AS codigo, informes.fecha AS fechaInforme, evaluacion_general.fechaEvaluacion AS fechaEvaluacion FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo WHERE informes.codigoCliente='".$datosCliente['codigo']."' GROUP BY codigoEvaluacion;",true);

			while($datos=mysql_fetch_assoc($consultaAux)){
				echo "
				<tr>
					<td>".$datosCliente['razon_s']."</td>
					<td>".formateaFechaWeb($datos['fechaInforme'])."</td>
					<td>".formateaFechaWeb($datos['fechaEvaluacion'])."</td>
					<td class='centro'>
						<div class='btn-group centro'>
							<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
						  	<ul class='dropdown-menu' role='menu'>
							    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
							    <li class='divider'></li>
							    <li><a href='".$_CONFIG['raiz']."generacion-de-informes/generaPDF.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descarga de informe</i></a></li>
							</ul>
						</div>
					</td>
					<td>
						<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
		        	</td>
				</tr>";
			}			

		}		
		//$consulta=consultaBD("SELECT MAX(informes.codigo) AS codigo, informes.fecha AS fechaInforme, evaluacion_general.fechaEvaluacion AS fechaEvaluacion FROM informes INNER JOIN evaluacion_general ON informes.codigoEvaluacion=evaluacion_general.codigo WHERE informes.codigoCliente='".$datosCliente['codigo']."' GROUP BY codigoEvaluacion;",true);		
	}
	

}*/



function generaDocumentosInformes($PHPWord,$codigoInforme){
	conexionBD();
	/*$datos=consultaBD("SELECT gestion_evaluacion_general.codigo AS codigoGestion, codigoEvaluacion, textoConclusiones, gestion_evaluacion_general.graficoBarra AS graficoBarraGestion, graficoCircular, comentariosDocumentacion1, comentariosDocumentacion2, comentariosDocumentacion3, comentariosDocumentacion4, comentariosDocumentacion5, comentariosDocumentacion6 FROM gestion_evaluacion_general INNER JOIN informes ON gestion_evaluacion_general.codigo=informes.codigoGestion WHERE informes.codigo='$codigoInforme';",false,true);
	$recomendaciones=obtieneCalculoRecomendacionesGestion($datos['codigoGestion']);
	$graficosDelitos=obtieneGraficosDelitos($datos['codigoEvaluacion']);
	$tablasRecomendaciones=obtieneTablasRecomendaciones($datos['codigoGestion'],$graficosDelitos);
	$datosFE=consultaBD("SELECT informes.fecha, razonSocial, nombreComercial, graficoArania, formularios_evaluacion.fecha AS fechaEvaluacion, datosRegistro, numEscritura, fechaEscritura, domicilio FROM ((informes INNER JOIN formularios_evaluacion ON informes.codigoFormularioEvaluacion=formularios_evaluacion.codigo) INNER JOIN clientes ON formularios_evaluacion.codigoCliente=clientes.codigo) INNER JOIN fe1 ON formularios_evaluacion.codigo=fe1.codigoFormularioEvaluacion WHERE informes.codigo='$codigoInforme';",false,true);
	cierraBD();
	
	//Formateo de datos
	$arrayFecha=explode('-',$datosFE['fecha']);
	$diaInforme=$arrayFecha[2];
	$mesInforme=obtieneMes($arrayFecha[1]);
	$anioInforme=$arrayFecha[0];

	$arrayFecha=explode('-',$datosFE['fechaEvaluacion']);
	$diaEvaluacion=$arrayFecha[2];
	$mesEvaluacion=obtieneMes($arrayFecha[1]);
	$anioEvaluacion=$arrayFecha[0];

	$razonSocial=preg_replace('/[&|\?]/','',$datosFE['razonSocial']);//Pare evitar errores en la generación del WORD
	$nombreComercial=preg_replace('/[&|\?]/','',$datosFE['nombreComercial']);*/

	//Fichero de Recomendaciones
	$documento=$PHPWord->loadTemplate('../documentos/recomendaciones/plantilla.docx');

	$documento->setValue("dia",utf8_decode($diaInforme));
	$documento->setValue("mes",utf8_decode($mesInforme));
	$documento->setValue("anio",utf8_decode($anioInforme));
	$documento->setValue("nombreCliente",utf8_decode($razonSocial));

	$documento->setValue("numMedidas",$recomendaciones['numMedidas']);
	$documento->setValue("numPreven",$recomendaciones['numPreven']);
	$documento->setValue("numMitiga",$recomendaciones['numMitiga']);
	$documento->setValue("numSeis",$recomendaciones['numSeis']);
	$documento->setValue("numDoce",$recomendaciones['numDoce']);
	$documento->setValue("numDieciocho",$recomendaciones['numDieciocho']);

	for($i=0;$i<6;$i++){
		$documento->setValue($i,$recomendaciones['grafico'.$i]);		
	}

	$documento->setValue("recomendaciones",$tablasRecomendaciones);

	//Gráfico circular
	$imagen=base64_decode($datos['graficoCircular']);
	file_put_contents('../documentos/recomendaciones/image6.png',$imagen);
	$documento->replaceImage('../documentos/recomendaciones/','image6.png');
	//Gráfico de barra
	$imagen=base64_decode($datos['graficoBarraGestion']);
	file_put_contents('../documentos/recomendaciones/image7.png',$imagen);
	$documento->replaceImage('../documentos/recomendaciones/','image7.png');


	$documento->save('../documentos/recomendaciones/Fichero_Recomendaciones.docx');

	//Fichero de Conclusiones
	$documento=$PHPWord->loadTemplate('../documentos/conclusiones/plantilla.docx');

	$documento->setValue("razonSocial",$razonSocial);
	$documento->setValue("nombreComercial",$nombreComercial);
	$documento->setValue("diaInforme",$diaInforme);
	$documento->setValue("mesInforme",utf8_decode($mesInforme));
	$documento->setValue("anioInforme",$anioInforme);
	$documento->setValue("diaFormulario",$diaEvaluacion);
	$documento->setValue("mesFormulario",utf8_decode($mesEvaluacion));
	$documento->setValue("anioFormulario",$anioEvaluacion);
	$documento->setValue("datosRegistro",utf8_decode($datosFE['datosRegistro']));
	$documento->setValue("numEscritura",utf8_decode($datosFE['numEscritura']));
	$documento->setValue("fechaEscritura",formateaFechaWeb($datosFE['fechaEscritura']));
	$documento->setValue("direccionCliente",utf8_decode($datosFE['domicilio']));
	for($i=1;$i<7;$i++){
		$documento->setValue("comentariosDocumentacion$i",utf8_decode($datos["comentariosDocumentacion$i"]));
	}


	$documento->setValue("conclusionesGestion",utf8_decode($datos['textoConclusiones']));

	//Gráficos delitos
	insertaGraficosDelitosDocumento($graficosDelitos,$documento);


	//Gráfico de araña
	$imagen=base64_decode($datosFE['graficoArania']);
	file_put_contents('../documentos/conclusiones/image3.png',$imagen);

	$documento->replaceImage('../documentos/conclusiones/','image3.png');

	$documento->save('../documentos/conclusiones/Fichero_Conclusiones.docx');

	//Fichero ZIP
	$zip = new ZipArchive();

	$nombreZip='Informes.zip';
	$fichero='../documentos/'.$nombreZip;
	
	if($zip->open($fichero,ZIPARCHIVE::CREATE)===true) {
		$zip->addFile('../documentos/recomendaciones/Fichero_Recomendaciones.docx','Fichero_Recomendaciones.docx');
		$zip->addFile('../documentos/conclusiones/Fichero_Conclusiones.docx','Fichero_Conclusiones.docx');

		if(!$zip->close()){
			echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
			echo $zip->getStatusString().'<br />';
		}
	}

	return $nombreZip;
}

function generaInformes($PHPWord,$codigoInforme){
	global $_CONFIG;
	$documento=$PHPWord->loadTemplate('../documentos/personal/plantillaInformes.docx');

	$documento->replaceImage('../img/','image8.png');

	$informe=datosRegistro('informes',$codigoInforme);

	$documento->setValue("fechaInforme",utf8_decode(formateaFechaWeb($informe['fecha'])));
	//Formateo de datos
	$arrayFecha=explode('-',$informe['fecha']);
	$diaInforme=$arrayFecha[2];
	$mesInforme=obtieneMes($arrayFecha[1]);
	$anioInforme=$arrayFecha[0];
	$documento->setValue("dia",utf8_decode($diaInforme));
	$documento->setValue("mes",utf8_decode($mesInforme));
	$documento->setValue("anio",utf8_decode($anioInforme));

	$documento->setValue("nombreEmpresa",utf8_decode($_CONFIG['tituloGeneral']));

	$datos=consultaBD("SELECT gestion_evaluacion_general.codigo AS codigoGestion, informes.codigoEvaluacion, gestion_evaluacion_general.graficoBarra AS graficoBarraGestion, graficoCircular FROM gestion_evaluacion_general INNER JOIN informes ON gestion_evaluacion_general.codigo=informes.codigoGestion WHERE informes.codigo='$codigoInforme';",false,true);
	$recomendaciones=obtieneCalculoRecomendacionesGestion($datos['codigoGestion']);
	$documento->setValue("numMedidas",$recomendaciones['numMedidas']);
	$documento->setValue("numPreven",$recomendaciones['numPreven']);
	$documento->setValue("numMitiga",$recomendaciones['numMitiga']);
	$documento->setValue("numSeis",$recomendaciones['numSeis']);
	$documento->setValue("numDoce",$recomendaciones['numDoce']);
	$documento->setValue("numDieciocho",$recomendaciones['numDieciocho']);

	$graficosRiesgos=obtieneGraficosRiesgos($datos['codigoEvaluacion']);
	$tablasRecomendaciones=obtieneTablasRecomendaciones($datos['codigoGestion'],$graficosRiesgos);
	$documento->setValue("recomendaciones",$tablasRecomendaciones);

	for($i=0;$i<6;$i++){
		$documento->setValue($i,$recomendaciones['grafico'.$i]);		
	}

	$imagen=base64_decode($datos['graficoCircular']);
	file_put_contents('../documentos/conclusiones/image6.png',$imagen);
	$documento->replaceImage('../documentos/conclusiones/','image6.png');
	//Gráfico de barra
	$imagen=base64_decode($datos['graficoBarraGestion']);
	file_put_contents('../documentos/conclusiones/image7.png',$imagen);
	$documento->replaceImage('../documentos/conclusiones/','image7.png');

	$documento->setValue("nombreAbogado",utf8_decode('Nombre Apellidos Abogado'));
	$documento->setValue("nombreAsesorJuridico",utf8_decode('Nombre Apellidos Asesor'));

	$documento->save('../documentos/personal/Fichero_Recomendaciones.docx');

	return "Fichero_Recomendaciones.docx";
}

function generaInforme($PHPWord,$codigoInforme){
	global $_CONFIG;
	$documento=$PHPWord->loadTemplate('../documentos/personal/plantilla_informe_riesgo.docx');


	$informe=datosRegistro('informes',$codigoInforme);
	$evaluacion=datosRegistro('evaluacion_general',$informe['codigoEvaluacion']);
	$gestiones=consultaBD("SELECT * FROM gestion_evaluacion_general WHERE codigoEvaluacion=".$informe['codigoEvaluacion'],true);

	$documento->setValue("fecha",formateaFechaWeb($informe['fecha']));
	$documento->setValue("cliente",utf8_decode($_CONFIG['tituloGeneral']));
	$documento->setValue("evaluacion",utf8_decode(tablaEvaluacion($evaluacion)));
	$documento->setValue("tablaDos",utf8_decode(tablaSegunda($gestiones)));
	//$documento->setValue("anio",utf8_decode($anioInforme));

	$consultaHistorico=consultaBD("SELECT fecha, historico FROM informes WHERE codigoEvaluacion='".$informe['codigoEvaluacion']."' ORDER BY fecha;",true);
	$i=1;
	while($datosHistorico=mysql_fetch_assoc($consultaHistorico)){
		$documento->setValue("r".$i,utf8_decode($i));
		$documento->setValue("f".$i,utf8_decode(formateaFechaWeb($datosHistorico['fecha'])));
		$documento->setValue("h".$i,utf8_decode($datosHistorico['historico']));
		$i++;
	}
	if($i<14){
		while($i<=14){
			$documento->setValue("r".$i,utf8_decode(''));
			$documento->setValue("f".$i,utf8_decode(''));
			$documento->setValue("h".$i,utf8_decode(''));
			$i++;
		}
	}

	$documento->replaceImage('../img/','image1.png');
	if (file_exists('../img/graficos/image5.png')) {
    	unlink('../img/graficos/image5.png');
	}
	$Base64Img = base64_decode($evaluacion['graficoBarraGlobal']);
	file_put_contents('../img/graficos/image5.png', $Base64Img); 
	$documento->replaceImage('../img/graficos/','image5.png');

	$documento->save('../documentos/personal/Fichero_Recomendaciones.docx');

	return "Fichero_Recomendaciones.docx";
}

function generaGráficos($gestion){
	if (file_exists('../img/graficos/image5.png')) {
    	unlink('../img/graficos/image5.png');
	}
	if (file_exists('../img/graficos/image6.png')) {
    	unlink('../img/graficos/image6.png');
	}
	$evaluacion = $gestion['codigoEvaluacion'];
	$riesgos = consultaBD("SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion=".$gestion['codigo']." LIMIT 1",true,true);
	$riesgos = explode('&$&', $riesgos['codigoRiesgo']);
	$i=0;
	$imagen=5;
	while(isset($riesgos[$i])){
		$riesgo = consultaBD("SELECT * FROM riesgos_evaluacion_general WHERE codigoEvaluacion=".$evaluacion." AND codigoRiesgo=".$riesgos[$i],true, true);
		$Base64Img = base64_decode($riesgo['graficoBarra']);
		//escribimos la información obtenida en un archivo llamado 
		//unodepiera.png para que se cree la imagen correctamente
		file_put_contents('../img/graficos/image'.$imagen.'.png', $Base64Img); 
		$i++;
		$imagen++;
	}
	return $i;
}

function tablaEvaluacion($evaluacion){
	$res = cabeceraTablaUno($evaluacion['fechaEvaluacion']);
		$verde='468847';
		$naranja='F89406';
		$rojo='FF0000';
		$probabilidad=array('0'=>'','1'=>'Remota','2'=>'Inusual','3'=>'Ocasional','4'=>'Frecuente');
		$consecuencias=array('0'=>'','1'=>'Moderada','2'=>'Relevante','3'=>'Grave','4'=>'Catastrófica');
		$consulta=consultaBD("SELECT * FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoEvaluacion=".$evaluacion['codigo']." ORDER BY codigoProceso, actividad",true);
		$actividad = '1';
		while($riesgo=mysql_fetch_assoc($consulta)){
			$datosRiesgo=datosRegistro('riesgos',$riesgo['codigoRiesgo']);
			if($actividad == '1'){
				$actividad = $datosRiesgo['actividad'];
			} else if($actividad != $datosRiesgo['actividad']){
				$actividad = $datosRiesgo['actividad'];
				$res.=creaSeparador();
			}
			$colorEnviar='';
			$total='';
			if($riesgo['total']>6){
				$colorEnviar=$rojo;
				$total='INTOLERABLE';
			}elseif($riesgo['total']>=4 && $riesgo['total']<=6){	
				$colorEnviar=$naranja;
				$total='SIGNIFICATIVO';
			}else{
				$colorEnviar=$verde;
				$total='TOLERABLE';
			}
			$res.=creaLineaTablaUno($datosRiesgo['nombre'],$datosRiesgo['descripcion'],$probabilidad[$riesgo['probabilidad']],$consecuencias[$riesgo['consecuencias']],$colorEnviar,$total,$datosRiesgo['codigoProceso'],$datosRiesgo['actividad']);
		}
	$res.='</w:tbl>';

	return $res;
}

function tablaSegunda($gestiones){
	$res = '';
	$codigoGestion=0;
	while($gestion=mysql_fetch_assoc($gestiones)){
	if($codigoGestion == 0){
		$res .= cabeceraTablaDos($gestion['fecha']);
		$codigoGestion = $gestion['codigo'];
	} else if($codigoGestion != $gestion['codigo']){
		$res.='</w:tbl><w:p w14:paraId="0C51166C" w14:textId="77777777" w:rsidR="007E0610" w:rsidRDefault="007E0610" w:rsidP="007E0610"><w:pPr><w:pStyle w:val="ListParagraph"/><w:ind w:left="360"/><w:rPr><w:sz w:val="24"/></w:rPr></w:pPr><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w14:paraId="4B162B5F" w14:textId="26DA1971" w:rsidR="00D23046" w:rsidRDefault="00D23046"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/></w:pPr><w:r><w:br w:type="page"/></w:r></w:p><w:p w14:paraId="180ECA09" w14:textId="14BEAB53" w:rsidR="00D26F43" w:rsidRDefault="00D26F43"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/></w:pPr></w:p><w:p w14:paraId="71BF410D" w14:textId="77777777" w:rsidR="00D26F43" w:rsidRDefault="00D26F43"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/></w:pPr></w:p>';
		$res.= cabeceraTablaDos($gestion['fecha']);
		$codigoGestion = $gestion['codigo'];
	}
		$verde='468847';
		$naranja='F89406';
		$rojo='FF0000';
		$plazos=array('6'=>'6 meses','12'=>'12 meses','18'=>'18 meses','1'=>'Otros');
		$evaluacion=datosRegistro('evaluacion_general',$gestion['codigoEvaluacion']);
		$consulta=consultaBD("SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion=".$gestion['codigo'],true);
		while($riesgo=mysql_fetch_assoc($consulta)){
			$responsables='';
			if($riesgo['responsable'] != '' ){
				$listado = explode('&$&', $riesgo['responsable']);
				$i=0;
				while(isset($listado[$i])){
					$responsable=datosRegistro('personal',$listado[$i]);
					if($i == 0){
						$responsables .= $responsable['nombre'].' '.$responsable['apellidos'];
					} else {
						$responsables .= ','. $responsable['nombre'].' '.$responsable['apellidos'];
					}
					$i++;
				}
			} else {
				$listado = explode('&$&', $riesgo['area']);
				$i=0;
				while(isset($listado[$i])){
					$responsable=datosRegistro('puestosTrabajo',$listado[$i]);
					if($i == 0){
						$responsables .= $responsable['funcion'];
					} else {
						$responsables .= ','. $responsable['funcion'];
					}
					$i++;
				}
			}
			$riesgosExplode=explode('&$&',$riesgo['codigoRiesgo']);
			$riesgoDatos=consultaBD("SELECT total FROM riesgos_evaluacion_general WHERE codigoEvaluacion='".$evaluacion['codigo']."';",true,true);
			foreach($riesgosExplode AS $valor){
				$datosRiesgo=datosRegistro('riesgos',$valor);
				$colorEnviar='';
				$total='';
				if($riesgoDatos['total']>6){
					$colorEnviar=$rojo;
					$total='INTOLERABLE';
				}elseif($riesgoDatos['total']>=4 && $riesgoDatos['total']<=6){	
					$colorEnviar=$naranja;
					$total='SIGNIFICATIVO';
				}else{
					$colorEnviar=$verde;
					$total='TOLERABLE';
				}
				$res.=creaLineaTablaDos($datosRiesgo['nombre'],$riesgo['recomendacion'],$plazos[$riesgo['plazo']],$responsables,$riesgo['checkEjecutada'],$colorEnviar,$total);
			}	
		}
	}
	$res.='</w:tbl>';
	return $res;
}

function obtieneGraficosRiesgos($codigoEvaluacion){
	$res=array();

	$consulta=consultaBD("SELECT riesgos.codigo, descripcion, graficoBarra, nombre FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoEvaluacion='$codigoEvaluacion' ORDER BY nombre;");
	$i=5;//El contador empieza en 5 porque dentro de la estructura interna del Word, la primera imagen a sustituir por un gráfico se llama "image5".
	$j=1;//El contador j sirve para la numeración de los gráficos en el informe de Conclusiones (8.1, 8.2, etc) y para la correlación con el informe de Recomendaciones
	while($datos=mysql_fetch_assoc($consulta)){
		array_push($res,creaImagenGraficoRiesgoDelito($datos,$i,$j));
		$i++;
		$j++;
	}

	//Parte de gráfico global
	$datos=consultaBD("SELECT graficoBarraGlobal FROM evaluacion_general WHERE codigo='$codigoEvaluacion';",false,true);
	$imagen=base64_decode($datos['graficoBarraGlobal']);
	file_put_contents("../documentos/conclusiones/image4.png",$imagen);
	$res['graficoGlobal']='image4.png';
	//Fin parte gráfico global

	return $res;
}

/*
	La siguiente función crea la imagen a partir de la información almacenada en la BDD, 
	y devuelve un array con la forma: numero - delito - grafico - descripcion
	
	Los índices numero, delito, grafico y descripcion se usan para insertar los gráficos en el documento.
	Los índices código y numero se usan para la correlación de las recomendaciones y los delitos en el Informe de Recomendaciones.
*/
function creaImagenGraficoRiesgoDelito($datos,$i,$j){
	$imagen=base64_decode($datos['graficoBarra']);
	file_put_contents("../documentos/conclusiones/image$i.png",$imagen);

	return array('codigoRiesgo'=>$datos['codigo'],'numero'=>"8.$j",'nombre'=>utf8_decode($datos['nombre']),'imagen'=>"image$i.png",'descripcion'=>utf8_decode($datos['descripcion']));
}


function insertaGraficosDelitosDocumento($graficosDelitos,$documento){	
	$i=1;
	foreach ($graficosDelitos as $grafico){
		if($grafico=='image4.png'){//Gráfico global
			$documento->replaceImage('../documentos/conclusiones/',$grafico);
		}
		elseif($grafico['imagen']!='image34.png'){
			$documento->setValue("tituloGrafico$i","8.$i ".$grafico['nombreDelito']);
			$documento->replaceImage('../documentos/conclusiones/',$grafico['imagen']);
			$documento->setValue("descripcionGrafico$i",$grafico['descripcionRiesgo']);
			$documento->setValue("departamentosGrafico$i",$grafico['departamentosGrafico']);
			$i++;
		}
	}
}

function obtieneCalculoRecomendacionesGestion($codigoGestion){
	$res=array('numMedidas'=>0,'numPreven'=>0,'numMitiga'=>0,'numSeis','numDoce'=>0,'numDieciocho'=>0,'grafico0'=>0,'grafico1'=>0,'grafico2'=>0,'grafico3'=>0,'grafico4'=>0,'grafico5'=>0);
	
	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion';",false,true);
	$res['numMedidas']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion' AND area IN(0,1,2,3,4);",false,true);
	$res['numPreven']=$consulta['total'];

	$res['numMitiga']=$res['numMedidas']-$res['numPreven'];//Así me ahorro una consulta

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion' AND plazo='6';",false,true);
	$res['numSeis']=$consulta['total'];

	$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion' AND plazo='12';",false,true);
	$res['numDoce']=$consulta['total'];

	$res['numDieciocho']=$res['numMedidas']-$res['numSeis']-$res['numDoce'];//Y así me ahorro otra consulta :3

	//Para la leyenda del gráfico circular dentro del Word
	for($i=0;$i<6;$i++){
		$consulta=consultaBD("SELECT COUNT(codigo) AS total FROM medidas_riesgos_evaluacion_general WHERE codigoGestion='$codigoGestion' AND area IN($i);",false,true);
		$res['grafico'.$i]=$consulta['total'];
	}

	return $res;
}

function obtieneMes($num){
	$num--;
	$meses=array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
	return $meses[(int)$num];//Hago el casting a int para quitar el 0 de la izquierda (los meses vienen: 01, 02...).
}

function obtieneTablasRecomendaciones($codigoGestion,$datosDelitos){//El sentido del parámetro $graficosDelitos viene explicado en el comentario de la funcón creaImagenGraficoRiesgoDelito
	$res='';
	$imagenes=array('6'=>'rId11','12'=>'rId12','18'=>'rId13');//Este array tiene como claves el número de meses que tiene de plazo cada recomendación para ser implantada, y como valor el ID interno que le asigna Word a cada una de las 3 imágenes en forma de señal con exclamación (roja, amarilla y verde respectivamente).
	$consulta=consultaBD("SELECT medidas_riesgos_evaluacion_general.*, descripcionRiesgo, medidas_riesgos_evaluacion_general.codigoRiesgo, riesgos.nombre AS riesgo, area, responsable FROM ((gestion_evaluacion_general INNER JOIN medidas_riesgos_evaluacion_general ON gestion_evaluacion_general.codigo=medidas_riesgos_evaluacion_general.codigoGestion) INNER JOIN riesgos_evaluacion_general ON gestion_evaluacion_general.codigoEvaluacion=riesgos_evaluacion_general.codigoEvaluacion) INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE codigoGestion='$codigoGestion' GROUP BY medidas_riesgos_evaluacion_general.codigo;");


	while($datos=mysql_fetch_assoc($consulta)){
		$res.=creaTablaRecomendaciones($datos,$imagenes[$datos['plazo']],$datosDelitos);
	}



	return $res;
}

function creaTablaRecomendaciones($datos,$imagen,$datosDelitos){
	$numero=obtieneNumeroCorrelacionDelito($datos['codigoRiesgo'],$datosDelitos);
	$area = consultaBD("SELECT funcion FROM puestosTrabajo WHERE codigo=".$datos['area'],true,true);

	return '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="9634" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1685"/><w:gridCol w:w="1267"/><w:gridCol w:w="3797"/><w:gridCol w:w="1751"/><w:gridCol w:w="1134"/></w:tblGrid>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="535"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t xml:space="preserve">'.utf8_decode('Riesgo').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="5121" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Descripción').'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="00F825E7"><w:trPr><w:trHeight w:val="70"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['riesgo']).'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="5121" w:type="dxa"/><w:gridSpan w:val="2"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['descripcionRiesgo']).'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="00F825E7"><w:trPr><w:trHeight w:val="290"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1278" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="3843" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="996"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Area</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($area['funcion']).'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1123"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Responsable</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['responsable']).'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1376"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Recomendación').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['recomendacion']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1376"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Prioridad').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="center"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-ES" w:eastAsia="es-ES"/></w:rPr><w:drawing><wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="6F693A28" wp14:editId="32E90C64"><wp:extent cx="409575" cy="409575"/><wp:effectExtent l="0" t="0" r="9525" b="9525"/><wp:docPr id="2" name="Picture 2"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/></wp:cNvGraphicFramePr><a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:nvPicPr><pic:cNvPr id="1" name="200px-Gtk-dialog-warning.svg_[1].png"/><pic:cNvPicPr/></pic:nvPicPr><pic:blipFill><a:blip r:embed="'.$imagen.'" cstate="print"><a:extLst><a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}"><a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/></a:ext></a:extLst></a:blip><a:stretch><a:fillRect/></a:stretch></pic:blipFill><pic:spPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="409575" cy="409575"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></pic:spPr></pic:pic></a:graphicData></a:graphic></wp:inline></w:drawing></w:r></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
			</w:tbl><w:p w14:paraId="287010F2" w14:textId="77777777" w:rsidR="00E70E2C" w:rsidRDefault="00E70E2C"/><w:p w14:paraId="287010F2" w14:textId="77777777" w:rsidR="00E70E2C" w:rsidRDefault="00E70E2C"/>';

			
}

function creaTablaRecomendaciones2($datos,$imagen,$datosDelitos){
	$numero=obtieneNumeroCorrelacionDelito($datos['codigoRiesgo'],$datosDelitos);

	return '<w:tbl><w:tblPr><w:tblStyle w:val="Tablaconcuadrcula"/><w:tblW w:w="9634" w:type="dxa"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="1685"/><w:gridCol w:w="1267"/><w:gridCol w:w="3797"/><w:gridCol w:w="1751"/><w:gridCol w:w="1134"/></w:tblGrid>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="535"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t xml:space="preserve">'.utf8_decode('Correlación').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="5121" w:type="dxa"/><w:gridSpan w:val="2"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Descripción').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Responsabilidad Penal (Si/No)</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Prioridad</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="00F825E7"><w:trPr><w:trHeight w:val="70"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.$numero.'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="5121" w:type="dxa"/><w:gridSpan w:val="2"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['descripcionRiesgo']).'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t></w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="auto"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="center"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-ES" w:eastAsia="es-ES"/></w:rPr><w:drawing><wp:inline distT="0" distB="0" distL="0" distR="0" wp14:anchorId="6F693A28" wp14:editId="32E90C64"><wp:extent cx="409575" cy="409575"/><wp:effectExtent l="0" t="0" r="9525" b="9525"/><wp:docPr id="2" name="Picture 2"/><wp:cNvGraphicFramePr><a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/></wp:cNvGraphicFramePr><a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main"><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture"><pic:nvPicPr><pic:cNvPr id="1" name="200px-Gtk-dialog-warning.svg_[1].png"/><pic:cNvPicPr/></pic:nvPicPr><pic:blipFill><a:blip r:embed="'.$imagen.'" cstate="print"><a:extLst><a:ext uri="{28A0092B-C50C-407E-A947-70E740481C1C}"><a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" val="0"/></a:ext></a:extLst></a:blip><a:stretch><a:fillRect/></a:stretch></pic:blipFill><pic:spPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="409575" cy="409575"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></pic:spPr></pic:pic></a:graphicData></a:graphic></wp:inline></w:drawing></w:r></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidTr="00F825E7"><w:trPr><w:trHeight w:val="290"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1278" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="3843" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="000000" w:themeFill="text1"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="996"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>Riesgo</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['riesgo']).'</w:t></w:r></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1123"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Consecuencia Jurídica').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t></w:t></w:r></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
				<w:tr w:rsidR="003856F2" w:rsidRPr="00DD7126" w:rsidTr="003856F2"><w:trPr><w:trHeight w:val="1376"/></w:trPr>
					<w:tc><w:tcPr><w:tcW w:w="1678" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="FEB40A"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r w:rsidRPr="00401C9C"><w:rPr><w:b/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode('Recomendación').'</w:t></w:r></w:p></w:tc>
					<w:tc><w:tcPr><w:tcW w:w="7956" w:type="dxa"/><w:gridSpan w:val="4"/></w:tcPr><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:jc w:val="both"/><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr><w:r><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr><w:t>'.utf8_decode($datos['recomendacion']).'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:p w:rsidR="003856F2" w:rsidRPr="00401C9C" w:rsidRDefault="003856F2" w:rsidP="00F825E7"><w:pPr><w:rPr><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:val="es-VE"/></w:rPr></w:pPr></w:p></w:tc>
				</w:tr>
			</w:tbl><w:p w14:paraId="287010F2" w14:textId="77777777" w:rsidR="00E70E2C" w:rsidRDefault="00E70E2C"/><w:p w14:paraId="287010F2" w14:textId="77777777" w:rsidR="00E70E2C" w:rsidRDefault="00E70E2C"/>';
}

function obtieneNumeroCorrelacionDelito($codigoDelito,$datosDelitos){
	$res='';
	foreach ($datosDelitos as $delito) {
		if(isset($delito['codigoDelito']) && $delito['codigoDelito']==$codigoDelito){
			$res=$delito['numero'];
		}
	}

	return $res;
}

function obtieneGestiones(){
	$res='';
	$datos=arrayFormulario();
	$res="<option value='NULL'></option>";
		
	$consulta=consultaBD("SELECT DISTINCT g.codigo, DATE_FORMAT(fecha,'%d/%m/%Y') AS fecha, m.codigoRiesgo AS riesgos FROM gestion_evaluacion_general g INNER JOIN medidas_riesgos_evaluacion_general m ON g.codigo=m.codigoGestion INNER JOIN riesgos r ON m.codigoRiesgo = r.codigo WHERE g.codigoEvaluacion=".$datos['codigoEvaluacion'],true);
	
	while($datos=mysql_fetch_assoc($consulta)){
		$i=0;
		$riesgos=explode('&$&', $datos['riesgos']);
		$nombresRiesgos = '';
		$primer = true;
		while(isset($riesgos[$i])){
			$riesgo=datosRegistro('riesgos',$riesgos[$i]);
			if($primer){
				$nombresRiesgos=$riesgo['nombre'];
			} else {
				$nombresRiesgos.=", ".$riesgo['nombre'];
			}
			$primer =false;
			$i++;
		}
		$res.="<option value='".$datos['codigo']."'>".$nombresRiesgos." - ".$datos['fecha']."</option>";
	}

	echo $res;
}

function cabeceraTablaUno($fechaEvaluacion){
	return '<w:tbl>
	<w:tblPr>
		<w:tblW w:w="13990" w:type="dxa"/>
		<w:tblInd w:w="40" w:type="dxa"/>
		<w:tblLayout w:type="fixed"/>
		<w:tblCellMar>
			<w:left w:w="70" w:type="dxa"/>
			<w:right w:w="70" w:type="dxa"/>
		</w:tblCellMar>
		<w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
	</w:tblPr>
	<w:tblGrid>
		<w:gridCol w:w="1207"/>
		<w:gridCol w:w="2653"/>
		<w:gridCol w:w="2974"/>
		<w:gridCol w:w="1717"/>
		<w:gridCol w:w="2961"/>
		<w:gridCol w:w="1651"/>
		<w:gridCol w:w="827"/>
	</w:tblGrid>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:gridAfter w:val="2"/>
			<w:wAfter w:w="2478" w:type="dxa"/>
			<w:trHeight w:val="386"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="5627" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>RIESGOS EMPRESARIALES</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1717" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="28"/>
						<w:szCs w:val="28"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2961" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="372"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>PROCESO</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>ACTIVIDAD</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>RIESGO</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>DESCRIPCIÓN</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>VALORACIÓN</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="8" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="8" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="FFFFFF"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>PUNT.</w:t>
				</w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="264"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t xml:space="preserve">Fecha Valoración: </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="006831E1" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="006831E1">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.formateaFechaWeb($fechaEvaluacion).'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="nil"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="4" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:b/>
						<w:bCs/>
						<w:color w:val="000000"/>
						<w:sz w:val="24"/>
						<w:szCs w:val="24"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t> </w:t>
				</w:r>
			</w:p>
		</w:tc>
	</w:tr>';
}

function creaLineaTablaUno($riesgo, $descripcion, $probabilidad, $consecuencia, $color, $total, $proceso, $actividad){
	$proceso=datosRegistro('indicadores',$proceso);
	$proceso=$proceso['nombre'];
	return '<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="255"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:vMerge w:val="restart"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="center"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$proceso.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:vMerge w:val="restart"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$actividad.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:vMerge w:val="restart"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$riesgo.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:vMerge w:val="restart"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$descripcion.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>Probabilidad</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$probabilidad.'</w:t>
				</w:r>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="255"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>Consecuencia</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:proofErr w:type="spellStart"/>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$consecuencia.'</w:t>
				</w:r>
				<w:proofErr w:type="spellEnd"/>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="255"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
	</w:tr>
	<w:tr w:rsidR="008A343D" w:rsidRPr="00C52182" w:rsidTr="00AA4A38">
		<w:trPr>
			<w:trHeight w:val="191"/>
		</w:trPr>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1207" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2653" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="2974" w:type="dxa"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="4678" w:type="dxa"/>
				<w:gridSpan w:val="2"/>
				<w:vMerge/>
				<w:tcBorders>
					<w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:vAlign w:val="center"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="1651" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="auto" w:fill="auto"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r w:rsidRPr="00E1407E">
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>TOTAL</w:t>
				</w:r>
			</w:p>
		</w:tc>
		<w:tc>
			<w:tcPr>
				<w:tcW w:w="827" w:type="dxa"/>
				<w:tcBorders>
					<w:top w:val="nil"/>
					<w:left w:val="nil"/>
					<w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/>
					<w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/>
				</w:tcBorders>
				<w:shd w:val="clear" w:color="000000" w:fill="'.$color.'"/>
				<w:noWrap/>
				<w:vAlign w:val="bottom"/>
				<w:hideMark/>
			</w:tcPr>
			<w:p w:rsidR="008A343D" w:rsidRPr="00E1407E" w:rsidRDefault="008A343D" w:rsidP="00AA4A38">
				<w:pPr>
					<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
					<w:jc w:val="right"/>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
				</w:pPr>
				<w:r>
					<w:rPr>
						<w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/>
						<w:color w:val="000000"/>
						<w:sz w:val="20"/>
						<w:szCs w:val="20"/>
						<w:lang w:eastAsia="es-ES"/>
					</w:rPr>
					<w:t>'.$total.'</w:t>
				</w:r>
				<w:bookmarkStart w:id="0" w:name="_GoBack"/>
				<w:bookmarkEnd w:id="0"/>
			</w:p>
		</w:tc>
	</w:tr>';
}


function cabeceraTablaDos($fechaEvaluacion){
	return '<w:tbl>
			<w:tblPr>
				<w:tblW w:w="14053" w:type="dxa"/>
				<w:tblBorders>
					<w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
					<w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
				</w:tblBorders>
				<w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/>
			</w:tblPr>
			<w:tblGrid>
				<w:gridCol w:w="2286"/>
				<w:gridCol w:w="1914"/>
				<w:gridCol w:w="3388"/>
				<w:gridCol w:w="944"/>
				<w:gridCol w:w="271"/>
				<w:gridCol w:w="1289"/>
				<w:gridCol w:w="1523"/>
				<w:gridCol w:w="2438"/>
			</w:tblGrid>
			<w:tr w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidTr="00886D27">
				<w:trPr>
					<w:trHeight w:val="626"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="7588" w:type="dxa"/>
						<w:gridSpan w:val="3"/>
						<w:tcBorders>
							<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						</w:tcBorders>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
							</w:rPr>
							<w:t>MEDIDAS PARA LA GESTIÓN DE RIESGOS</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1215" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:sz w:val="28"/>
							</w:rPr>
						</w:pPr>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="5250" w:type="dxa"/>
						<w:gridSpan w:val="3"/>
						<w:shd w:val="clear" w:color="auto" w:fill="339966"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:sz w:val="28"/>
							</w:rPr>
							<w:t>FECHA</w:t>
						</w:r>
						<w:r w:rsidRPr="006831E1">
							<w:rPr>
								<w:b/>
								<w:sz w:val="28"/>
							</w:rPr>
							<w:t>: '.formateaFechaWeb($fechaEvaluacion).'</w:t>
						</w:r>
					</w:p>
				</w:tc>
			</w:tr>
			<w:tr w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidTr="00886D27">
				<w:trPr>
					<w:trHeight w:val="412"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="2286" w:type="dxa"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>RIESGO</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1914" w:type="dxa"/>
						<w:tcBorders>
							<w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
						</w:tcBorders>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>NIVEL DE RIESGO</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="4332" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>RECOMENDACIÓN</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1560" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>PLAZO</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1523" w:type="dxa"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>RESPONSABLE</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="2438" w:type="dxa"/>
						<w:shd w:val="clear" w:color="auto" w:fill="006666"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="00886D27">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
						</w:pPr>
						<w:r w:rsidRPr="00C52182">
							<w:rPr>
								<w:b/>
								<w:color w:val="FFFFFF"/>
							</w:rPr>
							<w:t>EJECUTADA</w:t>
						</w:r>
					</w:p>
				</w:tc>
			</w:tr>';
}

function creaLineaTablaDos($riesgo,$recomendacion,$plazo,$responsable,$ejecutada,$color,$total){
	return '<w:tr w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidTr="0056060A">
				<w:trPr>
					<w:trHeight w:val="1956"/>
				</w:trPr>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="2286" w:type="dxa"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
							<w:rPr>
								<w:b/>
							</w:rPr>
						</w:pPr>
						<w:proofErr w:type="spellStart"/>
						<w:r>
							<w:rPr>
								<w:b/>
							</w:rPr>
							<w:t>'.$riesgo.'</w:t>
						</w:r>
						<w:proofErr w:type="spellEnd"/>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1914" w:type="dxa"/>
						<w:shd w:val="clear" w:color="auto" w:fill="'.$color.'"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:r>
							<w:t>'.$total.'</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="4332" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:r>
							<w:t>'.$recomendacion.'</w:t>
						</w:r>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1560" w:type="dxa"/>
						<w:gridSpan w:val="2"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:proofErr w:type="spellStart"/>
						<w:r>
							<w:t>'.$plazo.'</w:t>
						</w:r>
						<w:proofErr w:type="spellEnd"/>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="1523" w:type="dxa"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:r>
							<w:t>'.$responsable.'</w:t>
						</w:r>
						<w:bookmarkStart w:id="0" w:name="_GoBack"/>
						<w:bookmarkEnd w:id="0"/>
					</w:p>
				</w:tc>
				<w:tc>
					<w:tcPr>
						<w:tcW w:w="2438" w:type="dxa"/>
						<w:vAlign w:val="center"/>
					</w:tcPr>
					<w:p w:rsidR="0048154A" w:rsidRPr="00C52182" w:rsidRDefault="0048154A" w:rsidP="0048154A">
						<w:pPr>
							<w:tabs>
								<w:tab w:val="left" w:pos="7320"/>
							</w:tabs>
							<w:spacing w:after="0" w:line="240" w:lineRule="auto"/>
							<w:jc w:val="center"/>
						</w:pPr>
						<w:r>
							<w:t>'.$ejecutada.'</w:t>
						</w:r>
					</w:p>
				</w:tc>
			</w:tr>';
}

function creaSeparador(){
	return '<w:tr w:rsidR="00AD20F0" w:rsidRPr="00E1407E" w14:paraId="29C97D53" w14:textId="77777777" w:rsidTr="00AD20F0"><w:trPr><w:trHeight w:val="255"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="3604" w:type="dxa"/><w:gridSpan w:val="2"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:noWrap/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="2204E78D" w14:textId="6C401450" w:rsidR="00AD20F0" w:rsidRDefault="0023752D" w:rsidP="00A80799"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t></w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2771" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:noWrap/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="3133E3BD" w14:textId="77777777" w:rsidR="00AD20F0" w:rsidRDefault="00AD20F0" w:rsidP="00A80799"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="center"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4364" w:type="dxa"/><w:gridSpan w:val="2"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:vAlign w:val="center"/></w:tcPr><w:p w14:paraId="2EB4A97F" w14:textId="77777777" w:rsidR="00AD20F0" w:rsidRDefault="00AD20F0" w:rsidP="00EE7A63"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1544" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="nil"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:noWrap/><w:vAlign w:val="bottom"/></w:tcPr><w:p w14:paraId="63D72CD3" w14:textId="77777777" w:rsidR="00AD20F0" w:rsidRPr="00E1407E" w:rsidRDefault="00AD20F0" w:rsidP="00EE7A63"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1707" w:type="dxa"/><w:tcBorders><w:top w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:left w:val="nil"/><w:bottom w:val="single" w:sz="12" w:space="0" w:color="000000"/><w:right w:val="single" w:sz="12" w:space="0" w:color="000000"/></w:tcBorders><w:shd w:val="clear" w:color="auto" w:fill="D9D9D9" w:themeFill="background1" w:themeFillShade="D9"/><w:noWrap/><w:vAlign w:val="bottom"/></w:tcPr><w:p w14:paraId="50DF83A0" w14:textId="2A19F898" w:rsidR="00AD20F0" w:rsidRDefault="0023752D" w:rsidP="00EE7A63"><w:pPr><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:jc w:val="right"/><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr></w:pPr><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Arial"/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/><w:lang w:eastAsia="es-ES"/></w:rPr><w:t></w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';
}

function generaPDF($codigo){
	global $_CONFIG;
	$datos=datosRegistro('informes',$codigo);
    $contenido = "
	<style type='text/css'>
	<!--
			html{
				margin:0px;
				padding:0px;
				width:100%;
			}
	        body{
	            font-size:12px;
	            font-family: Arial;
	            font-weight: lighter;
	            line-height: 24px;
	            margin:0px;
				padding:0px;
				width:100%;

	        }

	        li{
	        	padding-bottom:10px;
	        	line-height:20px;
	        }

	        .header{
	        	padding-left:50px;
	        	height:50px;
	        	float:left;
	        }

	        .container{
	        	width:100%;
	        	float:left;
	        }

	        .content{
	        	width:80%;
	        	margin-left:60px;
	        }

	        table{
	        	border:1px solid #000;
	        	border-collapse: collapse;
	        	width:100%;
	        }

	        table td,
	        table th{
	        	border:1px solid #000;
	        	font-size: 12px;
	        	padding:5px;
	        	width:100%;
	        	text-align:center;
	        }

	        .col2 td,
	        .col2 th{
	        	width:50%;
	        }

	        .col3 td,
	        .col3 th{
	        	width:33.3%;
	        }

	        .col4 td,
	        .col4 th{
	        	width:25%;
	        }

	        .col5 td,
	        .col5 th{
	        	width:20%;
	        }

	        table th{
	        	background:#999999;
	        }

	        .blue th{
	        	background:rgb(211,222,238);
	        }

	        .conclusion td{
	        	text-align:left;
	        	font-weight:bold;
	        }

	        .conclusion .impar{
	        	background:rgb(211,222,238);
	        }

	        hr{
	        	color:rgb(211,222,238);
	        }
	        
	        .title{
	        	font-size:14px;
	        	font-weight:bold;
	        	text-align:left;
	        	padding:5px;
	        	margin:0px 0px 10px 0px;
	        	width:100%;
	        }

	        .subtitle{
	        	margin-left:15px;
	        }

	        .ficheros{
	        	width:100%;
	      		line-height:18px;
	        	border:0px !important;
	        }

	        .ficheros td{
	        	border:0px;
	        	border-bottom:1 dashed #000;
	        	padding:5px 0px;
	        	text-align:left;
	        }

	        .ficheros .tituloFichero{
	        	width:30%;
	        }

	        .ficheros .descFichero{
	        	width:70%;
	        }

	        .calificacion{
	        	width:25px;
	        	height:22px;
	        	background:rgb(11,89,179);
	        	border:1px solid #000;
	        	padding:12px;
	        	margin-top:5px; 
	        	margin-left:350px; 
	        	margin-bottom:5px; 
	        	margin-right:0px;
	        	position:relative;
	        }

	        .calificacion .inner{
	        	background:rgb(211,222,238);
	        	position:absolute;
	        	top:10px;
	        	width:30px;
	        	left:10px;
	        	border:1px solid #000;
	        	text-align:center;
	        	font-size:24px;
	        }

	        .cajaPie{
	            width:100%;
	            height:auto;
	            position:relative;
	            margin-top:50px;
	            color:#333;
	            padding:10px;
	            padding-top:0px;
	            font-size:10px;
	            text-align:center;
	        }

	        .cajaPiePortada{
	            width:100%;
	            height:auto;
	            position:relative;
	            color:#333;
	            padding:10px;
	            padding-top:0px;
	            font-size:10px;
	            text-align:right;
	            margin-bottom:20px;
	        }

	        .cajaPie .left{
	        	text-align:left;
	        }

	        .titlePortada{
	        	font-size:36px;
	        	color:#11836B;
	        	text-align:right;
	        	margin-right:20px;
	        	margin-top:200px;
	        }

	        .textPortada{
	        	margin-top:0px;
	        	font-size:26px;
	        	color:#0C519A;
	        	z-index:10;
	        	position:absolute;
	        	top:100px;
	        	left:50px;
	        	width:580px;
	        	text-align:center;
	        }

	        .razonPortada{
	        	font-size:26px;
	        	z-index:10;
	        	position:absolute;
	        	top:350px;
	        	left:50px;
	        	margin:0px;
	        	width:580px;
	        }	  	       

	        .fondoPortada{
	        	z-index:1;
	        	width:100%;
	        	margin-top:-15px;
	        }

	        .fechaPortada{
	        	margin-top:50px;
	        	text-align:center;
	        } 

	        .certificado{
	        	line-height:20px;
	        }

	        .firma{
	        	float:right;
	        }

	        ol.indice{
	        	margin-top:40px;
	        	margin-left:100px;
	        }

	        ol.indice li{{
	        	margin-bottom:20px;
	        }

	        table.historico .a15{
	        	width:15%;
	        	text-align:center;
	        }

	        table.historico .a70{
	        	width:70%;
	        	text-align:center;
	        }

	        table.historico th{
	        	background:rgb(92,147,101);
	        	color:#FFF;
	        }

	        table.historico .even td{
	        	background:rgb(229,239,217);
	        }

	        table .a10{
	        	width:10%;
	        }

	        table .a15{
	        	width:15%;
	        }

	        table .a20{
	        	width:20%;
	        }

	        table .a25{
	        	width:25%;
	        }

	        table .a30{
	        	width:30%;
	        }

	        table .a40{
	        	width:40%;
	        }

	        table .a70{
	        	width:70%;
	        }

	        table .a75{
	        	width:75%;
	        }

	        table .a80{
	        	width:80%;
	        }

	        table .a100{
	        	width:100%;
	        	background:rgb(216,216,216);
	        }

	        table.evaluacion .primeraCabecera .uno{
	        	background:transparent;
	        	text-align:left;
	        }

	        table.evaluacion .primeraCabecera .dos{
	        	background:transparent;
	        	border-top:0px;
	        	border-right:0px;
	        }

	        table.evaluacion .segundaCabecera th{
	        	background:rgb(58,102,102);;
	        	color:#FFF;
	        	font-weight:bold;
	        }

	        table.evaluacion .terceraCabecera .uno{
	        	background:rgb(95,151,104);
	     		text-align:right;
	        }

	        table.evaluacion .terceraCabecera .dos{
	        	background:rgb(95,151,104);
	     		text-align:left;
	        }

	        table.evaluacion .right{
	        	text-align:right;
	        }

	        table .naranja{
	        	background:#F89406;
	        }

	        table .rojo{
	        	background:#FF0000;
	        }

	        table .verde{
	        	background:#468847;
	        }

	        table .info{
	        	background:#d9edf7;
	        }

	        table.gestion .primeraCabecera .uno{
	        	background:transparent;
	        	text-align:center;
	        }	

	        table.gestion .primeraCabecera .dos{
	        	background:rgb(95,151,104);
	        	text-align:center;
	        }

	        table.gestion .segundaCabecera th{
	        	background:rgb(58,102,102);;
	        	color:#FFF;
	        	font-weight:bold;
	        }		

	        img.graficoBarra{
	        	height:70%;
	        }
	-->
	</style>
	

	<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' >
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<div align='center'>
		<img src='../img/logoInforme.png' alt='QMA consultores' width='327' height='152'>
	</div>
	<div class='titlePortada'>EVALUACIÓN DE<br/>RIESGOS<br/>EMPRESARIALES</div>
	<page_footer>
	    	<div class='cajaPiePortada'>
	    		<img src='../img/logo.png' alt='QMA consultores' height='100'>
	        	<p>ALCANCE DE LAS ACTIVIDADES</p>
	    	</div>
	</page_footer>
	</page>";

	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' >
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<ol class='indice'>
		<li>HISTÓRICO DE REVISIONES</li>
		<li>OBJETO</li>
		<li>ALCANCE</li>
		<li>GENERAL
			<ol>
				<li>DEFINICIONES</li>
				<li>NORMA DE REFERENCIA</li>
			</ol>
		</li>
		<li>PRODECIMIENTO
			<ol>
				<li>IDENTIFICACIÓN Y EVALUACIÓN DE RIESGOS EMPRESARIALES</li>
				<li>GESTIÓN DEL RIESGO</li>
			</ol>
		</li>
		<li>ANEXO I: RIESGOS EVALUDADOS</li>
		<li>ANEXO II: MEDIDAS PARA LA GESTIÓN DEL RIESGO</li>
		<li>ANEXO III: ANÁLISIS GRÁFICO DE LA EXPOSICIÓN AL RIESGO</li>
	</ol>
	</page>";

	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' >
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<p class='title'>1. HISTÓRICO DE REVISIONES</p>
	<p>Todas las revisiones relativas al presente informe se registrarán en la siguiente tabla, con su fecha y causas</p>
	".obtieneTablaHistorico($datos['codigoEvaluacion'])."
	</page>";

	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' >
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<p class='title'>2. OBJETO</p>
	<p>Establecer la sistemática a seguir para identificar, evaluar y registrar los riesgos a los que <b>".$_CONFIG['title']."</b> está expuesto y que generan incertidumbre para la consecución de nuestros objetivos, para determinar aquellos que tienen o pueden tener efectos significativos en los objetivos de <b>".$_CONFIG['title']."</b> y establecer medidas para su control.<br/><br/>
		La identificación y evaluación de riesgo permite:
	</p>
	<ul>
		<li>Asegurar que los riesgos empresariales son identificados.</li>
		<li>Establecer medidas de control, al menos, sobre aquellos que supongan un riesgo intolerable para <b>".$_CONFIG['title']."</b> a corto plazo.</li>
		<li>Definir las pautas de actuación ante situaciones de materialización de los riesgos evaluados.</li>
	</ul>
	
	<p class='title'>3 ALCANCE</p>
	<p>Este procedimiento aplica a las actividades actuales y futuras desarrolladas por <b><b>".$_CONFIG['title']."</b></b> (directamente o a través de subcontratas relacionadas con actividades y servicios dentro del ALCANCE DEL SISTEMA.)
	</p>
	
	<p class='title'>4. GENERAL</p>
	<p class='title subtitle'>4.1. DEFINICIONES</p>
	<p><b>Organización</b>: Persona o grupo de personas que tiene sus propias funciones con responsabilidades, autoridades y relaciones para lograr sus objetivos.<br/>
Nota 1. El concepto de organización incluye, entre otros, un trabajador independiente, compañía, corporación, firma, empresa, autoridad, sociedad, asociación, organización benéfica o institución, o una parte o combinación de éstas, ya estén constituidas o no, públicas o privadas.<br/>
Nota 2. Este término constituye uno de los términos comunes y definiciones esenciales para las normas de sistemas de gestión que se proporcionan en el Anexo SL del Suplemento ISO consolidado de la Parte 1 de las Directivas ISO/IEC. La definición original se ha modificado añadiendo la nota 1 a la entrada.<br/><br/>

<b>Contexto de la organización</b>: Combinación de cuestiones internas y externas que pueden tener un efecto en el enfoque de la organización para el desarrollo y logro de sus objetivos.<br/>
Nota 1 a la entrada Los objetivos de la organización pueden estar relacionados con sus productos y servicios, inversiones y comportamiento hacia sus partes interesadas.<br/>
Nota 2 a la entrada El concepto de contexto de la organización se aplica por igual tanto a organizaciones sin fines de lucro o de servicio público como a aquellas que buscan beneficios con frecuencia.<br/>
Nota 3 a la entrada En inglés, este concepto con frecuencia se denomina mediante otros términos, tales como “entorno empresarial”, “entorno de la organización” o “ecosistema de una organización”.<br/>
Nota 4 a la entrada: Entender la infraestructura puede ayudar a definir el contexto de la organización.<br/><br/>

<b>Parte interesada</b>: Persona u organización que puede afectar, verse afectada o percibirse como afectada por una decisión o actividad.<br/>
EJEMPLO Clientes, propietarios, personas de una organización, proveedores, banca, legisladores, sindicatos, socios o sociedad en general que puede incluir competidores o grupos de presión con intereses opuestos.<br/><br/>

Nota 1 a la entrada Este término constituye uno de los términos comunes y definiciones esenciales para las normas de sistemas de gestión que se proporcionan en el Anexo SL del Suplemento ISO consolidado de la Parte 1 de las Directivas ISO/IEC. La definición original se ha modificado añadiendo el ejemplo.<br/><br/></p>
</page>";
$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' >
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
<p>
<b>Objetivo</b>: Resultado a lograr.<br/>
Nota 1. Un objetivo puede ser estratégico, táctico u operativo.<br/>
Nota 2. Los objetivos pueden referirse a diferentes disciplinas (tales como objetivos financieros, de salud y seguridad y ambientales) y se pueden aplicar en diferentes niveles [como estratégicos, para toda la organización (3.2.1), para el proyecto (3.4.2), el producto (3.7.6) y el proceso (3.4.1)].<br/>
Nota 3. Un objetivo se puede expresar de otras maneras, por ejemplo, como un resultado previsto, un propósito, un criterio operativo, un objetivo de la calidad (3.7.2), o mediante el uso de términos con un significado similar (por ejemplo, fin o meta).<br/>
Nota 4. En el contexto de sistemas de gestión de la calidad (3.5.4), la organización (3.2.1) establece los objetivos de la calidad (3.7.2), de forma coherente con la política de la calidad (3.5.9), para lograr resultados específicos.<br/><br/>

<b>Riesgo</b>: Efecto de la incertidumbre.<br/>
Nota 1. Un efecto es una desviación de lo esperado, ya sea positivo o negativo.<br/>
Nota 2. Incertidumbre es el estado, incluso parcial, de deficiencia de información (3.8.2) relacionada con la comprensión o conocimiento de un evento, su consecuencia o su probabilidad.<br/>
Nota 3. Con frecuencia el riesgo se caracteriza por referencia a eventos potenciales (según se define en la Guía ISO 73:2009, 3.5.1.3) y consecuencias (según se define en la Guía ISO 73:2009, 3.6.1.3), o a una combinación de éstos.<br/>
Nota 4. Con frecuencia el riesgo se expresa en términos de una combinación de las consecuencias de un evento (incluidos cambios en las circunstancias) y la probabilidad (según se define en la Guía ISO 73:2009, 3.6.1.1) asociada de que ocurra.<br/> 
Nota 5. La palabra “riesgo” algunas veces se utiliza cuando sólo existe la posibilidad de consecuencias negativas.<br/><br/>
	</p>
	<p class='title subtitle'>4.2. NORMA DE REFERENCIA</p>
	<p>Este informe es en base a la implantación revisada y finalizada de la Norma UNE EN ISO 9001:2015, así como a la Norma ISO 9000:2015.</p>

	<p class='title'>5. PRODECIMIENTO</p>
	<p class='title subtitle'>5.1. IDENTIFICACIÓN Y EVAUACIÓN DE RIESGOS EMPRESARIALES</p>
	<p>El presente informe es el resultado de la aplicación de la IT – 01, EVALUACIÓN DEL RIESGO, que incluye los criterios para la identificación y evaluación de los Riesgos incluidos en el presente informe.</p>

	<p class='title subtitle'>5.2. GESTIÓN DEL RIESGO EMPRESARIAL</p>
	<p>Este informe no es sino el punto de partida para que, una vez identificados los riesgos a los que nuestra compañía está expuesta y evaluados los efectos de su materialización en base a la probabilidad de ocurrencia, las medidas incluidas en el ANEXO II sean puestas en marchas con las prioridades establecidas en cada una de ellas, con el objetivo de minimizar los efectos en caso de materialización y/o, en su caso, llegar a eliminarlos.</p>
	</page>";


	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' orientation='L'>
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<p class='title'>6. ANEXO I: EVALUACIÓN DE RIESGOS</p>
	".obtieneTablaEvaluacion($datos['codigoEvaluacion'])."
	</page>";

	$contenido.="<page footer='page' backbottom='27mm' backleft='0mm' backright='0mm' orientation='L'>
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($datos['fecha'])."</p>
	<p class='title'>7. ANEXO II: PROGRAMA DE GESTIÓN DE RIESGOS</p>
	".obtieneTablaGestion($datos['codigoEvaluacion'],$datos['fecha'])."
	</page>";

	/*$evaluacion=datosRegistro('evaluacion_general',$datos['codigoEvaluacion']);
	$Base64Img = base64_decode($evaluacion['graficoBarraGlobal']);
	file_put_contents('../img/graficos/image5.png', $Base64Img); 
	$contenido.="<p class='title'>8. ANEXO III: ANÁLISIS GRÁFICO DE LA EXPOSICIÓN AL RIESGO</p>
	<img class='graficoBarra' src='../img/graficos/image5.png'>";*/

	return $contenido;
}

function obtieneTablaHistorico($evaluacion){
	$tabla="<table class='historico'>
		<tr>
			<th class='a15'>REVISIÓN</th>
			<th class='a15'>FECHA</th>
			<th class='a70'>HISTÓRICO</th>
		</tr>";
	$clase='even';
	$informes=consultaBD("SELECT * FROM informes WHERE codigoEvaluacion=".$evaluacion." ORDER BY fecha",true);
	$i=1;
	while($datos=mysql_fetch_assoc($informes)){
		$tabla.="
		<tr class='".$clase."'>
			<td class='a15'>".$i."</td>
			<td class='a15'>".formateaFechaWeb($datos['fecha'])."</td>
			<td class='a70'>".$datos['historico']."</td>
		</tr>";
		$i++;
		$clase=$clase=='even'?'odd':'even';
	}
	$tabla.='</table>';
	return $tabla;
}

function obtieneTablaEvaluacion($evaluacion){
	$evaluacion=datosRegistro('evaluacion_general',$evaluacion);
	$probabilidad=array('0'=>'',''=>'','1'=>'Remota','2'=>'Inusual','3'=>'Ocasional','4'=>'Frecuente');
	$consecuencias=array('0'=>'',''=>'','1'=>'Moderada','2'=>'Relevante','3'=>'Grave','4'=>'Catastrófica');
	$tabla="<table class='evaluacion'>
		<tr class='primeraCabecera'>
			<th class='a80 uno' colspan='4'>RIESGOS EMPRESARIALES</th>
			<th class='a20 dos' colspan='2'></th>
		</tr>
		<tr class='segundaCabecera'>
			<th class='a10' >TIPO</th>
			<th class='a15' >CÓDIGO</th>
			<th class='a15' >RIESGO</th>
			<th class='a40' >DESCRIPCIÓN</th>
			<th class='a10' >VALORACIÓN</th>
			<th class='a10' >PUNT.</th>
		</tr>
		<tr class='terceraCabecera'>
			<th class='a25 uno' colspan='2'>Fecha Valoración:</th>
			<th class='a75 dos' colspan='4'>".formateaFechaWeb($evaluacion['fechaEvaluacion'])."</th>
		</tr>";
	$riesgos=consultaBD("SELECT riesgos_evaluacion_general.*, riesgos.codigoProceso, riesgos.actividad, riesgos.nombre FROM riesgos_evaluacion_general INNER JOIN riesgos ON riesgos_evaluacion_general.codigoRiesgo=riesgos.codigo WHERE riesgos_evaluacion_general.codigoEvaluacion='".$evaluacion['codigo']."';",true);
	$codigoProceso='';
	$first=true;
	while($datos=mysql_fetch_assoc($riesgos)){
		$total="<td class='a10 verde right'>TOLERABLE</td>";
		if($datos['total']>6){
			$total="<td class='a10 rojo right'>INTOLERABLE</td>";
		}elseif($datos['total']>=4 && $datos['total']<=6){	
			$total="<td class='a10 naranja right'>SIGNIFICATIVO</td>";
		}

		$riesgo=datosRegistro('riesgos',$datos['codigoRiesgo']);
		if($datos['codigoProceso'] != $codigoProceso && !$first){
			$tabla.="<tr><td colspan='6' class='a100'></td></tr>";
		}
		$codigoProceso=$datos['codigoProceso'];
		if($riesgo['tipo'] == 'RIESGO'){
			$tabla.="<tr>
				<td class='a10' rowspan='4'>".$datos['codigoProceso']."</td>
				<td class='a15' rowspan='4'>".$riesgo['actividad']."</td>
				<td class='a15' rowspan='4'>".$riesgo['nombre']."</td>
				<td class='a40' rowspan='4'>".$datos['descripcionRiesgo']."</td>
				<td class='a10'>Probabilidad</td>
				<td class='a10'>".$probabilidad[$datos['probabilidad']]."</td>
			</tr>
			<tr>
				<td class='a10'>Consecuencia</td>
				<td class='a10'>".$consecuencias[$datos['consecuencias']]."</td>
			</tr>
			<tr>
				<td class='a10'></td>
				<td class='a10'></td>
			</tr>
			<tr>
				<td class='a10 right'>TOTAL</td>
				".$total."
			</tr>";
		} else {
			$tabla.="<tr>
			<td class='a10'>".$datos['codigoProceso']."</td>
			<td class='a15'>".$riesgo['actividad']."</td>
			<td class='a15'>".$riesgo['nombre']."</td>
			<td class='a40'>".$datos['descripcionRiesgo']."</td>";
			$total="<td class='a20 info right' text-align='center' colspan='2'>OPORTUNIDAD</td>";
			$tabla.=$total."
			</tr>";
		}
		$first=false;
	}
	$tabla.="</table>";
	return $tabla;
}

function obtieneTablaGestion($codigoEvaluacion,$fechaEvaluacion){
	$tabla='';
	$pie="</page><page footer='page' backbottom='0mm' backleft='0mm' backright='0mm' orientation='L'>
	<img class='fondoPortada' src='../img/fondoInforme.png' alt='QMA Consultores'>
	<p class='fechaPortada'>INFORME GESTIÓN DEL RIESGO: ".formateaFechaWeb($fechaEvaluacion)."</p>";

	$riesgosEvaluacion=consultaBD("SELECT * FROM riesgos_evaluacion_general WHERE codigoEvaluacion=".$codigoEvaluacion,true);
	while($riesgoEvaluacion=mysql_fetch_assoc($riesgosEvaluacion)){
	$gestiones=consultaBD("SELECT * FROM gestion_evaluacion_general WHERE codigoEvaluacion=".$riesgoEvaluacion['codigo']." ORDER BY fecha",true);
	$j=1;
	while($gestion=mysql_fetch_assoc($gestiones)){
		$tabla.=creaCabeceraTablaGestion($gestion['fecha']);
		$medidas=consultaBD("SELECT * FROM medidas_riesgos_evaluacion_general WHERE codigoGestion=".$gestion['codigo'],true);
		$i=0;
		$listadoRiesgos=array();
		while($medida=mysql_fetch_assoc($medidas)){
			$nombreRiesgo=datosRegistro('riesgos',$riesgoEvaluacion['codigoRiesgo']);
			//$proceso=datosRegistro('indicadores',$nombreRiesgo['codigoProceso']);
			//$tipo=$proceso['nombre'];
			$listadoRiesgos[$i]=array();
			$listadoRiesgos[$i]['nombre']=$nombreRiesgo['nombre'];
			$listadoRiesgos[$i]['recomendacion']=$medida['recomendacion'];
			$listadoRiesgos[$i]['total']=$riesgoEvaluacion['total'];
			$listadoRiesgos[$i]['medida']=$medida['recomendacion'];
			$listadoRiesgos[$i]['plazo']=$medida['plazo'] == 1 ? $medida['otroPlazo'] : $medida['plazo'].' meses';
			$listadoRiesgos[$i]['responsable']=$medida['responsable'];
			//$listadoRiesgos[$i]['area']=$medida['area'];
			$listadoRiesgos[$i]['checkEjecutada']=$medida['checkEjecutada'];
			//$listadoRiesgos[$i]['tipo']=$tipo;
			$listadoRiesgos[$i]['tipoRiesgo']=$nombreRiesgo['tipo'];
			$listadoRiesgos[$i]['codigoInterno']=$nombreRiesgo['actividad'];
			$i++;
	
		}
		usort($listadoRiesgos, "compararProcesos");
		foreach ($listadoRiesgos as $riesgo) {
			$lineaTotal="<td class='a15 verde right'>TOLERABLE</td>";
			if($riesgo['total']>11){
				$lineaTotal="<td class='a15 rojo right'>INTOLERABLE</td>";
			}elseif($riesgo['total']>=3 && $riesgo['total']<=11){	
				$lineaTotal="<td class='a15 naranja right'>SIGNIFICATIVO</td>";
			}
			if($riesgo['tipoRiesgo'] == 'OPORTUNIDAD'){
				$lineaTotal="<td class='a15 info right'>OPORTUNIDAD</td>";
			}
			$plazo = $riesgo['plazo'];
			$listaResponsables='';
			if($riesgo['responsable'] != ''){
				$listaResponsables=$riesgo['responsable'];
				//$responsables=explode('&$&', $riesgo['responsable']);
				/*foreach ($responsables as $responsable) {
					$responsable=datosRegistro('personal',$responsable);
					if($listaResponsables==''){
						$listaResponsables=$responsable['nombre'].' '.$responsable['apellidos'];
					} else {
						$listaResponsables.=', '.$responsable['nombre'].' '.$responsable['apellidos'];
					}
				}*/
			} else {
				//$areas=explode('&$&', $riesgo['area']);
				/*foreach ($areas as $area) {
					$area=datosRegistro('puestosTrabajo',$area);
					if($listaResponsables==''){
						$listaResponsables=$area['funcion'];
					} else {
						$listaResponsables.=', '.$area['funcion'];
					}
				}*/
			}
			$tabla.="
				<tr>
					<td class='a15'>".$riesgo['codigoInterno']." - ".$riesgo['nombre']."</td>
						".$lineaTotal."
					<td class='a40'>".$riesgo['recomendacion']."</td>
					<td class='a10'>".$plazo."</td>
					<td class='a10'>".$listaResponsables."</td>
					<td class='a10'>".$riesgo['checkEjecutada']."</td>
				</tr>";	
		}
		$tabla.='</table><br/>';
		if($j%2==0){
			$tabla.=$pie;
		}
		$j++;
	}
	}
	if($j%2==0){
		$tabla.=$pie;
	}
	return $tabla;
}

function creaCabeceraTablaGestion($fecha){
	$cabeceraTabla="<table class='gestion'>
		<tr class='primeraCabecera'>
			<th class='a70 uno' colspan='3'>MEDIDAS PARA LA GESTIÓN DE RIESGOS</th>
			<th class='a30 dos' colspan='3'>FECHA: ".formateaFechaWeb($fecha)."</th>
		</tr>
		<tr class='segundaCabecera'>
			<th class='a15'>RIESGO</th>
			<th class='a15'>NIVEL DE RIESGO</th>
			<th class='a40'>RECOMENDACIÓN</th>
			<th class='a10'>PLAZO</th>
			<th class='a10'>RESPONSABLE</th>
			<th class='a10'>EJECUTADA</th>
		</tr>";
	return $cabeceraTabla;
}

function compararProcesos($a, $b){
	$res=0;
	$i=0;
	$campos=array('codigoInterno');
	while($res==0 && $i<1){
    	$res=strcmp($a[$campos[$i]], $b[$campos[$i]]);
    	$i++;
	}
    return $res;
}

//Fin parte de generación de informes
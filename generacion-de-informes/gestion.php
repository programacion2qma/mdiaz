<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionInforme();
?>

<script src="../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/funciones25.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	$('.selectpicker').selectpicker();

	$('#codigoEvaluacion').change(function(){
		var codigoEvaluacion=$(this).val();
		var consultaGestiones=$.post('obtieneGestiones.php',{'codigoEvaluacion':codigoEvaluacion});
		consultaGestiones.done(function(gestiones){
			actualizaTablaGestiones(gestiones);
		});
	});
});

function actualizaTablaGestiones(gestiones){
	$('#tablaGestiones').find('select').each(function(){
		$(this).html(gestiones);//select
		$(this).selectpicker('refresh');
	});
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
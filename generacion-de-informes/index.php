<?php
  $seccionActiva=3;
  $codigo=8;
  include_once('../cabecera.php');
  
  if($_SESSION['tipoUsuario']=='CLIENTE'){
    $codigo=$_SESSION['codigoU'];
    $datosCliente=datosRegistro('usuarios_clientes',$codigo,'codigoUsuario');
    $codigoCliente=$datosCliente['codigoCliente'];
  } else {
    $codigoCliente=false;
  }
  //$datosCliente=datosRegistro('clientes',$codigoCliente,'empleado');

  $res=operacionesInformes();
  $estadisticas=estadisticasInformes($codigoCliente);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre informes:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-paste"></i> <span class="value"><?php echo $estadisticas; ?></span> <br>Informes generados</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
        </div>
        <!-- /span6 -->

        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-edit"></i>
              <h3>Gestión de Informes</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <?php
                botonesGestion($codigo,2);
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>
		

      <div class="span12">		    
        <div class="widget widget-table action-table">
          <div class="widget-header"> <i class="icon-list"></i>
            <h3>Informes registrados</h3>
          </div>
          <!-- /widget-header -->
          <div class="widget-content">
            <table class="table table-striped table-bordered datatable">
              <thead>
                <tr>
                  <?php if($_SESSION['tipoUsuario']!='CLIENTE'){?>
                  <th> Cliente </th>
                  <?php } ?>
                  <th> Fecha de informe </th>
                  <th> Fecha formulario evaluación </th>
                  <th class="centro"></th>
                  <th><input type='checkbox' id="todo"></th>
                </tr>
              </thead>
              <tbody>

        				<?php
        					imprimeInformes($codigoCliente);
        				?>
              
              </tbody>
            </table>
          </div>
          <!-- /widget-content-->
        </div>
		  


      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap.datatable.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/filtroTabla.js" type="text/javascript"></script>

<script src="<?php echo $_CONFIG['raiz']; ?>../api/js/gestionRegistros.js" type="text/javascript"></script>

<!-- contenido --></div>

<?php include_once('../pie.php'); ?>
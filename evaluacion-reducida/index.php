<?php
  $seccionActiva=3;
  $codigo=44;
  include_once('../cabecera.php');

  operacionesEvaluacionesReducidas();
  $estadisticas=estadisticasEvaluacionesReducidas();
  $botonNuevo='<a href="'.$_CONFIG['raiz'].'evaluacion-reducida/selecciona-clientes.php" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva Evaluación Previa</span> </a>';
  if($_SESSION['tipoUsuario']=='CLIENTE'){
    $cliente=datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');
    $botonNuevo='<a href="'.$_CONFIG['raiz'].'evaluacion-reducida/gestion.php?cliente='.$cliente['codigoCliente'].'" class="shortcut"><i class="shortcut-icon icon-plus-circle"></i><span class="shortcut-label">Nueva Evaluación Previa</span> </a>';
  }
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-bar-chart"></i>
              <h3>Estadísticas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  <h6 class="bigstats">Estadísticas del sistema sobre evaluaciones previas:</h6>
                   <div id="big_stats" class="cf">
                     <div class="stat"> <i class="icon-cogs"></i> <span class="value"><?php echo $estadisticas['total']; ?></span> <br>E. Previas Finalizadas</div>
                      <!-- .stat -->
                   </div>
                </div> <!-- /widget-content -->                
              </div>
            </div>
          </div>
        </div>
          
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-cog"></i>
              <h3>Gestión de Evaluaciones Previas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts">
                <?php
                echo $botonNuevo;
                botonesGestion($codigo,2);
                ?>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
        </div>


      <div class="span12">
    
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Evaluaciones Previas registradas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped table-bordered datatable">
                <thead>
                  <tr>
                    <?php if($_SESSION['tipoUsuario']!='CLIENTE'){?>
                    <th> Cliente </th>
                    <?php } ?>
                    <th> Fecha de evaluación </th>
                    <th> Evaluación realizada </th>                    
                    <th class="td-actions"> </th>
                    <th><input type='checkbox' id='todo'></th>
                  </tr>
                </thead>
                <tbody>

                  <?php
                    
                    imprimeEvaluacionesReducidas();

                  ?>
                
                </tbody>
              </table>
            </div>
            <!-- /widget-content --> 
          </div>


      </div>
    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

</div>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/jquery.dataTables.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filtroTabla.js"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>

<?php include_once('../pie.php'); ?>

<?php
  $seccionActiva=3;
  include_once("../cabecera.php");
  gestionEvaluacionesReducidas();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="../js/funciones25.js" type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
		$('.selectpicker').selectpicker();

		$('#tablaTratamientos').on('change','.selectDeclaracion',function(){
			obtieneFechaDeclaracion($(this));
		});

		comprobar3940();
		$('select[name=pregunta53],select[name=pregunta54]').change(function(){
			comprobar3940();
		});

		if($('#codigo').length==0){
			$('button[type=submit]').unbind();
			$('button[type=submit]').click(function(e){
				e.preventDefault();
				var res='NO';
				for(var i=0;i<14;i++){
					if($('select[name=pregunta'+i+']').val()=='SI'){
						res='SI';
					}
				}
				if(res=='SI'){
					alert('Al haber respondido SI a alguno de los apartados analizados, después del guardado deberás responder a un segundo bloque de preguntas.');
				}
				$('form').submit();
			});
		}
	});

function obtieneFechaDeclaracion(elem){
	var val=elem.val();
	var i=obtieneFilaCampo(elem);
	if(val!='NULL'){
		var consulta=$.post('../listadoAjax.php?include=evaluacion-reducida&funcion=obtieneFechaDeclaracion();',{'codigo':val},function(respuesta){
				//$('#fechaInicio'+i).val(respuesta);
				$('#fechaInicio'+i).datepicker('setValue',respuesta);
		});
	}
}

function comprobar3940(){
	var valor=$('select[name=pregunta53]').val();
	if(valor=='NO'){
		$('select[name=pregunta54]').val('NO PROCEDE').selectpicker('refresh');
	} 
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
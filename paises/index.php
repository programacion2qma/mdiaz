<?php  
    $seccionActiva = 62;
    include_once('../cabecera.php');  
    operacionesPaises();
?> 

<div class="main" id="contenido">
  	<div class="main-inner">
    	<div class="container">
      		<div class="row">		

      			<div class="span12">
        			<div class="widget widget-table action-table">
            			<div class="widget-header"> <i class="icon-list"></i>
              				<h3>Países registrados</h3>
            			</div>
            			<!-- /widget-header -->
            			<div class="widget-content">
              				<table class="table table-striped table-bordered datatable" id="tablaPaises">
                				<thead>
                  					<tr>
                    					<th> Nombre </th>
                    					<th> Medidas </th>
                    					<th class="centro"></th>                    
                  					</tr>
                				</thead>
                				<tbody>
									<?php listadoPaises(); ?>
                				</tbody>
              				</table>
            			</div>
            			<!-- /widget-content-->
          			</div>
      			</div>
	  
			</div>
    		<!-- /container --> 
  		</div>
  		<!-- /main-inner --> 
	</div>
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>
<script type="text/javascript" src="../../api/js/filtroTabla.js"></script>

<script type="text/javascript">
  	// $(document).ready(function(){
    // 	listadoTabla('#tablaPaises','../listadoAjax.php?include=paises&funcion=listadoPaises();');    
  	// });
</script>

<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
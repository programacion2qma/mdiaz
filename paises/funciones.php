<?php
	@include_once('../config.php');//Carga del archivo de configuración
	@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
	@include_once('../funciones.php');//Carga de las funciones globales

	function operacionesPaises(){
	
		$res = true;

		if(isset($_POST['codigo'])){
			$res = actualizaDatos('paises');
		}

		mensajeResultado('nombre',$res,'Pais');
		mensajeResultado('elimina',$res,'Pais', true);
	}

	function gestionPaises(){
	
		operacionesPaises();

		abreVentanaGestion('Gestión de Paises', '?');
	
			$datos = compruebaDatos('paises');

			campoOculto($datos, 'codigoInterno');

			abreColumnaCampos();

				campoTexto('nombre', 'Nombre', $datos);
				campoSelect('medidas', 'Medidas', ['Normales', 'Reforzadas', 'Examen'], ['NORMALES', 'REFORZADAS', 'EXAMEN'], $datos);

			cierraColumnaCampos(true);

		cierraVentanaGestion('index.php',true);
	}


	function listadoPaises() {

		$etiquetas = [
			'NORMALES'   => "<span class='label label-success'>Normales</span>",
			'REFORZADAS' => "<span class='label label-warning'>Reforzadas</span>",
			'EXAMEN'     => "<span class='label label-danger'>Examen</span>"
		];
	
		$query = "SELECT * FROM paises";
	
		conexionBD();
			
		$consulta = consultaBD($query);
	
		while ($datos = mysql_fetch_assoc($consulta)) {
		
			echo "
				<tr>
					<td>".$datos['nombre']."</td>
					<td class='centro'>".$etiquetas[$datos['medidas']]."</td>
					<td class='centro'>".creaBotonDetalles('paises/gestion.php?codigo='.$datos['codigo'],'Editar')."</td>
        		</tr>
			";

			
		}

		cierraBD();		
	}

?>
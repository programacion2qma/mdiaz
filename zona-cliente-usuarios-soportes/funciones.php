<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesUsuarios(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaUsuario();
	}
	elseif(isset($_POST['nombreAutomatizados'])){
		$res=insertaUsuario();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		//$res=eliminaDatos('agrupaciones');
	}

	mensajeResultado('nombreAutomatizados',$res,'Soporte');
    mensajeResultado('elimina',$res,'Soporte', true);
}

function insertaUsuario(){
	$res=true;
	prepararCampos();
	$res=insertaDatos('soportes_automatizados_lopd');
	return $res;
}

function actualizaUsuario(){
	$res=true;
	prepararCampos();
	$res=actualizaDatos('soportes_automatizados_lopd');
	return $res;
}

function prepararCampos(){
	$datos=arrayFormulario();
	$select = $datos['soportesAutomatizados'];
	$text ='';
	if(!empty($select)){
		for($j=0;$j<count($select);$j++){
			if($j > 0){
				$text .= "&$&";
			}
			$text .= $select[$j];
		}
	}
	$_POST['soportesAutomatizados'] = $text;

}


function listadoUsuarios(){
	global $_CONFIG;

	$columnas=array('nombreAutomatizados','cargoAutomatizados','soportesAutomatizados','servicio','nombreAutomatizados','nombreAutomatizados');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();

	
	conexionBD();
	$consulta=consultaBD("SELECT soportes_automatizados_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio, formulario FROM soportes_automatizados_lopd INNER JOIN trabajos ON soportes_automatizados_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT soportes_automatizados_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio, formulario FROM soportes_automatizados_lopd INNER JOIN trabajos ON soportes_automatizados_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$formulario = recogerFormularioServicios($datos);
		$usuario=obtieneUsuario($datos['nombreAutomatizados'],$formulario);
		$soportes=obtieneSoportes($datos['soportesAutomatizados']);
		$cargo=obtieneCargo($datos['nombreAutomatizados'],$formulario,$datos['cargoAutomatizados']);
		$fila=array(
			$usuario,
			$cargo,
			$soportes,
			formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'],
			creaBotonDetalles("zona-cliente-usuarios-soportes/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function imprimeSoportesUsuarios(){
	global $_CONFIG;

	$codigoCliente=obtenerCodigoCliente(true);
	$having="HAVING codigoCliente=".$codigoCliente;

	$consulta=consultaBD("SELECT soportes_automatizados_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio, formulario FROM soportes_automatizados_lopd INNER JOIN trabajos ON soportes_automatizados_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having;",true);
	while($datos=mysql_fetch_assoc($consulta)){
		$formulario = recogerFormularioServicios($datos);
		$usuario=obtieneUsuario($datos['nombreAutomatizados'],$formulario);
		$soportes=obtieneSoportes($datos['soportesAutomatizados']);
		$cargo=obtieneCargo($datos['nombreAutomatizados'],$formulario,$datos['cargoAutomatizados']);
		echo '<tr>';
			echo '<td>'.$usuario.'</td>';
			echo '<td>'.$cargo.'</td>';
			echo '<td>'.$soportes.'</td>';
			echo '<td>'.formateaFechaWeb($datos['fecha']).' - '.$datos['servicio'].'</td>'; 
			echo '<td>'.creaBotonDetalles("zona-cliente-usuarios-soportes/gestion.php?codigo=".$datos['codigo']).'</td>';
			echo '<td><input type="checkbox" name="codigoLista[]" value="'.$datos['codigo'].'"></td>';
		echo '</tr>';
	}
}

function obtieneUsuario($usuario,$formulario){
	$res='';
	$usuario=explode('_', $usuario);
	$tablas=array('USUARIOS'=>'usuarios_lopd','ENCARGADOFIJO'=>'trabajos','ENCARGADO'=>'otros_encargados_lopd');
	if(strtoupper($usuario[0])=='USUARIOS'){
		$usuario=datosRegistro('usuarios_lopd',$usuario[1]);
		$res=$usuario['nombreUsuarioLOPD'];
	} else if(strtoupper($usuario[0])=='ENCARGADOFIJO'){
		$usuario[1]++;
		$res=$formulario['pregunta'.$usuario[1]];
	} else if(strtoupper($usuario[0])=='ENCARGADO'){
		$usuario=datosRegistro('otros_encargados_lopd',$usuario[1]);
		$res=$usuario['nombreEncargado'];
	}
	return $res;
}

function obtieneCargo($usuario,$formulario,$cargo){
	$res='';
	if($cargo==''){
		$usuario=explode('_', $usuario);
		$tablas=array('USUARIOS'=>'usuarios_lopd','ENCARGADOFIJO'=>'trabajos','ENCARGADO'=>'otros_encargados_lopd');
		if(strtoupper($usuario[0])=='USUARIOS'){
			$usuario=datosRegistro('usuarios_lopd',$usuario[1]);
			$res=$usuario['puestoUsuarioLOPD'];
		} else {
			$res=$cargo;
		}
	} else {
		$res=$cargo;
	}
	return $res;
}

function obtieneSoportes($soportes){
	$res='';
	$soportes=explode('&$&', $soportes);
	foreach ($soportes as $key => $value) {
		$item=datosRegistro('soportes_lopd_nueva',$value);
		if($res!=''){
			$res.=',<br/>';
		}
		$res.=$item['nombreSoporteLOPD'];
	}

	return $res;
}


function gestionUsuarios(){
	operacionesUsuarios();

	abreVentanaGestion('Gestión de Usuarios que tienen asignados soportes ','?','','icon-edit','margenAb');
	$datos=compruebaDatos('soportes_automatizados_lopd');

	$consulta=consultaBD("SELECT codigo, nombreFichero FROM declaraciones WHERE codigoCliente='".obtenerCodigoCliente(true)."';",true);
	$opciones=array();
	$valores=array();
    while($datosFicheros=mysql_fetch_assoc($consulta)){
        array_push($opciones, $datosFicheros['nombreFichero']);
        array_push($valores, $datosFicheros['codigo']);        
    }

    if(!$datos){
    	abreColumnaCampos();
    		campoSelectConsulta('codigoTrabajo','Consultoría','SELECT trabajos.codigo, servicios.servicio AS texto FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE codigoCliente='.obtenerCodigoCliente(true),false,'selectpicker span5');
    	cierraColumnaCampos(true);
    } else {
    	campoOculto($datos['codigoTrabajo'],'codigoTrabajo');
    	campoOculto($datos['nombreAutomatizados'],'codigoUsuario');
    }

	abreColumnaCampos();
		campoSelect('nombreAutomatizados','Usuario',array(),array(),$datos);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoTexto('cargoAutomatizados','Cargo',$datos);
		campoTexto('nifAutomatizados','Ref. soporte',$datos,'input-small');
	cierraColumnaCampos();

	abreColumnaCampos();
		campoTexto('accesosAutomatizados','Accesos',$datos);
		campoSelectSoporte($datos);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}


function recogeUsuario(){
	$res='<option value="NULL">&nbsp;</option>';
	$datos=arrayFormulario();
	$consulta=consultaBD('SELECT * FROM usuarios_lopd WHERE codigoTrabajo='.$datos['codigoTrabajo'],true);
	while($item=mysql_fetch_assoc($consulta)){
		$codigo='USUARIOS_'.$item['codigo'];
		$res.='<option value="'.$codigo.'"';
		if($datos['codigoUsuario']==$codigo){
			$res.=' selected';
		}
		$res.='>'.$item['nombreUsuarioLOPD'].'</option>';
	}

	$encargados=array(158,168,178,189,200,210,220,230,240,250,260,270,280,290,487);
	$trabajo=datosRegistro('trabajos',$datos['codigoTrabajo']);
	$formulario=recogerFormularioServicios($trabajo);
	foreach ($encargados as $key => $value) {
		if($formulario['pregunta'.$value]=='SI'){
			$n=$value+1;
			$codigo='ENCARGADOFIJO_'.$value;
			$res.='<option value="'.$codigo.'"';
			if($datos['codigoUsuario']==$codigo){
				$res.=' selected';
			}
			$res.='>'.$formulario['pregunta'.$n].'</option>';
		}
	}
	$consulta=consultaBD('SELECT * FROM otros_encargados_lopd WHERE codigoTrabajo='.$datos['codigoTrabajo'],true);
	while($item=mysql_fetch_assoc($consulta)){
		$codigo='ENCARGADO_'.$item['codigo'];
		$res.='<option value="'.$codigo.'"';
		if($datos['codigoUsuario']==$codigo){
			$res.=' selected';
		}
		$res.='>'.$item['nombreEncargado'].'</option>';
	}

	echo $res;
}

function campoSelectSoporte($datos){
	$nombres=array();
	$valores=array();
	$consulta=consultaBD('SELECT * FROM soportes_lopd_nueva WHERE codigoCliente='.obtenerCodigoCliente(true));
	while($item=mysql_fetch_assoc($consulta)){
		array_push($nombres, $item['nombreSoporteLOPD']);
		array_push($valores, $item['codigo']);
	}
	campoSelectMultiple('soportesAutomatizados','¿En que tipo de soportes?',$nombres,$valores,$datos,'selectpicker span3 show-tick','');
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
<?php
  $seccionActiva=62;
  include_once("../cabecera.php");
  gestionUsuarios();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	recogerUsuarios($('#codigoTrabajo').val());
	$('#codigoTrabajo').change(function(){
		recogerUsuarios($(this).val());
	});
});

function recogerUsuarios(valor){
	if($('#codigoUsuario').length){
		var codigoUsuario=$('#codigoUsuario').val();
	} else {
		var codigoUsuario=0;
	}
	var consulta=$.post('../listadoAjax.php?include=zona-cliente-usuarios-soportes&funcion=recogeUsuario();',{'codigoTrabajo':valor,'codigoUsuario':codigoUsuario});
	consulta.done(function(respuesta){
		$('#nombreAutomatizados').html(respuesta);
		$('#nombreAutomatizados').selectpicker('refresh');
	});
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
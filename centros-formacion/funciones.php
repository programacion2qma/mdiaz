<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de centros

function operacionesCentros(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('centros_formacion');
	}
	elseif(isset($_POST['razonSocial'])){
		$res=insertaDatos('centros_formacion');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('centros_formacion');
	}

	mensajeResultado('razonSocial',$res,'Centro');
    mensajeResultado('elimina',$res,'Centro', true);
}


function listadoCentros(){
	global $_CONFIG;

	$columnas=array('razonSocial','cif','responsable','telefono','localidad','activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, razonSocial, cif, responsable, telefono, localidad, activo FROM centros_formacion $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, razonSocial, cif, responsable, telefono, localidad, activo FROM centros_formacion $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['razonSocial'],
			$datos['cif'],
			$datos['responsable'],
			"<a href='tel:".$datos['telefono']."'>".formateaTelefono($datos['telefono'])."</a>",
			$datos['localidad'],
        	creaBotonDetalles("centros-formacion/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionCentro(){
	operacionesCentros();

	abreVentanaGestion('Gestión de centros de formación','?','span3');
	$datos=compruebaDatos('centros_formacion');

	campoTexto('razonSocial','Razón social',$datos,'span3');
	campoTextoValidador('cif','CIF',$datos,'input-small validaCIF','centros_formacion');
	campoTexto('responsable','Responsable',$datos,'span3');
	campoTextoSimbolo('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small','centros_formacion');
	campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large','centros_formacion');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto('domicilio','Domicilio',$datos,'span3');
	campoTexto('cp','Código Postal',$datos,'input-mini pagination-right');
	campoTexto('localidad','Localidad',$datos);
	campoRadio('activo','Activo',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}

function filtroCentros(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectSiNoFiltro(5,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de centros
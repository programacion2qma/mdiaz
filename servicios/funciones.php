<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesServicios(){
	$res=true;

	if(isset($_POST['codigo'])){
		formateaPrecioBDD('precio');
		$res=actualizaDatos('servicios');
	}
	elseif(isset($_POST['referencia'])){
		formateaPrecioBDD('precio');
		$res=insertaDatos('servicios');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('servicios');
	}

	mensajeResultado('referencia',$res,'Servicio');
    mensajeResultado('elimina',$res,'Servicio', true);
}


function listadoServicios(){
	$columnas=array('referencia','servicio','precio','servicios.activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListadoServicios($columnas);//Uso de función personalizada
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT servicios.codigo, referencia, servicio, precio, servicios.activo FROM servicios $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT servicios.codigo, referencia, servicio, precio, servicios.activo FROM servicios $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['referencia'],
			$datos['servicio'],
			"<div class='pagination-right'>".formateaNumeroWeb($datos['precio']).' €</div>',
			creaBotonDetalles("servicios/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

//Esta función utiliza un "orden natural" (primero por longitud, luego por el propio valor) para la primera columna del listado de servicios (que es mayoritariamente numérica)
function obtieneOrdenListadoServicios($columnas){
	$orden = '';
    if(isset($_GET['iSortCol_0'])) {
        $orden = 'ORDER BY  ';
        for ($i=0;$i<(int)$_GET['iSortingCols'];$i++) {
            if($_GET['bSortable_'.(int)$_GET['iSortCol_'.$i]]=='true'){
                $indice=(int)$_GET['iSortCol_'.$i];

                if($indice==0 && $_GET['sSortDir_'.$i]==='asc'){//Si se trata de la primera columna y el orden es ascendente...
                	$orden.="LENGTH(referencia), referencia, ";//.. ordeno por longitud y luego valor
                }
                elseif($indice==0 && $_GET['sSortDir_'.$i]!=='asc'){//Lo mismo si el orden es descendente
                	$orden.="LENGTH(referencia) DESC, referencia DESC, ";
                }
                else{//Orden normal
                	$orden.=$columnas[$indice].' '.($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
                }
            }
        }

        $orden = substr_replace($orden, '', -2);
        if ($orden == 'ORDER BY') {
            $orden = '';
        }
    }
    return $orden;
}


function gestionServicio(){
	operacionesServicios();

	abreVentanaGestion('Gestión de Servicios','?','span3','icon-edit','margenAb');
	$datos=compruebaDatos('servicios');

	campoTexto('referencia','Referencia',$datos,'input-small');
	areaTexto('servicio','Servicio',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();
	campoSelectConsulta('codigoFamilia','Familia',"SELECT codigo, CONCAT(referencia,' - ',nombre) AS texto FROM servicios_familias ORDER BY referencia;",$datos);
	campoTextoSimbolo('precio','Precio','€',formateaNumeroWeb($datos['precio']));
	campoRadio('activo','Activo',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}

function filtroServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Referencia');
	campoTexto(1,'Servicio','','span3');
	campoTextoSimbolo(2,'Precio','€');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(3,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de servicios
<?php

    include_once('funciones.php');
    compruebaSesion();

    include_once('../../api/js/firma/signature-to-image.php');
    require_once('../../api/phpword/PHPWord.php');
    require_once('../../api/html2pdf/html2pdf.class.php');

    $nombreFichero=wordInteresadoDerechos(new PHPWord(),$_GET['codigo']);

    header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header("Content-Disposition: attachment; filename=".$nombreFichero);
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/derechos/respuesta.docx');
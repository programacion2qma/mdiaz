<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesDerechos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDerecho();
	}
	elseif(isset($_POST['interesado'])){
		$res=insertaDerecho();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('derechos');
	}

	mensajeResultado('interesado',$res,'Derecho');
    mensajeResultado('elimina',$res,'Derecho', true);
}

function insertaDerecho(){
	$res=true;
	responsable();
	$res=insertaDatos('derechos');
	return $res;
}

function actualizaDerecho(){
	$res=true;
	responsable();
	$res=actualizaDatos('derechos');
	return $res;
}

function responsable(){
	$_POST['responsable']='';
	$_POST['responsableDireccion']='';
	if($_POST['respuesta']==4 && $_POST['selectResponsable']!='NULL'){
		$i=$_POST['selectResponsable'];
		$_POST['responsable']=$_POST['responsable'.$i];
		$_POST['responsableDireccion']=$_POST['direccion'.$i];
	}
}

function listadoDerechos(){
	global $_CONFIG;

	$columnas=array('interesado','fechaRecepcion','fechaRespuesta','respuesta','tipo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente." AND tipo='Acceso'",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having;");
	cierraBD();
	$respuestas=array('',
	'La solicitud no reúne los requisitos ',
	'No figuran datos personales del interesado',
	'Procede la visualización interesada',
	'Comunicación al Responsable de Tratamientos',
	'Datos personales bloqueados',
	'Denegación de acceso',
	'Otorgamiento de acceso');
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$otrosDatos="";
		if($datos['respuesta']==7 && $datos['otrosDatos']!=''){
			$otrosDatos=' - '.$datos['otrosDatos'];
		}
		$fila=array(
			$datos['referencia'],
			$datos['interesado'],
			formateaFechaWeb($datos['fechaRecepcion']),
			formateaFechaWeb($datos['fechaRespuesta']),
			$respuestas[$datos['respuesta']].$otrosDatos,
			botonAcciones(array('Detalles','Descargar respuesta'),array("zona-cliente-derechos-acceso/gestion.php?codigo=".$datos['codigo'],"zona-cliente-derechos-acceso/generaRespuesta.php?codigo=".$datos['codigo']),array('icon-search-plus','icon-cloud-download')),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionDerechos(){
	operacionesDerechos();

	abreVentanaGestion('Gestión de interesados que ejercen el derecho de acceso','?','','icon-edit','margenAb');
	$datos=compruebaDatos('derechos');

    if(!$datos){
    	$codigoCliente=obtenerCodigoCliente(true);
    	$referencia=obtieneReferenciaDerecho('Acceso',$codigoCliente);
    	campoOculto($codigoCliente,'codigoCliente');
    }  else {
    	$codigoCliente=$datos['codigoCliente'];
    	$referencia=$datos['referencia'];
    }

    campoOculto('','nombreCesionario');

	abreColumnaCampos();
		$valores=array('','Acceso','Rectificación','Supresión o cancelación','Oposición y decisiones individuales automatizadas','Limitación del tratamiento','Portabilidad de los datos');
		campoOculto('Acceso','tipo');
		campoTexto('referencia','Referencia',$referencia,'input-mini',true);
   		campoTexto('interesado','Nombre y apellido del interesado',$datos,'span7');
   		campoTexto('interesadoDni','NIF',$datos,'input-small');
   		campoTexto('representante','Representante legal',$datos,'span7');
   		campoTexto('representanteDni','NIF',$datos,'input-small');
   		campoTexto('direccion','Dirección a efecto de notificaciones',$datos,'span7');
   		campoTexto('email','Correo electrónico',$datos,'span7');
   	cierraColumnaCampos(true);
   	abreColumnaCampos();
		campoFecha('fechaRecepcion','Fecha de Recepción',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoFecha('fechaRespuesta','Fecha de Respuesta',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		campoRadio('mediosElectronicos','La solicitud se ha presentado por medios electrónicos',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		echo '<div id="divMediosDiferente" class="hide">';
			campoRadio('mediosDiferente','El interesado solicita que se le responda por otros medios diferentes al correo electrónico',$datos);
		echo '</div>';
	cierraColumnaCampos();
	abreColumnaCampos();
		echo '<div id="divOtroMedio" class="hide">';
			campoTexto('otroMedio','Indicar',$datos);
		echo '</div>';
	cierraColumnaCampos(true);
	abreColumnaCampos();
		$select=rellenaSelect();
		campoSelect('respuesta','Respuesta',$select['nombres'],$select['valores'],$datos,'selectpicker span6 show-tick');
	cierraColumnaCampos(true);
	$responsables=array();
	$direcciones=array();
	conexionBD();
	$declaraciones=consultaBD('SELECT * FROM declaraciones WHERE codigoCliente='.$codigoCliente);
	while($d=mysql_fetch_assoc($declaraciones)){
		if(!in_array($d['n_razon'],$responsables)){
			array_push($responsables,$d['n_razon']);
			array_push($direcciones,$d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$d['provincia_responsableFichero']);
		}
		$otros=consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion='.$d['codigo']);
		while($o=mysql_fetch_assoc($otros)){
			if($o['razonSocial']!='' && !in_array($o['razonSocial'],$responsables)){
				array_push($responsables,$o['razonSocial']);
				array_push($direcciones,$o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$o['provincia']);
			}
		}
	}
	cierraBD();
	abreColumnaCampos('span3 divResponsable hide');
		$nombres=array('');
		$valores=array('NULL');
		foreach ($responsables as $key => $value) {
			campoOculto($value,'responsable'.$key);
			array_push($nombres,$value);
			array_push($valores,$key);
		}
		foreach ($direcciones as $key => $value) {
			campoOculto($value,'direccion'.$key);
		}
		if($datos && $datos['responsable']!=''){
			campoDato('Responsable de tratamiento',$datos['responsable']);
		}
		campoSelect('selectResponsable','Selecciona responsable',$nombres,$valores);
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divOtrosDatos hide');
		areaTexto('otrosDatos','Información a facilitar al interesado –
<a id="btnModal" class="noAjax">artículo 15 Reglamento (UE) 2016/679</a>-',$datos); 
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divTratamiento hide');
		campoSelectConsulta('codigoTratamiento','Fichero/Tratamiento','SELECT codigo, nombreFichero AS texto FROM declaraciones WHERE codigoCliente='.$codigoCliente,$datos);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoRadio('visualizacion','Visualización',$datos,'NO',array('No procede','Procede'),array('NO','SI'));
		echo '<div id="divProcede" class="hide">';
			campoFecha('fechaVisualizacion','Fecha visualización',$datos);
			campoHora('horaVisualizacion','Hora visualización',$datos);
		echo '</div>';
	cierraColumnaCampos(true);

	abreColumnaCampos();
		areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 
	cierraColumnaCampos(true);

	cierraVentanaGestion('index.php',true);

	abreVentanaModal('información','cajaGestion');
	echo '<i><b>Artículo 15 Reglamento (UE) 2016/679: Derecho de acceso del interesado </b><br/><br/>
	1. El interesado tendrá derecho a obtener del responsable del tratamiento confirmación de si se están tratando o no datos personales que le  conciernen y, en tal caso, derecho de acceso a los datos personales y a la siguiente información: <br/><br/>
		a) los fines del tratamiento; <br/>
		b) las categorías de datos personales de que se trate; <br/>
		c) los destinatarios o las categorías de destinatarios a los que se comunicaron o serán comunicados los datos personales, en particular destinatarios en terceros u organizaciones internacionales; <br/>
		d) de ser posible, el plazo previsto de conservación de los datos personales o, de no ser posible, los criterios utilizados para determinar este plazo; <br/>
		e)  la  existencia  del  derecho  a  solicitar  del  responsable  la  rectificación  o  supresión  de datos  personales  o  la  limitación  del  tratamiento  de  datos  personales  relativos  al interesado, o a oponerse a dicho tratamiento; <br/>
		f) el derecho a presentar una reclamación ante una autoridad de control; <br/>
		g) cuando los datos personales no se hayan obtenido del interesado, cualquier información disponible sobre su origen; <br/>
		h) la existencia de decisiones automatizadas, incluida la elaboración de perfiles, a que se  refiere el  artículo  22,  apartados  1  y  4,  y,  al  menos  en  tales  casos,  información significativa  sobre  la  lógica  aplicada,  así  como  la  importancia  y  las  consecuencias previstas de dicho tratamiento para el interesado. <br/><br/>
	2.  Cuando  se  transfieran  datos  personales  a  un  tercer  país  o  a  una  organización internacional, el interesado tendrá derecho a ser informado de las garantías adecuadas en virtud del artículo 46 relativas a la transferencia. <br/><br/>
	3. El responsable del tratamiento facilitará una copia de los datos personales objeto de tratamiento.  El  responsable  podrá  percibir  por  cualquier  otra  copia  solicitada  por  el interesado un canon razonable basado en los costes administrativos. Cuando el interesado presente  la  solicitud  por  medios  electrónicos,  y  a  menos  que  este  solicite que se  facilite  de otro modo, la información se facilitará en un formato electrónico de uso común. <br/><br/>
	4. El derecho a obtener copia mencionado en el apartado 3 no afectará negativamente a los derechos y libertades de otros.</i>';
	cierraVentanaModal('','','',false,'Cerrar');
}

function rellenaSelect(){
	$res=array('valores'=>array(0,1,2,3,4,5,6,7),'nombres'=>array('',
	'La solicitud no reúne los requisitos ',
	'No figuran datos personales del interesado',
	'Procede la visualización interesada',
	'Comunicación al Responsable de Tratamientos',
	'Datos personales bloqueados',
	'Denegación de acceso',
	'Otorgamiento de acceso'));
	return $res;
}

function wordInteresadoDerechos($PHPWord,$codigoDerecho){
    global $_CONFIG;
    $datos=consultaBD('SELECT derechos.*, clientes.razonSocial, CONCAT(clientes.domicilio,", CP ",clientes.cp,", ",clientes.localidad,", ",clientes.provincia) AS direccionCliente, ficheroLogo, administrador FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo WHERE derechos.codigo='.$codigoDerecho,true,true);
    $tipo=$datos['tipo'];
    $ref=$datos['referencia']<10?'0'.$datos['referencia']:$datos['referencia'];
    $fichero='RESPUESTA_DERECHO_'.mb_strtoupper($tipo).'_INTERESADO_REF_'.$ref.'.docx';
    $documento=$PHPWord->loadTemplate('../documentos/derechos/plantillaDerechos.docx');
    $documento->setValue("true","");
    $documento->setValue("cliente",utf8_decode(sanearCaracteresDerechos($datos['razonSocial'])));  
    $documento->setValue("titulo",utf8_decode("RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO ".$datos['interesado']." REF. ".$ref));

    reemplazarLogoDerechos($documento,$datos,'image2.png'); 
    $respuestas=array('','LA SOLICITUD NO REÚNE LOS REQUISITOS NECESARIOS','NO FIGURAN DATOS PERSONALES DEL INTERESADO EN LOS FICHEROS DEL RESPONSABLE','VISUALIZACIÓN EN PANTALLA','COMUNICACIÓN AL RESPONSABLE DE TRATAMIENTOS ','DATOS PERSONALES BLOQUEADOS','DENEGACIÓN DE ACCESO','OTORGAMIENTO DE ACCESO');
    if($datos['respuesta']==4){
        $texto="RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": ".$respuestas[$datos['respuesta']]."<w:br/><w:br/>".$datos['razonSocial'].", en concepto de encargado del tratamiento de datos personales, de los cuales es responsable del tratamiento ".$datos['responsable'].", mediante el presente escrito es para dar respuesta a la petición de acceso de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['responsableDireccion']."<w:br/><w:br/>";
    } else {
        $texto="RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": ".$respuestas[$datos['respuesta']]."<w:br/><w:br/>El presente escrito es para dar respuesta a la petición de acceso de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['direccion']."<w:br/><w:br/>";
        
    }
    if($datos['representante']!=''){
        $texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    } else {
        $texto.=$datos['interesado'];
    }
    $texto.=$direccion;
    switch($datos['respuesta']){
        case 1:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que su solicitud no reúne los requisitos necesarios para el cumplimiento de lo establecido por el Reglamento General de Protección de Datos (2016/679 UE) –Considerando 64, artículos 11.2  y  12.2-, así como por la Ley Orgánica 3/2018, de 5 de diciembre de Protección de Datos Personales y Garantía de los Derechos Digitales –artículos 11, 12 y 13-:<w:br/><w:br/>1. Nombre y apellidos del interesado; fotocopia de su documento nacional de identidad, o de su pasaporte u otro documento válido que lo identifique y, en su caso, de la persona que lo  represente, o instrumentos electrónicos equivalentes; así como el documento o instrumento electrónico acreditativo de tal representación. La utilización de firma electrónica identificativa del afectado eximirá de la presentación de las fotocopias del DNI o documento equivalente.<w:br/>2. Petición en que se concreta la solicitud.<w:br/>3. Dirección a efectos de notificaciones, fecha y firma del solicitante. <w:br/>4. Documentos acreditativos de la petición que formula, en su caso";
        break;

        case 2:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>No figuran datos personales del interesado/a en nuestros sistemas de tratamiento de datos.";
        break;

        case 3:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>Para proceder a la visualización de datos en pantalla solicitada, deberá personarse en nuestras  dependencias, sitas en ".$datos['direccionCliente'].", el día ".formateaFechaWeb($datos['fechaVisualizacion'])." a las ".formateaHoraWeb($datos['horaVisualizacion'])." horas";

        case 4:
        $texto.="Se adjunta copia de la solicitud de acceso recibida, a fin de que el responsable del tratamiento resuelva sobre la misma. ";
        break;

        case 5:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>Le deniega la solicitud de rectificación planteada, en virtud del artículo 12.5 del Reglamento (UE) 2016/679, que establece lo siguiente:<w:br/><w:br/>Artículo 12.5 Reglamento (UE) 2016/679. Transparencia de la información, comunicación y modalidades de ejercicio de los derechos del interesado: \"5. La información facilitada en virtud de los artículos 13 y 14 así como toda comunicación y cualquier actuación realizada en virtud de los artículos 15 a 22 y 34 serán a título gratuito. Cuando las solicitudes sean manifiestamente infundadas o excesivas, especialmente debido a su carácter repetitivo, el responsable del tratamiento podrá:<w:br/><w:br/>a) cobrar un canon razonable en función de los costes administrativos afrontados para facilitar la información o la comunicación o realizar la actuación solicitada, o<w:br/>b) negarse a actuar respecto de la solicitud.<w:br/>El responsable del tratamiento soportará la carga de demostrar el carácter manifiestamente infundado o excesivo de la solicitud.\"";
        break;

        case 6:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/><w:br/>Artículo 12.5 Reglamento (UE) 2016/679. Transparencia de la información, comunicación y modalidades de ejercicio de los derechos del interesado: \"5. La información facilitada en virtud de los artículos 13 y 14 así como toda comunicación y cualquier actuación realizada en virtud de los artículos 15 a 22 y 34 serán a título gratuito. Cuando las solicitudes sean manifiestamente infundadas o excesivas, especialmente debido a su carácter repetitivo, el responsable del tratamiento podrá:<w:br/><w:br/>a) cobrar un canon razonable en función de los costes administrativos afrontados para facilitar la información o la comunicación o realizar la actuación solicitada, o<w:br/><w:br/>b) negarse a actuar respecto de la solicitud.<w:br/><w:br/>El responsable del tratamiento soportará la carga de demostrar el carácter manifiestamente infundado o excesivo de la solicitud.\"<w:br/><w:br/>Artículo 13 LOPD-GDD. Derecho de acceso: \"3. A los efectos establecidos en el artículo 12.5 del Reglamento (UE) 2016/679 se podrá considerar repetitivo el ejercicio del derecho de acceso en más de una ocasión durante el plazo de seis meses, a menos que exista causa legítima para ello.\"";
        break;

        case 7:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le confirma que está tratando datos personales que conciernen al interesado/a, informándole a continuación de los extremos establecidos por el artículo 15 del Reglamento (UE) 2016/679: ".$datos['otrosDatos'];
        break;
    }
    $texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones']."<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];

    if($datos['respuesta']==4){
    	$texto.="<w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/>RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": COMUNICACIÓN AL INTERESADO EN CONCEPTO DE ENCARGADO/ DEL TRATAMIENTO<w:br/><w:br/>El presente escrito es para dar respuesta a la petición de acceso de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['direccion']."<w:br/><w:br/>";
        if($datos['representante']!=''){
        	$texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    	} else {
        	$texto.=$datos['interesado'];
    	}
    	$texto.=$direccion;
    	$texto.=$datos['razonSocial']." le informa que:<w:br/><w:br/>1º. El tratamiento que realiza de los datos personales objeto del derecho de acceso interesado por el interesado referenciado en el presente escrito, lo lleva a cabo en concepto de encargado del tratamiento, por lo que deberá ejercitar su derecho de acceso ante el Responsable del Tratamiento ".$datos['direccion']." con dirección en ".$datos['responsableDireccion'].".<w:br/><w:br/>2º. Se ha procedido a trasladar a dicho Responsable del Tratamiento la solicitud de acceso objeto del presente documento.";
    	$texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
    	$texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];
    }

    $documento->setValue("texto",utf8_decode($texto));

    $documento->save('../documentos/derechos/respuesta.docx');
    return $fichero;
}
//Fin parte de agrupaciones
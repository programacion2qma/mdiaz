<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de categorías

function generaDatosOrganigrama(){
	$res="{id: 1, name: 'LA ACADEMIA EMPRESAS', parent: 0},";
        
    $consulta=consultaBD("SELECT * FROM categorias_agentes",true);

    while($datos=mysql_fetch_assoc($consulta)){
    	$res.="{id:".$datos['codigo'].", name:'".$datos['categoria']."', parent:";
    	if($datos['codigoCategoriaSuperior']==NULL){
    		$res.="1";
    	}
    	else{
    		$res.=$datos['codigoCategoriaSuperior'];
    	}

    	$res.="},";
    }

    $res=quitaUltimaComa($res,1);

    echo $res;
}


function creaActualizaCategoria(){
	$res='fallo';

	$datos=arrayFormulario();

	conexionBD();

	$consulta=consultaBD("SELECT * FROM categorias_agentes WHERE codigo=".$datos['codigo']);
	if(mysql_num_rows($consulta)>0){
		$consulta=consultaBD("UPDATE categorias_agentes SET categoria='".$datos['nombre']."' WHERE codigo=".$datos['codigo']);
	}
	else{
		if($datos['codigoSuperior']==1){
			$datos['codigoSuperior']='NULL';
		}

		$consulta=consultaBD("INSERT INTO categorias_agentes VALUES(".$datos['codigo'].",'".$datos['nombre']."',".$datos['codigoSuperior'].", 'SI');");
	}

	cierraBD();

	if($consulta){
		$res='ok';
	}

	echo $res;
}

function eliminaCategoria(){
	$res='fallo';
	$codigo=$_POST['codigo'];

	$consulta=consultaBD("DELETE FROM categorias_agentes WHERE codigo=$codigo",true);
	if($consulta){
		$res='ok';
	}

	echo $res;
}

//Fin parte de categorías
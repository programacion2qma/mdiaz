<?php
  $seccionActiva=17;
  include_once('../cabecera.php');
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container ancho100">

          <div class="widget">
            <div class="widget-header"> <i class="icon-sitemap"></i>
              <h3>Categorías de La Academia Empresas</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                  
                  <div id="organigrama" class="animated fadeInUp"></div>
                  
                </div> <!-- /widget-content -->
                <!-- /widget-content --> 
              </div>
            </div>
          </div>
        
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../js/jquery.orgchart.js"></script>

<script type="text/javascript">
var categorias=[
      <?php generaDatosOrganigrama(); ?>
];

$(document).ready(function(){
  var organigrama=$('#organigrama').orgChart({
      data: categorias,
      showControls: true,
      allowEdit: true,
      onAddNode: function(node){ 
          organigrama.newNode(node.data.id);
      },
      onDeleteNode: function(node){
          if(confirm('¿Está seguro/a de que desea eliminar la categoría seleccionada?')){
            organigrama.deleteNode(node.data.id);
            eliminaCategoria(node.data.id);
          }
      },
      onEditNode: function(node){
        creaActualizaCategoria(node.data.id,node.data.name,node.data.parent);
      }
  });
});

function creaActualizaCategoria(codigo,nombre,codigoSuperior){
  var creacion=$.post('../listadoAjax.php?include=categorias&funcion=creaActualizaCategoria();',{'codigo':codigo,'nombre':nombre,'codigoSuperior':codigoSuperior});
  creacion.done(function(respuesta){
    if(respuesta=='fallo'){
      alert('Se ha producido un error al crear/actualizar la categoría. Por favor, inténtelo de nuevo.');
    }
  });
}

function eliminaCategoria(codigo){
  var creacion=$.post('../listadoAjax.php?include=categorias&funcion=eliminaCategoria();',{'codigo':codigo});
  creacion.done(function(respuesta){
    if(respuesta=='fallo'){
      alert('Se ha producido un error al eliminar la categoría. Por favor, inténtelo de nuevo.');
    }
  });
}
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
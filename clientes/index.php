<?php
  $seccionActiva=16;
  include_once('../cabecera.php');
  
  operacionesClientes();
  creaVentanaImportarFichero();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <?php
        creaBotonesGestionCliente();
      ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Clientes registrados</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroClientes();
              ?>
              <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaClientes">
                <thead>
                  <tr>
                    <th class='nowrap'> Razón Social </th>
                    <th> CIF/NIF </th>
                    <th class='nowrap'> Colaborador </th>
                    <th> Convenio </th>
                    <th> Provincia </th>
                    <th> Teléfono </th>
                    <th> eMail </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaClientes','../listadoAjax.php?include=clientes&funcion=listadoClientes();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaClientes');

    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

    $('#cajaFichero form').attr('action','../informes-clientes/index.php')
    $('#importaFichero').attr('data-dismiss','modal')
    $('#importaFichero').click(function(){
      $('#cajaFichero form').submit();
    });
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
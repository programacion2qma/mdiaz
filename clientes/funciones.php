<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de clientes

function operacionesClientes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaCliente();
	}
	elseif(isset($_POST['razonSocial'])){
		$res=creaCliente();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaClientes();
	}

	mensajeResultado('razonSocial',$res,'Cliente');
    mensajeResultado('elimina',$res,'Cliente', true);
}

function creaCliente(){

	$res=insertaDatos('clientes',time(),'../documentos/logos-clientes/');
	$codigoCliente=$res;
	$datos=arrayFormulario();
	$datos['codigo']=$codigoCliente;
	$res=actualizaUsuario($datos);
	$res=$res && creaPBC($datos,$codigoCliente);
	$res=$res && creaAccionComercial($datos,$codigoCliente);

	return $res;
}

function actualizaCliente(){


	$res=actualizaDatos('clientes',time(),'../documentos/logos-clientes/');
	$datos=arrayFormulario();
	$res=actualizaUsuario($datos);
	$res=$res && creaPBC($datos,$_POST['codigo']);
	$res=actualizaMantenimiento($datos);
	$res=actualizaTomasDeDatos($_POST['codigo']);
	return $res;
}

function actualizaTomasDeDatos($codigo){
	$res=true;
	$campos=array('pregunta3'=>'razonSocial','pregunta4'=>'domicilio','pregunta5'=>'localidad','pregunta6'=>'telefono','pregunta7'=>'email','pregunta8'=>'administrador','pregunta9'=>'cif','pregunta10'=>'cp','pregunta11'=>'provincia','pregunta12'=>'fax','pregunta13'=>'actividad','pregunta14'=>'nifAdministrador','pregunta627'=>'apellido1','pregunta628'=>'apellido2');
	$datos=arrayFormulario();
	$trabajos=consultaBD('SELECT codigo,formulario FROM trabajos WHERE codigoCliente='.$codigo,true);
	while($t=mysql_fetch_assoc($trabajos)){
		$query='';
		$formulario=recogerFormularioServicios($t);
		foreach ($formulario as $key => $value) {
			if($query!=''){
				$query.='&{}&';
			}
			if(array_key_exists($key,$campos)){
				$query.=$key.'=>'.$datos[$campos[$key]];
			} else {
				$query.=$key.'=>'.$value;
			}
		}
		$res=consultaBD('UPDATE trabajos SET formulario="'.$query.'" WHERE codigo='.$t['codigo'],true);
	} 
	return $res;
}

function creaUsuario($datos,$codigoCliente){
	conexionBD();
		$res=consultaBD("INSERT INTO usuarios values(NULL,'".$datos['administrador']."','".$datos['apellido1']." ".$datos['apellido2']."','".$datos['nifAdministrador']."','".$datos['email']."','".$datos['telefono']."','','".$datos['usuario']."','".$datos['clave']."','SI','CLIENTE','');");
		$codigoUsuario=mysql_insert_id();
		$res=consultaBD("INSERT INTO usuarios_clientes values(NULL,".$codigoCliente.",".$codigoUsuario.");");
	cierraBD();
	return $res;
}

function actualizaUsuario($datos){
	$res=true;
	$usuario=consultaBD('SELECT usuarios.codigo FROM usuarios_clientes INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo WHERE usuarios_clientes.codigoCliente='.$datos['codigo'],true,true);
	if($usuario){
		$res=consultaBD('UPDATE usuarios SET nombre="'.$datos['administrador'].'",dni="'.$datos['nifAdministrador'].'",email="'.$datos['email'].'",telefono="'.$datos['telefono'].'",usuario="'.$datos['usuario'].'",clave="'.$datos['clave'].'",apellidos="'.$datos['apellido1'].' '.$datos['apellido2'].'" WHERE codigo='.$usuario['codigo'],true);
	} else {
		$res=$res && creaUsuario($datos,$datos['codigo']);
	}
	return $res;
}

function actualizaMantenimiento($datos){
	$res=true;
	if(isset($datos['codigoMantenimiento'])){
		$res=consultaBD('UPDATE clientes_mantenimiento SET fecha3meses="'.$datos['fecha3meses'].'",fecha7meses="'.$datos['fecha7meses'].'",fecha11meses="'.$datos['fecha11meses'].'" WHERE codigo='.$datos['codigoMantenimiento'],true);
		$res=consultaBD('UPDATE tareas SET fechaInicio="'.$datos['fecha3meses'].'",fechaFin="'.$datos['fecha3meses'].'
			",codigoUsuario="'.$datos['codigoConsultor'].'" WHERE codigo='.$datos['tarea3meses'],true);
		$res=consultaBD('UPDATE tareas SET fechaInicio="'.$datos['fecha7meses'].'",fechaFin="'.$datos['fecha7meses'].'",codigoUsuario="'.$datos['codigoConsultor'].'" WHERE codigo='.$datos['tarea7meses'],true);
		$res=consultaBD('UPDATE tareas SET fechaInicio="'.$datos['fecha11meses'].'",fechaFin="'.$datos['fecha11meses'].'",codigoUsuario="'.$datos['codigoConsultor'].'" WHERE codigo='.$datos['tarea11meses'],true);
	}
	return $res;
}


function compruebaCamposRadioClientes(){
	$_POST['procedeDeListado']=compruebaCampoRadio('procedeDeListado');
	$_POST['llamado']=compruebaCampoRadio('llamado');
	$_POST['visita']=compruebaCampoRadio('visita');
	$_POST['resultadoVisita']=compruebaCampoRadio('resultadoVisita');
	$_POST['resultadoPreventa']=compruebaCampoRadio('resultadoPreventa');
}

function insertaCuentasSSCliente($datos,$codigoCliente,$actualizacion=false){
	$res=eliminaDatosCliente('cuentas_ss_clientes',$codigoCliente,$actualizacion);

	for($i=0;isset($datos['cuentaSS'.$i]);$i++){
		$cuentaSS=$datos['cuentaSS'.$i];

		$res=$res && consultaBD("INSERT INTO cuentas_ss_clientes VALUES(NULL,'$cuentaSS',$codigoCliente);");
	}

	return $res;
}

function insertaCuentasCliente($datos,$codigoCliente,$actualizacion=false){
	//Los registros de cuentas_cliente no se pueden eliminar al actualizar de forma normal, pues están vinculados con la facturación
	//$res=eliminaDatosCliente('cuentas_cliente',$codigoCliente,$actualizacion);

	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos["codigoCuenta$i"]) || isset($datos["ccc$i"]);$i++){
		if(isset($datos['codigoCuenta'.$i]) && isset($datos['ccc'.$i])){//Actualización
			$res=$res && consultaBD("UPDATE cuentas_cliente SET ccc='".$datos["ccc$i"]."', iban='".$datos["iban$i"]."', bic='".$datos["bic$i"]."' WHERE codigo='".$datos['codigoCuenta'.$i]."';");
		}
		elseif(!isset($datos['codigoCuenta'.$i]) && isset($datos['ccc'.$i])){//Inserción
			$res=$res && consultaBD("INSERT INTO cuentas_cliente VALUES(NULL,'".$datos['ccc'.$i]."',
			'".$datos['iban'.$i]."','".$datos['bic'.$i]."',$codigoCliente)");
		}
		else{//Eliminación
			$res=$res && consultaBD("DELETE FROM cuentas_cliente WHERE codigo='".$datos['codigoCuenta'.$i]."';");
		}
	}

	cierraBD();

	return $res;
}

function creaPBC($datos,$cliente){
	$res=true;
	$trabajo=consultaBD('SELECT trabajos.* FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo WHERE codigoCliente='.$cliente,true,true);
	if($trabajo){
		$res=actualizaPBC($datos,$trabajo);
	} else {
		//$res=insertaPBC($datos,$cliente);
	}
	return $res;
}

function insertaPBC($datos,$cliente){
	$res=true;
	conexionBD();
	$fecha=date('Y-m-d');
	$fechaRevision = obtieneFechaLimiteTrabajoVenta($fecha,'P21D');
    $fechaPrevista = obtieneFechaLimiteTrabajoVenta($fechaRevision,'P51D');
    $fechaTomaDatos = obtieneFechaLimiteTrabajoVenta($fecha,'P14D');
    $fechaEnvio = obtieneFechaLimiteTrabajoVenta($fecha,'P28D');
    $fechaMantenimiento = obtieneFechaLimiteTrabajoVenta($fecha,'P11M');
    $servicio=obtenerCodigoServicio($datos);
	$res = consultaBD("INSERT INTO trabajos VALUES(NULL,NULL,'".$fecha."','".$fechaPrevista."',".$cliente.",".$servicio.",'','NO','NO','NO','NO','NO','NO','".$fechaTomaDatos."','".$fechaRevision."','".$fechaRevision."','".$fechaEnvio."','".$fechaEnvio."','".$fechaMantenimiento."','SI','NO','');");
	$trabajo=mysql_insert_id();
	if($datos['pblc']=='SI'){
		$persona=isset($_POST['checkAutonomo']) ? 'FISICA':'JURIDICA';
		$res = consultaBD("INSERT INTO manuales_pbc(codigo,codigoTrabajo,fechaInicio,fechaFin,sujeto,actividad,servicios,empleados, volumen,tomo,folio,hoja,libro,seccion,representante,nifRepresentante) VALUES (NULL,".$trabajo.",'".$datos['fechaInicio']."','".$datos['fechaFin']."','".$persona."','".$datos['actividad']."','".$datos['servicios']."','".$datos['empleados']."','".$datos['volumen']."','".$datos['tomo']."','".$datos['folio']."','".$datos['hoja']."','".$datos['libro']."','".$datos['seccion']."','".$datos['administrador']." ".$datos['apellido1']." ".$datos['apellido2']."','".$datos['nifAdministrador']."')",true);
	}
	cierraBD();
	return $res;
}

function actualizaPBC($datos,$trabajo){
	$servicio=obtenerCodigoServicio($datos);
	$res=true;
	$res=consultaBD("UPDATE trabajos SET codigoServicio='".$servicio."' WHERE codigo=".$trabajo['codigo'],true);
		$persona=isset($_POST['checkAutonomo']) ? 'FISICA':'JURIDICA';
		if($datos['pblc']=='SI'){
			$manual=datosRegistro('manuales_pbc',$trabajo['codigo'],'codigoTrabajo');
			if($manual){
				$res = consultaBD("UPDATE manuales_pbc SET sujeto = '".$persona."' ,actividad = '".$datos['actividad']."',servicios = '".$datos['servicios']."',empleados='".$datos['empleados']."', volumen='".$datos['volumen']."',tomo='".$datos['tomo']."',folio='".$datos['folio']."',hoja='".$datos['hoja']."',libro='".$datos['libro']."',seccion='".$datos['seccion']."',representante='".$datos['administrador']." ".$datos['apellido1']." ".$datos['apellido2']."',nifRepresentante='".$datos['nifAdministrador']."',fechaInicio='".$datos['fechaInicio']."',fechaFin='".$datos['fechaFin']."' WHERE codigoTrabajo=".$trabajo['codigo'],true);
				
			} else {
				$fecha=date('Y-m-d');
				$res = consultaBD("INSERT INTO manuales_pbc(codigo,codigoTrabajo,fechaInicio,fechaFin,sujeto,actividad,servicios,empleados, volumen,tomo,folio,hoja,libro,seccion,representante,nifRepresentante) VALUES (NULL,".$trabajo['codigo'].",'".$datos['fechaInicio']."','".$datos['fechaFin']."','".$persona."','".$datos['actividad']."','".$datos['servicios']."','".$datos['empleados']."','".$datos['volumen']."','".$datos['tomo']."','".$datos['folio']."','".$datos['hoja']."','".$datos['libro']."','".$datos['seccion']."','".$datos['administrador']." ".$datos['apellido1']." ".$datos['apellido2']."','".$datos['nifAdministrador']."')",true);
			}
		}
	return $res;
}

function obtenerCodigoServicio($datos){
	$servicio=0;
	$listado=array(0=>0);
	$consulta=consultaBD('SELECT * FROM servicios ORDER BY referencia',true);
	while($serv=mysql_fetch_assoc($consulta)){
		$listado[$serv['referencia']]=$serv['codigo'];
	}
	
	if($datos['pblc']=='SI'){
		if($datos['lopd']=='SI'){
			if(isset($datos['checkAutonomo'])){
				if($datos['empleados']==10){
					$servicio=5;
				} else if ($datos['empleados']==49) {
					$servicio=7;
				} else if ($datos['empleados']==50) {
					$servicio=13;	
				}
			} else {
				if($datos['empleados']==10){
					$servicio=10;
				} else if ($datos['empleados']==49) {
					$servicio=11;
				} else if ($datos['empleados']==50) {
					$servicio=13;
				}
			}
		} else {
			if(isset($datos['checkAutonomo'])){
				if($datos['empleados']==10){
					$servicio=4;
				} else if ($datos['empleados']==49) {
					$servicio=6;
				} else if ($datos['empleados']==50) {
					$servicio=12;
				}
			} else {
				if($datos['empleados']==10){
					$servicio=8;
				} else if ($datos['empleados']==49) {
					$servicio=9;
				} else if ($datos['empleados']==50) {
					$servicio=12;
				}
			}
		}
	} else {
		if($datos['lopd']=='SI'){
			if(!isset($datos['checkAutonomo'])){
				if($datos['empleados']==10){
					$servicio=1;
				} else {
					$servicio=2;
				}
			} else {
				if($datos['empleados']==10){
					$servicio=3;
				} else {
					//TODO
				}
			}
		} else {
			//TODO
		}
	}
	$servicio=$listado[$servicio];
	return $servicio;
}

function creaAccionComercial($datos,$codigo){
	$res=true;
	$res=consultaBD('INSERT INTO accion_comercial(codigo,codigoCliente,codigoUsuario) VALUES(NULL,'.$codigo.','.$datos['codigoConsultor'].')',true);
	return $res;
}

function listadoClientes(){
	$iconoValidado=array(''=>'-','SI'=>'<i class="icon-check-circle iconoFactura icon-success" title="Crédito validado"></i>','NO'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Crédito no validado"></i>');

	//fechaAlta lo utilizo para el filtro por ejercicios
	$columnas=array('razonSocial','clientes.cif','administrador','clientes.telefono','clientes.email','fechaAlta','colaboradores.codigo','codigoConvenio', 'provincia');
	$where=obtieneWhereListado("HAVING posibleCliente='NO'",$columnas,false,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	$query="SELECT clientes.codigo AS codigoCliente, razonSocial, clientes.cif, administrador, clientes.telefono, clientes.email, posibleCliente, 
			 fechaAlta, colaboradores.nombre AS nombreColaborador, convenios.codigo AS codigoConvenio, convenios.nombre AS nombreConvenio, clientes.provincia, colaboradores.codigo, clientes.apellido1, clientes.apellido2
			FROM clientes 
			LEFT JOIN colaboradores ON clientes.codigoColaborador=colaboradores.codigo
			LEFT JOIN convenios ON clientes.codigoConvenio=convenios.codigo
			$where";
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$botones=obtieneBotonesCliente($datos);
		if($datos['nombreColaborador']=='' || $datos['nombreColaborador']=='NULL'){
			$datos['nombreColaborador'] = 'SIN COLABORADOR';
		} 
			
		if($datos['nombreConvenio']=='' || $datos['codigoConvenio']=='NULL'){
			$datos['nombreConvenio']='EMPRESAS SIN CONVENIO';
		}

		$fila=array(
			"<a href='gestion.php?codigo=".$datos['codigoCliente']."'>".$datos['razonSocial']."</a>",
			$datos['cif'],
			$datos['nombreColaborador'],
			$datos['nombreConvenio'],
			convertirMinuscula($datos['provincia']),
			"<a href='tel:".$datos['telefono']."' class='nowrap'>".formateaTelefono($datos['telefono'])."</a>",
			"<a href='mailto:".$datos['email']."'>".$datos['email']."</a>",
			$botones,
        	obtieneCheckTabla($datos,'codigoCliente'),
        	"DT_RowId"=>$datos['codigoCliente']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function obtieneBotonesCliente($datos){
	global $_CONFIG;
	$analisis=datosRegistro('analisis_previo',$datos['codigoCliente'],'codigoCliente');
	if($analisis){
		$url="<li><a href='".$_CONFIG['raiz']."analisis-previo/gestion.php?codigo=".$analisis['codigo']."&sesion=16'><i class='icon-exclamation-circle'></i> Ver Análisis previo de riesgo</i></a></li>
		<li class='divider'></li>
			<li><a href='".$_CONFIG['raiz']."analisis-previo/generaDocumento.php?codigo=".$analisis['codigo']."' target='_blank' class='noAjax'><i class='icon-download'></i> Descargar Análisis previo de riesgo</i></a></li>";
	} else {
		$url="<li><a href='".$_CONFIG['raiz']."analisis-previo/gestion.php?codigoCliente=".$datos['codigoCliente']."&sesion=16'><i class='icon-exclamation-circle'></i> Crear Análisis previo de riesgo</i></a></li>";
	}
	//La clase generacionDocumento no tiene valor en los estilos; sirve como selector en el JS ../js/filtroTablaAJAXBusqueda.js
	$res="
		<div class='btn-group'>
			<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> <span class='caret'></span></button>
		  	<ul class='dropdown-menu' role='menu'>
			    <li><a href='".$_CONFIG['raiz']."clientes/gestion.php?codigo=".$datos['codigoCliente']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
			    <li class='divider'></li>
			    <li><a href='".$_CONFIG['raiz']."clientes/listadoServicios.php?codigo=".$datos['codigoCliente']."'><i class='icon-list'></i> Servicios contratados</i></a></li> 
			    <li class='divider'></li>
			    <li><a href='".$_CONFIG['raiz']."posibles-clientes/index.php?desconfirmar=".$datos['codigoCliente']."'><i class='icon-question-circle'></i> Pasar a cliente potencial</i></a></li>
			    <li class='divider'></li>
			    <li><a href='".$_CONFIG['raiz']."clientes/generaContrato.php?tipo=word&codigo=".$datos['codigoCliente']."'><i class='icon-cloud-download'></i> Descargar Contrato WORD</i></a></li>
			    <li class='divider'></li>
			    <li><a target='_blank' href='".$_CONFIG['raiz']."clientes/generaContrato.php?tipo=pdf&codigo=".$datos['codigoCliente']."'><i class='icon-cloud-download'></i> Descargar Contrato PDF</i></a></li>
			    <li class='divider'></li>
			    <li><a href='#' class='importarDatos noAajax' codigoCliente='".$datos['codigoCliente']."'><i class='icon-upload'></i> Importar datos de cliente</a></li>
			    <li class='divider'></li>
			    <li><a href='' codigoCliente='".$datos['codigoCliente']."' class='espacioTrabajo noAjax'><i class='icon-folder'></i> Espacio de cliente</i></a></li>
			    <li class='divider'></li>
			    <li><a href='".$_CONFIG['raiz']."zona-cliente-encargados/index.php?codigoCliente=".$datos['codigoCliente']."'><i class='icon-archive'></i> Encargados del<br/>tratamiento</i></a></li> ";


	$res.="</ul>
		</div>";

	return $res;
}

function obtieneCreditoCliente($codigoCliente){
	$res='';
	
	if(isset($_SESSION['ejercicio'])){
		$ejercicio=$_SESSION['ejercicio'];
	}
	else{
		$ejercicio=date('Y');
	}

	$consulta=consultaBD("SELECT creditoDisponible FROM creditos_cliente WHERE codigoCliente=$codigoCliente AND ejercicio=$ejercicio",false,true);
	
	if(trim($consulta['creditoDisponible'])!=''){
		$res="<div class='pagination-right'>".formateaNumeroCredito($consulta['creditoDisponible'],true)." €</div>";
	}

	return $res;
}


function obtieneOrdenListadoClientes($columnas){
	$res=obtieneOrdenListado($columnas);

	if($res==''){
		$res='ORDER BY creditos_cliente.codigo DESC';//Para que siempre aparezcan los últimos datos rellenos
	}
	else{
		$res.=', creditos_cliente.codigo DESC';
	}

	return $res;
}


function gestionCliente(){
	operacionesClientes();

	abreVentanaGestion('Gestión de clientes','?','','icon-edit','',true,'noAjax');
		$datos=compruebaDatos('clientes');
		$fecha=array();
		$fechas['fechaInicio']='';
		$fechas['fechaFin']='';
		if($datos){
			$usuario=consultaBD('SELECT usuario,clave FROM usuarios_clientes INNER JOIN usuarios ON usuarios_clientes.codigoUsuario=usuarios.codigo WHERE usuarios_clientes.codigoCliente='.$datos['codigo'],true,true);
			$mantenimiento=datosRegistro('clientes_mantenimiento',$datos['codigo'],'codigoCliente');
			$trabajo=datosRegistro('trabajos',$datos['codigo'],'codigoCliente');
			$manual=datosRegistro('manuales_pbc',$trabajo['codigo'],'codigoTrabajo');
			if($manual){
				$fechas['fechaInicio']=$manual['fechaInicio'];
				$fechas['fechaFin']=$manual['fechaFin'];
			}
		} else {
			$usuario=false;
			$mantenimiento=false;
		}
		
		campoOculto($datos,'videovigilancia');
		campoCheckIndividual('checkAutonomo','Marcar si es persona física (autónomo)',$datos);
		campoCheckIndividual('checkColegiado','Colegiado',$datos);
		abreColumnaCampos();
			campoOculto(formateaFechaWeb($datos['fechaAlta']),'fechaAlta',fecha());
			campoOculto($datos,'posibleCliente','NO');
			campoTexto('razonSocial','Nombre de la empresa',$datos,'span3');
			campoTexto('sector','Sector',$datos);
			campoSelect('actividad','Actividad',array('Abogacía','Asesoramiento fiscal','Asesoramiento fiscal y contable','Contabilidad externa','Auditoría de cuentas','Promoción inmobiliaria','Otra'),array('ABOGACIA','FISCAL','CONTABLE','CONTABILIDAD','AUDITORIA','PROMOCION','OTRA'),$datos);
			areaTexto('servicios','Servicios habituales',$datos);
			campoRadio('pblc','¿Tiene PBLC?',$datos,'SI');
			echo '<div id="fechasPBC">';
			campoFecha('fechaInicio','Fecha inicio del manual de PBC',$fechas);
			campoFecha('fechaFin','Fecha fin del manual de PBC',$fechas);
			echo '</div>';
			campoSelect('empleados','Número de empleados',array('Menos de 10 personas','Entre 10 y 49 personas','50 personas o más'),array(10,49,50),$datos);
			campoSelect('volumen','Volumen de negocios anual',array('No supera los 2 millones de euros','Supera los 2 millones de euros sin superar los 10 millones de euros','Supera los 10 millones de euros'),array(1,2,3),$datos);
			campoRadio('lopd','Implantación de LOPD',$datos);
			campoRadio('aceptaContrato','Contrato aceptado',$datos);
			echo '<div id="divAcepta">';
				campoFecha('fechaAcepta','Fecha de aceptación del contrato',$datos);
			echo '</div>';
		cierraColumnaCampos();

		abreColumnaCampos();
			campoTextoValidador('cif','CIF/NIF',$datos,'input-small','clientes');
			campoLogo('ficheroLogo','Logo',0,$datos,'../documentos/logos-clientes/','Ver');
			campoDato('Representante legal','');
			campoTexto('administrador','Nombre',$datos);
			campoTexto('apellido1','Primer apellido',$datos);
			campoTexto('apellido2','Segundo apellido',$datos);
			campoTextoValidador('nifAdministrador','NIF',$datos,'input-small','clientes');
			campoTexto('tomo','Tomo',$datos);
			campoTexto('libro','Libro',$datos);
			campoTexto('folio','Folio',$datos);
			campoTexto('seccion','Sección',$datos);
			campoTexto('hoja','Hoja',$datos);
			echo '<br/>';
			campoTexto('domicilio','Dirección',$datos,'span3');
			campoTexto('cp','Código Postal',$datos,'input-mini pagination-right');
			campoTexto('localidad','Población',$datos,'span3');
			campoSelectProvincia($datos,'provincia','Provincia',0,'');
			selectPaises('pais',$datos);
		cierraColumnaCampos();

		echo '<br clear="all">';
		abreColumnaCampos();
			campoSelectConsulta('codigoConvenio','Convenio','SELECT codigo, nombre AS texto FROM convenios ORDER BY nombre',$datos);
			campoSelectConsultaAjax('codigoColaborador','Colaborador','SELECT codigo, nombre AS texto FROM colaboradores',$datos,'colaboradores/gestion.php','selectpicker selectAjax span3 show-tick');
			campoSelectConsulta('codigoConsultor','Operador','SELECT codigo, CONCAT(nombre," ",apellidos) AS texto FROM usuarios WHERE tipo <> "CLIENTE";',$datos);
		cierraColumnaCampos();

		abreColumnaCampos();
			campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small','clientes');
			campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',$datos,'input-small','clientes');
			campoTexto('contacto','Persona de contacto',$datos);
			campoTextoSimboloValidador('fax','Fax','<i class="icon-fax"></i>',$datos,'input-small','clientes');
			campoTextoSimboloValidador('email','eMail principal','<i class="icon-envelope"></i>',$datos,'input-large','clientes');	
		cierraColumnaCampos();
		abreColumnaCampos();
			campoTextoValidador('usuario','Usuario',$usuario,'input-large obligatorio','usuarios','usuario');
			campoClaveUsuario($usuario);
		cierraColumnaCampos();
		
		if($mantenimiento){
			campoOculto($mantenimiento['codigo'],'codigoMantenimiento');
			echo '<br clear="all"><br/><h3 class="apartadoFormulario">Mantenimiento</h3><div class="mantenimiento">';
			campoDato('Fecha de contrato',formateaFechaWeb($mantenimiento['fechaContrato']));
			echo '<div class="mantenimiento">';
			campoFecha('fecha3meses','Recordatorio para la realización o actualización anual de las Relaciones de Negocio o introducción de clientes. / Pedir Pymes',$mantenimiento['fecha3meses']);
			campoOculto($mantenimiento['tarea3meses'],'tarea3meses');
			//echo "<a href='../agenda/gestion.php?codigo=".$mantenimiento['tarea3meses']."' class='btn btn-propio'>Ver tarea</a>";
			campoFecha('fecha7meses','Comprobar implantación de Prevención del blanqueo de capitales, recordar realización de formación e informes de riesgo. / Pedir Pymes',$mantenimiento['fecha7meses']);
			campoOculto($mantenimiento['tarea7meses'],'tarea7meses');
			//echo "<a href='../agenda/gestion.php?codigo=".$mantenimiento['tarea3meses']."' class='btn btn-propio'>Ver tarea</a>";
			campoFecha('fecha11meses','Gestión de la renovación anual de su espacio web para la PBLC / Comprobar implantación de LOPD y reflejar posibles cambios respecto al organigrama / Auditar LOPD',$mantenimiento['fecha11meses']);
			campoOculto($mantenimiento['tarea11meses'],'tarea11meses');
			//echo "<a href='../agenda/gestion.php?codigo=".$mantenimiento['tarea3meses']."' class='btn btn-propio'>Ver tarea</a>";
			echo '</div>';
		}

	$enlace='index.php';
	if(isset($_GET['consultoria']) || isset($_POST['consultoria'])){
		campoOculto('consultoria','consultoria');
		$enlace='../consultoria/index.php';
	}	
	cierraVentanaGestion($enlace,true);
}

function compruebaMensajeConfirmarCliente(){
	if(isset($_GET['confirmar'])){
		mensajeAdvertencia("complete los campos obligatorios para confirmar el cliente.");
	}
}

function creaTablaCuentaBancaria($datos){
	echo "
	<h3 class='apartadoFormulario'>Cuentas bancarias</h3>
	<div class='centro'>
		<table class='table table-striped tabla-simple mitadAncho' id='tablaCuentas'>
	      <thead>
	        <tr>
	          <th> C.C.C. (20 dígitos) </th>
	          <th> IBAN (24 caracteres) </th>
	          <th> BIC (11 caracteres) </th>
	          <th> </th>
	        </tr>
	      </thead>
	      <tbody>";
	  	
	  		$i=0;

	  		if($datos!=false){
	  			$consulta=consultaBD("SELECT * FROM cuentas_cliente WHERE codigoCliente='".$datos['codigo']."'",true);
	  			while($datosC=mysql_fetch_assoc($consulta)){
					imprimeLineaTablaCuentaBancaria($i,$datosC);
					campoOculto($datosC['codigo'],'codigoCuenta'.$i);
					$i++;
	  			}
	  		}
	  		
	  		if($i==0){
	  			imprimeLineaTablaCuentaBancaria(0,false);
	  		}
      
    echo "</tbody>
    	</table>
		<button type='button' class='btn btn-small btn-success' onclick='insertaFilaCuenta();'><i class='icon-plus'></i> Añadir cuenta</button> 
		<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCuentas\");'><i class='icon-trash'></i> Eliminar cuenta</button>
	</div>";
}

function imprimeLineaTablaCuentaBancaria($i,$datos){
	$j=$i+1;

	echo "<tr>";
		campoTextoTabla('ccc'.$i,$datos['ccc'],'input-large numeroCuenta pagination-right');
		campoTextoTablaValidador('iban'.$i,$datos['iban'],'input-large','cuentas_cliente','codigoCliente');
		campoTextoTabla('bic'.$i,$datos['bic'],'input-medium');
	echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
	</tr>";
}

function filtroClientes(){
	$columnas=array('razonSocial','clientes.cif','administrador','clientes.telefono','clientes.email','fechaAlta','colaboradores.codigo','codigoConvenio', 'colaboradores.provincia');
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Razón social');
	campoTexto(1,'CIF','','input-small');
	campoTexto(2,'Representante legal');
	campoTexto(3,'Teléfono',false,'input-small pagination-right');
	campoTexto(4,'eMail');
	

	cierraColumnaCampos();
	abreColumnaCampos();
		campoSelectConsultaAjax(6,'Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre;",false,'colaboradores/gestion.php?codigo=',"span3 selectAjax selectpicker show-tick");
		campoSelectConsulta(7,'Convenio','SELECT codigo, nombre AS texto FROM convenios ORDER BY nombre');
		campoSelectProvinciaAndalucia(false,8);
	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function generaWordAdhesion($PHPWord,$codigoCliente){
	$documento=$PHPWord->loadTemplate('../documentos/plantillaContratoEmpresa.docx');
	$datos=datosRegistro('clientes',$codigoCliente);
	$venta=consultaBD('SELECT * FROM ventas_servicios WHERE codigoCliente='.$codigoCliente.' ORDER BY FECHA DESC LIMIT 1;',true,true);
	$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');
	$fecha=explode('-', $datos['fechaAcepta']);
	$documento->setValue("razonSocial",utf8_decode(sanearCaracteres($datos['razonSocial'])));
	$documento->setValue("cif",utf8_decode($datos['cif']));
	$documento->setValue("domicilio",utf8_decode($datos['domicilio']));
	$documento->setValue("cp",utf8_decode($datos['cp']));
	$documento->setValue("localidad",utf8_decode($datos['localidad']));
	$documento->setValue("provincia",utf8_decode(convertirMinuscula($datos['provincia'])));
	$documento->setValue("administrador",utf8_decode($datos['administrador'].' '.$datos['apellido1'].' '.$datos['apellido2']));
	$documento->setValue("observaciones",strip_tags(utf8_decode($venta['observacionesParaFactura'])));
	if($datos['aceptaContrato']=='SI'){
		$documento->setValue("fecha",'Lo cual aceptan a '.utf8_decode('día').' '.$fecha[2].' de '.$meses[$fecha[1]].' de '.$fecha[0]);
	} else {
		$documento->setValue("fecha",'Lo cual '.utf8_decode('aún').' no han aceptado');
	}
	$documento->setValue("true","");
	$documento->save('../documentos/adhesiones/Contrato-Empresa.docx');
}

function construyeFechaFirmaAdhesion($fechaFirma,$fechaFirmaDocumentos,$ejercicioSeleccionado){
	if($fechaFirma!='0000-00-00'){
		$array=explode('-',$fechaFirma);

		if($array[0]!=$ejercicioSeleccionado && $ejercicioSeleccionado!='Todos'){
			$res=$array[2].'/'.$array[1].'/'.$ejercicioSeleccionado;
		}
		else{
			$res=$array[2].'/'.$array[1].'/'.$array[0];
		}

	}
	else{
		$res=formateaFechaWeb($fechaFirmaDocumentos);
	}

	return $res;
}

function obtieneNombreCliente($codigoCliente){
	$datos=consultaBD("SELECT razonSocial FROM clientes WHERE codigo=$codigoCliente",true,true);

	return "<a href='gestion.php?codigo=$codigoCliente'>".$datos['razonSocial']."</a>";
}

function listadoServicios($codigoCliente){
	$iconos=array(
					'VALIDA'=>'<i class="icon-check-circle iconoFactura icon-success" title="Válida"></i>',
					'INCIDENTADA'=>'<i class="icon-exclamation-circle iconoFactura icon-naranja" title="Incidentada"></i>',
					'PENDIENTE'=>'<i class="icon-minus-circle iconoFactura icon-inverse" title="Pendiente"></i>',
					'ANULADA'=>'<i class="icon-times-circle iconoFactura icon-danger" title="Anulada"></i>'
				);
	$columnas=array('ventas_servicios.fecha','servicios.referencia','servicios.servicio','ventas_servicios.confirmada','ventas_servicios.estado');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	//Unifico en una misma consulta el las ventas de formación y de servicios, mediante UNION ALL. Casi ná
	$query="SELECT ventas_servicios.codigo, ventas_servicios.fecha, servicios.referencia, servicios.servicio, ventas_servicios.confirmada, ventas_servicios.estado
	FROM clientes 
	INNER JOIN ventas_servicios ON clientes.codigo=ventas_servicios.codigoCliente
	INNER JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio
	INNER JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo WHERE clientes.codigo=".$codigoCliente;
	
	conexionBD();
	$consulta=consultaBD($query." $having $orden $limite;");
	$consultaPaginacion=consultaBD($query." $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$confirmada='PREVENTA';
		if($datos['confirmada']=='SI'){
			$confirmada='<div style="float:left;">VENTA</div> <div style="float:right">'.$iconos[$datos['estado']].'</div>';
		}
		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['referencia'],			
			$datos['servicio'],
			$confirmada,
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function obtieneEnlaces($datos){
	if($datos['codigoAccionFormativa']!='-'){
		$datos['accion']="<a href='../acciones-formativas/gestion.php?codigo=".$datos['codigoAccionFormativa']."'>".$datos['accion']."</a>";
		$datos['alumno']="<a href='../trabajadores/gestion.php?codigo=".$datos['codigoTrabajador']."'>".$datos['alumno']."</a>";
	}

	return $datos;
}

function obtieneOrdenListadoServicios($columnas){
	$res=obtieneOrdenListado($columnas);

	if($res==''){
		$res='ORDER BY fechaVencimiento DESC';//Para que siempre aparezcan los últimos datos rellenos
	}
	else{
		$res.=', fechaVencimiento DESC';
	}

	return $res;
}


function creaBotonesGestionCliente(){
	echo '
    <a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
}

function creaBotonesListadoServicios(){
	echo '
    <a class="btn-floating btn-large btn-default btn-eliminacion" href="index.php" title="Volver"><i class="icon-chevron-left"></i></a>';
}




function generaInformeRLT($PHPWord,$codigoCliente,$codigoEmisor,$ejercicio){
	$datos=consultaBD("SELECT clientes.*, emisores.razonSocial AS emisor, emisores.cif AS cifEmisor, fechaFirma 
					   FROM clientes INNER JOIN emisores_documentos_cliente ON clientes.codigo=emisores_documentos_cliente.codigoCliente 
					   INNER JOIN emisores ON emisores_documentos_cliente.codigoEmisor=emisores.codigo 
					   LEFT JOIN creditos_cliente ON clientes.codigo=creditos_cliente.codigoCliente
					   WHERE clientes.codigo=$codigoCliente AND codigoEmisor=$codigoEmisor
					   GROUP BY creditos_cliente.codigoCliente
					   ORDER BY ejercicio DESC",true,true);
	
	$fecha=construyeFechaFirmaInformeRLT($datos['fechaFirma'],$datos['fechaFirmaDocumentos'],$ejercicio);

	$documento=$PHPWord->loadTemplate('../documentos/rlt-clientes/plantilla.docx');

	$documento->setValue("razonSocial",utf8_decode($datos['razonSocial']));
	$documento->setValue("cif",utf8_decode($datos['cif']));
	$documento->setValue("numCentros",utf8_decode($datos['numCentros']));
	$documento->setValue("ubicacionCentros",utf8_decode($datos['ubicacionCentros']));

	$documento->setValue("nombreRL",utf8_decode($datos['administrador'].' '.$datos['apellido1'].' '.$datos['apellido2']));
	$documento->setValue("nifRL",utf8_decode($datos['nifAdministrador']));

	$documento->setValue("nombreRLT",utf8_decode($datos['nombreRLT']));
	$documento->setValue("nifRLT",utf8_decode($datos['nifRLT']));
	
	$documento->setValue("localidad",utf8_decode($datos['localidad']));
	$documento->setValue("dia",$fecha[2]);
	$documento->setValue("mes",$fecha[1]);
	$documento->setValue("anio",$fecha[0]);

	$documento->setValue("razonSocialEmisor",utf8_decode($datos['emisor']));
	$documento->setValue("cifEmisor",utf8_decode($datos['cifEmisor']));

	insertaImagenFirmaWord($datos['firma'],$documento,'image1.png');
	insertaImagenFirmaWord($datos['firmaRLT'],$documento,'image2.png');


	$documento->save('../documentos/rlt-clientes/Informe-RLT.docx');
}

function construyeFechaFirmaInformeRLT($fechaFirma,$fechaFirmaDocumentos,$ejercicioSeleccionado){
	if($fechaFirma!='0000-00-00'){
		$res=explode('-',$fechaFirma);
	}
	else{
		$res=explode('-',$fechaFirmaDocumentos);
	}

	if($res[0]!=$ejercicioSeleccionado && $ejercicioSeleccionado!='Todos'){//Si se elige un ejercicio distinto al de la última firma, se colca éste como año de la misma (lo pidieron así)
		$res[0]=$ejercicioSeleccionado;
	}

	$res[1]=obtieneMesFechaInformeRLT($res[1]);

	return $res;
}

function obtieneMesFechaInformeRLT($mesFirma){
	$meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');

	return $meses[(int)$mesFirma];
}


function generaWordSepa($PHPWord,$codigoCliente,$codigoEmisor,$ejercicio){
	$datos=consultaBD("SELECT clientes.codigo, referenciaAcreedor, emisores.razonSocial AS emisor, emisores.domicilio AS direccionEmisor,
	emisores.cp AS cpEmisor, emisores.localidad AS localidadEmisor, emisores.provincia AS provinciaEmisor,
	clientes.razonSocial AS cliente, clientes.domicilio AS direccionCliente, clientes.cp AS cpCliente, clientes.localidad AS localidadCliente,
	clientes.provincia AS provinciaCliente, cuentas_cliente.bic, cuentas_cliente.iban, fechaSepa, fechaFirmaDocumentos, tipoSepa, firma, emisores.ficheroLogo

	FROM clientes INNER JOIN emisores_documentos_cliente ON clientes.codigo=emisores_documentos_cliente.codigoCliente
	INNER JOIN emisores ON emisores_documentos_cliente.codigoEmisor=emisores.codigo
	INNER JOIN cuentas_cliente ON clientes.codigo=cuentas_cliente.codigoCliente

	WHERE clientes.codigo=$codigoCliente AND emisores.codigo=$codigoEmisor

	GROUP BY clientes.codigo",true,true);

	$documento=$PHPWord->loadTemplate('../documentos/sepa/plantilla.docx');

	$referencia=str_pad($datos['codigo'],5,'0',STR_PAD_LEFT);

	$documento->setValue("referenciaFactura",utf8_decode($referencia));
	$documento->setValue("refAcreedor",utf8_decode($datos['referenciaAcreedor']));
	$documento->setValue("emisor",utf8_decode($datos['emisor']));
	$documento->setValue("domicilioEmisor",utf8_decode($datos['direccionEmisor']));
	$documento->setValue("cpEmisor",utf8_decode($datos['cpEmisor']));
	$documento->setValue("localidadEmisor",utf8_decode($datos['localidadEmisor']));
	$documento->setValue("provinciaEmisor",convertirMinuscula($datos['provinciaEmisor']));

	$documento->setValue("razonSocialCliente",utf8_decode($datos['cliente']));
	$documento->setValue("domicilioCliente",utf8_decode($datos['direccionCliente']));
	$documento->setValue("cpCliente",utf8_decode($datos['cpCliente']));
	$documento->setValue("localidadCliente",utf8_decode($datos['localidadCliente']));
	$documento->setValue("provinciaCliente",convertirMinuscula($datos['provinciaCliente']));

	$documento->setValue("bicCliente",utf8_decode($datos['bic']));
	$documento->setValue("ibanCliente",utf8_decode($datos['iban']));
	
	if($datos['tipoSepa']=='Pago recurrente'){
		$documento->setValue("re",'X');
		$documento->setValue("un",' ');
	}
	else{
		$documento->setValue("re",' ');
		$documento->setValue("un",'X');
	}

	$documento->setValue("fechaSepa",obtieneFechaFirmaSepaWord($datos));

	insertaImagenFirmaWord($datos['firma'],$documento);
	insertaImagenLogoSepa($datos['ficheroLogo'],$documento);

	$documento->save('../documentos/sepa/Orden-SEPA.docx');
}

function obtieneFechaFirmaSepaWord($datos){
	if($datos['fechaSepa']!='0000-00-00'){
		$res=formateaFechaWeb($datos['fechaSepa']);
	}
	else{
		$res=formateaFechaWeb($datos['fechaFirmaDocumentos']);
	}

	return $res;
}

function insertaImagenLogoSepa($logo,$documento){
	if(file_exists('../documentos/emisores/'.$logo) && trim($logo)!='' && $logo!='NO'){
		copy('../documentos/emisores/'.$logo,'../documentos/sepa/image1.png');//Porque la imagen debe llamarse igual que en el Word

		$documento->replaceImage('../documentos/sepa/','image1.png');
	}
}

function recogeExcel(){
	$res='';
	$excels=consultaBD('SELECT * FROM informes_clientes_excel WHERE codigoCliente='.$_POST['codigo'].' ORDER BY fecha',true);
	while($excel=mysql_fetch_assoc($excels)){
		$res.="<li id='ficheroExcel".$excel['codigo']."'><a href='../informes-clientes/documentos/".$excel['excel']."'><i class='icon-download'></i> ".formateaFechaWeb($excel['fecha'])." - ".$excel['excel']."</a> <a class='eliminaExcel' href='#' class='noAjax' codigo='".$excel['codigo']."'><i class='icon-trash'></i></a></li>";
	}
	$res=$res==''?'<li>No hay fichero subidos para este cliente</li>':$res;
	echo $res;
}

function generaPDFContrato($codigo){

	$datos = datosRegistro('clientes',$codigo);

	$meses=array('01'=>'Enero','02'=>'Febrero','03'=>'Marzo','04'=>'Abril','05'=>'Mayo','06'=>'Junio','07'=>'Julio','08'=>'Agosto','09'=>'Septiembre','10'=>'Octubre','11'=>'Noviembre','12'=>'Diciembre');

	$fecha=explode('-', $datos['fechaAcepta']);

	$contenido = "
	<style type='text/css'>
	<!--
	        body{
	            font-size:11px;
	            font-family: helvetica;
	            font-weight: lighter;
	            line-height: 24px;
	        }

	        .cabecera img{
	        	text-align:center;
	        }

	        .cabecera img{
	        	width:18%;
	        }

	        .cabecera p{
	        	text-align:center;
	        	font-weight:bold;
	        	font-size:18px;
	        }

	        .cuerpo .titulo{
	        	font-weight:bold;
	        	font-size:16px;
	        	line-height:8px;
	        }

	        .cuerpo p{
	        	font-size:14px;
	        	text-align:justify;
	        }

	        table{
	        	width:100%;
	        }

	        table td{
	        	width:50%;
	        	font-size:14px;
	        }

	        ul li{
	        	font-size:14px;
	        }
	-->
	</style>
	<page backleft='20mm' backright='20mm' backtop='5mm'>
	<div class='cabecera'>
		<p>CONTRATO PARA ALTA DE EMPRESAS</p>
		<p><img src='../img/logo.png' /></p>
	</div>
	<div class='cuerpo'>
		<p> <span class='titulo'>De una parte:</span><br/>
		- Muñoz y Díaz Asesores de calidad, S.L.U. (en adelante UVED Asesores y Consultores)<br/>
		- Con CIF/NIF: B92598101<br/>
		- Dirección: C/ Cuesta de los Rojas, 8, 3º B<br/>
		- Código Postal: 29200<br/>
		- Localidad: Antequera<br/>
		- Provincia: Málaga<br/>
		- Representante legal: Víctor Díaz Torres</p>
		<p> <span class='titulo'>Y de otra parte:</span><br/>
		- ".$datos['razonSocial']."<br/>
		- Con CIF/NIF: ".$datos['cif']."<br/>
		- Dirección: ".$datos['domicilio']."<br/>
		- Código Postal: ".$datos['cp']."<br/>
		- Localidad: ".$datos['localidad']."<br/>
		- Provincia: ".$datos['provincia']."<br/>
		- Representante legal: ".$datos['administrador']." ".$datos['apellido1']." ".$datos['apellido2']."</p>
		<p> <span class='titulo'>Acuerdan las siguientes condiciones de contratación</span><br/></p>
		<p> <span class='titulo'>1. TITULARIDAD</span><br/><br/>
		A través del presente documento la UVED Asesores y Consultores expone las condiciones generales de contratación que rigen los servicios prestados a través de la página Web de su propiedad.</p>
		<p> <span class='titulo'>2. OBJETO DEL SERVICIO</span><br/><br/>
		El presente contrato tiene por objeto la concesión por parte de la UVED Asesores y Consultores a favor del CLIENTE de una licencia de uso de los servicios que proporciona la página web www.crmparapymes.com.es/mdiaz. Dicha licencia de uso consiste en habilitar un espacio en la plataforma web www.crmparapymes.com.es/mdiaz para el alojamiento de la empresa del CLIENTE.
		<p> <span class='titulo'>3. ACEPTACIÓN DEL CONTRATO</span><br/><br/></p>
		Este documento, deberá ser aceptado por el CLIENTE, para contratar los servicios el CLIENTE deberá leer atentamente las presentes condiciones de contratación y aceptarlas.</p>
		<p> <span class='titulo'>4. MODALIDAD DE CONTRATACIÓN</span><br/><br/>
		La modalidad de contratación es única, y estarán incluidos todos los servicios de la plataforma web, sin ningún tipo de limitaciones en cuanto a número de consultas, ni alta de clientes y/o registros.</p>
		<p> <span class='titulo'>5. SERVICIO Y COSTE</span><br/><br/>
		En caso de baja, el CLIENTE deberá comunicarlo con un mes de antelación. El coste del servicio -software de gestión y soporte PBC-FT y/o RGPD-, (250€+IVA anuales), será domiciliado en el número de cuenta facilitado por el CLIENTE. </p>
		<p> <span class='titulo'>6. IMPAGADOS Y BAJAS</span><br/><br/>
		UVED Asesores y Consultores, procederá a limitar el acceso del CLIENTE, cuando por cualquier motivo se produzca una devolución del importe facturado según las condiciones económicas. El acceso a la plataforma volverá a activarse una vez que el CLIENTE esté al corriente de sus pagos.</p>
		<p> <span class='titulo'>7. FACTURACIÓN</span><br/><br/>
		Todas las facturas emitidas por la UVED Asesores y Consultores serán incrementadas conforme a los impuestos que le sean aplicables el día de su facturación. Dichas facturas serán remitidas al email proporcionado por el CLIENTE en el formulario de suscripción.</p>
		<p> <span class='titulo'>8. DERECHOS DE PROPIEDAD INTELECTUAL</span><br/><br/>
		Los derechos de propiedad intelectual y nombre comercial correspondientes a la plataforma www.crmparapymes.com.es/mdiaz pertenecen a UVED Asesores y Consultores.</p>
		<p> <span class='titulo'>9. CONDICIONES DEL SERVICIO Y SOPORTE</span><br/><br/>
		UVED Asesores y Consultores, propietaria de la plataforma www.crmparapymes.com.es/mdiaz. En este sentido, UVED Asesores y Consultores se reserva el derecho de añadir nueva información, mejorar, o modificar la presentación del servicio en cualquier momento, así como introducir los elementos necesarios para facilitar el control de su utilización. El mantenimiento o reparación de los equipos informáticos y de transmisión de datos u otros elementos similares ajenos al control de UVED Asesores y Consultores, podrán motivar la modificación o suspensión temporal del servicio, reservándose UVED Asesores y Consultores el derecho de hacerlo.
		UVED Asesores y Consultores atenderá las consultas y dudas planteadas por el CLIENTE, siempre que estas sean sobre la operatoria de la plataforma web www.crmparapymes.com.es/mdiaz atendiendo las llamadas telefónicas que se produzcan cuando esto sea posible, o en su defecto mediante correo electrónico que deberá remitir el CLIENTE.</p>
		<p> <span class='titulo'>10. RESPONSABLE DE LOS DATOS</span><br/><br/>
		El responsable de los datos registrados en la plataforma web objeto del presente contrato (responsable de los ficheros) es el CLIENTE.
		La plataforma web www.crmparapymes.com.es/mdiaz tiene la única consideración de herramienta de ayuda y formación. UVED Asesores y Consultoresno tienen ninguna responsabilidad sobre los datos introducidos por el CLIENTE (responsable del fichero) en la web www.crmparapymes.com.es/mdiaz</p>
		<p> <span class='titulo'>11. ENCARGADO DEL TRATAMIENTO</span><br/><br/>
		UVED Asesores y Consultores como propietaria de la plataforma web www.crmparapymes.com.es/mdiaz, tienen la consideración de encargados del tratamiento, pues tan sólo tienen acceso a la plataforma web para consultar, y en ningún caso deciden sobre la finalidad, uso o destino de los datos personales que están contenidos en esta plataforma y son introducidos por el CLIENTE como responsable de los ficheros.<br/>
		En este sentido, UVED Asesores y Consultoresse comprometen a:</p>
		<ul>
			<li>Guardar el deber de secreto con respecto a la información relativa a datos de carácter personal contenidos en la plataforma.</li>
			<li>Mantener actualizados sus documentos de seguridad y cumplir el resto de requisitos exigidos por la LOPD.</li>
			<li>No ceder o comunicar datos personales sin la correspondiente autorización expresa del responsable del fichero o tratamiento.</li>
			<li>Comunicar al CLIENTE responsable del fichero o tratamiento cualquier petición sobre derechos ARCO que se reciba de los interesados.</li>
			<li>Destruir todos los datos personales una vez finalizado el presente contrato, previo el correspondiente bloqueo de los mismos.</li>
			<li>Acceder a los datos personales exclusivamente cuando sea necesario una consulta a petición del CLIENTE, o por algún requerimiento técnico necesario para el mantenimiento del sistema.</li>
		</ul>

		<p> <span class='titulo'>12. LIMITACIONES DE USO</span><br/><br/>
		Queda expresamente prohibido al CLIENTE reproducir, copiar, transformar, modificar o bajo cualquier procedimiento alterar la información contenida en la web www.crmparapymes.com.es/mdiaz , ya sea de forma parcial o total.</p>
		<p> <span class='titulo'>13. RESOLUCIÓN</span><br/><br/>
		La renovación del presente contrato se realizará de manera tácita y se prolongara en el tiempo, hasta que alguna de las partes comunique lo contrario. En caso de baja, el CLIENTE deberá comunicarlo con un mes de antelación.</p>
		<p> <span class='titulo'>14. ARBITRAJE Y JURISDICCIÓN</span><br/><br/>
		En caso de cualquier conflicto, se acepta por las partes que los tribunales competentes para dirimirlo serán los pertenecientes a la ciudad de Málaga (España) desistiendo el CLIENTE explícitamente al fuero que le pudiera pertenecer y aceptando la solución del conflicto a través del Arbitraje.</p><br/><br/><br/><br/><p>";

	if($datos['aceptaContrato']=='SI'){
		$contenido.='Lo cual aceptan a día '.$fecha[2].' de '.$meses[$fecha[1]].' de '.$fecha[0];
	} else {
		$contenido.='Lo cual aún no han aceptado';
	}

	$contenido.="</p><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<table>
		<tr>
			<td>UVED Asesores y Consultores</td>
			<td>CLIENTE</td>
		</tr>
		<tr>
			<td><img style='width:200px;' src='../img/firmaContratoCliente.jpg'></td>
			<td></td>
		</tr>
	</table>
	</div>
	</page>";

	return $contenido;
}


function sanearCaracteres($texto){
	$texto = str_replace( '&', '&#38;', $texto);
	$texto = str_replace( '–', '&#45;', $texto);
	$texto = str_replace( '"', '', $texto);
	return $texto;
}
//Fin parte de clientes
<?php
  $seccionActiva=16;
  include_once('../cabecera.php');
  $_SESSION['espacio']=$_GET['codigoCliente'];
  if(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
    $res=eliminaDatos('informes_clientes');
  }
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
        <?php
          creaBotonesGestionCliente();
        ?>

       <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Registros de informes de examen especial</h3>
              <div class="pull-right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                //filtroServicios();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaServicios4">
                <thead>
                  <tr>
                    <?php
                      if($_SESSION['tipoUsuario']=='ADMIN'){
                         echo '<th> Cliente </th>'; 
                      }
                    ?>
                    <th> Referencia </th>
                    <th> Nº </th>
                    <th> Fecha </th>
                    <th> Solicitante </th>
                    <th> Finalizado </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo1"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Informes registrados</h3>
              <div class="pull-right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                //filtroServicios();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaServicios">
                <thead>
                  <tr>
                    <?php
                      if($_SESSION['tipoUsuario']=='ADMIN'){
                         echo '<th> Cliente </th>'; 
                      }
                    ?>
                    <th> Referencia </th>
                    <th> Fecha </th>
                    <th> Nº </th>
                    <th> Solicitante </th>
                    <th> Finalizado </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo1"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Servicios registrados</h3>
              <div class="pull-right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                //filtroServicios();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaServicios2">
                <thead>
                  <tr>
                    <th> Fecha </th>
                    <th> Servicio </th>
                    <th class="centro"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>

    <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Análisis previos registrados</h3>
              <div class="pull-right">
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                //filtroServicios();
              ?>
              <table class="table table-striped table-bordered datatable" id="tablaServicios3">
                <thead>
                  <tr>
                    <th> Empleados </th>
                    <th> Volumen </th>
                    <th> Fecha </th>
                    <th class="centro"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
    </div>


    </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaServicios','../listadoAjax.php?include=informes-clientes&funcion=listadoTrabajos();');
    listadoTabla('#tablaServicios2','../listadoAjax.php?include=zona-cliente&funcion=listadoTrabajos();');
    listadoTabla('#tablaServicios3','../listadoAjax.php?include=analisis-previo&funcion=listadoTrabajos();');
    listadoTabla('#tablaServicios4','../listadoAjax.php?include=informes-clientes&funcion=listadoTrabajos(\'SI\');');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
<?php

    include_once('funciones.php');
    compruebaSesion();

    include_once('../../api/js/firma/signature-to-image.php');
    require_once('../../api/phpword/PHPWord.php');


    generaInformeRLT(new PHPWord(),$_GET['codigo'],$_GET['codigoEmisor'],$_GET['ejercicio']);

    // Definir headers
    header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header("Content-Disposition: attachment; filename=Informe-RLT.docx");
    header("Content-Transfer-Encoding: binary");

    // Descargar archivo
    readfile('../documentos/rlt-clientes/Informe-RLT.docx');
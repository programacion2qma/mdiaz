<?php

    include_once('funciones.php');
    compruebaSesion();

    include_once('../../api/js/firma/signature-to-image.php');
    require_once('../../api/phpword/PHPWord.php');
    require_once('../../api/html2pdf/html2pdf.class.php');


    if($_GET['tipo']=='word'){
        generaWordAdhesion(new PHPWord(),$_GET['codigo']);

        header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        header("Content-Disposition: attachment; filename=Contrato.docx");
        header("Content-Transfer-Encoding: binary");

        readfile('../documentos/adhesiones/Contrato-Empresa.docx');
    } else if($_GET['tipo']=='pdf'){
        $contenido=generaPDFContrato($_GET['codigo']);
        $nombre='Contrato.pdf';

        $html2pdf=new HTML2PDF('P','A4','es');
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->WriteHTML($contenido);
        $html2pdf->Output($nombre);
    }
<?php
  $seccionActiva=16;
  include_once('../cabecera.php');

  $codigoCliente=$_GET['codigo'];
  $nombreCliente=obtieneNombreCliente($codigoCliente);
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
          creaBotonesListadoServicios();
        ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Listado de servicios contratados por <?=$nombreCliente?></h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <table class="table table-striped tablaTextoPeque table-bordered datatable" id="tablaServicios">
                <thead>
                  <tr>
                    <th>Fecha venta</th>
                    <th>Cód. Serv</th>
                    <th>Servicio</th>
                    <th>Estado</th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaServicios','../listadoAjax.php?include=clientes&funcion=listadoServicios(<?=$codigoCliente?>);');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
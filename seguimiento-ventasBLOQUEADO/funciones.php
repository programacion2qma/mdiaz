<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de vencimientos

function creaEstadisticasVencimientos(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='ADMINISTRACION2'){
		$res=consultaBD("SELECT COUNT(vencimientos_facturas.codigo) AS total 
		FROM vencimientos_facturas LEFT JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo
		LEFT JOIN comerciales ON facturas.codigoComercial=comerciales.codigo
		WHERE codigoFactura IS NOT NULL AND codigoUsuario=$codigoUsuario",true,true);
	}
	else{
		$res=estadisticasGenericas('vencimientos_facturas',false,'codigoFactura IS NOT NULL');;
	}

	return $res;
}

function listadoVencimientos(){
	global $_CONFIG;

	$columnas=array(
		'fechaVencimiento',
		'facturas.fecha',
		'clientes.razonSocial',
		'serie',
		'numero',
		'emisores.razonSocial',
		'vencimientos_facturas.importe',
		'baseImponible',
		'total',
		'facturas.medioPago',
		'estado',
		'comerciales.nombre',
		'asesoria',
		'telefonoPrincipal',
		'llamadaAsesoria',
		'llamadaEmpresa',
		'fechaVencimiento',
		'facturas.fecha',
		'cuentas_cliente.codigo',
		'codigoUsuario',
		'cobros_facturas.codigo',
		'facturas.activo',
		'comerciales.codigo'
	);

	$where=obtieneWhereListadoVencimientos($columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();

	$consulta=consultaBD("SELECT facturas.codigo AS codigoFactura, clientes.codigo AS codigoCliente, asesorias.codigo AS codigoAsesoria, fechaVencimiento, 
	facturas.fecha, clientes.razonSocial AS cliente, serie, numero, emisores.razonSocial AS emisor, vencimientos_facturas.importe, baseImponible, total, facturas.medioPago, 
	estado, comerciales.nombre AS comercial, asesoria, asesorias.telefonoPrincipal, codigoUsuario, facturas.activo, vencimientos_facturas.codigo,
	codigoUsuario, codigoFacturaAsociada, tipoFactura, llamadaAsesoria, llamadaEmpresa

	FROM vencimientos_facturas INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
	LEFT JOIN cuentas_cliente ON clientes.codigo=cuentas_cliente.codigoCliente
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN comerciales ON facturas.codigoComercial=comerciales.codigo
	LEFT JOIN asesorias ON clientes.codigoAsesoria=asesorias.codigo
	LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura

	$where GROUP BY vencimientos_facturas.codigo $orden $limite;");
	
	$consultaPaginacion=consultaBD("SELECT facturas.codigo AS codigoFactura, clientes.codigo AS codigoCliente, asesorias.codigo AS codigoAsesoria, fechaVencimiento, 
	facturas.fecha, clientes.razonSocial AS cliente, serie, numero, emisores.razonSocial AS emisor, vencimientos_facturas.importe, baseImponible, total, facturas.medioPago, 
	estado, comerciales.nombre AS comercial, asesoria, asesorias.telefonoPrincipal, codigoUsuario, facturas.activo, vencimientos_facturas.codigo,
	codigoUsuario, codigoFacturaAsociada, tipoFactura, llamadaAsesoria, llamadaEmpresa

	FROM vencimientos_facturas INNER JOIN facturas ON vencimientos_facturas.codigoFactura=facturas.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
	LEFT JOIN cuentas_cliente ON clientes.codigo=cuentas_cliente.codigoCliente
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN comerciales ON facturas.codigoComercial=comerciales.codigo
	LEFT JOIN asesorias ON clientes.codigoAsesoria=asesorias.codigo
	LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura

	$where GROUP BY vencimientos_facturas.codigo");

	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		if(compruebaAbonoTotal($datos)){
			$datos=compruebaImportesAbono($datos);
			$llamadaAsesoria=generaCampoCheckVencimiento('llamadaAsesoria',$datos);
			$llamadaEmpresa=generaCampoCheckVencimiento('llamadaEmpresa',$datos);

			$fila=array(
				'<div class="nowrap">'.formateaFechaWeb($datos['fechaVencimiento']).'</div>',
				'<div class="nowrap">'.formateaFechaWeb($datos['fecha']).'</div>',
				"<a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a>",
				$datos['serie'],
				$datos['numero'],
				$datos['emisor'],
				'<div class="nowrap pagination-right">'.formateaNumeroWeb($datos['importe']).' €'.'</div>',
				'<div class="nowrap pagination-right">'.formateaNumeroWeb($datos['baseImponible']).' €'.'</div>',
				'<div class="nowrap pagination-right">'.formateaNumeroWeb($datos['total']).' €'.'</div>',
				$datos['medioPago'],
				$datos['estado'],
				$datos['comercial'],
				$datos['asesoria'],
				'<div class="nowrap">'.$datos['telefonoPrincipal'].'</div>',
				$llamadaAsesoria,
				$llamadaEmpresa,
				botonAcciones(array('Ver factura','Ver cliente','Ver asesoría'),array('facturas/gestion.php?codigo='.$datos['codigoFactura'],'clientes/gestion.php?codigo='.$datos['codigoCliente'],'asesorias/gestion.php?codigo='.$datos['codigoAsesoria']),array('icon-eur','icon-shopping-cart','icon-life-ring'),false,false,''),
	        	"DT_RowId"=>$datos['codigo']
			);

			$res['aaData'][]=$fila;
		}
	}

	echo json_encode($res);
}

function generaCampoCheckVencimiento($nombreCampo,$datos){
	$valor=compruebaValorCampo($datos,$nombreCampo);

    $res="<div class='centro'><label class='checkbox inline'>
    		<input type='checkbox' name='$nombreCampo".$datos['codigo']."' id='$nombreCampo".$datos['codigo']."' value='SI'";

    if($valor!=false && $valor=='SI'){
    	$res.=" checked='checked'";
    }
    $res.="></label></div>";

    return $res;
}

function compruebaImportesAbono($datos){
	if($datos['codigoFacturaAsociada']!=NULL){
		$datos['importe']*=-1;
		$datos['baseImponible']*=-1;
		$datos['total']*=-1;
	}

	return $datos;
}

function compruebaAbonoTotal($datos){
	$res=true;

	if($datos['tipoFactura']=='ABONO'){
		$facturaAsociada=consultaBD("SELECT total FROM facturas WHERE codigo=".$datos['codigoFacturaAsociada'],false,true);
		if($datos['total']==$facturaAsociada['total']){
			$res=false;
		}
	}
	else{
		$abonoAsociado=consultaBD("SELECT total FROM facturas WHERE codigoFacturaAsociada=".$datos['codigoFactura'],false,true);
		if($datos['total']==$abonoAsociado['total']){
			$res=false;
		}
	}

	return $res;
}

function obtieneWhereListadoVencimientos($columnas){
	$where="WHERE 1=1";

	if((isset($_GET['sSearch_23']) && $_GET['sSearch_23']!='') || (isset($_GET['sSearch_24']) && $_GET['sSearch_24']!='')){
		$whereFecha=generaWhereCursosFinalizados();
		$where="WHERE facturas.codigo IN(SELECT grupos_en_facturas.codigoFactura FROM grupos_en_facturas INNER JOIN grupos ON grupos_en_facturas.codigoGrupo=grupos.codigo WHERE $whereFecha)";
	}

	$res=obtieneWhereListado($where,$columnas,true);

	return $res;
}

function generaWhereCursosFinalizados(){
	$res='grupos_en_facturas.codigoFactura IS NOT NULL';

	if(isset($_GET['sSearch_23']) && $_GET['sSearch_23']!=''){
		$res.=" AND grupos.fechaFin>='".formateaFechaBD($_GET['sSearch_23'])."'";
	}

	if(isset($_GET['sSearch_24']) && $_GET['sSearch_24']!=''){
		$res.=" AND grupos.fechaFin<='".formateaFechaBD($_GET['sSearch_24'])."'";
	}

	return $res;
}

function filtroVencimientos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectConsulta(3,'Serie',"SELECT serie AS codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",false,'span1 selectpicker show-tick');
	campoTexto(4,'Nº Factura',false,'input-mini pagination-right');
	campoTexto(5,'Emisor');
	campoTexto(2,'Cliente');
	campoFormaPago(false,9);
	campoSelectEstadoVencimiento(10);
	campoSelectTieneFiltro(20,'Pago realizado');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(0,'F. vencimiento desde');
	campoFecha(16,'Hasta');
	campoFecha(23,'Fin curso desde');//Éste índice no está en $columnas, lo uso manualmente en obtieneWhereListadoVencimientos()
	campoFecha(24,'Hasta');//Éste índice no está en $columnas, lo uso manualmente en obtieneWhereListadoVencimientos()
	campoFecha(1,'F. factura desde');
	campoFecha(17,'Hasta');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta(12,'Asesoría',"SELECT asesoria AS codigo, asesoria AS texto FROM asesorias WHERE activo='SI' ORDER BY asesoria");
	campoSelectComercial(false,false,22);
	campoSelectConsulta(19,'Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') AND activo='SI' ORDER BY nombre,apellidos");
	campoSelectTieneFiltro(18,'Cuenta bancaria');
	campoSelectSiNoFiltro(21,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function campoSelectEstadoVencimiento($nombre){
	$estados=array('','Anticipado','Descontado','Impagado definitivo','Impagado gestión de cobro','Pagado','Pendiente pago');
	$valores=array('','ANTICIPADO','DESCONTADO','IMPAGADO DEFINITIVO','IMPAGADO GESTIÓN DE COBRO','PAGADO','PENDIENTE PAGO');
	campoSelect($nombre,'Estado',$estados,$valores,false,'selectpicker span3 show-tick','');
}

function cambiaEstadoLlamada(){
	$datos=arrayFormulario();

	cambiaValorCampo($datos['nombreCampo'],$datos['valor'],$datos['codigoVencimiento'],'vencimientos_facturas');
}

//Fin parte de vencimientos
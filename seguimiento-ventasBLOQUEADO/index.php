<?php
  $seccionActiva=1;
  include_once('../cabecera.php');
  
  $estadisticas=creaEstadisticasVencimientos();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container ancho100">        
		

      
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Vencimientos pendientes de pago registrados</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroVencimientos();
              ?>
              <table class="table table-striped table-bordered tablaTextoPeque tablaVencimientos datatable" id="tablaListadoVencimientos">
                <thead>
                  <tr>
                    <th> F. Venta </th>
                    <th> F. Inicio </th>
                    <th> F. Fin </th>
                    <th> Admin. </th>
                    <th> Comercial </th>
                    <th> Empresa </th>
                    <th> Venta </th>
                    <th> Crédito Formativo </th>
                    <th> Crédito Validado </th>
                    <th> Comprobación cliente </th>
                    <th> Comrpobación venta </th>
                    <th> XML de Inicio </th>
                    <th> Inicio curso notificado </th>
                    <th> Factura Generada </th>
                    <th> Factura Enviada </th>
                    <th> XML de Fin </th>
                    <th> Fin curso notificado </th>
                    <th> I.B. enviado </th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
  
  listadoTabla('#tablaListadoVencimientos','../listadoAjax.php?include=vencimientos&funcion=listadoVencimientos();');
  $('.cajaFiltros select').selectpicker();
  oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaListadoVencimientos');
});

function oyenteChecksVencimientos(campo){
  var valor='NO';
  if(campo.prop('checked')){
    valor='SI';
  }

  var nombreCampo=campo.attr('id').replace(/\d/g,'');
  var codigoVencimiento=campo.attr('id').replace(/\D/g,'');

  $.post('../listadoAjax.php?include=vencimientos&funcion=cambiaEstadoLlamada();',{'valor':valor,'nombreCampo':nombreCampo,'codigoVencimiento':codigoVencimiento});
}
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
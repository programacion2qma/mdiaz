<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesDeclaraciones(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDeclaracion();
	}
	elseif(isset($_POST['nombreFichero'])){
		$res=insertaDeclaracion();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('declaraciones_predefinidos');
	}

	mensajeResultado('nombreFichero',$res,'Declaración predefinada');
    mensajeResultado('elimina',$res,'Declaración predefinada', true);
}

function insertaDeclaracion(){
	$res=true;
	$res=insertaDatos('declaraciones_predefinidos');
	$codigo=$res;
	$res=$res && insertaTransferencias($codigo);
	return $res;
}

function actualizaDeclaracion(){
	$res=true;
	$res=actualizaDatos('declaraciones_predefinidos');
	$codigo=$_POST['codigo'];
	$res=$res && insertaTransferencias($codigo);
	return $res;
}

function insertaTransferencias($codigo){
	$res=true;
	conexionBD();
	$res=consultaBD('DELETE FROM transferencias_internacionales_predefinidos WHERE codigoDeclaracion='.$codigo);
	$i=0;
	while(isset($_POST['paisTrans'.$i])){
		$res=consultaBD('INSERT INTO transferencias_internacionales_predefinidos VALUES(NULL,'.$codigo.',"'.$_POST['paisTrans'.$i].'","'.$_POST['categoriaTrans'.$i].'","'.$_POST['otrosTrans'.$i].'","'.$_POST['destNSadecuado'.$i].'","'.$_POST['destNSinadecuado'.$i].'","'.$_POST['otrosDestTI'.$i].'","'.$_POST['tiautorizacionAEPD'.$i].'")');
		$i++;
	}
	cierraBD();
	return $res;
}

function listadoDeclaraciones(){
	$columnas=array('nombreFichero','codigo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);//Uso de función personalizada
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, nombreFichero FROM declaraciones_predefinidos $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, nombreFichero FROM declaraciones_predefinidos $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['nombreFichero'],
			creaBotonDetalles('declaraciones-predefinidas/gestion.php?codigo='.$datos['codigo']),
			obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function gestionDeclaraciones(){
	operacionesDeclaraciones();

	abreVentanaGestion('Gestión de Declaraciones predefinidas','?','','icon-edit','',false,'formLOPD');
	$datos=compruebaDatos('declaraciones_predefinidos');


	creaPestaniasAPI($paginas=array('Responsable del Fichero','Datos para ejercer derechos','Finalidad del Fichero','Origen y Procedencia de los Datos','Tipos de Datos','Cesiones','Tratamiento','Transferencias internacionales'));
		
		if(isset($_POST['pestanaActiva'])){
			$pestana=$_POST['pestanaActiva'];
		} else {
			$pestana=1;
		}
		campoOculto($pestana,'pestanaActiva');

		abrePestaniaAPI(1,$pestana==1?true:false);
			abreColumnaCampos();
				campoRadio('datosResponsable','Distinto al declarante',$datos,'NO',array('No','Si'),array('NO','SI'));
			cierraColumnaCampos();
		cierraPestaniaAPI();

		abrePestaniaAPI(2,$pestana==2?true:false);
			campoRadio('datosDerecho','Copiar datos desde el',$datos,'NO',array('Declarante','Responsable','Ninguno'),array('SI','NO','0'));
		cierraPestaniaAPI();

		abrePestaniaAPI(3,$pestana==3?true:false);
			campoTexto('nombreFichero','Nombre fichero/tratamiento',$datos,'input-large maximoCaracteres');
			areaTexto('desc_fin_usos','Descripción de finalidad y usos',$datos,'areaInforme maximoCaracteres');
			campoCheckFinalidades($datos);
		cierraPestaniaAPI();

		abrePestaniaAPI(4,$pestana==4?true:false);
			campoCheckOrigenes($datos);
			campoCheckColectivos($datos);
		cierraPestaniaAPI();

		abrePestaniaAPI(5,$pestana==5?true:false);
			campoSelect('nivel','Nivel de medidas de seguridad',array('Básico','Medio','Alto'),array(1,2,3),$datos,'span2 selectpicker show-tick','');
			campoCheckDatosProtegidos($datos);
			campoCheckDatosEspeciales($datos);
			campoCheckDatosIdentificativos($datos);
			campoCheckOtrosDatos($datos);
		cierraPestaniaAPI();

		abrePestaniaAPI(6,$pestana==6?true:false);
			campoCheckCesiones($datos);
		cierraPestaniaAPI();

		abrePestaniaAPI(7,$pestana==7?true:false);
			abreColumnaCampos();
				areaTexto('tipoTratamiento','Tipo Tratamiento',$datos);
				areaTexto('baseJuridica','Base Jurídica del tratamiento',$datos);
			cierraColumnaCampos();
			abreColumnaCampos();			
				areaTexto('numInteresados','Nº de interesados afectados',$datos);
				areaTexto('plazo','Plazo o Criterio de conservación de la información',$datos);
				$tratamiento=$datos?$datos['tratamiento']:3;
				campoSelect('tratamiento','Sistema de tratamiento',array('Automatizado','Manual','Mixto'),array('1','2','3'),$tratamiento,'selectpicker span2 show-tick','');
			cierraColumnaCampos(true);
			echo '<div>';
			echo '<br/><h3 class="apartadoFormulario">Tipo Tratamiento</h3>';
			abreColumnaCampos();
				campoCheckIndividual('checkTipoTratamiento1','Recogida',$datos);
				campoCheckIndividual('checkTipoTratamiento4','Modificación',$datos);
				campoCheckIndividual('checkTipoTratamiento7','Consulta',$datos);
				campoCheckIndividual('checkTipoTratamiento10','Comunicación',$datos);
				campoCheckIndividual('checkTipoTratamiento13','Comunicación por transmisión al responsable del fichero',$datos);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoCheckIndividual('checkTipoTratamiento2','Registro',$datos);
				campoCheckIndividual('checkTipoTratamiento5','Conservación',$datos);
				campoCheckIndividual('checkTipoTratamiento8','Cotejo',$datos);
				campoCheckIndividual('checkTipoTratamiento11','Supresión',$datos);
				campoCheckIndividual('checkTipoTratamiento14','Comunicación a la Administración Pública competente',$datos);
			cierraColumnaCampos();
			abreColumnaCampos();
				campoCheckIndividual('checkTipoTratamiento3','Estructuración',$datos);
				campoCheckIndividual('checkTipoTratamiento6','Interconexión',$datos);
				campoCheckIndividual('checkTipoTratamiento9','Análisis de conducta',$datos);
				campoCheckIndividual('checkTipoTratamiento12','Destrucción',$datos);
				campoCheckIndividual('checkTipoTratamiento15','Comunicación permitida por ley',$datos);
			cierraColumnaCampos();
			echo '</div>';
		cierraPestaniaAPI();

		abrePestaniaAPI(8,$pestana==8?true:false);
			creaTablaDestinatarios($datos);
		cierraPestaniaAPI();		
	cierraPestaniasAPI();;
		
	cierraVentanaGestion('index.php');
}

function campoCheckFinalidades($datos){
	$finalidadesAgencia=array('400'=>'Gestión de Clientes, Contable, Fiscal y Administrativa','401'=>'Recursos Humanos','402'=>'Gestión de Nóminas','403'=>'Prevención de Riesgos Laborales','404'=>'Prestación de Servicios de Solvencia Patrimonial y Crédito','405'=>'Cumplimiento/Incumplimiento de Obligaciones Dinerarias','406'=>'Servicios Económicos-Financieros y Seguros','407'=>'Análisis de Perfiles','408'=>'Publicidad y Prospección Comercial','409'=>'Prestación de Servicios de Comunicaciones Electrónicas','410'=>'Guías/Repertorios de Servicios de Comunicaciones Electrónicas','411'=>'Comercio Electrónico','412'=>'Prestación de Servicios de Certificación Electrónica','413'=>'Gestión de Asociados o Miembros de Partidos Políticos, Sindicatos, Iglesias, Confesiones o Comunidades Religiosas y Asociaciones, Fundaciones y tras Entidades sin Ánimo de Lucro, cuya finalidad sea Política, Filosófica, Religiosa o Sindical','414'=>'Gestión de Actividades Asociativas, Culturales, Recreativas, Deportivas y Sociales','415'=>'Gestión de Asistencia Social','416'=>'Educación','417'=>'Investigación Epidemiológica y Actividades Análogas','418'=>'Gestión y Control Sanitario','419'=>'Historial Clínico','420'=>'Seguridad Privada','421'=>'Seguridad y Control de acceso a edificios','422'=>'Videovigilancia','423'=>'Fines Estadísticos, Históricos o Científicos','499'=>'Otras Finalidades');
	$valores=array();
	$nombres=array();

	foreach ($finalidadesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	echo "<div id='cajaFinalidades'>";
	campoCheck('checkFinalidad','Finalidades (máximo 6)',$datos,$nombres,$valores,true);
	echo "</div>";
}

function campoCheckOrigenes($datos){
	$origenesAgencia=array('1'=>'El propio interesado o su representante legal','2'=>'Otras personas físicas','3'=>'Fuentes accesibles al público','4'=>'Registros públicos','5'=>'Entidad privada','6'=>'Administraciones Públicas');
	$valores=array();
	$nombres=array();

	foreach ($origenesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	campoCheck('checkOrigen','Orígenes',$datos,$nombres,$valores,true);
}

function campoCheckColectivos($datos){
	$colectivosAgencia=array('01'=>'Empleados','02'=>'Clientes y Usuarios','03'=>'Proveedores','04'=>'Asociados o Miembros','05'=>'Propietarios o Arrendatarios','06'=>'Pacientes','07'=>'Estudiantes','08'=>'Personas de Contacto','09'=>'Padres o Tutores','10'=>'Representante Legal','11'=>'Solicitantes','12'=>'Beneficiarios','13'=>'Cargos Públicos','OTRO'=>'Otro colectivo');
	$valores=array();
	$nombres=array();

	foreach ($colectivosAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	echo "<div id='cajaColectivos'>";
	campoCheck('checkColectivo','Colectivos (máximo 6)',$datos,$nombres,$valores,true);
	echo "</div>";

	echo "<div id='cajaOtroColectivo' class='hide'>";
	areaTexto('otro_col','Otro colectivo',$datos);
	echo "</div>";
}

function campoCheckDatosProtegidos($datos){
	$datosProtegidos=array('1'=>'Ideología','2'=>'Afiliación sindical','3'=>'Religión','4'=>'Creencias');
	$valores=array();
	$nombres=array();

	foreach ($datosProtegidos as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	campoCheck('checkDatosProtegidos','Datos especialmente protegidos',$datos,$nombres,$valores,true);
}

function campoCheckDatosEspeciales($datos){
	$datosEspeciales=array('1'=>'Origen racial o Étnico','2'=>'Salud','3'=>'Vida sexual');
	$valores=array();
	$nombres=array();

	foreach ($datosEspeciales as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	campoCheck('checkDatosEspeciales','Otros datos especialmente protegidos',$datos,$nombres,$valores,true);
}


function campoCheckDatosIdentificativos($datos){
	$datosIdentificativosAgencia=array('1'=>'NIF / DNI','2'=>'Nº SS / Mutualidad','3'=>'Nombre y apellidos','4'=>'Tarjeta sanitaria','5'=>'Dirección','6'=>'Teléfono','7'=>'Firma','8'=>'Huella','9'=>'Imagen / voz','10'=>'Marcas físicas','11'=>'Firma electrónica','13'=>'Correo electrónico','12'=>'Otros datos biométricos','otroIden'=>'Otros datos de carácter identificativo');
	$valores=array();
	$nombres=array();

	foreach ($datosIdentificativosAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoCheck('checkDatosIdentificativos','Datos de carácter identificativo',$datos,$nombres,$valores,true);

	echo "<div id='cajaOtroIdentificativo' class='hide'>";
	areaTexto('ODCI','Otros datos',$datos,'areaTexto maximoCaracteres');
	echo "</div>";
}


function campoCheckOtrosDatos($datos){
	$datosTipificadosAgencia=array('1'=>'Características Personales','2'=>'Académicos y profesionales','3'=>'Detalles del empleo','4'=>'Económicos, financieros y de seguros');
	$valores=array();
	$nombres=array();

	foreach ($datosTipificadosAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoRadio('otrosTiposDatos','Otros tipos de datos',$datos);

	echo "<div id='cajaOtrosDatosTipificados' class='hide'>";
	campoCheck('checkOtrosTiposDatos','Tipos',$datos,$nombres,$valores,true);
	areaTexto('desc_otros_tipos','Otros datos',$datos);
	echo "</div>";
}

function campoCheckCesiones($datos){
	$destinatariosAgencia=array('01'=>'Organizaciones o Personas Directamente Relacionadas con el Responsable','02'=>'Organismos de la Seguridad Social','03'=>'Registros Públicos','04'=>'Colegios Profesionales','05'=>'Administración Tributaria','06'=>'Otros Órganos de la Administración Pública','07'=>'Comisión Nacional del Mercado de Valores','08'=>'Comisión Nacional del Juego','09'=>'Notarios y Procuradores','10'=>'Fuerzas y Cuerpos de Seguridad','11'=>'Organismos de la Unión Europea','12'=>'Entidades Dedicadas al Cumplimiento o Incumplimiento de Obligaciones Dinerarias','13'=>'Bancos, Cajas de Ahorros y Cajas Rurales','14'=>'Entidades Aseguradoras','15'=>'Otras Entidades Financieras','16'=>'Entidades Sanitarias','17'=>'Prestaciones de Servicios de Telecomunicaciones','18'=>'Empresas Dedicadas a Publicidad o Marketing Directo','19'=>'Asociaciones y Organizaciones sin Ánimo de Lucro','20'=>'Sindicatos y Juntas de Personal','21'=>'Administración Pública con Competencia en la Materia','21'=>'Otros destinatarios de cesiones');
	$valores=array();
	$nombres=array();

	foreach ($destinatariosAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoCheck('checkDestinatariosCesiones','Categorías de destinatarios de cesiones',$datos,$nombres,$valores,true);

	echo "<div id='cajaOtrosDestintarios' class='hide'>";
	areaTexto('desc_otros','Otros destinatarios de cesiones',$datos);
	echo "</div>";
}

function creaTablaDestinatarios($datos){
	conexionBD();

	echo "	                 
			<div class='table-responsive centroDestinatarios'>
				<table class='table table-striped tabla-simple' id='tablaDestinatarios'>
				  	<thead>
				    	<tr>
				            <th> Destinatarios en países con nivel de protección adecuado </th>
				            <th> Destinatarios en países sin nivel de protección adecuado </th>
				            <th> Otros destinatarios de Transferencias Internacionales </th>
				            <th> Transferencias internacionales con autorización del Director de la AEPD </th>				            
				            <th> País </th>
				            <th> Categoría </th>
				            <th> Otros </th>
				            <th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		$j=1;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM transferencias_internacionales_predefinidos WHERE codigoDeclaracion=".$datos['codigo']);
				  			while($item=mysql_fetch_assoc($consulta)){
				  				echo '<tr>';
				  					campoTextoTabla('destNSadecuado'.$i,$item['destNSadecuado'],'input-small');
				  					campoTextoTabla('destNSinadecuado'.$i,$item['destNSinadecuado'],'input-small');
				  					campoTextoTabla('otrosDestTI'.$i,$item['otrosDestTI'],'input-small');
				  					campoTextoTabla('tiautorizacionAEPD'.$i,$item['tiautorizacionAEPD'],'input-small');
				  					campoSelectPaisesAEPDTabla('paisTrans'.$i,$item['pais']);
				  					campoDestinatarios('categoriaTrans'.$i,$item['categoria']);
				  					areaTextoTabla('otrosTrans'.$i,$item['otros']); 
				  				echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
				  				</tr>";
				  				$i++;
				  				$j++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr>';
				  					campoTextoTabla('destNSadecuado'.$i,'','input-small');
				  					campoTextoTabla('destNSinadecuado'.$i,'','input-small');
				  					campoTextoTabla('otrosDestTI'.$i,'','input-small');
				  					campoTextoTabla('tiautorizacionAEPD'.$i,'','input-small');
				  					campoSelectPaisesAEPDTabla('paisTrans'.$i);
				  					campoDestinatarios('categoriaTrans'.$i);
				  					areaTextoTabla('otrosTrans'.$i);
				  			echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
							</tr>";
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaDestinatarios\");'><i class='icon-plus'></i> Añadir destinatario</button>
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaDestinatarios\");'><i class='icon-trash'></i> Eliminar destinatario</button> 
				</div>
			</div>";

	cierraBD();
}

function campoSelectPaisesAEPDTabla($nombreCampo,$datos=false){
	$valores=array();
	$nombres=array();
	$paisesAgencia=array('VP'=>'INTERNACIONAL','AF'=>'AFGANISTAN','AL'=>'ALBANIA','DE'=>'ALEMANIA','AD'=>'ANDORRA','AO'=>'ANGOLA','AG'=>'ANTIGUA Y BARBUDA','SA'=>'ARABIA SAUDI','DZ'=>'ARGELIA','AR'=>'ARGENTINA','AM'=>'ARMENIA','AU'=>'AUSTRALIA','AT'=>'AUSTRIA','AZ'=>'AZERBAIYAN','BS'=>'BAHAMAS','BH'=>'BAHREIN','BD'=>'BANGLADESH','BB'=>'BARBADOS','BY'=>'BIELORRUSIA','BE'=>'BELGICA','BZ'=>'BELICE','BJ'=>'BENIN','BT'=>'BHUTÁN','BO'=>'BOLIVIA','BA'=>' BOSNIA HERZEGOVINA','BW'=>'BOTSWANA','BR'=>'BRASIL','BN'=>'BRUNEI DARUSSALAM','BG'=>'BULGARIA','BF'=>'BURKINA FASO','BI'=>'BURUNDI','CV'=>'CABO VERDE','KH'=>'CAMBOYA','CM'=>'CAMERUN','CA'=>'CANADA','TD'=>'CHAD','CL'=>'CHILE','CN'=>'CHINA','CY'=>'CHIPRE','CO'=>'COLOMBIA','KM'=>'COMORAS','CG'=>'CONGO','CI'=>'COSTA DE MARFIL','CR'=>'COSTA RICA','HR'=>'CROACIA','CU'=>'CUBA','DK'=>'DINAMARCA DJ DJIBOUTI','DM'=>'DOMINICA','EC'=>'ECUADOR','EG'=>'EGIPTO','SV'=>'EL SALVADOR','AE'=>'EMIRATOS ARABES','ER'=>'ERITREA','SI'=>'ESLOVENIA','ES'=>'ESPAÑA','US'=>'ESTADOS UNIDOS','EP'=>'EEUU - ESCUDO PRIVACIDAD EE ESTONIA','ET'=>'ETIOPIA','RU'=>'FEDERACIÓN DE RUSIA','FJ'=>'FIJI','PH'=>'FILIPINAS','FI'=>'FINLANDIA','FR'=>'FRANCIA','GA'=>'GABON','GM'=>'GAMBIA','GE'=>'GEORGIA','GH'=>'GHANA','GD'=>'GRANADA','GR'=>'GRECIA','GL'=>'GROENLANDIA','GT'=>'GUATEMALA','GG'=>'GUERNSEY','GN'=>'GUINEA','GW'=>'GUINEA BISSAU','GQ'=>'GUINEA ECUATORIAL','GY'=>'GUYANA','HT'=>'HAITI','HN'=>'HONDURAS','HK'=>'HONG KONG','HU'=>'HUNGRIA','IN'=>'INDIA','ID'=>'INDONESIA','IR'=>'IRAN','IQ'=>'IRAQ','IE'=>'IRLANDA','IM'=>'ISLA DE MAN','IS'=>'ISLANDIA','FO'=>'ISLAS FEROE','MH'=>'ISLAS MARSHALL','SB'=>'ISLAS SALOMÓN','IL'=>'ISRAEL','IT'=>'ITALIA','JM'=>'JAMAICA','JP'=>'JAPON','JO'=>'JORDANIA','KZ'=>'KAZAJSTÁN','KE'=>'KENIA','KG'=>'KIRGUISTÁN','KI'=>'KIRIBATI','KW'=>'KUWAIT','LS'=>'LESOTHO','LV'=>'LETONIA','LB'=>'LIBANO','LR'=>'LIBERIA','LY'=>'LIBIA','LI'=>'LIECHTENSTEIN','LT'=>'LITUANIA','LU'=>'LUXEMBURGO','MK'=>'MACEDONIA','MG'=>'MADAGASCAR','MY'=>'MALASIA','MW'=>'MALAWI','MV'=>'MALDIVAS','ML'=>'MALÍ','MT'=>'MALTA','MA'=>'MARRUECOS','MU'=>'MAURICIO','MR'=>'MAURITANIA','MX'=>'MEJICO','FM'=>'MICRONESIA','MC'=>'MONACO','MN'=>'MONGOLIA','ME'=>'MONTENEGRO','MZ'=>'MOZAMBIQUE','MM'=>'MYANMAR','NA'=>'NAMIBIA','NR'=>'NAURU','NP'=>'NEPAL','NI'=>'NICARAGUA','NE'=>'NÍGER','NG'=>'NIGERIA','NO'=>'NORUEGA','NZ'=>'NUEVA ZELANDA','OM'=>'OMÁN','NL'=>'PAISES BAJOS','PW'=>'PALAU','PA'=>'PANAMA','PG'=>'PAPUA NUEVA GUINEA','PK'=>'PAQUISTAN','PY'=>'PARAGUAY','PE'=>'PERU','PL'=>'POLONIA','PT'=>'PORTUGAL','PR'=>'PUERTO RICO','QA'=>'QATAR','GB'=>'REINO UNIDO','CF'=>'REP. CENTROAFRICANA','CZ'=>'REPÚBLICA CHECA','KR'=>'REPÚBLICA DE COREA','DO'=>'REPÚBLICA DOMINICANA','MD'=>'REPÚBLICA DE MOLDOVA','CD'=>'REP. DEM. DEL CONGO','KP'=>'REP. DEM. POP. DE COREA','LA'=>'REP. DEM. POP. DE LAOS','TL'=>'REP. DEM. DE TIMOR-LESTE','SK'=>'REPÚBLICA ESLOVACA','RO'=>'RUMANIA','RW'=>'RWANDA','KN'=>'SAINT KITTS Y NEVIS','WS'=>'SAMOA','SM'=>'SAN MARINO','VC'=>'S.VICENTE Y GRANADINAS','LC'=>'SANTA LUCÍA','ST'=>'SANTO TOMÉ Y PRÍNCIPE','SN'=>'SENEGAL','RS'=>'SERBIA','SC'=>'SEYCHELLES','SL'=>'SIERRA LEONA','SG'=>'SINGAPUR','SY'=>'SIRIA','SO'=>'SOMALIA','LK'=>'SRI LANKA','ZA'=>'SUDAFRICA','SD'=>'SUDAN','SE'=>'SUECIA','CH'=>'SUIZA','SR'=>'SURINAM','SZ'=>'SWAZILANDIA','TH'=>'TAILANDIA','TW'=>'TAIWAN','TZ'=>'TANZANIA','TJ'=>'TAYIKISTÁN','TG'=>'TOGO','TO'=>'TONGA','TT'=>'TRINIDAD Y TOBAGO','TN'=>'TUNEZ','TM'=>'TURKMENISTÁN','TR'=>'TURQUIA','TV'=>'TUVALU','UA'=>'UCRANIA','UG'=>'UGANDA','UY'=>'URUGUAY','UZ'=>'UZBEKISTÁN','VU'=>'VANUATU','VE'=>'VENEZUELA','VN'=>'VIETNAM','YE'=>'YEMEN','YU'=>'YUGOSLAVIA','ZR'=>'ZAIRE','ZM'=>'ZAMBIA','ZW'=>'ZIMBABWE');

	
	foreach ($paisesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}
	if($datos==false){
		$datos[$nombreCampo]='ES';
	}
	campoSelect($nombreCampo,'País',$nombres,$valores,$datos,'selectpicker span3 show-tick',"data-live-search='true'",1);
}

function campoDestinatarios($nombreCampo,$datos=false){
	$valores=array();
	$nombres=array();
	$paisesAgencia=array('01'=>'COMPAÑIAS FILIALES','02'=>'COMPAÑIA MATRIZ','03'=>'COMPAÑIAS DEL GRUPO','04'=>'PRESTADORES DE SERVICIO','05'=>'UNIVERSIDADES Y CENTROS EDUCATIVOS','06'=>'ÓRGANOS PÚBLICOS DE OTROS ESTADOS','07'=>'ORGANISMOS INTERNACIONALES','08'=>'ENTIDADES SANITARIAS','09'=>'ÓRGANOS JUDICIALES','10'=>'ENTIDADES FINANCIERAS','11'=>'DATOS DE PASAJEROS CON DESTINO A LOS ORGANISMOS DE FRONTERAS DE EEUU Y CANADÁ');
	foreach ($paisesAgencia as $codigo => $nombre) {
		array_push($valores,$codigo);
		array_push($nombres,$nombre);
	}

	campoSelect($nombreCampo,'Destinatarios',$nombres,$valores,$datos,'selectpicker span3 show-tick',"data-live-search='true'",1);
}



function filtroDeclaraciones(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nombre fichero','','span3');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

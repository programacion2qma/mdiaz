<?php
  $seccionActiva=70;
  include_once("../cabecera.php");
  gestionDeclaraciones();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>js/maskedinput.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	oyenteOtroColectivo();
		$('input[type=checkbox][value=OTRO]').change(function(){
			oyenteOtroColectivo();
		});

		oyenteOtroIdentificativo();
		$('input[type=checkbox][value=otroIden]').change(function(){
			oyenteOtroIdentificativo();
		});

		oyenteOtroTipificado();
		$('input[type=radio][name=otrosTiposDatos]').change(function(){
			oyenteOtroTipificado();
		});

		oyenteOtrosDestinatarios();
		$('input[type=checkbox][value=21]').change(function(){
			oyenteOtrosDestinatarios();
		});
});

function oyenteOtroColectivo(){
		var check=$('input[type=checkbox][value=OTRO]');
		if(check.is(':checked')){
			$('#cajaOtroColectivo').removeClass('hide');
		}
		else{
			$('#cajaOtroColectivo').addClass('hide');
		}
	}
	
	function oyenteOtroIdentificativo(){
		var check=$('input[type=checkbox][value=otroIden]');
		if(check.is(':checked')){
			$('#cajaOtroIdentificativo').removeClass('hide');
		}
		else{
			$('#cajaOtroIdentificativo').addClass('hide');
		}
	}

	function oyenteOtroTipificado(){
		var check=$('input[type=radio][name=otrosTiposDatos]:checked').val();
		if(check=='SI'){
			$('#cajaOtrosDatosTipificados').removeClass('hide');
		}
		else{
			$('#cajaOtrosDatosTipificados').addClass('hide');
		}
	}

	function oyenteOtrosDestinatarios(){
		var check=$('input[type=checkbox][value=21]');
		if(check.is(':checked')){
			$('#cajaOtrosDestintarios').removeClass('hide');
		}
		else{
			$('#cajaOtrosDestintarios').addClass('hide');
		}
	}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesDerechos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDerecho();
	}
	elseif(isset($_POST['interesado'])){
		$res=insertaDerecho();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('derechos');
	}

	mensajeResultado('interesado',$res,'Derecho');
    mensajeResultado('elimina',$res,'Derecho', true);
}

function insertaDerecho(){
	$res=true;
	responsable();
	$res=insertaDatos('derechos');
	return $res;
}

function actualizaDerecho(){
	$res=true;
	responsable();
	$res=actualizaDatos('derechos');
	return $res;
}

function responsable(){
	$_POST['responsable']='';
	$_POST['responsableDireccion']='';
	if($_POST['respuesta']==3 && $_POST['selectResponsable']!='NULL'){
		$i=$_POST['selectResponsable'];
		$_POST['responsable']=$_POST['responsable'.$i];
		$_POST['responsableDireccion']=$_POST['direccion'.$i];
	}
}

function listadoDerechos(){
	global $_CONFIG;

	$columnas=array('interesado','fechaRecepcion','fechaRespuesta','respuesta','tipo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente." AND tipo='Limitación del tratamiento'",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having;");
	cierraBD();
	$respuestas=array('',
	'La solicitud no reúne los requisitos de identificación',
	'No figuran datos personales del interesado',
	'Comunicación al Responsable de Tratamientos',
	'Datos personales bloqueados',
	'Denegación a la limitación del tratamiento solicitada',
	'Otorgamiento a la limitación del tratamiento solicitada');
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$otrosDatos="";
		if($datos['respuesta']==7 && $datos['otrosDatos']!=''){
			$otrosDatos=' - '.$datos['otrosDatos'];
		}
		$opciones=array('Detalles','Descargar respuesta');
		$enlaces=array("zona-cliente-derechos-limitacion/gestion.php?codigo=".$datos['codigo'],"zona-cliente-derechos-limitacion/generaRespuesta.php?codigo=".$datos['codigo']);
		$iconos=array('icon-search-plus','icon-cloud-download');
		if($datos['visualizacion']=='SI'){
			array_push($opciones,'Descargar comunicación a cesionario');
			array_push($enlaces,"zona-cliente-derechos-limitacion/generaRespuesta.php?cesionario&codigo=".$datos['codigo']);
			array_push($iconos,'icon-cloud-download');
		}
		$fila=array(
			$datos['referencia'],
			$datos['interesado'],
			formateaFechaWeb($datos['fechaRecepcion']),
			formateaFechaWeb($datos['fechaRespuesta']),
			$respuestas[$datos['respuesta']].$otrosDatos,
			botonAcciones($opciones,$enlaces,$iconos),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionDerechos(){
	operacionesDerechos();

	abreVentanaGestion('Gestión de interesados que ejercen el derecho de limitación del tratamiento','?','','icon-edit','margenAb');
	$datos=compruebaDatos('derechos');

    if(!$datos){
    	$codigoCliente=obtenerCodigoCliente(true);
    	$referencia=obtieneReferenciaDerecho('Limitación del tratamiento',$codigoCliente);
    	campoOculto($codigoCliente,'codigoCliente');
    }  else {
    	$codigoCliente=$datos['codigoCliente'];
    	$referencia=$datos['referencia'];
    }

    campoOculto('','horaVisualizacion');
    campoOculto('','otrosDatos');

	abreColumnaCampos();
		campoOculto('Limitación del tratamiento','tipo');
		campoTexto('referencia','Referencia',$referencia,'input-mini',true);
   		campoTexto('interesado','Nombre y apellido del interesado',$datos,'span7');
   		campoTexto('interesadoDni','NIF',$datos,'input-small');
   		campoTexto('representante','Representante legal',$datos,'span7');
   		campoTexto('representanteDni','NIF',$datos,'input-small');
   		campoTexto('direccion','Dirección a efecto de notificaciones',$datos,'span7');
   		campoTexto('email','Correo electrónico',$datos,'span7');
   	cierraColumnaCampos(true);
   	abreColumnaCampos();
		campoFecha('fechaRecepcion','Fecha de Recepción',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoFecha('fechaRespuesta','Fecha de Respuesta',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		campoRadio('mediosElectronicos','La solicitud se ha presentado por medios electrónicos',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		echo '<div id="divMediosDiferente" class="hide">';
			campoRadio('mediosDiferente','El interesado solicita que se le responda por otros medios diferentes al correo electrónico',$datos);
		echo '</div>';
	cierraColumnaCampos();
	abreColumnaCampos();
		echo '<div id="divOtroMedio" class="hide">';
			campoTexto('otroMedio','Indicar',$datos);
		echo '</div>';
	cierraColumnaCampos(true);
	abreColumnaCampos();
		$select=rellenaSelect();
		campoSelect('respuesta','Respuesta',$select['nombres'],$select['valores'],$datos,'selectpicker span6 show-tick');
	cierraColumnaCampos(true);
	$responsables=array();
	$direcciones=array();
	conexionBD();
	$declaraciones=consultaBD('SELECT * FROM declaraciones WHERE codigoCliente='.$codigoCliente);
	while($d=mysql_fetch_assoc($declaraciones)){
		if(!in_array($d['n_razon'],$responsables)){
			array_push($responsables,$d['n_razon']);
			array_push($direcciones,$d['dir_postal_responsableFichero'].', CP '.$d['postal_responsableFichero'].', '.$d['localidad_responsableFichero'].', '.$d['provincia_responsableFichero']);
		}
		$otros=consultaBD('SELECT * FROM responsables_fichero WHERE codigoDeclaracion='.$d['codigo']);
		while($o=mysql_fetch_assoc($otros)){
			if($o['razonSocial']!='' && !in_array($o['razonSocial'],$responsables)){
				array_push($responsables,$o['razonSocial']);
				array_push($direcciones,$o['direccion'].', CP '.$o['cp'].', '.$o['localidad'].', '.$o['provincia']);
			}
		}
	}
	cierraBD();
	abreColumnaCampos('span3 divResponsable hide');
		$nombres=array('');
		$valores=array('NULL');
		foreach ($responsables as $key => $value) {
			campoOculto($value,'responsable'.$key);
			array_push($nombres,$value);
			array_push($valores,$key);
		}
		foreach ($direcciones as $key => $value) {
			campoOculto($value,'direccion'.$key);
		}
		if($datos && $datos['responsable']!=''){
			campoDato('Responsable de tratamiento',$datos['responsable']);
		}
		campoSelect('selectResponsable','Selecciona responsable',$nombres,$valores);
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divOtrosDatos hide');
		echo '<a id="art11" style="margin-left:85px" class="btnModal noAjax">artículo 11 RGPD</a><br/>';
		echo '<a id="art12" style="margin-left:85px" class="btnModal noAjax">artículo 12.6 RGPD</a>';
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divOtrosDatos1 hide');
		echo '<a id="art181" style="margin-left:85px" class="btnModal noAjax">artículo 18.1 RGPD</a>';
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divOtrosDatos2 hide');
		echo '<a id="art18" style="margin-left:85px" class="btnModal noAjax">artículo 18 RGPD</a>';
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divTratamiento hide');
		campoSelectConsulta('codigoTratamiento','Fichero/Tratamiento','SELECT codigo, nombreFichero AS texto FROM declaraciones WHERE codigoCliente='.$codigoCliente,$datos);
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoRadio('visualizacion','Previamente a la solicitud de limitación del tratamiento, ¿se cedieron los datos objeto de la limitación? <a id="art19" class="btnModal noAjax">-artículo 19 RGPD-</a>',$datos,'NO',array('No','Sí'),array('NO','SI'));
		echo '<div id="divProcede" class="hide">';
			campoTexto('nombreCesionario','Nombre del cesionario',$datos);
			campoFecha('fechaVisualizacion','Fecha comunicación al cesionario',$datos);
		echo '</div>';
	cierraColumnaCampos(true);

	abreColumnaCampos();
		areaTexto('observaciones','Observaciones',$datos,'areaInforme'); 
	cierraColumnaCampos(true);

	cierraVentanaGestion('index.php',true);

	abreVentanaModal('información','cajaGestion11');
	echo '<i><b>Artículo 11 RGPD</b>. Tratamiento que no requiere identificación.<br/><br/>
	1. Si los fines para los cuales un responsable trata datos personales no requieren o ya no requieren la identificación de un interesado por el responsable, este no estará obligado a mantener, obtener o tratar información adicional con vistas a identificar al interesado con la única finalidad de cumplir el presente Reglamento.<br/><br/>
	2. Cuando, en los casos a que se refiere el apartado 1 del presente artículo, el responsable sea capaz de demostrar que no está en condiciones de identificar al interesado, le informará en consecuencia, de ser posible. En tales casos no se aplicarán los artículos 15 a 20, excepto cuando el interesado, a efectos del ejercicio de sus derechos en virtud de dichos artículos, facilite información adicional que permita su identificación.</i>';
	cierraVentanaModal('','','',false,'Cerrar');

	abreVentanaModal('información','cajaGestion12');
	echo '<i><b>Artículo 12.6 RGPD.</b> 6. Sin perjuicio de lo dispuesto en el artículo 11, cuando el responsable del tratamiento tenga dudas razonables en relación con la identidad de la persona física que cursa la solicitud a que se refieren los artículos 15 a 21, podrá solicitar que se facilite la información adicional necesaria para confirmar la identidad del interesado.</i>';
	cierraVentanaModal('','','',false,'Cerrar');

	abreVentanaModal('información','cajaGestion18');
	echo '<i><b>Artículo 18 RGPD.</b> Derecho a la limitación del tratamiento.<br/><br/>
	1. El interesado tendrá derecho a obtener del responsable del tratamiento la limitación del tratamiento de los datos cuando se cumpla alguna de las condiciones siguientes:<br/><br/>
	a) el interesado impugne la exactitud de los datos personales, durante un plazo que permita al responsable verificar la exactitud de los mismos;<br/><br/>
	b) el tratamiento sea ilícito y el interesado se oponga a la supresión de los datos personales y solicite en su lugar la limitación de su uso;<br/><br/>
	c) el responsable ya no necesite los datos personales para los fines del tratamiento, pero el interesado los necesite para la formulación, el ejercicio o la defensa de reclamaciones;<br/><br/>
	d) el interesado se haya opuesto al tratamiento en virtud del artículo 21, apartado 1, mientras se verifica si los motivos legítimos del responsable prevalecen sobre los del interesado.<br/><br/>
	2. Cuando el tratamiento de datos personales se haya limitado en virtud del apartado 1, dichos datos solo podrán ser objeto de tratamiento, con excepción de su conservación, con el consentimiento del interesado o para la formulación, el ejercicio o la defensa de reclamaciones, o con miras a la protección de los derechos de otra persona física o jurídica o por razones de interés público importante de la Unión o de un determinado Estado miembro.<br/><br/>
	3. Todo interesado que haya obtenido la limitación del tratamiento con arreglo al apartado 1 será informado por el responsable antes del levantamiento de dicha limitación</i>';
	cierraVentanaModal('','','',false,'Cerrar');

	abreVentanaModal('información','cajaGestion19');
	echo '<i><b>Artículo 19 RGPD. Obligación de notificación relativa a la rectificación o supresión de
datos personales o la limitación del tratamiento.</b> El responsable del tratamiento
comunicará cualquier rectificación o supresión de datos personales o limitación del
tratamiento efectuada con arreglo al artículo 16, al artículo 17, apartado 1, y al artículo 18
a cada uno de los destinatarios a los que se hayan comunicado los datos personales, salvo
que sea imposible o exija un esfuerzo desproporcionado. El responsable informará al
interesado acerca de dichos destinatarios, si este así lo solicita.</i>';
	cierraVentanaModal('','','',false,'Cerrar');
}

function rellenaSelect(){
	$res=array('valores'=>array(0,1,2,3,4,5,6),'nombres'=>array('',
	'La solicitud no reúne los requisitos de identificación ',
	'No figuran datos personales del interesado',
	'Comunicación al Responsable de Tratamientos',
	'Datos personales bloqueados',
	'Denegación a la limitación del tratamiento solicitada',
	'Otorgamiento a la limitación del tratamiento solicitada'));
	return $res;
}

function wordInteresadoDerechos($PHPWord,$codigoDerecho){
    global $_CONFIG;
    $datos=consultaBD('SELECT derechos.*, clientes.razonSocial, CONCAT(clientes.domicilio,", CP ",clientes.cp,", ",clientes.localidad,", ",clientes.provincia) AS direccionCliente, ficheroLogo, administrador FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo WHERE derechos.codigo='.$codigoDerecho,true,true);
    $tipo=$datos['tipo'];
    $ref=$datos['referencia']<10?'0'.$datos['referencia']:$datos['referencia'];
    $fichero='RESPUESTA_DERECHO_'.mb_strtoupper($tipo).'_INTERESADO_REF_'.$ref.'.docx';
    $documento=$PHPWord->loadTemplate('../documentos/derechos/plantillaDerechos.docx');
    $documento->setValue("true","");
    $documento->setValue("cliente",utf8_decode(sanearCaracteresDerechos($datos['razonSocial'])));  
    $documento->setValue("titulo",utf8_decode("RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO ".$datos['interesado']." REF. ".$ref));

    reemplazarLogoDerechos($documento,$datos,'image2.png'); 
    $respuestas=array('','LA SOLICITUD NO REÚNE LOS REQUISITOS NECESARIOS DE IDENTIFICACIÓN','NO FIGURAN DATOS PERSONALES DEL INTERESADO EN LOS FICHEROS DEL RESPONSABLE','COMUNICACIÓN AL RESPONSABLE DE TRATAMIENTOS ','DATOS PERSONALES BLOQUEADOS','DENEGACIÓN DE LIMITACIÓN DEL TRATAMIENTO','OTORGAMIENTO DE LIMITACIÓN DEL TRATAMIENTO');
    if($datos['respuesta']==3){
        $texto="RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": ".$respuestas[$datos['respuesta']]."<w:br/><w:br/>".$datos['razonSocial'].", en concepto de encargado del tratamiento de datos personales, de los cuales es responsable del tratamiento ".$datos['responsable'].", mediante el presente escrito es para dar respuesta a la petición de limitación del tratamiento recibida el día ".formateaFechaWeb($datos['fechaRecepcion'])." realizada por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['responsableDireccion']."<w:br/><w:br/>";
    } else {
        $texto="RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": ".$respuestas[$datos['respuesta']]."<w:br/><w:br/>El presente escrito es para dar respuesta a la petición de limitación del tratamiento de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['direccion']."<w:br/><w:br/>";
        
    }
    if($datos['representante']!=''){
        $texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    } else {
        $texto.=$datos['interesado'];
    }
    $texto.=$direccion;
    switch($datos['respuesta']){
        case 1:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que no se puede llevar a cabo la limitación del tratamiento solicitada debido a que no puede identificar al interesado/representación legal del interesado -artículos 11 y 12.6 del Reglamento General de Protección de Datos (UE)-; no obstante, si desea que el responsable del tratamiento proceda a la limitación del tratamiento interesada, puede facilitarnos información adicional que permita su identificación/representación legal del interesado:<w:br/><w:br/>1. Nombre y apellidos del interesado; fotocopia de su documento nacional de identidad, o de su pasaporte u otro documento válido que lo identifique y, en su caso, de la persona que lo represente, o instrumentos electrónicos equivalentes; así como el documento o instrumento electrónico acreditativo de tal representación. La utilización de firma electrónica identificativa del afectado eximirá de la presentación de las fotocopias del DNI o documento equivalente.<w:br/>2. Petición en que se concreta la solicitud<w:br/>3. Dirección a efectos de notificaciones, fecha y firma del solicitante";
        break;

        case 2:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>No figuran datos personales del interesado/a en nuestros sistemas de tratamiento de datos, por lo que no podemos proceder a la limitación del tratamiento de datos solicitada por el/la interesado/a";
        break;

        case 3:
        $texto.="Se adjunta copia de la solicitud de limitación del tratamiento de datos recibida, a fin de que el/la responsable del tratamiento resuelva sobre lamisma.";
        break;

        case 4:
        $texto.=$datos['razonSocial']." en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/>Los datos personales del interesado se encuentran bloqueados, por lo que no pueden ser tratados excepto para su puesta a disposición de las Administraciones Públicas competentes, Jueces y Tribunales y Ministerio Fiscal. Una vez transcurrido el plazo de prescripción que corresponda, los datos personales bloqueados son eliminados de nuestros sistemas de tratamiento/archivos –\"Artículo 5.1.e) RGPD (limitación del plazo de conservación) y artículo 32 LOPD-GDD (\"Bloqueo de los datos. 1. El responsable del tratamiento estará obligado a bloquear los datos cuando proceda a su rectificación o supresión. 2. El bloqueo de los datos consiste en la identificación y reserva de los mismos, adoptando medidas técnicas y organizativas, para impedir su tratamiento, incluyendo su visualización, excepto para la puesta a disposición de los datos a los jueces y tribunales, el Ministerio Fiscal o las Administraciones Públicas competentes, en particular de las autoridades de protección de datos, para la exigencia de posibles responsabilidades derivadas del tratamiento y solo por el plazo de prescripción de las mismas. Transcurrido ese plazo deberá procederse a la destrucción de los datos. 3. Los datos bloqueados no podrán ser tratados para ninguna finalidad distinta de la señalada en el apartado anterior.\")-.";
        break;

        case 5:
        $texto.=$datos['razonSocial'].", en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/> Le deniega la solicitud de limitación del tratamiento de datos planteada, al no cumplir ninguna de las condiciones establecidas en el artículo 18.1 del Reglamento (UE) 2016/679 (RGPD), que establece lo siguiente:<w:br/><w:br/>\"Artículo 18 RGPD. Derecho a la limitación del tratamiento.<w:br/><w:br/>1. El interesado tendrá derecho a obtener del responsable del tratamiento la limitación del tratamiento de los datos cuando se cumpla alguna de las condiciones siguientes:<w:br/><w:br/>a) el interesado impugne la exactitud de los datos personales, durante un plazo que permita al responsable verificar la exactitud de los mismos;<w:br/><w:br/>b) el tratamiento sea ilícito y el interesado se oponga a la supresión de los datos personales y solicite en su lugar la limitación de su uso;<w:br/><w:br/>c) el responsable ya no necesite los datos personales para los fines del tratamiento, pero el interesado los necesite para la formulación, el ejercicio o la defensa de reclamaciones;<w:br/><w:br/>d) el interesado se haya opuesto al tratamiento en virtud del artículo 21, apartado 1, mientras se verifica si los motivos legítimos del responsable prevalecen sobre los del interesado.<w:br/><w:br/>2. Cuando el tratamiento de datos personales se haya limitado en virtud del apartado 1, dichos datos solo podrán ser objeto de tratamiento, con excepción de su conservación, con el consentimiento del interesado o para la formulación, el ejercicio o la defensa de reclamaciones, o con miras a la protección de los derechos de otra persona física o jurídica o por razones de interés público importante de la Unión o de un determinado Estado miembro.<w:br/><w:br/>3. Todo interesado que haya obtenido la limitación del tratamiento con arreglo al apartado 1 será informado por el responsable antes del levantamiento de dicha limitación.\"";
        break;

        case 6:
        $texto.=$datos['razonSocial'].", en calidad de responsable del tratamiento de datos personales, le informa que:<w:br/> Le deniega la solicitud de limitación del tratamiento de datos planteada, al no cumplir ninguna de las condiciones establecidas en el artículo 18.1 del Reglamento (UE) 2016/679 (RGPD), que establece lo siguiente:<w:br/><w:br/>\"Artículo 18 RGPD. Derecho a la limitación del tratamiento.<w:br/><w:br/>1. El interesado tendrá derecho a obtener del responsable del tratamiento la limitación del tratamiento de los datos cuando se cumpla alguna de las condiciones siguientes:<w:br/><w:br/>a) el interesado impugne la exactitud de los datos personales, durante un plazo que permita al responsable verificar la exactitud de los mismos;<w:br/><w:br/>b) el tratamiento sea ilícito y el interesado se oponga a la supresión de los datos personales y solicite en su lugar la limitación de su uso;<w:br/><w:br/>c) el responsable ya no necesite los datos personales para los fines del tratamiento, pero el interesado los necesite para la formulación, el ejercicio o la defensa de reclamaciones;<w:br/><w:br/>d) el interesado se haya opuesto al tratamiento en virtud del artículo 21, apartado 1, mientras se verifica si los motivos legítimos del responsable prevalecen sobre los del interesado.<w:br/><w:br/>2. Cuando el tratamiento de datos personales se haya limitado en virtud del apartado 1, dichos datos solo podrán ser objeto de tratamiento, con excepción de su conservación, con el consentimiento del interesado o para la formulación, el ejercicio o la defensa de reclamaciones, o con miras a la protección de los derechos de otra persona física o jurídica o por razones de interés público importante de la Unión o de un determinado Estado miembro.<w:br/><w:br/>3. Todo interesado que haya obtenido la limitación del tratamiento con arreglo al apartado 1 será informado por el responsable antes del levantamiento de dicha limitación.\"";
        break;
    }
    $texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
   	if($datos['respuesta']==1){
    	$texto.="<w:br/><w:br/>Deberá subsanar este defecto ante ".$datos['razonSocial'].", con domicilio a efectos de notificaciones en ".$datos['direccion']." para que podamos atender su petición";
    }
    $texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];

    if($datos['respuesta']==3){
    	$texto.="<w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/><w:br/>RESPUESTA A DERECHO DE ".mb_strtoupper($tipo)." EJERCITADO<w:br/>SUPUESTO Nº ".$datos['respuesta'].": COMUNICACIÓN AL INTERESADO EN CONCEPTO DE ENCARGADO/ DEL TRATAMIENTO<w:br/><w:br/>El presente escrito es para dar respuesta a la petición de limitación del tratamiento de fecha ".formateaFechaWeb($datos['fechaRecepcion'])." por D./ª";
        $direccion=" con domicilio a efectos de notificaciones en ".$datos['direccion']."<w:br/><w:br/>";
        if($datos['representante']!=''){
        	$texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    	} else {
        	$texto.=$datos['interesado'];
    	}
    	$texto.=$direccion;
    	$texto.=$datos['razonSocial']." le informa que:<w:br/><w:br/>1º. El tratamiento que realiza respecto los datos personales que solicita se limite su tratamiento, lo realizamos en concepto de encargado del tratamiento, por lo que deberá solicitar la limitación del tratamiento de dichos datos personales ante el Responsable del Tratamiento  ".$datos['direccion']." con dirección en ".$datos['responsableDireccion'].".<w:br/><w:br/>2º. Se ha procedido a trasladar al Responsable del Tratamiento referenciado la solicitud de limitación del tratamiento de datos referenciada en el presente escrito.";
    	$texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
    	$texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];
    }
    
    $documento->setValue("texto",utf8_decode($texto));

    $documento->save('../documentos/derechos/respuesta.docx');
    return $fichero;
}

function wordCesionario($PHPWord,$codigoDerecho){
    global $_CONFIG;
    $datos=consultaBD('SELECT derechos.*, clientes.razonSocial, CONCAT(clientes.domicilio,", CP ",clientes.cp,", ",clientes.localidad,", ",clientes.provincia) AS direccionCliente, ficheroLogo, administrador FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo WHERE derechos.codigo='.$codigoDerecho,true,true);
    $tipo=$datos['tipo'];
    $ref=$datos['referencia']<10?'0'.$datos['referencia']:$datos['referencia'];
    $fichero='COMUNICACIÓN_A_CESIONARIO_DE_'.mb_strtoupper($tipo).'_SOBRE_DATOS_INTERESADO_REF_'.$ref.'.docx';
    $documento=$PHPWord->loadTemplate('../documentos/derechos/plantillaDerechos.docx');
    $documento->setValue("true","");
    $documento->setValue("cliente",utf8_decode(sanearCaracteresDerechos($datos['razonSocial'])));  
    $documento->setValue("titulo",utf8_decode("COMUNICACIÓN A CESIONARIO DE ".mb_strtoupper($tipo)." SOBRE DATOS INTERESADO ".$datos['interesado']." REF. ".$ref));

    reemplazarLogoDerechos($documento,$datos,'image2.png'); 
    
    $texto="COMUNICACIÓN A CESIONARIO DE DATOS PERSONALES DE LIMITACIÓN DEL TRATAMIENTO EFECTUADA SOBRE DICHOS DATOS.<w:br/><w:br/><w:br/><w:br/>".$datos['razonSocial'].", en concepto de responsable del tratamiento de datos de carácter personal cedidos a ".$datos['nombreCesionario'].", mediante el presente escrito informamos al cesionario referenciado que hemos efectuado sobre los datos objeto de dicha cesión la limitación del tratamiento solicitada el día ".formateaFechaWeb($datos['fechaRecepcion'])." realizada por D./ª";
    $direccion=" con domicilio a efectos de notificaciones en ".$datos['responsableDireccion']."<w:br/><w:br/>";
    
    if($datos['representante']!=''){
        $texto.=$datos['representante']." en nombre y representación de D./ª ".$datos['interesado'];
    } else {
        $texto.=$datos['interesado'];
    }
    $texto.=$direccion;
    $texto.="<w:br/><w:br/>Adjuntamos copia de la solicitud de limitación del tratamiento recibida y efectuada, objeto del presente comunicado.";
    $texto.="<w:br/><w:br/>Observaciones: ".$datos['observaciones'];
    $texto.="<w:br/><w:br/>Así mismo le informamos que estamos a su disposición para cualquier aclaración sobre la información que contiene la presente respuesta. Para ello puede dirigirse a ".$datos['razonSocial']." con domicilio a efectos de notificaciones en  ".$datos['direccion'].", así como que tiene derecho a  recabar la tutela de la Agencia Española de Protección de Datos.<w:br/><w:br/>A ".formateaFechaWeb($datos['fechaRespuesta'])."<w:br/><w:br/><w:br/><w:br/>Fdo. ".$datos['razonSocial'];

    $documento->setValue("texto",utf8_decode($texto));

    $documento->save('../documentos/derechos/respuesta.docx');
    return $fichero;
}
//Fin parte de agrupaciones
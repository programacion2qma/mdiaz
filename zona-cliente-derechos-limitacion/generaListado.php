<?php

    include_once('funciones.php');
    compruebaSesion();

    include_once('../../api/js/firma/signature-to-image.php');
    require_once('../../api/phpword/PHPWord.php');
    require_once('../../api/html2pdf/html2pdf.class.php');

    $codigoCliente=obtenerCodigoCliente(true);
    $tipo='Limitación del tratamiento';
    wordListadoDerechos(new PHPWord(),$codigoCliente,$tipo);

    header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header("Content-Disposition: attachment; filename=Registro_de_derechos_de_limitación_del_tratamiento_recibidos.docx");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/derechos/Limitación.docx');
<?php
  $seccionActiva=75;
  include_once("../cabecera.php");
  gestionDerechos();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
	otrosDatos();
	$('#respuesta').change(function(){
		otrosDatos();
	});

	mediosComunicacion();
	$('input[name=mediosElectronicos],input[name=mediosDiferente]').change(function(){
		mediosComunicacion();
	});

	procede();
	$('input[name=visualizacion]').change(function(){
		procede();
	});

	$('.btnModal').unbind();
	$('.btnModal').click(function(e){
		if($(this).attr('id')=='art11'){
			$('#cajaGestion11').modal({'show':true,'backdrop':'static','keyboard':false});
		} else if($(this).attr('id')=='art12'){
			$('#cajaGestion12').modal({'show':true,'backdrop':'static','keyboard':false});
		} else {
			$('#cajaGestion18').modal({'show':true,'backdrop':'static','keyboard':false});
		}
	})
});


function otrosDatos(valor){
	var valor=$('select#respuesta option:selected').val();
	if(valor==1){
		$('.divResponsable').addClass('hide');
		$('.divOtrosDatos').removeClass('hide');
		$('.divOtrosDatos1').addClass('hide');
		$('.divOtrosDatos2').addClass('hide');
	} else if(valor==3) {
		$('.divResponsable').removeClass('hide');
		$('.divOtrosDatos').addClass('hide');
		$('.divOtrosDatos1').addClass('hide');
		$('.divOtrosDatos2').addClass('hide');
	} else if(valor==5) {
		$('.divResponsable').addClass('hide');
		$('.divOtrosDatos').addClass('hide');
		$('.divOtrosDatos1').removeClass('hide');
		$('.divOtrosDatos2').addClass('hide');
	} else if(valor==6) {
		$('.divResponsable').addClass('hide');
		$('.divOtrosDatos').addClass('hide');
		$('.divOtrosDatos1').addClass('hide');
		$('.divOtrosDatos2').removeClass('hide');
	} else {
		$('.divResponsable').addClass('hide');
		$('.divOtrosDatos').addClass('hide');
		$('.divOtrosDatos1').addClass('hide');
		$('.divOtrosDatos2').addClass('hide');
	}
}

function mediosComunicacion(){
	var mediosElectronicos=$('input[name=mediosElectronicos]:checked').val();
	var mediosDiferente=$('input[name=mediosDiferente]:checked').val();
	if(mediosElectronicos=='SI'){
		$('#divMediosDiferente').removeClass('hide');
		if(mediosDiferente=='SI'){
			$('#divOtroMedio').removeClass('hide');
		} else {
			$('#divOtroMedio').addClass('hide');
		}
	} else {
		$('#divMediosDiferente').addClass('hide');
		$('#divOtroMedio').addClass('hide');
	}
}

function procede(){
	var visualizacion=$('input[name=visualizacion]:checked').val();
	if(visualizacion=='SI'){
		$('#divProcede').removeClass('hide');
	} else {
		$('#divProcede').addClass('hide');
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de comerciales

function operacionesComerciales(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaComercial();
	}
	elseif(isset($_POST['nombre'])){
		$res=creaComercial();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('comerciales');
	}

	mensajeResultado('nombre',$res,'Comerciales');
    mensajeResultado('elimina',$res,'Comerciales', true);
}


function creaComercial(){
	$res=insertaDatos('comerciales');

	if($res){
		$res=insertaNumerosCuentaComercial($res);
	}

	return $res;
}

function actualizaComercial(){
	$res=actualizaDatos('comerciales');
	
	if($res){
		$res=insertaNumerosCuentaComercial($_POST['codigo'],true);
	}

	return $res;
}

function insertaNumerosCuentaComercial($codigoComercial,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	if($actualizacion){
		$res=consultaBD("DELETE FROM cuentas_comercial WHERE codigoComercial=$codigoComercial");
	}

	for($i=0;isset($datos['numeroCuenta'.$i]);$i++){
		$res=$res && consultaBD("INSERT INTO cuentas_comercial VALUES(NULL,'".$datos['numeroCuenta'.$i]."',$codigoComercial);");
	}

	cierraBD();

	return $res;
}


function creaEstadisticasComerciales(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='ADMINISTRACION2'){
        $res=consultaBD("SELECT COUNT(codigo) AS total FROM comerciales WHERE codigoUsuario=$codigoUsuario",true,true);
    }
    else{
        $res=estadisticasGenericas('comerciales');
    }
	
	return $res;
}

function listadoComerciales(){
	$columnas=array('comerciales.nombre','comerciales.dni','provincia','telefono','movil','email','comercialSuperior','CONCAT(usuarios.nombre," ",usuarios.apellidos)','categoria','comerciales.activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT comerciales.codigo, comerciales.nombre AS agente, comerciales.dni, comerciales.provincia, comerciales.telefono, comerciales.movil, comerciales.email, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario, usuarios.nombre, usuarios.apellidos, comerciales.activo, categoria, comerciales2.nombre AS comercialSuperior, comerciales.codigoUsuario AS codigoC FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo LEFT JOIN comerciales AS comerciales2 ON comerciales.codigoComercial=comerciales2.codigo LEFT JOIN categorias_agentes ON comerciales.codigoCategoria=categorias_agentes.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT comerciales.codigo, comerciales.nombre AS agente, comerciales.dni, comerciales.provincia, comerciales.telefono, comerciales.movil, comerciales.email, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS usuario, usuarios.nombre, usuarios.apellidos, comerciales.activo, categoria, comerciales2.nombre AS comercialSuperior, comerciales.codigoUsuario AS codigoC FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo LEFT JOIN comerciales AS comerciales2 ON comerciales.codigoComercial=comerciales2.codigo LEFT JOIN categorias_agentes ON comerciales.codigoCategoria=categorias_agentes.codigo $having");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['codigo'],
			$datos['agente'],
			$datos['dni'],
			convertirMinuscula($datos['provincia']),
			"<div class='nowrap'>".formateaTelefono($datos['telefono'])."</div>",
			"<div class='nowrap'>".formateaTelefono($datos['movil'])."</div>",
			"<a href='mailto:".$datos['email']."'>".$datos['email']."</a>",
			$datos['comercialSuperior'],
			$datos['usuario'],
			$datos['categoria'],
			creaBotonDetalles("comerciales/gestion.php?codigo=".$datos['codigo'],''),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionComercial(){
	operacionesComerciales();

	abreVentanaGestion('Gestión de Comerciales','?','span3');
	$datos=compruebaDatos('comerciales');

	campoID($datos);
	campoTexto('nombre','Nombre',$datos,'span3');
	campoTextoValidador('dni','DNI',$datos,'input-small validaDNI','comerciales');
	campoSelectProvincia($datos);
	campoSelectConsulta('codigoCategoria','Categoría',"SELECT codigo, categoria AS texto FROM categorias_agentes WHERE activo='SI' ORDER BY categoria",$datos);
	campoSelectConsulta('codigoComercial','Depende del agente',"SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre",$datos);
	campoSelectConsulta('codigoUsuario','Administrativo/a',"SELECT codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') AND activo='SI' ORDER BY nombre,apellidos",$datos);
	campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',$datos,'input-small','comerciales');
	campoTextoSimboloValidador('movil','Móvil','<i class="icon-mobile"></i>',$datos,'input-small','comerciales');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoSimboloValidador('fax','Fax','<i class="icon-fax"></i>',$datos,'input-small','comerciales');
	campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',$datos,'input-large','comerciales');
	campoTextoSimboloValidador('email2','eMail secundario','<i class="icon-envelope"></i>',$datos,'input-large','comerciales');
	campoTexto('direccion','Dirección',$datos,'span4');
	campoTexto('cp','Código Postal',$datos,'input-mini');
	campoTexto('localidad','Localidad',$datos);
	creaTablaCuentasBancarias($datos);
	campoUsuarioAsociado($datos);
	campoCheck('checkMensajeria','Mensajería que desea recibir',$datos,array('Convenio de Adhesión y SEPA','Claves de Acceso al curso','Factura','Informe de bonificación'),array('SI','SI','SI','SI'),true);
	campoRadio('activo','Activo',$datos,'SI');

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');

	listadoComercialesDependientes($datos);

	cierraVentanaGestion('index.php');
}

function campoUsuarioAsociado($datos){
	campoOculto($datos,'codigoUsuarioAsociado','NULL');

	if($datos['codigoUsuarioAsociado']!=NULL){
		$usuario=consultaBD("SELECT usuario FROM usuarios WHERE codigo=".$datos['codigoUsuarioAsociado'],true,true);
		campoDato('Usuario',"<a href='../usuarios/gestion.php?codigo=".$datos['codigoUsuarioAsociado']."'>".$usuario['usuario']."</a>");
	}
}


function filtroComerciales(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nombre');
	campoSelectProvincia(false,2);
	campoSelectConsulta(8,'Categoría',"SELECT categoria AS codigo, categoria AS texto FROM categorias_agentes WHERE activo='SI' ORDER BY categoria");

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta(7,'Administrativo/a',"SELECT CONCAT(nombre,' ',apellidos) AS codigo, CONCAT(nombre,' ',apellidos) AS texto FROM usuarios WHERE (tipo='ADMINISTRACION1' OR tipo='ADMINISTRACION2') AND activo='SI' ORDER BY nombre,apellidos");
	campoSelectSiNoFiltro(9,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function listadoComercialesDependientes($datos){
	if($datos!=false){
		$codigoComercial=$datos['codigo'];

		$consulta=consultaBD("SELECT comerciales.codigo, nombre, dni, provincia, telefono, movil, email, categoria FROM comerciales LEFT JOIN categorias_agentes ON comerciales.codigoCategoria=categorias_agentes.codigo WHERE codigoComercial=$codigoComercial",true);
		if(mysql_num_rows($consulta)>0){
			echo "<br />
			<h3 class='apartadoFormulario'>Comerciales que dependen de ".$datos['nombre']."</h3>
			<div class='centro'>
				<table class='table table-striped tabla-simple'>
			      <thead>
			        <tr>
			          <th> Nombre </th>
			          <th> DNI </th>
			          <th> Provincia </th>
			          <th> Teléfono </th>
			          <th> Móvil </th>
			          <th> eMail </th>
			          <th> Categoría </th>
			          <th> </th>
			        </tr>
			      </thead>
			      <tbody>";
			  	
			  		while($comercial=mysql_fetch_assoc($consulta)){
						echo "
						<tr>
							<td>".$comercial['nombre']."</td>
							<td>".$comercial['dni']."</td>
							<td>".$comercial['provincia']."</td>
							<td>".$comercial['telefono']."</td>
							<td>".$comercial['movil']."</td>
							<td>".$comercial['email']."</td>
							<td>".$comercial['categoria']."</td>
							<td class='centro'><a href='gestion.php?codigo=".$comercial['codigo']."' class='btn btn-small btn-primary'><i class='icon-search-plus'> Ver </a></td>
						</tr>";
					}
		      
		    echo "</tbody>
		    	</table>
			</div>";
		}
	}
}


function creaTablaCuentasBancarias($datos){
	echo "
	<div class='control-group'>                     
	      <label class='control-label'>Número/s de cuenta:</label>
	      <div class='controls'>
			<table class='table table-striped table-bordered mitadAncho' id='tablaCuentas'>
		      <tbody>";
		  	
		  		$i=0;

		  		if($datos!=false){
		  			$consulta=consultaBD("SELECT * FROM cuentas_comercial WHERE codigoComercial='".$datos['codigo']."'",true);
		  			while($datosP=mysql_fetch_assoc($consulta)){
						imprimeLineaTablaCuentasBancarias($i,$datosP);
						$i++;
		  			}
		  		}
		  		
		  		if($i==0){
		  			imprimeLineaTablaCuentasBancarias(0,false);
		  		}
	      
	    echo "</tbody>
	    	</table>
			<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCuentas\");'><i class='icon-plus'></i> Añadir cuenta</button> 
			<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCuentas\");'><i class='icon-trash'></i> Eliminar cuenta</button>
		</div>
	</div>";
}

function imprimeLineaTablaCuentasBancarias($i,$datos){
	$j=$i+1;

	echo "<tr>";
		campoTextoTabla('numeroCuenta'.$i,$datos['numeroCuenta'],'input-large pagination-right');
	echo "<td><input type='checkbox' name='filasTabla[]' value='$j'></td>
	</tr>";
}


//Fin parte de comerciales
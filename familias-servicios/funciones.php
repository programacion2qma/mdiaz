<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesServicios(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('servicios_familias');
	}
	elseif(isset($_POST['referencia'])){
		$res=insertaDatos('servicios_familias');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('servicios_familias');
	}

	mensajeResultado('referencia',$res,'Familia');
    mensajeResultado('elimina',$res,'Familia', true);
}


function listadoServicios(){
	$columnas=array('referencia','nombre','referencia','referencia');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListadoServicios($columnas);//Uso de función personalizada
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, referencia, nombre FROM servicios_familias $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, referencia, nombre FROM servicios_familias $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['referencia'],
			$datos['nombre'],
			/*botonAcciones(array('Detalles'),array('familias-servicios/gestion.php?codigo='.$datos['codigo']),array('icon-edit'),false,true,''),*/
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

//Esta función utiliza un "orden natural" (primero por longitud, luego por el propio valor) para la primera columna del listado de servicios (que es mayoritariamente numérica)
function obtieneOrdenListadoServicios($columnas){
	$orden = '';
    if(isset($_GET['iSortCol_0'])) {
        $orden = 'ORDER BY  ';
        for ($i=0;$i<(int)$_GET['iSortingCols'];$i++) {
            if($_GET['bSortable_'.(int)$_GET['iSortCol_'.$i]]=='true'){
                $indice=(int)$_GET['iSortCol_'.$i];

                if($indice==0 && $_GET['sSortDir_'.$i]==='asc'){//Si se trata de la primera columna y el orden es ascendente...
                	$orden.="LENGTH(referencia), referencia, ";//.. ordeno por longitud y luego valor
                }
                elseif($indice==0 && $_GET['sSortDir_'.$i]!=='asc'){//Lo mismo si el orden es descendente
                	$orden.="LENGTH(referencia) DESC, referencia DESC, ";
                }
                else{//Orden normal
                	$orden.=$columnas[$indice].' '.($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
                }
            }
        }

        $orden = substr_replace($orden, '', -2);
        if ($orden == 'ORDER BY') {
            $orden = '';
        }
    }
    return $orden;
}


function gestionServicio(){
	operacionesServicios();

	abreVentanaGestion('Gestión de Familais','?','span3','icon-edit','margenAb');
	$datos=compruebaDatos('servicios_familias');

	campoTexto('referencia','Referencia',$datos,'input-small');
	areaTexto('nombre','Nombre',$datos);

	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}

function filtroServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Referencia');
	campoTexto(1,'Nombre','','span3');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de servicios
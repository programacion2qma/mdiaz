<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de complementos

function operacionesComplementos(){
	$res=true;

	if(isset($_POST['codigo'])){
		formateaPrecioBDD('precio');
		$res=actualizaDatos('complementos');
	}
	elseif(isset($_POST['complemento'])){
		formateaPrecioBDD('precio');
		$res=insertaDatos('complementos');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('complementos');
	}

	mensajeResultado('complemento',$res,'Complemento');
    mensajeResultado('elimina',$res,'Complemento', true);
}


function listadoComplementos(){
	global $_CONFIG;

	$columnas=array('complemento','precio','activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, complemento, precio, activo FROM complementos $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, complemento, precio, activo FROM complementos $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['complemento'],
			"<div class='pagination-right'>".formateaNumeroWeb($datos['precio']).' €</div>',
			creaBotonDetalles("complementos/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionComplemento(){
	operacionesComplementos();

	abreVentanaGestion('Gestión de Complementos','?','span3','icon-edit','margenAb');
	$datos=compruebaDatos('complementos');

	campoTexto('complemento','Complemento',$datos,'span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTextoSimbolo('precio','Precio','€',formateaNumeroWeb($datos['precio']));
	campoRadio('activo','Activo',$datos,'SI');

	cierraVentanaGestion('index.php',true);
}

function filtroComplementos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectSiNoFiltro(2,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de complementos
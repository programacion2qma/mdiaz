<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de ventas de servicios

function operacionesVentasServicios(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaVentaServicios();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=creaVentaServicios();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaVentas();
	}
	elseif(isset($_GET['desconfirmar'])){
		$res=desconfirmaVenta();
	}

	mensajeResultado('codigoCliente',$res,'Venta');
	mensajeResultado('codigoCliente0',$res,'Venta');
    mensajeResultado('elimina',$res,'Venta', true);
}

function desconfirmaVenta(){
	$_POST['codigoCliente']='desconfirmar';//Para que entre en mensajeResultado

	if(isset($_GET['tipo']) && $_GET['tipo']=='FORMACION'){
		$res=consultaBD("UPDATE grupos SET confirmada='NO', yaTramitado='NO' WHERE codigo=".$_GET['codigo'],true);//Pongo yaTramitado a NO para que cuando se vuelva a confirmar se creen las tutorías de nuevo
	}
	else{
		$res=cambiaValorCampo('confirmada','NO',$_GET['codigo'],'ventas_servicios');
	}

	return $res;
}

function eliminaVentas(){//Porque los registros vienen de 2 tablas distintas...
	$res=true;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$res=$res && consultaBD("DELETE FROM ventas_servicios WHERE codigo=".$datos['codigo'.$i]);	
	}
	cierraBD();

	return $res;
}

/*function creaEstadisticasVentas(){
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];
    $ejercicio=obtieneWhereEjercicioEstadisticas('fecha');
    $ejercicioGrupo=obtieneWhereEjercicioEstadisticas('fechaAlta');

    if($perfil=='ADMINISTRACION2'){
    	$res=consultaBD("SELECT COUNT(ventas_servicios.codigo) AS total FROM ventas_servicios LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo WHERE codigoUsuario=$codigoUsuario $ejercicio",true,true);
    	$formacion=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo WHERE esVenta='SI' AND codigoUsuario=$codigoUsuario $ejercicioGrupo",true,true);
    }
    elseif($perfil=='COMERCIAL'){
    	$res=consultaBD("SELECT COUNT(ventas_servicios.codigo) AS total FROM ventas_servicios LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo WHERE codigoUsuarioAsociado=$codigoUsuario $ejercicio",true,true);
    	$formacion=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN comerciales ON clientes.codigoComercial=comerciales.codigo WHERE esVenta='SI' AND codigoUsuarioAsociado=$codigoUsuario $ejercicioGrupo",true,true);
    }
    elseif($perfil=='TELEMARKETING'){
    	$res=consultaBD("SELECT COUNT(ventas_servicios.codigo) AS total FROM ventas_servicios LEFT JOIN telemarketing ON ventas_servicios.codigoComercial=telemarketing.codigo WHERE codigoUsuarioAsociado=$codigoUsuario $ejercicio",true,true);
    	$formacion=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo LEFT JOIN telemarketing ON clientes.codigoComercial=telemarketing.codigo WHERE esVenta='SI' AND codigoUsuarioAsociado=$codigoUsuario $ejercicioGrupo",true,true);
    }
    elseif($perfil=='DELEGADO'){
    	$res=consultaBD("SELECT COUNT(ventas_servicios.codigo) AS total FROM ventas_servicios WHERE codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario $ejercicio)",true,true);
    	$formacion=consultaBD("SELECT COUNT(grupos.codigo) AS total FROM grupos LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo WHERE esVenta='SI' AND codigoComercial IN(SELECT comerciales.codigo FROM comerciales LEFT JOIN usuarios ON comerciales.codigoUsuarioAsociado=usuarios.codigo WHERE usuarios.codigoUsuario=$codigoUsuario $ejercicioGrupo)",true,true);
    }
    else{
    	$res=estadisticasGenericas('ventas_servicios',false,"1=1 $ejercicio");
    	$formacion=estadisticasGenericas('grupos',false,"esVenta='SI' $ejercicioGrupo");
    }

    $res['total']+=$formacion['total'];

    return $res;
}*/

function creaVentaServicios(){
	formateaPrecioBDD('total');
	$res=insertaDatos('ventas_servicios');
	if($res){
		$codigoVenta=$res;
		$datos=arrayFormulario();
		$res=insertaConceptosVentaServicios($codigoVenta,$datos);
		$res=$res && insertaConceptosComplementos($codigoVenta,$datos);
	}
	comprobarServiciosVenta();
	return $res;
}


function actualizaVentaServicios(){
	$datos=arrayFormulario();

	//Control de cambios por parte de comerciales
	detectaCambios('ventas_servicios',$datos);//Función para detectar los cambios realizados por los perfiles COMERCIAL y derivados
	detectaCambiosSubTabla('codigoServicio','codigoVentaServicio','conceptos_venta_servicios',$datos);
	//Fin control de cambios
	formateaPrecioBDD('total');
	$res=actualizaDatos('ventas_servicios');
	if($res){
		$res=insertaConceptosVentaServicios($_POST['codigo'],$datos,true);
		$res=$res && insertaConceptosComplementos($_POST['codigo'],$datos,true);
	}
	comprobarServiciosVenta();
	return $res;
}

//La siguiente función inserta cada línea de concepto en la venta de servicios. Si además los conceptos son nuevos para la venta (o la propia venta es nueva), inserta los trabajos asociados para la sección de Consultoría
function insertaConceptosVentaServicios($codigoVenta,$datos,$actualizacion=false){
	$res=true;

	conexionBD();
	$_SESSION['trabajos']=array();
	if($actualizacion){
		$res=consultaBD("DELETE FROM conceptos_venta_servicios WHERE codigoVentaServicio=$codigoVenta");
	}

	for($i=0;isset($datos['codigoServicio'.$i]);$i++){
		$res=$res && insertaNuevoServicioVenta($codigoVenta,$datos,$i);
	}
	$trabajos = $separado_por_comas = implode(",", $_SESSION['trabajos']);
	if($trabajos!=''){
		$res=consultaBD('DELETE FROM trabajos WHERE codigoServicio NOT IN('.$trabajos.') AND codigoVenta='.$codigoVenta);
	}
	cierraBD();

	return $res;
}

function insertaNuevoServicioVenta($codigoVenta,$datos,$i){
	$res=true;

	if($datos['codigoServicio'.$i]!='NULL'){
		$codigoServicio=$datos['codigoServicio'.$i];
		$precioUnidad=formateaNumeroWeb($datos['precioUnidad'.$i],true);
		$unidades=formateaNumeroWeb($datos['unidades'.$i],true);
		$total=formateaNumeroWeb($datos['total'.$i],true);

		$res=consultaBD("INSERT INTO conceptos_venta_servicios VALUES(NULL,$codigoVenta,$codigoServicio,$precioUnidad,$unidades,$total);");
		$res=$res && insertaTrabajoDesdeVenta($datos['estado'],$datos['codigoCliente'],$codigoServicio,$codigoVenta,$datos['fecha']);//Inserción del trabajo solo en inserción de servicios nuevos
	}

	return $res;
}


function insertaConceptosComplementos($codigoVenta,$datos,$actualizacion=false){
	$res=true;

	conexionBD();

	if($actualizacion){
		$res=consultaBD("DELETE FROM complementos_venta_servicios WHERE codigoVentaServicio=$codigoVenta");
	}

	for($i=0;isset($datos['codigoComplemento'.$i]);$i++){
		$codigoComplemento=$datos['codigoComplemento'.$i];
		$precioComplemento=formateaNumeroWeb($datos['precioComplemento'.$i],true);
		$observaciones=$datos['observaciones'.$i];

		$res=$res && consultaBD("INSERT INTO complementos_venta_servicios VALUES(NULL,$codigoComplemento,$precioComplemento,'$observaciones','$codigoVenta');");
	}

	cierraBD();

	return $res;
}

function listadoVentasServicios(){
	global $_CONFIG;

	//Uso 2 columnas y 2 where porque las 2 consultas no tienen las columnas con el mismo nombre
	$columnas=array('fecha','razonSocial','servicios.referencia','servicio','ventas_servicios.total','fecha','razonSocial','razonSocial');

	//Parte de WHERE
	$where=obtieneWhereListado("WHERE confirmada='NO'",$columnas);
	//Fin parte de WHERE

	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
		

	$query="SELECT ventas_servicios.codigo, clientes.codigo AS codigoCliente, fecha, comerciales.nombre, razonSocial, ventas_servicios.total, ventas_servicios.codigoColaborador, ventas_servicios.activo, ventas_servicios.codigoComercial, servicios.referencia, servicios.servicio

	FROM ventas_servicios
	LEFT JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio
	LEFT JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo 
	LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo 
	LEFT JOIN telemarketing ON ventas_servicios.codigoTelemarketing=telemarketing.codigo 
	LEFT JOIN clientes ON ventas_servicios.codigoCliente=clientes.codigo 

	$where GROUP BY ventas_servicios.codigo";

	conexionBD();

	$consulta=consultaBD($query." $orden $limite");
	$consultaPaginacion=consultaBD($query);

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$datosServicios=obtieneServiciosVenta($datos['codigo'],$datos);
		$botones=obtieneBotonesCheckVenta($datos);

		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['razonSocial'],
			$datosServicios['referencias'],
			$datosServicios['servicios'],
			"<div class='pagination-right nowrap'>".formateaNumeroWeb($datos['total']).' €</div>',
			$botones,
        	"<div class='centro'><input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."' /></div>",
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	cierraBD();

	echo json_encode($res);
}

function obtieneBotonesCheckVenta($datos){

		$nombres=array('Detalles','Confirmar');
		$direcciones=array("preventas/venta-servicios.php?codigo=".$datos['codigo'],"ventas/index.php?confirmacion=".$datos['codigo']);
		$iconos=array('icon-search-plus','icon-check-circle');
	/*$documentacion=false;
	if($datos['tipoVenta']=='SERVICIOS'){
		$conceptos=consultaBD("SELECT * FROM conceptos_venta_servicios WHERE codigoVentaServicio=".$datos['codigo']." AND codigoServicio IN (SELECT codigo FROM servicios WHERE codigoFamilia IN (SELECT codigo FROM servicios_familias WHERE referencia<>'OTROS'))",true,true);
		if(isset($conceptos['codigo'])){
			$documentacion=true;
			array_push($nombres, 'Documentación');
			array_push($iconos, 'icon-download');
		}
	}

	if($documentacion){
		array_push($direcciones, 'preventas/generaDocumento.php?codigo='.$datos['codigo']);
	}*/

	/*$conceptos=consultaBD("SELECT * FROM conceptos_venta_servicios WHERE codigoVentaServicio=".$datos['codigo']." AND codigoServicio IN (SELECT codigo FROM servicios WHERE codigoFamilia = (SELECT codigo FROM servicios_familias WHERE referencia='LOPDCP'))",true,true);
	if(isset($conceptos['codigo'])){
		array_push($nombres, 'Documentación');
		array_push($direcciones, '');
		array_push($iconos, 'icon-download');
	}*/

	$res=botonAcciones($nombres,$direcciones,$iconos);

	return $res;
}

function obtieneServiciosVenta($codigoVenta,$datosVenta){
	$res=array('referencias'=>'','servicios'=>'');


	$consulta=consultaBD("SELECT referencia, servicio FROM servicios INNER JOIN conceptos_venta_servicios ON servicios.codigo=conceptos_venta_servicios.codigoServicio WHERE codigoVentaServicio=$codigoVenta");
	while($datos=mysql_fetch_assoc($consulta)){
		$res['referencias'].=$datos['referencia'].', ';
		$res['servicios'].=$datos['servicio'].', ';
	}

	$res['referencias']=quitaUltimaComa($res['referencias']);
	$res['servicios']=quitaUltimaComa($res['servicios']);

	return $res;
}


function seleccionTipoVenta(){
	abreVentanaGestion('Gestión preventas','?','','icon-edit','',false,'noAjax');
	campoSelect('tipoVenta','Tipo de venta',array('Formación','Otros servicios'),array('FORMACION','SERVICIOS'));
	cierraVentanaGestion('index.php',false,true,'Continuar','icon-chevron-right');
}

function compruebaTipoVenta(){
	if(isset($_POST['tipoVenta']) && $_POST['tipoVenta']=='FORMACION'){
		header('Location: venta-formacion.php');
	}
	elseif(isset($_POST['tipoVenta']) && $_POST['tipoVenta']=='SERVICIOS'){
		header('Location: venta-servicios.php');
	}
}

function gestionVentaServicios(){
	operacionesVentasServicios();

	abreVentanaGestion('Gestión de preventa de servicios','index.php','span3');
	$datos=compruebaDatos('ventas_servicios');

	campoSelectClienteFiltradoPorUsuario($datos,'Cliente','codigoCliente','',true);
	campoFechaVenta($datos);
	campoFecha('fechaFacturarServicio','Fecha para facturar',$datos);
	campoVencimientoVenta($datos);
	campoFechaVencimientoVenta($datos);

	//campoSelectConsulta('codigoComercial','Comercial',"SELECT codigo, nombre AS texto FROM comerciales WHERE activo='SI' ORDER BY nombre;",$datos);
	campoSelectComercial($datos,false);

	cierraColumnaCampos();
	abreColumnaCampos();

	//campoSelectConsulta('codigoColaborador','Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre;",$datos);
	campoSelectColaborador($datos,false);
	//campoSelectConsulta('codigoTelemarketing','Telemarketing',"SELECT codigo, nombre AS texto FROM telemarketing WHERE activo='SI' ORDER BY nombre;",$datos);
	campoTelemarketing($datos);
	
	campoOculto('NO','anulado');
	campoOculto('SI','activo');
	campoOculto($datos,'camposModificados','');
	campoOculto($datos,'confirmada','NO');
	campoOculto($datos,'estado','PENDIENTE');
	campoOculto($datos,'observacionesVerificacion','');
	campoOculto($datos,'tipoVentaServicio','PRIVADA');

	cierraColumnaCampos();
	abreColumnaCampos('span3');

	creaTablaVentaServicios($datos);
	creaTablaComplementos($datos);
	campoTextoSimbolo('total','Importe total','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right',0,true);

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');

	campoObservacionesParaFactura($datos);

	cierraVentanaGestion('index.php');
}

function creaTablaVentaServicios($datos){
	conexionBD();

	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Servicio/s:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaVentaServicios'>
				  	<thead>
				    	<tr>
				            <th> Servicio </th>
							<th> Precio/Ud. </th>
							<th> Unidades </th>
							<th> Total </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM conceptos_venta_servicios WHERE codigoVentaServicio=".$datos['codigo']);
				  			while($datosVenta=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaVentaServicios($datosVenta,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaVentaServicios(false,$i);
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' id='insertaFilaTabla'><i class='icon-plus'></i> Añadir servicio</button> 
					<button type='button' class='btn btn-small btn-danger' id='eliminaFilaTabla' tabla='tablaVentaServicios'><i class='icon-trash'></i> Eliminar servicio</button>
				</div>
			</div>
		</div>
	</div>";

	generaCamposPreciosServicios();
	cierraBD();
}

function imprimeLineaTablaVentaServicios($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	campoSelectConsulta('codigoServicio'.$i,'',"SELECT codigo, CONCAT(referencia,' - ',servicio) AS texto FROM servicios WHERE activo='SI' ORDER BY referencia;",$datos['codigoServicio'],'selectpicker span4 show-tick','data-live-search="true"','',1,false);
	campoTextoSimbolo('precioUnidad'.$i,'','€',formateaNumeroWeb($datos['precioUnidad']),'input-mini pagination-right precioUnidad',1);//La clase precioUnidad no tiene CSS, es para el JS
	campoTextoTabla('unidades'.$i,$datos['unidades'],'input-mini pagination-right unidades');
	campoTextoSimbolo('total'.$i,'','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right',1);
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

function generaCamposPreciosServicios(){
	$consulta=consultaBD("SELECT codigo, precio FROM servicios ORDER BY codigo;");
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto(formateaNumeroWeb($datos['precio']),'servicio'.$datos['codigo']);
	}
}

function filtroVentasServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoFecha(0,'Fecha desde');
	campoTexto(1,'Cliente','','span3');
	
	

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(5,'Hasta');
	campoTexto(2,'Códigos');
	campoSelectConsulta(3,'Servicio',"SELECT servicio AS codigo, servicio AS texto FROM servicios WHERE activo='SI' ORDER BY servicio");

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function creaTablaComplementos($datos){
	conexionBD();

	echo "
	<br />
	<div class='control-group'>                     
		<label class='control-label'>Complemento/s:</label>
    	<div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaComplementos'>
				  	<thead>
				    	<tr>
				            <th> Complemento </th>
							<th> Precio </th>
							<th> Observaciones </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$query="SELECT * FROM complementos_venta_servicios WHERE codigoVentaServicio=".$datos['codigo'];
				  			$consulta=consultaBD($query);
				  			while($datosVenta=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaComplementos($datosVenta,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaComplementos(false,$i);
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaComplemento();'><i class='icon-plus'></i> Añadir complemento</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaComplementos\");' tabla='tablaComplementos'><i class='icon-trash'></i> Eliminar complemento</button>
				</div>
			</div>
		</div>
	</div>";

	generaCamposPreciosComplementos();
	cierraBD();
}

function imprimeLineaTablaComplementos($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	campoSelectConsulta('codigoComplemento'.$i,'',"SELECT codigo, complemento AS texto FROM complementos WHERE activo='SI' ORDER BY complemento;",$datos['codigoComplemento'],'selectpicker selectComplemento span4 show-tick','data-live-search="true"','',1,false);
	campoTextoSimbolo('precioComplemento'.$i,'','€',formateaNumeroWeb($datos['precioComplemento']),'input-mini pagination-right precioComplemento',1);//La clase precioComplemento no tiene CSS, es para el JS
	areaTextoTabla('observaciones'.$i,$datos['observaciones'],'areaTexto');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}


function generaCamposPreciosComplementos(){
	$consulta=consultaBD("SELECT codigo, precio FROM complementos ORDER BY codigo;");
	while($datos=mysql_fetch_assoc($consulta)){
		divOculto(formateaNumeroWeb($datos['precio']),'complemento'.$datos['codigo']);
	}
}

//Fin parte de ventas de servicios

//Parte de ventas de formación

function gestionGrupo(){
	operacionesVentasServicios();

	abreVentanaGestion('Gestión de preventa de formación','index.php','span12');
	$datos=obtieneDatosVentaFormacion();

	campoSelectAccionFormativaVenta($datos);
	
	cierraColumnaCampos();
	abreColumnaCampos();

	campoFechaVenta($datos,'fechaAlta');
	//campoSelectClienteFiltradoPorUsuario($datos,'Cliente','codigoCliente','',true);

	campoDato('Precio referencia','','precioReferencia');
	campoTextoSimbolo('importeVenta','Precio','€',formateaNumeroWeb($datos['importeVenta']));

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectComercial($datos,false);
	areaTexto('observacionesVenta','Observaciones venta',$datos);

	campoOculto($datos,'esVenta','SI');
	campoOculto($datos,'confirmada','NO');
	campoOculto('SI','nuevo');//Campo que no existe en la BDD y que sirve para que se limpien los datos al pulsar en "Guardar y registrar otra"

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');

	//creaTablaAlumnos($datos);
	creaTablaEmpresasVenta($datos);

	creaTablaComplementosFormacion($datos,true);

	campoObservacionesParaFactura($datos);

	cierraVentanaGestionVentaFormacion('Guardar y registrar otra','index.php',true);
	
	ventanaCreacionAccionFormativa();
	ventanaCreacionTrabajador();
}

function obtieneDatosVentaFormacion(){
	if(isset($_POST['nuevo'])){//Se ha pulsado en "Guardar y registrar otra"
		$datos=arrayFormulario();
		$datos['codigo']=false;
		$datos['codigoAccionFormativa']=false;
		$datos['importeVenta']=false;
		$datos['observaciones']=false;
	}
	else{
		$datos=compruebaDatos('grupos');
		if($datos){
			$consulta=consultaBD("SELECT codigoCliente FROM clientes_grupo WHERE codigoGrupo=".$datos['codigo']." ORDER BY codigo ASC",true,true);
			$datos['codigoCliente']=$consulta['codigoCliente'];
		}
	}

	return $datos;
}

function ventanaCreacionAccionFormativa(){
	abreVentanaModal('Acciones Formativas','cajaAccionFormativa');
	campoTexto('accionAF','Nombre acción',false,'span3');
	cierraVentanaModal('creaAccionFormativa');
}


/*function creaTablaAlumnos($datos){
	echo "
	<br />
	<h3 class='apartadoFormulario'>Alumnos</h3>
	<div class='table-responsive centro'>
		<table class='table table-striped tabla-simple' id='tablaAlumnos'>
		  	<thead>
		    	<tr>
		            <th> Trabajador </th>
		            <th> Observaciones </th>
					<th> </th>
		    	</tr>
		  	</thead>
		  	<tbody>";
		  	
		  	conexionBD();

		  	$i=0;
		  	if($datos!=false && $datos['codigo']!=false){
		  		$consulta=consultaBD("SELECT * FROM participantes WHERE codigoGrupo=".$datos['codigo']);
		  		while($participante=mysql_fetch_assoc($consulta)){
		  			imprimeLineaTablaAlumnos($i,$participante);
		  			$i++;
		  		}
		  	}

		  	if($i==0){
		  		imprimeLineaTablaAlumnos($i,false);
		  	}

		  	cierraBD();

		echo "
		  	</tbody>
		</table>
		<div class='centro'>
			<button type='button' class='btn btn-small btn-success' onclick='insertaFilaAlumno();'><i class='icon-plus'></i> Añadir alumno</button> 
			<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaAlumnos\");'><i class='icon-trash'></i> Eliminar alumno</button>
		</div>
	</div>";

	//El siguiente div oculto sirve para guardar la consulta de selección de alumnos, que será obtenida y modificada en JS para filtrar por empresas.
	divOculto("SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI'",'consultaAlumnos');
}

function imprimeLineaTablaAlumnos($i,$datos){
	$j=$i+1;

	echo "<tr>";
			//campoSelectConsultaAjax('codigoTrabajador'.$i,'',"SELECT trabajadores_cliente.codigo, CONCAT(nif,' - ',nombre,' ',apellido1,' ',apellido2,' (',razonSocial,')') AS texto, codigoCliente FROM trabajadores_cliente LEFT JOIN clientes ON trabajadores_cliente.codigoCliente=clientes.codigo WHERE trabajadores_cliente.activo='SI';",$datos['codigoTrabajador'],'trabajadores/gestion.php?codigo=','selectpicker selectAjax span6 show-tick selectAlumno','',1,false);
			campoTrabajadorVenta($i,$datos);
			areaTextoTabla('observacionesAlumno'.$i,$datos['observaciones'],'observacionesVentaFormacion');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		  </td>
		</tr>";
}
*/

function creaVentaFormacion(){
	$res=true;

	$datos=arrayFormulario();
	$fechaActualizacion=fechaBD();

	$importeVenta=formateaNumeroWeb($datos['importeVenta'],true);

	conexionBD();

	$res=consultaBD("INSERT INTO grupos(codigo,codigoAccionFormativa,fechaAlta,observacionesVenta,activo,esVenta,importeVenta,confirmada,observacionesParaFactura,estado,fechaActualizacionGrupo,codigoComercial) 
						  VALUES(NULL,".$datos['codigoAccionFormativa'].",'".$datos['fechaAlta']."','".$datos['observacionesVenta']."','PENDIENTE','".$datos['esVenta']."','".$importeVenta."',
						  '".$datos['confirmada']."','".$datos['observacionesParaFactura']."','PENDIENTE','$fechaActualizacion',".$datos['codigoComercial'].");");
	if($res){
		$codigoGrupo=mysql_insert_id();

		$res=insertaEmpresasParticipantesVentaFormacion($codigoGrupo,$datos);

		$res=$res && insertaAlumnosVentaFormacion($codigoGrupo,$datos);
		//$res=$res && insertaComplementosVentaFormacion($codigoGrupo,$datos);
	}

	cierraBD();

	return $res;
}


function actualizaVentaFormacion(){
	$res=true;

	$datos=arrayFormulario();
	$fechaActualizacion=fechaBD();

	$importeVenta=formateaNumeroWeb($datos['importeVenta'],true);

	conexionBD();

	$res=consultaBD("UPDATE grupos SET codigoAccionFormativa=".$datos['codigoAccionFormativa'].", fechaAlta='".$datos['fechaAlta']."', observacionesParaFactura='".$datos['observacionesParaFactura']."',
					 observacionesVenta='".$datos['observacionesVenta']."', importeVenta='$importeVenta', codigoComercial=".$datos['codigoComercial'].",
					 fechaActualizacionGrupo='$fechaActualizacion' WHERE codigo=".$datos['codigo']);
	if($res){
		$res=insertaEmpresasParticipantesVentaFormacion($datos['codigo'],$datos,false,true);

		$res=$res && insertaAlumnosVentaFormacion($datos['codigo'],$datos,true);
		//$res=$res && insertaComplementosVentaFormacion($datos['codigo'],$datos,true);
	}

	cierraBD();

	return $res;
}

//Fin parte de ventas de formación
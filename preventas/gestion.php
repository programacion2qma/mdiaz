<?php
  $seccionActiva=21;
  include_once("../cabecera.php");
  gestionVentaServicios();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="../js/selectAjax.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript" src="../../api/js/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-wysihtml5.es-ES.js"></script>

<script type="text/javascript" src="../js/campoFechaVenta.js"></script>

<script type="text/javascript">
$(document).ready(function(){
	if(!$('#observacionesParaFactura').hasClass('hide')){
		$('#observacionesParaFactura').wysihtml5({locale: "es-ES"});
	}

	oyenteCambios();

	//Oyentes tabla
	$('#insertaFilaTabla').click(function(){
		insertaFila('tablaVentaServicios');
		oyenteUltimaFila();
	});


	$('#insertaFilaComplementos').click(function(){
		insertaFila('tablaComplementos');
		oyenteUltimaFilaComplemento();
	});


	$('#eliminaFilaTabla,#eliminaFilaComplementos').click(function(){
		var tabla=$(this).attr('tabla');
		eliminaFila(tabla);
		
		oyenteCalcula();
	});
	//Fin oyentes tabla

	oyentePreciosServicios();
	oyentePreciosComplementos();

	$('#codigoCliente').change(function(){
		oyenteCliente($(this).val());
	});
});

function oyentePreciosServicios(){
	$('#tablaVentaServicios').find('select').change(function(){
		obtienePrecioServicio($(this));
		oyenteCalcula();
	});

	$('#tablaVentaServicios').find('.precioUnidad,.unidades').change(function(){
		oyenteCalcula();
	});
}

function oyenteUltimaFila(){
	obtienePrecioServicio($('#tablaVentaServicios tr:last select'));
	$('#tablaVentaServicios').find('tr:last select').change(function(){
		obtienePrecioServicio($(this));
		oyenteCalcula();
	});

	$('#tablaVentaServicios').find('tr:last .precioUnidad, tr:last .unidades').change(function(){
		oyenteCalcula();
	});
}

function oyenteCalcula(){
	var total=0;
	$('#tablaVentaServicios').find('.precioUnidad').each(function(){
		var fila=obtieneFilaCampo($(this));
		var precioUnidad=formateaNumeroCalculo($(this).val());
		var unidades=formateaNumeroCalculo($('#unidades'+fila).val());
		var totalLinea=precioUnidad*unidades;

		total+=totalLinea;
		$('#total'+fila).val(formateaNumeroWeb(totalLinea));
	});

	$('#tablaComplementos').find('.precioComplemento').each(function(){
		var precioComplemento=formateaNumeroCalculo($(this).val());
		total+=precioComplemento;
	});

	$('#total').val(formateaNumeroWeb(total));
}

function obtienePrecioServicio(campo){
	var fila=obtieneFilaCampo(campo);
	var precio=$('#servicio'+campo.val()).text();
	$('#precioUnidad'+fila).val(precio);

	//Para poner 1 unidad por defecto:
	if($('#unidades'+fila).val()=='' || $('#unidades'+fila).val()=='0'){
		$('#unidades'+fila).val('1');
	}
}

function oyenteCliente(codigoCliente){
	if(codigoCliente!='NULL'){
		var consulta=$.post('../listadoAjax.php?funcion=consultaDatosClienteVenta();',{'codigoCliente':codigoCliente},
		function(respuesta){
			$('#codigoComercial').val(respuesta.codigoComercial);
			$('#codigoColaborador').val(respuesta.codigoColaborador);
			$('#codigoTelemarketing').val(respuesta.codigoTelemarketing);
			$('#codigoServicio0').val(respuesta.codigoServicio);

			$('#codigoComercial').selectpicker('refresh');
			$('#codigoColaborador').selectpicker('refresh');
			$('#codigoTelemarketing').selectpicker('refresh');
			$('#codigoServicio0').selectpicker('refresh');
			obtienePrecioServicio($('#codigoServicio0'));
			oyenteCalcula();
		},'json');
	}
}

//Parte de complementos
function oyentePreciosComplementos(){
	$('#tablaComplementos').find('select').change(function(){
		obtienePrecioComplemento($(this));
		oyenteCalcula();
	});

	$('#tablaComplementos').find('.precioComplemento').change(function(){
		oyenteCalcula();
	});
}

function obtienePrecioComplemento(campo){
	var fila=obtieneFilaCampo(campo);
	var precio=$('#complemento'+campo.val()).text();
	$('#precioComplemento'+fila).val(precio);
}


function oyenteUltimaFilaComplemento(){
	obtienePrecioComplemento($('#tablaComplementos tr:last select'));
	$('#tablaComplementos').find('tr:last select').change(function(){
		obtienePrecioComplemento($(this));
		oyenteCalcula();
	});

	$('#tablaComplementos').find('tr:last .precioComplemento').change(function(){
		oyenteCalcula();
	});
}

function insertaComplemento(){
	insertaFila("tablaComplementos");
	oyenteComplemento($('#tablaComplementos tr:last .selectComplemento'));
}

function oyenteComplemento(selector){
	$(selector).change(function(){
		var codigoComplemento=$(this).val();
		var fila=obtieneFilaCampo($(this));
		var id=$(this).attr('id').replace('codigo','').replace('#','').replace(/\d/g,'');

		var consulta=$.post('../listadoAjax.php?funcion=consultaPrecioComplemento();',{
			'codigoComplemento':codigoComplemento
		});

		consulta.done(function(respuesta){
			$('#precio'+id+fila).val(respuesta);
			oyenteCalcula();
		});
	});
}

//Fin parte de complementos
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
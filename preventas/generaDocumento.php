<?php

@include_once('funciones.php');
	
	$documentos=array();
	$documentos[0]=generaContratoConsultoria($_GET['codigo']);
	generaZip($documentos);

function generaContratoConsultoria($codigo){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
    conexionBD();
    	$datos = datosRegistro("ventas_servicios",$codigo);
    	$cliente = datosRegistro('clientes',$datos['codigoCliente']);

	cierraBD();
	$nombreFichero="contrato_consultoria.docx";
	$documento=$PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	
	$texto='REPRESENTANTE LEGAL '.$cliente['administrador'].'<w:br />
	DNI '.$cliente['nifAdministrador'].' EMAIL '.$cliente['email'].' TELÉFONO '.$cliente['telefono'].'<w:br />
	RAZÓN SOCIAL '.$cliente['razonSocial'].' CIF '.$cliente['cif'].'<w:br />
	DOMICILIO '.$cliente['domicilio'].'<w:br />
	POBLACIÓN '.$cliente['localidad'].' PROVINCIA '.$cliente['provincia'].' CÓDIGO POSTAL '.$cliente['cp'].'<w:br />
	PERSONA DE CONTACTO '.$cliente['contacto'].' CORREO ELECTRÓNICO';
	$documento->setValue("texto",utf8_decode($texto));
	


	$documento->save('../documentos/consultorias/'.$nombreFichero);
	
	/*header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$nombreFichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$nombreFichero);*/

    return '../documentos/consultorias/'.$nombreFichero;
}

function generaZip($documentos){
	$zip = new ZipArchive();
	
	$nameZip = '../documentos/consultorias/documentos.zip';
	if(file_exists($nameZip)){
		unlink($nameZip);
	}
	$fichero = $nameZip;
	
	if($zip->open($fichero,ZIPARCHIVE::CREATE)===true) {
		foreach ($documentos as $documento){
			if(is_array($documento)){
				foreach ($documento as $doc){
					$name = str_replace('../documentos/consultorias/', '', $doc);
					$zip->addFile($doc , $name);
				}
			} else {
				$name = str_replace('../documentos/consultorias/', '', $documento);
				$zip->addFile($documento , $name);
			}
		}
	}
	if(!$zip->close()){
		echo "Se ha producido un error mientras se procesaba el paquete de documentos. Contacte con el webmaster indicándole el siguiente código de error: <br />";
		echo $zip->getStatusString();
	}

	header("Content-Type: application/zip");
	header("Content-Disposition: attachment; filename=documentos.zip");
	header("Content-Transfer-Encoding: binary");

	readfile($nameZip);
}
?>
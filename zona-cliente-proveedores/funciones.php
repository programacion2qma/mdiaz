<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesProveedores(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaProveedor();
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaProveedor();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		//$res=eliminaDatos('agrupaciones');
	}

	mensajeResultado('nombre',$res,'Proveedor');
    mensajeResultado('elimina',$res,'Proveedor', true);
}

function insertaProveedor(){
	$res=true;
	$res=insertaDatos('proveedores_lopd');
	return $res;
}

function actualizaProveedor(){
	$res=true;
	$res=actualizaDatos('proveedores_lopd');
	return $res;
}

function listadoProveedores(){
	global $_CONFIG;

	$columnas=array('nombre','nif','representante','servicio','fechaAlta','fechaBaja');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas,false,false,false);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT proveedores_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM proveedores_lopd INNER JOIN trabajos ON proveedores_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT proveedores_lopd.*, trabajos.codigoCliente, trabajos.fecha, servicios.servicio FROM proveedores_lopd INNER JOIN trabajos ON proveedores_lopd.codigoTrabajo=trabajos.codigo INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['nombre'],
			$datos['nif'],
			$datos['representante'],
			$datos['servicio'],
			formateaFechaWeb($datos['fechaAlta']),
			formateaFechaWeb($datos['fechaBaja']),
			creaBotonDetalles("zona-cliente-proveedores/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionProveedores(){
	operacionesProveedores();

	abreVentanaGestion('Gestión de Proveedores sin acceso a datos y con acceso a las dependencias','?','','icon-edit','margenAb');
	$datos=compruebaDatos('proveedores_lopd');


    if(!$datos){
    	abreColumnaCampos();
    		campoSelectConsulta('codigoTrabajo','Consultoría','SELECT trabajos.codigo, servicios.servicio AS texto FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo WHERE codigoCliente='.obtenerCodigoCliente(true),false,'selectpicker span5');
    	cierraColumnaCampos(true);
    }

	abreColumnaCampos();
		campoTexto('nombre','Nombre o denominación',$datos,'span5');
	cierraColumnaCampos(true);

	abreColumnaCampos();
		campoTexto('nif','NIF',$datos,'input-small');
		campoTexto('representante','Representante legal',$datos);
		campoTexto('servicio','Servicio',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
		campoFecha('fechaAlta','Fecha alta',$datos);
		campoFecha('fechaBaja','Fecha baja',$datos);
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}


//Fin parte de agrupaciones
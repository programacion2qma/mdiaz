<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de tutores

function operacionesTutores(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('tutores');
	}
	elseif(isset($_POST['nombre'])){
		$res=insertaDatos('tutores');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('tutores');
	}

	mensajeResultado('nombre',$res,'Tutor');
    mensajeResultado('elimina',$res,'Tutor', true);
}


function listadoTutores(){
	global $_CONFIG;

	$columnas=array('nombre','apellido1','apellido2','dni','observaciones','razonSocial','tutores.activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT tutores.codigo, nombre, apellido1, apellido2, dni, observaciones, razonSocial, tutores.activo FROM tutores LEFT JOIN centros_formacion ON tutores.codigoCentroFormacion=centros_formacion.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT tutores.codigo, nombre, apellido1, apellido2, dni, observaciones, razonSocial, tutores.activo FROM tutores LEFT JOIN centros_formacion ON tutores.codigoCentroFormacion=centros_formacion.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['nombre'],
			$datos['apellido1'],
			$datos['apellido2'],
			$datos['dni'],
			$datos['observaciones'],
			$datos['razonSocial'],
        	botonAcciones(array('Detalles','Listado de cursos','Listado de cursos tutorizando'),array('tutores/gestion.php?codigo='.$datos['codigo'],'tutores/listado-acciones.php?codigo='.$datos['codigo'],'tutores/listado-cursos.php?codigo='.$datos['codigo'].'&tipo=actualmente'),array("icon-search-plus","icon-pencil","icon-briefcase")),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionTutor(){
	operacionesTutores();

	abreVentanaGestion('Gestión de tutores','?','span3');
	$datos=compruebaDatos('tutores');

	campoSelectConsulta('codigoCentroFormacion','Centro de formación',"SELECT codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial;",$datos);
	campoTextoValidador('dni','DNI',$datos,'input-small validaDNI','tutores');
	campoTexto('nombre','Nombre',$datos);
	campoTexto('apellido1','Primer apellido',$datos);
	campoTexto('apellido2','Segundo apellido',$datos);
	campoTextoSimbolo('telefonoTutor','Teléfono','<i class="icon-phone"></i>',$datos,'input-small pagination-right');
	campoTextoSimbolo('emailTutor','eMail','<i class="icon-envelope"></i>',$datos,'input-large');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelect('actividadTutor','Tipo de actividad',array('','Formador','Dinamizador','Ambos'),array('','1','2','3'),$datos,'selectpicker span2 show-tick','');
	campoCheck('checkTipoTutoria','Tipo de tutoría',$datos,array('Correo Electrónico','Videoconferencia','Foro','Otras'),array('SI','SI','SI','SI'));
	echo "<div id='otrasTutorias' class='hide'>";
	campoTexto('otrosTipoTutoria','Descripción',$datos);
	echo "</div>";
	areaTexto('observaciones','Observaciones',$datos);
	campoRadio('activo','Activo',$datos,'SI');
	campoOculto(fecha(),'fechaActualizacionTutor');

	cierraVentanaGestion('index.php',true);
}

function filtroTutores(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoTexto(0,'Nombre');
	campoTexto(1,'Primer apellido');
	campoTexto(2,'Segundo apellido');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta(5,'Centro de formación',"SELECT codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial;");
	campoSelectSiNoFiltro(6,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function obtieneNombreTutor(){
	$codigo=$_GET['codigo'];

	$datos=consultaBD("SELECT CONCAT(nombre,' ',apellido1,' ',apellido2) AS nombre FROM tutores WHERE codigo=$codigo",true,true);

	return "<a href='gestion.php?codigo=$codigo'>".$datos['nombre']."</a>";
}

function obtieneNombreListado(){
	$tutor=obtieneNombreTutor();

	$res="que $tutor está tutorizando actualmente";	

	return $res;
}

function listadoCursosTutor($codigoTutor){
	global $_CONFIG;

	$columnas=array('grupos.fechaAlta','accion','modalidad','accionFormativa','grupos.grupo','grupos.numParticipantes','fechaInicio','fechaFin','anulado','razonSocial','codigoTutor','grupos.fechaAlta');
	
	$where=obtieneWhereListado("WHERE tutores_grupo.codigoTutor=$codigoTutor",$columnas);//Utilizo WHERE por la agrupación de clientes y tutores (el HAVING realiza el filtrado TRAS la selección, y el WHERE ANTES)
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, grupos.fechaAlta, accion, modalidad, accionFormativa, grupos.grupo, grupos.numParticipantes, fechaInicio, fechaFin, anulado, razonSocial, codigoTutor, grupos.activo, grupos.codigo AS codigoGrupo
			FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo 
			LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo 
			LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo 
			LEFT JOIN tutores_grupo ON grupos.codigo=tutores_grupo.codigoGrupo 
			$where 
			GROUP BY grupos.codigo";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			formateaFechaWeb($datos['fechaAlta']),
			$datos['accion'],
			$datos['modalidad'],
			$datos['accionFormativa'],
			$datos['grupo'],
			$datos['numParticipantes'],
			formateaFechaWeb($datos['fechaInicio']),
			formateaFechaWeb($datos['fechaFin']),
			"<div class='centro'>".$datos['anulado']."</div>",
			creaBotonDetalles("inicios-grupos/gestion.php?codigo=".$datos['codigoGrupo'],'Ver curso'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function creaBotonesGestionListadoTutor(){
    echo '<a class="btn-floating btn-large btn-default btn-eliminacion noAjax" href="index.php" title="Volver"><i class="icon-chevron-left"></i></a>';
}



function listadoAFTutor($codigoTutor){
	global $_CONFIG;

	$columnas=array('accion','accionFormativa','codigo');
	$having=obtieneWhereListado("WHERE codigoTutor=$codigoTutor",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, accion, accionFormativa 
			FROM acciones_formativas INNER JOIN tutores_accion_formativa ON acciones_formativas.codigo=tutores_accion_formativa.codigoAccionFormativa 
			$having";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['accion'],
			$datos['accionFormativa'],
        	creaBotonDetalles("acciones-formativas/gestion.php?codigo=".$datos['codigo'],'Ver Acción Formativa'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function filtroCursosTutor(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoFecha(0,'Fecha desde');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(11,'Hasta');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de tutores
<?php
include_once('../config.php');//Carga del archivo de configuración
include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
include_once('../funciones.php');//Carga de las funciones globales

function operacionesIncidenciasLOPD(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('incidencias');
	   	$res= $res && insertaFicherosAdjuntos($_POST['codigo']);		
	}
	elseif(isset($_POST['incidente'])){
		$res=insertaDatos('incidencias');
	   	$res= $res && insertaFicherosAdjuntos($res);		
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('incidencias');
	}

	mensajeResultado('incidente',$res,'Incidencia');
    mensajeResultado('elimina',$res,'Incidencia', true);
}


function gestionIncidenciaLOPD(){
	operacionesIncidenciasLOPD();
	
	abreVentanaGestion('Gestión de Incidencias','?','','icon-edit','',true,'noAjax');
	$datos=compruebaDatos('incidencias');

	echo "<fieldset class='sinFlotar'>";
	abreColumnaCampos();
		campoSelectConsulta('codigoCliente','Cliente','SELECT codigo, razonSocial AS texto FROM clientes ORDER BY razonSocial',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		if($datos!=false){
			campoTexto('referencia','Referencia', $datos['referencia'],'input-small',true);
		}
		else{
			$consulta=consultaBD("SELECT MAX(referencia) AS max FROM incidencias ORDER BY referencia DESC LIMIT 1;",true);
		    $datosConsulta=mysql_fetch_assoc($consulta);

		    $codigoInterno=$datosConsulta['max']+1;

		    if($codigoInterno<10){
				$codigoInterno="000".$codigoInterno;
			}elseif($codigoInterno>=10&&$codigoInterno<100){
				$codigoInterno="00".$codigoInterno;
			}elseif($codigoInterno>=100&&$codigoInterno<1000){
				$codigoInterno="0".$codigoInterno;
			}
		    
		    campoTexto('referencia','Referencia', $codigoInterno."/".date('Y'),'input-small',true);
		}	
	//campoTexto('referencia','Referencia',$datos,'input-small');
	campoTexto('incidente','Incidente',$datos);
	campoFecha('fechaDeteccion','Fecha Detección',$datos);
	campoFecha('fechaOcurrio','Fecha en que ocurrió',$datos);
	campoTexto('ficherosDatosQuiebra','Fichero/s en el que están incluidos los datos objeto de quiebra de seguridad',$datos);
	campoTexto('naturalezaQuiebra','Naturaleza de la quiebra de seguridad',$datos);
	cierraColumnaCampos();

	abreColumnaCampos();
	campoTexto('categoriaDatosAfectados','Categorías de datos afectados',$datos);
	campoTexto('categoriaInteresadosAfectados','Categoría de interesados afectados',$datos);
	campoTexto('medidasAdoptadas','Medidas adoptadas para solventar la quiebra',$datos);
	campoTexto('medidasAplicadas','Medidas aplicadas para paliar posibles efectos negativos sobre los interesados',$datos);							
	campoFecha('fechaNotificadaAutoridad','Notificada a la autoridad de protección de datos competente el día',$datos);
	campoFecha('fechaNotificadaInteresados','Notificada a los interesados el día',$datos);
	campoSelect('cerrada','Incidencia Cerrada',array('No','Si'),array('NO','SI'),$datos,'selectpicker span1 show-tick');
	echo "<div id='cajaFecha'>";
	campoFecha('fechaResolucion','Fecha de cierre y aprobación por el responsable del tratamiento',$datos);
	echo "</div>";
	cierraColumnaCampos();
	echo "</fieldset>";

	echo "<h3 class='apartadoFormulario'> DATOS ADJUNTOS:</h3>";
	echo "<fieldset class='sinFlotar'>";
	creaTablaFicheros($datos);
	echo "</fieldset>";

	cierraVentanaGestion('index.php',true);
}


function imprimeIncidenciasLOPD(){
	$consulta=consultaBD("SELECT incidencias.*, clientes.razonSocial FROM incidencias LEFT JOIN clientes ON incidencias.codigoCliente=clientes.codigo",true);
	while($datos=mysql_fetch_assoc($consulta)){

		if($datos['fechaDeteccion']!=''){
			$notificadoAutoridad='SI';
		}else{
			$notificadoAutoridad='NO';			
		}

		echo "<tr>
				<td> ".$datos['referencia']." </td>
				<td> ".$datos['razonSocial']." </td>
				<td> ".$datos['incidente']." </td>				
				<td> ".formateaFechaWeb($datos['fechaDeteccion'])." </td>
				<td> $notificadoAutoridad </td>
				<td class='centro'>
					<div class='btn-group centro'>
						<button type='button' class='btn btn-propio dropdown-toggle' data-toggle='dropdown'><i class='icon-cogs'></i> Acciones <span class='caret'></span></button>
					  	<ul class='dropdown-menu' role='menu'>
						    <li><a href='gestion.php?codigo=".$datos['codigo']."'><i class='icon-search-plus'></i> Detalles</i></a></li> 
						    <li class='divider'></li>
						    <li><a href='generaWord.php?codigo=".$datos['codigo']."' class='noAjax' target='_blank'><i class='icon-cloud-download'></i> Descarga de incidencia</i></a></li>
						</ul>
					</div>				
				</td>
				<td>
					<input type='checkbox' name='codigoLista[]' value='".$datos['codigo']."'>
        	  	</td>
			 </tr>";
	}
}

function obtieneDepartamentoIncidencia($departamento){
	$departamentos=array('ADMINISTRACION'=>'Administración','COMERCIAL'=>'Comercial','TECNICO'=>'Técnico');
	return $departamentos[$departamento];
}

function obtienePrioridadIncidencia($prioridad){
	$prioridades=array('NORMAL'=>"<span class='label label-info'>Normal</span>",'ALTA'=>"<span class='label label-danger'>Alta</span>",'BAJA'=>"<span class='label'>Baja</span>");
	return $prioridades[$prioridad];
}

function obtieneEstadoIncidencia($estado){
	$estados=array('PENDIENTE'=>"<span class='label label-danger'>Pendiente</span>",'RESUELTA'=>"<span class='label label-success'>Resuelta</span>",'ENPROCESO'=>"<span class='label label-warning'>En proceso</span>");
	return $estados[$estado];
}




function generaDatosGraficoIncidencias(){
	$datos=array('PENDIENTE'=>0,'RESUELTA'=>0);

	$consulta=consultaBD("SELECT * FROM incidencias;",true);	
	while($datosConsulta=mysql_fetch_assoc($consulta)){
		$datos=clasificaEstadoIncidencia($datosConsulta['cerrada'],$datos);
	}

	return $datos;
}

function clasificaEstadoIncidencia($estado,$datos){
	if($estado=='SI'){
		$datos['RESUELTA']++;
	}
	else{
		$datos['PENDIENTE']=1;	
	}

	return $datos;
}

function creaTablaFicheros($datos){
	echo '<table class="table table-striped table-bordered" style="width:50%;margin-left:220px;" id="tablaAdjuntos">
	  		<thead>
				<tr>
					<th class="centro">Ficheros</th>
					<th></th>
				</tr>
			</thead>
			<tbody>';
				$i=0;

				if($datos){
					$consulta=consultaBD("SELECT * FROM incidencias_lopd_adjuntos WHERE codigoIncidenciaLopd='".$datos['codigo']."';", true);				  
					while($datosAdjuntos=mysql_fetch_assoc($consulta)){
					 	echo "<tr>";
						campoFichero('ficheroAdjunto'.$i,'',1,$datosAdjuntos['ficheroAdjunto'],$datosAdjuntos['directorio']);
						echo"
								<td>
									<input type='checkbox' name='filasTabla[]' value='$i'>
					        	</td>
							</tr>";
						$i++;
					}
				}


				if($i==0){
				echo '<tr>';
						campoFichero('ficheroAdjunto'.$i,'',1);
						echo "<td>
                                <input type='checkbox' name='filasTabla[]' value='1'>
                             </td>";
				echo '</tr>';
				}

				echo '</tbody>
				</table>
				<center>
        			<button type="button" class="btn btn-success" onclick="insertaFila(\'tablaAdjuntos\');"><i class="icon-plus"></i></button> 
        			<button type="button" class="btn btn-danger" onclick="eliminaFila(\'tablaAdjuntos\');"><i class="icon-minus"></i></button> 
    			</center>';
}

function insertaFicherosAdjuntos($codigo){
	global $_CONFIG;
	$res=true;
	$i=0;
	
	$res = $res && consultaBD("DELETE FROM incidencias_lopd_adjuntos WHERE codigoIncidenciaLopd='$codigo'");	
	
	conexionBD();
	while(isset($_FILES['ficheroAdjunto'.$i])){
		if($_FILES['ficheroAdjunto'.$i]['tmp_name']!=''){
			$fichero=subeDocumento('ficheroAdjunto'.$i,time().'-'.$i,'../documentos/incidenciasLOPD/');
			$res=consultaBD('INSERT INTO incidencias_lopd_adjuntos VALUES(NULL,'.$codigo.',"'.$fichero.'","../documentos/incidenciasLOPD/")');
		}
		$i++;
	}
	cierraBD();
	return $res;
}

//Fin parte de incidencias
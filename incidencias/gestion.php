<?php
  $seccionActiva=25;
  include_once("../cabecera.php");
  gestionIncidenciaLOPD();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="../../api/js/filasTabla.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_CONFIG['raiz']; ?>../api/js/bootstrap-select.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    $('.selectpicker').selectpicker();

    oyenteResolucion();
    $('select[name=cerrada]').change(function(){
    	oyenteResolucion();
    });
  });

  function oyenteResolucion(){
  	if($('select[name=cerrada]').val()=='SI'){
  		$('#cajaFecha').removeClass('hide');
  	}
  	else{
  		$('#cajaFecha').addClass('hide');
  	}
  }
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
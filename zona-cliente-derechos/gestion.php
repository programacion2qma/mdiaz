<?php
  $seccionActiva=63;
  include_once("../cabecera.php");
  gestionDerechos();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	$('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});

	rellenaSelect($('#tipo option:selected').val());
	$('#tipo').change(function(){
		rellenaSelect($(this).val());
	});
	$('#respuesta').change(function(){
		otrosDatos($(this).val());
	});
});

function rellenaSelect(tipo){
	if(tipo!='NULL'){
		var codigo=$('#codigo').length?$('#codigo').val():0;
		var consulta=$.post('../listadoAjax.php?include=zona-cliente-derechos&funcion=rellenaSelect();',{'codigo':codigo,'tipo':tipo});
		consulta.done(function(respuesta){
			$('#respuesta').html(respuesta);
			$('#respuesta').selectpicker('refresh');
			otrosDatos($('#respuesta option:selected').val());
		});
	} else {
		$('#respuesta').html('<option value="NULL"></option>');
		$('#respuesta').selectpicker('refresh');
		otrosDatos($('#respuesta option:selected').val());
	}
}

function otrosDatos(valor){
	if(valor=='Procede la visualización interesada (3)'){
		$('.divOtrosDatos').removeClass('hide');
		$('.divTratamiento').addClass('hide');
		$('label[for=otrosDatos]').html('Fecha y hora:')
	} else if(valor=='Comunicación al Responsable de Ficheros (3)' || valor=='Comunicación al Responsable de Ficheros (4)'){
		$('.divOtrosDatos').removeClass('hide');
		$('.divTratamiento').addClass('hide');
		$('label[for=otrosDatos]').html('Responsable:')
	} else if(valor=='Comunicación a cesionario de dcho. de limitación ejercitado y otorgado (4.1)' || valor=='Comunicación a cesionario de dcho. de rectificación ejercitado y otorgado (6.1)' || valor=='Comunicación a cesionario de dcho. de cancelación ejercitado y otorgado (6.1)'){
		$('.divOtrosDatos').removeClass('hide');
		$('.divTratamiento').addClass('hide');
		$('label[for=otrosDatos]').html('Cedidos a:')
	} else if(valor=='Otorgamiento de acceso (6)' || valor=='Otorgamiento de rectificación (5)' || valor=='Otorgamiento de cancelación (5)' || valor=='Otorgamiento de oposición (6)' || valor=='Otorgamiento de limitación del tratamiento (4)' || valor=='Otorgamiento de portabilidad (6)' ){
		$('.divOtrosDatos').addClass('hide');
		$('.divTratamiento').removeClass('hide');
	} else {
		$('.divOtrosDatos').addClass('hide');
		$('.divTratamiento').addClass('hide');
	}
}
</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
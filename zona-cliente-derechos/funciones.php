<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesDerechos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDerecho();
	}
	elseif(isset($_POST['interesado'])){
		$res=insertaDerecho();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('derechos');
	}

	mensajeResultado('interesado',$res,'Derecho');
    mensajeResultado('elimina',$res,'Derecho', true);
}

function insertaDerecho(){
	$res=true;
	$res=insertaDatos('derechos');
	return $res;
}

function actualizaDerecho(){
	$res=true;
	$res=actualizaDatos('derechos');
	return $res;
}


function listadoDerechos(){
	global $_CONFIG;

	$columnas=array('interesado','fechaRecepcion','fechaRespuesta','respuesta','tipo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT derechos.* FROM derechos INNER JOIN clientes ON derechos.codigoCliente=clientes.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['interesado'],
			formateaFechaWeb($datos['fechaRecepcion']),
			formateaFechaWeb($datos['fechaRespuesta']),
			$datos['respuesta'],
			$datos['tipo'],
			creaBotonDetalles("zona-cliente-derechos/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionDerechos(){
	operacionesDerechos();

	abreVentanaGestion('Gestión de Derechos','?','','icon-edit','margenAb');
	$datos=compruebaDatos('derechos');

    if(!$datos){
    	$codigoCliente=obtenerCodigoCliente(true);
    	campoOculto($codigoCliente,'codigoCliente');
    }  else {
    	$codigoCliente=$datos['codigoCliente'];
    }

	abreColumnaCampos();
   		campoTexto('interesado','Interesado',$datos,'span7');
   		campoTexto('interesadoDni','DNI',$datos,'input-small');
   		campoTexto('representante','Representante',$datos,'span7');
   		campoTexto('representanteDni','DNI',$datos,'input-small');
   		campoTexto('direccion','Dirección',$datos,'span7');
   		$valores=array('','Acceso','Rectificación','Supresión o cancelación','Oposición y decisiones individuales automatizadas','Limitación del tratamiento','Portabilidad de los datos');
   		campoSelect('tipo','Tipo de derecho',$valores,$valores,$datos,'selectpicker span4 show-tick');
   	cierraColumnaCampos(true);
   	abreColumnaCampos();
		campoFecha('fechaRecepcion','Fecha de Recepción',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoFecha('fechaRespuesta','Fecha de Respuesta',$datos);
	cierraColumnaCampos(true);
	abreColumnaCampos();
		campoSelect('respuesta','Respuesta',array(''),array(''),$datos,'selectpicker span6 show-tick');
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divOtrosDatos hide');
		areaTexto('otrosDatos','Otros datos',$datos); 
	cierraColumnaCampos(true);

	abreColumnaCampos('span3 divTratamiento hide');
		campoSelectConsulta('codigoTratamiento','Fichero/Tratamiento','SELECT codigo, nombreFichero AS texto FROM declaraciones WHERE codigoCliente='.$codigoCliente,$datos);
	cierraColumnaCampos(true);

	cierraVentanaGestion('index.php',true);
}

function rellenaSelect(){
	$valores=array('Acceso'=>array('Subsanar defecto falta de identificación - art. 12.6 RGPD (1)','No figuran datos personales del interesado (2)','Procede la visualización interesada (3)','Comunicación al Responsable de Ficheros (4)','Datos personales bloqueados (5)','Otorgamiento de acceso (6)')
		,'Rectificación'=>array('Subsanar defecto falta de identificación - art. 12.6 RGPD (1)','No figuran datos personales del interesado (2)','Comunicación al Responsable de Ficheros (3)','Datos personales bloqueados (4)','Otorgamiento de rectificación (5)','Comunicación a cesionario de dcho. de rectificación ejercitado y otorgado (6.1)')
		,'Supresión o cancelación'=>array('Subsanar defecto falta de identificación - art. 12.6 RGPD (1)','No figuran datos personales del interesado (2)','Comunicación al Responsable de Ficheros (3)','Denegación de supresión inmediata por art. 17.3 RGPD (4)','Otorgamiento de cancelación (5)','Comunicación a cesionario de dcho. de cancelación ejercitado y otorgado (6.1)')
		,'Oposición y decisiones individuales automatizadas'=>array('Subsanar defecto falta de identificación - art. 12.6 RGPD (1)','No figuran datos personales del interesado (2)','Comunicación al Responsable de Ficheros (3)','Datos personales bloqueados (4)','Denegación de oposición (5)','Denegación petición a no ser objeto de decisión individual automatizada (5.2)','Otorgamiento de oposición (6)')
		,'Limitación del tratamiento'=>array('Subsanar defecto falta de identificación - art. 12.6 RGPD (1)','No figuran datos personales del interesado (2)','Comunicación al Responsable de Ficheros (3)','Otorgamiento de limitación del tratamiento (4)','Comunicación a cesionario de dcho. de limitación ejercitado y otorgado (4.1)')
		,'Portabilidad de los datos'=>array('Subsanar defecto falta de identificación - art. 12.6 RGPD (1)','No figuran datos personales del interesado (2)','Comunicación al Responsable de Ficheros (3)','Datos personales bloqueados (4)','Denegación de portabilidad (5)','Otorgamiento de portabilidad (6)'));
	if($_POST['codigo']!=0){
		$derecho=datosRegistro('derechos',$_POST['codigo']);
		$respuesta=$derecho['respuesta'];
	} else {
		$respuesta=0;
	}
	$res='<option value="NULL"></option>';
	$valores=$valores[$_POST['tipo']];
	foreach ($valores as $key => $value) {
		$res.='<option value="'.$value.'"';
		if(trim($respuesta)==trim($value)){
			echo 'entra<br/>';
			$res.='selected';
		}
		$res.='>'.$value.'</option>';
	}
	echo $res;
}
//Fin parte de agrupaciones
<?php
@include_once('funciones.php');
compruebaSesion();

if($_GET['ref']=='pbc'){
	ob_start();
	$contenido=generaPBC();
	$nombre='manual_de_pbc.pdf';
	require_once('../../api/html2pdf/html2pdf.class.php');
	$html2pdf=new HTML2PDF('P','A4','es');
	$html2pdf->pdf->SetDisplayMode('fullpage');
	$html2pdf->WriteHTML($contenido);
	ob_clean();
	$html2pdf->Output($nombre);
} else if($_GET['ref']=='lopd'){
	generaLOPD();
	header("Content-Type: application/vnd.ms-docx");
	header("Content-Disposition: attachment; filename=Documento_seguridad.docx");
	header("Content-Transfer-Encoding: binary");

	readfile('../documentos/consultorias/Documento_seguridad.docx');
} 



function generaPBC(){

	$datos    = datosRegistro("manuales_pbc",$_GET['codigo'],'codigoTrabajo');
	$trabajo  = datosRegistro("trabajos",$_GET['codigo']);
	$cliente  = datosRegistro('clientes',$trabajo['codigoCliente']);
	$personas = consultaBD('SELECT * FROM personas_pbc WHERE codigoManual='.$datos['codigo'], true);

	$sujetos = array(
		'FISICA'   => 'Persona física',
		'JURIDICA' => 'Persona jurídica'
	);

	$actividades = array(
		'ABOGACIA'     => 'Abogacía',
		'FISCAL'       => 'Asesoramiento fiscal',
		'CONTABLE'     => 'Asesoramiento fiscal y contable',
		'CONTABILIDAD' => 'Contabilidad externa',
		'AUDITORIA'    => 'Auditoría de cuentas',
		'PROMOCION'    => 'Promoción inmobiliaria',
		'OTRA'         => 'Otra'
	);

	$actividad = $datos['actividad'] == 'OTRA' ? $datos['otra'] : $actividades[$datos['actividad']];

	$empleados = array(
		10 => 'Menos de 10 personas',
		49 => 'Entre 10 y 49 personas',
		50 => '50 personas o más'
	);

	$volumenes = array(
		1 => 'No supera los 2 millones de euros',
		2 => 'Supera los 2 millones de euros sin superar los 10 millones de euros',
		3 => 'Supera los 10 millones de euros'
	);

	$contenido = "
		<style type='text/css'>
			<!--
				body{
					font-size:11px;
					font-family: helvetica;
					font-weight: lighter;
					line-height: 24px;
				}

				.principal{
					width:310px;
					border-left:1px solid #0071C1;
					padding-left:5px;
					padding-top:5px;
					padding-bottom:10px;
					margin-top:200px;
					margin-left:200px;
				} 
				
				.principal h1{
					color:#0071C1;
					font-weight:normal;
					line-height:45px;
					font-size:34px;
				}

				.fechas{
					margin-left:200px;
					margin-top:500px;
				}

				h2{
					text-align:center;
					font-size:16px;
				}

				p {
					text-align:justify;
				}

				ul{
					list-style:none;
					margin-left:-33px;
				}

				.conEstilo{
					list-style:normal;
					margin-left:-23px;
				}

				.sinMargen{
					margin-top:-10px;
					padding-top:0px;
				}

				ul li{
					text-align:justify;
				}

				#tablaFirmas{
					width:100%;
					margin-top:200px;
				}

				#tablaFirmas td{
					width:50%;
					text-align:center;
				}

				#articulo12{
					font-size:9px;
				}

				.textoPie{
					text-align:left;
					font-size:9px;
					position:absolute;
					bottom:-15px;
				}
				.titleCabecera{
					margin-left:20px;
					margin-top:5px;
					font-size:9px;
				}
			-->
		</style>
	<page footer='page'>
	<page_header>
		<div class='titleCabecera'>
			Documento generado por M&D Asesores
		</div>
	</page_header>
	<div class='principal'>
		<h1>MANUAL DE PREVENCIÓN DEL BLANQUEO DE CAPITALES Y DE LA FT</h1>
		".$cliente['razonSocial'].", NIF: ".$cliente['cif']."
	</div>
	<div class='fechas'>
		De ".formateaFechaWeb($datos['fechaInicio'])." a ".formateaFechaWeb($datos['fechaFin'])."
	</div>
	</page>
	<page footer='page' backbottom='20mm' backleft='20mm' backright='20mm' backtop='20mm'>
	<page_header>
		<div class='titleCabecera'>
			Documento generado por M&D Asesores
		</div>
	</page_header>
	<h2>MANUAL DE PREVENCIÓN DEL BLANQUEO DE CAPITALES Y DE LA FINANCIACION DEL TERRORISMO</h2>
	<p>Adaptado a la Ley 10/2010,  de 28 de abril, de Prevención del Blanqueo de Capitales y de la Financiación del Terrorismo y al Real Decreto 304/2014, de 5 de mayo, que la desarrolla.</p>

	<p><b>1. DATOS DEL SUJETO OBLIGADO</b></p>
	<p>Nombre o Razón social: ".$cliente['razonSocial']."</p>
	<p>Domicilio: ".$cliente['domicilio']." - CP: ".$cliente['cp']." - ".$cliente['localidad']." - ".$cliente['provincia']."</p>
	<p>Inscripción en el registro Mercantil: Tomo ".$datos['tomo'].", libro ".$datos['libro'].", folio ".$datos['folio'].", sección ".$datos['seccion'].", hoja ".$datos['hoja']."</p>
	<p>NIF: ".$cliente['cif']."</p>
	<p>Actividad: ".$actividad."</p>
	<p>Servicios habituales: ".$datos['servicios']."</p>
	<p>Número de empleados del sujeto obligado -con inclusión de los agentes-: ".$empleados[$datos['empleados']]."</p>
	<p>Volumen de negocios anual o balance general anual: ".$volumenes[$datos['volumen']]."</p>
	<p><b>2. NORMATIVA  DE PREVENCIÓN DEL BLANQUEO DE CAPITALES Y DE LA FINANCIACIÓN DEL TERRORISMO. </b></p>
	<p><b>Normativa internacional</b></p>
	<ul class='sinMargen'>
		<li>- Directiva 2005/60/CEE del Parlamento Europeo y del Consejo, de 26 de octubre de 
2005 relativa a la prevención de la utilización del sistema financiero para el blanqueo de capitales y para la financiación del terrorismo (DOCE 25/11/2005). 
</li>
		<li>- Directiva de la Comisión 2006/70/CE, de 1 de agosto de 2006. </li>
		<li>- Nuevas Cuarenta Recomendaciones del Grupo de Acción Financiera Internacional sobre el Blanqueo de Capitales (GAFI) y notas interpretativas. Febrero de 2012. </li>
	</ul>
	<p><b>Normativa española</b></p>
	<ul class='sinMargen'>
		<li>- Ley 10/2010, de 28 de abril, de prevención de blanqueo de capitales y de la financiación del terrorismo, en adelante “LPBC”, modificada por Ley 19/2013, de 9 de diciembre, de transparencia, acceso a la información pública y buen gobierno y por Ley 21/2011, de 26 de julio, de dinero electrónico. </li>
		<li>- Real Decreto 304/2014, de 5 de mayo, por el que se aprueba el Reglamento de la Ley 10/2010, de 28 de abril, de Prevención de Blanqueo de Capitales y de la Financiación del Terrorismo, en adelante “RLPBC”. </li>
		<li>- Resolución de 10 de agosto de 2012, de la Secretaría General del Tesoro y Política Financiera, por la que se publica el Acuerdo de 17 de julio de 2012, de la Comisión de Prevención del Blanqueo de Capitales e Infracciones Monetarias, por el que se determinan las jurisdicciones que establecen requisitos equivalentes a los de la legislación española de prevención del blanqueo de capitales y de la financiación del terrorismo. </li>
		<li>- Real Decreto 1080/1991, de 5 de julio, por el que se determinan los países o territorios a que se refieren los artículos 2, apartado 3, número 4, de la Ley 17/1991, de 27 de mayo, de Medidas Fiscales Urgentes, y 62 de la Ley 31/1990, de 27 de diciembre, de Presupuestos Generales del Estado para 1991. </li>
	</ul>
	<p>En materia penal, los artículos 301 a 304 del Código Penal en vigor.</p>
	<p>Son sujetos obligados para prevenir el blanqueo de capitales y la financiación del terrorismo, entre otros, auditores de cuentas, contables externos, asesores fiscales –artículo 2.1 letra m)- así como quienes ejercen la abogacía en los términos previstos en el artículo 2.1 letra ñ) de la LPBC, en adelante, “las operaciones sujetas”. También se considera a los Abogados sujetos obligados cuando realicen actividades de asesoramiento fiscal o ciertas de representación o apoyo, no estrictamente propias ni exclusivas de la profesión, incluidas en las letras m) y o) del artículo citado. </p><br/>
	<p>".$cliente['razonSocial'].", el día ".formateaFechaWeb($datos['fechaInicio'])." aprueba el presente MANUAL, en el cual se establecen los procedimientos a seguir en el ejercicio de su actividad para el cumplimiento de la normativa de Prevención del Blanqueo de Capitales y de la Financiación del Terrorismo en vigor.</p>
	<p>En materia de prevención de blanqueo de capitales y financiación del terrorismo, ".$cliente['razonSocial']." cumplirá con la obligación de comunicar al SEPBLAC cualquier operación que presente indicios de ser constitutiva de blanqueo de capitales, obtendrá de los clientes información suficiente sobre su identidad, titularidad real y actividad económica o profesional y propósito de la operación; establecerá procedimientos de control interno y comunicación, y recibirá formación adecuada en cuestiones relativas a la prevención del blanqueo de capitales y la financiación del terrorismo. </p>
	<p>Todas las actividades desarrolladas por ".$cliente['razonSocial']." se realizarán conforme a las mejores prácticas y con estricto cumplimiento de la normativa vigente, con el más firme propósito de no participar en ninguna operación ilegal.</p>
	<p><b>Análisis previo de riesgo</b></p>
	<p>".$cliente['razonSocial'].", en cumplimiento del artículo 32 del RLPBC,  ha realizado un previo análisis del riesgo de blanqueo de capitales y de financiación del terrorismo del bufete, en el que identifica y evalúa los riesgos a los que está expuesto,  haciendo hincapié en los tipos de clientes, países o áreas geográficas, productos, servicios, operaciones y canales de distribución. <br/>
Dicho análisis será objeto de revisión periódicamente y, en todo caso, cuando se produzca un cambio significativo que pudiera influir en el perfil de riesgo del bufete como, por ejemplo, la prestación de nuevos servicios o el uso de una nueva tecnología, debiendo aplicarse medidas adecuadas para gestionar y mitigar los riesgos identificados en el análisis. <br/>
Todos los socios, empleados y colaboradores de ".$cliente['razonSocial']." que presten servicios relacionados con las operaciones sujetas, conocerán  y cumplirán las normas estipuladas en el presente Manual para conocer al cliente y detectar operaciones que puedan constituir indicios o certeza de blanqueo de capitales o de financiación del terrorismo, utilizando la herramienta informática que al efecto se determine por ".$cliente['razonSocial'].".
</p>
<p><b>2.1. Normativa interna existente y ámbito de aplicación </b></p>
<p>En cumplimiento de lo establecido por la LPBC y por el RLPBC, ".$cliente['razonSocial']." aprueba el presente Manual, estableciendo a través del mismo las normas y procedimientos que se aplicarán a partir de la fecha de su aprobación –".formateaFechaWeb($datos['fechaInicio'])."- y hasta que se acuerde su modificación en función del desarrollo normativo, por la experiencia obtenida así como ante  cambios significativos en la propia actividad u organización del sujeto obligado.</p>
<p>La normativa contenida en el presente manual es de aplicación a todos los que prestan servicios a  ".$cliente['razonSocial'].", ya sea en calidad de titular del despacho, socio/a, empleado/a o colaborador/a, en lo que se refiere a las siguientes operaciones sujetas: </p>
<ul>
	<li>- Asesoramiento fiscal.</li>
	<li>- Contabilidad externa.</li>
	<li>- Auditoría de cuentas.</li>
	<li>- Cuando, en el ejercicio de la abogacía, procuraduría u otra profesión independiente, participen en la concepción, realización o asesoramiento de operaciones por cuenta de clientes relativas a la compraventa de bienes inmuebles o entidades comerciales, la gestión de fondos, valores u otros activos, la apertura o gestión de cuentas corrientes, cuentas de ahorros o cuentas de valores, la organización de las aportaciones necesarias para la creación, el funcionamiento o la gestión de empresas o la creación, el funcionamiento o la gestión de fideicomisos («trusts»), sociedades o estructuras análogas, o cuando actúen por cuenta de clientes en cualquier operación financiera o inmobiliaria.</li>
	<li>- Cuando, con carácter profesional, se presten los siguientes servicios a terceros: constituir sociedades u otras personas jurídicas; ejercer funciones de dirección o secretaría de una sociedad, socio de una asociación o funciones similares en relación con otras personas jurídicas o disponer que otra persona ejerza dichas funciones; facilitar un domicilio social o una dirección comercial, postal, administrativa y otros servicios afines a una sociedad, una asociación o cualquier otro instrumento o persona jurídicos; ejercer funciones de fideicomisario en un fideicomiso (“trust”) expreso o instrumento jurídico similar o disponer que otra persona ejerza dichas funciones; o ejercer funciones de accionista por cuenta de otra persona, exceptuando las  sociedades que coticen en un mercado regulado y estén sujetas a requisitos de información conformes con el derecho comunitario o a normas internacionales equivalentes o disponer que otra persona ejerza dichas funciones. </li>
	<li>- Promoción inmobiliaria y ejercicio profesional de actividades de agencia, comisión o intermediación en la compraventa de bienes inmuebles.</li>
</ul>
<p>Quienes ejercen la Abogacía no estarán sometidos a las obligaciones establecidas en los Artículos 7.3 (no establecimiento de relaciones de negocio), 18 (comunicación de operaciones) y 21 (colaboración con la Comisión de Prevención de Blanqueo de Capitales) de la LPBC, con respecto a la información que reciban de uno de sus clientes u obtengan sobre él al determinar su posición jurídica, esto es, asesorarles o desempeñar su misión de defensa incluido el asesoramiento sobre la incoación o la forma de evitar un proceso, independientemente de si han recibido u obtenido dicha información antes, durante o después de tales procesos. </p>
<p>Quienes ejercen la Abogacía guardarán el deber de secreto profesional de conformidad con la legislación vigente. </p>
<p><b>2.2. Comunicación y acceso a la normativa </b></p>
<p>El Manual formará parte de los procedimientos internos de ".$cliente['razonSocial'].", siendo obligatorio su conocimiento y cumplimiento para todos los que prestan servicios al mismo, ya sea como titular del despacho, socio/a, empleado/a o colaborador/a, en cualquier servicio relacionado con las operaciones sujetas. Todos tendrán acceso a la versión del Manual actualizada, y estarán implicados en la tarea de prevención, para lo que serán debidamente informados e instruidos sobre la materia.</p>
<p><b>2.3. Idoneidad de empleados y directivos </b></p>
<p>Cuando se incorporen nuevos socios/as, empleados/as o colaboradores/as, se recabará previamente a los candidatos un historial profesional completo, verificándose las actividades declaradas con documentación aportada por los aspirantes y fuentes externas. De mantenerse dudas sobre la certeza de la información recibida, podrá solicitarse al candidato la aportación de un certificado de antecedentes penales.</p>
<br/><br/><br/>
<p><b>3. ORGANIZACIÓN INTERNA</b></p>
<p><b>3.1. Estructura organizativa </b></p>";
if($datos['empleados']==50 || $datos['volumen']==3){
	$contenido.='<p><b>El órgano de control interno está compuesto por las siguientes personas:</b></p>
	<ul>';
	while($persona=mysql_fetch_assoc($personas)){
		$contenido.='<li>Don/Doña '.$persona['nombre'].'</li>';
	}
	$contenido.='</ul>';

}
$contenido.="<p><b>Actuará como representante ante el SEPBLAC Don/Doña ".$datos['representante']." NIF ".$datos['nifRepresentante']."</b></p>";

if($datos['empleados']==50 || $datos['volumen']==3){
	$contenido.='<p>El órgano de control, con la colaboración de las personas que de ellos dependen, será el responsable de la aplicación de las políticas y procedimientos recogidos en este manual. </p>';

}

if($datos['empleados']>10 || $datos['volumen']==2){
	$contenido.='<p>El nombramiento de la persona designada como representante ante el SEPBLAC será debidamente comunicado al mismo, utilizando el formulario correspondiente que se dirigirá a dicho organismo –al SEPBLAC-. Los  cambios de representante también han de ser comunicados. En la actualidad, el formulario utilizado al efecto es el F22, que puede descargarse en la siguiente dirección:<br/>http://www.sepblac.es/espanol/sujetos_obligados/Formulario_F22.doc. </p>';

}

if($datos['empleados']==50 || $datos['volumen']==3){
	$contenido.='<p><b>3.2. Funciones del órgano de control interno  y del representante ante el SEPBLAC.</b></p>
	<p>Las funciones del <u>órgano de control</u> serán las siguientes: </p>
	<ul class="conEstilo">
		<li>Elaborar y mantener permanentemente actualizada la normativa interna y el manual, dejando constancia por escrito de las modificaciones, de la fecha de aprobación y de entrada en vigor. </li>
		<li>Difundir entre los miembros del despacho la información y la documentación necesaria en materia de prevención. </li>
		<li>Diseñar y ejecutar los planes anuales de formación. </li>
		<li>Detectar, analizar y comunicar, en su caso, al SEPBLAC, con criterios de seguridad, rapidez, eficacia y coordinación, todas aquellas operaciones de riesgo, anormales, inusuales en las que existan indicios o certeza de estar relacionadas con el blanqueo de capitales y la financiación del terrorismo, en adelante, “las operaciones”. </li>
		<li>Facilitar al SEPBLAC y al resto de autoridades (judiciales, policiales, administrativas) la información que requieran en el ejercicio de sus facultades, guardando el secreto profesional. </li>
		<li>Examinar con especial atención cualquier operación que por su cuantía o su naturaleza pueda estar particularmente relacionada con la financiación del terrorismo. </li>
		<li>Decidir sobre pertinencia de las comunicaciones que deben efectuarse al SEPBLAC sobre las operaciones respecto de las que existan indicios o certeza de que están relacionadas con la financiación de actividades terroristas. </li>
		<li>Recibir las comunicaciones de operaciones en las que existan indicios o certeza de estar relacionadas con los hechos antes descritos y proceder a su estudio y valoración. </li>
		<li>Promover las modificaciones del presente procedimiento interno, cuando lo estime necesario. </li>
		<li>Conservar con la máxima diligencia la documentación generada por cada incidencia que le sea reportada. </li>
		<li>Capacitar a los profesionales y al personal en las materias de prevención de blanqueo y financiación del terrorismo. </li>

	</ul>
	<p>Por su parte, el <u>representante</u> se encargará de:  </p>
	<ul class="conEstilo">
		<li>Comparecer en los eventuales procedimientos administrativos o judiciales relativos a estas materias. </li>
		<li>Efectuar las comunicaciones al SEPBLAC relativas a las operaciones en las que exista certeza o indicios de blanqueo de capitales o financiación del terrorismo. </li>
		<li>Convocar las reuniones del órgano de control interno. </li>
		<li>Mantener puntualmente informado al resto de los miembros del bufete y al personal que en él trabaja y a profesionales colaboradores, de cualquier circunstancia que pudiera alterar la política de prevención aquí recogida. </li>
	

	</ul>';

} else {
	$contenido.='<p><b>3.2. Funciones del representante ante el SEPBLAC.</b></p>
	<ul class="conEstilo">
		<li>Elaborar y mantener permanentemente actualizada la normativa interna y el manual, dejando constancia por escrito de las modificaciones, de la fecha de aprobación y de entrada en vigor. </li>
		<li>Difundir entre los miembros del despacho la información y la documentación necesaria en materia de prevención. </li>
		<li>Diseñar y ejecutar los planes anuales de formación. </li>
		<li>Detectar, analizar y comunicar, en su caso, al SEPBLAC, con criterios de seguridad, rapidez, eficacia y coordinación, todas aquellas operaciones de riesgo, anormales, inusuales en las que existan indicios o certeza de estar relacionadas con el blanqueo de capitales y la financiación del terrorismo, en adelante, “las operaciones”. </li>
		<li>Facilitar al SEPBLAC y al resto de autoridades (judiciales, policiales, administrativas) la información que requieran en el ejercicio de sus facultades, guardando el secreto profesional. </li>
		<li>Examinar con especial atención cualquier operación que por su cuantía o su naturaleza pueda estar particularmente relacionada con la financiación del terrorismo. </li>
		<li>Decidir sobre pertinencia de las comunicaciones que deben efectuarse al SEPBLAC sobre las operaciones respecto de las que existan indicios o certeza de que están relacionadas con la financiación de actividades terroristas. </li>
		<li>Recibir las comunicaciones de operaciones en las que existan indicios o certeza de estar relacionadas con los hechos antes descritos y proceder a su estudio y valoración. </li>
		<li>Promover las modificaciones del presente procedimiento interno, cuando lo estime necesario. </li>
		<li>Conservar con la máxima diligencia la documentación generada por cada incidencia que le sea reportada. </li>
		<li>Capacitar a los profesionales y al personal en las materias de prevención de blanqueo y financiación del terrorismo. </li>
		<li>Comparecer en los eventuales procedimientos administrativos o judiciales relativos a estas materias. </li>
		<li>Efectuar las comunicaciones al SEPBLAC relativas a las operaciones en las que exista certeza o indicios de blanqueo de capitales o financiación del terrorismo. </li>
		<li>Mantener puntualmente informado al resto de los miembros del bufete y al personal que en él trabaja y a profesionales colaboradores, de cualquier circunstancia que pudiera alterar la política de prevención aquí recogida. </li>
	

	</ul>';
}

if($datos['empleados']==50 || $datos['volumen']==3){
	$contenido.='<p><b>Reuniones, actas y acuerdos </b></p>
	<p>El órgano de control se reunirá siempre que las circunstancias así lo demanden y, al menos, con carácter anual. Los acuerdos de cada una de sus reuniones se recogerán en las correspondientes actas. Dichas actas, que formarán parte de la documentación del sistema de prevención de blanqueo de capitales y financiación del terrorismo de la entidad, describirán en el período de referencia: </p>
	<ul class="conEstilo">
		<li>Los asuntos que deben ser objeto de estudio por constituir actividades sujetas incluidas en el artículo 2 de la LPBC y el resultado del examen. </li>
		<li>Un resumen del análisis realizado de operaciones susceptibles de ofrecer indicios o certeza y de las comunicaciones realizadas al SEPBLAC, en su caso.</li>
	</ul>';

}

$contenido.='<p><b>4. MEDIDAS DE DILIGENCIA DEBIDA </b></p>
<p><b>4.1 Aplicables con carácter general </b></p>
<p>Todas las medidas que se detallan en los siguientes apartados se refieren a operaciones sujetas.<br/> 
El estudio del cliente se inicia con la comprobación de su identidad mediante documentos fehacientes, que permitan conocerle <u>antes de</u> realizar ninguna operación por su cuenta. En ningún caso se podrá asumir un  asunto sin haberse seguido previamente las normas de identificación que se establecen a continuación, sin perjuicio de lo previsto en el <b>artículo 12 de la LPBC*</b> y en el artículo 4 y siguientes del RLPBC. <br/>
El cliente mostrará al profesional que le atienda  su documento de identidad original, y el profesional hará una copia de dicho documento, que  se conservará  en el expediente. <br/>
Se recogerá la manifestación sobre la titularidad real de la operación (personas físicas con posesión o control, directo o indirecto, de más de un 25% del capital o de los derechos de voto de un cliente persona jurídica o que por otros medios ejerzan el control de su gestión). En caso de no existir tales personas, se considerará titulares reales a sus administradores.</p>
<page_footer>
<div id="articulo12" name="articulo12">
*Artículo 12 LPBC. Relaciones de negocio y operaciones no presenciales.1. Los sujetos obligados podrán establecer relaciones de negocio o ejecutar operaciones a través de medios telefónicos, electrónicos o telemáticos con clientes que no se encuentren físicamente presentes, siempre que concurra alguna de las siguientes circunstancias: a) La identidad del cliente quede acreditada de conformidad con lo dispuesto en la normativa aplicable sobre firma electrónica. b) El primer ingreso proceda de una cuenta a nombre del mismo cliente abierta en una entidad domiciliada en España, en la Unión Europea o en países terceros equivalentes. c) Se verifiquen los requisitos que se determinen reglamentariamente. En todo caso, en el plazo de un mes desde el establecimiento de la relación de negocio, los sujetos obligados deberán obtener de estos clientes una copia de los documentos necesarios para practicar la diligencia debida. Cuando se aprecien discrepancias entre los datos facilitados por el cliente y otra información accesible o en poder del sujeto obligado, será preceptivo proceder a la identificación presencial. Los sujetos obligados adoptarán medidas adicionales de diligencia debida cuando en el curso de la relación de negocio aprecien riesgos superiores al riesgo promedio.2. Los sujetos obligados establecerán políticas y procedimientos para afrontar los riesgos específicos asociados con las relaciones de negocio y operaciones no presenciales.
</div>
</page_footer>
</page>';
$contenido.="<page footer='page' backbottom='20mm' backleft='20mm' backright='20mm' backtop='20mm'>
<page_header>
	<div class='titleCabecera'>
		Documento generado por M&D Asesores
	</div>
</page_header>
<p>Se recabará del cliente con el objeto de verificar la información suministrada la documentación complementaria que, para cada caso, se indica: <br/>
</p>
";

$contenido.='<p><u>Clientes personas jurídicas: </u></p>
<ul class="sinMargen">
	<li>- Documentación fehaciente acreditativa de su denominación, forma jurídica, domicilio y objeto social (escrituras, consulta al Registro Mercantil y otros similares). </li>
	<li>- Poderes y documento de identidad de las personas que actúen en su nombre. </li>
</ul>
<p><u>Clientes personas físicas: </u></p>
<ul class="sinMargen">
	<li>- Documento Nacional de Identidad, tarjeta de residencia, tarjeta de identidad de extranjero, pasaporte o documento de identificación válido en el país de procedencia que incorpore fotografía de su titular. </li>
	<li>- Poderes y documento de identidad de las personas que actúen en su nombre.</li>
</ul>
<p>Asimismo, con carácter previo, se debe reclamar y obtener información de los clientes sobre el propósito e índole prevista de la relación de negocios. En particular, se recabará información a fin de conocer la naturaleza de su actividad profesional o empresarial, y adoptar medidas dirigidas a comprobar razonablemente la veracidad de dicha información. </p>
<p>Por último, se deberá realizar un seguimiento de la relación de negocios en el caso de que se trate de relaciones duraderas y no esporádicas, de forma periódica –cada tres años-.  En el caso de clientes de riesgo superior al promedio –aquellos a los que se les aplique medida reforzadas  y/o se les haya realizado examen especial si bien se haya decidido prestarle el servicio por considerar que no presenta indicios de blanqueo de capitales- con carácter anual. </p>
<p>Tales medidas consistirán en el establecimiento y aplicación de procedimientos de verificación de las actividades declaradas por los clientes, según el nivel de riesgo. </p>
<p>El profesional que preste alguno de los servicios sujetos deberá registrar todos estos datos en el correspondiente informe del cliente, para lo cual '.$cliente['razonSocial'].' pone a su disposición una herramienta informática a través de la cual se elaborarán estos informes, dejando constancia a través del mismo de las medidas de diligencia debida que el profesional a adoptado en función del nivel de riesgo que presente.</p>
<p>Las copias de toda la documentación relativa a la identificación del cliente, incluido el correspondiente informe  de cliente, deberán ser debidamente archivados  durante 10 años.</p>
<p>Con carácter general, se aplicarán las siguientes medidas de diligencia debida: </p>
<ul class="sinMargen">
	<li>Identificación formal en los términos establecidos en el presente apartado. </li>
	<li>Identificación del titular real. </li>
	<li>Información sobre el propósito e índole prevista de la relación de negocios. </li>
	<li>Información sobre el ejercicio por el cliente o sus familiares o allegados de funciones públicas importantes en el extranjero o en España, en cargos políticos o equivalentes, actualmente o en los dos años anteriores. </li>
</ul>
<p>Respecto de aquellos clientes y operaciones a los cuales son aplicables las medidas simplificadas previstas en la Ley se aplicarán una o varias de las siguientes medidas en función del riesgo: </p>
<ul class="conEstilo">
	<li>No se recabará información sobre la actividad empresarial o profesional del cliente, infiriéndose del tipo de operaciones o relación de negocios establecida. </li>
	<li>Se revisará la documentación aportada cada 3  años. </li>
	<li>Se realizará el seguimiento de la relación de negocios cada 3 años. </li>
</ul>
<p>Si en el proceso de admisión del cliente concurrieran indicios o certeza de blanqueo de capitales o de financiación del terrorismo, o de riesgos superiores al promedio, no podrán aplicarse estas medidas sino las que correspondan en aplicación del presente manual. </p>
<p><b>4.2. Política de admisión de clientes</b></p>
<p><u>No se admitirán como clientes: </u></p>
<ul>
	<li>- Aquellos que por las circunstancias que en ellos concurran no parezca que realicen actividades profesionales o empresariales o dispongan de medios compatibles con la operación que se propongan realizar, aquellos que no faciliten los datos que de ellos se soliciten a efectos de comprobar su identidad, la identidad del titular real, en su caso o los oculten o falseen, aquellos respecto de los cuales se compruebe que los datos por ellos suministrados no corresponden a la realidad y aquellos que, por provenir de jurisdicciones remotas imposibiliten el cumplimiento de las obligaciones que impone la Ley. </li>
	<li>- Aquellas personas jurídicas cuya estructura de propiedad o de control no haya podido determinarse.</li>
</ul>
<p><u>Serán considerados clientes con riesgo superior al promedio los que presenten una o varias de las siguientes características: </u></p>
<ul>
	<li>- Los que resulten del informe del cliente por apreciar el profesional que concurren en el mismo o en el servicio solicitado, circunstancias de riesgo establecidas por la LPBC y el RLPBC. </li>
	<li>- Las sociedades con acciones al portador.</li>
	<li>- Los nacionales o residentes en países, territorios o jurisdicciones de riesgo o que supongan transferencias de o hacia tales países, territorios o jurisdicciones, incluyendo los países para los que el Grupo de Acción Financiera Internacional (GAFI) exija la aplicación de medidas de diligencia reforzada. </li>
	<li>- Las personas con responsabilidad pública. </li>
	<li>- Las sociedades cuyo capital no sea suficiente para la realización de las actividades que proyecta, salvo que sus fuentes de financiación sean conocidas. </li>
	<li>- La transmisión de acciones o participaciones de sociedades pre-constituidas. </li>
</ul>
<p>En tales casos se emplearán una o varias de las siguientes medidas reforzadas de diligencia debida en función del riesgo: </p>
<ul class="sinMargen conEstilo">
	<li>Se revisará la documentación obtenida en el proceso de aceptación del cliente anualmente. </li>
	<li>Se obtendrá documentación o información adicional  a fin de conocer la naturaleza de la actividad profesional o empresarial del cliente. </li>
	<li>Se obtendrá documentación adicional a fin de determinar el origen del patrimonio y de los fondos con los que se llevará a cabo la operación. </li>
	<li>Se obtendrá autorización del  representante ante el SEPBLAC del bufete para admitir a ese cliente. </li>
	<li>Se exigirá que los pagos se realicen desde una cuenta a nombre del cliente abierta en una entidad de crédito domiciliada en la <b>Unión Europea</b> o en <a class="noAjax" href="../documentos/paises.pdf" target="_blank"><b>países terceros equivalentes</b></a> de acuerdo con la Resolución de 10 de agosto de 2012, de la Secretaría General del Tesoro y Política Financiera, por la que se publica el Acuerdo de 17 de julio de 2012, de la Comisión de Prevención del Blanqueo de Capitales e Infracciones Monetarias, por el que se determinan las jurisdicciones que establecen requisitos equivalentes a los de la legislación española de prevención del blanqueo de capitales y la financiación del terrorismo.  </li>
	<li>Se examinará y documentará la lógica económica de la operación. </li>
	<li>Se examinará y documentará la congruencia de la relación de negocios o de las operaciones con la documentación e información disponibles sobre el cliente. </li>
</ul>';
if($datos['empleados']==50 || $datos['volumen']==3){
	$contenido.='<p>El órgano de control adoptará la decisión definitiva en relación a la admisión o no de un determinado cliente. </p>';

} else {
	$contenido.='<p>El representante ante el SEPBLAC adoptará la decisión definitiva en relación a la admisión o no de un determinado cliente. </p>';
}

$contenido.="<p>En lo relativo a los clientes habituales del despacho: </p>
<ul class='conEstilo'>
	<li>Volverá a evaluarse qué medidas de diligencia debida le corresponden cuando se produzca una operación significativa por su volumen o complejidad. </li>
	<li>Antes del 29 de abril de 2015 se aplicarán las medidas de diligencia debida en todo caso a los clientes ya existentes en el despacho a la entrada en vigor de la LPBC –el 29/04/2015 se cumplen los cinco años de la entrada en vigor de la LPBC, por lo que se debe tener en cuenta este plazo a los efectos de lo dispuesto por la disposición transitoria séptima de la LPBC-. </li>
	<li>Se actualizará la base de datos de clientes en cuanto a titularidad real para la inclusión de los administradores como titulares reales de las personas jurídicas en los supuestos del artículo 8.b del RLPBC en el plazo máximo de dos años desde la entrada en vigor del Reglamento, esto es, antes del 6 de mayo del 2016. </li>
	<li>La aplicación de medidas de diligencia debida simplificada a clientes que a la entrada en vigor del Reglamento se beneficiaban del régimen de diligencia debida simplificada, deberá hacerse en función del riesgo y en un plazo máximo de tres años desde su entrada en vigor. </li>
</ul>
<p><b>5. CONSERVACIÓN DE LA DOCUMENTACIÓN</b></p>
<p>Habrán de conservarse los siguientes documentos: </p>
<ul class='conEstilo'>
	<li>Copia de los documentos exigibles en aplicación de las medidas de diligencia debida, durante un período mínimo de diez años desde la ejecución de la operación o terminación de la relación con el cliente. Las copias de los documentos fehacientes de identificación formal deberán almacenarse en soportes ópticos, magnéticos o electrónicos –a partir del 6 de mayo del 2014, esta forma de almacenamiento no es exigible a los bufetes que ocupen a menos de diez personas y cuyo volumen de negocios anual o balance general anual no supere los 2 millones de euros, que podrán optar por mantener copias físicas (salvo que el bufete se integre en un grupo empresarial que sí supere estas cifras)-.</li>
	<li>Original o copia de los documentos o registros que acrediten adecuadamente las operaciones y los intervinientes durante un período mínimo de diez años desde la ejecución de la operación. Asimismo, deberá mantenerse registro de todas las operaciones y relaciones de negocio –informes de clientes elaborados por el profesional que intervenga- durante el mismo período mínimo contado desde su ejecución o terminación de tal manera que puedan surtir efecto probatorio si fuera necesario. </li>
	<li>Documentos en que se formalice el cumplimiento de las obligaciones de comunicación y de control interno y de realización de examen especial. </li>
</ul>
<p>Las copias de la documentación se harán en el bufete teniendo a la vista los documentos originales.</p>
<p><b>6. DETECCIÓN DE OPERACIONES </b></p>
<p>Todo miembro del bufete tiene la obligación de examinar con especial atención cualquier operación sujeta, con independencia de su cuantía, que pudiera arrojar indicios de estar relacionada con el blanqueo de capitales o la financiación del terrorismo, trasladándolo al  órgano de control interno -o al representante ante el SEPBLAC en ausencia de dicho órgano- para que decida si procede su comunicación al SEPBLAC.</p>
<p>A estos efectos, se considerarán como marco de referencia las operaciones recogidas en el catalogo ejemplificativo de operaciones de riesgo de blanqueo de capitales para profesionales <b>(COR) aprobado por el SEPBLAC, que se anexa a este manual</b>.<br/> 
Quien hubiera detectado alguna de estas circunstancias lo pondrá, de inmediato, en conocimiento del órgano de control -o del representante ante el SEPBLAC en el supuesto que no sea preceptivo el nombramiento de órgano de control interno por parte del sujeto obligado-.<br/> 
Efectuada la comunicación referenciada al órgano competente, el comunicante quedará exento de responsabilidad. Cualquiera que sea el criterio adoptado por el órgano de control o por el representante ante el SEPBLAC, con respecto a las comunicaciones realizadas, se informará al comunicante del curso que se le dé. </p><br/><br/><br/>
<p><b>7. ANÁLISIS DE LAS OPERACIONES	</b></p>
<p>El  representante ante el SEPBLAC o el órgano de control llevará a cabo las gestiones adicionales de investigación sobre las operaciones detectadas con la máxima profundidad y rapidez posible, mediante la obtención de toda la información y documentación disponibles, y la investigación global de la operativa de los clientes, contemplando la posible relación con otros clientes o sectores de actividad. <br/>
A la vista de toda la documentación recabada, el órgano de control decidirá sobre la procedencia de su comunicación al SEPBLAC. En caso afirmativo, la operación será comunicada, junto con la documentación que soporte las gestiones realizadas. <br/>
Se utilizará para ello el formulario o medio de comunicación electrónica previsto en cada caso por el SEPBLAC. <br/>
De los análisis de operaciones de riesgo (anormales, inusuales o potencialmente constitutivas de indicio o certeza), de las deliberaciones habidas, así como de las comunicadas al SEPBLAC, se guardará constancia. En especial, dichos registros harán referencia a cada operación estudiada, cliente, identificación, motivo de la alerta, ampliación de datos efectuada si resultara preciso, decisión adoptada de remisión o de archivo y motivo, así como cualquier otro dato o antecedente que, a la vista de la operación concreta, se mostrare relevante para su evaluación. 
</p>
<p><b>8. COMUNICACIÓN AL SEPBLAC DE LAS OPERACIONES</b></p>
<p>Las comunicaciones del representante al SEPBLAC se efectuarán de inmediato, en cuanto haya seguridad o indicio razonable de que las operaciones analizadas están relacionadas con el blanqueo de capitales o la financiación del terrorismo. <br/>
Para las comunicaciones se utilizará el formulario o medio de comunicación electrónico previsto en cada caso por el SEPBLAC. 
En las comunicaciones, habrá de informarse de: </p>
<ul class='sinMargen'>
	<li>- Relación e identificación de las personas físicas o jurídicas que participan en la operación y concepto de su participación en ella. </li>
	<li>- Actividad conocida de las personas físicas o jurídicas que participan en la operación y correspondencia entre la actividad y la operación.</li>
	<li>- Gestiones realizadas por el sujeto obligado comunicante para investigar la operación comunicada. </li>
 	<li>- Exposición de las circunstancias de las que pueda inferirse el indicio o certeza de relación con el blanqueo de capitales o con la financiación del terrorismo o que pongan de manifiesto la falta de justificación económica, profesional o de negocio para la realización de la operación. </li>
	<li>- Cualesquiera otros datos relevantes para la prevención del blanqueo de capitales o la financiación del terrorismo que se estimen necesarios o convenientes </li>
</ul>
<p>Los profesionales del bufete están enterados de la prohibición absoluta de revelar al cliente ni a terceros que se ha comunicado información al SEPBLAC o que se está examinando o puede examinarse alguna operación. </p>
<p><b>9. COLABORACIÓN CON LA COMISIÓN DE PREVENCIÓN DE BLANQUEO DE CAPITALES E INFRACCIONES MONETARIAS</b></p>
<p>Con independencia de la comunicación individual de operaciones sospechosas recogida en el apartado anterior, el bufete colaborará con dicha Comisión o sus órganos de apoyo, facilitando conforme a la normativa legal vigente en cada momento la documentación e información que le requiera en el ejercicio de sus competencias, sobre si mantienen o han mantenido a lo largo de los diez años anteriores relaciones de negocios con determinadas personas físicas o jurídicas y sobre la naturaleza de dichas relaciones, guardando el secreto profesional.<br/> 
El representante ante el SEPBLAC será el responsable de: </p>
<ul class='sinMargen'>
	<li>- Recibir los requerimientos. </li>
	<li>- Ejecutar las acciones necesarias de investigación interna dentro del bufete, para dar respuesta a los requerimientos habidos, siempre dentro de los plazos indicados. </li>
	<li>- Hacer llegar al Servicio la respuesta, conteniendo los datos requeridos. </li>
</ul>
<p><b>10. FORMACIÓN DEL PERSONAL</b></p>
<p>El bufete tiene establecidas medidas para lograr el perfeccionamiento en la tarea de prevención de blanqueo de capitales y financiación del terrorismo. </p>";
if($datos['empleados']>10 || $datos['volumen']>1){
	$contenido.='<p>Estas medidas incluyen la participación anual de directivos, empleados y colaboradores, en cursos específicos de formación orientados a detectar las operaciones que puedan estar relacionadas con el blanqueo de capitales o la financiación del terrorismo para instruirles sobre la forma de proceder en tales casos. </p>';

}
if($datos['empleados']==10 && $datos['volumen']==1){
	$contenido.='<p>Estas medidas incluyen la participación del representante ante el SEPBLAC en cursos específicos de formación orientados a detectar las operaciones que puedan estar relacionadas con el blanqueo de capitales o la financiación del terrorismo e instruirle sobre la forma de proceder en tales casos, quien transmitirá a los restantes profesionales y al personal que intervenga en los servicios sujetos sobre la forma de proceder para prevenir que el despacho pueda intervenir en actividades que presenten indicios de blanqueo de capitales. </p>';

}
$contenido.='<p>Los cursos podrán ser  impartidos on line, por empresas dedicadas a prestar soporte y asesoramiento en prevención del blanqueo de capitales, guardándose en cualquier caso el certificado de la actividad formativa llevada a cabo y firma de los profesionales que la hayan realizado.</p>';

if($datos['sujeto']=='JURIDICA' && ($datos['empleados']>10 || $datos['volumen']==2)){
	$contenido.='<p><b>11. EXAMEN EXTERNO </b></p>
	<p>Las medidas de control interno establecidas serán objeto de examen anual por un experto externo. El examen anual podrá ser sustituido en los dos años sucesivos por un informe de seguimiento. <br/>
Los resultados del examen serán consignados en un informe escrito que describirá las medidas de control interno existentes, valorará su eficacia operativa y propondrá, en su caso, eventuales rectificaciones o mejoras. <br/>
Se adoptarán sin dilación, en su caso, las medidas necesarias para solucionar las deficiencias identificadas. Aquellas deficiencias que no puedan ser subsanadas inmediatamente serán objeto de un plan de remedio elaborado por el órgano de administración, con un calendario preciso para la implantación de medidas correctoras que no podrá exceder de un año natural. El informe estará en todo caso a disposición de la Comisión o de sus órganos durante los cinco años siguientes a la fecha de emisión.
</p>
<p><b>12. REVISIÓN INTERNA </b></p>
	<p>Sin perjuicio de la revisión efectuada por el experto externo, el órgano de administración efectuará una revisión interna, con carácter anual, de todos los procedimientos aprobados por el bufete en materia de prevención del blanqueo de capitales y la financiación del terrorismo. <br/>
Cualquier irregularidad que se detecte se pondrá en conocimiento del órgano de control, que se encargará de subsanarla lo antes posible.

</p>';

}
$contenido.="
</page>
<page footer='page' backbottom='20mm' backleft='20mm' backright='20mm' backtop='20mm'>
<page_header>
	<div class='titleCabecera'>
		Documento generado por M&D Asesores
	</div>
</page_header>
<br/><br/><h2>ANEXO<br/>
<a href='../documentos/anexo_manual_PBC.pdf'>CATÁLOGO EJEMPLIFICATIVO DE OPERACIONES DE RIESGO DE BLANQUEO DE CAPITALES PARA PROFESIONALES ELABORADO POR EL SEPBLAC</a></h2>
<p>En ".convertirMinuscula($cliente['localidad']).", a ".formateaFechaWeb($datos['fechaInicio'])."</p>
<p>Firmado por:<br/><br/><br/><br/><br/><br/><br/><br/></p>";
if($datos['sujeto']=='JURIDICA'){
$contenido.="<p>".$datos['representante']."<br/>Representante de ".$cliente['razonSocial']."</p>";
} else {
$contenido.="<p>".$cliente['razonSocial']."</p>";
}
$contenido.="

<page_footer>
<div class='textoPie'>

</div>
</page_footer>	
</page>";

return $contenido;
}

function generaLOPD(){
	require_once '../../api/phpword/PHPWord.php';
	$PHPWord = new PHPWord();
	$document = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_ds.docx');
	conexionBD();
    	$datos = datosRegistro("trabajos",$_GET['codigo']);
    	$cliente = datosRegistro('clientes',$datos['codigoCliente']);
    	$formulario = recogerFormularioServicios($datos);
	cierraBD();
	$document->setValue("cliente",utf8_decode($formulario['pregunta3']));
	$document->setValue("fechaHoy",utf8_decode(fecha()));
	$document->setValue("fecha",utf8_decode($formulario['pregunta2']));
	$document->setValue("cif",utf8_decode($formulario['pregunta9']));
	$document->setValue("direccion",utf8_decode($formulario['pregunta4']));
	$document->setValue("direccion2",utf8_decode('CP: '.$formulario['pregunta10'].' '.$formulario['pregunta5'].' ('.convertirMinuscula($formulario['pregunta11']).')'));
	$document->setValue("pais",utf8_decode('ESPAÑA'));
	$document->setValue("poblacion",utf8_decode($formulario['pregunta5']));
	$document->setValue("provincia",convertirMinuscula($formulario['pregunta11']));
	$document->setValue("responsable",utf8_decode($formulario['pregunta15']));
	$document->setValue("nifResponsable",utf8_decode($formulario['pregunta17']));
	$document->setValue("representante",utf8_decode($formulario['pregunta8']));
	$document->setValue("nifRepresentante",utf8_decode($formulario['pregunta14']));
	$document->save('../documentos/consultorias/Documento_seguridad.docx');
}
?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de servicios

function operacionesServicios(){
	$res=true;

	if(isset($_POST['codigoCliente'])){
		$res=actualizaServicios();
	}
	/*elseif(isset($_POST['codigo'])){
		$res=actualizar();
	}
	elseif(isset($_POST['sujeto'])){
		$res=insertar();
	}*/
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('servicios');
	}

	mensajeResultado('codigoCliente',$res,'Toma de datos');
    mensajeResultado('elimina',$res,'Toma de datos', true);
}

function actualizaServicios(){
	$res=true;
	transformaValoresPOST();
	if(isset($_POST['codigo'])){
		$res=actualizar();
	}
	return $res;
}


function insertar(){
	$res=true;
	$res=insertaDatos($_POST['tabla'],'','documentos');
	$_POST['codigo']=mysql_insert_id();
	$res=completar($_POST['tabla'],$_POST['codigo']);
	return $res;
}

/*function actualizar(){
	$res=true;
	$res=actualizaDatos($_POST['tabla'],'','documentos');
	$res=completar($_POST['tabla'],$_POST['codigo']);
	return $res;
}*/

function actualizar(){
	$res=true;
	$i=1;
	while(isset($_POST['tabla'.$i])){
		if($_POST['tabla'.$i]=='manuales_pbc'){
			comprobarModificacion($_POST['tabla'.$i]);
			$res=actualizaDatos($_POST['tabla'.$i],'','documentos');
		}
		$res=completar($_POST['tabla'.$i],$_POST['codigo'],$i);
		$i++;
	}
	return $res;
}

function completar($tabla,$codigo,$i){
	$res=true;
	if($tabla=='manuales_pbc'){
		$res=insertaPersonas($codigo);
	} if ($tabla=='lopd1'){
		$res=insertaLOPD($i);
	}
	$res=actualizaCliente($tabla,$_POST['codigoCliente'],$i);
	return $res;
}

function actualizaCliente($tabla,$codigoCliente,$i){
	$res=true;
	if($tabla=='manuales_pbc'){
		/*$res=consultaBD('UPDATE clientes SET actividad="'.$_POST['actividad'].'", servicios="'.$_POST['servicios'].'",empleados="'.$_POST['empleados'].'",volumen="'.$_POST['volumen'].'",tomo="'.$_POST['tomo'].'",libro="'.$_POST['libro'].'",folio="'.$_POST['folio'].'",seccion="'.$_POST['seccion'].'",hoja="'.$_POST['hoja'].'" WHERE codigo='.$codigoCliente,true);*/
	} if ($tabla=='lopd1'){
		/*$res=consultaBD('UPDATE clientes SET razonSocial="'.$_POST['pregunta'.$i.'3'].'", domicilio="'.$_POST['pregunta'.$i.'4'].'",localidad="'.$_POST['pregunta'.$i.'5'].'",telefono="'.$_POST['pregunta'.$i.'6'].'",email="'.$_POST['pregunta'.$i.'7'].'",administrador="'.$_POST['pregunta'.$i.'8'].'",cif="'.$_POST['pregunta'.$i.'9'].'",cp="'.$_POST['pregunta'.$i.'10'].'",fax="'.$_POST['pregunta'.$i.'12'].'",provincia="'.$_POST['pregunta'.$i.'11'].'",nifAdministrador="'.$_POST['pregunta'.$i.'14'].'" WHERE codigo='.$codigoCliente,true);*/
	}
	return $res;
}

function insertaLOPD($i){
	$datos=arrayFormulario();
	$trabajo=datosRegistro('trabajos',$datos['trabajo'.$i]);
	$preguntas=recogerFormularioServicios($trabajo);
	//$datos=compruebaCampoCheck($datos,$i);
	$query='';
	$j=1;
	while(isset($preguntas['pregunta'.$j])){
		if($j>1){
			$query.="&{}&";
		}
		if(isset($datos['pregunta'.$i.$j])){
			if(is_array($datos['pregunta'.$i.$j])){
				$select = $datos['pregunta'.$i.$j];
				$text ='';
				if(!empty($select)){
					for($k=0;$k<count($select);$k++){
						if($k > 0){
							$text .= "&$&";
						}
						$text .= $select[$k];
					}
				}
				$datos['pregunta'.$i.$j]=$text;
			}
		} else {
			$datos['pregunta'.$i.$j]=$preguntas['pregunta'.$j];
		}
		$query.="pregunta$j=>".$datos['pregunta'.$i.$j];
		$j++;
	}
	//echo "UPDATE trabajos SET formulario = '".$query."' WHERE codigo=".$datos['trabajo'.$i];
	$res = consultaBD("UPDATE trabajos SET formulario = '".$query."' WHERE codigo=".$datos['trabajo'.$i],true);
	$res = $res && insertaUsuariosLOPD($datos['trabajo'.$i]);
	$res = $res && insertaAplicacionesLOPD($datos['trabajo'.$i]);
	$res = $res && insertaSoportesLOPD($datos['trabajo'.$i]);
	$res = $res && insertaOtrosFicherosLOPD($datos['trabajo'.$i]);
	$res = $res && insertaOtrosEncargadosLOPD($datos['trabajo'.$i]);
	$res = $res && insertaSoportesAutomatizadosLOPD($datos['trabajo'.$i]);
	$res = $res && insertaDelegacionesLOPD($datos['trabajo'.$i]);
	$res = $res && insertaCesionesLOPD($datos['trabajo'.$i]);
	$res = $res && insertaConcurrenLOPD($datos['trabajo'.$i]);
	//$res = $res && insertaCesionesLOPD($datos['trabajo'.$i]);
	return $res;
}

function insertaAplicacionesLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM aplicaciones_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreAplicacionLOPD'.$i])){
		if($datos['nombreAplicacionLOPD'.$i] != ''){
		$res = $res && consultaBD("INSERT INTO aplicaciones_lopd VALUES (NULL,'".$codigo."','".$datos['nombreAplicacionLOPD'.$i]."','".$datos['finalidadAplicacionLOPD'.$i]."','".$datos['empleadosAplicacionLOPD'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaSoportesLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM soportes_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreSoporteLOPD_uno'.$i])){
		if($datos['nombreSoporteLOPD_uno'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO soportes_lopd VALUES (NULL,'".$codigo."','".$datos['nombreSoporteLOPD_uno'.$i]."','".$datos['marcaSoporteLOPD_uno'.$i]."','".$datos['serieSoporteLOPD_uno'.$i]."','".$datos['fechaSoporteLOPD_uno'.$i]."','".$datos['ubicacionSoporteLOPD_uno'.$i]."','".$datos['sistemaSoporteLOPD_uno'.$i]."','".$datos['aplicacionesSoporteLOPD_uno'.$i]."','".$datos['conexionesSoporteLOPD_uno'.$i]."','".$datos['responsableSoporteLOPD_uno'.$i]."','".$datos['otrosSoporteLOPD_uno'.$i]."');");
		}
		if($datos['nombreSoporteLOPD_dos'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO soportes_lopd VALUES (NULL,'".$codigo."','".$datos['nombreSoporteLOPD_dos'.$i]."','".$datos['marcaSoporteLOPD_dos'.$i]."','".$datos['serieSoporteLOPD_dos'.$i]."','".$datos['fechaSoporteLOPD_dos'.$i]."','".$datos['ubicacionSoporteLOPD_dos'.$i]."','".$datos['sistemaSoporteLOPD_dos'.$i]."','".$datos['aplicacionesSoporteLOPD_dos'.$i]."','".$datos['conexionesSoporteLOPD_dos'.$i]."','".$datos['responsableSoporteLOPD_dos'.$i]."','".$datos['otrosSoporteLOPD_dos'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaUsuariosLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$consulta="SELECT * FROM usuarios_lopd WHERE codigoTrabajo=$codigo";
	$campos=array('nombreUsuarioLOPD','nifUsuarioLOPD','datosUsuarioLOPD','ficherosUsuarioLOPD','soportesUsuarioLOPD','accesoUsuarioLOPD','fechaAltaUsuarioLOPD','fechaBajaUsuarioLOPD');
	comprobarModificacionListado($consulta,$campos,'usuarios_lopd');
	$res = $res && consultaBD("DELETE FROM usuarios_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreUsuarioLOPD'.$i])){
		if($datos['nombreUsuarioLOPD'.$i] != ''){
			$select = $datos['ficherosUsuarioLOPD'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['ficherosUsuarioLOPD'.$i] = $text;

		$res = $res && consultaBD("INSERT INTO usuarios_lopd VALUES (NULL,'".$codigo."','".$datos['nombreUsuarioLOPD'.$i]."',
			'".$datos['nifUsuarioLOPD'.$i]."','".$datos['datosUsuarioLOPD'.$i]."','".$datos['ficherosUsuarioLOPD'.$i]."','".$datos['soportesUsuarioLOPD'.$i]."','".$datos['accesoUsuarioLOPD'.$i]."','".$datos['fechaAltaUsuarioLOPD'.$i]."','".$datos['fechaBajaUsuarioLOPD'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaOtrosEncargadosLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM otros_encargados_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['servicioEncargadoLOPD_'.$i])){
		if($datos['servicioEncargadoLOPD_'.$i] != ''){
			$select = $datos['ficheroEncargadoLOPD_'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['ficheroEncargadoLOPD_'.$i] = $text;
			$res = $res && consultaBD("INSERT INTO otros_encargados_lopd VALUES (NULL,'".$codigo."','".$datos['servicioEncargadoLOPD_'.$i]."','".$datos['nombreEncargadoLOPD_'.$i]."','".$datos['cifEncargadoLOPD_'.$i]."','".$datos['direccionEncargadoLOPD_'.$i]."','".$datos['emailEncargadoLOPD_'.$i]."','".$datos['localidadEncargadoLOPD_'.$i]."','".$datos['cpEncargadoLOPD_'.$i]."','".$datos['telefonoEncargadoLOPD_'.$i]."','".$datos['contactoEncargadoLOPD_'.$i]."','".$datos['representanteEncargadoLOPD_'.$i]."','".$datos['dondeEncargadoLOPD_'.$i]."','".$datos['subcontratacionEncargadoLOPD_'.$i]."','".$datos['fechaAltaEncargadoLOPD_'.$i]."','".$datos['fechaBajaEncargadoLOPD_'.$i]."','".$datos['ficheroEncargadoLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaSoportesAutomatizadosLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM soportes_automatizados_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreAutomatizadosLOPD_'.$i])){
		if($datos['nombreAutomatizadosLOPD_'.$i] != ''){
			$select = $datos['soportesAutomatizadosLOPD_'.$i];
			$text ='';
			if(!empty($select)){
				for($j=0;$j<count($select);$j++){
					if($j > 0){
						$text .= "&$&";
					}
					$text .= $select[$j];
				}
			}
			$datos['soportesAutomatizadosLOPD_'.$i] = $text;
			$res = $res && consultaBD("INSERT INTO soportes_automatizados_lopd VALUES (NULL,'".$codigo."','".$datos['nombreAutomatizadosLOPD_'.$i]."','".$datos['cargoAutomatizadosLOPD_'.$i]."','".$datos['accesosAutomatizadosLOPD_'.$i]."','".$datos['nifAutomatizadosLOPD_'.$i]."','".$datos['soportesAutomatizadosLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaCesionesLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM cesiones_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['tipoCesionesLOPD_'.$i])){
		if($datos['tipoCesionesLOPD_'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO cesiones_lopd VALUES (NULL,'".$codigo."','".$datos['tipoCesionesLOPD_'.$i]."','".$datos['quienCesionesLOPD_'.$i]."','".$datos['objetoCesionesLOPD_'.$i]."','".$datos['finalidadCesionesLOPD_'.$i]."','".$datos['ficheroCesionesLOPD_'.$i]."','".$datos['fechaCesionesLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaDelegacionesLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM delegaciones_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['usuario1DelegacionesLOPD_'.$i])){
		if($datos['usuario1DelegacionesLOPD_'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO delegaciones_lopd VALUES (NULL,'".$codigo."','".$datos['usuario1DelegacionesLOPD_'.$i]."','".$datos['usuario2DelegacionesLOPD_'.$i]."','".$datos['fechaDelegacionesLOPD_'.$i]."','".$datos['fechaFinDelegacionesLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function insertaConcurrenLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM concurren_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['nombreConcurrenLOPD_'.$i])){
		if($datos['nombreConcurrenLOPD_'.$i] != ''){
			$res = $res && consultaBD("INSERT INTO concurren_lopd VALUES (NULL,'".$codigo."','".$datos['nombreConcurrenLOPD_'.$i]."','".$datos['nifConcurrenLOPD_'.$i]."','".$datos['actividadConcurrenLOPD_'.$i]."','".$datos['fechaConcurrenLOPD_'.$i]."','".$datos['fechaFinConcurrenLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}


function insertaOtrosFicherosLOPD($codigo){
	$res=true;
	$i=0;

	$datos=arrayFormulario();

	$res = $res && consultaBD("DELETE FROM otros_ficheros_lopd WHERE codigoTrabajo='".$codigo."'");
	// Sino se insertan los nuevos registros
	while(isset($datos['otroFicheroLOPD_'.$i])){
		if($datos['otroFicheroLOPD_'.$i] == 'SI'){
		$res = $res && consultaBD("INSERT INTO otros_ficheros_lopd VALUES (NULL,'".$codigo."','".$datos['nombreFicheroLOPD_'.$i]."','".$datos['finalidadFicheroLOPD_'.$i]."','".$datos['origenFicheroLOPD_'.$i]."','".$datos['colectivoFicheroLOPD_'.$i]."','".$datos['datosFicheroLOPD_'.$i]."','".$datos['sistemaFicheroLOPD_'.$i]."','".$datos['nivelFicheroLOPD_'.$i]."','".$datos['fechaAltaFicheroLOPD_'.$i]."','".$datos['fechaBajaFicheroLOPD_'.$i]."');");
		}

		$i++;
	}

	return $res;
}

function compruebaCampoCheck($datos,$i){

	for($j=1;$j<40;$j++){

		if(!isset($datos['pregunta'.$i.$j])){
			$datos['pregunta'.$i.$j]='NO';
		}
	}

	return $datos;

}

function insertaPersonas($codigo){
	$res=true;
	$datos=arrayFormulario();
	conexionBD();
	$consulta="SELECT * FROM personas_pbc WHERE codigoManual=$codigo";
	$campos=array('nombre');
	comprobarModificacionListado($consulta,$campos,'personas_pbc');
	$res=consultaBD("DELETE FROM personas_pbc WHERE codigoManual=$codigo");

	for($i=0;isset($datos['nombre'.$i]);$i++){
		if($datos['nombre'.$i]!=''){
			$res=$res && consultaBD("INSERT INTO personas_pbc VALUES(NULL,".$codigo.",'".$datos['nombre'.$i]."');");
		}
	}

	cierraBD();

	return $res;
}


function listadoTrabajos(){
	$columnas=array('fecha','servicio','fecha','fecha');
	
	$having=obtieneWhereListado("HAVING 1=1",$columnas);
	$orden=obtieneOrdenListado($columnas);//Uso de función personalizada
	$limite=obtieneLimitesListado();
	
	if($_SESSION['espacio']=='NO'){
		$cliente=datosRegistro('usuarios_clientes',$_SESSION['codigoU'],'codigoUsuario');
		$having .= ' AND trabajos.codigoCliente = '.$cliente['codigoCliente'];
	} else {
		$having .= ' AND trabajos.codigoCliente = '.$_SESSION['espacio'];
	}
	
	conexionBD();
	$consulta=consultaBD("SELECT trabajos.codigo,fecha,servicio,trabajos.codigoCliente,servicios_familias.referencia, trabajos.codigoServicio, servicios.referencia AS referenciaServicio FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT trabajos.codigo,fecha,servicio,trabajos.codigoCliente,servicios_familias.referencia, trabajos.codigoServicio, servicios.referencia AS referenciaServicio FROM trabajos INNER JOIN servicios ON trabajos.codigoServicio=servicios.codigo INNER JOIN servicios_familias ON servicios.codigoFamilia=servicios_familias.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$manual = datosRegistro("manuales_pbc",$datos['codigo'],'codigoTrabajo');
		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['servicio'],
			crearBoton($datos,$manual),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function crearBoton($datos,$manual){
	$nombres=array();
	$direcciones=array();
	$iconos=array();
	$target=array();
	array_push($nombres, 'Toma de datos');
	array_push($direcciones, "consultoria/gestion.php?codigo=".$datos['codigo']."&codigoCliente=".$datos['codigoCliente']);
	array_push($iconos, 'icon-edit');
	array_push($target, 0);
	$conLOPD=array(5,7,10,11,13);
	if($datos['referencia']=='PBC1'){
		array_push($nombres, 'Descargar Manual de PBC');
		//cuando no hay y documentacion en ADMIN
		array_push($direcciones, 'zona-cliente/generaDocumento.php?ref=pbc&codigo='.$datos['codigo']);
		array_push($iconos, 'icon-download');
		array_push($target, 1);
	}
	if($datos['referencia']=='COMPL'){
		array_push($nombres, 'Inactivo');
		//cuando no hay y documentacion en ADMIN
		array_push($direcciones, 'zona-cliente/generaDocumento.php?ref=compl&codigo='.$datos['codigo']);
		array_push($iconos, 'icon-download');
		array_push($target, 1);
	}
	if($datos['referencia']=='LOPD1' || in_array($datos['referenciaServicio'], $conLOPD)){
		array_push($nombres, 'Descargar documentación LOPD');
		//cuando no hay y documentacion en ADMIN
		array_push($direcciones, 'zona-cliente/generaDocumento.php?ref=lopd&codigo='.$datos['codigo']);
		array_push($iconos, 'icon-download');
		array_push($target, 1);
	}

	$boton=botonAcciones($nombres,$direcciones,$iconos,$target);

	$nuevoBoton="<li class='divider'></li>
				<li><a onclick='abreVentana(".$manual['codigo'].");'><i class='icon-files-o'></i><span>Versiones</span> </a></li></ul>";
	//$boton=str_replace('</ul>', $nuevoBoton, $boton);

	return $boton;
	
}

//Esta función utiliza un "orden natural" (primero por longitud, luego por el propio valor) para la primera columna del listado de servicios (que es mayoritariamente numérica)
function obtieneOrdenListadoServicios($columnas){
	$orden = '';
    if(isset($_GET['iSortCol_0'])) {
        $orden = 'ORDER BY  ';
        for ($i=0;$i<(int)$_GET['iSortingCols'];$i++) {
            if($_GET['bSortable_'.(int)$_GET['iSortCol_'.$i]]=='true'){
                $indice=(int)$_GET['iSortCol_'.$i];

                if($indice==0 && $_GET['sSortDir_'.$i]==='asc'){//Si se trata de la primera columna y el orden es ascendente...
                	$orden.="LENGTH(referencia), referencia, ";//.. ordeno por longitud y luego valor
                }
                elseif($indice==0 && $_GET['sSortDir_'.$i]!=='asc'){//Lo mismo si el orden es descendente
                	$orden.="LENGTH(referencia) DESC, referencia DESC, ";
                }
                else{//Orden normal
                	$orden.=$columnas[$indice].' '.($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .', ';
                }
            }
        }

        $orden = substr_replace($orden, '', -2);
        if ($orden == 'ORDER BY') {
            $orden = '';
        }
    }
    return $orden;
}

function manualPBC(){
	global $_CONFIG;


	abreVentanaGestion('Gestión de Manuales PBC','?','','icon-edit','',true,'noAjax');
	$datos=compruebaItem('manuales_pbc');
	
	abreColumnaCampos();
	campoOculto('manuales_pbc','tabla');
	campoFecha('fechaInicio','Fecha inicio',$datos);
	campoSelect('sujeto','Sujeto',array('Persona física','Persona jurídica'),array('FISICA','JURIDICA'),$datos);
	campoSelect('actividad','Actividad',array('Abogacía','Asesoramiento fiscal','Asesoramiento fiscal y contable','Contabilidad externa','Auditoría de cuentas','Promoción inmobiliaria','Otra'),array('ABOGACIA','FISCAL','CONTABLE','CONTABILIDAD','AUDITORIA','PROMOCION','OTRA'),$datos);
	campoTexto('otra','Otra actividad',$datos);
	areaTexto('servicios','Servicios habituales',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha('fechaFin','Fecha fin',$datos);
	campoSelect('empleados','Número de empleados',array('Menos de 10 personas','Entre 10 y 49 personas','50 personas o más'),array(10,49,50),$datos);
	campoSelect('volumen','Volumen de negocios anual',array('No supera los 2 millones de euros','Supera los 2 millones de euros sin superar los 10 millones de euros','Supera los 10 millones de euros'),array(1,2,3),$datos);
	campoFichero('ficheroCatalogo','Catálogo',0,$datos,$_CONFIG['raiz'].'zona-cliente/documentos/','Descargar');
	cierraColumnaCampos();

	echo "<br clear='all'><h1 class='apartadoFormulario'>Registro mercantil</h1>";
	abreColumnaCampos();
	campoTexto('tomo','Tomo',$datos);
	campoTexto('folio','Folio',$datos);
	campoTexto('hoja','Hoja',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
	campoTexto('libro','Libro',$datos);
	campoTexto('seccion','Sección',$datos);
	cierraColumnaCampos();

	echo "<br clear='all'><h1 class='apartadoFormulario'>Representante ante el SEPBLAC</h1>";
	abreColumnaCampos();
		campoTexto('representante','Representante',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('nifRepresentante','NIF',$datos);
	cierraColumnaCampos();

	echo "<br clear='all'><h1 class='apartadoFormulario'>Organo de control interno</h1>";
	creaTablaPersonas($datos);

	cierraVentanaGestionPerfil('CLIENTE','index.php',true);
}

function creaTablaPersonas($datos){
	conexionBD();

	echo "	                 
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple mitadAncho' id='tablaPersonas'>
				  	<thead>
				    	<tr>
				            <th> Personas </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM personas_pbc WHERE codigoManual=".$datos['codigo']);
				  			while($persona=mysql_fetch_assoc($consulta)){
				  				echo '<tr>';
				  				campoTextoTabla('nombre'.$i,$persona['nombre'],'span11');
				  				echo '</tr>';
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			echo '<tr>';
				  			campoTextoTabla('nombre'.$i,'','span11');
				  			echo '</tr>';
				  		}

	echo "				  		
				  	</tbody>
				</table>
				<br/>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaPersonas\");'><i class='icon-plus'></i> Añadir persona</button> 
				</div>
			</div>";

	cierraBD();
}

function compruebaItem($tabla){
	if(isset($_REQUEST['codigo'])){//OJO: con $_REQUEST porque puede venir desde el listado ($_GET) o desde el alta
		$datos=datosRegistro($tabla,$_REQUEST['codigo'],'codigoTrabajo');
		if($datos){
			campoOculto($datos);
			campoOculto($datos,'codigoTrabajo');
		} else {
			campoOculto($_REQUEST['codigo'],'codigoTrabajo');
		}
	}
	else{
		$datos=false;
	}
	return $datos;
}



function filtroServicios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Referencia');
	campoTexto(1,'Servicio','','span3');
	campoTextoSimbolo(2,'Precio','€');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(3,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function recogeVersiones(){
	$datos=arrayFormulario();
	$trabajo=consultaBD('SELECT trabajos.codigo, trabajos.codigoCliente FROM trabajos INNER JOIN manuales_pbc ON trabajos.codigo=manuales_pbc.codigoTrabajo WHERE manuales_pbc.codigo='.$datos['codigo'],true,true);
	$versiones=consultaBD('SELECT trabajos_versiones.*, usuarios.tipo, CONCAT(nombre," ",apellidos) AS nombre FROM trabajos_versiones LEFT JOIN usuarios ON trabajos_versiones.codigoUsuario=usuarios.codigo WHERE codigoTrabajo='.$datos['codigo'].' AND codigoHistorico IS NULL ORDER BY fecha;',true);
	$res=array();
	$res['texto']='<ul>';
	while($version=mysql_fetch_assoc($versiones)){
		$usuario=$version['tipo']=='CLIENTE'?'Cliente':'Administrador ('.$version['nombre'].')';
		$res['texto'].='<li><a target="_blank" href="../consultoria/gestion.php?codigo='.$trabajo['codigo']."&codigoCliente=".$trabajo['codigoCliente'].'&codigoVersion='.$version['codigo'].'">'.formateaFechaWeb($version['fecha']).' - '.$usuario.'</a></li>';
	}	
	$res['texto'].='</ul>';
	echo json_encode($res);
}

//Fin parte de servicios
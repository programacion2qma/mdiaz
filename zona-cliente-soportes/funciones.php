<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de agrupaciones

function operacionesSoportes(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaSoporte();
	}
	elseif(isset($_POST['nombreSoporteLOPD'])){
		$res=insertaSoporte();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('soportes_lopd_nueva');
	}

	mensajeResultado('nombreSoporteLOPD',$res,'Soporte');
    mensajeResultado('elimina',$res,'Soporte', true);
}

function insertaSoporte(){
	$res=true;
	$res=insertaDatos('soportes_lopd_nueva');
	return $res;
}

function actualizaSoporte(){
	$res=true;
	$res=actualizaDatos('soportes_lopd_nueva');
	return $res;
}


function listadoSoportes(){
	global $_CONFIG;

	$columnas=array('nombreSoporteLOPD','activo');
	$codigoCliente=obtenerCodigoCliente(true);
	$having=obtieneWhereListado("HAVING codigoCliente=".$codigoCliente,$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	
	conexionBD();
	$consulta=consultaBD("SELECT soportes_lopd_nueva.* FROM soportes_lopd_nueva INNER JOIN clientes ON soportes_lopd_nueva.codigoCliente=clientes.codigo $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT soportes_lopd_nueva.* FROM soportes_lopd_nueva INNER JOIN clientes ON soportes_lopd_nueva.codigoCliente=clientes.codigo $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['nombreSoporteLOPD'],
			$datos['marcaSoporteLOPD'],
			formateaFechaWeb($datos['fechaSoporteLOPD']),
			$datos['responsableSoporteLOPD'],
			creaBotonDetalles("zona-cliente-soportes/gestion.php?codigo=".$datos['codigo']),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}



function gestionSoportes(){
	operacionesSoportes();

	abreVentanaGestion('Gestión de Soportes','?','','icon-edit','margenAb');
	$datos=compruebaDatos('soportes_lopd_nueva');

    if(!$datos){
    	campoOculto(obtenerCodigoCliente(true),'codigoCliente');
    }

	abreColumnaCampos();
   		campoTexto('nombreSoporteLOPD','Soporte',$datos);
		campoTexto('marcaSoporteLOPD','Marca/Modelo',$datos);
		campoTexto('serieSoporteLOPD','Nº Serie',$datos);
		campoFecha('fechaSoporteLOPD','Fecha de Adquisición',$datos);
		campoTexto('ubicacionSoporteLOPD','Ubicación',$datos);
		campoTexto('sistemaSoporteLOPD','Sistema operativo',$datos);
		campoTexto('aplicacionesSoporteLOPD','Aplicaciones instaladas',$datos);
		campoTexto('conexionesSoporteLOPD','Conexiones remotas',$datos);
		campoTexto('responsableSoporteLOPD','Responsable',$datos);
		campoTexto('otrosSoporteLOPD','Otros usuarios',$datos);
	cierraColumnaCampos();
	abreColumnaCampos();
		campoTexto('contenidoSoporteLOPD','Contenido',$datos);	
		areaTexto('mecanismosAccesoSoporteLOPD','Mecanismos de Acceso al puesto',$datos);		
		areaTexto('mecanismosAccesoRecursosSoporteLOPD','Mecanismos de Acceso a los recursos',$datos);	
	cierraColumnaCampos();

	cierraVentanaGestion('index.php',true);
}

function filtroUsuarios(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Agrupación','','span3');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectSiNoFiltro(1,'Activa');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de agrupaciones
<?php
    include_once("funciones.php");
    include_once('../../api/js/firma/signature-to-image.php');
    compruebaSesion();

    $contenido=generaPDFAbono($_GET['codigo']);

    require_once('../../api/html2pdf/html2pdf.class.php');
    $html2pdf=new HTML2PDF('P','A4','es',true,'UTF-8',array(20,10,10,15));
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Documento-Factura.pdf');
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de abonos

function creaEstadisticasAbonos($anio=''){
	if($anio==''){
		$anio=date('Y');
	}
	$perfil=$_SESSION['tipoUsuario'];
    $codigoUsuario=$_SESSION['codigoU'];

    if($perfil=='ADMINISTRACION2'){
        $res=consultaBD("SELECT COUNT(facturas.codigo) AS total, IFNULL(SUM(total),0) AS importe FROM facturas LEFT JOIN comerciales ON facturas.codigoComercial=comerciales.codigo WHERE facturas.activo='SI' AND tipoFactura='ABONO' AND codigoUsuario=$codigoUsuario AND fecha LIKE'$anio-%'",true,true);
    }
    else{
        $res=consultaBD("SELECT COUNT(codigo) AS total, IFNULL(SUM(total),0) AS importe FROM facturas WHERE activo='SI' AND tipoFactura='ABONO' AND fecha LIKE'$anio-%'",true,true);
    }
	
	return $res;
}

function operacionesAbonos(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaFactura();
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=creaFactura();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('facturas');
	}
	elseif(isset($_POST['codigoFactura0'])){
		$res=actualizaFacturacionPendientes();
	}

	mensajeResultado('codigoCliente',$res,'Factura');
    mensajeResultado('elimina',$res,'Factura', true);
}

function creaFactura(){
	formateaImportesFacturas();
	$res=insertaDatos('facturas');
	
	if($res){
		$res=insertaCamposAdicionalesFactura($res);
	}

	return $res;
}

function actualizaFactura(){
	formateaImportesFacturas();
	$res=actualizaDatos('facturas');
	
	if($res){
		$res=insertaCamposAdicionalesFactura($_POST['codigo'],true);
	}

	return $res;
}

function formateaImportesFacturas(){
	formateaPrecioBDD('baseImponible');
	formateaPrecioBDD('total');
}

function insertaCamposAdicionalesFactura($codigoFactura,$actualizacion=false){
	$res=true;
	$datos=arrayFormulario();

	conexionBD();

	if($actualizacion){
		$res=$res && consultaBD("DELETE FROM vencimientos_facturas WHERE codigoFactura=$codigoFactura");
	}

	$res=$res && insertaVencimientosFactura($datos,$codigoFactura);

	cierraBD();

	return $res;
}


function listadoAbonos($anio=''){
	if($anio==''){
		$anio=date('Y');
	}
	$columnas=array(
		'fecha',
		'serie',
		'numero',
		'emisores.razonSocial',
		'clientes.razonSocial',
		'clientes.cif',
		'baseImponible',
		'iva',
		'total',
		'anulada',
		'facturas.medioPago',
		'comerciales.nombre',
		'facturas.activo',
		'cobros_facturas.codigo',
		'codigoCentroGestor',
		'fecha',
		'facturas.codigoComercial',
		'facturas.codigoColaborador',
		'cuentas_cliente.codigo'
	);

	$where=obtieneWhereListado("WHERE tipoFactura='ABONO'",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT facturas.codigo, fecha, serie, numero, emisores.razonSocial AS emisor, clientes.razonSocial AS cliente, clientes.cif, baseImponible, iva, total, facturas.anulada, facturas.medioPago, comerciales.nombre AS comercial, facturas.activo,
			cobros_facturas.codigo AS codigoCobro, codigoCentroGestor, facturas.codigoComercial, facturas.codigoColaborador, cuentas_cliente.codigo AS codigoCuenta,
			clientes.codigo AS codigoCliente, codigoUsuario
			FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
			LEFT JOIN comerciales ON facturas.codigoComercial=comerciales.codigo
			LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
			LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo 
			LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura
			LEFT JOIN grupos_en_facturas ON facturas.codigo=grupos_en_facturas.codigoFactura
			LEFT JOIN grupos ON grupos_en_facturas.codigoGrupo=grupos.codigo
			LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo
			LEFT JOIN cuentas_cliente ON facturas.codigoCliente=cuentas_cliente.codigoCliente
			$where AND fecha LIKE '$anio-%'";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$botonAcciones=obtieneBotonAccionesFactura($datos);

		$fila=array(
			formateaFechaWeb($datos['fecha']),
			$datos['serie'],
			$datos['numero'],
			$datos['emisor'],
			"<a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' class='noAjax' target='_blank'>".$datos['cliente']."</a>",
			$datos['cif'],
			'<div class="nowrap pagination-right">'.formateaNumeroWeb($datos['baseImponible']).' €</div>',
			'<div class="nowrap pagination-right">'.$datos['iva'].' %</div>',
			'<div class="nowrap pagination-right">'.formateaNumeroWeb($datos['total']).' €</div>',
			$datos['anulada'],
			$datos['medioPago'],
			$datos['comercial'],
        	$botonAcciones,
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function obtieneBotonAccionesFactura($datos){
	/*if($datos['compromisoPago']=='NO'){
		$res=botonAcciones(array('Detalles','Descargar factura'),array('facturas/gestion.php?codigo='.$datos['codigo'],'facturas/generaDocumento.php?tipo=factura&codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download'),array(0,1));
	}
	else{
		$res=botonAcciones(array('Detalles','Descargar factura','Descargar compromiso de pago'),array('facturas/gestion.php?codigo='.$datos['codigo'],'facturas/generaDocumento.php?tipo=factura&codigo='.$datos['codigo'],'facturas/generaDocumento.php?tipo=compromiso&codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download',"icon-cloud-download"),array(0,1,1));
	}*/

	$res=botonAcciones(array('Detalles','Descargar abono'),array('abonos/gestion.php?codigo='.$datos['codigo'],'abonos/generaAbono.php?codigo='.$datos['codigo']),array("icon-search-plus",'icon-cloud-download'),array(0,1),false,'');
	

	return $res;
}

function gestionAbono(){
	if((isset($_REQUEST['codigoFactura']) && $_REQUEST['codigoFactura']!='NULL') || isset($_GET['codigo'])){
		formularioDetallesAbono();
	}
	else{
		formularioSeleccionFactura();
	}
}

function formularioSeleccionFactura(){
	abreVentanaGestion('Gestión de Abonos','?','','icon-edit','margenAb');

	echo "<p>Por favor, seleccione a continuación la factura para la que desea emitir el abono:</p><br />";
	campoSelectConsulta('codigoFactura','Factura',"SELECT facturas.codigo, CONCAT(serie,'-',numero,' del ',DATE_FORMAT(fecha,'%d/%m/%Y'),' para el cliente ',razonSocial) AS texto FROM facturas LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE facturas.activo='SI' AND tipoFactura!='ABONO' ORDER BY fecha DESC;",false,'selectpicker span8 show-tick');

	cierraVentanaGestion('index.php',false,true,'Continuar','icon-chevron-right');
}

function formularioDetallesAbono(){
	operacionesAbonos();

	abreVentanaGestion('Gestión de Abonos','?','span3');
	$datos=obtieneDatosAbono();

	campoSelectClienteFiltradoPorUsuario($datos,'Cliente','codigoCliente','obligatorio');
	campoSelectConsulta('codigoAsesoria','Asesoría',"SELECT codigo, asesoria AS texto FROM asesorias WHERE activo='SI'",$datos);
	campoTexto('fecha','Fecha',formateaFechaWeb($datos['fecha']),'input-small datepicker hasDatepicker obligatorio');
	
	campoFacturaAsociada($datos);

	campoTextoSimbolo('baseImponible','Base imponible','€',formateaNumeroWeb($datos['baseImponible']));
	campoTextoSimbolo('iva','Tipo IVA','%',$datos);
	campoTextoSimbolo('total','Total','€',formateaNumeroWeb($datos['total']),'input-mini pagination-right obligatorio');
	campoFormaPago($datos);
	campoCuentaBancariaCliente($datos);
	areaTexto('observaciones','Observaciones',$datos);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectConsulta('codigoSerieFactura','Serie',"SELECT codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",$datos,'span1 selectpicker show-tick');
	campoTexto('numero','Número',$datos,'input-mini pagination-right obligatorio');
	campoSelectConsulta('codigoEmisor','Emisor','SELECT codigo, razonSocial AS texto FROM emisores WHERE activo="SI" ORDER BY razonSocial;',$datos,'selectpicker show-tick span3 obligatorio');
	campoSelectComercial($datos,false);
	campoSelectColaborador($datos,false);

	campoSelectConsulta('codigoTelemarketing','Telemarketing',"SELECT codigo, nombre AS texto FROM telemarketing WHERE activo='SI' ORDER BY nombre;",$datos);
	areaTexto('comentariosLlamada','Comentarios de llamada');
	campoRadio('anulada','Anulada',$datos);
	campoOculto($datos,'activo','SI');
	

	cierraColumnaCampos();
	abreColumnaCampos('span11');

	creaTablaVencimientos($datos);

	cierraVentanaGestion('index.php',true);
}

function obtieneDatosAbono(){
	if(isset($_REQUEST['codigoFactura'])){
		$res=datosRegistro('facturas',$_REQUEST['codigoFactura']);
		$res['codigoSerieFactura']=false;
		$res['numero']=false;
		$res['fecha']=false;
		$res['creacion']=true;//Índice personalizado para indicar que el formulario está en la creación de un nuevo abono
	}
	else{
		$res=compruebaDatos('facturas');	
		$res['creacion']=false;
	}

	return $res;
}


function campoFacturaAsociada($datos){
	
	if(isset($datos['codigoFacturaAsociada'])){
		$codigoFactura=$datos['codigoFacturaAsociada'];
	}
	else{
		$codigoFactura=$_REQUEST['codigoFactura'];
	}

	campoSelectConsulta('codigoFacturaAsociada','Factura asociada',"SELECT facturas.codigo, CONCAT(serie,'-',numero,' del ',DATE_FORMAT(fecha,'%d/%m/%Y'),' para el cliente ',razonSocial) AS texto FROM facturas LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE facturas.activo='SI' AND tipoFactura!='ABONO' ORDER BY fecha DESC;",$codigoFactura,'selectpicker span3 show-tick','',true);
	campoOculto($codigoFactura,'codigoFacturaAsociada');
	campoOculto('ABONO','tipoFactura');
}

function campoCuentaBancariaCliente($datos){
	echo "<div id='cajaCuenta'>";

	if(!$datos){
		campoSelect('codigoCuentaCliente','Cuenta cliente',array(),array());
	}
	else{
		campoSelectConsulta('codigoCuentaCliente','Cuenta cliente',"SELECT codigo, ccc AS texto FROM cuentas_cliente WHERE codigoCliente=".$datos['codigoCliente']." ORDER BY ccc",$datos);
	}

	echo "</div>";
}

function consultaNumeroSerieFactura(){
	$serie=$_POST['codigoSerieFactura'];

	$datos=consultaBD("SELECT IFNULL(MAX(numero),0) AS numero FROM facturas WHERE codigoSerieFactura=$serie;",true,true);

	$numero=$datos['numero']+1;

	echo $numero;
}

function campoTipoFactura($datos){
	if(!$datos){
		campoSelect('tipoFactura','Tipo',array('','Formación','Servicios','Otros conceptos'),array('','FORMACION','SERVICIOS','OTROS'),$datos,'selectpicker span2 show-tick obligatorio','');
	}
	else{
		$tipos=array('FORMACION'=>'Formación','SERVICIOS'=>'Servicios','OTROS'=>'Otros conceptos');
		campoDato('Tipo',$tipos[$datos['tipoFactura']]);
		campoOculto($datos,'tipoFactura');
	}
}



/*
	Reglas para obtener el vencimiento:
	+ A Bonificación: Ej: Si un curso finaliza el mes de Mayo, independientemente del día, tendría que aparecer la fecha 01 de Julio. 
	+ Quincenas: Si la factura tiene fecha del 01 al 15 el vencimiento es el 24 del mismo mes. 
	             Si la factura tiene fecha del 16 al 31, el vencimiento es el 09 del mes siguiente.
	+ Día 1 pasados 2 meses: Si la factura tiene fecha 28/06 sería el 1 de Septiembre.
*/
function obtieneVencimientoConcepto($tipoVencimiento,$fecha){
	$arrayFecha=explode('-',$fecha);

	if($tipoVencimiento=='A BONIFICACIÓN'){
		$arrayFecha=sumaMesFecha($arrayFecha,2);
		$arrayFecha[2]='01';
	}
	elseif($tipoVencimiento=='QUINCENAS' && $arrayFecha[2]<16){
		$arrayFecha[2]='24';
	}
	elseif($tipoVencimiento=='QUINCENAS' && $arrayFecha[2]>15){
		$arrayFecha=sumaMesFecha($arrayFecha,1);
		$arrayFecha[2]='09';
	}
	else{
		$arrayFecha=sumaMesFecha($arrayFecha,3);
		$arrayFecha[2]='01';
	}

	$res=$arrayFecha[2].'/'.$arrayFecha[1].'/'.$arrayFecha[0];

	return $res;
}

function sumaMesFecha($arrayFecha,$suma){
	$arrayFecha[1]+=$suma;

	if($arrayFecha[1]>12){
		$arrayFecha[1]-=12;
		$arrayFecha[0]+=1;
	}

	if($arrayFecha[1]<10){
		$arrayFecha[1]='0'.$arrayFecha[1];
	}

	return $arrayFecha;
}


function creaTablaVencimientos($datos){
	echo "<br />
	<div class='control-group'>                     
		<label class='control-label'>Vencimientos:</label>
	    <div class='controls'>
	    </div>
	</div>
	<div class='centro'>
		<div class='table-responsive'>
			<table class='table table-striped tabla-simple' id='tablaVencimientos'>
			  	<thead>
			    	<tr>
			            <th> Medio pago </th>
						<th> F. Vencimiento </th>
						<th> Importe </th>
						<th> Estado </th>
						<th> Concepto </th>
						<th> F. Devolución </th>
						<th> Gastos </th>
						<th> </th>
			    	</tr>
			  	</thead>
			  	<tbody>";

			  		$i=0;
			  		conexionBD();

		  			$consulta=consultaBD("SELECT * FROM vencimientos_facturas WHERE codigoFactura=".$datos['codigo']);
		  			while($datosVencimiento=mysql_fetch_assoc($consulta)){
		  				imprimeLineaTablaVencimientos($datosVencimiento,$i,$datos);
		  				campoOculto($datosVencimiento['codigo'],'codigoVencimiento'.$i);
		  				$i++;
		  			}
			  		

			  		if($i==0){
			  			imprimeLineaTablaVencimientos(false,0,false);
			  		}
			  		cierraBD();

	echo "		</tbody>
			</table>
			<div class='centro'>
				<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaVencimientos\");'><i class='icon-plus'></i> Añadir vencimiento</button> 
				<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaVencimientos\");'><i class='icon-trash'></i> Eliminar vencimiento</button>
			</div>
		</div>
	</div><br /><br />";
}

function imprimeLineaTablaVencimientos($datosVencimiento,$i,$datosFactura){
	$j=$i+1;

	if(!$datosFactura['creacion'] || ($datosFactura['creacion'] && $i==0)){//Si se está en los detalles o en la creación y es la primera fila, se imprime (en la creación solo hay 1 vencimiento)
		$importeVencimiento=formateaNumeroWeb($datosVencimiento['importe']);

		if($datosFactura['creacion']){
			$datosVencimiento['medioPago']=false;
			$datosVencimiento['estado']='PENDIENTE PAGO';
			$datosVencimiento['concepto']=false;
			$importeVencimiento=formateaNumeroWeb($datosFactura['total']);
			$datosVencimiento['fechaDevolucion']='0000-00-00';
			$datosVencimiento['gastosDevolucion']=false;
		}

		echo "
		<tr>";
			campoFormaPago($datosVencimiento['medioPago'],'medioPago'.$i,1);
			campoFechaTabla('fechaVencimiento'.$i,$datosVencimiento['fechaVencimiento']);
			campoTextoSimbolo('importeVencimiento'.$i,'','€',$importeVencimiento,'input-mini pagination-right',1);
			campoSelectEstadoVencimiento($datosVencimiento,$i);
			campoSelectConceptoVencimiento($datosVencimiento,$i,$datosFactura);
			campoFechaTabla('fechaDevolucion'.$i,$datosVencimiento['fechaDevolucion']);		
			campoTextoSimbolo('gastosDevolucion'.$i,'','€',formateaNumeroWeb($datosVencimiento['gastosDevolucion']),'input-mini pagination-right',1);
			campoOculto($datosVencimiento['llamadaAsesoria'],'llamadaAsesoria'.$i,'NO');
			campoOculto($datosVencimiento['llamadaEmpresa'],'llamadaEmpresa'.$i,'NO');
		echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
		</tr>";
	}
}

function campoSelectEstadoVencimiento($datos,$i){
	$estados=array('','Anticipado','Descontado','Impagado definitivo','Impagado gestión de cobro','Pagado','Pendiente pago');
	$valores=array('','ANTICIPADO','DESCONTADO','IMPAGADO DEFINITIVO','IMPAGADO GESTIÓN DE COBRO','PAGADO','PENDIENTE PAGO');

	if(!$datos){
		$datos=array('estado'=>'PENDIENTE PAGO');
	}

	campoSelect('estado'.$i,'',$estados,$valores,$datos['estado'],'selectpicker span3 show-tick','',1);
}

function campoSelectConceptoVencimiento($datos,$i,$datosFactura){
	campoTextoTabla('conceptoVencimiento'.$i,$datos['concepto']);

	//Comentado temporalmente, a espera de ver si prefieren el campo de texto
	/*if(!$datos){
		campoSelect('codigoConcepto'.$i,'',array(),array(),false,'selectpicker span3 show-tick','data-live-search="true"',1);
	}
	else{
		$query=obtieneConsultaConceptosVencimiento($datosFactura['codigo'],$datosFactura['tipo']);
		campoSelectConsulta('codigoConcepto'.$i,'',$query,$datos['codigoConcepto'],'selectpicker span3 show-tick','data-live-search="true"','',1);
	}*/
}


function obtieneConsultaConceptosVencimiento($codigoFactura,$tipo){
	$res='';

	if($tipo=='FORMACION'){
		$res="SELECT grupos.codigo, CONCAT(accionFormativa,' - ',accion,' - ',grupos.grupo) AS texto FROM acciones_formativas INNER JOIN grupos ON acciones_formativas.codigo=grupos.codigoAccionFormativa INNER JOIN grupos_en_facturas ON grupos.codigo=grupos_en_facturas.codigoGrupo WHERE codigoFactura=$codigoFactura ORDER BY accionFormativa;";
	}
	elseif($tipo=='SERVICIOS'){
		$res="SELECT codigo, DATE_FORMAT(ventas_servicios.fecha,'%d/%m/%Y') AS texto FROM ventas_servicios INNER JOIN ventas_en_facturas ON ventas_servicios.codigo=ventas_en_facturas.codigoVentaServicio WHERE codigoFactura=$codigoFactura ORDER BY fecha;";
	}
	elseif($tipo=='OTROS'){
		$res="SELECT codigo, concepto AS texto FROM conceptos_libres_facturas WHERE codigoFactura=$codigoFactura ORDER BY concepto;";
	}

	return $res;
}


function creaTablaCobros($datos){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Cobros:</label>
	    <div class='controls'>
			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaCobros'>
				  	<thead>
				    	<tr>
				            <th> Medio pago </th>
							<th> F. Cobro </th>
							<th> Importe </th>
							<th> Banco </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		conexionBD();
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM cobros_facturas WHERE codigoFactura=".$datos['codigo']);
				  			while($datosVencimiento=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaCobros($datosVencimiento,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaCobros(false,0);
				  		}
				  		cierraBD();

	echo "			</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaCobros\");'><i class='icon-plus'></i> Añadir cobro</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaCobros\");'><i class='icon-trash'></i> Eliminar cobro</button>
				</div>
			</div>
		</div>
	</div>";
}

function imprimeLineaTablaCobros($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	
		campoFormaPago($datos['medioPago'],'medioPagoCobro'.$i,1);
		campoFechaTabla('fechaCobro'.$i,$datos['fechaCobro']);
		campoTextoSimbolo('importeCobro'.$i,'','€',formateaNumeroWeb($datos['importe']),'input-mini pagination-right',1);
		campoSelectConsulta('codigoCuentaPropia'.$i,'',"SELECT codigo, nombre AS texto FROM cuentas_propias WHERE activo='SI' ORDER BY nombre;",$datos['codigoCuentaPropia'],'selectpicker span3 show-tick','data-live-search="true"','',1);
	
	echo "<td class='centro'><input type='checkbox' name='filasTabla[]' value='$j'></td>
	</tr>";
}


function obtieneConceptosFormacion(){
	$codigoCliente=$_POST['codigoCliente'];
	$codigoEmisor=$_POST['codigoEmisor'];

	echo rellenaOptionsConcepto("SELECT grupos.codigo, CONCAT(accionFormativa,' - ',accion,' - ',grupos.grupo) AS texto FROM acciones_formativas INNER JOIN grupos ON acciones_formativas.codigo=grupos.codigoAccionFormativa INNER JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo WHERE grupos.activo='SI' AND codigoCliente=$codigoCliente AND codigoEmisor=$codigoEmisor AND grupos.codigo NOT IN(SELECT codigoGrupo FROM grupos_en_facturas WHERE codigoFactura IS NOT NULL) ORDER BY accionFormativa;");
}

function obtieneConceptosServicios(){
	$codigoCliente=$_POST['codigoCliente'];
	$codigoEmisor=$_POST['codigoEmisor'];

	echo rellenaOptionsConcepto("SELECT ventas_servicios.codigo, DATE_FORMAT(ventas_servicios.fecha,'%d/%m/%Y') AS texto FROM ventas_servicios LEFT JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio LEFT JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo WHERE codigoCliente=$codigoCliente AND codigoEmisor=$codigoEmisor GROUP BY ventas_servicios.codigo ORDER BY fecha ASC;");
}

function rellenaOptionsConcepto($query){
	$res='<option value="NULL"></option>';

	$consulta=consultaBD($query,true);
	while($datos=mysql_fetch_assoc($consulta)){
		$res.="<option value='".$datos['codigo']."'>".$datos['texto']."</option>";
	}

	return $res;
}

function obtieneDatosCliente(){
	$codigoCliente=$_POST['codigoCliente'];
	$res=array();

	$res['asesoria']=obtieneAsesoriaCliente($codigoCliente);

	$res['cuentas']=rellenaOptionsConcepto("SELECT codigo, ccc AS texto FROM cuentas_cliente WHERE codigoCliente=$codigoCliente ORDER BY ccc");

	echo json_encode($res);
}

function obtieneAsesoriaCliente($codigoCliente){
	$asesoria=consultaBD("SELECT asesorias.codigo FROM asesorias LEFT JOIN clientes ON asesorias.codigo=clientes.codigoAsesoria WHERE clientes.codigo=$codigoCliente",true,true);
	
	return $asesoria['codigo'];
}

function obtieneIVAEmisor(){
	$codigoEmisor=$_POST['codigoEmisor'];

	$datos=consultaBD("SELECT tipoIva FROM emisores WHERE codigo=$codigoEmisor",true,true);
	echo $datos['tipoIva'];
}

function filtroAbonos(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoSelectTieneFiltro(13,'Pago realizado');
	campoSelectConsulta(1,'Serie',"SELECT serie AS codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",false,'span1 selectpicker show-tick');
	campoTexto(2,'Número',false,'input-mini pagination-right');
	campoTexto(3,'Emisor');
	campoTexto(5,'CIF cliente',false,'input-small');
	campoTexto(4,'Nombre cliente');
	campoFormaPago(false,10);
	campoFecha(0,'Fecha desde');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(15,'Hasta');
	campoSelectConsulta(14,'Centro',"SELECT codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial");
	campoSelectComercial(false,false,16);
	campoSelectConsulta(17,'Colaborador',"SELECT codigo, nombre AS texto FROM colaboradores WHERE activo='SI' ORDER BY nombre;");
	campoSelectTieneFiltro(18,'Cuenta bancaria');
	//campoSelectSiNoFiltro(19,'G. devolución pagados');
	campoSelectSiNoFiltro(9,'Anulada');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Parte de generación de documentos

function generaPDFAbono($codigoFactura){
	conexionBD();
	
	$datos=consultaBD("SELECT facturas.*, serie, clientes.razonSocial AS cliente, clientes.cif AS cifCliente, clientes.domicilio AS domicilioCliente, 
	clientes.cp AS cpCliente, clientes.localidad AS localidadCliente, clientes.provincia AS provinciaCliente,
	emisores.razonSocial AS emisor, emisores.cif AS cifEmisor, emisores.domicilio AS domicilioEmisor,  emisores.iban AS cuentaEmisor,
	emisores.cp AS cpEmisor, emisores.localidad AS localidadEmisor, emisores.provincia AS provinciaEmisor,
	emisores.telefono, emisores.fax, emisores.web, emisores.email, emisores.registro, emisores.ficheroLogo, codigoFacturaAsociada
	FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	WHERE facturas.codigo=$codigoFactura",false,true);

	$conceptos=obtieneConceptoAbono($datos['codigoFacturaAsociada']);

	$medioPago=obtieneMedioPagoFactura($datos['medioPago'],$datos['codigoCuentaCliente'],$datos['cuentaEmisor']);

	$vencimientos=obtieneVencimientosFactura($datos['codigo']);

	cierraBD();

	$logo='../documentos/emisores/'.$datos['ficheroLogo'];

	$contenido = "
	<style type='text/css'>
	<!--
		.tablaEmisorCliente{
			margin-top:40px;
			font-size:12px;
			width:100%;
		}

		.celdaEmisor{
			width:50%;
			padding-right:20px;
		}

		.celdaCliente{
			border:1px solid #000;
			width:50%;
			padding:10px 30px 1px 30px;
			line-height:15px;
		}

		.cajaNumero{
			margin-top:20px;
			font-size:12px;
		}

		.cajaFecha{
			text-align:right;
			margin-right:170px;
		}

		.cabeceraConceptos, .tablaConceptos{
			margin-top:20px;
			border:1px solid #000;
			width:100%;
		}
		.celdaConcepto{
			width:85%;
			padding:10px;
		}
		.celdaImporte{
			width:15%;
			text-align:right;
			padding:10px;
		}

		.tablaConceptos{
			border-collapse:collapse;
		}

		.tablaConceptos td, .tablaConceptos th{
			font-size:12px;
			border-top:1px solid #000;
			border-bottom:1px solid #000;
			line-height:18px;
		}

		.tablaConceptos .celdaConcepto{
			border-left:1px solid #000;
		}

		.tablaConceptos .celdaImporte{
			border-right:1px solid #000;
		}

		.tablaTotales{
			width:40%;
			position:relative;
			left:408px;
		}

		.tablaTotales .celdaConcepto{
			width:50%;
		}
		.tablaTotales .celdaImporte{
			width:50%;
		}

		.cajaMedioPago{
			font-size:12px;
			line-height:18px;
		}

		.cajaMedioPago ol{
			margin-top:-20px;
		}

		.pie{
			font-size:10px;
			text-align:center;
			line-height:18px;
		}
	-->
	</style>
	<page footer='page'>
	    <img src='".$logo."' class='logo' />
	    <br />

	    <table class='tablaEmisorCliente'>
		    <tr>
		    	<td class='celdaEmisor'>
		    		".$datos['emisor']."<br />
				    ".$datos['domicilioEmisor']."<br />
				    ".$datos['cpEmisor']." ".$datos['localidadEmisor']." - ".convertirMinuscula($datos['provinciaEmisor'])."<br />
				    CIF: ".$datos['cifEmisor']."
		    	</td>
		    	<td class='celdaCliente'>
		    		".$datos['cliente']."<br />
				    ".$datos['domicilioCliente']."<br />
				    ".$datos['cpCliente']." ".$datos['localidadCliente']."<br />
				    ".convertirMinuscula($datos['provinciaCliente'])."<br />
				    CIF: ".$datos['cifCliente']."
		    	</td>
		    </tr>
	    </table>

	    <div class='cajaNumero'>
	    	Abono nº: ".$datos['serie']."".$datos['numero']."
	    	<div class='cajaFecha'>
	    		Fecha: ".formateaFechaWeb($datos['fecha'])."
	    	</div>
	    </div>

	    <table class='cabeceraConceptos'>
		    <tr>
		    	<th class='celdaConcepto'>Concepto</th>
		    	<th class='celdaImporte'></th>
		    </tr>
	    </table>

	    <table class='tablaConceptos'>
		    ".$conceptos."		    
	    </table>

	    <table class='tablaConceptos tablaTotales'>
	    	<tr>
	    		<td class='celdaConcepto'>Base imponible:</td>
	    		<td class='celdaImporte'>".formateaNumeroWeb($datos['baseImponible'])." €</td>
	    	</tr>
	    	<tr>
	    		<td class='celdaConcepto'>I.V.A.:</td>
	    		<td class='celdaImporte'>".$datos['iva']." %</td>
	    	</tr>
	    	<tr>
	    		<th class='celdaConcepto'>Total factura:</th>
	    		<th class='celdaImporte'>".formateaNumeroWeb($datos['total'])." €</th>
	    	</tr>
	    </table>

	    <div class='cajaMedioPago'>
		    ".$medioPago."
		    ".$vencimientos."
		    <br />
		    Observaciones:
		    ".nl2br($datos['observaciones'])."
		</div>

	    <page_footer>
	    	<div class='pie'>
	    		Teléfono: ".formateaTelefono($datos['telefono'])." &nbsp; 
	    		Fax: ".formateaTelefono($datos['fax'])." &nbsp;
	    		Web: <a href='".$datos['web']."'>".$datos['web']."</a> &nbsp;
	    		eMail: <a href='mailto:".$datos['email']."'>".$datos['email']."</a><br />
	    		".$datos['registro']." 
	    	</div>
	    </page_footer>
	</page>";

	return $contenido;
}

function obtieneMedioPagoFactura($medioPago,$codigoCuentaCliente,$cuentaEmisor){
	$res='Medio pago: '.$medioPago.'.';
	
	if($medioPago=='TRANSFERENCIA' || $medioPago=='RECIBO SEPA'){
		$datos=consultaBD("SELECT iban FROM cuentas_cliente WHERE codigo='$codigoCuentaCliente';",false,true);
		$res.='<br />Cuenta de origen: '.$datos['iban'];
		$res.='<br />Cuenta de destino: '.$cuentaEmisor;
	}

	return $res;
}

function obtieneVencimientosFactura($codigoFactura){
	$res='';

	$consulta=consultaBD("SELECT fechaVencimiento, importe, medioPago FROM vencimientos_facturas WHERE codigoFactura=$codigoFactura ORDER BY fechaVencimiento;");
	if(mysql_num_rows($consulta)>0){
		$res='<br />Vencimientos:<ol>';

		while($datos=mysql_fetch_assoc($consulta)){
			$res.="<li>".formateaFechaWeb($datos['fechaVencimiento']).": ".formateaNumeroWeb($datos['importe']).' € ('.$datos['medioPago'].')</li>';
		}

		$res.='</ol>';
	}

	return $res;
}

function obtieneConceptoAbono($codigoFacturaAsociada){
	$res=consultaBD("SELECT CONCAT(serie,'-',numero,' del ',DATE_FORMAT(fecha,'%d/%m/%Y'),' para el cliente ',razonSocial) AS texto FROM facturas LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE facturas.codigo=$codigoFacturaAsociada",false,true);

	$res="<tr><td class='celdaConcepto'>Abono de la factura ".$res['texto'].".</td><td class='celdaImporte'></td></tr>";

	return $res;
}


/*function generaPDFCompromisoPago($codigoFactura){
	conexionBD();
	
	$datos=consultaBD("SELECT facturas.codigo, facturas.fecha, facturas.medioPago, administrador, nifAdministrador, clientes.razonSocial AS cliente,
	emisores.razonSocial AS emisor, serie, numero, total, ficheroLogo, firma
	FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	WHERE facturas.codigo=$codigoFactura",false,true);

	$vencimientos=obtieneVencimientosCompromiso($datos['codigo']);

	cierraBD();

	$imagenFirma=obtieneImagenFirmaConsentimiento($datos['firma']);

	$logo='../documentos/emisores/'.$datos['ficheroLogo'];

	$contenido = "
	<style type='text/css'>
	<!--
		.formaPago, .introduccion, .cajaFirma{
			line-height:28px;
		}

		.formaPago{
			font-weight:bold;
			text-align:center;
			margin-top:65px;
			font-size:15px;
		}

		.introduccion{
			margin-top:20px;
			margin-bottom:20px;
			font-size:16px;
			text-align:justify;
			width:100%;
		}

		.vencimientos{
			margin-top:30px;
			margin-left:20px;
			font-size:16px;
			margin-bottom:30px;
		}

		.cajaFirma{
			font-size:16px;
		}
	-->
	</style>
	<page>
	    <img src='".$logo."' class='logo' />
	    <br />
	    <div class='formaPago'>
	    	Compromiso de pago por: ".$datos['medioPago']."
	    </div>

	    <div class='introduccion'>
		    D./Dña. <strong>".$datos['administrador']."</strong> con NIF <strong>".$datos['nifAdministrador']."</strong>
		    en representación legal de la Entidad <strong>".$datos['cliente']."</strong>.
		    <br /><br />
		    En relación con el curso de Formación Continua que <strong>".$datos['emisor']."</strong> nos ha tramitado,
		    cuya factura número <strong>".$datos['serie'].'/'.$datos['numero']."</strong> asciende a 
		    <strong>".formateaNumeroWeb($datos['total'])."</strong> €, ruego atiendan el recibo que se girará en el número de cuenta y
		    fechas indicadas en la factura.
	    </div>

	    <div class='vencimientos'>
	    	".$vencimientos."
	    </div>

	    <div class='cajaFirma'>
	    	Y para que sirva como justificante del mismo firmo a <strong>".formateaFechaWeb($datos['fecha'])."</strong>:
	    	<br /><br /><br />
	    	Firma y sello<br />
	    	".$imagenFirma."
	    </div>

	</page>";

	return $contenido;
}

function obtieneImagenFirmaConsentimiento($firma){
	$res='';

	if(trim($firma)!=''){
		$imagenFirma=sigJsonToImage($firma, array('imageSize'=>array(310, 100)));
		imagepng($imagenFirma,'../documentos/facturas/firma.png');

		$res="<img src='../documentos/facturas/firma.png' />";
	}

	return $res;
}


function obtieneVencimientosCompromiso($codigoFactura){
	$res='';

	$consulta=consultaBD("SELECT fechaVencimiento, importe FROM vencimientos_facturas WHERE codigoFactura=$codigoFactura ORDER BY fechaVencimiento;");
	while($datos=mysql_fetch_assoc($consulta)){
		$res.="Fecha: <strong>".formateaFechaWeb($datos['fechaVencimiento'])."</strong>. Importe: <strong>".formateaNumeroWeb($datos['importe']).' €</strong><br /><br />';
	}

	
	return $res;
}*/

function generaWordSepa($PHPWord,$codigoFactura){
	$datos=consultaBD("SELECT serie, numero, referenciaAcreedor, emisores.razonSocial AS emisor, emisores.domicilio AS domicilioEmisor,
	emisores.cp AS cpEmisor, emisores.localidad AS localidadEmisor, emisores.provincia AS provinciaEmisor, 
	clientes.razonSocial AS razonSocialCliente, clientes.domicilio AS domicilioCliente, clientes.cp AS cpCliente,
	clientes.localidad AS localidadCliente, clientes.provincia AS provinciaCliente, cuentas_cliente.bic, cuentas_cliente.iban, 
	fechaSepa, tipoSepa, firma, ficheroLogo
	FROM facturas LEFT JOIN series_facturas ON facturas.codigoSerieFactura=series_facturas.codigo
	LEFT JOIN emisores ON facturas.codigoEmisor=emisores.codigo
	LEFT JOIN cobros_facturas ON facturas.codigo=cobros_facturas.codigoFactura
	LEFT JOIN cuentas_propias ON cobros_facturas.codigoCuentaPropia=cuentas_propias.codigo
	LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo
	LEFT JOIN cuentas_cliente ON facturas.codigoCuentaCliente=cuentas_cliente.codigo
	WHERE facturas.codigo=$codigoFactura
	GROUP BY cobros_facturas.codigoFactura",true,true);

	$documento=$PHPWord->loadTemplate('../documentos/sepa/plantilla.docx');

	$referencia=$datos['serie'].str_pad($datos['numero'],5,'0',STR_PAD_LEFT);

	$documento->setValue("referenciaFactura",utf8_decode($referencia));
	$documento->setValue("refAcreedor",utf8_decode($datos['referenciaAcreedor']));
	$documento->setValue("emisor",utf8_decode($datos['emisor']));
	$documento->setValue("domicilioEmisor",utf8_decode($datos['domicilioEmisor']));
	$documento->setValue("cpEmisor",utf8_decode($datos['cpEmisor']));
	$documento->setValue("localidadEmisor",utf8_decode($datos['localidadEmisor']));
	$documento->setValue("provinciaEmisor",convertirMinuscula($datos['provinciaEmisor']));

	$documento->setValue("razonSocialCliente",utf8_decode($datos['razonSocialCliente']));
	$documento->setValue("domicilioCliente",utf8_decode($datos['domicilioCliente']));
	$documento->setValue("cpCliente",utf8_decode($datos['cpCliente']));
	$documento->setValue("localidadCliente",utf8_decode($datos['localidadCliente']));
	$documento->setValue("provinciaCliente",convertirMinuscula($datos['provinciaCliente']));

	$documento->setValue("bicCliente",utf8_decode($datos['bic']));
	$documento->setValue("ibanCliente",utf8_decode($datos['iban']));
	
	if($datos['tipoSepa']=='Pago recurrente'){
		$documento->setValue("re",'X');
		$documento->setValue("un",' ');
	}
	else{
		$documento->setValue("re",' ');
		$documento->setValue("un",'X');
	}

	$documento->setValue("fechaSepa",formateaFechaWeb($datos['fechaSepa']));

	insertaImagenFirmaWord($datos['firma'],$documento);
	insertaImagenLogoSepa($datos['ficheroLogo'],$documento);

	$documento->save('../documentos/sepa/Orden-SEPA.docx');
}

function insertaImagenLogoSepa($logo,$documento){
	if(trim($logo)!='' && $logo!='NO'){
		copy('../documentos/emisores/'.$logo,'../documentos/sepa/image1.png');//Porque la imagen debe llamarse igual que en el Word

		$documento->replaceImage('../documentos/sepa/','image1.png');
	}
}

//Fin parte generación documentos


function ventasPendientes(){
	if(!isset($_POST['codigoListaGrupo']) && !isset($_POST['codigoListaVenta'])){
		formularioSeleccionVentasPendientes();
	}
	else{
		formularioConfirmacionVentasPendientes();
	}
}

function formularioSeleccionVentasPendientes(){
	abreVentanaGestion('Gestión de ventas pendientes','?');
	echo "<p class='justificado'>A continuación, seleccione las ventas pendientes que desea facturar.
	Las ventas se agruparán en facturas por tipo de concepto (formación u otros servicios) y cliente, teniendo que seleccionar
	después el número de factura y el emisor:</p>

	<table class='table table-striped tablaTextoPeque table-bordered datatable' id='tablaVentas'>
        <thead>
          <tr>
          	<th>Cliente</th>
          	<th>Administrativo/a</th>
          	<th>Comercial</th>
          	<th>Fecha</th>
            <th>Curso/Servicio</th>
            <th>Acción Formativa</th>
            <th>Grupo</th>
            <th class='sumatorio'>Importe</th>
            <th class='centro'><input type='checkbox' id='todo'></th>
          </tr>
        </thead>
        <tbody>
          
    	</tbody>
    </table>";



	cierraVentanaGestion('index.php',false,true,'Continuar','icon-chevron-right');
}

function listadoVentasPendientes(){
	global $_CONFIG;
	$codigoEmisorFormacion=$_CONFIG['codigoEmisorFormacion'];

	$columnas=array('razonSocial','fecha','accionFormativa','accion','grupo','importe','activo');
	$having=obtieneWhereListado("HAVING 1=1",$columnas,true);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	//Unifico en una misma consulta el las ventas de formación y de servicios, mediante UNION ALL. Casi ná
	$query="SELECT grupos.codigo, accionFormativa, accion, grupos.grupo, razonSocial, precioFactura AS importe,
	grupos.fechaAlta AS fecha, clientes.codigo AS codigoCliente, 'Grupo' AS tipo, usuarios.codigo AS codigoUsuario,
	$codigoEmisorFormacion AS codigoEmisor, comerciales.nombre AS comercial, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS administrativo

	FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo
	LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo
	LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo
	LEFT JOIN comerciales_cliente ON clientes.codigo=comerciales_cliente.codigoCliente
	LEFT JOIN comerciales ON comerciales_cliente.codigoComercial=comerciales.codigo	
	LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo

	WHERE grupos.codigo NOT IN (SELECT codigoGrupo FROM grupos_en_facturas WHERE codigoGrupo IS NOT NULL AND codigoFactura IS NOT NULL)
	AND grupos.activo='SI'

	UNION ALL

	SELECT ventas_servicios.codigo, '-' AS accionFormativa, servicio AS accion, '-' AS grupo, razonSocial, 
	ventas_servicios.total AS importe, ventas_servicios.fecha AS fecha, clientes.codigo AS codigoCliente, 'Venta' AS tipo, usuarios.codigo AS codigoUsuario,
	codigoEmisor, comerciales.nombre AS comercial, CONCAT(usuarios.nombre,' ',usuarios.apellidos) AS administrativo

	FROM ventas_servicios LEFT JOIN conceptos_venta_servicios ON ventas_servicios.codigo=conceptos_venta_servicios.codigoVentaServicio
	LEFT JOIN servicios ON conceptos_venta_servicios.codigoServicio=servicios.codigo
	LEFT JOIN clientes ON ventas_servicios.codigoCliente=clientes.codigo
	LEFT JOIN comerciales ON ventas_servicios.codigoComercial=comerciales.codigo
	LEFT JOIN usuarios ON comerciales.codigoUsuario=usuarios.codigo

	WHERE ventas_servicios.codigo NOT IN (SELECT codigoVentaServicio FROM ventas_en_facturas WHERE codigoVentaServicio IS NOT NULL AND codigoFactura IS NOT NULL)
	AND ventas_servicios.activo='SI'

	GROUP BY ventas_servicios.codigo";
	
	conexionBD();
	$consulta=consultaBD($query." $having $orden $limite;");
	$consultaPaginacion=consultaBD($query." $having;");
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			"<a href='../clientes/gestion.php?codigo=".$datos['codigoCliente']."' target='_blank'>".$datos['razonSocial']."</a>",
			$datos['administrativo'],
			$datos['comercial'],
			formateaFechaWeb($datos['fecha']),
			$datos['accion'],
			$datos['accionFormativa'],			
			$datos['grupo'],			
			"<div class='pagination-right'>".formateaNumeroWeb($datos['importe']).' €</div><input type="hidden" name="emisor'.$datos['tipo'].$datos['codigo'].'" value="'.$datos['codigoEmisor'].'" />',//Añado en un input hidden el codigo del emisor, para ahorrarme consultarlo luego en la creación de las facturas.
			"<div class='centro'><input type='checkbox' name='codigoLista".$datos['tipo']."[]' value='".$datos['codigo']."'></div>",//Generará un codigoListaGrupo y un codigoListaVenta
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function formularioConfirmacionVentasPendientes(){
	abreVentanaGestion('Gestión de ventas pendientes','?');
	echo "<p class='justificado'>Indique por favor la serie, el número y el emisor de cada una de las facturas generadas:</p>

	<table class='table table-striped tabla-simple' id='tablaVentasRegistradas'>
        <thead>
          <tr>
          	<th>Cliente</th>
          	<th>Tipo</th>
            <th>Base imponible</th>
            <th>Serie</th>
            <th>Número</th>
            <th>Emisor</th>
          </tr>
        </thead>
        <tbody>";

        	conexionBD();

        	$facturas=creaFacturasVentasPendientes();

        	$i=0;
        	foreach($facturas as $codigoFactura){
        		$datos=consultaBD("SELECT razonSocial, tipoFactura, baseImponible, codigoEmisor FROM facturas LEFT JOIN clientes ON facturas.codigoCliente=clientes.codigo WHERE facturas.codigo=$codigoFactura;",false,true);
        		echo "<tr>
        				<td>".$datos['razonSocial']."</td>
        				<td>".$datos['tipoFactura']."</td>
        				<td>".formateaNumeroWeb($datos['baseImponible'])." €</td>";
        				campoSelectConsulta('codigoSerieFactura'.$i,'',"SELECT codigo, serie AS texto FROM series_facturas WHERE activo='SI' ORDER BY serie",false,'span2 selectpicker serie show-tick','data-live-search="true"','',1,false);
        				campoTextoTabla('numero'.$i,false,'input-mini pagination-right');
        				campoSelectConsulta('codigoEmisor'.$i,'','SELECT codigo, razonSocial AS texto FROM emisores WHERE activo="SI" ORDER BY razonSocial;',$datos['codigoEmisor'],'selectpicker show-tick span3','data-live-search="true"','',1,false);
        		echo "</tr>";

        		campoOculto($codigoFactura,'codigoFactura'.$i);
        		campoOculto($datos['baseImponible'],'baseImponible'.$i);//Para hacer el cálculo del total con el IVA del emisor
        		$i++;
        	}

        	cierraBD();

	echo "          
    	</tbody>
    </table>";



	cierraVentanaGestion('?',false,true,'Guardar','icon-check',false);
}


function creaFacturasVentasPendientes(){
	$res=array();
	$datos=arrayFormulario();
	$fecha=fechaBD();

	$res=creaFacturasConceptosVentasPendientes($res,$datos,$fecha,'FORMACION','codigoListaGrupo','emisorGrupo','clientes_grupo','codigoGrupo',
	'precioFactura','grupos_en_facturas');//Modularización al poder

	$res=creaFacturasConceptosVentasPendientes($res,$datos,$fecha,'SERVICIOS','codigoListaVenta','emisorVenta','ventas_servicios','codigo','total',
	'ventas_en_facturas');//No hago $res=$res && porque $res es un array

	return $res;
}

function creaFacturasConceptosVentasPendientes($res,$datos,$fecha,$tipoVenta,$indiceListado,$indiceEmisor,$tablaImportes,$campoCondicion,$campoImporte,$tablaConceptosFactura){
	if(isset($datos[$indiceListado])){
		$clientes=array();//Para agrupar facturas por clientes y emisores. Formato: $clientes=array($codigoCliente=>array($emisor=>$total,$emisor2=>$total2),$codigoCliente2=>array...);
		$conceptos=array();//Estructura: $conceptos[$cliente]=array($emisor1=>array($concepto1,$concepto2,...),$emisor2=>array(...));

		foreach($datos[$indiceListado] as $codigoVenta){
			$venta=consultaBD("SELECT * FROM $tablaImportes WHERE $campoCondicion=$codigoVenta;",false,true);

			$codigoEmisor=$datos[$indiceEmisor.$codigoVenta];
			$codigoCliente=$venta['codigoCliente'];

			
			if(!isset($clientes[$codigoCliente])){//No existe el cliente en el array
				$clientes[$codigoCliente]=array();
				$clientes[$codigoCliente][$codigoEmisor]=$venta[$campoImporte];

				$conceptos[$codigoCliente]=array();
				$conceptos[$codigoCliente][$codigoEmisor]=array();
			}
			elseif(isset($clientes[$codigoCliente]) && !isset($clientes[$codigoCliente][$codigoEmisor])){//Existe el cliente pero no el emisor
				$clientes[$codigoCliente][$codigoEmisor]=$venta[$campoImporte];

				$conceptos[$codigoCliente][$codigoEmisor]=array();
			}
			else{//Ya existen el emisor y el cliente
				$clientes[$codigoCliente][$codigoEmisor]+=$venta[$campoImporte];
			}


			array_push($conceptos[$codigoCliente][$codigoEmisor],$codigoVenta);
		}

		foreach($clientes as $codigoCliente => $emisores){
			//A continuación extraigo de la ficha del cliente el medio de pago y los colaboradores, para insertarlos por defecto en la factura (lo hago antes del segundo foreach porque las ventas ya están agrupadas por clientes)
			$datosCliente=obtieneDatosColaboradoresCliente($codigoCliente);

			foreach($emisores as $codigoEmisor => $baseImponible){

				$consulta=consultaBD("INSERT INTO facturas(codigoEmisor,fecha,codigoCliente,tipoFactura,baseImponible,codigoComercial,codigoColaborador,
				codigoTelemarketing,medioPago,anulada,activo) VALUES($codigoEmisor,'$fecha',$codigoCliente,'$tipoVenta',$baseImponible,
				".$datosCliente['codigoComercial'].",".$datosCliente['codigoColaborador'].",".$datosCliente['codigoTelemarketing'].",
				'".$datosCliente['medioPago']."','NO','SI');");

				if($consulta){
					$codigoFactura=mysql_insert_id();

					foreach($conceptos[$codigoCliente][$codigoEmisor] as $codigoVenta){
						$consulta=$consulta && consultaBD("INSERT INTO $tablaConceptosFactura VALUES(NULL,$codigoVenta,$codigoFactura)");
					}

					if($consulta){
						array_push($res,$codigoFactura);
					}
				}
			}
		}
	}

	return $res;
}

function obtieneDatosColaboradoresCliente($codigoCliente){
	$res=consultaBD("SELECT medioPago, codigoComercial, codigoColaborador, codigoTelemarketing FROM clientes WHERE codigo=$codigoCliente",false,true);

	$res['codigoComercial']=compruebaCampoNULLCliente($res,'codigoComercial');
	$res['codigoColaborador']=compruebaCampoNULLCliente($res,'codigoColaborador');
	$res['codigoTelemarketing']=compruebaCampoNULLCliente($res,'codigoTelemarketing');

	return $res;
}

function compruebaCampoNULLCliente($datos,$indice){
	$res=$datos[$indice];

	if($res==NULL){
		$res='NULL';
	}

	return $res;
}


function actualizaFacturacionPendientes(){
	$res=true;
	$_POST['codigoCliente']='';//Para que entre en mensajeResultado
	$datos=arrayFormulario();

	conexionBD();

	for($i=0;isset($datos['codigoFactura'.$i]);$i++){
		$codigoFactura=$datos['codigoFactura'.$i];
		$codigoSerieFactura=$datos['codigoSerieFactura'.$i];
		$numero=$datos['numero'.$i];
		$codigoEmisor=$datos['codigoEmisor'.$i];
		$baseImponible=$datos['baseImponible'.$i];

		//Calulo de total con el IVA
		$emisor=consultaBD("SELECT tipoIva FROM emisores WHERE codigo=$codigoEmisor",false,true);
		$iva=$emisor['tipoIva'];

		$total=$baseImponible+round($baseImponible*$iva/100,2);

		//Actualización de la factura creada con los datos obtenidos
		$res=$res && consultaBD("UPDATE facturas SET codigoSerieFactura=$codigoSerieFactura, numero=$numero, codigoEmisor=$codigoEmisor, 
								 iva=$iva, total=$total WHERE codigo=$codigoFactura");
	}

	cierraBD();

	return $res;
}

function creaBotonesGestionAbonos(){
	global $_CONFIG;

    echo '
    <a class="btn-floating btn-large btn-default btn-creacion2" href="'.$_CONFIG['raiz'].'facturas/" title="Volver a Facturas"><i class="icon-chevron-left"></i></a>
    <a class="btn-floating btn-large btn-success btn-creacion" href="gestion.php" title="Nuevo registro"><i class="icon-plus"></i></a>
    <a class="btn-floating btn-large btn-danger btn-eliminacion noAjax" id="eliminar" title="Eliminar"><i class="icon-trash"></i></a>';
}

//Fin parte de abonos
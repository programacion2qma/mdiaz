<?php
  $seccionActiva=45;
  include_once('../cabecera.php');
  
  operacionesAbonos();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

      <?php
        creaBotonesGestionAbonos();
      ?>


      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Abonos registrados</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroAbonos();
              ?>
              <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaAbonos">
                <thead>
                  <tr>
                    <th> Fecha </th>
                    <th> Serie </th>
                    <th> Nº </th>
                    <th> Emisor </th>
                    <th> Cliente </th>
                    <th> CIF </th>
                    <th class='sumatorio'> Base Imp. </th>
                    <th> IVA </th>
                    <th class='sumatorio'> Total </th>
                    <th> Anulada </th>
                    <th> Medio pago </th>
                    <th> Comercial </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/filtroTablaAJAX.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    $('.hasDatepicker').datepicker({format:'dd/mm/yyyy',weekStart:1}).on('changeDate',function(e){$(this).datepicker('hide');});
    
    listadoTabla('#tablaAbonos','../listadoAjax.php?include=abonos&funcion=listadoAbonos();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaAbonos');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de acciones formativas

function operacionesAccionesFormativas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaAccionFormativa();
	}
	elseif(isset($_POST['accion'])){
		$res=creaAccionFormativa();
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaAccionesFormativas();
	}

	mensajeResultado('accion',$res,'Servicio');
}


function eliminaAccionesFormativas(){
	$res=true;
	$bloqueadas=0;
	$datos=arrayFormulario();

	conexionBD();
	for($i=0;isset($datos['codigo'.$i]);$i++){
		$codigoAccion=$datos['codigo'.$i];

		if(!compruebaGruposAccionFormativa($codigoAccion)){
			$res=$res && consultaBD("DELETE FROM acciones_formativas WHERE codigo='$codigoAccion';");
		}
		else{
			$accionFormativa=obtieneCodigoAF($codigoAccion);
			mensajeAdvertencia('la acción '.$accionFormativa.' tiene ventas asociadas y no se puede eliminar.');
			$bloqueadas++;
		}
	}
	cierraBD();


	if($bloqueadas==0){
		mensajeResultado('elimina',$res,'Servicio', true);
	}

	return $res;
}

function obtieneCodigoAF($codigo){
	$datos=consultaBD("SELECT accionFormativa FROM acciones_formativas WHERE codigo=$codigo",false,true);

	return $datos['accionFormativa'];
}

function compruebaGruposAccionFormativa($codigoAccion){
	return consultaBD("SELECT codigo FROM grupos WHERE codigoAccionFormativa=$codigoAccion",false,true);
}

function formateaImportesAccion(){
	$_POST['precio']=formateaNumeroWeb($_POST['precio'],true);
	$_POST['precioCoste']=formateaNumeroWeb($_POST['precioCoste'],true);
}


function creaAccionFormativa(){
	formateaImportesAccion();
	$res=insertaDatos('acciones_formativas');

	if($res){
		$codigoAccion=$res;
		$datos=arrayFormulario();

		conexionBD();
		$res=insertaTutoresAccionFormativa($datos,$codigoAccion);
		$res=$res && insertaFormadoresAccionFormativa($datos,$codigoAccion);
		cierraBD();
	}

	return $res;
}

function actualizaAccionFormativa(){
	formateaImportesAccion();
	$res=actualizaDatos('acciones_formativas');

	if($res){
		$datos=arrayFormulario();

		conexionBD();
		$res=insertaTutoresAccionFormativa($datos,$_POST['codigo'],true);
		$res=insertaFormadoresAccionFormativa($datos,$_POST['codigo'],true);
		cierraBD();
	}

	return $res;
}

function insertaTutoresAccionFormativa($datos,$codigoAccionFormativa,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM tutores_accion_formativa WHERE codigoAccionFormativa=$codigoAccionFormativa");
	}

	for($i=0;isset($datos['codigoTutor'.$i]);$i++){
		$res=$res && consultaBD("INSERT INTO tutores_accion_formativa VALUES(NULL,$codigoAccionFormativa,".$datos['codigoTutor'.$i].",
			'".$datos['horas'.$i]."');");
	}

	return $res;
}


function insertaFormadoresAccionFormativa($datos,$codigoAccionFormativa,$actualizacion=false){
	$res=true;

	if($actualizacion){
		$res=consultaBD("DELETE FROM formadores_af_presencial WHERE codigoAccionFormativa=$codigoAccionFormativa");
	}

	for($i=0;isset($datos['nifFormador'.$i]);$i++){
		$nifFormador=$datos['nifFormador'.$i];
		$nombreFormador=$datos['nombreFormador'.$i];
		$apellidosFormador1=$datos['primerApellidoFormador'.$i];
		$apellidosFormador2=$datos['segundoApellidoFormador'.$i];
		$telefonoFormador=$datos['telefonoFormador'.$i];
		$emailFormador=$datos['emailFormador'.$i];
		$horasFormador=$datos['horasFormador'.$i];

		$res=$res && consultaBD("INSERT INTO formadores_af_presencial VALUES(NULL,'$nifFormador','$nombreFormador','$apellidosFormador1','$apellidosFormador2','$telefonoFormador','$emailFormador','$horasFormador',$codigoAccionFormativa);");
	}

	return $res;
}


function listadoAccionesFormativas(){
	global $_CONFIG;

	$columnas=array('accion','modalidad','horas','horasPresencial','precio','precioCoste','accionFormativa','proveedor','razonSocial','acciones_formativas.activo','accionFormativa','proveedor','privado');
	$having=obtieneHavingAccionesFormativas($columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, accion, modalidad, horas, horasPresencial, precio, precioCoste, accionFormativa, proveedor, 
			razonSocial, acciones_formativas.activo, accionFormativa, proveedor, privado
			FROM acciones_formativas LEFT JOIN centros_formacion ON acciones_formativas.codigoCentroGestor=centros_formacion.codigo
			GROUP BY acciones_formativas.codigo
			$having";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){

		$fila=array(
			"<a href='gestion.php?codigo=".$datos['codigo']."'>".$datos['accion']."</a>",
			$datos['modalidad'],
			$datos['horas'],
			$datos['horasPresencial'],
			"<div class='pagination-right'>".formateaNumeroWeb($datos['precio']).' €</div>',
			"<div class='pagination-right'>".formateaNumeroWeb($datos['precioCoste']).' €</div>',
			$datos['accionFormativa'],
			$datos['proveedor'],
			$datos['razonSocial'],
			creaBotonDetalles("acciones-formativas/gestion.php?codigo=".$datos['codigo'],''),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function compruebaAccionFormativaExportadaListado($datos){
	$res='<div class="centro"><i class="icon-times-circle icon-danger iconoFactura" title="Sin exportar"></i></div>';
	if($datos['codigoXml']!=NULL && $datos['exportado']=='SI'){
		$res='<div class="centro"><i class="icon-check-circle icon-success iconoFactura" title="Exportada"></i></div>';
	}

	return $res;
}

function obtieneHavingAccionesFormativas($columnas){
	$res=obtieneWhereListado("HAVING 1=1",$columnas);

	if($res=="HAVING 1=1"){
		$res="HAVING acciones_formativas.activo='SI'";
	}

	return $res;
}

function gestionAccionFormativa(){
	operacionesAccionesFormativas();

	abreVentanaGestion('Gestión de Acciones Formativas','?','span4');
	$datos=compruebaDatos('acciones_formativas');

	campoSelectConsultaPlus('codigoCentroGestor','Centro gestor pla. teleformación',"SELECT codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial;",$datos,'selectpicker selectPlus show-tick obligatorio',"data-live-search='true'",'',0,true,'btn-primary',"<i class='icon-plus'></i> Nuevo");
	campoTexto('accion','Nombre acción',$datos,'span3 obligatorio');
	campoSelect('modalidad','Modalidad',array('','Distancia','Mixta','Presencial','Teleformación'),array('','DISTANCIA','MIXTA','PRESENCIAL','TELEFORMACIÓN'),$datos,'span3 selectpicker show-tick obligatorio');
	campoTexto('horas','Total de horas',$datos,'input-mini pagination-right obligatorio');
	campoTextoValidador('accionFormativa','Código AF',$datos,'input-mini pagination-right obligatorio','acciones_formativas','codigo');

	campoSelectGrupo($datos);
	campoRadio('mediosFormacion','Medios para formación',$datos,'ENTIDADEXTERNA',array('De la entidad externa que tiene encomendada la organización','De la entidad de formación acreditada o inscrita'),array('ENTIDADEXTERNA','ENTIDADACREDITADA'),true);

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelectAreaProfesional($datos);
	campoTexto('proveedor','Código de proveedor',$datos,'input-large obligatorio');
	campoTextoSimbolo('precioModulo','Precio Mód. Económico','€',formateaNumeroWeb($datos['precioModulo']));
	campoTextoSimbolo('precioCoste','Coste','€',formateaNumeroWeb($datos['precioCoste']));
	campoTextoSimbolo('precio','P.V.P.','€',formateaNumeroWeb($datos['precio']));
	campoRadio('tipoEspecifica','Tipo de Acción',$datos,'GENÉRICA',array('Genérica','Específica'),array('GENÉRICA','ESPECÍFICA'));
	campoRadio('nivelSuperior','Nivel de Formación',$datos,'BÁSICO',array('Básico','Superior'),array('BÁSICO','SUPERIOR'));
	campoRadio('privado','Tipo venta',$datos,'PRIVADA',array('Privada','Bonificada','Obsequio'),array('PRIVADA','BONIFICADA','OBSEQUIO'));
	campoRadio('activo','Activo',$datos,'SI');
	campoAccionFormativaExportada($datos);

	cierraColumnaCampos();
	abreColumnaCampos('sinFlotar');
	cierraColumnaCampos();
	abreColumnaCampos();

	echo '<div id="cajaPresencial" class="hide">
	<h3 class="apartadoFormulario">Formación presencial</h3>';
	creaTablaFormadores($datos);
	echo '</div>

	<div id="cajaMixta" class="hide">';
	campoTexto('horasPresencial','Horas presenciales',$datos,'input-mini pagination-right');
	campoSelectConsulta('codigoCentroPresencial','Centro presencial',"SELECT codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial;",$datos);
	echo '</div>

	<div id="cajaTeleformacion" class="hide">
	<h3 class="apartadoFormulario">Teleformación</h3>';
	campoTexto('horasTeleformacion','Horas teleformación',$datos,'input-mini pagination-right');
	campoTexto('url','Dirección URL',$datos,'span3');
	campoTexto('usuario','Usuario',$datos);
	campoTexto('clave','Clave',$datos);

	creaTablaTutores($datos);
	echo "</div>
	<h3 class='apartadoFormulario'> </h3>";

	areaTexto('objetivos','Objetivos',$datos,'areaInforme');
	areaTexto('contenidos','Contenidos',$datos,'areaInforme');
	areaTexto('temario','Contenido parte trasera del Diploma',$datos,'areaInforme');

	creaBotonPrevisualizaDiploma();

	cierraVentanaGestion('index.php',true);

	ventanaCentroFormacion();
	ventanaTutor();
}

function campoAccionFormativaExportada($datos){
	if($datos){
		if(compruebaAccionFormativaExportada($datos['codigo'])){
			$res="<i class='icon-check-circle icon-success'></i> Si";
		}
		else{
			$res="<i class='icon-times-circle icon-danger'></i> No";
		}

		campoDato('Exportada',$res);
	}
}

function campoSelectAreaProfesional($datos){
	$areasProfesionales=array(''=>'','ADGD'=>'ADMINISTRACIÓN Y AUDITORÍA','ADGG'=>'GESTIÓN DE LA INFORMACIÓN Y COMUNICACIÓN','ADGN'=>'FINANZAS Y SEGUROS','AFDA'=>'ACTIVIDADES FÍSICO DEPORTIVAS RECREATIVAS','AFDP'=>'PREVENCIÓN Y RECUPERACION','AGAJ'=>'JARDINERÍA','AGAN'=>'GANADERÍA','AGAR'=>'FORESTAL','AGAU'=>'AGRICULTURA','ARGA'=>'ACTIVIDADES Y TÉCNICAS GRÁFICAS ARTÍSTICAS','ARGC'=>'ENCUADERNACIÓN INDUSTRIAL','ARGG'=>'DISEÑO GRÁFICO Y MULTIMEDIA','ARGI'=>'IMPRESIÓN','ARGN'=>'EDICIÓN','ARGP'=>'PREIMPRESIÓN','ARGT'=>'TRANSFORMACION Y CONVERSIÓN EN INDUSTRIAS GRÁFICAS','ARTA'=>'ARTESANÍA TRADICIONAL','ARTB'=>'JOYERÍA Y ORFEBRERÍA','ARTG'=>'FABRICACIÓN Y MANTENIMIENTO DE INSTRUMENTOS MUSICALES','ARTN'=>'VIDRIO Y CERÁMICA ARTESANAL','ARTR'=>'RECUPERACIÓN, REPARACIÓN Y MANTENIMIENTO ARTÍSTICOS','ARTU'=>'ARTES ESCÉNICAS','COML'=>'LOGÍSTICA COMERCIAL Y GESTIÓN DEL TRANSPORTE','COMM'=>'MARKETING Y RELACIONES PÚBLICAS','COMT'=>'COMPRAVENTA','ELEE'=>'INSTALACIONES ELÉCTRICAS','ELEQ'=>'EQUIPOS ELECTRÓNICOS','ENAA'=>'AGUA','ENAE'=>'ENERGÍAS RENOVABLES','ENAL'=>'ENERGÍA ELÉCTRICA','ENAS'=>'GAS','EOCB'=>'ALBAÑILERÍA Y ACABADOS','EOCE'=>'ESTRUCTURAS','EOCJ'=>'COLOCACIÓN Y MONTAJE','EOCO'=>'PROYECTOS Y SEGUIMIENTO DE OBRAS','EOCQ'=>'MAQUINARIA DE CONSTRUCCIÓN','FCOA'=>'ASPECTOS MEDIOAMBIENTALES','FCOE'=>'LENGUAS EXTRANJERAS','FCOL'=>'LENGUAS Y DIALECTOS ESPAÑOLES','FCOV'=>'COMPETENCIAS CLAVE','FMEC'=>'CONSTRUCCIONES METÁLICAS','FMEF'=>'FUNDICIÓN','FMEH'=>'OPERACIONES MECÁNICAS','FMEM'=>'PRODUCCIÓN MECÁNICA','HOTA'=>'ALOJAMIENTO','HOTJ'=>'JUEGOS DE AZAR','HOTR'=>'RESTAURACIÓN','HOTT'=>'TURISMO','HOTU'=>'AGROTURISMO','IEXD'=>'PIEDRA NATURAL','IEXM'=>'MINERÍA','IFCD'=>'DESARROLLO','IFCM'=>'COMUNICACIONES','IFCT'=>'SISTEMAS Y TELEMÁTICA','IMAI'=>'MONTAJE Y MANTENIMIENTO DE INSTALACIONES','IMAQ'=>'MAQUINARIA Y EQUIPO INDUSTRIAL','IMAR'=>'FRÍO Y CLIMATIZACIÓN','IMPE'=>'ESTÉTICA','IMPQ'=>'PELUQUERÍA','IMSE'=>'ESPECTACULOS EN VIVO','IMST'=>'PRODUCCIONES FOTOGRÁFICAS','IMSV'=>'PRODUCCIÓN AUDIOVISUAL','INAD'=>'ALIMENTOS DIVERSOS','INAE'=>'LÁCTEOS','INAF'=>'PANADERÍA, PASTELERÍA, CONFITERÍA Y MOLINERÍA','INAH'=>'BEBIDAS','INAI'=>'CÁRNICAS','INAJ'=>'PRODUCTOS DE LA PESCA','INAK'=>'ACEITES Y GRASAS','INAV'=>'CONSERVAS VEGETALES','MAMA'=>'TRANSFORMACIÓN MADERA Y CORCHO','MAMB'=>'INSTALACIÓN Y AMUEBLAMIENTO','MAMD'=>'PRODUCCION CARPINTERÍA Y MUEBLE','MAPB'=>'BUCEO','MAPN'=>'PESCA Y NAVEGACION','MAPU'=>'ACUICULTURA','QUIA'=>'ANÁLISIS Y CONTROL','QUIE'=>'PROCESO QUÍMICO','QUIM'=>'FARMAQUÍMICA','QUIO'=>'PASTA, PAPEL Y CARTÓN','QUIT'=>'TRANSFORMACIÓN DE POLÍMEROS','SANP'=>'SERVICIOS Y PRODUCTOS SANITARIOS','SANS'=>'SOPORTE Y AYUDA AL DIAGNÓSTICO','SANT'=>'ATENCIÓN SANITARIA','SEAD'=>'SEGURIDAD Y PREVENCIÓN','SEAG'=>'GESTIÓN AMBIENTAL','SSCB'=>'ACTIVIDADES CULTURALES Y RECREATIVAS','SSCE'=>'FORMACIÓN Y EDUCACIÓN','SSCG'=>'ATENCIÓN SOCIAL','SSCI'=>'SERVICIOS AL CONSUMIDOR','TCPC'=>'CALZADO','TCPF'=>'CONFECCIÓN EN TEXTIL Y PIEL','TCPN'=>'ENNOBLECIMIENTO DE MATERIAS TEXTILES Y PIELES','TCPP'=>'PRODUCCIÓN DE HILOS Y TEJIDOS','TMVB'=>'FERROCARRIL Y CABLE','TMVG'=>'ELECTROMECÁNICA DE VEHÍCULOS','TMVI'=>'CONDUCCION DE VEHÍCULOS POR CARRETERA','TMVL'=>'CARROCERÍA DE VEHÍCULOS','TMVO'=>'AERONÁUTICA','TMVU'=>'NÁUTICA','VICF'=>'FABRICACIÓN CERÁMICA','VICI'=>'VIDRIO INDUSTRIAL','FCOI'=>'Informática complementaria','FCOM'=>'Manipulación alimentaria','FCOO'=>'Orientación laboral','FCOS'=>'Seguridad y salud laboral','ELEM'=>'MÁQUINAS ELECTROMECÁNICAS','ELES'=>'INSTALACIONES DE TELECOMUNICACIÓN','ENAC'=>'EFICIENCIA ENERGÉTICA','FMEA'=>'CONSTRUCCIONES AREONÁUTICAS');
	asort($areasProfesionales);//Para ordenar los valores del array (paso de hacerlo a mano ¬.¬)

	campoSelect('areaProfesional','Área profesional',array_values($areasProfesionales),array_keys($areasProfesionales),$datos);
}

function compruebaAccionFormativaExportada($codigoAccion){
	$res=false;

	$xml=consultaBD("SELECT xml_accion.codigo FROM xml_accion INNER JOIN acciones_formativas_xml_accion ON xml_accion.codigo=acciones_formativas_xml_accion.codigoXmlAccion WHERE codigoAccionFormativa=$codigoAccion AND exportado='SI';",true,true);
	if($xml){
		$res=true;
	}

	return $res;
}

function ventanaCentroFormacion(){
	abreVentanaModal('Centro gestor de la plataforma de teleformación');

	campoTexto('razonSocial','Razón social',false,'span3');
	campoTextoValidador('cif','CIF',false,'input-small validaCIF','centros_formacion');
	campoTexto('responsable','Responsable',false,'span3');
	campoTextoSimboloValidador('telefono','Teléfono','<i class="icon-phone"></i>',false,'input-small','centros_formacion');
	campoTextoSimboloValidador('email','eMail','<i class="icon-envelope"></i>',false,'input-large','centros_formacion');
	campoTexto('domicilio','Domicilio',false,'span3');
	campoTexto('cp','Código Postal',false,'input-mini pagination-right');
	campoTexto('localidad','Localidad');

	cierraVentanaModal();
}


function ventanaTutor(){
	abreVentanaModal('Tutores','cajaTutor');

	campoSelectConsulta('codigoCentroFormacionTutor','Centro de formación',"SELECT codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial;");
	campoTextoValidador('dniTutor','DNI',false,'input-small validaDNI','tutores');
	campoTexto('nombreTutor','Nombre');
	campoTexto('apellido1Tutor','Primer apellido');
	campoTexto('apellido2Tutor','Segundo apellido');

	cierraVentanaModal('registraTutor');
}


function creaCentroFormacion(){
	$res='fallo';

	$codigoCentro=insertaDatos('centros_formacion');
	if($codigoCentro){
		$res=$codigoCentro;
	}

	echo $res;
}

function creaTutor(){
	$res='fallo';

	$datos=arrayFormulario();
	$fechaActualizacion=fechaBD();

	conexionBD();

	$consulta=consultaBD("INSERT INTO tutores(codigo,codigoCentroFormacion,dni,nombre,apellido1,apellido2,fechaActualizacionTutor) VALUES(NULL,
						'".$datos['codigoCentroFormacion']."','".$datos['dni']."','".$datos['nombre']."','".$datos['apellido1']."',
						'".$datos['apellido2']."','$fechaActualizacion');");

	if($consulta){
		$res=mysql_insert_id();
	}

	cierraBD();

	echo $res;
}

function filtroAccionesFormativas(){
	abreCajaBusqueda();
	abreColumnaCampos();

	campoTexto(0,'Nombre acción',false,'span3');
	campoSelect(1,'Modalidad',array('','Presencial','Teleformación','Mixta'),array('','PRESENCIAL','TELEFORMACIÓN','MIXTA'));
	campoTexto(2,'Horas',false,'input-mini pagination-right');
	campoTexto(6,'Código AF',false,'input-mini pagination-right');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoTexto(7,'Código de proveedor','','input-small');
	campoSelectConsulta(8,'Centro impartidor',"SELECT razonSocial AS codigo, razonSocial AS texto FROM centros_formacion WHERE activo='SI' ORDER BY razonSocial;");
	campoSelect(13,'Tipo venta',array('','Privada','Bonificada','Obsequio'),array('','PRIVADA','MODIFICADA','OBSEQUIO'),false,'selectpicker show-tick span2');
	campoSelectSiNoFiltro(10,'Activo');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

function campoSelectGrupo($datos){
	$valores=array('','001-00', '002-00', '003-00', '003-01', '003-02', '003-03', '004-00', '004-01', '005-00', '006-00', '007-00', '008-00', '008-01', '008-02', '008-03', '009-00', '009-01', '009-02', '009-03', '010-00', '010-01', '010-02', '011-00', '013-00', '014-00', '015-00', '016-00', '016-01', '017-00', '017-01', '017-02', '017-03', '017-04', '017-05', '017-06', '017-07', '017-08', '018-00', '018-01', '019-00', '019-01', '019-02', '019-03', '019-04', '024-00', '024-01', '024-02', '024-03', '024-04', '024-05', '025-00', '025-01', '025-02', '026-01', '026-02', '026-03', '026-04', '026-05', '026-06', '026-07', '026-08', '026-09', '026-10', '026-99', '027-00', '027-01', '027-02', '028-00', '028-01', '028-02', '029-00', '030-00', '030-01', '031-00', '032-00', '032-01', '032-02', '032-03', '032-04', '032-05', '034-01', '034-02', '034-03', '034-04', '034-05', '034-06', '034-07', '034-08', '034-09', '034-10', '034-11', '034-12', '034-13', '034-14', '034-15', '034-16', '034-17', '034-18', '034-19', '034-20', '034-21', '034-22', '034-23', '034-24', '034-25', '034-26', '034-27', '034-28', '034-29', '034-30', '034-31', '034-32', '034-33', '034-34', '034-35', '034-36', '034-37', '034-38', '034-39', '034-40', '034-41', '034-42', '034-43', '034-44', '034-45', '034-46', '034-47', '034-48', '034-49', '034-50', '034-51', '034-52', '034-53', '034-54', '034-55', '034-56', '034-57', '035-01', '035-02', '035-03', '035-04', '035-05', '035-06', '035-07', '035-08', '035-09', '035-10', '035-11', '035-12', '035-13', '035-14', '035-15', '035-16', '035-17', '035-18', '035-19', '035-20', '035-21', '035-22', '035-23', '035-24', '035-25', '035-26', '035-27', '035-28', '035-29', '035-30', '035-31', '035-32', '035-33', '035-34', '035-35', '035-36', '035-37', '035-38', '035-39', '035-40', '035-41', '035-42', '035-43', '035-44', '035-45', '035-46', '035-47', '035-48', '035-49', '035-50', '035-51', '035-52', '035-53', '035-54', '035-55', '035-56', '035-57', '036-00', '037-00', '038-00', '038-01', '038-02', '039-00', '040-00', '041-00', '042-00', '042-01', '043-00', '043-01', '043-02', '043-03', '043-99', '044-00', '044-01', '044-02', '044-03', '044-04', '044-05', '044-06', '044-07', '044-08', '045-00', '046-00', '046-01', '046-02', '046-03', '046-04', '047-01', '047-02', '047-03', '047-04', '047-99', '048-00', '048-01', '048-02', '049-00', '050-00', '051-00', '051-01', '051-02', '051-03', '051-04', '052-00', '052-01', '054-00', '054-01', '054-02', '055-00', '056-00', '056-01', '057-00', '058-00', '058-01', '059-00', '060-00', '061-00', '061-01', '061-02', '061-03', '062-00', '063-00', '063-01', '063-02', '064-00', '065-00', '066-00', '067-00', '067-01', '068-00', '068-01', '068-02', '068-03', '068-04', '068-05', '068-06', '069-00', '069-01', '069-02', '069-03', '070-00', '071-00', '072-00', '073-00', '074-00', '074-01', '074-02', '074-03', '075-00', '078-00', '079-00', '079-01', '079-02', '080-00', '080-01', '080-02', '080-03', '087-00', '087-01', '087-02', '087-03', '087-04', '087-05', '087-06', '087-07', '087-08', '087-09', '087-10', '087-11', '087-12', '087-13', '087-14', '087-15', '087-16', '088-00', '089-00', '090-00', '090-01', '090-02', '090-03', '090-04', '090-05', '090-06', '091-00', '091-01', '091-02', '091-03', '091-04', '091-05', '091-06', '091-07', '091-08', '092-00', '093-00', '094-00', '095-00', '096-00', '096-01', '096-02', '096-03', '096-04', '096-05', '097-00', '097-01', '097-02', '097-03', '097-04', '097-05', '101-00', '102-00', '102-01', '102-02', '102-03', '102-04', '102-05', '102-06', '103-00', '103-01', '103-02', '103-03', '104-00', '104-01', '104-02', '104-03', '104-04', '104-05', '105-00', '105-01', '105-02', '105-03', '105-04', '105-05', '105-06', '105-07', '105-08', '105-09', '105-10', '105-11', '105-12', '106-00', '106-01', '106-02', '106-03', '106-04', '106-05', '106-06', '107-00', '107-01', '107-02', '107-03', '107-04', '107-05', '108-00', '109-01', '109-02', '109-03', '109-04', '109-05', '109-06', '109-07', '109-08', '109-09', '109-10', '109-11', '109-12', '109-13', '109-14', '109-15', '109-16', '109-17', '109-18', '109-19', '109-20', '109-21', '109-22', '109-23', '109-24', '109-25', '109-26', '109-27', '109-28', '109-29', '109-30', '109-31', '109-32', '109-33', '109-34', '109-35', '110-00', '111-00', '112-00', '114-00', '114-01', '114-02', '114-03', '114-04', '114-05', '115-00', '116-00', '117-00', '117-01', '117-02', '117-03', '117-04', '117-05', '117-06', '117-07', '118-01', '118-02', '118-03', '118-04', '118-05', '118-06', '118-07', '118-08', '118-09', '118-10', '118-11', '118-12', '118-13', '118-14', '118-15', '118-16', '118-17', '118-18', '118-19', '118-20', '118-21', '118-22', '118-23', '118-24', '118-25', '118-26', '118-27', '118-28', '118-29', '118-99', '119-00', '119-01', '119-02', '119-03', '119-04', '119-05', '119-06', '119-07', '119-08', '119-09', '119-10', '119-99', '120-00', '120-01', '120-02', '120-03', '120-04', '120-05', '121-00', '122-00', '122-01', '122-02', '122-03', '122-04', '122-05', '122-06', '122-07', '122-08', '122-09', '122-10', '122-11', '122-12', '122-13', '122-14', '122-15', '122-16', '122-17', '122-18', '122-19', '122-20', '122-21', '122-22', '122-23', '122-24', '122-25', '123-00', '123-01', '123-02', '124-00', '124-01', '124-02', '124-03', '124-04', '124-05', '125-00', '127-01', '127-02', '127-03', '127-04', '127-05', '127-06', '127-07', '127-99', '129-00', '133-00', '135-01', '135-02', '135-03', '135-04', '135-05', '135-06', '135-07', '135-08', '135-09', '135-10', '135-11', '135-12', '135-13', '135-14', '135-15', '135-16', '135-17', '135-18', '135-19', '135-20', '135-21', '135-22', '135-23', '135-24', '135-25', '135-26', '135-27', '135-99', '136-00', '137-01', '137-02', '137-03', '137-04', '137-05', '137-06', '137-07', '137-08', '137-09', '137-10', '137-11', '137-12', '137-13', '137-14', '137-15', '137-16', '137-17', '137-18', '137-19', '137-20', '137-21', '137-22', '137-23', '137-24', '137-25', '137-26', '137-27', '137-28', '137-29', '137-30', '137-31', '137-32', '137-33', '137-34', '137-35', '137-36', '137-37', '137-38', '137-99', '138-01', '138-02', '138-03', '138-04', '138-05', '138-06', '138-07', '138-08', '138-09', '138-10', '138-99', '139-01', '139-02', '139-03', '139-04', '139-05', '139-06', '139-99', '140-01', '140-02', '140-03', '140-04', '140-05', '140-06', '140-07', '140-08', '140-09', '140-10', '140-11', '140-12', '140-13', '140-14', '140-15', '140-16', '140-17', '140-18', '140-19', '140-20', '140-21', '140-22', '140-23', '140-24', '140-25', '140-26', '140-99', '141-01', '141-02', '141-03', '141-04', '141-05', '141-06', '141-07', '141-08', '141-09', '141-10', '141-11', '141-12', '141-13', '141-14', '141-15', '141-16', '141-17', '141-18', '141-19', '141-20', '141-21', '141-22', '141-23', '141-99', '142-01', '142-02', '142-03', '142-04', '142-05', '142-06', '142-07', '142-08', '142-09', '142-10', '142-11', '142-12', '142-99', '143-01', '143-02', '143-03', '143-04', '143-05', '143-06', '143-07', '143-08', '143-09', '143-10', '143-11', '143-12', '143-13', '143-14', '143-15', '143-16', '143-17', '143-18', '143-19', '143-20', '143-21', '143-22', '143-23', '143-24', '143-25', '143-26', '143-27', '143-28', '143-29', '143-30', '143-31', '143-32', '143-33', '143-34', '143-35', '143-36', '143-37', '143-38', '143-39', '143-40', '143-41', '143-42', '143-43', '143-44', '143-45', '143-46', '143-47', '143-99', '144-01', '144-02', '144-03', '144-04', '144-05', '144-06', '144-07', '144-08', '144-09', '144-10', '144-11', '144-12', '144-13', '144-14', '144-15', '144-16', '144-17', '144-18', '144-99', '145-01', '145-02', '145-03', '145-04', '145-05', '145-06', '145-07', '145-08', '145-09', '145-10', '145-11', '145-12', '145-13', '145-14', '145-15', '145-16', '145-17', '145-18', '145-19', '145-20', '145-99', '146-01', '146-02', '146-03', '146-04', '146-05', '146-06', '146-07', '146-08', '146-09', '146-10', '146-11', '146-12', '146-13', '146-14', '146-15', '146-16', '146-17', '146-18', '146-19', '146-20', '146-99', '147-01', '147-02', '147-03', '147-04', '147-05', '147-06', '147-07', '147-08', '147-09', '147-10', '147-11', '147-12', '147-13', '147-99', '148-01', '148-02', '148-03', '148-04', '148-05', '148-06', '148-07', '148-08', '148-99', '149-00', '149-01', '149-02', '149-03', '149-04', '150-00', '150-01', '151-00', '151-01', '151-02', '151-03', '151-04', '151-99', '152-00', '152-01', '153-00', '154-00', '154-01', '154-02', '155-00', '157-00', '158-00', '159-00', '159-01', '160-00', '160-01', '160-02', '160-03', '161-00', '161-01', '161-02', '161-03', '162-01', '162-02', '162-03', '162-04', '162-05', '162-06', '162-07', '162-08', '162-09', '162-10', '162-99', '163-00', '163-01', '163-02', '163-03', '163-04', '163-05', '163-06', '163-07', '163-08', '163-99', '164-00', '164-01', '164-02', '165-00', '165-01', '165-02', '165-03', '165-04', '165-05', '165-06', '165-07', '165-08', '165-99', '166-01', '166-02', '166-03', '166-04', '166-05', '166-06', '166-07', '166-08', '166-09', '166-10', '166-11', '166-99', '167-00', '168-00', '169-00', '170-00', '171-01', '171-02', '171-03', '171-04', '171-05', '171-06', '171-99', '172-00', '173-00', '174-01', '174-02', '174-03', '174-04', '174-05', '174-06', '174-07', '174-08', '174-09', '174-10', '174-11', '174-12', '174-13', '174-14', '174-15', '174-16', '174-17', '174-18', '174-19', '174-20', '174-99');
	$nombres=array('','001-00 Actualización en docencia en general', '002-00 Administración de personal en general', '003-00 Secretariado y otros trabajos auxiliares de oficina en general', '003-01 Trabajos auxiliares de Oficina', '003-02 Atención telefónica y recepción', '003-03 Secretariado', '004-00 Almacenaje, Stocks y Envíos en general', '004-01 Operación de carretillas', '005-00 Análisis de riesgos-banca en general', '006-00 Análisis y Control de Costes en general', '007-00 Análisis y Control Financiero en general', '008-00 Análisis y ensayos de laboratorio en general', '008-01 Trabajos auxiliares en  laboratorios de química industrial', '008-02 Análisis en laboratorios de química industrial', '008-03 Trabajos auxiliares en laboratorios de industrias alimentarias', '009-00 Atención al Cliente/Calidad Servicio en general', '009-01 Información al cliente', '009-02 Calidad de servicio', '009-03 Atención al cliente mediante TICs', '010-00 Atención al paciente o usuario de servicios sanitarios en general', '010-01 Atención al paciente o usuario hospitalario', '010-02 Atención extrahospitalaria: farmacia, ortopedia, etc.', '011-00 Auditoría ambiental en general', '013-00 Auditoría económico financiera en general', '014-00 Auditoría informática en general', '015-00 Autómatas programables y robótica en general', '016-00 Automatismos industriales en general', '016-01 Instalación de automatismos', '017-00 Cuidados sanitarios auxiliares en general', '017-01 Movilización y traslado de pacientes', '017-02 Cuidados auxiliares de transporte sanitario', '017-03 Cuidados auxiliares de enfermería hospitalaria', '017-04 Cuidados auxiliares en geriatría', '017-05 Cuidados auxiliares en salud mental y toxicomanías', '017-06 Cuidados auxiliares en rehabilitación', '017-07 Higiene dental', '017-08 Prótesis dental', '018-00 Bibliotecas, Archivos y Documentación en general', '018-01 Documentación en medios de comunicación', '019-00 Calderería industrial en general', '019-01 Tubería industrial', '019-02 Trabajos auxiliares de calderería', '019-03 Calderería- Tubería', '019-04 Técnicas de calderería', '024-00 Hostelería-Servicio de comidas y bebidas en general', '024-01 Jefatura de salas de restauración', '024-02 Servicio en restaurantes y bares', '024-03 Presentación de vinos y bebidas', '024-04 Presentación de Buffetes', '024-05 Coctelería', '025-00 Servicios de Juegos de Azar en general', '025-01 Gestión de apuestas en casinos y mesas de juego', '025-02 Operación de apuestas', '026-01 Estadística', '026-02 Bioquímica', '026-03 Edafología', '026-04 Botánica', '026-05 Economía', '026-06 Geología', '026-07 Matemáticas', '026-08 Química', '026-09 Física', '026-10 Psicología', '026-99 Otras Ciencias aplicadas', '027-00 Cobros e Impagos en general', '027-01 Análisis de riesgos y gestión crediticia', '027-02 Gestión de cobros y reclamaciones', '028-00 Hostelería- cocina en general', '028-01 Jefatura de cocinas', '028-02 Repostería-pastelería', '029-00 Comercio Exterior en general', '030-00 Compras y Aprovisionamientos en general', '030-01 Negociación y otras técnicas de relación con proveedores', '031-00 Comunicaciones Informáticas en general', '032-00 Conducción y pilotaje de vehículos, aeronaves y trenes en general', '032-01 Conducción de vehículos ligeros', '032-02 Conducción de camiones pesados', '032-03 Conducción de autobuses', '032-04 Pilotaje de aeronaves', '032-05 Conducción de ferrocarriles y trenes', '034-01 Conocimiento del producto: Agricultura, ganadería, caza y actividades de los servicios relacionados con las mismas', '034-02 Conocimiento del producto: Selvicultura, explotación forestal y actividades de los servicios relacionados con las mismas', '034-03 Conocimiento del producto: Pesca, acuicultura y actividades de los servicios relacionados con las mismas', '034-04 Conocimiento del producto: Extracción y aglomeración de antracita, hulla, lignito y turba', '034-05 Conocimiento del producto: Extracción de crudos de petróleo y gas natural; actividades de los servicios relacionados con las explotaciones petrolíferas y de gas, excepto actividades de prospección', '034-06 Conocimiento del producto: Extracción de minerales de uranio y torio', '034-07 Conocimiento del producto: Extracción de minerales metálicos', '034-08 Conocimiento del producto: Extracción de minerales no metálicos ni energéticos', '034-09 Conocimiento del producto: Industria de productos alimenticios y bebidas', '034-10 Conocimiento del producto: Industria del tabaco', '034-11 Conocimiento del producto: Industria textil', '034-12 Conocimiento del producto: Industria de la confección y de la peletería', '034-13 Conocimiento del producto: Preparación, curtido y acabado del cuero; fabricación de artículos de marroquinería y viaje; artículos de guarnicionería, talabartería y zapatería', '034-14 Conocimiento del producto: Industria de la madera y del corcho, excepto muebles; cestería y espartería', '034-15 Conocimiento del producto: Industria del papel', '034-16 Conocimiento del producto: Edición, artes gráficas y reproducción de soportes grabados', '034-17 Conocimiento del producto: Industria química', '034-18 Conocimiento del producto: Fabricación de productos de caucho y materias plásticas', '034-19 Conocimiento del producto: Metalurgia', '034-20 Conocimiento del producto: Fabricación de productos metálicos, excepto maquinaria y equipo', '034-21 Conocimiento del producto: Industria de la construcción de maquinaria y equipo mecánico', '034-22 Conocimiento del producto: Fabricación de máquinas de oficina y equipos informáticos', '034-23 Conocimiento del producto: Fabricación de maquinaria y material eléctrico', '034-24 Conocimiento del producto: Fabricación de material electrónico; fabricación de equipo y aparatos de radio, televisión y comunicaciones', '034-25 Conocimiento del producto: Fabricación de equipo e instrumentos médico-quirúrgicos, de precisión, óptica y relojería', '034-26 Conocimiento del producto: Fabricación de vehículos de motor, remolques y semirremolques', '034-27 Conocimiento del producto: Fabricación de otro material de transporte', '034-28 Conocimiento del producto: Coquerías, refino de petróleo y tratamiento de combustibles nucleares', '034-29 Conocimiento del producto: Fabricación de otros productos minerales no metálicos', '034-30 Conocimiento del producto: Fabricación de muebles', '034-31 Conocimiento del producto: Reciclaje', '034-32 Conocimiento del producto: Producción y distribución de energía eléctrica, gas, vapor y agua caliente', '034-33 Conocimiento del producto: Captación, depuración y distribución de agua', '034-34 Conocimiento del producto: Construcción', '034-35 Conocimiento del producto: Venta, mantenimiento y reparación de vehículos de motor, motocicletas y ciclomotores; venta al por menor de combustible para vehículos de motor', '034-36 Conocimiento del producto: Comercio al por mayor e intermediarios del comercio, excepto vehículos de motor y motocicletas', '034-37 Conocimiento del producto: Comercio al por menor, excepto el comercio de vehículos de motor, motocicletas y ciclomotores; reparación de efectos personales y enseres domésticos', '034-38 Conocimiento del producto: Hostelería', '034-39 Conocimiento del producto: Transporte terrestre; transporte por tuberías', '034-40 Conocimiento del producto: Transporte marítimo, de cabotaje y por vías de navegación interiores', '034-41 Conocimiento del producto: Transporte aéreo y espacial', '034-42 Conocimiento del producto: Actividades anexas a los transportes; actividades de agencias de viaje', '034-43 Conocimiento del producto: Correos y telecomunicaciones', '034-44 Conocimiento del producto: Intermediación financiera, excepto seguros y planes de pensiones', '034-45 Conocimiento del producto: Actividades auxiliares ala intermediación financiera', '034-46 Conocimiento del producto: Seguros y planes de pensiones, excepto seguridad social', '034-47 Conocimiento del producto: Actividades inmobiliarias', '034-48 Conocimiento del producto: Alquiler de maquinaria y equipo sin operario, de efectos personales y enseres domésticos', '034-49 Conocimiento del producto: Actividades informáticas', '034-50 Conocimiento del producto: Investigación y desarrollo', '034-51 Conocimiento del producto: Otras actividades empresariales', '034-52 Conocimiento del producto: Educación', '034-53 Conocimiento del producto: Actividades sanitarias y veterinarias, servicios sociales', '034-54 Conocimiento del producto: Actividades de saneamiento público', '034-55 Conocimiento del producto: Actividades asociativas', '034-56 Conocimiento del producto: Actividades recreativas, culturales y deportivas', '034-57 Conocimiento del producto: Actividades diversas de servicios personales', '035-01 Conocimiento del sector: Agricultura, ganadería, caza', '035-02 Conocimiento del sector: Selvicultura, explotación forestal y actividades de los servicios relacionados con las mismas', '035-03 Conocimiento del sector: Pesca, acuicultura y actividades de los servicios relacionados con las mismas', '035-04 Conocimiento del sector: Extracción y aglomeración de antracita, hulla, lignito y turba', '035-05 Conocimiento del sector: Extracción de crudos de petróleo y gas natural; actividades de los servicios relacionados con las explotaciones petrolíferas y de gas, excepto actividades de prospección', '035-06 Conocimiento del sector: Extracción de minerales de uranio y torio', '035-07 Conocimiento del sector: Extracción de minerales metálicos', '035-08 Conocimiento del sector: Extracción de minerales no metálicos ni energéticos', '035-09 Conocimiento del sector: Industria de productos alimenticios y bebidas', '035-10 Conocimiento del sector: Industria del tabaco', '035-11 Conocimiento del sector: Industria textil', '035-12 Conocimiento del sector: Industria de la confección y de la peletería', '035-13 Conocimiento del sector: Preparación, curtido y acabado del cuero; fabricación de artículos de marroquinería y viaje; artículos de guarnicionería, talabartería y zapatería', '035-14 Conocimiento del sector: Industria de la madera y del corcho, excepto muebles; cestería y espartería', '035-15 Conocimiento del sector: Industria del papel', '035-16 Conocimiento del sector: Edición, artes gráficas y reproducción de soportes grabados', '035-17 Conocimiento del sector: Industria química', '035-18 Conocimiento del sector: Fabricación de productos de caucho y materias plásticas', '035-19 Conocimiento del sector: Metalurgia', '035-20 Conocimiento del sector: Fabricación de productos metálicos, excepto maquinaria y equipo', '035-21 Conocimiento del sector: Industria de la construcción de maquinaria y equipo mecánico', '035-22 Conocimiento del sector: Fabricación de máquinas de oficina y equipos informáticos', '035-23 Conocimiento del sector: Fabricación de maquinaria y material eléctrico', '035-24 Conocimiento del sector: Fabricación de material electrónico; fabricación de equipo y aparatos de radio, televisión y comunicaciones', '035-25 Conocimiento del sector: Fabricación de equipo e instrumentos médico-quirúrgicos, de precisión, óptica y relojería', '035-26 Conocimiento del sector: Fabricación de vehículos de motor, remolques y semirremolques', '035-27 Conocimiento del sector: Fabricación de otro material de transporte', '035-28 Conocimiento del sector: Coquerías, refino de petróleo y tratamiento de combustibles nucleares', '035-29 Conocimiento del sector: Fabricación de otros productos minerales no metálicos', '035-30 Conocimiento del sector: Fabricación de muebles', '035-31 Conocimiento del sector: Reciclaje', '035-32 Conocimiento del sector: Producción y distribución de energía eléctrica, gas, vapor y agua caliente', '035-33 Conocimiento del sector: Captación, depuración y distribución de agua', '035-34 Conocimiento del sector: Construcción', '035-35 Conocimiento del sector: Venta, mantenimiento y reparación de vehículos de motor, motocicletas y ciclomotores; venta al por menor de combustible para vehículos de motor', '035-36 Conocimiento del sector: Comercio al por mayor e intermediarios del comercio, excepto vehículos de motor y motocicletas', '035-37 Conocimiento del sector: Comercio al por menor, excepto el comercio de vehículos de motor, motocicletas y ciclomotores; reparación de efectos personales y enseres domésticos', '035-38 Conocimiento del sector: Hostelería', '035-39 Conocimiento del sector: Transporte terrestre; transporte por tuberías', '035-40 Conocimiento del sector: Transporte marítimo, de cabotaje y por vías de navegación interiores', '035-41 Conocimiento del sector: Transporte aéreo y espacial', '035-42 Conocimiento del sector: Actividades anexas a los transportes; actividades de agencias de viaje', '035-43 Conocimiento del sector: Correos y telecomunicaciones', '035-44 Conocimiento del sector: Intermediación financiera, excepto seguros y planes de pensiones', '035-45 Conocimiento del sector: Actividades auxiliares ala intermediación financiera', '035-46 Conocimiento del sector: Seguros y planes de pensiones, excepto seguridad social', '035-47 Conocimiento del sector: Actividades inmobiliarias', '035-48 Conocimiento del sector: Alquiler de maquinaria y equipo sin operario, de efectos personales y enseres domésticos', '035-49 Conocimiento del sector: Actividades informáticas', '035-50 Conocimiento del sector: Investigación y desarrollo', '035-51 Conocimiento del sector: Otras actividades empresariales', '035-52 Conocimiento del sector: Educación', '035-53 Conocimiento del sector: Actividades sanitarias y veterinarias, servicios sociales', '035-54 Conocimiento del sector: Actividades de saneamiento público', '035-55 Conocimiento del sector: Actividades asociativas', '035-56 Conocimiento del sector: Actividades recreativas, culturales y deportivas', '035-57 Conocimiento del sector: Actividades diversas de servicios personales', '036-00 Contabilidad en general', '037-00 Control de Gestión en general', '038-00 Control numérico en general', '038-01 Preparación de máquinas herramienta y Control Numérico (CNC)', '038-02 Programación de máquinas herramienta con Control Numérico (CNC)', '039-00 Diseño asistido por ordenador en general', '040-00 Diseño Gráfico Informatizado en general', '041-00 Diseño industrial en general', '042-00 Atención y venta en establecimientos comerciales en general', '042-01 Cobro y atención en Caja', '043-00 Enfermería en general', '043-01 Enfermería de atención primaria', '043-02 Enfermería de hospitales', '043-03 Enfermería de salud mental y toxicomanías', '043-99 Enfermería- otros ámbitos', '044-00 Equipos técnicos- Información y manifestaciones artísticas en general', '044-01 Operación de cámaras', '044-02 Edición y montaje de imágenes', '044-03 Técnicas de sonido', '044-04 Operación de equipos en estación de radio y televisión', '044-05 Proyecciones cinematográficas', '044-06 Fotografía', '044-07 Laboratorio de imagen', '044-08 Iluminación', '045-00 Escaparatismo en general', '046-00 Escenografía y ambientación artística en general', '046-01 Decoración de escenarios', '046-02 Vestuario', '046-03 Caracterización', '046-04 Regiduría de escena', '047-01 Traumatología', '047-02 Ginecología', '047-03 Cirugía', '047-04 Neurología', '047-99 Otras especialidades médicas', '048-00 Procesos productivos industria pesada: Fabricación de estructuras metálicas en general', '048-01 Carpintería metálica y de PVC', '048-02 Montaje de estructuras metálicas', '049-00 Estudios de mercado en general', '050-00 Finanzas para no Financieros en general', '051-00 Explotación forestal en general', '051-01 Trabajos forestales', '051-02 Manipulación de motosierras', '051-03 Explotación del alcornoque', '051-04 Explotación cinegética', '052-00 Formación de formadores en general', '052-01 Aplicación de TICs a la formación', '054-00 Montaje e instalación de frío industrial en general', '054-01 Montaje e instalación de cámaras frigoríficas', '054-02 Instalación y mantenimiento de aire acondicionado', '055-00 Gestión administrativa en general', '056-00 Gestión ambiental en general', '056-01 Sensibilización hacia el medio ambiente', '057-00 Dirección y gestión bancaria en general', '058-00 Gestión comercial en general', '058-01 Gestión en Agencia Comercial', '059-00 Gestión de almacén y/o distribución en general', '060-00 Gestión de empresas de Ec. Social en general', '061-00 Gestión de flotas en general', '061-01 Gestión de flotas- transporte por carretera', '061-02 Gestión de flotas- transporte marítimo', '061-03 Gestión de flotas-Transporte aéreo', '062-00 Gestión de la formación en general', '063-00 Gestión de obra en general', '063-01 Supervisión de ejecución de obra de edificación', '063-02 Supervisión de ejecución de obra civil', '064-00 Gestión de la producción en general', '065-00 Gestión de proyectos (no de proyectos informáticos) en general', '066-00 Gestión de proyectos informáticos en general', '067-00 Gestión de Pymes en general', '067-01 Gerencia de Pequeño Comercio', '068-00 Gestión de recursos humanos en general', '068-01 Selección y desarrollo de Recursos Humanos', '068-02 Habilidades directivas', '068-03 Habilidades personales e interpersonales en el entorno laboral', '068-04 Comunicación Interna', '068-05 Recursos humanos: Negociación colectiva', '068-06 Conocimiento de la empresa (acogida), cultura de empresa, cambio de cultura', '069-00 Gestión del mantenimiento en general', '069-01 Técnicas de organización de instalaciones y mantenimiento de edificios y equipamientos urbanos', '069-02 Técnicas de organización del mantenimiento de equipos electromecánicos de uso no industrial', '069-03 Técnicas de organización del mantenimiento industrial', '070-00 Gestión económico financiera en general', '071-00 Gestión de grandes empresas y redes empresariales en general', '072-00 Gestión fiscal en general', '073-00 Gestión hospitalaria en general', '074-00 Gestión hotelera en general', '074-01 Gestión de hoteles y otros alojamientos', '074-02 Gestión de establecimientos de restauración', '074-03 Jefatura de economatos y bodegas', '075-00 Gestión inmobiliaria en general', '078-00 Habilitación y especialización en docencia en general', '079-00 Seguridad alimentaria: manipulación y control de alimentos en general', '079-01 Higiene alimentaria / Manipulación de alimentos', '079-02 Control de puntos críticos (Ind. Alimentaria)', '080-00 Hostelería-Atención en pisos en general', '080-01 Gobernanza de pisos', '080-02 Servicio de pisos', '080-03 Lencería, lavandería,  planchado', '087-00 Informática de Usuario / Ofimática en general', '087-01 Ofimática: Procesadores de texto', '087-02 Ofimática: Bases de datos', '087-03 Ofimática: Hojas de Cálculo', '087-04 Ofimática: Aplicaciones para Presentaciones en público', '087-05 Ofimática: Internet-intranet y navegadores', '087-06 Ofimática: Diseño de páginas web', '087-07 Informática de usuario: Aplicaciones para el tratamiento de imágenes, sonido y vídeo', '087-08 Informática de usuario: Tratamiento e informes de grandes almacenes de datos', '087-09 Informática de usuario: Lenguajes de programación', '087-10 Informática de usuario: E-bussines & e-commerce', '087-11 Informática de usuario: Aplicaciones estándar de gestión de recursos, compras y ventas', '087-12 Informática de usuario: Aplicaciones para tratamiento estadístico', '087-13 Informática de usuario: Sistemas operativos', '087-14 Introducción a la informática y/o Nuevas Tecnologías de la información y comunicación', '087-15 Informática de usuario: Utilidades y herramientas de apoyo para el tratamiento de ficheros', '087-16 Informática de usuario: Aplicaciones estándar para estudios y oficinas técnicas', '088-00 Informática  de desarrollo en general', '089-00 Ingeniería y nuevas tecnologías i+d+i en general', '090-00 Instalaciones de viviendas y edificios en general', '090-01 Fontanería', '090-02 Calefacción y climatización', '090-03 Instalación de gas', '090-04 Instalación de aislamientos', '090-05 Impermeabilización', '090-06 Instalaciones eléctricas en edificios', '091-00 Montaje e instalación de equipos industriales en general', '091-01 Instalación de equipos y sistemas electrónicos', '091-02 Instalación de máquinas y equipos industriales', '091-03 Instalación de equipos y sistemas de comunicación electrónicos', '091-04 Instalación y mantenimiento  de conducciones de fluidos', '091-05 Montaje electromecánico en Industrias de Fabricación de Equipos Electromecánicos', '091-06 Trabajos auxiliares de montajes electrónicos en Industrias de Fabricación de Equipos Electromecánicos', '091-07 Montaje de dispositivos y cuadros electrónicos en Industrias de Fabricación de Equipos Electromecánicos', '091-08 Montaje y ajuste de equipos electrónicos en Industrias de Fabricación de Equipos Electromecánicos', '092-00 Instrumentación y control en general', '093-00 Interiorismo y decoración en general', '094-00 Interpretación de planos y delineación en general', '095-00 Inversiones-banca en general', '096-00 Jardinería, floricultura y arte floral en general', '096-01 Cultivo de flores', '096-02 Manipulación de flores', '096-03 Trabajos de centros de jardinería', '096-04 Trabajos en viveros', '096-05 Jardinería', '097-00 Laboratorio clínico en general', '097-01 Técnicas de microbiología', '097-02 Análisis de hematología', '097-03 Técnicas de inmunología-bioquímica', '097-04 Técnicas de anatomía patológica', '097-05 Técnicas auxiliares de laboratorio clínico', '101-00 Logística Integral en general', '102-00 Manipulación de mercancías en general', '102-01 Manipulación de mercancías-Transporte por carretera', '102-02 Manipulación de mercancías en puerto', '102-03 Manipulación de mercancías- Estiba y desestiba de buques', '102-04 Manipulación de mercancías con grúas portuarias', '102-05 Manipulación de mercancías en cubierta de buques', '102-06 Manipulación de mercancías-Transporte aéreo', '103-00 Mantenimiento de Edificios y Otro Equipamiento Urbano en general', '103-01 Análisis y diagnóstico de edificios', '103-02 Domótica', '103-03 Mantenimiento de instalaciones de climatización de edificios', '104-00 Mantenimiento de Equipos Electromecánicos de uso no industrial en general', '104-01 Mantenimiento de Equipos de climatización', '104-02 Mantenimiento de Equipos Electrodomésticos y de limpieza de edificios', '104-03 Mantenimiento de Equipos Informáticos y periféricos', '104-04 Mantenimiento de Equipos de Telefonía y Comunicaciones', '104-05 Mantenimiento de Equipos de electromedicina', '105-00 Mantenimiento Industrial en general', '105-01 Mantenimiento mecánico', '105-02 Mantenimiento electromecánico', '105-03 Mantenimiento eléctrico', '105-04 Mantenimiento electrónico', '105-05 Trabajos auxiliares en mantenimiento mecánico en Industrias de Fabricación de Equipos Electromecánicos', '105-06 Mantenimiento de instalaciones mecánicas en Industrias de Fabricación de Equipos Electromecánicos', '105-07 Mantenimiento mecánico en Industrias de Fabricación de Equipos Electromecánicos', '105-08 Trabajos auxiliares de mantenimiento electromecánico en Industrias de Fabricación de Equipos Electromecánicos', '105-09 Técnicas de mantenimiento electromecánico en Industrias de Fabricación de Equipos Electromecánicos', '105-10 Técnicas de mantenimiento de equipos eléctricos en Industrias de Fabricación de Equipos Electromecánicos', '105-11 Técnicas de mantenimiento electrónico en Industrias de Fabricación de Equipos Electromecánicos', '105-12 Mantenimiento de equipos electromecánicos en Industrias de Fabricación de Equipos Electromecánicos', '106-00 Mantenimiento y Reparación de Equipos de Transporte en general', '106-01 Reparación de maquinaria agrícola autopropulsada', '106-02 Reparación de maquinaria industrial autopropulsada', '106-03 Reparación de motores de aviación', '106-04 Reparación de motores náuticos y componentes mecánicos navales', '106-05 Mantenimiento de vehículos ferroviarios de tracción', '106-06 Reparación de vehículos pesados', '107-00 Mantenimiento y Reparación de Vehículos Ligeros en general', '107-01 Reparación de carrocerías de vehículos', '107-02 Reparación de motores y equipos de inyección', '107-03 Reparación de sistemas eléctricos y electrónicos de vehículos', '107-04 Diagnosis de vehículos', '107-05 Reparación de vehículos de dos o tres ruedas', '108-00 Marketing en general', '109-01 Materiales y materias primas: Agricultura, ganadería, caza y actividades de los servicios relacionados con las mismas', '109-02 Materiales y materias primas: Selvicultura, explotación forestal y actividades de los servicios relacionados con las mismas', '109-03 Materiales y materias primas: Pesca, acuicultura y actividades de los servicios relacionados con las mismas', '109-04 Materiales y materias primas: Extracción y aglomeración de antracita, hulla, lignito y turba', '109-05 Materiales y materias primas: Extracción de crudos de petróleo y gas natural; actividades de los servicios relacionados con las explotaciones petrolíferas y de gas, excepto actividades de prospección', '109-06 Materiales y materias primas: Extracción de minerales de uranio y torio', '109-07 Materiales y materias primas: Extracción de minerales metálicos', '109-08 Materiales y materias primas: Extracción de minerales no metálicos ni energéticos', '109-09 Materiales y materias primas: Industria de productos alimenticios y bebidas', '109-10 Materiales y materias primas: Industria del tabaco', '109-11 Materiales y materias primas: Industria textil', '109-12 Materiales y materias primas: Industria de la confección y de la peletería', '109-13 Materiales y materias primas: Preparación, curtido y acabado del cuero; fabricación de artículos de marroquinería y viaje; artículos de guarnicionería, talabartería y zapatería', '109-14 Materiales y materias primas: Industria de la madera y del corcho, excepto muebles; cestería y espartería', '109-15 Materiales y materias primas: Industria del papel', '109-16 Materiales y materias primas: Edición, artes gráficas y reproducción de soportes grabados', '109-17 Materiales y materias primas: Industria química', '109-18 Materiales y materias primas: Fabricación de productos de caucho y materias plásticas', '109-19 Materiales y materias primas: Metalurgia', '109-20 Materiales y materias primas: Fabricación de productos metálicos, excepto maquinaria y equipo', '109-21 Materiales y materias primas: Industria de la construcción de maquinaria y equipo mecánico', '109-22 Materiales y materias primas: Fabricación de máquinas de oficina y equipos informáticos', '109-23 Materiales y materias primas: Fabricación de maquinaria y material eléctrico', '109-24 Materiales y materias primas: Fabricación de material electrónico; fabricación de equipo y aparatos de radio, televisión y comunicaciones', '109-25 Materiales y materias primas: Fabricación de equipo e instrumentos médico-quirúrgicos, de precisión, óptica y relojería', '109-26 Materiales y materias primas: Fabricación de vehículos de motor, remolques y semirremolques', '109-27 Materiales y materias primas: Fabricación de otro material de transporte', '109-28 Materiales y materias primas: Coquerías, refino de petróleo y tratamiento de combustibles nucleares', '109-29 Materiales y materias primas: Fabricación de otros productos minerales no metálicos', '109-30 Materiales y materias primas: Fabricación de muebles; otras industrias manufactureras', '109-31 Materiales y materias primas: Reciclaje', '109-32 Materiales y materias primas: Producción y distribución de energía eléctrica, gas, vapor y agua caliente', '109-33 Materiales y materias primas: Captación, depuración y distribución de agua', '109-34 Materiales y materias primas: Construcción', '109-35 Materiales y materias primas: Hostelería', '110-00 Mercados financieros en general', '111-00 Merchandising en general', '112-00 Metodologías-Didácticas específicas en general', '114-00 Calidad en general', '114-01 Auditoría de Calidad', '114-02 Introducción a la calidad', '114-03 Herramientas de calidad', '114-04 Gestión de calidad total/Modelos de excelencia empresarial', '114-05 Normalización/Homologación/ certificación de calidad', '115-00 Nuevas tecnologías e investigación aplicadas a la docencia', '116-00 Nutrición y dietética en general', '117-00 Oficina técnica-Construcción en general', '117-01 Delineación  en edificación y obras civiles', '117-02 Técnicas auxiliares de obra', '117-03 Técnicas auxiliares de topografía', '117-04 Técnicas auxiliares de laboratorio de obra', '117-05 Técnicas auxiliares de control y vigilancia de obras', '117-06 Planificación y control de obras', '117-07 Cálculo de estructuras y cimentaciones', '118-01 Encofrado', '118-02 Ferralla', '118-03 Hormigonado', '118-04 Entibado', '118-05 Montaje de estructuras tubulares', '118-06 Albañilería', '118-07 Revoco', '118-08 Tendido de yesos', '118-09 Estucado', '118-10 Colocación de escayola', '118-11 Colocación de prefabricados ligeros', '118-12 Instalación de redes de saneamiento', '118-13 Cantería', '118-14 Colocación de mármol', '118-15 Mampostería', '118-16 Pavimentación', '118-17 Trabajos en portland', '118-18 Pintura de edificios', '118-19 Entarimado', '118-20 Enmoquetado-entelado', '118-21 Colocación de pavimentos ligeros', '118-22 Acristalado de edificios', '118-23 Colocación de pizarra', '118-24 Solado-alicatado', '118-25 Techado en chapas y placas', '118-26 Operación de plantas de áridos', '118-27 Operación de plantas de hormigón', '118-28 Operación de plantas de aglomerados asfálticos', '118-29 Prefabricado de hormigón', '118-99 Otros competencias específicas de oficios de construcción', '119-00 Operación de maquinaria de construcción en general', '119-01 Operaciones con maquinas de perforación', '119-02 Operaciones de voladura con explosivos', '119-03 Operaciones con maquinaria excavadora', '119-04 Operaciones con maquinaria explanadora', '119-05 Operaciones con maquinaria de transporte de tierras', '119-06 Operaciones con maquinaria compactadoras', '119-07 Operaciones con maquinaria de firmes y pavimentos', '119-08 Operaciones con maquinaria de dragado', '119-09 Operaciones con maquinaria de vías', '119-10 Operación de grúas', '119-99 Operación de otra maquinaria de construcción', '120-00 Operativa bancaria en general', '120-01 Trabajos administrativos en entidades financieras', '120-02 Operaciones financieras de activo', '120-03 Operaciones financieras internacionales', '120-04 Operaciones financieras internas', '120-05 Gestión comercial de servicios financieros', '121-00 Operativa de agencias de viajes en general', '122-00 Procesos de producción, transformación y distribución de energía y agua en general', '122-01 Operación y mantenimiento de centrales hidroeléctricas', '122-02 Supervisión de centrales hidroeléctricas', '122-03 Operación de centrales termoeléctrica', '122-04 Control de centrales termoeléctricas', '122-05 Montaje eléctrico de centrales eléctricas', '122-06 Montaje mecánico de centrales eléctricas', '122-07 Montaje de equipos de instrumentación y control  de central eléctrica', '122-08 Operaciones de instrumentación y control de centrales eléctricas', '122-09 Operaciones en líneas eléctricas de alta tensión', '122-10 Montaje y mantenimiento de subestaciones eléctricas', '122-11 Operaciones en  subestaciones eléctricas de alta tensión', '122-12 Operaciones en redes y centros de distribución de energía eléctrica', '122-13 Montaje y mantenimiento de instalaciones de distribución de energía eléctrica', '122-14 Operación de centros de maniobra de distribución de energía eléctrica', '122-15 Instalación de sistemas de energía solar térmica', '122-16 Instalación de sistemas fotovoltaicos y eólicos', '122-17 Técnicas de sistemas de energías renovables', '122-18 Operaciones de Calderas Industriales', '122-19 Operaciones en gasoductos', '122-20 Operaciones en sistemas de distribución de gas', '122-21 Supervisión de sistemas de distribución de gas', '122-22 Mantenimiento de plantas de captación y tratamiento de agua', '122-23 Operaciones en plantas de tratamiento de agua', '122-24 Operaciones en sistemas de distribución de agua', '122-25 Técnicas de sistemas de distribución de agua', '123-00 Operativa de seguros en general', '123-01 Técnicas administrativas de seguros', '123-02 Comercialización de seguros', '124-00 Operativa de transportes en general', '124-01 Operativa de transportes por carretera', '124-02 Operativa de transporte marítimo', '124-03 Operativa de transporte por ferrocarril. Control de circulación', '124-04 Atención a pasajeros en aeronaves', '124-05 Operación de centros de facilitación aeroportuaria', '125-00 Organización de centros educativos en general', '127-01 Legislación y normativa comunitaria', '127-02 Legislación y normativa internacional no comunitaria', '127-03 Legislación y normativa financiera y tributaria', '127-04 Legislación y normativa sociolaboral', '127-05 Legislación y normativa mercantil', '127-06 Legislación y normativa medioambiental', '127-07 Legislación y normativa administrativa (excepto la medioambiental)', '127-99 Otra legislación y normativa', '129-00 Montaje, instalación de equipos de uso no industrial en general', '133-00 Planificación y organización empresarial en general', '135-01 Carpintería-ebanistería artesana', '135-02 Restauración en madera', '135-03 Elaboración de objetos de fibra vegetal', '135-04 Cerámica', '135-05 Talla en piedra', '135-06 Soplado de vidrio', '135-07 Vidriería artística', '135-08 Decoración de objetos de vidrio', '135-09 Fundición artesana', '135-10 Cerrajería artística', '135-11 Calderería artística', '135-12 Marroquinería', '135-13 Guarnicionería', '135-14 Zapatería', '135-15 Tejeduría manual', '135-16 Adornos textiles', '135-17 Modistería', '135-18 Sastrería', '135-19 Costura', '135-20 Joyería', '135-21 Platería', '135-22 Luthería', '135-23 Encuadernación-restauración de libros', '135-24 Muñequería', '135-25 Elaboración de figuras plásticas', '135-26 Reparación de relojes', '135-27 Construcción de maquetas', '135-99 Otros Procesos productivos de Artesanía', '136-00 Procesos productivos Automoción en general', '137-01 Sacrificio de ganado', '137-02 Carnicería', '137-03 Elaboración de productos cárnicos', '137-04 Procesado de leche', '137-05 Elaboración de quesos', '137-06 Elaboración de helados', '137-07 Elaboración de productos lácteos y ovoproductos', '137-08 Elaboración de aceites y grasas', '137-09 Panadería', '137-10 Pastelería', '137-11 Elaboración de galletas', '137-12 Elaboración de cacao y chocolate', '137-13 Elaboración de turrones y mazapanes', '137-14 Elaboración de caramelos y dulces', '137-15 Elaboración de conservas de productos de la pesca', '137-16 Elaboración de conservas vegetales', '137-17 Procesado de productos congelados', '137-18 Molinería', '137-19 Elaboración de piensos compuestos', '137-20 Elaboración de azúcar', '137-21 Elaboración de precocinados y cocinados', '137-22 Procesado de catering', '137-23 Elaboración de café', '137-24 Elaboración de té e infusiones', '137-25 Elaboración de frutos secos y aperitivos snacks', '137-26 Elaboración de sopas, salsas, caldos y postres deshidratados', '137-27 Elaboración de alimentos infantiles', '137-28 Pescadería', '137-29 Elaboración de vinos', '137-30 Elaboración de sidras', '137-31 Elaboración de cerveza', '137-32 Elaboración de alcoholes y licores', '137-33 Elaboración de bebidas analcoholicas', '137-34 Elaboración de tabaco', '137-35 Trabajos auxiliares en industrias alimentarias', '137-36 Almacenaje en industrias alimentarias', '137-37 Envasado de productos alimentarios', '137-38 Supervisión de operaciones de industrias alimentarias', '137-99 Otros Procesos productivos-Ind-Alimentarias', '138-01 Operación de montaje y ajuste mecánico', '138-02 Técnicas de ajuste mecánico', '138-03 Operación de máquinas-herramienta', '138-04 Operación de torno y fresadura', '138-05 Construcción de prototipos mecánicos', '138-06 Desarrollo de productos mecánicos', '138-07 Trabajos auxiliares de electricidad', '138-08 Electricidad industrial', '138-09 Diseño de sistemas de control eléctrico', '138-10 Operaciones de matricería y moldes', '138-99 Otros Procesos productivos- Industrias de fabricación de equipos mecánicos, eléctricos y electrónicos', '139-01 Procesos de fundición', '139-02 Procesos de forja', '139-03 Procesos de laminación', '139-04 Procesos de estirado', '139-05 Procesos de tratamientos térmicos', '139-06 Recubrimiento de superficies metálicas', '139-99 Otros procesos productivos industria pesada: Construcciones metálicas y metalurgia en general', '140-01 Técnicas de producción editorial', '140-02 Técnicas de edición', '140-03 Realización de proyectos gráficos y maquetas', '140-04 Redacción y corrección de textos', '140-05 Trabajos de pre-impresión', '140-06 Preparación de textos', '140-07 Preparación de imágenes', '140-08 Integración de fotorreproducción', '140-09 Elaboración de pruebas', '140-10 Montaje y preparación de la forma impresora', '140-11 Impresión en huecograbado', '140-12 Impresión en offset-bobina', '140-13 Impresión en offset-hoja', '140-14 Impresión de flexografía', '140-15 Impresión de serigrafía', '140-16 Impresión de pequeños formatos', '140-17 Confección de complejos', '140-18 Confección de bolsas y sobres', '140-19 Extrusionado', '140-20 Encuadernación industrial', '140-21 Contracolado-engomado', '140-22 Troquelado', '140-23 Confección de productos de cartón', '140-24 Confección de etiquetas', '140-25 Maquetación de prensa', '140-26 Infografía de prensa', '140-99 Otros Procesos productivos- Industrias gráficas', '141-01 Fabricación de abrasivos rígidos', '141-02 Fabricación de abrasivos flexibles', '141-03 Fabricación de cerámicas especiales', '141-04 Fabricación de ladrillos y revestimientos cerámicos', '141-05 Fabricación de artículos del hogar y porcelana sanitaria', '141-06 Fabricación de vidrio y transformado en vidrio', '141-07 Elaboración de artículos de vidrio y transformados en vidrio', '141-08 Fabricación de fibra de vidrio', '141-09 Fabricación de vidrio óptico', '141-10 Fabricación de cadenas de joyería y bisutería', '141-11 Fabricación de artículos de joyería y bisutería por colada', '141-12 Fabricación de joyería y bisutería por prensado', '141-13 Fabricación de artículos de joyería por conformado electrolítico', '141-14 Acabado y verificado de artículos de joyería y bisutería', '141-15 Transformado de plástico para juguetes', '141-16 Manufacturado de juguetes de plástico y textil', '141-17 Fabricación de juguetes metálicos', '141-18 Fabricación de juguetes educativos y de sociedad', '141-19 Elaboración de lápices', '141-20 Fabricación de máquinas de escritorio', '141-21 Fabricación de cremalleras', '141-22 Teñido de artículos diversos', '141-23 Fabricación de cursores y remaches', '141-99 Otros procesos productivos-Industrias manufactureras diversas', '142-01 Operación de plantas químicas', '142-02 Técnicas de plantas químicas', '142-03 Operación en la fabricación en industrias farmacéuticas', '142-04 Técnicas de fabricación en industrias farmacéuticas', '142-05 Operación en la fabricación en industrias petroquímicas', '142-06 Técnicas de fabricación en industrias petroquímicas', '142-07 Operación en la fabricación de productos en otras industrias químicas', '142-08 Técnicas de fabricación en otras industrias químicas', '142-09 Operación en la transformación de plástico y caucho', '142-10 Técnicas de transformación de plástico y caucho', '142-11 Operación de industrias papeleras', '142-12 Técnicas de industrias papeleras', '142-99 Otros procesos productivos-Industrias químicas', '143-01 Procesado de fibras', '143-02 Procesado de fibras para la hilatura', '143-03 Hilado y acabado de hilados', '143-04 Coordinación técnica de producción de hilatura', '143-05 Control de calidad de hilatura', '143-06 Urdido y encolado', '143-07 Preparación de monturas y C:A:D:Jacquard', '143-08 Tejeduría en telar de calada', '143-09 Revisado y reparado de productos textiles. Tejeduría de calada', '143-10 Coordinación técnica de producción de tejeduría de calada', '143-11 Control de calidad de tejeduría de calada', '143-12 Procesado de telas no tejidas', '143-13 Coordinación técnica de producción de telas no tejidas', '143-14 Control de calidad de telas no tejidas', '143-15 Tejeduría de género de punto en máquinas de recogida', '143-16 Tejeduría de género de punto en máquinas de urdimbre', '143-17 Revisado y reparado de productos textiles. Géneros de punto', '143-18 Coordinación técnica de producción de géneros de punto', '143-19 Control de calidad de género de punto', '143-20 Preparado y blanqueado textil', '143-21 Estampado textil', '143-22 Teñido textil', '143-23 Acabados y aprestos', '143-24 Preparación de disoluciones', '143-25 Coordinación técnica de producción de ennoblecimiento textil', '143-26 Control de calidad de ennoblecimiento textil', '143-27 Confección de patrones y escalado', '143-28 Operación de máquinas de confección industrial', '143-29 Planchado', '143-30 Bordado y acolchado', '143-31 Revisión de productos textiles de confección', '143-32 Coordinación técnica de producción de confección', '143-33 Control de calidad de confección', '143-34 Curtición de pieles y cueros', '143-35 Acabado de cueros', '143-36 Control de procesos de curtición', '143-37 Confección de patrones de calzado', '143-38 Cortado de cuero, ante y napa', '143-39 Preparación y cosido de cuero, ante y napa', '143-40 Montado de calzado', '143-41 Terminado de calzado', '143-42 Confección de patrones de maroquinería y guantería', '143-43 Marroquinería industrial', '143-44 Confección de patrones de peletería, ante y napa', '143-45 Cortado, clavado y clasificado de peletería', '143-46 Operación de máquinas de peletería', '143-47 Forrado y terminado de peletería, ante y napa', '143-99 Otros procesos productivos- Industrias textiles, de la piel y el cuero', '144-01 Prospecciones y sondeos', '144-02 Preparación y conservación de galerías', '144-03 Arranque de carbón', '144-04 Transporte y extracción de mineral', '144-05 Electromecánica minera', '144-06 Extracción por sutiraje', '144-07 Aplicación de explosivos y arranque de minerales', '144-08 Minería de rocas para usos ornamentales', '144-09 Obtención y tratamiento de sal común', '144-10 Tratamiento y clasificación de rocas y minerales', '144-11 Beneficio de rocas para ornamentación', '144-12 Operación de coquización', '144-13 Sinterización de minerales de hierro', '144-14 Operación de horno alto', '144-15 Procesos de lixiviación y electrolisis', '144-16 Procesos de fusión y colada', '144-17 Obtención de alúmina y aluminio', '144-18 Operación de hornos de tostación, calcinación y sinterización', '144-99 Otros procesos productivos- Minería y primeras transformaciones', '145-01 Piscicultura de criadero', '145-02 Piscicultura de engorde en aguas marinas', '145-03 Piscicultura en aguas continentales', '145-04 Reproducción y cultivo de larvas', '145-05 Estabulación de crustáceos (Cetareas)', '145-06 Cultivo de crustáceos (Criadero y engorde)', '145-07 Cultivo de moluscos en criadero', '145-08 Cultivo de moluscos en medio natural', '145-09 Reproducción de moluscos', '145-10 Marisqueo', '145-11 Cultivo de zooplancton', '145-12 Cultivo de fitoplancton', '145-13 Cultivo de macroalgas en medio natural', '145-14 Recolección de macroalgas', '145-15 Pesca de bajura', '145-16 Pesca de litoral', '145-17 Mecánica de pesca en litoral', '145-18 Pesca de altura y gran altura', '145-19 Mecánica de pesca en altura y gran altura', '145-20 Buceo profesional', '145-99 Otros procesos productivos-Pesca y acuicultura', '146-01 Aserrado  de madera', '146-02 Secado y tratamiento de madera', '146-03 Fabricación de chapa y tablero contrachapado', '146-04 Fabricación de laminados de madera', '146-05 Fabricación de tableros de partículas y de fibras', '146-06 Elaboración de proyectos de carpintería y mueble', '146-07 Despiece de madera y tableros', '146-08 Mecanizado de madera y tableros', '146-09 Armado y montaje de carpintería y mueble', '146-10 Barnizado-lacado', '146-11 Tapicería de muebles', '146-12 Confección de muebles de caña, junco y mimbre', '146-13 Carpintería semi industrializada', '146-14 Carpintería de armar', '146-15 Carpintería de ribera', '146-16 Ebanistería semi industrializada', '146-17 Fabricación de toneles', '146-18 Preparación de corcho', '146-19 Fabricación de tapones', '146-20 Fabricación de artículos de corcho aglomerado', '146-99 Otros procesos productivos-Industrias de la madera y el corcho', '147-01 Cultivo de cereales y leguminosas de grano', '147-02 Cultivo de raíces y tubérculos', '147-03 Cultivo de plantas forrajeras y pratenses', '147-04 Cultivo de plantas industriales', '147-05 Fruticultura', '147-06 Viticultura', '147-07 Olivicultura', '147-08 Horticultura', '147-09 Producción de setas', '147-10 Producción de plantas hortícolas', '147-11 Manipulación de frutas y hortalizas', '147-12 Manipulación de tractores', '147-13 Manipulación de cosechadoras', '147-99 Producción agrícola- otros cultivos', '148-01 Explotación de ganado vacuno', '148-02 Explotación de ganado ovino-caprino', '148-03 Explotación de ganado equino', '148-04 Explotación extensiva de ganado porcino', '148-05 Explotación intensiva de ganado porcino', '148-06 Cunicultura', '148-07 Avicultura', '148-08 Apicultura', '148-99 Otros tipos de producción ganadera', '149-00 Producción y realización de audiovisuales, radio y espectáculos en general', '149-01 Producción de radio, cine, televisión, teatro y espectáculos', '149-02 Realización de radio, cine, televisión, teatro y espectáculos', '149-03 Técnicas audiovisuales', '149-04 Infografía en medios audiovisuales', '150-00 Productividad en general', '150-01 Herramientas de medida de la productividad', '151-00 Diagnóstico clínico y radioterapia en general', '151-01 Técnicas de radiodiagnóstico', '151-02 Técnicas de medicina nuclear', '151-03 Técnicas de radioterapia', '151-04 Técnicas no radiológicas de diagnóstico clínico', '151-99 Otras técnicas de diagnóstico clínico', '152-00 Publicidad y comunicación externa de la empresa en general', '152-01 Relaciones Públicas y protocolo empresarial', '153-00 Recepción hotelera en general', '154-00 Redacción/Guión/locución-Información y manifestaciones artísticas en general', '154-01 Técnicas de redacción, guión y periodismo', '154-02 Técnicas de locución y doblaje', '155-00 Reforma educativa de la LOGSE en general', '157-00 Salud laboral y enfermedades profesionales en general', '158-00 Seguridad de Instalaciones y dispositivos de alto riesgo en general', '159-00 Prevención de riesgos laborales en general', '159-01 Prevención de riesgos en la construcción', '160-00 Seguridad y vigilancia en general', '160-01 Seguridad privada', '160-02 Prevención de incendios', '160-03 Protección Civil', '161-00 Servicios Asistenciales en general', '161-01 Prevención de la marginación escolar y social', '161-02 Ayuda domiciliaria', '161-03 Servicios asistenciales a mayores: geriatría y gerocultura', '162-01 Peluquería', '162-02 Estilismo', '162-03 Manicura', '162-04 Estética', '162-05 Maquillaje', '162-06 Operación de cementerios', '162-07 Servicios funerarios', '162-08 Tanatología', '162-09 Atención al consumidor', '162-10 Servicio doméstico', '162-99 Otros Servicios Personales', '163-00 Servicios Recreativos, Culturales y Deportivos en general', '163-01 Mantenimiento de bienes culturales', '163-02 Animación deportiva', '163-03 Masaje deportivo', '163-04 Monitoraje deportivo', '163-05 Animación socio- cultural', '163-06 Conservación de parques y jardines', '163-07 Operación de parques zoológicos', '163-08 Operación de centros de recreo', '163-99 Otros servicios recreativos, culturales y deportivos', '164-00 Socorrismo y Primeros Auxilios en general', '164-01 Socorrismo acuático', '164-02 Primeros auxilios', '165-00 Soldadura en general', '165-01 Mantenimiento de estructuras metálicas', '165-02 Técnicas de soldadura de estructuras metálicas ligeras', '165-03 Técnicas de soldadura de estructuras metálicas pesadas', '165-04 Técnicas de soldadura de tuberías y recipientes de alta presión', '165-05 Homologación en Soldadura', '165-06 Soldadura especializada', '165-07 Soldadura eléctrica y oxigasística', '165-08 Supervisión de obra soldada', '165-99 Otras técnicas de soldadura', '166-01 Limpieza viaria', '166-02 Tratamiento y eliminación de residuos urbanos', '166-03 Depuración de aguas residuales', '166-04 Operación del mantenimiento  alcantarillado', '166-05 Supervisión del mantenimiento de alcantarillado', '166-06 Control de plagas', '166-07 Lavandería', '166-08 Tintorería', '166-09 Plancha', '166-10 Recepción de prendas para teñido y lavado', '166-11 Limpieza de inmuebles', '166-99 Otros servicios de limpieza y tratamiento de residuos urbanos', '167-00 Técnicas de venta y formación de vendedores en general', '168-00 Telecomunicaciones no informáticas en general', '169-00 Telemárketing / márketing telefónico', '170-00 Tratamiento de residuos (excepto residuos urbanos) en general', '171-01 Atención en ruta', '171-02 Animación turística', '171-03 Atención en congresos', '171-04 Información turística', '171-05 Desarrollo turístico', '171-06 Turismo rural', '171-99 Otros servicios turísticos especializados', '172-00 Tutorías y orientación en general', '173-00 Conocimiento y promoción de productos para farmacias en general', '174-01 Alemán', '174-02 Árabe', '174-03 Asturiano/bable', '174-04 Braille', '174-05 Castellano', '174-06 Catalán', '174-07 Chino', '174-08 Euskera', '174-09 Francés', '174-10 Gallego', '174-11 Griego', '174-12 Holandés', '174-13 Inglés', '174-14 Italiano', '174-15 Japonés', '174-16 Lenguaje de signos', '174-17 Portugués', '174-18 Ruso', '174-19 Sueco', '174-20 Valenciano', '174-99 Otros Idiomas no clasificables anteriormente');
	campoSelect('grupo','Grupo de acciones',$nombres,$valores,$datos,'selectpicker span4 show-tick');
}


function creaTablaTutores($datos){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'>Tutor/es:</label>
	    <div class='controls'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaTutores'>
				  	<thead>
				    	<tr>
				            <th> Tutor <button type='button' class='btn btn-primary btn-small' id='crearTutor'><i class='icon-plus-circle'></i> Crear tutor</button></th>
							<th> Horas tutoría</th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		conexionBD();
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM tutores_accion_formativa WHERE codigoAccionFormativa=".$datos['codigo']);
				  			while($datosTutoria=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaTutores($datosTutoria,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaTutores(false,$i);
				  		}
				  		cierraBD();
	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaTutores\");'><i class='icon-plus'></i> Añadir tutoría</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaTutores\");'><i class='icon-trash'></i> Eliminar tutoría</button>
				</div>
			</div>
		</div>
	</div>
	<br />";
}

function imprimeLineaTablaTutores($datos,$i){
	$j=$i+1;

	echo "
	<tr>";
	campoSelectConsulta('codigoTutor'.$i,'',"SELECT codigo, CONCAT(nombre,' ',apellido1,' ',apellido2) AS texto FROM tutores WHERE activo='SI' ORDER BY nombre;",$datos['codigoTutor'],'selectpicker span4 show-tick','data-live-search="true"','',1,false);
	campoTextoTabla('horas'.$i,$datos['horas'],'input-mini pagination-right horas');
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}


//Parte de previsualización de diploma

function creaBotonPrevisualizaDiploma(){
	echo "<a href='#' id='previsualizaDiploma' class='btn btn-small btn-primary noAjax'><i class='icon-credit-card'></i> Vista previa del diploma</a>";
}


function generaPDFFactura($accion,$temario){
	$res="
	<style type='text/css'>
	<!--
		*{
			color:#2e3d64;
			font-family:DejaVu Sans Condensed;
		}

		.cabecera{
			width:100%;
			height:193.5px;
		}

		.tituloAccion{
			margin-top:13px;
			padding-right:80px;
			width:100%;
			text-align:right;
			font-size:12px;
		}

		.temario{
			font-size:9px;
			margin-left:80px;
			margin-top:21px;
			line-height:9px;
		}
	-->
	</style>
	<page>
		<img src='../img/cabeceraDiploma.png' class='cabecera' />
		<br />
		<div class='tituloAccion'>".$accion."</div>
		<div class='temario'>
			".nl2br($temario)."
		</div>
	</page>";



	return $res;
}

//Fin parte de previsualización de diploma

function compruebaCodigoAF(){
	$res='ok';

	$codigoAF=$_POST['codigoAF'];
	$codigo=$_POST['codigo'];

	$whereCodigo='';
	if($codigo!='NULL'){
		$whereCodigo="AND codigo!='$codigo'";
	}

	$consulta=consultaBD("SELECT codigo FROM acciones_formativas WHERE accionFormativa='$codigoAF' $whereCodigo;",true,true);
	if($consulta){
		$res='error';
	}

	echo $res;
}



function creaTablaFormadores($datos){
	echo "	
	<div class='control-group'>                     
		<label class='control-label'style='text-align:left'>Formador/es:</label>
	    <div class='controls' style='margin-left:0px'>

			<div class='table-responsive'>
				<table class='table table-striped tabla-simple' id='tablaFormadores'>
				  	<thead>
				    	<tr>
				            <th> NIF</th>
							<th> Nombre</th>
							<th> 1º Apellido </th>
							<th> 2º Apellido </th>							
							<th> Teléfono </th>
							<th> eMail </th>
							<th> Horas imp. </th>
							<th> </th>
				    	</tr>
				  	</thead>
				  	<tbody>";

				  		$i=0;
				  		conexionBD();
				  		if($datos){
				  			$consulta=consultaBD("SELECT * FROM formadores_af_presencial WHERE codigoAccionFormativa=".$datos['codigo']);
				  			while($datosFormador=mysql_fetch_assoc($consulta)){
				  				imprimeLineaTablaFormadores($datosFormador,$i);
				  				$i++;
				  			}
				  		}

				  		if($i==0){
				  			imprimeLineaTablaFormadores(false,$i);
				  		}
				  		cierraBD();
	echo "				  		
				  	</tbody>
				</table>
				<div class='centro'>
					<button type='button' class='btn btn-small btn-success' onclick='insertaFila(\"tablaFormadores\");'><i class='icon-plus'></i> Añadir formador</button> 
					<button type='button' class='btn btn-small btn-danger' onclick='eliminaFila(\"tablaFormadores\");'><i class='icon-trash'></i> Eliminar formador</button>
				</div>
			</div>
		</div>
	</div>";
}

function imprimeLineaTablaFormadores($datos,$i){
	$j=$i+1;

	echo "<tr>";

	campoTextoTabla('nifFormador'.$i,$datos['nifFormador'],'input-small');
	campoTextoTabla('nombreFormador'.$i,$datos['nombreFormador'],'input-medium');
	campoTextoTabla('primerApellidoFormador'.$i,$datos['primerApellidoFormador']);
	campoTextoTabla('segundoApellidoFormador'.$i,$datos['segundoApellidoFormador']);	
	campoTextoTabla('telefonoFormador'.$i,$datos['telefonoFormador'],'input-small pagination-right');
	campoTextoTabla('emailFormador'.$i,$datos['emailFormador'],'input-medium');
	campoTextoTabla('horasFormador'.$i,$datos['horasFormador'],'input-mini pagination-right');
	
	echo "<td>
			<input type='checkbox' name='filasTabla[]' value='$j'>
		 </td>
	</tr>";
}

//Fin parte de acciones formativas
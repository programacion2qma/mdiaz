<?php
	include_once('funciones.php');
    require_once('../../api/html2pdf/html2pdf.class.php');
	compruebaSesion();

	$contenido=generaPDFFactura($_POST['accion'],$_POST['temario']);

    $html2pdf=new HTML2PDF('L','A4','es',true,'UTF-8',array(0,0,0,0));
    $html2pdf->addFont('DejaVuSansCondensed', '', '../../api/html2pdf/_tcpdf_5.0.002/fonts/dejavusanscondensed.php');
    
    $html2pdf->pdf->SetDisplayMode('fullpage');
    $html2pdf->WriteHTML($contenido);
    $html2pdf->Output('Factura.pdf');
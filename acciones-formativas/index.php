<?php
  $seccionActiva=7;
  include_once('../cabecera.php');
  
  operacionesAccionesFormativas();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">

        <?php
          creaBotonesGestion();
        ?>
		

      <div class="span12">
        <div class="widget widget-table action-table">
            <div class="widget-header"> <i class="icon-list"></i>
              <h3>Acciones Formativas registradas</h3>
              <div class="pull-right">
                <button type="button" class="btn btn-primary btn-small" id="botonFiltro" estado="oculto"><i class="icon-filter"></i> Búsqueda por filtros</button>
              </div>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <?php
                filtroAccionesFormativas();
              ?>
              <table class="table table-striped table-bordered datatable tablaTextoPeque" id="tablaAccionesFormativas">
                <thead>
                  <tr>
                    <th> Acción </th>
                    <th> Modalidad </th>
                    <th> Horas </th>
                    <th> H. presenciales </th>
                    <th class='sumatorio'> Precio Mod. Económico </th>
                    <th class='sumatorio'> Coste </th>
                    <th> Código AF </th>
                    <th> Proveedor </th>
                    <th> Centro impartidor </th>
                    <th class="centro"></th>
                    <th class='centro'><input type='checkbox' id="todo"></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
            </div>
            <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>
<script type="text/javascript" src="../js/oyenteBotonFiltros.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap-select.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaAccionesFormativas','../listadoAjax.php?include=acciones-formativas&funcion=listadoAccionesFormativas();');
    $('.cajaFiltros select').selectpicker();
    oyenteBotonFiltros('#botonFiltro','#cajaFiltros','#tablaAccionesFormativas');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>
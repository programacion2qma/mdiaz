<?php
  $seccionActiva=7;
  include_once("../cabecera.php");
  gestionAccionFormativa();
?>

<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/gestionRegistros.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/funciones.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?php echo $_CONFIG['raiz']; ?>../../api/js/filasTabla.js" type="text/javascript"></script>
<script src="../js/validador.js" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){
	$('select').selectpicker();
	
	$('#boton-codigoCentroGestor').click(function(){
		$('#cajaGestion').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	oyenteModalidad();
	$('#modalidad').change(function(){
		oyenteModalidad();
	});

	$('#enviar').click(function(){
		creaCentroFormacion();
	});


	$('#crearTutor').click(function(){
		$('#cajaTutor').modal({'show':true,'backdrop':'static','keyboard':false});
	});

	$('#registraTutor').click(function(){
		creaTutor();
	});

	//Parte de validación de horas de teleformación
	$(':submit').unbind();//Le quito los oyentes de envío de formulario por defecto al botón submit (para el validador de tutores)
	$(':submit').click(function(e){
		e.preventDefault();
		compruebaValidacionEspecificaDeAccionesFormativas();
	});
	//Fin parte de validación de horas de teleformación

	//Parte de previsualización del diploma
	$('#previsualizaDiploma').click(function(){
		previsualizaDiploma();
	});
	//Fin parte de previsualización del diploma
});

function oyenteModalidad(){
	if($('#modalidad').val()=='TELEFORMACIÓN' || $('#modalidad').val()=='MIXTA'){
		$('#cajaTeleformacion').removeClass('hide');
		$('#codigoCentroGestor').parent().prev().html('<span class="asterisco">*</span> Centro gestor pla. teleformación:');

		if($('#modalidad').val()=='MIXTA'){
			$('#cajaPresencial').removeClass('hide');
			$('#cajaMixta').removeClass('hide');
		}
		else{
			$('#cajaPresencial').addClass('hide');
			$('#cajaMixta').addClass('hide');
		}
	}
	else if($('#modalidad').val()=='DISTANCIA'){
		$('#cajaPresencial').addClass('hide');
		$('#cajaMixta').addClass('hide');
		$('#cajaTeleformacion').addClass('hide');
		$('#codigoCentroGestor').parent().prev().html('<span class="asterisco">*</span> Centro gestor pla. teleformación:');
	}
	else{
		$('#codigoCentroGestor').parent().prev().html('<span class="asterisco">*</span> Centro de Formación (Imparte la form. presencial):');
		$('#cajaMixta').addClass('hide');
		$('#cajaTeleformacion').addClass('hide');
		$('#cajaPresencial').removeClass('hide');
	}
}

function creaCentroFormacion(){
	$('#enviar').html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var razonSocial=$('#razonSocial').val();
	var cif=$('#cif').val();
	var responsable=$('#responsable').val();
	var telefono=$('#telefono').val();
	var email=$('#email').val();
	var domicilio=$('#domicilio').val();
	var cp=$('#cp').val();
	var localidad=$('#localidad').val();

	var consulta=$.post('../listadoAjax.php?include=acciones-formativas&funcion=creaCentroFormacion();',{
		'razonSocial':razonSocial,
		'cif':cif,
		'responsable':responsable,
		'telefono':telefono,
		'email':email,
		'domicilio':domicilio,
		'cp':cp,
		'localidad':localidad,
		'activo':'SI'
	});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar el centro de formación.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			$('#codigoCentroGestor option[value=NULL]').after('<option value="'+respuesta+'">'+razonSocial+'</option>');
			$('#codigoCentroGestor').selectpicker('refresh');
		}

		$('#cajaGestion').modal('hide');
		$('#enviar').html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original

		//Borrado de valores del formulario (para siguientes inserciones)
		$('#razonSocial').val('');
		$('#cif').val('');
		$('#responsable').val('');
		$('#telefono').val('');
		$('#email').val('');
		$('#domicilio').val('');
		$('#cp').val('');
		$('#localidad').val('');
	});
}

function compruebaHorasTeleformacion(){
	var res=true;

	if($('#modalidad').val()!='PRESENCIAL' && $('#modalidad').val()!='DISTANCIA'){
		var horas=0;
		$('#tablaTutores').find('.horas').each(function(){
			horas+=obtieneHorasCampo($(this));
		});

		var horasCurso=obtieneHorasCampo($('#horasTeleformacion'));

		if(horas!=horasCurso){
			alert('Error: las horas de teleformación del curso no coinciden con las de tutores');
			res=false;
		}
	}

	return res;
}

function obtieneHorasCampo(campo){
	var res=parseInt(campo.val());

	if(isNaN(res)){
		res=0;
	}

	return res;
}

function creaTutor(){
	$('#registraTutor').html('<i class="icon-spinner icon-spin"></i> Procesando...');//Animación de carga

	var codigoCentroFormacionTutor=$('#codigoCentroFormacionTutor').val();
	var dniTutor=$('#dniTutor').val();
	var nombreTutor=$('#nombreTutor').val();
	var apellido1Tutor=$('#apellido1Tutor').val();
	var apellido2Tutor=$('#apellido2Tutor').val();

	var consulta=$.post('../listadoAjax.php?include=acciones-formativas&funcion=creaTutor();',{
		'codigoCentroFormacion':codigoCentroFormacionTutor,
		'dni':dniTutor,
		'nombre':nombreTutor,
		'apellido1':apellido1Tutor,
		'apellido2':apellido2Tutor
	});

	consulta.done(function(respuesta){
		if(respuesta=='fallo'){
			alert('Ha habido un problema al registrar el tutor.\nRevise los datos introducidos.\nSi el problema persiste, contacte con webmaster@qmaconsultores.com');
		}
		else{
			$('#tablaTutores').find('option[value=NULL]').after('<option value="'+respuesta+'">'+nombreTutor+' '+apellido1Tutor+' '+apellido2Tutor+'</option>');
			$('#tablaTutores .selectpicker').selectpicker('refresh');
		}

		$('#cajaTutor').modal('hide');
		$('#registraTutor').html('<i class="icon-check"></i> Guardar');//Reestablecimiento del botón original

		//Borrado de valores del formulario (para siguientes inserciones)
		$('#codigoCentroFormacionTutor').val('');
		$('#dniTutor').val('');
		$('#nombreTutor').val('');
		$('#apellido1Tutor').val('');
		$('#apellido2Tutor').val('');
	});
}

function previsualizaDiploma() {
  var contenido=$('#contenido');
  var form=document.createElement("form");

  form.setAttribute("method","post");
  form.setAttribute("action", "generaDiploma.php");
  form.setAttribute("id","formularioPrevisualiza");
  form.setAttribute("target","_blank");

  insertaCampoFormularioPrevisualizacionDiploma(form,'accion',$('#accion').val());
  insertaCampoFormularioPrevisualizacionDiploma(form,'temario',$('#temario').val());

  document.getElementById('contenido').appendChild(form);
  
  form.submit();
  $('#formularioPrevisualiza').remove();
}

function insertaCampoFormularioPrevisualizacionDiploma(form,nombre,valor){
	var campo=document.createElement("input");
	campo.setAttribute("type", "hidden");
	campo.setAttribute("name", nombre);
	campo.setAttribute("value", valor);
	form.appendChild(campo);
}

function compruebaValidacionEspecificaDeAccionesFormativas(){
	var codigoAF=$('#accionFormativa').val();
	var codigo=$('#codigo').val();

	if(codigo==undefined){
		codigo='NULL';
	}
	
	var consulta=$.post('../listadoAjax.php?include=acciones-formativas&funcion=compruebaCodigoAF();',{'codigoAF':codigoAF,'codigo':codigo});
	consulta.done(function(respuesta){
		if(respuesta!='error'){//Validación específica: comprobación de duplicidad del código de la AF y comprobación de horas de tutores
			if(compruebaHorasTeleformacion()){
				validaCamposObligatorios('#edit-profile');//Validación genérica de campos
			}
		}
		else{
			alert('El código de AF introducido ya ha sido utilizado en otro registro.');
		}
	});
}

</script>

</div><!-- contenido -->
<?php include_once('../pie.php'); ?>
<?php
@include_once('../config.php');//Carga del archivo de configuración
@include_once('../../api/nucleo.php');//Carga del núcleo de funciones comunes
@include_once('../funciones.php');//Carga de las funciones globales

//Inicio de funciones específicas

//Parte de tutores

function operacionesEntrasPeriodicas(){
	$res=true;

	if(isset($_POST['codigo'])){
		$res=actualizaDatos('entradas_periodicas');
	}
	elseif(isset($_POST['codigoCliente'])){
		$res=insertaDatos('entradas_periodicas');
	}
	elseif(isset($_POST['elimina']) && $_POST['elimina']=='SI'){
		$res=eliminaDatos('entradas_periodicas');
	}

	mensajeResultado('codigoCliente',$res,'Tutor');
    mensajeResultado('elimina',$res,'Tutor', true);
}


function listadoEntradasPeriodicas(){
	global $_CONFIG;

	$codigoCliente=obtenerCodigoCliente(true);
	$columnas=array('tipo','soporte','frecuencia','usuarioAutorizado','fechaPrimeraOperacion','fechaDevolucion');
	$having=obtieneWhereListadoEntradas("HAVING codigoCliente='$codigoCliente' AND",$columnas,false,false,false);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	$tipo=array('ENTRADASALIDA'=>'Salida y Entrada','ENTRADA'=>'Entrada','SALIDA'=>'Salida');
	$frecuencia=array('1'=>'Semanal','2'=>'Mensual','3'=>'Anual');
	
	
	conexionBD();
	$consulta=consultaBD("SELECT codigo, codigoCliente, tipo, soporte, frecuencia, usuarioAutorizado, fechaPrimeraOperacion, fechaDevolucion FROM entradas_periodicas $having $orden $limite;");
	$consultaPaginacion=consultaBD("SELECT codigo, codigoCliente, tipo, soporte, frecuencia, usuarioAutorizado, fechaPrimeraOperacion, fechaDevolucion FROM entradas_periodicas $having;");
	cierraBD();
//echo "SELECT codigo, codigoCliente, tipo, frecuencia, usuarioAutorizado, fechaPrimeraOperacion, fechaDevolucion FROM entradas_periodicas $having $orden $limite;";
	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$datosCliente=datosRegistro('clientes',$datos['codigoCliente']);
		$fila=array(
			$tipo[$datos['tipo']],
			$datos['soporte'],			
			$frecuencia[$datos['frecuencia']],
			$datos['usuarioAutorizado'],
			formateaFechaWeb($datos['fechaPrimeraOperacion']),
			formateaFechaWeb($datos['fechaDevolucion']),
        	botonAcciones(array('Detalles'),array('zona-cliente-entradas-periodicas/gestion.php?codigo='.$datos['codigo']),array("icon-search-plus")),
        	obtieneCheckTabla($datos),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function gestionEntradasPeriodicas(){
	operacionesEntrasPeriodicas();

	abreVentanaGestion('Gestión de entradas y salidas periódicas','?','span3');
	$datos=compruebaDatos('entradas_periodicas');
	$codigoCliente=obtenerCodigoCliente(true);

	campoOculto($codigoCliente,'codigoCliente');
	campoSelect('tipo','Tipo',array('Salida y Entrada','Entrada','Salida'),array('ENTRADASALIDA','ENTRADA','SALIDA'),$datos);
	campoFecha('fechaPrimeraOperacion','F. 1ª Operación',$datos);
	campoTexto('usuarioAutorizado','Usuario Autorizado',$datos);	
	campoTexto('soporte','Soporte Físico',$datos);
	campoTexto('numUnidades','Nº Unidades',$datos,'input-mini');
	campoTexto('criterio','Criterio/Tratamiento',$datos);
	campoTexto('emisor','Emisor',$datos);
	campoTexto('destinatario','Destinatario',$datos);	

	cierraColumnaCampos();
	abreColumnaCampos();

	campoSelect('frecuencia','Frecuencia',array('','Semanal','Mensual','Anual'),array('','1','2','3'),$datos,'selectpicker span2 show-tick','');
	areaTexto('motivoOperacion','Motivo de la Operación',$datos);
	campoRadio('cesionDatos','Cesión de Datos',$datos,'NO');
	campoRadio('devolucion','Devolución Obligatoria',$datos,'NO');
	campoFecha('fechaDevolucion','F. Devolución',$datos);	
	campoTexto('seguridades','Seguridades',$datos);	

	cierraVentanaGestion('index.php',true);
}

function filtroEntradasPeriodicas(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoSelect(0,'Tipo',array('','Salida y Entrada','Entrada','Salida'),array('','ENTRADASALIDA','ENTRADA','SALIDA'));
	campoSelect(2,'Frecuencias',array('','Semanal','Mensual','Anual'),array('','1','2','3'));

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(4,'F. 1ª Operación');
	campoFecha(5,'F. Devolución');	

	cierraColumnaCampos();
	cierraCajaBusqueda();
}


function obtieneNombreTutor(){
	$codigo=$_GET['codigo'];

	$datos=consultaBD("SELECT CONCAT(nombre,' ',apellido1,' ',apellido2) AS nombre FROM tutores WHERE codigo=$codigo",true,true);

	return "<a href='gestion.php?codigo=$codigo'>".$datos['nombre']."</a>";
}

function obtieneNombreListado(){
	$tutor=obtieneNombreTutor();

	$res="que $tutor está tutorizando actualmente";	

	return $res;
}

function listadoCursosTutor($codigoTutor){
	global $_CONFIG;

	$columnas=array('grupos.fechaAlta','accion','modalidad','accionFormativa','grupos.grupo','grupos.numParticipantes','fechaInicio','fechaFin','anulado','razonSocial','codigoTutor','grupos.fechaAlta');
	
	$where=obtieneWhereListado("WHERE tutores_grupo.codigoTutor=$codigoTutor",$columnas);//Utilizo WHERE por la agrupación de clientes y tutores (el HAVING realiza el filtrado TRAS la selección, y el WHERE ANTES)
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, grupos.fechaAlta, accion, modalidad, accionFormativa, grupos.grupo, grupos.numParticipantes, fechaInicio, fechaFin, anulado, razonSocial, codigoTutor, grupos.activo, grupos.codigo AS codigoGrupo
			FROM grupos LEFT JOIN acciones_formativas ON grupos.codigoAccionFormativa=acciones_formativas.codigo 
			LEFT JOIN clientes_grupo ON grupos.codigo=clientes_grupo.codigoGrupo 
			LEFT JOIN clientes ON clientes_grupo.codigoCliente=clientes.codigo 
			LEFT JOIN tutores_grupo ON grupos.codigo=tutores_grupo.codigoGrupo 
			$where 
			GROUP BY grupos.codigo";

	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			formateaFechaWeb($datos['fechaAlta']),
			$datos['accion'],
			$datos['modalidad'],
			$datos['accionFormativa'],
			$datos['grupo'],
			$datos['numParticipantes'],
			formateaFechaWeb($datos['fechaInicio']),
			formateaFechaWeb($datos['fechaFin']),
			"<div class='centro'>".$datos['anulado']."</div>",
			creaBotonDetalles("inicios-grupos/gestion.php?codigo=".$datos['codigoGrupo'],'Ver curso'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}

function creaBotonesGestionListadoTutor(){
    echo '<a class="btn-floating btn-large btn-default btn-eliminacion noAjax" href="index.php" title="Volver"><i class="icon-chevron-left"></i></a>';
}



function listadoAFTutor($codigoTutor){
	global $_CONFIG;

	$columnas=array('accion','accionFormativa','codigo');
	$having=obtieneWhereListado("WHERE codigoTutor=$codigoTutor",$columnas);
	$orden=obtieneOrdenListado($columnas);
	$limite=obtieneLimitesListado();
	
	$query="SELECT acciones_formativas.codigo, accion, accionFormativa 
			FROM acciones_formativas INNER JOIN tutores_accion_formativa ON acciones_formativas.codigo=tutores_accion_formativa.codigoAccionFormativa 
			$having";
	
	conexionBD();
	$consulta=consultaBD($query." $orden $limite;");
	$consultaPaginacion=consultaBD($query);
	cierraBD();

	$res=inicializaArrayListado($consulta,$consultaPaginacion);
	while($datos=mysql_fetch_assoc($consulta)){
		$fila=array(
			$datos['accion'],
			$datos['accionFormativa'],
        	creaBotonDetalles("acciones-formativas/gestion.php?codigo=".$datos['codigo'],'Ver Acción Formativa'),
        	"DT_RowId"=>$datos['codigo']
		);

		$res['aaData'][]=$fila;
	}

	echo json_encode($res);
}


function filtroCursosTutor(){
	abreCajaBusqueda();
	abreColumnaCampos();
	
	campoFecha(0,'Fecha desde');

	cierraColumnaCampos();
	abreColumnaCampos();

	campoFecha(11,'Hasta');

	cierraColumnaCampos();
	cierraCajaBusqueda();
}

//Fin parte de tutores

function generaDocumento($codigoCliente){
	require_once '../../api/phpword/PHPWord.php';
    $PHPWord=new PHPWord();
	
	conexionBD();

	anexoVLOPD($codigoCliente, $PHPWord, 'anexoV.docx');

	cierraBD();	

}

function datosPersonales($datos,$formulario){
	if($datos['familia']=='LOPD1'){
		$datos['razonSocial']=$formulario['pregunta3'];
		$datos['domicilio']=$formulario['pregunta4'];
		$datos['cp']=$formulario['pregunta10'];
		$datos['localidad']=$formulario['pregunta5'];
		$datos['provincia']=convertirMinuscula($formulario['pregunta11']);
		$datos['cif']=$formulario['pregunta9'];
	}

	return $datos;
}

function sanearCaracteres($texto){
	$texto = str_replace( '&', '&#38;', $texto);
	$texto = str_replace( '–', '&#45;', $texto);
	$texto = str_replace( '<br />', '<w:br/>', $texto);
	$texto = str_replace( '"', '', $texto);
	return $texto;
}

function reemplazarLogo($documento,$datos,$imagen='image1.png',$campo='ficheroLogo'){
	$hayLogo=str_replace('../documentos/logos-clientes/', '', $datos[$campo]);
    if($hayLogo!='NO' && $hayLogo!=''){
    	$logo = '../documentos/logos-clientes/'.$datos[$campo];
    	$nuevo_logo = '../documentos/logos-clientes/'.$imagen;	
		if (!copy($logo, $nuevo_logo)) {
    		echo "Error al copiar $fichero...\n";
		}	
		$documento->replaceImage('../documentos/logos-clientes/',$imagen);	
	}
}

function fechaTabla($fecha,$texto){
	return '<w:p w14:paraId="1060A2A2" w14:textId="7A44C6D1" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="00397BF1" w:rsidP="0016753F"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">REGISTRO DE '.$texto.' A </w:t></w:r><w:r w:rsidR="00D303CF"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$fecha.'</w:t></w:r></w:p>';
}

function pieTabla($total){
	return '<w:p w14:paraId="2CF98860" w14:textId="77777777" w:rsidR="00A33BF6" w:rsidRPr="00A34E51" w:rsidRDefault="001F0F57" w:rsidP="001F0F57"><w:pPr><w:spacing w:before="100" w:beforeAutospacing="1" w:after="100" w:afterAutospacing="1" w:line="240" w:lineRule="auto"/><w:jc w:val="right"/><w:rPr><w:rFonts w:ascii="Helvetica" w:eastAsia="Times New Roman" w:hAnsi="Helvetica" w:cs="Times New Roman"/><w:caps/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t>Nº total de re</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/><w:r w:rsidRPr="00A34E51"><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/></w:rPr><w:t xml:space="preserve">gistros </w:t></w:r><w:r><w:rPr><w:rFonts w:eastAsia="Times New Roman" w:cs="Times New Roman"/><w:b/></w:rPr><w:t>'.$total.'</w:t></w:r></w:p>';
}

function anexoVLOPD($codigo, $PHPWord, $nombreFichero = '') {
	
	$fichero = 'ANEXO_V_ENTRADAS_Y_SALIDAS_PERIÓDICAS.docx';
	
	$tipo = array(
		'ENTRADASALIDA' => 'Salida y Entrada',
		'ENTRADA'       => 'Entrada',
		'SALIDA'        => 'Salida'
	);
	
	$frecuencia = array(
		''  => '',
		'1' => 'Semanal',
		'2' => 'Mensual',
		'3' => 'Anual'
	);
	
	$sql = "SELECT 
				c.*, 
				sf.referencia AS familia, 
				t.formulario 
			FROM 
				trabajos t 
			INNER JOIN clientes c ON 
				t.codigoCliente = c.codigo 
			INNER JOIN servicios s ON 
				t.codigoServicio = s.codigo 
			INNER JOIN servicios_familias sf ON 
				s.codigoFamilia = sf.codigo 
			WHERE 
				c.codigo = ".$codigo;

	$datos      = consultaBD($sql, false, true);	
	$formulario = recogerFormularioServicios($datos);	
	$datos      = datosPersonales($datos,$formulario);
	
	$documento = $PHPWord->loadTemplate('../documentos/consultorias/plantilla_'.$nombreFichero);
	
	$documento->setValue("cliente",utf8_decode(sanearCaracteres($datos['razonSocial'])));	
	$documento->setValue("fecha",utf8_decode(date("d/m/Y")));
	
	reemplazarLogo($documento,$datos,'image2.png');	

	$tablaEntradasP = fechaTabla(date("d/m/Y"), 'ENTRADAS Y SALIDAS PERIÓDICAS');

	$tablaEntradasP .= '<w:tbl><w:tblPr><w:tblW w:w="13716" w:type="dxa"/><w:tblBorders><w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/><w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/></w:tblBorders><w:tblLayout w:type="fixed"/><w:tblLook w:val="04A0" w:firstRow="1" w:lastRow="0" w:firstColumn="1" w:lastColumn="0" w:noHBand="0" w:noVBand="1"/></w:tblPr><w:tblGrid><w:gridCol w:w="798"/><w:gridCol w:w="4697"/><w:gridCol w:w="1701"/><w:gridCol w:w="2268"/><w:gridCol w:w="4252"/></w:tblGrid><w:tr w:rsidR="00F44EB4" w:rsidRPr="008F5EA5" w14:paraId="4FF7F3F8" w14:textId="77777777" w:rsidTr="00F44EB4"><w:tc><w:tcPr><w:tcW w:w="798" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="0B3FED2D" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:rFonts w:ascii="Helvetica" w:hAnsi="Helvetica"/><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="008F5EA5"><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>REF.</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4697" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2E7A904B" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>USUARIO AUTORIZADO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1701" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="2EFE68E2" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>TIPO</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2268" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="710A858F" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>FECHA 1º OPERACIÓN</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="4252" w:type="dxa"/><w:shd w:val="clear" w:color="auto" w:fill="4793B3"/></w:tcPr><w:p w14:paraId="3BB2A886" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="008F5EA5" w:rsidRDefault="00F44EB4" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr></w:pPr><w:r><w:rPr><w:b/><w:color w:val="FFFFFF" w:themeColor="background1"/></w:rPr><w:t>SOPORTE</w:t></w:r></w:p></w:tc></w:tr>';

	$i = 1;	
	
	$entradasP = consultaBD("SELECT * FROM entradas_periodicas WHERE codigoCliente='".$datos['codigo']."';");
	
	while ($entrada = mysql_fetch_assoc($entradasP)) {
		
		$ref = $i < 10 ? '0'.$i : $i;
		
		$tablaEntradasP .= '<w:tr w:rsidR="00304DA2" w14:paraId="74D4D6F1" w14:textId="77777777" w:rsidTr="00304DA2"><w:trPr><w:trHeight w:val="382"/></w:trPr><w:tc><w:tcPr><w:tcW w:w="798" w:type="dxa"/></w:tcPr><w:p w14:paraId="59B5D326" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2669" w:type="dxa"/></w:tcPr><w:p w14:paraId="2F7249DF" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['usuarioAutorizado'].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1134" w:type="dxa"/></w:tcPr><w:p w14:paraId="4E88CD86" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$tipo[$entrada['tipo']].'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="1417" w:type="dxa"/></w:tcPr><w:p w14:paraId="71415B87" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRPr="002B0C55" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.formateaFechaWeb($entrada['fechaPrimeraOperacion']).'</w:t></w:r></w:p></w:tc><w:tc><w:tcPr><w:tcW w:w="2047" w:type="dxa"/></w:tcPr><w:p w14:paraId="73613FAD" w14:textId="77777777" w:rsidR="00304DA2" w:rsidRDefault="00304DA2" w:rsidP="00CD47FF"><w:pPr><w:rPr><w:b/></w:rPr></w:pPr><w:r><w:rPr><w:b/></w:rPr><w:t>'.$entrada['soporte'].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p></w:tc></w:tr>';

		$i++;		
	}	
	
	$total = $i - 1;
	$tablaEntradasP .= '</w:tbl>'.pieTabla($total);

	$tablaTexto = '';
	
	$j=1;
	
	$entradasAux = consultaBD("SELECT * FROM entradas_periodicas WHERE codigoCliente = '".$datos['codigo']."';");
	
	while ($entradasP = mysql_fetch_assoc($entradasAux)) {
		
		$ref = str_pad($j, 4, '0', STR_PAD_LEFT);
		
		$tablaTexto .= '<w:p w:rsidR="00F917B3" w:rsidRPr="002539ED" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr></w:pPr><w:r w:rsidRPr="002539ED"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr><w:t xml:space="preserve">REFERENCIA: </w:t></w:r><w:r w:rsidRPr="002539ED"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/><w:u w:val="single"/></w:rPr><w:t>'.$ref.'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:b/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">TIPO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$tipo[$entradasP['tipo']].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">FRECUENCIA: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$frecuencia[$entradasP['frecuencia']].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">FECHA 1ª OPERACIÓN:  </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaPrimeraOperacion']).'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">USUARIO AUTORIZADO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['usuarioAutorizado'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">SOPORTE FÍSICO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['soporte'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">Nº UNIDADES: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['numUnidades'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">CRITERIO / TRATAMIENTO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['criterio'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">EMISOR: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['emisor'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">DESTINATARIO: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['destinatario'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">MOTIVO DE LA OPERACIÓN: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">'.$entradasP['motivoOperacion'].' </w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">CESIÓN DE DATOS: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['cesionDatos'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">DEVOLUCIÓN OBLIGATORIA: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['devolucion'].'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">FECHA DE DEVOLUCIÓN: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.formateaFechaWeb($entradasP['fechaDevolucion']).'</w:t></w:r></w:p><w:p w:rsidR="00F917B3" w:rsidRPr="007042C4" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:pPr><w:autoSpaceDE w:val="0"/><w:autoSpaceDN w:val="0"/><w:adjustRightInd w:val="0"/><w:spacing w:after="0" w:line="240" w:lineRule="auto"/><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr></w:pPr></w:p><w:p w:rsidR="00F22FB9" w:rsidRDefault="00F917B3" w:rsidP="00F917B3"><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:bCs/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve">SEGURIDADES: </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t xml:space="preserve"> </w:t></w:r><w:r w:rsidRPr="007042C4"><w:rPr><w:rFonts w:ascii="Calibri" w:hAnsi="Calibri" w:cs="Calibri"/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="20"/></w:rPr><w:t>'.$entradasP['seguridades'].'</w:t></w:r><w:bookmarkStart w:id="0" w:name="_GoBack"/><w:bookmarkEnd w:id="0"/></w:p><w:sectPr w:rsidR="00F22FB9"><w:pgSz w:w="11906" w:h="16838"/><w:pgMar w:top="1417" w:right="1701" w:bottom="1417" w:left="1701" w:header="708" w:footer="708" w:gutter="0"/><w:cols w:space="708"/><w:docGrid w:linePitch="360"/></w:sectPr>';

		$j++;
	}


	$documento->setValue("tablaEntradas",utf8_decode($tablaEntradasP));
	$documento->setValue("tablaTexto",utf8_decode($tablaTexto));	

	$documento->save('../documentos/consultorias/'.$fichero);
	
	header("Content-Type: application/vnd.ms-docx");
    header("Content-Disposition: attachment; filename=$fichero");
    header("Content-Transfer-Encoding: binary");

    readfile('../documentos/consultorias/'.$fichero);
}

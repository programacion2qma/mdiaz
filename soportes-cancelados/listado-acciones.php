<?php
  $seccionActiva=9;
  include_once('../cabecera.php');
  
  $nombreListado=obtieneNombreListado();
?> 

<div class="main" id="contenido">
  <div class="main-inner">
    <div class="container">
      <div class="row">
		
        <?php
          creaBotonesGestionListadoTutor();
        ?>

        <div class="span12">
          <div class="widget widget-table action-table">
              <div class="widget-header"> <i class="icon-list"></i>
                <h3>Listado de acciones formativas del tutor <?php echo $nombreListado; ?></h3>
              </div>
              <!-- /widget-header -->
              <div class="widget-content">
                <table class="table table-striped table-bordered datatable" id="tablaCursos">
                  <thead>
                    <tr>
                      <th> Acción Formativa </th>
                      <th> Código AF </th>
                      <th class="centro"></th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                </table>
              </div>
              <!-- /widget-content-->
          </div>

      </div>
	  </div>
    <!-- /container --> 
  </div>
  <!-- /main-inner --> 
</div>
<!-- /main -->

<script type="text/javascript" src="../../api/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="../../api/js/bootstrap.datatable.js"></script>
<script type="text/javascript" src="../../api/js/gestionRegistros.js"></script>

<script type="text/javascript" src="../js/filtroTablaAJAXBusqueda.js"></script>

<script type="text/javascript">
  $(document).ready(function(){
    listadoTabla('#tablaCursos','../listadoAjax.php?include=tutores&funcion=listadoAFTutor(<?php echo $_GET["codigo"]; ?>);');
  });
</script>


<!-- contenido -->
</div>

<?php include_once('../pie.php'); ?>